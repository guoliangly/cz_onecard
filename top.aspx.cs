﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.PrivilegePR;
using Master;
using TM.UserManager;
using TDO.UserManager;
using TM;
using System.Runtime.Remoting.Messaging;
using System.Diagnostics;

public partial class top : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        labDepartName.Text = context.s_DepartName;
        labUserID.Text = context.s_UserID;
        labUserName.Text = context.s_UserName;
        LabCardNo.Text = context.s_CardID;

        Hashtable hash = (Hashtable)Session["LogonInfo"];
        String LogonLevel = hash["LogonLevel"].ToString();

        if (LogonLevel.Equals("Admin"))
        {
            ImgOffDuty.Visible = false;
        }
        else
        {
            ImgOffDuty.Visible = true;
        }
        Session["MenuType"] = null;
        ImgOffDuty.Attributes.Add("onclick", "return confirm('确认要签退吗?')");

        //判断交通一卡通权限
        if (CheckNewCardPower())
        {
            liNewCard.Visible = true;
        }

        string type = string.Empty;
        //logontest
        if (hash["Debugging"].ToString() == "True")
        {
            type = "test";
        }
        //logonadmin
        else if (hash["LogonLevel"].ToString() == "Admin")
        {
            type = "admin";
        }
        //普通情况
        else
        {
            type = "normal";
        }
        linkNewCard.NavigateUrl = "http://" + Session["strIp"] + ":" + ConfigurationManager.AppSettings["LoginNewServerPort"] +
            string.Format("/home/login?type={0}&deptno={1}&deptname={2}&staffno={3}&staffname={4}&operateCard={5}&token={6}",
            type, context.s_DepartID, context.s_DepartName, context.s_UserID, context.s_UserName, context.s_CardID, Session["token"]);

        btnConfirm_Click(sender, e);
    }

    /// <summary>
    /// 验证省交通一卡通权限
    /// </summary>
    /// <returns></returns>
    private bool CheckNewCardPower()
    {
        DDOBase tdoDDOBaseIn = new DDOBase();
        TD_M_MENUTM tmTD_M_MENU = new TD_M_MENUTM();
        tdoDDOBaseIn.Columns = new String[1][];
        tdoDDOBaseIn.Columns[0] = new String[] { "STAFFNO", "String" };
        tdoDDOBaseIn.Hash.Add("STAFFNO", 0);
        String[] array = new String[1];
        tdoDDOBaseIn.setArray(array);
        tdoDDOBaseIn.ArrayList.SetValue(context.s_UserID, 0);

        TD_M_MENUTDO[] tdoTD_M_MENUOutArr = tmTD_M_MENU.selByPK(context, tdoDDOBaseIn);
        bool result = false;
        foreach (TD_M_MENUTDO menu in tdoTD_M_MENUOutArr)
        {
            if (menu.PMENUNO == "N10000" || menu.PMENUNO == "N20000" || menu.PMENUNO == "N30000")
            {
                result = true;
                break;
            }
        }
        return result;
    }

    protected void linkOneCard_Click(object sender, EventArgs e)
    {

        Session["MenuType"] = null;
        linkResource.CssClass = "";
        linkOneCard.CssClass = "on";
        linkReport.CssClass = "";
        ScriptManager.RegisterStartupScript(
                this, this.GetType(), "reloadPage",
                "window.parent.frames['main'].window.frames['menu'].location.href('menu.aspx'); ", true);
    }

    protected void linkReport_Click(object sender, EventArgs e)
    {

        Session["MenuType"] = 1;
        linkResource.CssClass = "";
        linkOneCard.CssClass = "";
        linkReport.CssClass = "on";
        ScriptManager.RegisterStartupScript(
                this, this.GetType(), "reloadPage",
                "window.parent.frames['main'].window.frames['menu'].location.href('menu.aspx'); ", true);
    }

    protected void linkResource_Click(object sender, EventArgs e)
    {
        linkResource.CssClass = "on";
        linkOneCard.CssClass = "";
        linkReport.CssClass = "";
        Session["MenuType"] = 2;
        ScriptManager.RegisterStartupScript(
                this, this.GetType(), "reloadPage",
                "window.parent.frames['main'].window.frames['menu'].location.href('menu.aspx'); ", true);
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        pdo.funcCode = "QueryNewMsgNum";
        pdo.var1 = context.s_DepartID;
        pdo.var2 = context.s_UserID;

        DataTable dt = new StoreProScene().Execute(context, pdo);
        labNewMsgNum.Text = "" + dt.Rows[0].ItemArray[0];

        ScriptManager.RegisterStartupScript(
            this, this.GetType(), "refreshScript",
            "refreshNewMsgNum();", true);

    }
    protected void lbOffDuty_Click(object sender, EventArgs e)
    {
        SP_PR_RecordOnOffDutyPDO pdoSP_PR_OnOffDuty = new SP_PR_RecordOnOffDutyPDO();
        pdoSP_PR_OnOffDuty.TRADETYPECODE = "02";

        bool offDutyOk = TMStorePModule.Excute(context, pdoSP_PR_OnOffDuty);

        if (!offDutyOk)
        {
            Response.Write("<script language='javascript'>alert('签退失败');</script>");
            return;
        }

        Response.Redirect("Logon.aspx");
    }

    //欢迎光临
    protected void btnWelcome_Click(object sender, EventArgs e)
    {
        //欢迎光临正常，请您评价正常 再次按欢迎光临

        if (hidWel.Value == "1" && hidThk.Value == "1")
        {
            hidValue.Value = "09";
            btnClick_Click(sender, e);
        }

        if (hidWeValid.Value == "1")   //评价器运行出错
        {
            hidWeValid.Value = "0";
            return;
        }
        ClearSession();

        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        pdo.funcCode = "QueryServiceTime";
        DataTable dt = new StoreProScene().Execute(context, pdo);
        try
        {
            Session["beginTime"] = dt.Rows[0][0].ToString();
            hidWel.Value = "1";
        }
        catch
        {
            Session["beginTime"] = DateTime.Now.ToString("yyyyMMdd HHmmss");
        }
    }

    //请您评价
    protected void btnThank_Click(object sender, EventArgs e)
    {
        //校验评价器是否运行成功

        if (hidThValid.Value == "1")   //评价器运行出错
        {
            hidThValid.Value = "0";
            return;
        }

        try
        {
            if (string.IsNullOrEmpty(Session["beginTime"].ToString()))  //未按欢迎光临
            {
                hidWel.Value = "0";
            }
            else
            {
                hidWel.Value = "1";
            }
        }
        catch
        {
            hidWel.Value = "0";
        }
        //清空评价结果
        hidValue.Value = "";

        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        pdo.funcCode = "QueryServiceTime";
        DataTable dt = new StoreProScene().Execute(context, pdo);
        try
        {
            Session["endTime"] = dt.Rows[0][0].ToString();
            hidThk.Value = "1";
        }
        catch
        {
            Session["endTime"] = DateTime.Now.ToString("yyyyMMdd HHmmss");
        }

    }

    #region 异步注释
    //声明委托
    public delegate string GetEvaDelegate();

    /// <summary>
    /// 取得评价结果
    /// </summary>
    /// <returns>评价结果 311：非常满意  211：满意  111：一般   11：不满意  09：未评价</returns>
    private string GetEvaStr()
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        for (int i = 0; i < 3; i++)
        {
            if (sw.ElapsedMilliseconds <= 20000)    //20s内评价消息有效
            {
                if (string.IsNullOrEmpty(hidValue.Value))
                {
                    i--;
                    continue;
                }
                else
                {
                    break;
                }
            }
            else   //超过19s为超时 记为未评价
            {
                hidValue.Value = "09";
                break;
            }
        }
        sw.Reset();
        return hidValue.Value;
    }
    /// <summary>
    /// 回调函数Callback
    /// </summary>
    /// <param name="tag">异步</param>
    private void CallBack(IAsyncResult tag)
    {
        string evaStr;
        //取得AsynResult
        AsyncResult result = (AsyncResult)tag;
        //取得委托对象
        GetEvaDelegate evaDel = (GetEvaDelegate)result.AsyncDelegate;

        evaStr = evaDel.EndInvoke(tag);

        string realEva = "";
        if (evaStr == "311")
        {
            realEva = "2";  //非常满意
        }
        else if (evaStr == "211")
        {
            realEva = "3";  //满意
        }
        else if (evaStr == "111")
        {
            realEva = "4";  //一般

        }
        else if (evaStr == "11")
        {
            realEva = "5";  //不满意

        }
        else if (evaStr == "09")
        {
            realEva = "1";  //未评价

        }

        context.SPOpen();
        context.AddField("p_begintime").Value = Session["beginTime"];
        context.AddField("p_endtime").Value = Session["endTime"];
        context.AddField("p_evaluateType").Value = realEva;
        bool bl = context.ExecuteSP("SP_PR_EVALLUATE");

        hidThk.Value = "0";
        hidWel.Value = "0";
        hidWeValid.Value = "0";
        hidThValid.Value = "0";
        ClearSession();
    }
    #endregion

    //
    protected void btnClick_Click(object sender, EventArgs e)
    {
        string beginTime = Session["beginTime"].ToString();
        string endTime = Session["endTime"].ToString();
        if (string.IsNullOrEmpty(beginTime) || string.IsNullOrEmpty(endTime))
        {
            return;
        }

        string evaStr = hidValue.Value;
        string realEva = "";
        if (evaStr == "311")
        {
            realEva = "2";  //非常满意
        }
        else if (evaStr == "211")
        {
            realEva = "3";  //满意
        }
        else if (evaStr == "111")
        {
            realEva = "4";  //一般

        }
        else if (evaStr == "11")
        {
            realEva = "5";  //不满意

        }
        else if (evaStr == "09")
        {
            realEva = "1";  //未评价

        }

        context.SPOpen();
        context.AddField("p_begintime").Value = Session["beginTime"];
        context.AddField("p_endtime").Value = Session["endTime"];
        context.AddField("p_evaluateType").Value = realEva;
        bool bl = context.ExecuteSP("SP_PR_EVALLUATE");

        hidThk.Value = "0";
        hidWel.Value = "0";
        ClearSession();

        //初始化

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "set", "setI()", true);
    }


    private void ClearSession()
    {
        Session["beginTime"] = "";
        Session["endTime"] = "";
    }
}
