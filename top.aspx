<%@ Page Language="C#" AutoEventWireup="true" CodeFile="top.aspx.cs" Inherits="top" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link href="css/top.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function refreshNewMsgNum() {
            setTimeout("$get('btnConfirm').click();", 60000);
        }

        function mover() {
            //去除相邻节点样式
            var object_nav = arguments[0].parentElement.parentElement;
            for (var i = 0; i < object_nav.children.length; ++i) {
                var object_cld = object_nav.children[i].children[0];
                    object_cld.children[0].style.color = "#000";
                    object_cld.className = null;

            }

            //当前节点添加样式
            arguments[0].className = "on";
            arguments[0].children[0].style.color = "#fff";
            arguments[0].children[0].style.cursor = "pointer";
        }
        function mout() {
            var object = document.getElementById("linkOneCard");
            //mover(object);
        }  

        var i = 0;
        setInterval("calcTime()", 1000);
        function calcTime() {
            var wel = $get('hidWel').value;     //0:未按欢迎光临 1:已按欢迎光临
            var thk = $get('hidThk').value;     //0:未按请您评价 1:已按请您评价
            if (wel == 1 && thk == 1) {       
                i = i + 1;
                if (i == 20) {
                    $get('hidValue').value = "09";
                    setTimeout("$get('btnClick').click();", 0);
                }
                else {
                    ExeForEva();
                }
            }
        }

        function setI() {
            i = 0;
        }
        
    </script>

</head>
<body>


    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
            <asp:LinkButton runat="server" ID="btnClick" OnClick="btnClick_Click" />
            <asp:HiddenField ID="hidValue" runat="server" />
            <asp:HiddenField ID="hidWeValid" runat="server"  Value="0"/>
            <asp:HiddenField ID="hidThValid" runat="server"  Value="0"/>
            <asp:HiddenField ID="hidWel" runat="server" />
            <asp:HiddenField ID="hidThk" runat="server" />
            <div class="iHeader">
                <div class="header">
                    <div class="logo">
                    </div>
                    <div class="header_right">
                        <div class="detail_info">
                            <p>
                                部门名称：<asp:Label ID="labDepartName" runat="server" Text="Label"></asp:Label>
                                员工号：<asp:Label ID="labUserID" runat="server" Text="Label"></asp:Label>
                                姓名：<asp:Label ID="labUserName" runat="server" Text="Label"></asp:Label>
                                卡号：<asp:Label ID="LabCardNo" runat="server" Text="Label"></asp:Label>
                            </p>
                            <div class="option">
                                <a class="new" href="ASP/PrivilegePR/PR_Msg.aspx" target="content">新消息（<asp:Label
                                    ID="labNewMsgNum" runat="server" Text="1"></asp:Label>）</a> <a class="help" href="#"
                                        >帮助</a>
                            </div>
                        </div>
                        
                        <div class="c">
                        </div>
                        <asp:ImageButton ID="ImgWel" runat="server"  ToolTip="欢迎光临" ImageUrl="~/Images/huanyin.jpg" OnClientClick="return welcomeForEva()" 
                         OnClick="btnWelcome_Click"  Width="90px" Height="30px"></asp:ImageButton>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:ImageButton ID="ImgThank" runat="server"  ToolTip="请您评价" ImageUrl="~/Images/qingnin.jpg" OnClientClick="return ThankForEva()" 
                         OnClick="btnThank_Click"  Width="90px" Height="30px"></asp:ImageButton>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:ImageButton runat="server" ToolTip="签退" ImageUrl="Images/qiantui2.jpg" ID="ImgOffDuty"
                         Text="签退" OnClick="lbOffDuty_Click" Width="90px" Height="30px"></asp:ImageButton>
                        <ul class="nav_list">
                            <li runat="server" id="liNewCard" visible="false">
                                <asp:HyperLink ID="linkNewCard" Target="_top" runat="server" ><span class="signA">交通一卡通</span></asp:HyperLink></li>
                            <li runat="server" id="liOneCard">
                                <asp:LinkButton ID="linkOneCard" CssClass="on" runat="server"    OnClick="linkOneCard_Click">
                                    <span class="signB">日常业务</span></asp:LinkButton></li>
                            <li runat="server" id="liGroupCard">
                                <asp:LinkButton ID="linkReport"  runat="server"  OnClick="linkReport_Click">
                                    <span class="signB">报&nbsp;&nbsp;&nbsp;&nbsp;表</span></asp:LinkButton></li>
                            <li runat="server" id="liResource"> 
                                <asp:LinkButton ID="linkResource" runat="server"    OnClick="linkResource_Click"> 
                                <span class="signB">资源与维护</span></asp:LinkButton></li>
                        </ul>
                    </div>
                    <div style="display: none">
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript" src="js/cardreader.js"></script>
</body>
</html>
