﻿using System;
using System.Data;
using System.Configuration;
using Common;
using Master;

/// <summary>
/// ResidentCardFile 的摘要说明
//制卡文件处理
//1.	√	序号	        Xh	        数值	10	否	
//2.		卡的识别码	    KSBM	    字符		否	内卡号
//3.		卡的类别	    KLB	        字符		否	固定‘3’
//4.		规范版本	    KGFBB	    字符		否	固定’1.00’
//5.		初始化机构编号	KCSHJGBM	字符		否	915600000232040032040082
//6.	√	制卡批次号	    ZKPCH	    字符	30	否	注：即盒号，每箱所包含的批次号清单打印在箱体外
//7.	√	卡号	        SHBZHM	    字符	9	否	
//8.	√	证件类型	    ZJLX	    字符	3	否	二级代码需要对应
//9.	√	证件号码	    SFZHM	    字符	18	否	（卡面的社会保障号）
//10.	√	姓名	        XM	        字符	30	否	
//11.	√	性别	        XB	        字符	1	否	1男2女9未说明
//12.		民族	        MZ	        字符	2		参见民族代码表
//13.	√	出生日期	    CSRQ	    字符	8		YYYYMMDD
//14.	√	发卡日期	    FKRQ	    字符	8		YYYYMMDD（卡片印刷只到月）
//15.		指纹	        ZW	        字符	300		存放特征码
//16.		指纹编号	    ZWBH	    字符	3		0右手食指
//17.	√	通讯地址	    TXDZ	    字符	100	否	
//18.	√	邮政编码	    YZBM	    字符	6	否	
//19.	√	联系电话	    LXDH	    字符	15	否	
//20.		户籍地址	    HJDZ	    字符	100		
//21.		银行代码	    YHDM	    字符	1		1工商银行
//22.		照片	        ZP	        字符			存放的路径
//23.	√	银行卡号	    YHKH	    字符	30	否	
//24.	√	龙城通卡号	    LCTKH	    字符	20	否	
//25.	√	制卡是否成功	ZKSFCG	    字符	3	否	1成功2不成功
//26.		制卡不成功原因	ZKBCGYY	    字符	200		二级代码
//27.	√	卡商名称	    KSMC	    字符	100		注：武汉天喻，深圳德诚，北京握奇
/// </summary>
public class ResidentCardFileRecord
{
    private CmnContext context;
    private int lineCount;

    public void ResidentCardFile()
    {
    }
    public void ResidentCardFile(CmnContext context)
    {
        this.context = context;
    }


    String fldID = "";           //制卡文件序号
    public String FldID
    {
        get { return fldID; }
        set
        {
            fldID = value;
            int len = Validation.strLen(FldID);
            if (len == 0 || !Validation.isNum(FldID))
            {
                context.AddError("第" + lineCount + "制卡序号非法，必须是数字");
            }
        }
    }

    String fldPapersType = "";  //证件类型
    public String FldPapersType
    {
        get { return fldPapersType; }
        set 
        { 
            fldPapersType = value;

            if("1"==realname&&string.IsNullOrEmpty(fldPapersType))
            {
                context.AddError("第"+lineCount+"行证件类型为空");
            }
            int len = fldPapersType.Length;
            if(!string.IsNullOrEmpty(fldPapersType)&&len !=2)
            {
                context.AddError("第" + lineCount + "行证件类型长度不对");
            }
        }
    }


    String fldPaperID = "";          // 证件号码
    public String FldPaperID
    {
        get { return fldPaperID; }
        set
        {
            fldPaperID = value;
            if("1" == realname && string.IsNullOrEmpty(fldPaperID))
            {
                context.AddError("第" + lineCount + "行证件号码不能为空");
            }
            int len = Validation.strLen(fldPaperID);
            if (fldPapersType == "00")//身份证
            {
                if (len != 18 || !Validation.isIDCardNo(fldPaperID))
                {
                    context.AddError("第" + lineCount + "行身份号码非法，必须是18位数字,前17位必须是数字最后一位必须是字母或者数字");
                }
            }
        }
    }

    String fldUserName = "";        // 姓名
    public String FldUserName
    {
        get { return fldUserName; }
        set
        {
            fldUserName = value;
            if("1" == realname && string.IsNullOrEmpty(fldUserName))
            {
                context.AddError("第" + lineCount + "行姓名为空");
            }
            int len = Validation.strLen(fldUserName);
            // 姓名
            if (len > 30)
            {
                context.AddError("第" + lineCount + "行姓名超过30位");
            }
        }
    }

    String fldUserSex = "";         // 性别
    public String FldUserSex
    {
        get { return fldUserSex; }
        set
        {
            fldUserSex = value;
            int len = Validation.strLen(fldUserSex);
            if ("1" == realname)
            {
                if (len != 1 || (fldUserSex != "1" && fldUserSex != "2" && fldUserSex != "9"))
                {
                    context.AddError("第" + lineCount + "行性别非法，正确格式是长度1位，1表示男，2表示女 9未说明");
                }
            }
            else
            {
                if(!string.IsNullOrEmpty(fldUserSex))
                {
                    if (len != 1 || (fldUserSex != "1" && fldUserSex != "2" && fldUserSex != "9"))
                    {
                        context.AddError("第" + lineCount + "行性别非法，正确格式是长度1位，1表示男，2表示女 9未说明");
                    }
                }
            }
            if (fldUserSex == "1") fldUserSex = "0";
            if (fldUserSex == "2") fldUserSex = "1";
            if (fldUserSex == "9") fldUserSex = "";
        }
    }


    String fldBrith = "";       //出生日期
    public String FldBrith
    {
        get { return fldBrith; }
        set
        {
            fldBrith = value;
            int len = Validation.strLen(fldBrith);
            if (!string.IsNullOrEmpty(fldBrith))
            {
                if (len != 8 || !Validation.isDate(fldBrith, "yyyyMMdd"))
                {
                    context.AddError("第" + lineCount + "出生日期非法，格式必须是YYYYMMDD的8位的数字");
                }
            }
        }
    }


    String fldAddress = "";     //通讯地址
    public String FldAddress
    {
        get { return fldAddress; }
        set
        {
            fldAddress = value;
            int len = Validation.strLen(fldAddress);
            if (fldAddress != "")
            {
                if (len > 50)//通讯地址不能超过50个字符wdx 20111222
                {
                    context.AddError("第" + lineCount + "通讯地址非法，必须是小于50位的文本");
                }
            }
        }
    }


    String fldZip = "";         //邮政编码
    public String FldZip
    {
        get { return fldZip; }
        set
        {
            fldZip = value;
            int len = Validation.strLen(fldZip);
            if (fldZip != "")
            {
                if (len != 6 && !Validation.isNum(fldZip))
                {
                    context.AddError("第" + lineCount + "行邮政编码非法，必须是6位的数字");
                }
            }
        }
    }

    String fldPhone = "";       //联系电话
    public String FldPhone
    {
        get { return fldPhone; }
        set { fldPhone = value; }
    }

    String fldLctNo = "";          // 龙城通卡号
    public String FldLctNo
    {
        get { return fldLctNo; }
        set
        {
            fldLctNo = value;
            int len = Validation.strLen(fldLctNo);
            if (len != 16 || !Validation.isNum(fldLctNo))
            {
                context.AddError("第" + lineCount + "行IC卡号非法，必须是16位数字");
            }
        }
    }

    String realname = "";

    /// <summary>
    /// 是否记名
    /// </summary>
    public String RealName
    {
        get { return realname; }
        set 
        {
            realname = value;
            int len = realname.Length;
            if(len!=1)
            {
                context.AddError("第" + lineCount + "行是否记名标识长度不对");
            }
            if("0"!=realname&&"1"!=realname)
            {
                context.AddError("第" + lineCount + "是否记名标志值不是0或1");
            }
        }
    }    

    public void setCmnContext(CmnContext context)
    {
        this.context = context;
    }
    public void setLineCount(int lineCount)
    {
        this.lineCount = lineCount;
    }
}
