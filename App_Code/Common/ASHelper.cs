﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using Master;
using Common;
using PDO.AdditionalService;
using Controls.Customer.Asp;
using System.Collections;

// 附加业务帮助类


public class ASHelper
{
    //初始化证件类型

    public static void initPaperTypeList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadPaperCodeName");
        GroupCardHelper.fill(lst, dt, true);
    }
    //初始化支付方式类型

    public static void initPayModeList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadPayModeName");
        GroupCardHelper.fill(lst, dt, true);
    }
    public static void initMonthlyCardTypeList(DropDownList lst)
    {
        lst.Items.Add(new ListItem("---请选择---", ""));
        lst.Items.Add(new ListItem("01:学生卡", "01"));
        lst.Items.Add(new ListItem("02:人才卡", "02"));
        lst.Items.Add(new ListItem("03:老人卡", "03"));
    }

    public static void initSexList(DropDownList lst)
    {
        initSexList(lst, true);
    }

    public static void initSexList(DropDownList lst, bool allowEmpty)
    {
        if (allowEmpty) lst.Items.Add(new ListItem("---请选择---", ""));
        lst.Items.Add(new ListItem("0:男", "0"));
        lst.Items.Add(new ListItem("1:女", "1"));
    }

    public static void initPackageTypeList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadPackageCodeName");
        GroupCardHelper.fill(lst, dt, true);
    }

    // 读取园林年卡可用次数
    public static String readParkTimes(CmnContext context)
    {
        // 从全局参数表中读取园林年卡的次数设置


        DataTable data = callQuery(context, "ParkNum");
        if (data.Rows.Count == 0)
        {
            context.AddError("S00501B002: 缺少系统参数-园林年卡总共次数");
            return "0";
        }
        Object[] row = data.Rows[0].ItemArray;
        return "" + row[0];
    }

    // 读取休闲年卡可用次数
    public static String readXXParkTimes(CmnContext context)
    {
        // 从全局参数表中读取休闲年卡的次数设置


        DataTable data = ASHelper.callQuery(context, "XXParkNum");
        if (data.Rows.Count == 0)
        {
            context.AddError("S00505B002: 缺少系统参数-休闲年卡总共次数");
            return "0";
        }
        Object[] row = data.Rows[0].ItemArray;
        return (string)row[0];
    }

    // 设置损坏类型
    public static void setChangeReason(DropDownList selReasonType, bool allowEmpty)
    {
        if (allowEmpty) selReasonType.Items.Add(new ListItem("---请选择---", ""));
        selReasonType.Items.Add(new ListItem("13:可读自然损坏卡", "13"));
        selReasonType.Items.Add(new ListItem("12:可读人为损坏卡", "12"));
        selReasonType.Items.Add(new ListItem("14:不可读人为损坏卡", "14"));
        selReasonType.Items.Add(new ListItem("15:不可读自然损坏卡", "15"));
    }

    public static void prepareShouJu(PrintShouJu ptnShouJu, String cardNo, String name, String price)
    {
        //收据
        ptnShouJu.CardNo = cardNo;
        ptnShouJu.StaffName = name;
        ptnShouJu.Price = ConvertNumChn.ConvertSum(price);
        DateTime now = DateTime.Now;
        ptnShouJu.Year = now.ToString("yyyy");
        ptnShouJu.Month = now.ToString("MM");
        ptnShouJu.Day = now.ToString("dd");

    }
    public static void prepareCommonPrintPingZheng(CommonPrintPingZheng ptnCommonPrintPingZheng,ArrayList list)
    {
        //通用凭证
        DateTime now = DateTime.Now;
        ptnCommonPrintPingZheng.Year = now.ToString("yyyy");
        ptnCommonPrintPingZheng.Month = now.ToString("MM");
        ptnCommonPrintPingZheng.Day = now.ToString("dd");
        ptnCommonPrintPingZheng.PingZhengList = list;

    }
    public static void prepareShouJu(PrintShouJu ptnShouJu,
           String cardNo, String custName,
           String tradeTypeName, String tradeMoney,
           String deposit, String custAcc, String custPaperNo,
           String balance, String depreciationFee, String totalFee, String staffName,
           String tradeMode, String paperTypeName, String handlingCharge, String other
           )
    {
        DateTime now = DateTime.Now;
        ptnShouJu.Year = now.ToString("yyyy");
        ptnShouJu.Month = now.ToString("MM");
        ptnShouJu.Day = now.ToString("dd");
        ptnShouJu.TIME = now.ToString("HH:mm:ss");
        ptnShouJu.CardNo = cardNo;//旧卡
        // ptnShouJu.NewCardNo = custAcc;
        ptnShouJu.UserName = custName;
        ptnShouJu.JiaoYiLeiXing = tradeTypeName;
        ptnShouJu.JiaoYiJinE = tradeMoney;
        //ptnShouJu.JiJuHao = "";
        // ptnPZ.KaYaJin = deposit;
        // ptnPZ.ZhangHao = custAcc;
        // ptnPZ.ZhengJianHaoMa = custPaperNo;
        // ptnPZ.JiJuHao = "112233445566";
        ptnShouJu.ICKaYuE = balance;
        //  ptnPZ.ZheJiuFei = depreciationFee;
        //ptnShouJu.SERVICEFEE = "0";
        ptnShouJu.ZongJinEChina = ConvertNumChn.ConvertSum(totalFee);
        //ptnShouJu.ZongJinE = totalFee;
        ptnShouJu.StaffName = staffName;

        //  ptnPZ.JiaoYiFangShi = tradeMode;
        // ptnPZ.ZhengJianMingChen = paperTypeName;
        // ptnShouJu.LiuShuiHao = "";
        //  ptnPZ.ShouXuFei = handlingCharge;
        ptnShouJu.LiuShuiHao = other;//流水号

        // ptnShouJu.TipUser = "";
    }
    public static void preparePingZheng(PrintHMXXPingZheng ptnPZ,
          String custName, String staffName, String tradeTypeName,
          String liuShuiHao, String tradeMoney, String shouFei, String cardNo,
          String YouXiaoQi)
    {
        DateTime now = DateTime.Now;
        ptnPZ.Date = now.ToString("yyyy")+" 年 "+now.ToString("MM")+" 月 "+ now.ToString("dd")+" 日 ";
        ptnPZ.CustName = custName;
        ptnPZ.StaffName = staffName;
        ptnPZ.LiuShuiHao = liuShuiHao;

        ptnPZ.YeWuLeiXing = tradeTypeName;
        ptnPZ.FaShengJinE = tradeMoney;
        ptnPZ.CardNo = cardNo;
        ptnPZ.ShouFei = shouFei;
        ptnPZ.YouXiaoQi = YouXiaoQi;
    }

    public static void preparePingZheng(PrintPingZheng ptnPZ,
       String cardNo, String custName,
       String tradeTypeName, String tradeMoney,
       String deposit, String custAcc, String custPaperNo,
       String balance, String depreciationFee, String totalFee, String staffName,
       String tradeMode, String paperTypeName, String handlingCharge, String other
       )
    {
        DateTime now = DateTime.Now;
        ptnPZ.Year = now.ToString("yyyy");
        ptnPZ.Month = now.ToString("MM");
        ptnPZ.Day = now.ToString("dd");
        ptnPZ.TIME = now.ToString("HH:mm:ss");
        ptnPZ.CardNo = cardNo;//旧卡
        ptnPZ.NewCardNo = custAcc;
        ptnPZ.UserName = custName;
        ptnPZ.JiaoYiLeiXing = tradeTypeName;
        ptnPZ.JiaoYiJinE = tradeMoney;
        ptnPZ.JiJuHao = "";
        // ptnPZ.KaYaJin = deposit;
        // ptnPZ.ZhangHao = custAcc;
        // ptnPZ.ZhengJianHaoMa = custPaperNo;
        // ptnPZ.JiJuHao = "112233445566";
        ptnPZ.ICKaYuE = balance;
        //  ptnPZ.ZheJiuFei = depreciationFee;
        ptnPZ.SERVICEFEE = "0.0";
        ptnPZ.ZongJinEChina = ConvertNumChn.ConvertSum(totalFee);
        ptnPZ.ZongJinE = totalFee;
        ptnPZ.StaffName = staffName;

        //  ptnPZ.JiaoYiFangShi = tradeMode;
        // ptnPZ.ZhengJianMingChen = paperTypeName;
        ptnPZ.LiuShuiHao = "";
        //  ptnPZ.ShouXuFei = handlingCharge;
        ptnPZ.Other = other;//存放流水号

        ptnPZ.TipUser = "";
    }

    public static void preparePingZheng(PrintPingZheng ptnPZ,
       String cardNo, String custName,
       String tradeTypeName, String tradeMoney,
       String deposit, String custAcc, String custPaperNo,
       String balance, String depreciationFee, String totalFee, String staffName,
       String tradeMode, String paperTypeName, String handlingCharge, String other, String tipUser
       )
    {
        DateTime now = DateTime.Now;
        ptnPZ.Year = now.ToString("yyyy");
        ptnPZ.Month = now.ToString("MM");
        ptnPZ.Day = now.ToString("dd");
        ptnPZ.TIME = now.ToString("HH:mm:ss");
        ptnPZ.CardNo = cardNo;//旧卡
        ptnPZ.NewCardNo = custAcc;
        ptnPZ.UserName = custName;
        ptnPZ.JiaoYiLeiXing = tradeTypeName;
        ptnPZ.JiaoYiJinE = tradeMoney;
        ptnPZ.JiJuHao = "";
        // ptnPZ.KaYaJin = deposit;
        // ptnPZ.ZhangHao = custAcc;
        // ptnPZ.ZhengJianHaoMa = custPaperNo;
        // ptnPZ.JiJuHao = "112233445566";
        ptnPZ.ICKaYuE = balance;
        //  ptnPZ.ZheJiuFei = depreciationFee;
        ptnPZ.SERVICEFEE = "0.0";
        ptnPZ.ZongJinEChina = ConvertNumChn.ConvertSum(totalFee);
        ptnPZ.ZongJinE = totalFee;
        ptnPZ.StaffName = staffName;

        //  ptnPZ.JiaoYiFangShi = tradeMode;
        // ptnPZ.ZhengJianMingChen = paperTypeName;
        ptnPZ.LiuShuiHao = "";
        //  ptnPZ.ShouXuFei = handlingCharge;
        //ptnPZ.Other = other;//提示什么时候来办理相关业务
        ptnPZ.TipUser = tipUser;
    }

    public static void changeCardQueryValidate(CmnContext context, TextBox txtPaperNo, TextBox txtCardNo,
        TextBox txtCustName)
    {
        Validation valid = new Validation(context);
        txtCustName.Text = txtCustName.Text.Trim();
        valid.check(Validation.strLen(txtCustName.Text) <= 50, "A005010001, 客户姓名长度不能超过50");

        bool b = Validation.isEmpty(txtPaperNo);
        if (!b)
        {
            b = valid.check(Validation.strLen(txtPaperNo.Text) <= 20, "A005010006: 证件号码位数必须小于等于20");
            if (b)
            {
                valid.beAlpha(txtPaperNo, "A005010007: 证件号码必须是英文或者数字");
            }
        }

        b = Validation.isEmpty(txtCardNo);
        if (!b)
        {
            b = valid.check(Validation.strLen(txtCardNo.Text) <= 16, "A00502A003: 旧卡卡号位数必须小于等于16");
            if (b)
            {
                valid.beNumber(txtCardNo, "A00502A004: 旧卡卡号必须是数字");
            }
        }
    }

    public static string toDateWithHyphen(string dateString)
    {
        return dateString.Substring(0, 4)
            + "-" + dateString.Substring(4, 2)
            + "-" + dateString.Substring(6, 2);
    }

    public static string toDateWithoutHyphen(string dateString)
    {
        return dateString.Substring(0, 4) +
            dateString.Substring(5, 2) + dateString.Substring(8, 2);
    }

    public static string toTimeWithHyphen(string timeString)
    {
        return timeString.Substring(0, 2)
            + ":" + timeString.Substring(2, 2)
            + ":" + timeString.Substring(4, 2);
    }

    public static string toTimeWithoutHyphen(string timeString)
    {
        return timeString.Substring(0, 2) +
            timeString.Substring(3, 2) + timeString.Substring(6, 2);
    }

    public static string getCellValue(Object obj)
    {
        return (obj == DBNull.Value ? "" : (string)obj);
    }

    public static DataTable callQuery(CmnContext context, string funcCode, params string[] vars)
    {
        SP_AS_QueryPDO pdo = new SP_AS_QueryPDO();
        pdo.funcCode = funcCode;
        int varNum = 0;
        foreach (string var in vars)
        {
            switch (++varNum)
            {
                case 1:
                    pdo.var1 = var;
                    break;
                case 2:
                    pdo.var2 = var;
                    break;
                case 3:
                    pdo.var3 = var;
                    break;
                case 4:
                    pdo.var4 = var;
                    break;
                case 5:
                    pdo.var5 = var;
                    break;
                case 6:
                    pdo.var6 = var;
                    break;
                case 7:
                    pdo.var7 = var;
                    break;
                case 8:
                    pdo.var8 = var;
                    break;
                case 9:
                    pdo.var9 = var;
                    break;
                case 10:
                    pdo.var10 = var;
                    break;
                case 11:
                    pdo.var11 = var;
                    break;
                case 12:
                    pdo.var12 = var;
                    break;
            }
        }

        StoreProScene storePro = new StoreProScene();

        return storePro.Execute(context, pdo);
    }

    // 选取行政区域列表
    public static void SelectDistricts(CmnContext context, DropDownList ddl, string appType)
    {
        DataTable data = ASHelper.callQuery(context, "ReadAppArea", appType);
        ddl.Items.Clear();
        GroupCardHelper.fill(ddl, data, false);
    }

    // 读取园林信息
    public static void readGardenInfo(CmnContext context, TextBox txtCardNo,
        Label labDbExpDate, Label labDbUsableTimes, Label labDbOpenTimes)
    {
        DataTable data = callQuery(context, "ReadParkInfo", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            context.AddError("A005040001: 当前卡片不是有效的园林年卡");
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        string endDate = (string)row[0];

        String today = DateTime.Now.ToString("yyyyMMdd");
        if (endDate.CompareTo(today) < 0)
        {
            context.AddError("A005040002: 当前卡片园林年卡功能已经到期");
        }

        labDbExpDate.Text = endDate;
        labDbUsableTimes.Text = "" + row[1];
        labDbOpenTimes.Text = "" + row[2];
    }

    public static void readAccountType(CmnContext context, String cardNo, Label labAccountType)
    {
        DataTable data = callQuery(context, "QueryAccountType", cardNo);
        if (data == null || data.Rows.Count <= 0)
        {
            return;
        }
        labAccountType.Text = "" + data.Rows[0].ItemArray[0];
    }

    public static void readGardenInfo(CmnContext context, TextBox txtCardNo,
        Label labDbExpDate, Label labDbUsableTimes, Label labDbOpenTimes, Label labUpdateStaff, Label labUpdateTime)
    {
        DataTable data = callQuery(context, "ReadParkInfo", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            context.AddError("A005040001: 当前卡片不是有效的园林年卡");
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        string endDate = (string)row[0];

        String today = DateTime.Now.ToString("yyyyMMdd");
        if (endDate.CompareTo(today) < 0)
        {
            context.AddError("A005040002: 当前卡片园林年卡功能已经到期");
        }

        labDbExpDate.Text = endDate;
        labDbUsableTimes.Text = "" + row[1];
        labDbOpenTimes.Text = "" + row[2];
        labUpdateStaff.Text = "" + row[3];
        labUpdateTime.Text = "" + row[4];
    }

    public static void readCardState(CmnContext context, String cardNo, TextBox txtCardState)
    {
        DataTable data = callQuery(context, "QueryCardState", cardNo);
        if (data == null || data.Rows.Count <= 0)
        {
            return;
        }
        txtCardState.Text = "" + data.Rows[0].ItemArray[1];
    }

    /// <summary>
    /// 创建注释
    /// 创建标识：石磊 2012-07-20
    /// 方法描述：校验日期有效性

    /// 使用的表：

    /// 使用的视图：
    /// 使用的存储过程：
    /// </summary>
    /// <param name="context">上下文环境</param>
    /// <param name="StartDate">开始日期</param>
    /// <param name="EndDate">结束日期</param>
    /// <param name="errorcode">错误编码</param>
    /// <returns></returns>
    public static void checkDate(CmnContext context, TextBox StartDate, TextBox EndDate, params string[] errorcode)
    {
        Validation valid = new Validation(context);

        //校验交易开始日期，交易结束日期
        bool b1 = Validation.isEmpty(StartDate);
        bool b2 = Validation.isEmpty(EndDate);
        DateTime? fromDate = null, toDate = null;

        if (!b1)
        {
            //开始日期是否符合日期格式

            fromDate = valid.beDate(StartDate, errorcode[0]);
        }
        if (!b2)
        {
            //结束日期是否符合日期格式
            toDate = valid.beDate(EndDate, errorcode[1]);
        }

        if (fromDate != null && toDate != null)
        {
            //结束日期不能小于开始日期

            if (!(fromDate.Value.CompareTo(toDate.Value) <= 0))
            {
                context.AddError(errorcode[2]);

                context.AddErrorControl(StartDate);
                context.AddErrorControl(EndDate);
            }
        }

    }

    //初始化停车场
    public static void initParkPlaceList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadParkPlaceCodeName");
        GroupCardHelper.fill(lst, dt, true);
    }

    //初始化自行车公司信息
    public static void initBikeCompanyList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadBikeCompanyInfo");
        GroupCardHelper.fill(lst, dt, true);
    }

    //初始化区域
    public static void initAreaList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadAreaCodeName");
        GroupCardHelper.fill(lst, dt, true);
    }
    public static void initAreaList(CmnContext context, DropDownList lst ,string areaCode)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadAreaCodeName", areaCode);
        GroupCardHelper.fill(lst, dt, true);
    }


    //初始化卡类型
    public static void initCardTypeList(CmnContext context, DropDownList lst)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadCardTypeCodeName");
        GroupCardHelper.fill(lst, dt, true);
    }

    // 读取休闲信息
    public static void readRelaxInfo(CmnContext context, TextBox txtCardNo,
        Label labDbExpDate, Label labDbUsableTimes, Label labDbOpenTimes)
    {
        DataTable data = callQuery(context, "ReadXXParkInfo", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            context.AddError("A005080001: 当前卡片不是有效的休闲年卡");
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        if (Convert.IsDBNull(row[0]))
        {
            context.AddError("库有效期为空");
            return;
        }
        string endDate = (string)row[0];

        String today = DateTime.Now.ToString("yyyyMMdd");
        if (endDate.CompareTo(today) < 0)
        {
            context.AddError("A005080002: 当前卡片休闲年卡功能已经到期");
        }

        labDbExpDate.Text = endDate;
        labDbUsableTimes.Text = "" + row[1];
        labDbOpenTimes.Text = "" + row[2];
    }

    public static void readRelaxOrGardenInfo(CmnContext context, TextBox txtCardNo,
Label labRelaxEndDate, Label labAccountType, Label labGardenEndDate)
    {
        DataTable dtRelax = callQuery(context, "ReadXXParkInfo", txtCardNo.Text);
        DataTable dtGarden = callQuery(context, "ReadParkInfo", txtCardNo.Text);
        if (dtRelax.Rows.Count == 0 && dtGarden.Rows.Count == 0)
        {
            context.AddError("A005080001: 当前卡片既不是有效的休闲年卡也不是有效的园林年卡");
            return;
        }

        if (dtRelax.Rows.Count > 0 && dtGarden.Rows.Count == 0)
        {
            Object[] rowRelax = dtRelax.Rows[0].ItemArray;
            if (Convert.IsDBNull(rowRelax[0]))
            {
                context.AddError("休闲库有效期为空");
                return;
            }
            string endDateRelax = (string)rowRelax[0];
            String today = DateTime.Now.ToString("yyyyMMdd");
            if (endDateRelax.CompareTo(today) < 0)
            {
                context.AddError("A005080002: 当前卡片休闲年卡已经到期");
            }

            labRelaxEndDate.Text = endDateRelax;
            labAccountType.Text = "" + rowRelax[5];
        }

        if (dtRelax.Rows.Count == 0 && dtGarden.Rows.Count > 0)
        {
            Object[] rowGarden = dtGarden.Rows[0].ItemArray;
            if (Convert.IsDBNull(rowGarden[0]))
            {
                context.AddError("园林有效期为空");
                return;
            }
            string endDateGarden = (string)rowGarden[0];
            String today = DateTime.Now.ToString("yyyyMMdd");
            if (endDateGarden.CompareTo(today) < 0)
            {
                context.AddError("A005080002: 当前卡片园林年卡已经到期");
            }

            labGardenEndDate.Text = endDateGarden;
        }
        if (dtRelax.Rows.Count > 0 && dtGarden.Rows.Count > 0)
        {
            Object[] rowRelax = dtRelax.Rows[0].ItemArray;
            Object[] rowGarden = dtGarden.Rows[0].ItemArray;
            if (Convert.IsDBNull(rowRelax[0]) && Convert.IsDBNull(rowGarden[0]))
            {
                context.AddError("休闲库有效期和园林有效期都为空");
                return;
            }

            string endDateRelax = (string)rowRelax[0];
            string endDateGarden = (string)rowGarden[0];

            String today = DateTime.Now.ToString("yyyyMMdd");
            if (endDateRelax.CompareTo(today) < 0 && endDateGarden.CompareTo(today) < 0)
            {
                context.AddError("A005080002: 当前卡片休闲年卡和园林功能都已经到期");
            }

            labRelaxEndDate.Text = endDateRelax;
            labAccountType.Text = "" + rowRelax[5];
            labGardenEndDate.Text = endDateGarden;
        }
    }

    public static void readRelaxInfo(CmnContext context, TextBox txtCardNo,
Label labDbExpDate, Label labDbUsableTimes, Label labDbOpenTimes, Label labUpdateStaff, Label labUpdateTime)
    {
        DataTable data = callQuery(context, "ReadXXParkInfo", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            context.AddError("A005080001: 当前卡片不是有效的休闲年卡");
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        if (Convert.IsDBNull(row[0]))
        {
            context.AddError("库有效期为空");
            return;
        }
        string endDate = (string)row[0];

        String today = DateTime.Now.ToString("yyyyMMdd");
        if (endDate.CompareTo(today) < 0)
        {
            context.AddError("A005080002: 当前卡片休闲年卡功能已经到期");
        }

        labDbExpDate.Text = endDate;
        labDbUsableTimes.Text = "" + row[1];
        labDbOpenTimes.Text = "" + row[2];
        labUpdateStaff.Text = "" + row[3];
        labUpdateTime.Text = "" + row[4];
    }

    public static void readRelaxInfo(CmnContext context, TextBox txtCardNo,
    Label labDbExpDate, Label labDbUsableTimes, Label labDbOpenTimes, Label labUpdateStaff, Label labUpdateTime, Label labAccountType)
    {
        DataTable data = callQuery(context, "ReadXXParkInfo", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            context.AddError("A005080001: 当前卡片不是有效的休闲年卡");
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        if (Convert.IsDBNull(row[0]))
        {
            context.AddError("库有效期为空");
            return;
        }
        string endDate = (string)row[0];

        String today = DateTime.Now.ToString("yyyyMMdd");
        if (endDate.CompareTo(today) < 0)
        {
            context.AddError("A005080002: 当前卡片休闲年卡功能已经到期");
        }

        labDbExpDate.Text = endDate;
        labDbUsableTimes.Text = "" + row[1];
        labDbOpenTimes.Text = "" + row[2];
        labUpdateStaff.Text = "" + row[3];
        labUpdateTime.Text = "" + row[4];
        labAccountType.Text = "" + row[5];
    }

    /// <summary>
    /// 获取套餐名称
    /// </summary>
    public static void readPackage(CmnContext context, String cardNo, Label labPackage, HiddenField hidFuncType)
    {
        DataTable data = callQuery(context, "QueryPackage", cardNo);
        if (data == null || data.Rows.Count <= 0)
        {
            return;
        }
        labPackage.Text = "" + data.Rows[0].ItemArray[0];
        hidFuncType.Value = "" + data.Rows[0].ItemArray[1];
    }

}

