﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.UserManager;
using TM;
using Master;
using PDO.BusService;

/// <summary>
/// 常州公交相关的方法

/// add by liuh 
/// 暂未使用
/// </summary>
public class BusCardHelper
{
    public BusCardHelper()
    {
        // 
        // TODO: 在此处添加构造函数逻辑
        //
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="cardNo"></param>
    /// <returns></returns>
    public static int GetSupplyRightCount(CmnContext context, string cardNo)
    {
        DataTable data = BusCardHelper.callQuery(context, "QuerySupplyRightCount",cardNo);

        return Convert.ToInt32(data.Rows[0][0].ToString());
    }

    public static DataTable callQuery(CmnContext context, string funcCode, params string[] vars)
    {
        SP_BS_QueryPDO pdo = new SP_BS_QueryPDO();
        pdo.funcCode = funcCode;
        int varNum = 0;
        foreach (string var in vars)
        {
            switch (++varNum)
            {
                case 1:
                    pdo.var1 = var;
                    break;
                case 2:
                    pdo.var2 = var;
                    break;
                case 3:
                    pdo.var3 = var;
                    break;
                case 4:
                    pdo.var4 = var;
                    break;
                case 5:
                    pdo.var5 = var;
                    break;
                case 6:
                    pdo.var6 = var;
                    break;
                case 7:
                    pdo.var7 = var;
                    break;
                case 8:
                    pdo.var8 = var;
                    break;
                case 9:
                    pdo.var9 = var;
                    break;
                case 10:
                    pdo.var10 = var;
                    break;
                case 11:
                    pdo.var11 = var;
                    break;
                case 12:
                    pdo.var12 = var;
                    break;
            }
        }

        StoreProScene storePro = new StoreProScene();

        return storePro.Execute(context, pdo);
    }
}
