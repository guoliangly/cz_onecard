﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Globalization;
using Master;
using System.Text.RegularExpressions;
using System.Net;

namespace Common
{
    public class Validation
    {
        private CmnContext context;
        public Validation(CmnContext contxt)
        {
            context = contxt;
        }

        public bool notEmpty(WebControl tb, String errCode)
        {
            string value = null;

            if (tb is TextBox)
            {
                TextBox txtBox = (TextBox)tb;
                txtBox.Text = txtBox.Text.Trim();
                value = txtBox.Text;
            }
            else if (tb is DropDownList)
            {
                value = ((DropDownList)tb).SelectedValue;
            }
            if (value != null && value.Length == 0)
            {
                context.AddError(errCode, tb);
                return false;
            }
            return true;
        }

        public bool maxLength(TextBox tb, int maxlen, String errCode)
        {
            tb.Text = tb.Text.Trim();
            if (strLen(tb.Text) > maxlen)
            {
                context.AddError(errCode, tb);
                return false;
            }
            return true;
        }

        public bool fixedLength(TextBox tb, int len, String errCode)
        {
            tb.Text = tb.Text.Trim();
            if (strLen(tb.Text) != len)
            {
                context.AddError(errCode, tb);
                return false;
            }
            return true;
        }
        public bool fixedLength(TextBox tb, int[] lens, String errCode)
        {
            bool success=false;
            tb.Text = tb.Text.Trim();
            foreach (int len in lens)
            {
                if (strLen(tb.Text) == len)
                {
                    return success;
                }
            }
            context.AddError(errCode, tb);
            return false;
        }

        public bool beAlpha(TextBox tb, String errCode)
        {
            tb.Text = tb.Text.Trim();
            if (!isCharNum(tb.Text))
            {
                context.AddError(errCode, tb);
                return false;
            }
            return true;
        }

        public void beIDCardNo(TextBox tb)
        {
            tb.Text = tb.Text.Trim();

            bool b = fixedLength(tb, 18, "A300001004:二代身份证号码必须是18位");
            if (!b) return;

            if (!isNum(tb.Text.Substring(0, 17)))
            {
                context.AddError("A300001005: 身份证号前17位必须是数字", tb);
            }

            if (!isCharNum(tb.Text.Substring(17)))
            {
                context.AddError("A300001005: 身份证最后一位必须是字母或者数字", tb);
            }

        }
        public void beIDCardNo15(TextBox tb)
        {
            tb.Text = tb.Text.Trim();
            bool b = fixedLength(tb, 15, "A300001004:一代身份证号码必须是15位");
            if (!b) return;

            if (!isNum(tb.Text.Substring(0, 15)))
            {
                context.AddError("A300001005: 身份证号15位都必须是数字", tb);
            }
        }

        public static bool isIDCardNo(string tb)
        {

            if (strLen(tb) != 18) return false;

            if (!isNum(tb.Substring(0, 17))) return false;

            if (!isCharNum(tb.Substring(17))) return false;

            return true;
        }

        public long beNumber(TextBox tb, String errCode)
        {
            tb.Text = tb.Text.Trim();
            if (!isNum(tb.Text))
            {
                context.AddError(errCode, tb);
                return 0;
            }

            long ret = 0;
            try
            {
                ret = Convert.ToInt64(tb.Text);
            }
            catch (Exception)
            {
            }

            return ret;
        }

        public static int getPrice(TextBox tb)
        {
            return (int)(Double.Parse(tb.Text) * 100);
        }

        public static bool isPrice(string tb)
        {
            return Regex.IsMatch(tb, @"^\d{1,7}(\.\d{0,2})?$");
        }

        public static bool isPriceEx(string tb)
        {
            return Regex.IsMatch(tb, @"^\-?\d{1,7}(\.\d{0,2})?$");
        }

        public long bePrice(TextBox tb, String errCode)
        {
            tb.Text = tb.Text.Trim();
            bool ret = isPrice(tb.Text);
            if (!ret)
            {
                if (errCode != null)
                {
                    context.AddError(errCode, tb);
                }
                return 0;
            }
            return (long)(Double.Parse(tb.Text) * 100);
        }

        public DateTime? beDate(TextBox tb, String errCode)
        {
            tb.Text = tb.Text.Trim();
            if (!isDate(tb.Text, "yyyyMMdd"))
            {
                context.AddError(errCode, tb);
                return null;
            }

            return DateTime.ParseExact(tb.Text, "yyyyMMdd", null); ;
        }

        public bool check(bool b, String errCode)
        {
            if (!b)
            {
                context.AddError(errCode);
                return false;
            }
            return true;
        }
        public bool check(bool b, String errCode, WebControl wb)
        {
            if (!b)
            {
                context.AddError(errCode, wb);
                return false;
            }
            return true;
        }

        public static bool isEmpty(TextBox tb)
        {
            tb.Text = tb.Text.Trim();
            return tb.Text.Length == 0;
        }

        /************************************************************************
         * 检验是否是半角数字
         * @param
         * @return
         ************************************************************************/
        public static Boolean isNum(String strInput)
        {
            System.Text.RegularExpressions.Regex reg1
                        = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
            return reg1.IsMatch(strInput);
        }

        /************************************************************************
         * 检验是否是半角正实数（可有1-2位小数）
         * @param
         * @return
         ************************************************************************/
        public static Boolean isPosRealNum(String strInput)
        {
            System.Text.RegularExpressions.Regex reg1
                        = new System.Text.RegularExpressions.Regex(@"^[0-9]+(.[0-9]{1,2})?$");
            return reg1.IsMatch(strInput);
        }

        /************************************************************************
         * 检验是否是半角字符
         * @param
         * @return
         ************************************************************************/
        public static Boolean isChar(String strInput)
        {
            System.Text.RegularExpressions.Regex reg1
                        = new System.Text.RegularExpressions.Regex(@"^[A-Za-z]+$");
            return reg1.IsMatch(strInput);
        }

        /************************************************************************
         * 检验是否是半角英数字
         * @param
         * @return
         ************************************************************************/
        public static Boolean isCharNum(String strInput)
        {
            System.Text.RegularExpressions.Regex reg1
                        = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9]+$");
            return reg1.IsMatch(strInput);
        }

        /************************************************************************
         * 检验是否是EMail
         * @param
         * @return
         ************************************************************************/
        public static System.Text.RegularExpressions.Regex reg1
            = new System.Text.RegularExpressions.Regex(
                 @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                 @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                 @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

        public string isEMail(string txtEmail)
        {
            string ret = "";
            if (txtEmail.Length <= 0) return ret;

            if (strLen(txtEmail) > 30)
            {
                ret += "电子邮件地址过长(不能超过30位)\n";
                return ret;
            }


            bool retbool = reg1.IsMatch(txtEmail);
            if (!retbool)
            {
                ret += "电子邮件地址格式非法\n";
            }

            return ret;
        }

        public bool isEMail(TextBox txtEmail)
        {
            string strInput = txtEmail.Text.Trim();
            txtEmail.Text = strInput;
            if (strInput.Length <= 0) return true;

            if (strLen(strInput) > 30)
            {
                this.context.AddError("A003100043: 电子邮件地址过长(不能超过30位)", txtEmail);

                return false;
            }

            bool ret = reg1.IsMatch(strInput);
            if (!ret)
            {
                this.context.AddError("A003100028: 电子邮件地址格式非法", txtEmail);
            }

            return ret;
        }

        /************************************************************************
         * 检验是否是Date
         * @param
         * @return
         ************************************************************************/
        public static bool isDate(String strInput)
        {
            return isDate(strInput, "yyyy-MM-dd");
        }
        public static bool isTime(String strInput)
        {
            return isDate(strInput, "HHmmss");
        }

        public static bool isIPAddress(String strInput)
        {
            IPAddress ip;
            return IPAddress.TryParse(strInput, out ip);
        }

        public static bool isDate(String strInput, String fmt)
        {
            if (strInput == "")
                return false;

            try
            {
                DateTime.ParseExact(strInput, fmt, null);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /************************************************************************
         * 得到字符串的半角长度
         * @param
         * @return
         ************************************************************************/
        public static int strLen(String strInput)
        {
            return Encoding.Default.GetBytes(strInput).Length;
        }

    }
}
