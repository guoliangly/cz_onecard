﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
//using Bea.Tuxedo.ATMI;
using Common;
using System.Data.OleDb;
using System.IO;
using System.Collections;

/// <summary>
/// CitizenCardHelper 的摘要说明
/// </summary>
public class ResidentCardHelper
{
    public static void checkIDCardNo(CmnContext context, TextBox txtIdcardno, bool allowEmpty)
    {
        Validation val = new Validation(context);
        bool b = Validation.isEmpty(txtIdcardno);
        if (!allowEmpty && b)
        {
            context.AddError("A300001003: 身份证号必须输入", txtIdcardno);
            return;
        }
        if (!b)
        {
            if (txtIdcardno.Text.Trim().Length == 15)
            {
                val.beIDCardNo15(txtIdcardno);
            }
            else
            {
                val.beIDCardNo(txtIdcardno);
            }
        }
    }
    public static void checkName(CmnContext context, TextBox txtName, bool allowEmpty)
    {
        Validation val = new Validation(context);
        bool b = Validation.isEmpty(txtName);
        if (!allowEmpty && b)
        {
            context.AddError("A300001001: 姓名必须输入", txtName);
            return;
        }
        if (!b)
        {
            val.maxLength(txtName, 20, "A300001002: 姓名不能超过20位");
        }
    }


    public static int imageRadom = 232332;

    public static bool showPicture(CmnContext context, HttpRequest req, HttpResponse res)
    {
        String idCardNo = req.Params["IDCardNo"];
        if (idCardNo != null)
        {
            byte[] pic = ResidentCardHelper.ReadPicture(context, idCardNo);
            ResidentCardHelper.RespondPicture(pic, res);
            res.End();
            return true;
        }
        return false;
    }
    public static DataTable showNameCardInfo(CmnContext context, Image image0, string aspx,
        string idcardno,string papertypecode, Panel div0, Label name0, Label sex0, Label idcardno0, Label addr0, Label size0)
    {
        //image0.ImageUrl = aspx + "?Random=" + (new Random(imageRadom)).Next()
        //   + "&IDCardNo=" + idcardno;

        image0.ImageUrl = "photoBlank.jpg";

        DataTable dt = SPHelper.callResidentCardQuery(context, "QryNameCardInfo", idcardno, papertypecode);
        if (dt != null && dt.Rows.Count > 0)
        {
            div0.Visible = true;
            object[] row = dt.Rows[0].ItemArray;
            name0.Text = "" + row[0];
            sex0.Text = "" + row[1];
            idcardno0.Text = "" + row[2];
            addr0.Text = "" + row[3];
            size0.Text = "" + row[4];
        }
        else
        {
            div0.Visible = false;
        }

        return dt;
    }

    public static string getFeeInfo(DataTable dt, CmnContext context, Label labFee)
    {
        if (dt != null && dt.Rows.Count > 0)
        {
            object[] row = dt.Rows[0].ItemArray;
            string dealStateCode = "" + row[5];

            return dealStateCode;
        }

        return "";
    }

    public static void RespondPicture(byte[] pic, HttpResponse res)
    {
        if (pic != null)
        {
            res.ContentType = "application/octet-stream";
            res.AddHeader("Content-Disposition", "attachment;FileName= picture.JPG");
            res.BinaryWrite(pic);

        }
    }

    public static byte[] ReadPicture(CmnContext context, string idCardNo)
    {
        //    string selectSql = "select PICTURE from TF_RESIDENTCARD where IDCARDNO=:IDCARDNO";
        //    context.DBOpen("Select");
        //    context.AddField(":IDCARDNO").Value = idCardNo;
        //    DataTable dt = context.ExecuteReader(selectSql);
        //    context.DBCommit();
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        return (byte[])dt.Rows[0].ItemArray[0];
        //    }

        return null;
    }


    public static byte[] ReadTmpPicture(CmnContext context, string idCardNo, string sessid)
    {
        string selectSql = @"select PICTURE from TF_B_PICTURE where SESSID=:SESSID
                           and  OLDIDCARDNO=:OLDIDCARDNO";
        context.DBOpen("Select");
        context.AddField(":SESSID").Value = sessid;
        context.AddField(":OLDIDCARDNO").Value = idCardNo;
        DataTable dt = context.ExecuteReader(selectSql);
        context.DBCommit();
        if (dt != null && dt.Rows.Count > 0)
        {
            return (byte[])dt.Rows[0].ItemArray[0];
        }

        return null;
    }


    private static string wsnaddr = null;
    private static string tuxservname = null;
    private static CmnContext socContext = new CmnContext("xx");

    // 同步处理调用
    public static void Sync(CmnContext context, string tradeid)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = tradeid;
        DataTable dt = context.ExecuteReader(@"
            SELECT TRADEID, TRADETYPECODE, NAME,
                   TJTCARDNO, IDCARDNO, BANKCARDNO, SOCLSECNO,
                   OLDTJTCARDNO, OLDBANKCARDNO, OLDSOCLSECNO,
                   SSSYNCCODE, BKSYNCCODE, OPERATETIME
            FROM   TF_B_SYNC
            WHERE  TRADEID = :TRADEID
        ");
        context.DBCommit();

        Dispatch(context, dt);
    }

    public static void Sync(CmnContext context, string tradeid, string idcardno)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        DataTable dt = context.ExecuteReader(@"
            SELECT TRADEID, TRADETYPECODE, NAME,
                   TJTCARDNO, IDCARDNO, BANKCARDNO, SOCLSECNO,
                   OLDTJTCARDNO, OLDBANKCARDNO, OLDSOCLSECNO,
                   SSSYNCCODE, BKSYNCCODE, OPERATETIME
            FROM   TF_B_SYNC
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
        ");
        context.DBCommit();

        Dispatch(context, dt);
    }

    private static void Dispatch(CmnContext context, DataTable dt)
    {
        for (int rowind = 0; rowind < dt.Rows.Count; ++rowind)
        {
            DataRow currRow = dt.Rows[rowind];
            if (currRow["SSSYNCCODE"].Equals("0"))
            {
                try
                {
                    DispatchSSSync(context, currRow);
                    UpdateSSSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "1", "");
                }
                catch (Exception e)
                {
                    context.AddError("同步社保失败: " + e.Message);
                    UpdateSSSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "2", e.Message);
                }
            }
            if (currRow["BKSYNCCODE"].Equals("0"))
            {
                try
                {
                    DispatchBKSync(currRow);
                    UpdateBKSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "1", "");
                }
                catch (Exception e)
                {
                    context.AddError("同步银行失败: " + e.Message);
                    UpdateBKSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "2", e.Message);
                }
            }
        }
    }



    private static void DispatchSSSync(CmnContext context, DataRow currRow)
    {
        string tradeTypeCode = "" + currRow["TRADETYPECODE"];
        if (tradeTypeCode.Equals("02")) // 我的任务
        {
            SSSyncMyTask(context, currRow);
        }
        else if (tradeTypeCode.Equals("13")) // 拒制
        {
            SSSyncReject(context, currRow);
        }
        else if (tradeTypeCode.Equals("03")) // 换卡登记
        {
            SSSyncChangeRegister(context, currRow);
        }
        else if (tradeTypeCode.Equals("05")) // 05 新卡领卡
        {
            SSSyncPickup(context, currRow, null);
        }
        else if (tradeTypeCode.Equals("15")) // 015换卡领
        {
            SSSyncPickup(context, currRow, "03");
        }
        else if (tradeTypeCode.Equals("16")) // 16补卡领卡
        {
            SSSyncPickup(context, currRow, "04");
        }
        else if (tradeTypeCode.Equals("06")) // 补卡登记
        {
            SSSyncReissue(context, currRow);
        }
        else if (tradeTypeCode.Equals("08")) // 挂失
        {
            SSSyncReportLoss(context, currRow);
        }
        else if (tradeTypeCode.Equals("09")) // 解挂
        {
            SSSyncReportLoss(context, currRow);
        }
        else
        {
            throw new Exception("不可识别的业务类型编码" + tradeTypeCode);
        }
    }

    private static void UpdateSSSyncCode(CmnContext context, string tradeid, string idcardno, string syncCode, string errorInfo)
    {
        context.DBOpen("Update");
        context.AddField(":BKSYNCCODE").Value = syncCode;
        context.AddField(":BKSYNCERRINFO").Value = errorInfo;
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        context.ExecuteNonQuery(@"
            UPDATE TF_B_SYNC
            SET    SSSYNCCODE = :SSSYNCCODE,
                   SSSYNCERRINFO = :SSSYNCERRINFO,
                   SSSYNCTIME = SYSDATE
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
            ");
        context.DBCommit();
    }

    private static void UpdateBKSyncCode(CmnContext context, string tradeid, string idcardno, string syncCode, string errorInfo)
    {
        context.DBOpen("Update");
        context.AddField(":SSSYNCCODE").Value = syncCode;
        context.AddField(":SSSYNCERRINFO").Value = errorInfo;
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        context.ExecuteNonQuery(@"
            UPDATE TF_B_SYNC
            SET    BKSYNCCODE = :BKSYNCCODE,
                   BKSYNCERRINFO = :BKSYNCERRINFO,
                   BKSYNCTIME = SYSDATE                
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
            ");
        context.DBCommit();
    }

    private static void DispatchBKSync(DataRow currRow)
    {
    }

    // 挂失解挂
    private static void SSSyncReportLoss(CmnContext context, DataRow currRow)
    {
        socContext.DBOpen("Insert");
        socContext.AddField(":TRADETYPECODE").Value = currRow["TRADETYPECODE"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":NAME").Value = currRow["NAME"];
        socContext.AddField(":SOCLSECNO").Value = currRow["SOCLSECNO"];
        socContext.AddField(":OPERATETIME", "DateTime").Value = currRow["OPERATETIME"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_LOSTSYNC (
                MSGID, MSGTIME, MSGSTATUS, TRADETYPECODE, IDCARDNO,
                NAME, SOCLSECNO, OPERATETIME, RSRV1, RSRV2, RSRV3)
            VALUES (
                TI_TJT_LOSTSYNC_SEQ.NEXTVAL, SYSDATE, '0', :TRADETYPECODE, :IDCARDNO,
                :NAME, :SOCLSECNO, :OPERATETIME, NULL, NULL, NULL)
        ");

        socContext.DBCommit();

    }

    // 补卡
    private static void SSSyncReissue(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, "04");
    }

    // 领卡
    private static void SSSyncPickup(CmnContext context, DataRow currRow, string tradeTypeCode)
    {
        if (tradeTypeCode == null)
        {
            context.DBOpen("Select");
            context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
            DataTable dt = context.ExecuteReader(@"
                SELECT MANUTYPE
                FROM   TF_RESIDENTCARD
                WHERE IDCARDNO = :IDCARDNO
            ");
            context.DBCommit();

            if (dt.Rows.Count == 0)
            {
                return;
            }

            DataRow dr = dt.Rows[0];
            tradeTypeCode = "" + dr["MANUTYPE"];
        }


        socContext.DBOpen("Insert");
        socContext.AddField(":MANUTYPE").Value = tradeTypeCode;
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":NAME").Value = currRow["NAME"];
        socContext.AddField(":SOCLSECNO").Value = currRow["SOCLSECNO"];
        socContext.AddField(":OPERATETIME", "DateTime").Value = currRow["OPERATETIME"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_RECVSYNC (
                MSGID,MSGTIME, MSGSTATUS, MANUTYPE, 
                IDCARDNO, NAME, SOCLSECNO, OPERATETIME,
                RSRV1, RSRV2, RSRV3)
            VALUES  (
                TI_TJT_RECVSYNC_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :IDCARDNO, :NAME, :SOCLSECNO, :OPERATETIME,
                NULL, NULL, NULL )        
        ");

        socContext.DBCommit();

    }

    // 换卡登记
    private static void SSSyncChangeRegister(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, "03");
    }


    // 拒制
    private static void SSSyncReject(CmnContext context, DataRow currRow)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = currRow["TRADEID"];
        context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        DataTable dt = context.ExecuteReader(@"
            SELECT  t.APPTYPE, t.CORPNO, t.NAME, t.SEX,
                t.NATION, t.BIRTHDATE, t.RESIDENCETYPE, t.RESIDENCEADDRESS,
                t.TELEPHONE, t.HOMEADDRESS, t.POSTALCODE, t.INSURANCETYPE,
                t.INSURANCEDEPART, t.COLONYCODE, t.RSRV1, t.RSRV2,
                t.RSRV3, t.REJECTREASONCODE, t.REJECTREASONDESC, t.MANUTYPE,
                c.CORPNAME, c.CORPPHONE, c.CORPADDRESS, c.CORPPOSTALCODE
            FROM TF_B_RESIDENTREJECT  t, TD_RESIDENTCORP c
            WHERE t.TRADEID = :TRADEID
            AND   t.IDCARDNO = :IDCARDNO 
            AND   t.CORPNO = c.CORPNO(+)
        ");
        context.DBCommit();

        if (dt.Rows.Count == 0)
        {
            return;
        }

        DataRow dr = dt.Rows[0];

        socContext.DBOpen("Insert");

        socContext.AddField(":MANUTYPE").Value = dr["MANUTYPE"];
        socContext.AddField(":APPTYPE").Value = dr["APPTYPE"];
        socContext.AddField(":CORPNO").Value = dr["CORPNO"];
        socContext.AddField(":CORPNAME").Value = dr["CORPNAME"];
        socContext.AddField(":CORPPHONE").Value = dr["CORPPHONE"];
        socContext.AddField(":CORPADDRESS").Value = dr["CORPADDRESS"];
        socContext.AddField(":CORPPOSTALCODE").Value = dr["CORPPOSTALCODE"];
        socContext.AddField(":NAME").Value = dr["NAME"];
        socContext.AddField(":SEX").Value = dr["SEX"];
        socContext.AddField(":NATION").Value = dr["NATION"];
        socContext.AddField(":BIRTHDATE", "DateTime").Value = dr["BIRTHDATE"];
        socContext.AddField(":RESIDENCETYPE").Value = dr["RESIDENCETYPE"];
        socContext.AddField(":RESIDENCEADDRESS").Value = dr["RESIDENCEADDRESS"];
        socContext.AddField(":TELEPHONE").Value = dr["TELEPHONE"];
        socContext.AddField(":HOMEADDRESS").Value = dr["HOMEADDRESS"];
        socContext.AddField(":POSTALCODE").Value = dr["POSTALCODE"];
        socContext.AddField(":INSURANCETYPE").Value = dr["INSURANCETYPE"];
        socContext.AddField(":INSURANCEDEPART").Value = dr["INSURANCEDEPART"];
        socContext.AddField(":COLONYCODE").Value = dr["COLONYCODE"];
        socContext.AddField(":RSRV1").Value = dr["RSRV1"];
        socContext.AddField(":RSRV2").Value = dr["RSRV2"];
        socContext.AddField(":RSRV3").Value = dr["RSRV3"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":REJECTREASONCODE").Value = dr["REJECTREASONCODE"];
        socContext.AddField(":REJECTREASONDESC").Value = dr["REJECTREASONDESC"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_RESIDENTREJECT ( 
                MSGID, MSGTIME, MSGSTATUS, MANUTYPE,
                APPTYPE, CORPNO, CORPNAME, CORPPHONE,
                CORPADDRESS, CORPPOSTALCODE, NAME, SEX,
                NATION, BIRTHDATE, RESIDENCETYPE, RESIDENCEADDRESS,
                TELEPHONE, HOMEADDRESS, POSTALCODE, INSURANCETYPE,
                INSURANCEDEPART, COLONYCODE, RSRV1, RSRV2,
                RSRV3, IDCARDNO, REJECTREASONCODE, REJECTREASONDESC)
            VALUES ( 
                TI_TJT_CARDAPP_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :APPTYPE, :CORPNO, :CORPNAME, :CORPPHONE,
                :CORPADDRESS, :CORPPOSTALCODE, :NAME, decode(:SEX, '0', '1', '1', '2'),
                :NATION, :BIRTHDATE, :RESIDENCETYPE, :RESIDENCEADDRESS,
                :TELEPHONE, :HOMEADDRESS, :POSTALCODE, :INSURANCETYPE,
                :INSURANCEDEPART, :COLONYCODE, :RSRV1, :RSRV2,
                :RSRV3, :IDCARDNO, :REJECTREASONCODE, :REJECTREASONDESC
            )
        ");

        socContext.DBCommit();

    }

    // 我的任务社保同步
    private static void SSSyncMyTask(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, null);
    }

    private static void SSSyncCardApp(CmnContext context, DataRow currRow, string manuType)
    {
        context.DBOpen("Select");
        context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        DataTable dt = context.ExecuteReader(@"
            SELECT t.APPTYPE, t.CORPNO, t.NAME, t.SEX,
                   t.NATION, t.BIRTHDATE, t.RESIDENCETYPE, t.RESIDENCEADDRESS,
                   t.TELEPHONE, t.HOMEADDRESS, t.POSTALCODE, t.INSURANCETYPE,
                   t.INSURANCEDEPART, t.COLONYCODE, t.PICTURE, t.RSRV1,
                   t.RSRV2, t.RSRV3, t.MANUTYPE,
                   c.CORPNAME, c.CORPPHONE, c.CORPADDRESS, c.CORPPOSTALCODE
            FROM   TF_RESIDENTCARD t, TD_RESIDENTCORP c
            WHERE  t.IDCARDNO = :IDCARDNO 
            AND    t.CORPNO = c.CORPNO(+)
        ");
        context.DBCommit();

        if (dt.Rows.Count == 0)
        {
            return;
        }

        DataRow dr = dt.Rows[0];

        socContext.DBOpen("Insert");

        socContext.AddField(":MANUTYPE").Value = (manuType == null ? dr["MANUTYPE"] : manuType);
        socContext.AddField(":APPTYPE").Value = dr["APPTYPE"];
        socContext.AddField(":CORPNO").Value = dr["CORPNO"];
        socContext.AddField(":CORPNAME").Value = dr["CORPNAME"];
        socContext.AddField(":CORPPHONE").Value = dr["CORPPHONE"];
        socContext.AddField(":CORPADDRESS").Value = dr["CORPADDRESS"];
        socContext.AddField(":CORPPOSTALCODE").Value = dr["CORPPOSTALCODE"];
        socContext.AddField(":NAME").Value = dr["NAME"];
        socContext.AddField(":SEX").Value = dr["SEX"];
        socContext.AddField(":NATION").Value = dr["NATION"];
        socContext.AddField(":BIRTHDATE", "DateTime").Value = dr["BIRTHDATE"];
        socContext.AddField(":RESIDENCETYPE").Value = dr["RESIDENCETYPE"];
        socContext.AddField(":RESIDENCEADDRESS").Value = dr["RESIDENCEADDRESS"];
        socContext.AddField(":TELEPHONE").Value = dr["TELEPHONE"];
        socContext.AddField(":HOMEADDRESS").Value = dr["HOMEADDRESS"];
        socContext.AddField(":POSTALCODE").Value = dr["POSTALCODE"];
        socContext.AddField(":INSURANCETYPE").Value = dr["INSURANCETYPE"];
        socContext.AddField(":INSURANCEDEPART").Value = dr["INSURANCEDEPART"];
        socContext.AddField(":COLONYCODE").Value = dr["COLONYCODE"];
        socContext.AddField(":PICTURE", "Blob").Value = dr["PICTURE"];
        socContext.AddField(":RSRV1").Value = dr["RSRV1"];
        socContext.AddField(":RSRV2").Value = dr["RSRV2"];
        socContext.AddField(":RSRV3").Value = dr["RSRV3"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_CARDAPP ( 
                MSGID, MSGTIME, MSGSTATUS, MANUTYPE,
                APPTYPE, CORPNO, CORPNAME, CORPPHONE,
                CORPADDRESS, CORPPOSTALCODE, NAME, SEX,
                NATION, BIRTHDATE, RESIDENCETYPE, RESIDENCEADDRESS,
                TELEPHONE, HOMEADDRESS, POSTALCODE, INSURANCETYPE,
                INSURANCEDEPART, COLONYCODE, PICTURE, RSRV1,
                RSRV2, RSRV3, IDCARDNO )
            VALUES ( 
                TI_TJT_CARDAPP_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :APPTYPE, :CORPNO, :CORPNAME, :CORPPHONE,
                :CORPADDRESS, :CORPPOSTALCODE, :NAME, decode(:SEX, '0', '1', '1', '2'),
                :NATION, :BIRTHDATE, :RESIDENCETYPE, :RESIDENCEADDRESS,
                :TELEPHONE, :HOMEADDRESS, :POSTALCODE, :INSURANCETYPE,
                :INSURANCEDEPART, :COLONYCODE, :PICTURE, :RSRV1,
                :RSRV2, :RSRV3, :IDCARDNO) 
        ");

        socContext.DBCommit();
    }


    /*
    public static void SyncTuxedo(CmnContext context)
    {
        if (wsnaddr == null)
        {
            context.DBOpen("Select");
            DataTable dt = context.ExecuteReader("select CODEVALUE from TC_CODING where CODETYPE = 'CAT_SYNC_WSNADDR'");
            if (dt == null || dt.Rows.Count == 0)
            {
                if(showError)context.AddError("同步错误：没有在TC_CODING中配置类别CAT_SYNC_WSNADDR的CODEVALUE取值");
                return false;
            }

            wsnaddr = "" + dt.Rows[0].ItemArray[0];

            dt = context.ExecuteReader("select CODEVALUE from TC_CODING where CODETYPE = 'CAT_SYNC_SERVNAME'");
            if (dt == null || dt.Rows.Count == 0)
            {
                if (showError) context.AddError("同步错误：没有在TC_CODING中配置类别CAT_SYNC_SERVNAME的CODEVALUE取值");
                return false;
            }

            tuxservname = "" + dt.Rows[0].ItemArray[0]; ;
        }

        //设定环境变量
        //Utils.tuxputenv("WSNADDR=//127.0.0.1:5001");
        //Utils.tuxputenv("WSINTOPPRE71=yes");

        Bea.Tuxedo.ATMI.Utils.tuxputenv(wsnaddr);

        try
        {
            //初始化应用上下文
            Bea.Tuxedo.ATMI.TypedTPINIT tpinfo = new Bea.Tuxedo.ATMI.TypedTPINIT();
            tpinfo.flags = Bea.Tuxedo.ATMI.TypedTPINIT.TPMULTICONTEXTS;
            Bea.Tuxedo.ATMI.AppContext ac = Bea.Tuxedo.ATMI.AppContext.tpinit(tpinfo);

            //AppContext ac = AppContext.tpinit(null);

            //同步调用服务。
            // 同步调用时，服务器不返回结果或是出错之前，
            // tpcall方法不会返回，程序将等在这里。
            TypedString sndstr = new TypedString(1000);
            //sndstr.PutString(inputstring);
            //TypedBuffer rcvstr = new TypedString(1000);
            //ac.tpcall("TOUPPER", sndstr, ref rcvstr, 0);

            ac.tpacall(tuxservname, sndstr, 0);

            // 得到结果
            //string rcvstr_str = (rcvstr as TypedString).GetString(0, 1000);
            //关闭应用上下文
            ac.tpterm();
        }
        catch (Exception ex)
        {
            context.AddError("同步错误：" + ex.Message);
        }

    }
     */

    public static void fill(DropDownList ddl, DataTable dt, bool empty)
    {
        ddl.Items.Clear();


        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt.Rows.Count; ++i)
        {
            itemArray = dt.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[1] + ":" + itemArray[0], (String)itemArray[1]);
            ddl.Items.Add(li);
        }
    }
    private static ArrayList RemoveEmptyRow(DataTable dt)
    {
        ArrayList list = new ArrayList();
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                string[] strs = new string[4];
                if (row[0].ToString().Trim().Length > 0 || row[1].ToString().Trim().Length > 0
                    || row[2].ToString().Trim().Length > 0 || row[3].ToString().Trim().Length > 0)
                {

                    strs[0] = row[0].ToString();
                    strs[1] = row[1].ToString();
                    strs[2] = row[2].ToString();
                    strs[3] = row[3].ToString();
                    list.Add(strs);
                }
            }
        }
        return list;
    }
    public static void UploadCustomerInfoFile(CmnContext context, FileUpload FileUpload1, string sessionID)
    {
        GroupCardHelper.uploadFileValidate(context, FileUpload1);
        if (context.hasError())
        {
            return;
        }
        if (Path.GetExtension(FileUpload1.PostedFile.FileName) != ".xls")
        {
            context.AddError("上传的文件不是.xls类型");
        }
        if (context.hasError())
        {
            return;
        }
        if (File.Exists(context.ResourcePath + FileUpload1.FileName))
        {
            File.Delete(context.ResourcePath + FileUpload1.FileName);
        }
        byte[] bytes = FileUpload1.FileBytes;
        using (FileStream f = new FileStream(context.ResourcePath + FileUpload1.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite))
        {
            f.Write(bytes, 0, bytes.Length);
        }
        DataTable table = getExcel_info(context.ResourcePath + FileUpload1.FileName);
        ArrayList list = RemoveEmptyRow(table);
        // 首先清空临时表


        GroupCardHelper.clearTempTable(context, sessionID);
        context.DBOpen("Insert");
        if (list.Count > 0)
        {
            String[] fields = null;
            int lineCount = 0;
            int goodLines = 0;
            foreach (string[] row in list)
            {
                ++lineCount;
                fields = new String[] { row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString() };
                dealFileContent(context, fields, lineCount, sessionID);
                ++goodLines;
            }
            if (!context.hasError())
            {
                context.DBCommit();
            }
            else
            {
                context.RollBack();
            }
        }
        else
        {
            context.AddError("A004P01F01: 上传文件为空");
        }
    }

    public static DataTable getExcel_info(string filePath)
    {
        DataTable dt = new DataTable();
        string connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=YES\";";
        OleDbConnection oleconn = new OleDbConnection(connStr);
        try
        {
            oleconn.Open();
            DataTable dt1 = oleconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            if (dt1.Rows.Count > 0)
            {
                string sheet_str = dt1.Rows[0]["TABLE_NAME"].ToString();
                string selStr = "SELECT * FROM [" + sheet_str + "]";
                try
                {

                    OleDbCommand olecommand = new OleDbCommand(selStr, oleconn);
                    OleDbDataAdapter adapterin = new OleDbDataAdapter(olecommand);
                    adapterin.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        catch
        {

        }
        finally
        {
            oleconn.Close();
        }
        return dt;
    }

    public static void dealFileContent(CmnContext context,
        String[] fields, int lineCount, string sessionID)
    {
        // 证件号码
        string paperNo = fields[0].Trim();
        if (paperNo.Length <= 0)
        {
            context.AddError("第" + lineCount + "行证件号码为空");
        }
        else if (Validation.strLen(paperNo) > 20)
        {
            context.AddError("第" + lineCount + "行证件号码长度超过20位");
        }

        // 姓名
        string custName = fields[1].Trim();
        if (custName.Length <= 0)
        {
            context.AddError("第" + lineCount + "行姓名为空");
        }
        else if (Validation.strLen(custName) > 50)
        {
            context.AddError("第" + lineCount + "行姓名长度超过50位");
        }


        // 电话号码
        string custPhone = fields[2].Trim();
        // 手机号码
        string mobile = fields[3].Trim();
        if (custPhone.Length < 1 && mobile.Length < 1)
        {
            context.AddError("第" + lineCount + "行联系电话和手机号码都为空");
        }
        else
        {
            if (Validation.strLen(custPhone) > 20)
            {
                context.AddError("第" + lineCount + "行联系电话长度超过20位");
            }
            if (Validation.strLen(mobile) > 20)
            {
                context.AddError("第" + lineCount + "行手机号码长度超过20位");
            }
        }

        if (!context.hasError())
        {
            context.ExecuteNonQuery("insert into TMP_COMMON_NEW(f1, f2, f3,f4, SESSIONID) values('"
                + paperNo + "','" + custName + "','" + custPhone + "','" + mobile + "','" + sessionID + "')");
        }
    }

    //从(TC_CODING)中读取卡厂商数据，放入下拉列表中

    public static void selectManuFacturer(CmnContext context, DropDownList ddl, bool empty)
    {
        if (empty)
            ddl.Items.Add(new ListItem("---请选择---", ""));

        select(context, ddl, "MANUFACTURER");
        if (!empty && ddl.Items.Count == 0)
        {
            context.AddError("S009478002: 初始化卡厂商列表失败");
        }
    }

    //从(TC_CODING)中读取芯片类型数据，放入下拉列表中

    public static void selectChipType(CmnContext context, DropDownList ddl, bool empty)
    {
        if (empty)
            ddl.Items.Add(new ListItem("---请选择---", ""));

        select(context, ddl, "CHIPTYPE");
        if (!empty && ddl.Items.Count == 0)
        {
            context.AddError("S009478003: 初始化芯片类型列表失败");
        }
    }

    public static void select(CmnContext context, DropDownList ddl, string funcCode, params string[] vars)
    {
        DataTable dataTable = SPHelper.callResidentCardQuery(context, funcCode, vars);

        if (dataTable.Rows.Count == 0)
        {
            return;
        }

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dataTable.Rows.Count; ++i)
        {
            itemArray = dataTable.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[1] + ":" + itemArray[0], (String)itemArray[1]);
            ddl.Items.Add(li);
        }
    }

    public static long validateCardNoRangeForsmkCard(CmnContext context, TextBox txtFromCardNo, TextBox txtToCardNo,
            bool required, bool checkRange)
    {
        Validation valid = new Validation(context);
        long fromCard = -1, toCard = -1;
        long quantity = 0;

        //对起始卡号进行非空、长度、数字检验
        bool b1 = required
            ? valid.notEmpty(txtFromCardNo, "A002P01001: 起始卡号不能为空")                // 起始卡号不能为空
            : !Validation.isEmpty(txtFromCardNo);
        if (b1) b1 = valid.fixedLength(txtFromCardNo, 16, "A002P01002: 起始卡号长度必须是16位"); // 起始卡号长度必须是16位

        if (b1) fromCard = valid.beNumber(txtFromCardNo, "A002P01003: 起始卡号必须是数字");  // 起始卡号必须是数

        if (b1)
        {
            if (!txtFromCardNo.Text.Trim().StartsWith("915001"))
            {
                context.AddError("起始卡号不是市民卡,不能出入库",txtFromCardNo);
                fromCard = -1;
            }
        }

        //对终止卡号进行非空、长度、数字检验

        txtToCardNo.Text = txtToCardNo.Text.Trim();
        if (txtToCardNo.Text.Length == 0 && !context.hasError())
        {
            txtToCardNo.Text = txtFromCardNo.Text;
        }

        bool b2 = required
           ? valid.notEmpty(txtToCardNo, "A002P01004: 终止卡号不能为空")                 // 终止卡号不能为空
           : !Validation.isEmpty(txtToCardNo);
        if (b2) b2 = valid.fixedLength(txtToCardNo, 16, "A002P01005: 终止卡号必须是16位"); // 终止卡号长度必须是16位

        if (b2) toCard = valid.beNumber(txtToCardNo, "A002P01006: 终止卡号必须是数字");    // 终止卡号必须是数

        if (b2)
        {
            if (!txtToCardNo.Text.Trim().StartsWith("915001"))
            {
                context.AddError("终止卡号不是市民卡,不能出入库",txtToCardNo);
                toCard = -1;
            }
        }

        if (fromCard >= 0 && toCard >= 0)
        {
            quantity = toCard - fromCard + 1;

            if (checkRange)
            {
                b2 = valid.check(quantity > 0, "A002P01007: 起始卡号不能大于终止卡号");               // 终止卡号不能小于起始卡号
                if (b2) valid.check(quantity <= 10000, "A002P01008: 终止卡号不能超过起始卡号10000");        // 终止卡号不能超过起始卡号10000以上
            }
        }
        return quantity;
    }
}
