﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using TM;
using TDO.CardManager;
using Master;
using TDO.UserManager;

/// <summary>
/// CommonHelper 的摘要说明

/// </summary>
public class CommonHelper
{
    public static void clearTempTable(Master.CmnContext context)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_COMMON");
        context.DBCommit();
    }

    public static void clearNewTempTable(Master.CmnContext context, string sessionID)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_COMMON_NEW where SESSIONID = '" + sessionID + "'");
        context.DBCommit();
    }

    public static void clearTempTable(Master.CmnContext context,string sessionID)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from tmp_residentcard_input where SESSIONID = '" + sessionID + "'");
        context.DBCommit();
    }

    //是否网点负责人

    public static bool IsDepartLead(Master.CmnContext context)
    {
        string sql = string.Format("select count(*) from TD_M_ROLEPOWER rp where rp.powercode='201007' and rp.powertype='2' and rp.roleno in( select ROLENO from TD_M_INSIDESTAFFROLE ti where ti.Staffno='{0}')", context.s_UserID);
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable dt = tmTMTableModule.selByPKDataTable(context, sql, 0);
        Object obj = dt.Rows[0].ItemArray[0];
        if (obj == DBNull.Value)
            return false;
        return Convert.ToInt32(obj) > 0;
    }
    public static bool IsDepartLead(Master.CmnContext context,string staffno)
    {
        string sql = string.Format("select count(*) from TD_M_ROLEPOWER rp where rp.powercode='201007' and rp.powertype='2' and rp.roleno in( select ROLENO from TD_M_INSIDESTAFFROLE ti where ti.Staffno='{0}')", staffno);
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable dt = tmTMTableModule.selByPKDataTable(context, sql, 0);
        Object obj = dt.Rows[0].ItemArray[0];
        if (obj == DBNull.Value)
            return false;
        return Convert.ToInt32(obj) > 0;
    }
    //设置当前可打印打票代码和发票号码
    public static void SetInvoiceValues(Master.CmnContext context,Control volumetxt, Control invoicetxt)
    {
        string sql = string.Format("SELECT  VOLUMENO,INVOICENO  FROM TL_R_INVOICE tri where tri.ALLOTSTATECODE='02' and tri.usestatecode='00' and tri.ALLOTSTAFFNO='{0}' "
            + "  and allottime in (select min(allottime)  FROM TL_R_INVOICE tri where tri.ALLOTSTATECODE='02' and tri.usestatecode='00' and tri.ALLOTSTAFFNO='{0}' )  order by volumeno asc,invoiceno asc ", context.s_UserID);
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable data = tmTMTableModule.selByPKDataTable(context, sql, 0);
        if (data != null && data.Rows.Count > 0)
        {
            if (volumetxt is TextBox)
            {
                ((TextBox)volumetxt).Text = data.Rows[0][0].ToString();
            }
            if (invoicetxt is TextBox)
            {
                ((TextBox)invoicetxt).Text = data.Rows[0][1].ToString();
            }
            if (volumetxt is Label)
            {
                ((Label)volumetxt).Text = data.Rows[0][0].ToString();
            }
            if (invoicetxt is Label)
            {
                ((Label)invoicetxt).Text = data.Rows[0][1].ToString();
            }
        }
        else
        {
            if (volumetxt is TextBox)
            {
                ((TextBox)volumetxt).Text ="";
            }
            if (invoicetxt is TextBox)
            {
                ((TextBox)invoicetxt).Text ="";
            }
            if (volumetxt is Label)
            {
                ((Label)volumetxt).Text = "";
            }
            if (invoicetxt is Label)
            {
                ((Label)invoicetxt).Text = "";
            }
        }
    }

    //设置最近一笔已打印的打票代码和发票号码
    public static void SetInvoicedValues(Master.CmnContext context, Control volumetxt, Control invoicetxt)
    {
        string sql = string.Format("select * from (  SELECT  tri.VOLUMENO,tri.INVOICENO  FROM TL_R_INVOICE tri inner join TF_F_INVOICE f on f.invoiceno=tri.invoiceno and f.volumeno=tri.volumeno where  tri.ALLOTSTATECODE='02' and tri.usestatecode='01' and tri.ALLOTSTAFFNO='{0}'  order by f.operatetime desc) temp where rownum=1", context.s_UserID);
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable data = tmTMTableModule.selByPKDataTable(context, sql, 0);
        if (data != null && data.Rows.Count > 0)
        {
            if (volumetxt is TextBox)
            {
                ((TextBox)volumetxt).Text = data.Rows[0][0].ToString();
            }
            if (invoicetxt is TextBox)
            {
                ((TextBox)invoicetxt).Text = data.Rows[0][1].ToString();
            }
            if (volumetxt is Label)
            {
                ((Label)volumetxt).Text = data.Rows[0][0].ToString();
            }
            if (invoicetxt is Label)
            {
                ((Label)invoicetxt).Text = data.Rows[0][1].ToString();
            }
        }
        else
        {
            if (volumetxt is TextBox)
            {
                ((TextBox)volumetxt).Text = "";
            }
            if (invoicetxt is TextBox)
            {
                ((TextBox)invoicetxt).Text = "";
            }
            if (volumetxt is Label)
            {
                ((Label)volumetxt).Text = "";
            }
            if (invoicetxt is Label)
            {
                ((Label)invoicetxt).Text = "";
            }
        }
    }


    public static void readCardJiMingState(Master.CmnContext context, String cardNo, HiddenField hidIsJiMing)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从卡资料表中读取数据
        TF_F_CARDRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = cardNo;
        TF_F_CARDRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CARDRECTDO), null);
        hidIsJiMing.Value = ddoTF_F_CUSTOMERRECOut.CUSTRECTYPECODE=="1"?"1":"0";
    }

    public static void fill(DropDownList ddl, DataTable dt, bool empty)
    {
        ddl.Items.Clear();

        if (empty)
            ddl.Items.Add(new ListItem("---请选择---", ""));

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt.Rows.Count; ++i)
        {
            itemArray = dt.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[1] + ":" + itemArray[0], (String)itemArray[1]);
            ddl.Items.Add(li);
        }
    }
    //18位身份证号验证

    public static string CheckCidInfo(string cid1)
    {
        string cid = cid1;
        if (cid.Length == 15)
        {
            cid = per15To18(cid);
        }
        string[] aCity = new string[] { null, null, null, null, null, null, null, null, null, null, null, "北京", "天津", "河北", "山西", "内蒙古", null, null, null, null, null, "辽宁", "吉林", "黑龙江", null, null, null, null, null, null, null, "上海", "江苏", "浙江", "安微", "福建", "江西", "山东", null, null, null, "河南", "湖北", "湖南", "广东", "广西", "海南", null, null, null, "重庆", "四川", "贵州", "云南", "西藏", null, null, null, null, null, null, "陕西", "甘肃", "青海", "宁夏", "xinjiang", null, null, null, null, null, "台湾", null, null, null, null, null, null, null, null, null, "香港", "澳门", null, null, null, null, null, null, null, null, "国外" };
        double iSum = 0;
        System.Text.RegularExpressions.Regex rg = new System.Text.RegularExpressions.Regex(@"^\d{17}(\d|x)$");
        System.Text.RegularExpressions.Match mc = rg.Match(cid);
        if (!mc.Success)
        {
            return "规则有误";
        }
        cid = cid.ToLower();
        cid = cid.Replace("x", "a");
        if (aCity[int.Parse(cid.Substring(0, 2))] == null)
        {
            return "非法地区";
        }
        try
        {
            DateTime.Parse(cid.Substring(6, 4) + "-" + cid.Substring(10, 2) + "-" + cid.Substring(12, 2));
        }
        catch
        {
            return "非法生日";
        }
        for (int i = 17; i >= 0; i--)
        {
            iSum += (System.Math.Pow(2, i) % 11) * int.Parse(cid[17 - i].ToString(), System.Globalization.NumberStyles.HexNumber);

        }
        if (iSum % 11 != 1)
            return "非法证号";

        return "";
    }
    //15位身份证号转18位身份证号

    private static string per15To18(string perIDSrc)
    {
        int iS = 0;

        //加权因子常数 
        int[] iW = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
        //校验码常数 
        string LastCode = "10X98765432";
        //新身份证号 
        string perIDNew;

        perIDNew = perIDSrc.Substring(0, 6);
        //填在第6位及第7位上填上‘1’，‘9’两个数字 
        perIDNew += "19";

        perIDNew += perIDSrc.Substring(6, 9);

        //进行加权求和 
        for (int i = 0; i < 17; i++)
        {
            iS += int.Parse(perIDNew.Substring(i, 1)) * iW[i];
        }

        //取模运算，得到模值 
        int iY = iS % 11;
        //从LastCode中取得以模为索引号的值，加到身份证的最后一位，即为新身份证号。 
        perIDNew += LastCode.Substring(iY, 1);
        return perIDNew;
    }




    public static bool CheckIDCard(string Id)
    {
        if (Id.Length == 18)
        {
            bool check = CheckIDCard18(Id);
            return check;
        }
        else if (Id.Length == 15)
        {
            bool check = CheckIDCard15(Id);
            return check;
        }
        else
        {
            return false;
        }
    }

    private static bool CheckIDCard18(string Id)
    {
        long n = 0;
        if (long.TryParse(Id.Remove(17), out n) == false || n < Math.Pow(10, 16) || long.TryParse(Id.Replace('x', '0').Replace('X', '0'), out n) == false)
        {
            return false;//数字验证
        }
        string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
        if (address.IndexOf(Id.Remove(2)) == -1)
        {
            return false;//省份验证
        }
        string birth = Id.Substring(6, 8).Insert(6, "-").Insert(4, "-");
        DateTime time = new DateTime();
        if (DateTime.TryParse(birth, out time) == false)
        {
            return false;//生日验证
        }
        string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
        string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
        char[] Ai = Id.Remove(17).ToCharArray();
        int sum = 0;
        for (int i = 0; i < 17; i++)
        {
            sum += int.Parse(Wi[i]) * int.Parse(Ai[i].ToString());
        }
        int y = -1;
        Math.DivRem(sum, 11, out y);
        if (arrVarifyCode[y] != Id.Substring(17, 1).ToLower())
        {
            return false;//校验码验证

        }
        return true;//符合GB11643-1999标准
    }

    private static bool CheckIDCard15(string Id)
    {
        long n = 0;
        if (long.TryParse(Id, out n) == false || n < Math.Pow(10, 14))
        {
            return false;//数字验证
        }
        string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
        if (address.IndexOf(Id.Remove(2)) == -1)
        {
            return false;//省份验证
        }
        string birth = Id.Substring(6, 6).Insert(4, "-").Insert(2, "-");
        DateTime time = new DateTime();
        if (DateTime.TryParse(birth, out time) == false)
        {
            return false;//生日验证
        }
        return true;//符合15位身份证标准
    }

    /// <summary>
    /// 取联系地址
    /// </summary>
    /// <param name="custAddress">联系地址</param>
    /// <returns>取加密联系地址</returns>
    public static string GetCustAddress(string custAddress)
    {
        if (custAddress.Length > 3)
        {
            return custAddress.Substring(0, custAddress.Length - 3) + string.Empty.PadRight(3, '*');
        }
        else
        {
            return custAddress;
        }
    }

    /// <summary>
    /// 取联系电话
    /// </summary>
    /// <param name="custPhone">联系电话</param>
    /// <returns>加密联系电话</returns>
    public static string GetCustPhone(string custPhone)
    {
        if (custPhone.Length == 11)
        {
            return custPhone.Substring(0, 3) + string.Empty.PadRight(4, '*') + custPhone.Substring(7, 4);
        }
        else if (custPhone.Length > 2)
        {
            return custPhone.Substring(0, custPhone.Length - 2) + string.Empty.PadRight(2, '*');
        }
        else
        {
            return custPhone;
        }
    }

    /// <summary>
    /// 取身份证格式
    /// </summary>
    /// <param name="paperNo">身份证号</param>
    /// <returns>加密身份证</returns>
    public static string GetPaperNo(string paperNo)
    {
        if (paperNo.Length == 18)   //第二代身份证
        {
            return paperNo.Substring(0, 10) + string.Empty.PadRight(4, '*') + paperNo.Substring(14, 4);
        }
        else if (paperNo.Length == 15)  //第一代身份证
        {
            return paperNo.Substring(0, 8) + string.Empty.PadRight(4, '*') + paperNo.Substring(12, 3);
        }
        else if (paperNo.Length > 2)    //超过2位的最后两位改*
        {
            return paperNo.Substring(0, paperNo.Length - 2) + string.Empty.PadRight(2, '*');
        }
        else
        {
            return paperNo;
        }
    }

    /// <summary>
    /// 检查是否有权限
    /// </summary>
    /// <param name="context">上下文环境</param>
    /// <returns>当前用户是否有权限</returns>
    public static bool HasOperPower(CmnContext context)
    {
        return true;
        string powerCode = "201015";//客户信息查看权限编码
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tmTMTableModule.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// 检查是否有权限
    /// </summary>
    /// <param name="context">上下文环境</param>
    /// <returns>当前用户是否有权限</returns>
    public static bool HasOperPower(CmnContext context, string powerCode)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tmTMTableModule.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    public static string IsExpCardno(string cardno)
    {
        //因卡片制作过程中出错，导致卡面显示卡号异常,增加提示 wdx 20140410
        if (cardno.Length == 16 && cardno.Substring(0, 8) == "91500213")
        {
            if (Common.Validation.isNum(cardno) && (Convert.ToInt32(cardno.Substring(8, 8)) >= 86700001 && Convert.ToInt32(cardno.Substring(8, 8)) <= 86702000))
            {
                return "你好，你输入的卡号" + cardno + "实际应为" + cardno.Substring(0, 8) + "8673" + cardno.Substring(12, 4) + "，请验证并确认！";
            }
            else if (Common.Validation.isNum(cardno) && (Convert.ToInt32(cardno.Substring(8, 8)) >= 86702001 && Convert.ToInt32(cardno.Substring(8, 8)) <= 86730000))
            {
                return "你好，你输入的卡号有误，请根据卡面上卡号前8位调整录入，请验证并确认！";
            }

        }
        return "";
    }

    /// <summary>
    /// dataview中加密的字段解密
    /// </summary>
    /// <param name="dataview">需解密的dataview</param>
    /// <param name="list">需解密的字段</param>
    public static void AESDeEncrypt(DataView dataview, List<string> list)
    {

        StringBuilder strBuilder = new StringBuilder();

        if (list.Count == 0)
        {
            return;
        }

        for (int i = 0; i < dataview.Count; i++)
        {
            for (int s = 0; s < list.Count; s++)
            {
                if (dataview.Table.Columns.Contains(list[s]))
                {
                    strBuilder = new StringBuilder();
                    AESHelp.AESDeEncrypt(dataview[i][list[s]].ToString(), ref strBuilder);
                    dataview[i][list[s]] = strBuilder.ToString();
                }
            }
        }
    }

    /// <summary>
    /// datatable中加密的字段解密
    /// </summary>
    /// <param name="datatable">需解密的datatable</param>
    /// <param name="list">需解密的字段</param>
    public static void AESDeEncrypt(DataTable datatable, List<string> list)
    {

        StringBuilder strBuilder = new StringBuilder();

        if (list.Count == 0)
        {
            return;
        }

        for (int i = 0; i < datatable.Rows.Count; i++)
        {
            for (int s = 0; s < list.Count; s++)
            {
                if (datatable.Columns.Contains(list[s]))
                {
                    strBuilder = new StringBuilder();
                    AESHelp.AESDeEncrypt(datatable.Rows[i][list[s]].ToString(), ref strBuilder);
                    datatable.Rows[i][list[s]] = strBuilder.ToString();
                }
            }
        }
    }

    /// <summary>
    /// datatable中加密的字段解密。对于PAPERNAME字段值为000000000的不解密
    /// </summary>
    /// <param name="datatable">需解密的datatable</param>
    /// <param name="list">需解密的字段</param>
    public static void AESSpeDeEncrypt(DataTable datatable, List<string> list)
    {

        StringBuilder strBuilder = new StringBuilder();

        if (list.Count == 0)
        {
            return;
        }

        for (int i = 0; i < datatable.Rows.Count; i++)
        {
            for (int s = 0; s < list.Count; s++)
            {
                if (datatable.Columns.Contains(list[s]))
                {
                    if (list[s] == "PAPERNAME" && datatable.Rows[i][list[s]].ToString() == "000000000")
                    {
                        continue;
                    }
                    else
                    {
                        strBuilder = new StringBuilder();
                        AESHelp.AESDeEncrypt(datatable.Rows[i][list[s]].ToString(), ref strBuilder);
                        datatable.Rows[i][list[s]] = strBuilder.ToString();
                    }
                }
            }
        }
    }

    /// <summary>
    /// 加密字段解密
    /// </summary>
    /// <param name="ddoBase">数据表基类</param>
    /// <returns>解密之后的数据表</returns>
    public static DDOBase AESDeEncrypt(DDOBase ddoBase)
    {

        if (ddoBase == null)
        {
            return ddoBase;
        }

        StringBuilder strBuilder = new StringBuilder();

        List<string> list = new List<string>(new string[] { "CUSTNAME", "CUSTPHONE", "CUSTADDR", "PAPERNO" });
        for (int i = 0; i < ddoBase.Columns.Length; i++)
        {
            if (list.Contains(ddoBase.Columns[i][0]))
            {
                strBuilder = new StringBuilder();
                AESHelp.AESDeEncrypt(ddoBase.ArrayList.GetValue(i).ToString(), ref strBuilder);
                ddoBase.ArrayList.SetValue(strBuilder.ToString(), i);
            }
        }

        return ddoBase;

    }

    public static bool allowCardtype(CmnContext context, string cardno, params string[] cardtypes)
    {
        if (cardno.Length != 16)
        {
            context.AddError("卡号必须是16位");
            return false;
        }
        else if (Common.Validation.isNum(cardno) == false)
        {
            context.AddError("卡号必须是数字");
            return false;
        }

        foreach (string cardtype in cardtypes)
        {
            if (cardtype == "5101")
            {
                if ("5101".Equals(cardno.Substring(4, 4)))
                {
                    context.AddError("不能在该页面办理旅游年卡相关业务!");
                }
            }

            if (cardtype == "5103")
            {
                if ("5103".Equals(cardno.Substring(4, 4)))
                {
                    context.AddError("不能在该页面办理世乒旅游卡相关业务!");
                }
            }
        }

        return !(context.hasError());
    }
}
