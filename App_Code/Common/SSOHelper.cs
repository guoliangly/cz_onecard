﻿namespace SSO
{

    using System;
    using System.Data;
    using System.Text;
    using Common;
    using Master;

    /// <summary>
    ///SSOHelper 的摘要说明
    /// </summary>
    public class SSOHelper
    {

        //更新令牌
        public static bool UpdateToken(CmnContext context, string token, string staffTable, string staff)
        {
            context.DBOpen("StorePro");
            context.AddField("P_STAFF").Value = staff;
            context.AddField("P_TOKEN").Value = token;
            context.AddField("P_TABLE").Value = staffTable;
            bool ok = context.ExecuteSP("SP_SSO_UPDATETOKEN");
            return ok;
        }

        //登出
        public static string Logout(DataTable dt)
        {
            StringBuilder result = new StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                result.AppendLine("<iframe width=\"0\" height=\"0\" id=\"frameSSO\" src=\"" + dr["LogoutURL"].ToString() + "\" style=\"border-width: 0px;\"></iframe>");
            }
            return result.ToString();
        }

        //登录校验
        public static bool CheckLogin(bool isAdmin,CmnContext context, string username,
            string opercardno, string pwd, string ip, ref string loginURL, ref string staffTable, ref DataTable sysinfos)
        {
            DateTime lastActiveTiem = DateTime.MinValue;
            string lastActiceSys;
            string[] parm = new string[4];
            parm[0] = username;
            parm[1] = opercardno;
            parm[2] = DecryptString.EncodeString(pwd);
            parm[3] = ip;
            DataTable data = null;
            if (!isAdmin)
            {
                data = SPHelper.callQuery("SP_SSO_CHECKUSER", context, "CheckStaff", parm);
                if (data.Rows.Count == 0)
                {
                    context.AddError("A100001003"); //用户名密码错误
                    return false;
                }
            }
            else
            {
                data = SPHelper.callQuery("SP_SSO_CHECKUSER", context, "CheckStaffInAdmin", parm);//add by liuhe20120928 电子钱包系统添加admin页面登录限制
                if (data.Rows.Count == 0)
                {
                    context.AddError("用户名密码错误或存在登录限制"); 
                    return false;
                }
            }

            if (!isAdmin && data.Select("OPERCARDNO = '" + opercardno + "'").Length == 0)
            {
                context.AddError("A100001008"); //操作员卡不匹配
                return false;
            }
            if (data.Select("DIMISSIONTAG = '1'").Length == 0)
            {
                context.AddError("A100001007"); //用户无效
                return false;
            }
            if (!isAdmin)
            {
                data = SPHelper.callQuery("SP_SSO_CHECKUSER", context, "CheckStaffLogin", parm);
                if (data.Rows.Count == 0)
                {
                    context.AddError("A100001006"); //登录限制
                    return false;
                }
            }
            foreach (DataRow dr in sysinfos.Rows)
            {
                //检查校验结果
                string sysName = dr["SysName"].ToString();
                DataRow[] checkData = data.Select("SYSNAME='" + sysName + "'");
                dr.BeginEdit();
                if (checkData.Length == 1)
                {
                    dr["IsAuth"] = true;
                    DateTime lastActiveTime = DateTime.MinValue;
                    if (checkData[0]["LAST_ACTIVE_TIME"] != null && checkData[0]["LAST_ACTIVE_TIME"].ToString() !="" )
                    {
                        lastActiveTime = (DateTime)checkData[0]["LAST_ACTIVE_TIME"];
                    }
                    // 根据校验结果，找出当前用户最后活动的子系统
                    if ((checkData[0]["LAST_ACTIVE_TIME"].ToString() == "" && lastActiveTiem == DateTime.MinValue)
                        || lastActiveTiem < lastActiveTime)
                    {
                        lastActiveTiem = checkData[0]["LAST_ACTIVE_TIME"].ToString() == "" ?
                            DateTime.MinValue : lastActiveTime;
                        lastActiceSys = dr["SysName"].ToString();
                        loginURL = dr["LoginURL"].ToString();
                        staffTable = dr["StaffTable"].ToString();
                    }
                    //检查市民卡DBLINK
                    if (checkData[0]["SMKCHECK"].ToString() == "ERROR")
                    {
                        System.Web.HttpContext.Current.Session["SMKCHECK"] = false;
                    }
                }
                else
                {
                    dr["IsAuth"] = false;
                }
                dr.EndEdit();
            }
            return true;
        }

    }
}