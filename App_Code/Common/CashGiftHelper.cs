﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;

/// <summary>
/// CashGiftHelper 的摘要说明
/// </summary>
public class CashGiftHelper
{
    // 初始化有效期选项
    public static void initValidMonths(CmnContext context, DropDownList lst, bool allowEmpty)
    {

        SPHelper.fillCodingWoCode(context, lst, allowEmpty, "CATE_CG_VALID");
    }
}
