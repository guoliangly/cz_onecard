﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using Common;
using Master;
using TM;
using PDO.ChargeCard;
using Master;
using System.Net;
using System.Net.Sockets;
using System.Text;

/// <summary>
/// 充付器模块公共方法
/// </summary>
public class ReaderHelper
{
	public ReaderHelper()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}

    /// <summary>
    /// 查询充付器类型绑定到dropdownlist
    /// </summary>
    public static void ReaderTypeBindDDL(CmnContext context, DropDownList ddlReaderType)
    {
        string ReaderTypeSql = string.Format("select typeid,code||':'||name name from tf_f_readertype order by typeid");
        context.DBOpen("Select");
        DataTable ReaderTypeData = context.ExecuteReader(ReaderTypeSql);
        ddlReaderType.DataTextField = "name";
        ddlReaderType.DataValueField = "typeid";
        ddlReaderType.DataSource = ReaderTypeData;
        ddlReaderType.DataBind();

        ddlReaderType.Items.Insert(0, new ListItem("---请选择---", ""));
    }

    /// <summary>
    /// 厂商绑定到dropdownlist
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ddlManu"></param>
    public static void ManuBindDDL(CmnContext context, DropDownList ddlManu)
    {
        //充付器厂商
        string StrSql = string.Format("select manucode,manucode||':'||manuname manuname from td_m_manu order by manucode");
        context.DBOpen("Select");
        DataTable ManuData = context.ExecuteReader(StrSql);
        context.DBCommit();
        ddlManu.DataTextField = "manuname";
        ddlManu.DataValueField = "manucode";
        ddlManu.DataSource = ManuData;
        ddlManu.DataBind();

        ddlManu.Items.Insert(0, new ListItem("---请选择---", ""));
    }

    /// <summary>
    /// 绑定应用平台类型
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ddlPlatform"></param>
    public static void PlatFormBindDDL(CmnContext context, DropDownList ddlPlatform)
    {
        //平台类型
        string PlatFormSql = string.Format("select platformcode,platformcode||':'||platformname platformname from tf_f_platformtype order by platformcode");
        context.DBOpen("Select");
        DataTable PlatFormData = context.ExecuteReader(PlatFormSql);
        context.DBCommit();
        ddlPlatform.DataTextField = "platformname";
        ddlPlatform.DataValueField = "platformcode";
        ddlPlatform.DataSource = PlatFormData;
        ddlPlatform.DataBind();

        ddlPlatform.Items.Insert(0, new ListItem("---请选择---", ""));
    }

    /// <summary>
    /// 验证日期输入格式
    /// </summary>
    /// <param name="context"></param>
    /// <param name="txtDate"></param>
    /// <param name="errMsg">错误提示信息</param>
    public static void CheckDate(CmnContext context, TextBox txtDate, string errMsg)
    {
        Validation valid = new Validation(context);

        if (!string.IsNullOrEmpty(txtDate.Text.Trim()))
        {
            valid.beDate(txtDate,errMsg);
        }
    }

    /// <summary>
    /// 充付器类型操作
    /// </summary>
    public enum ReaderTypeOperate
    {
        /// <summary>
        /// 新增充付器类型
        /// </summary>
        Add=1,

        /// <summary>
        /// 更新充付器类型
        /// </summary>
        Update=2,

        /// <summary>
        /// 删除充付器类型
        /// </summary>
        Delete=3
    }

    /// <summary>
    /// 充付器操作后状态
    /// </summary>
    public enum ReaderState
    {

        /// <summary>
        /// 售出(在线充付0是有效)
        /// </summary>
        Sale = 0,

        /// <summary>
        /// 出售返销
        /// </summary>
        SaleRollback=1,

        /// <summary>
        /// 更换回收
        /// </summary>
        ChangeRecovery=3,

        /// <summary>
        /// 退货回收
        /// </summary>
        ReturnRecovery=4
    }

    /// <summary>
    /// 返回对应接口的操作编码
    /// </summary>
    /// <param name="operate"></param>
    /// <returns></returns>
    public static string GetReaderTypeOperateCode(ReaderTypeOperate operate)
    {
        string Code="";
        switch (operate)
        {
            case ReaderTypeOperate.Add: Code = "00"; break;
            case ReaderTypeOperate.Update: Code = ""; break;
            case ReaderTypeOperate.Delete: Code = "01"; break;
        }
        return Code;
    }

    /// <summary>
    /// 服务器IP
    /// </summary>
    private static string ServerIP
    {
        get { return "192.168.101.102"; }
    }

    /// <summary>
    /// 服务器端口
    /// </summary>
    private static int ServerPort
    {
        get { return 8088; }
    }

    /// <summary>
    /// 调用接口返回信息
    /// </summary>
    /// <param name="code"></param>
    /// <returns></returns>
    private static string GetRetMsg(string recvmsg,ref bool issuc)
    {
        string Code = recvmsg.Substring(0, 4);
        string RetMsg = "";
        switch (Code)
        {
            case "0000": RetMsg = "0000:同步成功"; issuc = true; break;
            case "0001": RetMsg = "0001:数据异常"; issuc = false; break;
            case "0002": RetMsg = "0002:未找到数据"; issuc = false; break;
            case "0903": RetMsg = "0903:格式异常"; issuc = false; break;
            case "9999": RetMsg = "9999:其他错误"; issuc = false; break;
            default: RetMsg = "无对应返回编码"; issuc = false; break;
        }
        return RetMsg;
    }

    /// <summary>
    /// 调用接口，同步充付器相关数据
    /// </summary>
    /// <param name="content">报文内容</param>
    /// <param name="issuc">是否成功</param>
    public static string ReaderDataSYN(string content,ref bool issuc)
    {
        //string SendMsg = ToHexString(Encoding.ASCII.GetBytes(content));
        Socket SendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint EndPoint = new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort);
        SendSocket.Connect(EndPoint);
        SendSocket.Send(Encoding.UTF8.GetBytes(content));
        byte[] Recv = new byte[2048];
        int n = SendSocket.Receive(Recv);
        string RecvCode = Encoding.UTF8.GetString(Recv);
        SendSocket.Shutdown(SocketShutdown.Send);
        SendSocket.Close();

        return GetRetMsg(RecvCode,ref issuc);
    }

    /// <summary>
    /// 返回更新充付器状态报文
    /// </summary>
    /// <param name="readerno">充付器序列号</param>
    /// <param name="state">状态</param>
    /// <returns>报文字符串</returns>
    public static string GetUpdateDatagram(string readerno, ReaderState state)
    {
        StringBuilder Datagram = new StringBuilder();
        int Length = 103;
        string MethondName = "CARDREADERREFUND";
        string DeviceSerial = readerno;
        string State = Convert.ToInt32(state).ToString();
        Datagram.Append(new string('0',6-Length.ToString().Length));
        Datagram.Append(Length.ToString());
        Datagram.Append(new string(' ',32-Encoding.Default.GetByteCount(MethondName)));
        Datagram.Append(MethondName);
        Datagram.Append(new string(' ', 64 - Encoding.Default.GetByteCount(DeviceSerial)));
        Datagram.Append(DeviceSerial);
        Datagram.Append("1");

        return Datagram.ToString();
    }

    /// <summary>
    /// 返回出售报文
    /// </summary>
    /// <param name="context"></param>
    /// <param name="readerno"></param>
    /// <returns></returns>
    public static string GetSaleDatagram(CmnContext context,string readerno)
    {
        DataTable ReaderData = GetReaderData(context, readerno);
        StringBuilder Datagram = new StringBuilder();
        int Length = 191;
        string MethondName = "CARDREADERSELL";
        string TypeId = ReaderData.Rows[0]["typeid"].ToString();
        string DeviceSerial = ReaderData.Rows[0]["serialnumber"].ToString();
        string OperatorId = context.s_UserID;
        string SaleDate = Convert.ToDateTime(ReaderData.Rows[0]["saletime"]).ToString("yyyyMMddhhmmss");
        string BatchId = ReaderData.Rows[0]["batchno"].ToString();
        string ReaderKey = ReaderData.Rows[0]["key"].ToString();
        string Status = Convert.ToInt32(ReaderHelper.ReaderState.Sale).ToString();

        Datagram.Append(new string('0', 6 - Length.ToString().Length));
        Datagram.Append(Length.ToString());
        Datagram.Append(new string(' ', 32 - MethondName.Length));
        Datagram.Append(MethondName);
        Datagram.Append(new string('0', 4 - TypeId.Length));
        Datagram.Append(TypeId);
        Datagram.Append(new string(' ', 64 - DeviceSerial.Length));
        Datagram.Append(DeviceSerial);
        Datagram.Append(new string(' ', 6 - OperatorId.Length));
        Datagram.Append(OperatorId);
        Datagram.Append(SaleDate);
        Datagram.Append('0', 32 - BatchId.Length);
        Datagram.Append(BatchId);
        Datagram.Append(' ', 32 - ReaderKey.Length);
        Datagram.Append(ReaderKey);
        Datagram.Append(Status);

        return Datagram.ToString();
    }

    /// <summary>
    /// 根据序列号查出充付器信息
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ReaderNo">序列号</param>
    /// <returns></returns>
    private static DataTable GetReaderData(CmnContext context,string readerno)
    {
        DataTable ReaderData = new DataTable();
        string ReaderSql = string.Format("select * from tl_r_reader where serialnumber='{0}'", readerno);
        context.DBOpen("Select");
        ReaderData = context.ExecuteReader(ReaderSql);
        context.DBCommit();

        return ReaderData;
    }
}