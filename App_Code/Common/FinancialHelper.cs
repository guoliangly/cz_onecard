﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using PDO.Financial;
using Master;

/// <summary>
///FinancialHelper 的摘要说明
/// </summary>
public class FinancialHelper
{
	public FinancialHelper()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    //从消费结算单元-佣金方案对应关系子表(TF_TBALUNIT_COMSCHEMECHANGE)中读取佣金方案数据，放入下拉列表中

    public static void selectManuFacturer(CmnContext context, DropDownList ddl, bool empty)
    {
        if (empty)
            ddl.Items.Add(new ListItem("---请选择---", ""));

        select(context, ddl, "MANUFACTURER");
        if (!empty && ddl.Items.Count == 0)
        {
            context.AddError("S009478002: 初始化卡厂商列表失败");
        }
    }

    public static void select(CmnContext context, DropDownList ddl, string funcCode, params string[] vars)
    {
        DataTable dataTable = callQuery(context, funcCode, vars);

        if (dataTable.Rows.Count == 0)
        {
            return;
        }

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dataTable.Rows.Count; ++i)
        {
            itemArray = dataTable.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[1] + ":" + itemArray[0], (String)itemArray[1]);
            ddl.Items.Add(li);
        }
    }

    public static DataTable callQuery(CmnContext context, string funcCode, params string[] vars)
    {
        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = funcCode;
        int varNum = 0;
        foreach (string var in vars)
        {
            switch (++varNum)
            {
                case 1:
                    pdo.var1 = var;
                    break;
                case 2:
                    pdo.var2 = var;
                    break;
                case 3:
                    pdo.var3 = var;
                    break;
                case 4:
                    pdo.var4 = var;
                    break;
                case 5:
                    pdo.var5 = var;
                    break;
                case 6:
                    pdo.var6 = var;
                    break;
                case 7:
                    pdo.var7 = var;
                    break;
                case 8:
                    pdo.var8 = var;
                    break;
                case 9:
                    pdo.var9 = var;
                    break;
            }
        }

        StoreProScene storePro = new StoreProScene();

        return storePro.Execute(context, pdo);
    }
}
