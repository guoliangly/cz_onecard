﻿using System;
using System.Collections.Generic;
using System.Web;
using Controls.Customer.Asp;
using System.Data;
using System.Collections;
using Master;
using System.Web.UI.WebControls;

/// <summary>
///PBHelper 的摘要说明
/// </summary>
public class PBHelper
{
    public static void openFunc(CmnContext context, OpenFunc openFunc, String cardNo)
    {
        DataTable dt = SPHelper.callPBQuery(context, "QueryOpenFuncs", cardNo);
        ArrayList list = new ArrayList();
        
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; ++i )
            {
                list.Add("" + dt.Rows[i].ItemArray[0]);
            }
        }

        openFunc.List = list;
    }

    //根据卡号判断是否有专户账户未转
    public static void hasAccountByCardNo(CmnContext context, String cardNo)
    {
        DataTable dt = SPHelper.callPBQuery(context, "QueryAccountByCardNo", cardNo);
        if (dt != null && dt.Rows.Count > 0)
        {
            context.AddError("卡片存在账户宝，请先转账户");
        }

    }

    public static void AccHasMoney(CmnContext context, String cardNo)
    {
        DataTable dt = SPHelper.callPBQuery(context, "AccHasMoney", cardNo);
        if (dt != null && dt.Rows.Count > 0)
        {
            if (Convert.ToDouble(dt.Rows[0][0].ToString()) > 0)
            {
                context.AddError("卡片账户宝余额不为0，请先转账户");
            }
        }
    }
    //根据证件号码判断是否有专户账户未转
    public static void hasAccountByPaperNo(CmnContext context, String cardNo)
    {
        DataTable dt = SPHelper.callPBQuery(context, "QueryAccountByPaperNo", cardNo);
        if (dt != null && dt.Rows.Count > 0)
        {
            context.AddError("卡片存在账户宝，请先转账户");
        }

    }

    //根据证件号码判断是否有专户账户未转
    public static void hasAccountReturnCard(CmnContext context, String cardNo)
    {
        DataTable dt = SPHelper.callPBQuery(context, "QueryAccountReturnCard", cardNo);
        if (dt != null && dt.Rows.Count > 0)
        {
            context.AddMessage("卡片账户宝余额大于0，请先转账户");
        }

    }

    public static void queryCardNo(CmnContext context, TextBox txtCardno)
    {
        if (txtCardno.Text.Length != 16)
        {
            DataTable dt = SPHelper.callPBQuery(context, "QueryCardNo", txtCardno.Text);
            if (dt.Rows.Count != 1)
            {
                string errMsg = "根据后缀" + txtCardno.Text + "找不到唯一的补全卡号, 符合后缀的卡号有"
                    + dt.Rows.Count + "个, 分别为: " + dt.Rows[0].ItemArray[0];

                for (int i = 1; i < dt.Rows.Count && i < 3; ++i)
                {
                    errMsg += ", " + dt.Rows[i].ItemArray[0];
                }

                if (dt.Rows.Count > 3)
                {
                    errMsg += ", ...";
                }

                context.AddError(errMsg);
            }
            else
            {
                txtCardno.Text = "" + dt.Rows[0].ItemArray[0];
            }
        }
    }
}
