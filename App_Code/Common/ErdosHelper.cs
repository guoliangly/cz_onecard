﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
//using Bea.Tuxedo.ATMI;
using Common;

/// <summary>
/// ErdosHelper 的摘要说明
/// </summary>
public class ErdosHelper
{
    public static void checkIDCardNo(CmnContext context, TextBox txtIdcardno, bool allowEmpty)
    {
        Validation val = new Validation(context);
        bool b = Validation.isEmpty(txtIdcardno);
        if (!allowEmpty && b)
        {
            context.AddError("A300001003: 身份证号必须输入", txtIdcardno);
            return;
        }
        if (!b)
        {
            val.beIDCardNo(txtIdcardno);
        }
    }
    public static void checkName(CmnContext context, TextBox txtName, bool allowEmpty)
    {
        Validation val = new Validation(context);
        bool b = Validation.isEmpty(txtName);
        if (!allowEmpty && b)
        {
            context.AddError("A300001001: 姓名必须输入", txtName);
            return;
        }
        if (!b)
        {
            val.maxLength(txtName, 20, "A300001002: 姓名不能超过20位");
        }
    }


    public static int imageRadom = 232332;

    public static bool showPicture(CmnContext context, HttpRequest req, HttpResponse res)
    {
        String idCardNo = req.Params["IDCardNo"];
        if (idCardNo != null)
        {
            byte[] pic = ErdosHelper.ReadPicture(context, idCardNo);
            ErdosHelper.RespondPicture(pic, res);
            res.End();
            return true;
        }
        return false;
    }
    public static DataTable showNameCardInfo(CmnContext context, Image image0, string aspx,
        string idcardno, Panel div0, Label name0, Label sex0, Label idcardno0, Label addr0, Label size0)
    {
        image0.ImageUrl = aspx + "?Random=" + (new Random(imageRadom)).Next()
           + "&IDCardNo=" + idcardno;

        DataTable dt = SPHelper.callERQuery(context, "QryNameCardInfo", idcardno);
        if (dt != null && dt.Rows.Count > 0)
        {
            div0.Visible = true;
            object[] row = dt.Rows[0].ItemArray;
            name0.Text = "" + row[0];
            sex0.Text = "" + row[1];
            idcardno0.Text = "" + row[2];
            addr0.Text = "" + row[3];
            size0.Text = "" + row[4];
        }
        else
        {
            div0.Visible = false;
        }

        return dt;
    }

    public static string getFeeInfo(DataTable dt, CmnContext context, Label labFee)
    {
        if (dt != null && dt.Rows.Count > 0)
        {
            object[] row = dt.Rows[0].ItemArray;
            string dealStateCode = "" + row[5];
            labFee.Text = "0";

            return dealStateCode;
        }

        return "";
    }

    public static void RespondPicture(byte[] pic, HttpResponse res)
    {
        if (pic != null)
        {
            res.ContentType = "application/octet-stream";
            res.AddHeader("Content-Disposition", "attachment;FileName= picture.JPG");
            res.BinaryWrite(pic);

        }
    }

    public static byte[] ReadPicture(CmnContext context, string idCardNo)
    {
        string selectSql = "select PICTURE from TF_RESIDENTCARD where IDCARDNO=:IDCARDNO";
        context.DBOpen("Select");
        context.AddField(":IDCARDNO").Value = idCardNo;
        DataTable dt = context.ExecuteReader(selectSql);
        context.DBCommit();
        if (dt != null && dt.Rows.Count > 0)
        {
            return (byte[])dt.Rows[0].ItemArray[0];
        }

        return null;
    }


    public static byte[] ReadTmpPicture(CmnContext context, string idCardNo, string sessid)
    {
        string selectSql = @"select PICTURE from TF_B_PICTURE where SESSID=:SESSID
                           and  OLDIDCARDNO=:OLDIDCARDNO";
        context.DBOpen("Select");
        context.AddField(":SESSID").Value = sessid;
        context.AddField(":OLDIDCARDNO").Value = idCardNo;
        DataTable dt = context.ExecuteReader(selectSql);
        context.DBCommit();
        if (dt != null && dt.Rows.Count > 0)
        {
            return (byte[])dt.Rows[0].ItemArray[0];
        }

        return null;
    }

    
    private static string wsnaddr = null;
    private static string tuxservname = null;
    private static CmnContext socContext = new CmnContext("xx");

    // 同步处理调用
    public static void Sync(CmnContext context, string tradeid)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = tradeid;
        DataTable dt = context.ExecuteReader(@"
            SELECT TRADEID, TRADETYPECODE, NAME,
                   TJTCARDNO, IDCARDNO, BANKCARDNO, SOCLSECNO,
                   OLDTJTCARDNO, OLDBANKCARDNO, OLDSOCLSECNO,
                   SSSYNCCODE, BKSYNCCODE, OPERATETIME
            FROM   TF_B_SYNC
            WHERE  TRADEID = :TRADEID
        ");
        context.DBCommit();

        Dispatch(context, dt);
    }

    public static void Sync(CmnContext context, string tradeid, string idcardno)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        DataTable dt = context.ExecuteReader(@"
            SELECT TRADEID, TRADETYPECODE, NAME,
                   TJTCARDNO, IDCARDNO, BANKCARDNO, SOCLSECNO,
                   OLDTJTCARDNO, OLDBANKCARDNO, OLDSOCLSECNO,
                   SSSYNCCODE, BKSYNCCODE, OPERATETIME
            FROM   TF_B_SYNC
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
        ");
        context.DBCommit();

        Dispatch(context, dt);
    }

    private static void Dispatch(CmnContext context, DataTable dt)
    {
        for (int rowind = 0; rowind < dt.Rows.Count; ++rowind)
        {
            DataRow currRow = dt.Rows[rowind];
            if (currRow["SSSYNCCODE"].Equals("0"))
            {
                try
                {
                    DispatchSSSync(context, currRow);
                    UpdateSSSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "1", "");
                }
                catch (Exception e)
                {
                    context.AddError("同步社保失败: " + e.Message);
                    UpdateSSSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "2", e.Message);
                }
            }
            if (currRow["BKSYNCCODE"].Equals("0"))
            {
                try
                {
                    DispatchBKSync(currRow);
                    UpdateBKSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "1", "");
                }
                catch (Exception e)
                {
                    context.AddError("同步银行失败: " + e.Message);
                    UpdateBKSyncCode(context, "" + currRow["TRADEID"], "" + currRow["IDCARDNO"], "2", e.Message);
                }
            }
        }
    }



    private static void DispatchSSSync(CmnContext context, DataRow currRow)
    {
        string tradeTypeCode = "" + currRow["TRADETYPECODE"];
        if (tradeTypeCode.Equals("02")) // 我的任务
        {
            SSSyncMyTask(context, currRow);
        }
        else if (tradeTypeCode.Equals("13")) // 拒制
        {
            SSSyncReject(context, currRow);
        }
        else if (tradeTypeCode.Equals("03")) // 换卡登记
        {
            SSSyncChangeRegister(context, currRow);
        }
        else if (tradeTypeCode.Equals("05")) // 05 新卡领卡
        {
            SSSyncPickup(context, currRow, null);
        }
        else if (tradeTypeCode.Equals("15") ) // 015换卡领
        {
            SSSyncPickup(context, currRow, "03");
        }
        else if (tradeTypeCode.Equals("16")) // 16补卡领卡
        {
            SSSyncPickup(context, currRow, "04");
        }
        else if (tradeTypeCode.Equals("06")) // 补卡登记
        {
            SSSyncReissue(context, currRow);
        }
        else if (tradeTypeCode.Equals("08")) // 挂失
        {
            SSSyncReportLoss(context, currRow);
        }
        else if (tradeTypeCode.Equals("09")) // 解挂
        {
            SSSyncReportLoss(context, currRow);
        }
        else
        {
            throw new Exception("不可识别的业务类型编码" + tradeTypeCode);
        }
    }

    private static void UpdateSSSyncCode(CmnContext context, string tradeid, string idcardno, string syncCode, string errorInfo)
    {
        context.DBOpen("Update");
        context.AddField(":BKSYNCCODE").Value = syncCode;
        context.AddField(":BKSYNCERRINFO").Value = errorInfo;
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        context.ExecuteNonQuery(@"
            UPDATE TF_B_SYNC
            SET    SSSYNCCODE = :SSSYNCCODE,
                   SSSYNCERRINFO = :SSSYNCERRINFO,
                   SSSYNCTIME = SYSDATE
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
            ");
        context.DBCommit();
    }

    private static void UpdateBKSyncCode(CmnContext context, string tradeid, string idcardno, string syncCode, string errorInfo)
    {
        context.DBOpen("Update");
        context.AddField(":SSSYNCCODE").Value = syncCode;
        context.AddField(":SSSYNCERRINFO").Value = errorInfo;
        context.AddField(":TRADEID").Value = tradeid;
        context.AddField(":IDCARDNO").Value = idcardno;
        context.ExecuteNonQuery(@"
            UPDATE TF_B_SYNC
            SET    BKSYNCCODE = :BKSYNCCODE,
                   BKSYNCERRINFO = :BKSYNCERRINFO,
                   BKSYNCTIME = SYSDATE                
            WHERE  TRADEID = :TRADEID
            AND    IDCARDNO = :IDCARDNO
            ");
        context.DBCommit();
    }

    private static void DispatchBKSync(DataRow currRow)
    {
    }

    // 挂失解挂
    private static void SSSyncReportLoss(CmnContext context, DataRow currRow)
    {
        socContext.DBOpen("Insert");
        socContext.AddField(":TRADETYPECODE").Value = currRow["TRADETYPECODE"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":NAME").Value = currRow["NAME"];
        socContext.AddField(":SOCLSECNO").Value = currRow["SOCLSECNO"];
        socContext.AddField(":OPERATETIME", "DateTime").Value = currRow["OPERATETIME"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_LOSTSYNC (
                MSGID, MSGTIME, MSGSTATUS, TRADETYPECODE, IDCARDNO,
                NAME, SOCLSECNO, OPERATETIME, RSRV1, RSRV2, RSRV3)
            VALUES (
                TI_TJT_LOSTSYNC_SEQ.NEXTVAL, SYSDATE, '0', :TRADETYPECODE, :IDCARDNO,
                :NAME, :SOCLSECNO, :OPERATETIME, NULL, NULL, NULL)
        ");

        socContext.DBCommit();

    }

    // 补卡
    private static void SSSyncReissue(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, "04");
    }

    // 领卡
    private static void SSSyncPickup(CmnContext context, DataRow currRow, string tradeTypeCode)
    {
        if (tradeTypeCode == null)
        {
            context.DBOpen("Select");
            context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
            DataTable dt = context.ExecuteReader(@"
                SELECT MANUTYPE
                FROM   TF_RESIDENTCARD
                WHERE IDCARDNO = :IDCARDNO
            ");
            context.DBCommit();

            if (dt.Rows.Count == 0)
            {
                return;
            }

            DataRow dr = dt.Rows[0];
            tradeTypeCode = "" + dr["MANUTYPE"];
        }


        socContext.DBOpen("Insert");
        socContext.AddField(":MANUTYPE").Value = tradeTypeCode;
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":NAME").Value = currRow["NAME"];
        socContext.AddField(":SOCLSECNO").Value = currRow["SOCLSECNO"];
        socContext.AddField(":OPERATETIME", "DateTime").Value = currRow["OPERATETIME"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_RECVSYNC (
                MSGID,MSGTIME, MSGSTATUS, MANUTYPE, 
                IDCARDNO, NAME, SOCLSECNO, OPERATETIME,
                RSRV1, RSRV2, RSRV3)
            VALUES  (
                TI_TJT_RECVSYNC_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :IDCARDNO, :NAME, :SOCLSECNO, :OPERATETIME,
                NULL, NULL, NULL )        
        ");

        socContext.DBCommit();

    }

    // 换卡登记
    private static void SSSyncChangeRegister(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, "03");
    }


    // 拒制
    private static void SSSyncReject(CmnContext context, DataRow currRow)
    {
        context.DBOpen("Select");
        context.AddField(":TRADEID").Value = currRow["TRADEID"];
        context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        DataTable dt = context.ExecuteReader(@"
            SELECT  t.APPTYPE, t.CORPNO, t.NAME, t.SEX,
                t.NATION, t.BIRTHDATE, t.RESIDENCETYPE, t.RESIDENCEADDRESS,
                t.TELEPHONE, t.HOMEADDRESS, t.POSTALCODE, t.INSURANCETYPE,
                t.INSURANCEDEPART, t.COLONYCODE, t.RSRV1, t.RSRV2,
                t.RSRV3, t.REJECTREASONCODE, t.REJECTREASONDESC, t.MANUTYPE,
                c.CORPNAME, c.CORPPHONE, c.CORPADDRESS, c.CORPPOSTALCODE
            FROM TF_B_RESIDENTREJECT  t, TD_RESIDENTCORP c
            WHERE t.TRADEID = :TRADEID
            AND   t.IDCARDNO = :IDCARDNO 
            AND   t.CORPNO = c.CORPNO(+)
        ");
        context.DBCommit();

        if (dt.Rows.Count == 0)
        {
            return;
        }

        DataRow dr = dt.Rows[0];

        socContext.DBOpen("Insert");

        socContext.AddField(":MANUTYPE").Value = dr["MANUTYPE"];
        socContext.AddField(":APPTYPE").Value = dr["APPTYPE"];
        socContext.AddField(":CORPNO").Value = dr["CORPNO"];
        socContext.AddField(":CORPNAME").Value = dr["CORPNAME"];
        socContext.AddField(":CORPPHONE").Value = dr["CORPPHONE"];
        socContext.AddField(":CORPADDRESS").Value = dr["CORPADDRESS"];
        socContext.AddField(":CORPPOSTALCODE").Value = dr["CORPPOSTALCODE"];
        socContext.AddField(":NAME").Value = dr["NAME"];
        socContext.AddField(":SEX").Value = dr["SEX"];
        socContext.AddField(":NATION").Value = dr["NATION"];
        socContext.AddField(":BIRTHDATE", "DateTime").Value = dr["BIRTHDATE"];
        socContext.AddField(":RESIDENCETYPE").Value = dr["RESIDENCETYPE"];
        socContext.AddField(":RESIDENCEADDRESS").Value = dr["RESIDENCEADDRESS"];
        socContext.AddField(":TELEPHONE").Value = dr["TELEPHONE"];
        socContext.AddField(":HOMEADDRESS").Value = dr["HOMEADDRESS"];
        socContext.AddField(":POSTALCODE").Value = dr["POSTALCODE"];
        socContext.AddField(":INSURANCETYPE").Value = dr["INSURANCETYPE"];
        socContext.AddField(":INSURANCEDEPART").Value = dr["INSURANCEDEPART"];
        socContext.AddField(":COLONYCODE").Value = dr["COLONYCODE"];
        socContext.AddField(":RSRV1").Value = dr["RSRV1"];
        socContext.AddField(":RSRV2").Value = dr["RSRV2"];
        socContext.AddField(":RSRV3").Value = dr["RSRV3"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        socContext.AddField(":REJECTREASONCODE").Value = dr["REJECTREASONCODE"];
        socContext.AddField(":REJECTREASONDESC").Value = dr["REJECTREASONDESC"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_RESIDENTREJECT ( 
                MSGID, MSGTIME, MSGSTATUS, MANUTYPE,
                APPTYPE, CORPNO, CORPNAME, CORPPHONE,
                CORPADDRESS, CORPPOSTALCODE, NAME, SEX,
                NATION, BIRTHDATE, RESIDENCETYPE, RESIDENCEADDRESS,
                TELEPHONE, HOMEADDRESS, POSTALCODE, INSURANCETYPE,
                INSURANCEDEPART, COLONYCODE, RSRV1, RSRV2,
                RSRV3, IDCARDNO, REJECTREASONCODE, REJECTREASONDESC)
            VALUES ( 
                TI_TJT_CARDAPP_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :APPTYPE, :CORPNO, :CORPNAME, :CORPPHONE,
                :CORPADDRESS, :CORPPOSTALCODE, :NAME, decode(:SEX, '0', '1', '1', '2'),
                :NATION, :BIRTHDATE, :RESIDENCETYPE, :RESIDENCEADDRESS,
                :TELEPHONE, :HOMEADDRESS, :POSTALCODE, :INSURANCETYPE,
                :INSURANCEDEPART, :COLONYCODE, :RSRV1, :RSRV2,
                :RSRV3, :IDCARDNO, :REJECTREASONCODE, :REJECTREASONDESC
            )
        ");

        socContext.DBCommit();

    }

    // 我的任务社保同步
    private static void SSSyncMyTask(CmnContext context, DataRow currRow)
    {
        SSSyncCardApp(context, currRow, null);
    }

    private static void SSSyncCardApp(CmnContext context, DataRow currRow, string manuType)
    {
        context.DBOpen("Select");
        context.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];
        DataTable dt = context.ExecuteReader(@"
            SELECT t.APPTYPE, t.CORPNO, t.NAME, t.SEX,
                   t.NATION, t.BIRTHDATE, t.RESIDENCETYPE, t.RESIDENCEADDRESS,
                   t.TELEPHONE, t.HOMEADDRESS, t.POSTALCODE, t.INSURANCETYPE,
                   t.INSURANCEDEPART, t.COLONYCODE, t.PICTURE, t.RSRV1,
                   t.RSRV2, t.RSRV3, t.MANUTYPE,
                   c.CORPNAME, c.CORPPHONE, c.CORPADDRESS, c.CORPPOSTALCODE
            FROM   TF_RESIDENTCARD t, TD_RESIDENTCORP c
            WHERE  t.IDCARDNO = :IDCARDNO 
            AND    t.CORPNO = c.CORPNO(+)
        ");
        context.DBCommit();

        if (dt.Rows.Count == 0)
        {
            return;
        }

        DataRow dr = dt.Rows[0];

        socContext.DBOpen("Insert");

        socContext.AddField(":MANUTYPE").Value = (manuType == null ? dr["MANUTYPE"] : manuType);
        socContext.AddField(":APPTYPE").Value = dr["APPTYPE"];
        socContext.AddField(":CORPNO").Value = dr["CORPNO"];
        socContext.AddField(":CORPNAME").Value = dr["CORPNAME"];
        socContext.AddField(":CORPPHONE").Value = dr["CORPPHONE"];
        socContext.AddField(":CORPADDRESS").Value = dr["CORPADDRESS"];
        socContext.AddField(":CORPPOSTALCODE").Value = dr["CORPPOSTALCODE"];
        socContext.AddField(":NAME").Value = dr["NAME"];
        socContext.AddField(":SEX").Value = dr["SEX"];
        socContext.AddField(":NATION").Value = dr["NATION"];
        socContext.AddField(":BIRTHDATE", "DateTime").Value = dr["BIRTHDATE"];
        socContext.AddField(":RESIDENCETYPE").Value = dr["RESIDENCETYPE"];
        socContext.AddField(":RESIDENCEADDRESS").Value = dr["RESIDENCEADDRESS"];
        socContext.AddField(":TELEPHONE").Value = dr["TELEPHONE"];
        socContext.AddField(":HOMEADDRESS").Value = dr["HOMEADDRESS"];
        socContext.AddField(":POSTALCODE").Value = dr["POSTALCODE"];
        socContext.AddField(":INSURANCETYPE").Value = dr["INSURANCETYPE"];
        socContext.AddField(":INSURANCEDEPART").Value = dr["INSURANCEDEPART"];
        socContext.AddField(":COLONYCODE").Value = dr["COLONYCODE"];
        socContext.AddField(":PICTURE", "Blob").Value = dr["PICTURE"];
        socContext.AddField(":RSRV1").Value = dr["RSRV1"];
        socContext.AddField(":RSRV2").Value = dr["RSRV2"];
        socContext.AddField(":RSRV3").Value = dr["RSRV3"];
        socContext.AddField(":IDCARDNO").Value = currRow["IDCARDNO"];

        socContext.ExecuteNonQuery(@"
            INSERT INTO TI_TJT_CARDAPP ( 
                MSGID, MSGTIME, MSGSTATUS, MANUTYPE,
                APPTYPE, CORPNO, CORPNAME, CORPPHONE,
                CORPADDRESS, CORPPOSTALCODE, NAME, SEX,
                NATION, BIRTHDATE, RESIDENCETYPE, RESIDENCEADDRESS,
                TELEPHONE, HOMEADDRESS, POSTALCODE, INSURANCETYPE,
                INSURANCEDEPART, COLONYCODE, PICTURE, RSRV1,
                RSRV2, RSRV3, IDCARDNO )
            VALUES ( 
                TI_TJT_CARDAPP_SEQ.NEXTVAL, SYSDATE, '0', :MANUTYPE,
                :APPTYPE, :CORPNO, :CORPNAME, :CORPPHONE,
                :CORPADDRESS, :CORPPOSTALCODE, :NAME, decode(:SEX, '0', '1', '1', '2'),
                :NATION, :BIRTHDATE, :RESIDENCETYPE, :RESIDENCEADDRESS,
                :TELEPHONE, :HOMEADDRESS, :POSTALCODE, :INSURANCETYPE,
                :INSURANCEDEPART, :COLONYCODE, :PICTURE, :RSRV1,
                :RSRV2, :RSRV3, :IDCARDNO) 
        ");

        socContext.DBCommit();
    }


    /*
    public static void SyncTuxedo(CmnContext context)
    {
        if (wsnaddr == null)
        {
            context.DBOpen("Select");
            DataTable dt = context.ExecuteReader("select CODEVALUE from TC_CODING where CODETYPE = 'CAT_SYNC_WSNADDR'");
            if (dt == null || dt.Rows.Count == 0)
            {
                if(showError)context.AddError("同步错误：没有在TC_CODING中配置类别CAT_SYNC_WSNADDR的CODEVALUE取值");
                return false;
            }

            wsnaddr = "" + dt.Rows[0].ItemArray[0];

            dt = context.ExecuteReader("select CODEVALUE from TC_CODING where CODETYPE = 'CAT_SYNC_SERVNAME'");
            if (dt == null || dt.Rows.Count == 0)
            {
                if (showError) context.AddError("同步错误：没有在TC_CODING中配置类别CAT_SYNC_SERVNAME的CODEVALUE取值");
                return false;
            }

            tuxservname = "" + dt.Rows[0].ItemArray[0]; ;
        }

        //设定环境变量
        //Utils.tuxputenv("WSNADDR=//127.0.0.1:5001");
        //Utils.tuxputenv("WSINTOPPRE71=yes");

        Bea.Tuxedo.ATMI.Utils.tuxputenv(wsnaddr);

        try
        {
            //初始化应用上下文
            Bea.Tuxedo.ATMI.TypedTPINIT tpinfo = new Bea.Tuxedo.ATMI.TypedTPINIT();
            tpinfo.flags = Bea.Tuxedo.ATMI.TypedTPINIT.TPMULTICONTEXTS;
            Bea.Tuxedo.ATMI.AppContext ac = Bea.Tuxedo.ATMI.AppContext.tpinit(tpinfo);

            //AppContext ac = AppContext.tpinit(null);

            //同步调用服务。
            // 同步调用时，服务器不返回结果或是出错之前，
            // tpcall方法不会返回，程序将等在这里。
            TypedString sndstr = new TypedString(1000);
            //sndstr.PutString(inputstring);
            //TypedBuffer rcvstr = new TypedString(1000);
            //ac.tpcall("TOUPPER", sndstr, ref rcvstr, 0);

            ac.tpacall(tuxservname, sndstr, 0);

            // 得到结果
            //string rcvstr_str = (rcvstr as TypedString).GetString(0, 1000);
            //关闭应用上下文
            ac.tpterm();
        }
        catch (Exception ex)
        {
            context.AddError("同步错误：" + ex.Message);
        }

    }
     */
}
