﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;


    public class RSAHelper
    {
        private string privateKey = "";
        private string publicKey = "";
        //公钥加密
        /// <summary>
        /// 公钥加密
        /// </summary>
        /// <param name="str">明文</param>
        /// <param name="PublicPath">公钥路径</param>
        /// <returns></returns>
        public string RSAEncrypt(string str, string PublicPath)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            if (publicKey == "")
            {
                StreamReader reader = new StreamReader(PublicPath);
                publicKey = reader.ReadToEnd();
                reader.Close();
            }
            rsa.FromXmlString(publicKey);

            UnicodeEncoding ByteConverter = new UnicodeEncoding();

            byte[] dataToEncrypt = ByteConverter.GetBytes(str);
            return Convert.ToBase64String(rsa.Encrypt(dataToEncrypt, false));

            //byte[] dataToEncrypt=Encoding.UTF8.GetBytes(str);
            //return Encoding.UTF8.GetString(rsa.Encrypt(dataToEncrypt, false));
        }
        //私钥解密
        /// <summary>
        /// 私钥解密
        /// </summary>
        /// <param name="str">加密后字符串</param>
        /// <param name="PrivatePath">私钥路径</param>
        /// <returns></returns>
        public string RSADecrypt(string str, string PrivatePath)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            if (privateKey == "")
            {
                StreamReader reader = new StreamReader(PrivatePath);
                privateKey = reader.ReadToEnd();
                reader.Close();
            }


            rsa.FromXmlString(privateKey);
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = Convert.FromBase64String(str);
            return ByteConverter.GetString(rsa.Decrypt(dataToEncrypt, false));
        }
        /// <summary>
        /// 私钥签名
        /// </summary>
        /// <param name="content">需要签名的内容(加密密码)</param>
        /// <param name="privateKey">私钥路径</param>
        /// <returns></returns>
        public string sign(string content, string PrivatePath)
        {
            byte[] Data = Encoding.UTF8.GetBytes(content);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            if (privateKey == "")
            {
                StreamReader reader = new StreamReader(PrivatePath);
                privateKey = reader.ReadToEnd();
                reader.Close();
            }
            rsa.FromXmlString(privateKey);
            SHA1 sh = new SHA1CryptoServiceProvider();
            byte[] signData = rsa.SignData(Data, sh);
            return Convert.ToBase64String(signData);
        }
        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="content">需要验证的内容(加密密码)</param>
        /// <param name="signedString">签名结果(签名后的字符串)</param>
        /// <param name="publicKey">公钥路径</param>
        /// <returns></returns>
        public bool SignVerify(string content, string signedString, string PublicPath)
        {
            bool result = false;
            byte[] Data = Encoding.UTF8.GetBytes(content);
            byte[] data = Convert.FromBase64String(signedString);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            if (publicKey == "")
            {
                StreamReader reader = new StreamReader(PublicPath);
                publicKey = reader.ReadToEnd();
                reader.Close();
            }
            rsa.FromXmlString(publicKey);
            SHA1 sh = new SHA1CryptoServiceProvider();
            result = rsa.VerifyData(Data, sh, data);
            return result;
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public string MD5(string str)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, "MD5").ToLower();
        }
        public String ChargeDecrypt(String strSource)
        {
            return EncodeString(strSource, "szcic");
        }

        private String EncodeString(String strSource, String strKey)
        {
            if (strKey.Trim().Equals(""))
                return strSource;

            int iKeyLen = strKey.Length;
            int iSourceLen = strSource.Length;
            int i, add = 0, add_tmp, source_tmp;
            byte bTmp = 0;
            String tmpStr = "";
            char[] arrTmp = null;
            String rtnStr = "";
            int isub = 0;
            char chTmp;

            for (int index = 0; index < iSourceLen; index++)
            {
                if (index < iKeyLen)
                {
                    i = index;
                }
                else
                {
                    i = (index + 1) / iKeyLen;
                    i = (index + 1) - i * iKeyLen;

                    if (i != 0)
                    {
                        i = index / iKeyLen;
                        i = index - i * iKeyLen;

                    }
                }

                tmpStr = strKey.Substring(i, 1);
                arrTmp = tmpStr.ToCharArray();
                bTmp = Convert.ToByte(arrTmp[0]);
                add = Convert.ToInt16(bTmp);
                add_tmp = add / 10;
                add = add - add_tmp * 10;

                tmpStr = strSource.Substring(index, 1);
                arrTmp = tmpStr.ToCharArray();
                bTmp = Convert.ToByte(arrTmp[0]);
                source_tmp = Convert.ToInt16(bTmp);

                isub = source_tmp - add;

                if ((isub) < 0)
                    rtnStr = rtnStr + source_tmp.ToString() + tmpStr;
                else
                {
                    bTmp = Convert.ToByte(isub);
                    chTmp = Convert.ToChar(bTmp);
                    rtnStr = rtnStr + "0" + chTmp;
                }

            }


            return rtnStr;
        }

    }

