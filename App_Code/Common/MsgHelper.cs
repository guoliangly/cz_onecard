﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

/**************************************
 * 2014-08-25  chenwentao
 * 发送短信相关方法
**************************************/

/// <summary>
///发送短信帮助类
/// </summary>
public class MsgHelper
{
	public MsgHelper()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    /// <summary>
    /// 生成验证码
    /// </summary>
    /// <param name="NumCount">验证码位数</param>
    /// <returns>NumCount位随机数</returns>
    public static string GetRandomNum(int NumCount)
    {
        string allChar = "0,1,2,3,4,5,6,7,8,9";
        string[] allCharArray = allChar.Split(',');//拆分成数组
        string randomNum = "";
        int temp = -1;                             //记录上次随机数的数值，尽量避免产生几个相同的随机数
        Random rand = new Random();
        for (int i = 0; i < NumCount; i++)
        {
            if (temp != -1)
            {
                rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
            }
            int t = rand.Next(9);
            if (temp == t)
            {
                return GetRandomNum(NumCount);
            }
            temp = t;
            randomNum += allCharArray[t];
        }
        return randomNum;
    }

    /// <summary>
    /// 需要发送验证码的短信内容
    /// </summary>
    /// <param name="captcha">验证码</param>
    /// <param name="tradetype">业务类型:00:开通,01:关闭,02:变更手机号</param>
    /// <returns>向用户发送短信验证码的短信内容</returns>
    public static string GetCaptchaMsg(string captcha,string tradetype)
    {
        return "您【" + GetTradeType(tradetype) + "】的验证码为:" + captcha + "(5分钟内有效)请勿提供给他人.客服电话0519-85175888.";
    }

    /// <summary>
    /// 获取业务类型
    /// </summary>
    /// <param name="tradetype">业务类型编码</param>
    /// <returns>业务类型名称</returns>
    private static string GetTradeType(string tradetype)
    {
        string str = string.Empty;
        switch (tradetype)
        {
            case "00": str = "开通在线支付"; break;
            case "01": str = "关闭在线支付"; break;
            case "02": str = "修改手机号码"; break;
            default: break;
        }
        return str;
    }

    /// <summary>
    /// 办理业务成功短信
    /// </summary>
    /// <param name="cardno">卡号</param>
    /// <param name="tradetype">业务类型:00:开通,01:关闭,01:变更手机号</param>
    /// <returns>办理业务成功时向用户发送的短信内容</returns>
    public static string GetScuMsg(string cardno,string tradetype)
    {
        DateTime dt=DateTime.Now;
        string strDate = dt.Year + "年" + dt.Month + "月" + dt.Day + "日" + dt.Hour + ":" + dt.Minute;
        //string strDate = dt.ToShortDateString()+" "+ dt.ToShortTimeString();
        string treadeName = GetTradeType(tradetype);

        return "您尾号" + cardno.Substring(cardno.Length - 4, 4) + "卡于" + strDate + "已成功办理【" + treadeName + "】业务,客服电话0519-85175888.";
    }

    public static string GetBatchOpenMsg(string cardno)
    {
        DateTime dt = DateTime.Now;
        string strDate = dt.Year + "年" + dt.Month + "月" + dt.Day + "日";
        return "您尾号" + cardno.Substring(cardno.Length - 4, 4) + "卡" + strDate + "成功开通【在线支付业务】,请及时更改您的账户初始密码,客服电话0519-85175888. ";
    }

    /// <summary>
    /// 发送短信
    /// </summary>
    /// <param name="phoneno">接收短信手机号</param>
    /// <param name="msg">短信内容</param>
    /// <returns>发送短信接口返回的编码</returns>
    public static string SendMsg(string phoneno, string msg)
    {
        StringBuilder Msg = new StringBuilder();
        char[] MsgHead = new char[32];
        MsgHead = ("SMSSEND" + new string(' ', 25)).ToCharArray();
        if (msg.Length<256)
        {
            msg = msg+new string(' ', (256 - msg.Length));
        }
        Msg.Append(MsgHead);
        Msg.Append(phoneno);
        Msg.Append(msg);

        byte[] EncMsg = AESEncrypt(Msg.ToString(),Key);
        string SendMsg = ToHexString(EncMsg);

        Socket SendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint EndPoint = new IPEndPoint(IPAddress.Parse("192.168.101.102"), 8090);
        SendSocket.Connect(EndPoint);
        int ret = SendSocket.Send(Encoding.UTF8.GetBytes(SendMsg));
        byte[] Recv = new byte[2048];
        int n = SendSocket.Receive(Recv);
        string RecvStr = Encoding.UTF8.GetString(Recv);
        SendSocket.Shutdown(SocketShutdown.Send);
        SendSocket.Close();

        return RecvStr;
    }

    /// <summary>
    /// 将16进制BYTE数组转换成16进制字符串
    /// </summary>
    /// <param name="bytes">字节数组</param>
    /// <returns>转换后的字符串数组</returns>
    private static string ToHexString(byte[] bytes) // 0xae00cf => "AE00CF "
    {
        string hexString = string.Empty;
        if (bytes != null)
        {
            StringBuilder strB = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                strB.Append(bytes[i].ToString("X2"));
            }
            hexString = strB.ToString();
        }
        return hexString;
    }

    /// <summary>
    /// AES加密密钥
    /// </summary>
    private static string Key
    {
        get { return "EGEKROZMXWUAANRAYBIRTDILSAGXOEHM"; }
    }

    /// <summary>  
    /// AES加密算法
    /// </summary>  
    /// <param name="plainText">明文字符串</param>
    /// <param name="strKey">密钥</param>
    /// <returns>返回加密后的密文字节数组</returns>
    public static byte[] AESEncrypt(string plainText, string strKey)
    {
        if (string.IsNullOrEmpty(plainText))
            return null;
        Byte[] data = Encoding.UTF8.GetBytes(plainText);
        RijndaelManaged rDel = new RijndaelManaged();
        rDel.Key = Encoding.ASCII.GetBytes(strKey);
        rDel.Mode = System.Security.Cryptography.CipherMode.ECB;
        rDel.Padding = System.Security.Cryptography.PaddingMode.None;
        ICryptoTransform cTransform = rDel.CreateEncryptor();

        // 加密前的长度
        int targetLen = data.Length;
        if (targetLen % 16 != 0)
        {
            targetLen = targetLen / 16 * 16 + 16;
        }

        // 构造加密数据
        byte[] dataAll = data;
        if (data.Length != targetLen)
        {
            dataAll = new byte[targetLen];
            System.Array.Copy(data, 0, dataAll, 0, data.Length);
        }

        // 加密处理
        byte[] ret = new byte[targetLen];
        for (int i = 0; i < dataAll.Length; i += 16)
        {
            int l = (dataAll.Length - i > 16 ? 16 : dataAll.Length - i);
            Byte[] block = new byte[l];
            cTransform.TransformBlock(dataAll, i, l, block, 0);
            System.Array.Copy(block, 0, ret, i, block.Length);
        }
        return ret;
    }
}