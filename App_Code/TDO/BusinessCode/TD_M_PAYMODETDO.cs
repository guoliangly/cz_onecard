using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace TDO.BusinessCode
{
     // 支付方式编码表
     public class TD_M_PAYMODETDO : DDOBase
     {
          public TD_M_PAYMODETDO()
          {
          }

          protected override void Init()
          {
               tableName = "TD_M_PAYMODE";

               columns = new String[6][];
               columns[0] = new String[]{"MODECODE", "string"};
               columns[1] = new String[]{"MODENAME", "String"};
               columns[2] = new String[]{"ISUSETAG", "string"};
               columns[3] = new String[]{"UPDATEDEPARTNO", "string"};
               columns[4] = new String[]{"UPDATESTAFFNO", "string"};
               columns[5] = new String[]{"UPDATETIME", "DateTime"};

               columnKeys = new String[]{
                   "MODECODE",
               };


               array = new String[6];
               hash.Add("MODECODE", 0);
               hash.Add("MODENAME", 1);
               hash.Add("ISUSETAG", 2);
               hash.Add("UPDATEDEPARTNO", 3);
               hash.Add("UPDATESTAFFNO", 4);
               hash.Add("UPDATETIME", 5);
          }

          // 支付方式编码
          public string MODECODE
          {
              get { return  Getstring("MODECODE"); }
              set { Setstring("MODECODE",value); }
          }

          // 支付方式名称
          public String MODENAME
          {
              get { return  GetString("MODENAME"); }
              set { SetString("MODENAME",value); }
          }

          // 有效标识
          public string ISUSETAG
          {
              get { return  Getstring("ISUSETAG"); }
              set { Setstring("ISUSETAG",value); }
          }

          // 更新部门
          public string UPDATEDEPARTNO
          {
              get { return  Getstring("UPDATEDEPARTNO"); }
              set { Setstring("UPDATEDEPARTNO",value); }
          }

          // 更新员工
          public string UPDATESTAFFNO
          {
              get { return  Getstring("UPDATESTAFFNO"); }
              set { Setstring("UPDATESTAFFNO",value); }
          }

          // 更新时间
          public DateTime UPDATETIME
          {
              get { return  GetDateTime("UPDATETIME"); }
              set { SetDateTime("UPDATETIME",value); }
          }

     }
}


