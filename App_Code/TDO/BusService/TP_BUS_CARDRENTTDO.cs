﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace TDO.CardManager
{
    // 常州公交卡租金表
    public class TP_BUS_CARDRENTTDO : DDOBase
    {
        public TP_BUS_CARDRENTTDO()
        {
        }

        protected override void Init()
        {
            tableName = "TP_BUS_CARDRENT";

            columns = new String[8][];
            columns[0] = new String[] { "CARDNO", "String" };
            columns[1] = new String[] { "CARDRENT", "Int32" };
            columns[2] = new String[] { "DEALSTATE", "string" };
            columns[3] = new String[] { "UPDATESTAFFNO", "string" };
            columns[4] = new String[] { "UPDATETIME", "DateTime" };
            columns[5] = new String[] { "RSRV1", "String" };
            columns[6] = new String[] { "RSRV2", "String" };
            columns[7] = new String[] { "RSRV3", "DateTime" };

            columnKeys = new String[]{
                   "CARDNO",
               };


            array = new String[8];
            hash.Add("CARDNO", 0);
            hash.Add("CARDRENT", 1);
            hash.Add("DEALSTATE", 2);
            hash.Add("UPDATESTAFFNO", 3);
            hash.Add("UPDATETIME", 4);
            hash.Add("RSRV1", 5);
            hash.Add("RSRV2", 6);
            hash.Add("RSRV3", 7);
        }

        // IC卡号
        public String CARDNO
        {
            get { return GetString("CARDNO"); }
            set { SetString("CARDNO", value); }
        }

        // 卡租金
        public Int32 CARDRENT
        {
            get { return GetInt32("CARDRENT"); }
            set { SetInt32("CARDRENT", value); }
        }

        // 处理状态
        public string DEALSTATE
        {
            get { return Getstring("DEALSTATE"); }
            set { Setstring("DEALSTATE", value); }
        }

        // 更新员工
        public string UPDATESTAFFNO
        {
            get { return Getstring("UPDATESTAFFNO"); }
            set { Setstring("UPDATESTAFFNO", value); }
        }

        // 更新时间
        public DateTime UPDATETIME
        {
            get { return GetDateTime("UPDATETIME"); }
            set { SetDateTime("UPDATETIME", value); }
        }

        // 备用1
        public String RSRV1
        {
            get { return GetString("RSRV1"); }
            set { SetString("RSRV1", value); }
        }

        // 备用2
        public String RSRV2
        {
            get { return GetString("RSRV2"); }
            set { SetString("RSRV2", value); }
        }

        // 备用3
        public DateTime RSRV3
        {
            get { return GetDateTime("RSRV3"); }
            set { SetDateTime("RSRV3", value); }
        }

    }
}


