using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace TDO.Warn
{
     // 告警黑名单表
     public class TF_B_WARN_BLACKTDO : DDOBase
     {
          public TF_B_WARN_BLACKTDO()
          {
          }

          protected override void Init()
          {
               tableName = "TF_B_WARN_BLACK";

               columns = new String[12][];
               columns[0] = new String[]{"CARDNO", "String"};
               columns[1] = new String[]{"CARDTYPE", "string"};
               columns[2] = new String[]{"BLACKSTATE", "string"};
               columns[3] = new String[]{"BLACKTYPE", "string"};
               columns[4] = new String[]{"BLACKLEVEL", "string"};
               columns[5] = new String[]{"CREATETIME", "DateTime"};
               columns[6] = new String[]{"WARNTYPE", "String"};
               columns[7] = new String[]{"WARNLEVEL", "string"};
               columns[8] = new String[]{"UPDATESTAFFNO", "string"};
               columns[9] = new String[]{"UPDATETIME", "DateTime"};
               columns[10] = new String[]{"DOWNTIME", "DateTime"};
               columns[11] = new String[]{"REMARK", "String"};

               columnKeys = new String[]{
                   "CARDNO",
               };


               array = new String[12];
               hash.Add("CARDNO", 0);
               hash.Add("CARDTYPE", 1);
               hash.Add("BLACKSTATE", 2);
               hash.Add("BLACKTYPE", 3);
               hash.Add("BLACKLEVEL", 4);
               hash.Add("CREATETIME", 5);
               hash.Add("WARNTYPE", 6);
               hash.Add("WARNLEVEL", 7);
               hash.Add("UPDATESTAFFNO", 8);
               hash.Add("UPDATETIME", 9);
               hash.Add("DOWNTIME", 10);
               hash.Add("REMARK", 11);
          }

          // 卡号
          public String CARDNO
          {
              get { return  GetString("CARDNO"); }
              set { SetString("CARDNO",value); }
          }

          // 卡类型
          public string CARDTYPE
          {
              get { return  Getstring("CARDTYPE"); }
              set { Setstring("CARDTYPE",value); }
          }

          // 黑名单状态
          public string BLACKSTATE
          {
              get { return  Getstring("BLACKSTATE"); }
              set { Setstring("BLACKSTATE",value); }
          }

          // 黑名单类型
          public string BLACKTYPE
          {
              get { return  Getstring("BLACKTYPE"); }
              set { Setstring("BLACKTYPE",value); }
          }

          // 黑名单级别
          public string BLACKLEVEL
          {
              get { return  Getstring("BLACKLEVEL"); }
              set { Setstring("BLACKLEVEL",value); }
          }

          // 生成时间
          public DateTime CREATETIME
          {
              get { return  GetDateTime("CREATETIME"); }
              set { SetDateTime("CREATETIME",value); }
          }

          // 告警类型
          public String WARNTYPE
          {
              get { return  GetString("WARNTYPE"); }
              set { SetString("WARNTYPE",value); }
          }

          // 告警级别
          public string WARNLEVEL
          {
              get { return  Getstring("WARNLEVEL"); }
              set { Setstring("WARNLEVEL",value); }
          }

          // 更新员工
          public string UPDATESTAFFNO
          {
              get { return  Getstring("UPDATESTAFFNO"); }
              set { Setstring("UPDATESTAFFNO",value); }
          }

          // 更新时间
          public DateTime UPDATETIME
          {
              get { return  GetDateTime("UPDATETIME"); }
              set { SetDateTime("UPDATETIME",value); }
          }

          // 下载时间
          public DateTime DOWNTIME
          {
              get { return  GetDateTime("DOWNTIME"); }
              set { SetDateTime("DOWNTIME",value); }
          }

          // 备注
          public String REMARK
          {
              get { return  GetString("REMARK"); }
              set { SetString("REMARK",value); }
          }

     }
}


