using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace TDO.CardManager
{
    // 停车卡资料表
    public class TF_F_PCARDRECTDO : DDOBase
    {
        public TF_F_PCARDRECTDO()
        {
        }

        protected override void Init()
        {
            tableName = "TF_F_PCARDREC";

            columns = new String[25][];
            columns[0] = new String[] { "CARDNO", "String" };
            columns[1] = new String[] { "CARDTYPECODE", "string" };
            columns[2] = new String[] { "PCODE", "string" };
            columns[3] = new String[] { "AREACODE", "string" };
            columns[4] = new String[] { "PLACECODE", "String" };
            columns[5] = new String[] { "PLATENO", "String" };
            columns[6] = new String[] { "CUSTNAME", "String" };
            columns[7] = new String[] { "CUSTSEX", "String" };
            columns[8] = new String[] { "CUSTBIRTH", "String" };
            columns[9] = new String[] { "PAPERTYPECODE", "String" };
            columns[10] = new String[] { "PAPERNO", "String" };
            columns[11] = new String[] { "CUSTADDR", "String" };
            columns[12] = new String[] { "CUSTPOST", "String" };
            columns[13] = new String[] { "CUSTPHONE", "String" };
            columns[14] = new String[] { "CUSTEMAIL", "String" };
            columns[15] = new String[] { "OPENDATE", "DateTime" };
            columns[16] = new String[] { "ENDDATE", "DateTime" };
            columns[17] = new String[] { "CURRENTPAYTIME", "DateTime" };
            columns[18] = new String[] { "CURRENTPAYFEE", "Int32" };
            columns[19] = new String[] { "LINKNO", "String" };
            columns[20] = new String[] { "USETAG", "string" };
            columns[21] = new String[] { "STATECODE", "string" };
            columns[22] = new String[] { "UPDATESTAFFNO", "string" };
            columns[23] = new String[] { "UPDATETIME", "DateTime" };
            columns[24] = new String[] { "REMARK", "String" };

            columnKeys = new String[]{
                   "CARDNO",
               };


            array = new String[25];
            hash.Add("CARDNO", 0);
            hash.Add("CARDTYPECODE", 1);
            hash.Add("PCODE", 2);
            hash.Add("AREACODE", 3);
            hash.Add("PLACECODE", 4);
            hash.Add("PLATENO", 5);
            hash.Add("CUSTNAME", 6);
            hash.Add("CUSTSEX", 7);
            hash.Add("CUSTBIRTH", 8);
            hash.Add("PAPERTYPECODE", 9);
            hash.Add("PAPERNO", 10);
            hash.Add("CUSTADDR", 11);
            hash.Add("CUSTPOST", 12);
            hash.Add("CUSTPHONE", 13);
            hash.Add("CUSTEMAIL", 14);
            hash.Add("OPENDATE", 15);
            hash.Add("ENDDATE", 16);
            hash.Add("CURRENTPAYTIME", 17);
            hash.Add("CURRENTPAYFEE", 18);
            hash.Add("LINKNO", 19);
            hash.Add("USETAG", 20);
            hash.Add("STATECODE", 21);
            hash.Add("UPDATESTAFFNO", 22);
            hash.Add("UPDATETIME", 23);
            hash.Add("REMARK", 24);
        }

        // 停车卡号
        public String CARDNO
        {
            get { return GetString("CARDNO"); }
            set { SetString("CARDNO", value); }
        }

        // 停车卡类别编码
        public string CARDTYPECODE
        {
            get { return Getstring("CARDTYPECODE"); }
            set { Setstring("CARDTYPECODE", value); }
        }

        // 停车场编码
        public string PCODE
        {
            get { return Getstring("PCODE"); }
            set { Setstring("PCODE", value); }
        }

        // 区域编码
        public string AREACODE
        {
            get { return Getstring("AREACODE"); }
            set { Setstring("AREACODE", value); }
        }

        // 车位编码
        public String PLACECODE
        {
            get { return GetString("PLACECODE"); }
            set { SetString("PLACECODE", value); }
        }

        // 车牌号
        public String PLATENO
        {
            get { return GetString("PLATENO"); }
            set { SetString("PLATENO", value); }
        }

        // 姓名
        public String CUSTNAME
        {
            get { return GetString("CUSTNAME"); }
            set { SetString("CUSTNAME", value); }
        }

        // 性别
        public String CUSTSEX
        {
            get { return GetString("CUSTSEX"); }
            set { SetString("CUSTSEX", value); }
        }

        // 出生日期
        public String CUSTBIRTH
        {
            get { return GetString("CUSTBIRTH"); }
            set { SetString("CUSTBIRTH", value); }
        }

        // 证件类型
        public String PAPERTYPECODE
        {
            get { return GetString("PAPERTYPECODE"); }
            set { SetString("PAPERTYPECODE", value); }
        }

        // 证件号码
        public String PAPERNO
        {
            get { return GetString("PAPERNO"); }
            set { SetString("PAPERNO", value); }
        }

        // 联系地址
        public String CUSTADDR
        {
            get { return GetString("CUSTADDR"); }
            set { SetString("CUSTADDR", value); }
        }

        // 邮政编码
        public String CUSTPOST
        {
            get { return GetString("CUSTPOST"); }
            set { SetString("CUSTPOST", value); }
        }

        // 联系电话
        public String CUSTPHONE
        {
            get { return GetString("CUSTPHONE"); }
            set { SetString("CUSTPHONE", value); }
        }

        // EMAIL地址
        public String CUSTEMAIL
        {
            get { return GetString("CUSTEMAIL"); }
            set { SetString("CUSTEMAIL", value); }
        }

        // 开通日期
        public DateTime OPENDATE
        {
            get { return GetDateTime("OPENDATE"); }
            set { SetDateTime("OPENDATE", value); }
        }

        // 结束日期
        public DateTime ENDDATE
        {
            get { return GetDateTime("ENDDATE"); }
            set { SetDateTime("ENDDATE", value); }
        }

        // 最近缴费时间
        public DateTime CURRENTPAYTIME
        {
            get { return GetDateTime("CURRENTPAYTIME"); }
            set { SetDateTime("CURRENTPAYTIME", value); }
        }

        // 最近缴费金额
        public Int32 CURRENTPAYFEE
        {
            get { return GetInt32("CURRENTPAYFEE"); }
            set { SetInt32("CURRENTPAYFEE", value); }
        }

        // 关联序列号
        public String LINKNO
        {
            get { return GetString("LINKNO"); }
            set { SetString("LINKNO", value); }
        }

        // 有效标志
        public string USETAG
        {
            get { return Getstring("USETAG"); }
            set { Setstring("USETAG", value); }
        }

        // 状态编码
        public string STATECODE
        {
            get { return Getstring("STATECODE"); }
            set { Setstring("STATECODE", value); }
        }

        // 更新员工
        public string UPDATESTAFFNO
        {
            get { return Getstring("UPDATESTAFFNO"); }
            set { Setstring("UPDATESTAFFNO", value); }
        }

        // 更新时间
        public DateTime UPDATETIME
        {
            get { return GetDateTime("UPDATETIME"); }
            set { SetDateTime("UPDATETIME", value); }
        }

        // 备注
        public String REMARK
        {
            get { return GetString("REMARK"); }
            set { SetString("REMARK", value); }
        }

    }
}


