﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Master;
using System.Configuration;
using System.Data;
namespace Controls.Customer.Asp
{
    [
    AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal),
    AspNetHostingPermission(SecurityAction.InheritanceDemand,
        Level = AspNetHostingPermissionLevel.Minimal),
    DefaultProperty("List"),
    ToolboxData("<{0}:CommonPrintPingZheng runat=\"server\"> </{0}:CommonPrintPingZheng>")
    ]
    public class CommonPrintPingZheng : WebControl
    {
        public CommonPrintPingZheng()
        {
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(""),
        Localizable(true)
        ]

        public virtual String PrintArea
        {
            get { return (String)ViewState["PrintArea"]; }
            set { ViewState["PrintArea"] = value; }
        }

        public virtual String Year
        {
            get { return (String)ViewState["Year"]; }
            set { ViewState["Year"] = value; }
        }

        public virtual String Month
        {
            get { return (String)ViewState["Month"]; }
            set { ViewState["Month"] = value; }
        }

        public virtual String Day
        {
            get { return (String)ViewState["Day"]; }
            set { ViewState["Day"] = value; }
        }
        public virtual ArrayList PingZhengList
        {
            get { return (ArrayList)ViewState["PingZhengList"]; }
            set { ViewState["PingZhengList"] = value; }
        }
       
        protected override void RenderContents(HtmlTextWriter writer)
        {
            int initHeight = 75;
            int addNum = 20;
            writer.Write("<div id=\"" + PrintArea + "\" style=\"display:none\">");
            writer.Write("<div class=\"juedui\" style=\"left:0px;top:-10px;\" >");
            writer.Write("<table><tr><td><img width=\"95px\" height=\"80px\" src=\"../../Images/pingzhenglogo.jpg\" /></td><td>业务回单</td></tr></table>");
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px;top:75px;\" >");
            writer.Write(Year + " 年 " + Month + " 月 " + Day + " 日 ");
            writer.Write("</div>");
            if (PingZhengList != null)
            {
                foreach (string var in PingZhengList)
                {
                    initHeight += addNum;
                    writer.Write("<div class=\"juedui\" style=\"left:10px; top:" + initHeight + "px;\" >");
                    writer.Write(var);
                    writer.Write("</div>");
                }
            }
            ArrayList al = (ArrayList)HttpContext.Current.Session["HDADVertise"];
            if (al.Count > 0)
            {
                initHeight += addNum;
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:" + initHeight + "px;\" >");
                writer.Write("&nbsp;");
                writer.Write("</div>");
            }
            foreach (string str in al)
            {
                initHeight += addNum;
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:" + initHeight + "px;\" >");
                writer.Write(str);
                writer.Write("</div>");
            }
            writer.Write("</div>");

        }



    }
}
