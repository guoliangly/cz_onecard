﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace Controls.Customer.Asp
{
    [
    AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal),
    AspNetHostingPermission(SecurityAction.InheritanceDemand,
        Level = AspNetHostingPermissionLevel.Minimal),
    DefaultProperty("List"),
    ToolboxData("<{0}:PrintFaPiao runat=\"server\"> </{0}:PrintFaPiao>")
    ]
    public class PrintFaPiao : WebControl
    {
        public PrintFaPiao()
        {
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(""),
        Localizable(true)
        ]

        public virtual String PrintArea
        {
            get { return (String)ViewState["PrintArea"]; }
            set { ViewState["PrintArea"] = value; }
        }

        public virtual String Year
        {
            get { return (String)ViewState["Year"]; }
            set { ViewState["Year"] = value; }
        }

        public virtual String Month
        {
            get { return (String)ViewState["Month"]; }
            set { ViewState["Month"] = value; }
        }

        public virtual String Day
        {
            get { return (String)ViewState["Day"]; }
            set { ViewState["Day"] = value; }
        }

        public virtual String FuKuanFang
        {
            get { return (String)ViewState["FuKuanFang"]; }
            set { ViewState["FuKuanFang"] = value; }
        }

        public virtual String ShouKuanFang
        {
            get { return (String)ViewState["ShouKuanFang"]; }
            set { ViewState["ShouKuanFang"] = value; }
        }

        public virtual String NaShuiRen
        {
            get { return (String)ViewState["NaShuiRen"]; }
            set { ViewState["NaShuiRen"] = value; }
        }

        public virtual String PiaoHao
        {
            get { return (String)ViewState["PiaoHao"]; }
            set { ViewState["PiaoHao"] = value; }
        }

        public virtual String JinEChina
        {
            get { return (String)ViewState["JinEChina"]; }
            set { ViewState["JinEChina"] = value; }
        }

        public virtual String JinE
        {
            get { return (String)ViewState["JinE"]; }
            set { ViewState["JinE"] = value; }
        }

        public virtual String KaiPiaoRen
        {
            get { return (String)ViewState["KaiPiaoRen"]; }
            set { ViewState["KaiPiaoRen"] = value; }
        }

        public virtual ArrayList ProjectList
        {
            get { return (ArrayList)ViewState["ProjectList"]; }
            set { ViewState["ProjectList"] = value; }
        }

        public virtual ArrayList RemarkList
        {
            get { return (ArrayList)ViewState["RemarkList"]; }
            set { ViewState["RemarkList"] = value; }
        }

        public virtual String FuKuanFangCode
        {
            get { return (String)ViewState["FuKuanFangCode"]; }
            set { ViewState["FuKuanFangCode"] = value; }
        }

        public virtual String JuanHao
        {
            get { return (String)ViewState["JuanHao"]; }
            set { ViewState["JuanHao"] = value; }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.Write("<div id=\"" + PrintArea + "\" style=\"display:none\">");

            writer.Write("<div class=\"juedui\" style=\"left:120px;top:65px;\">");
            if (Year != null && Month != null && Day != null)
                writer.Write(Year + "-" + Month + "-" + Day);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:340px;top:65px;\">");
            writer.Write("其他服务");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:60px;top:90px;\">");
            writer.Write("付款方：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:128px;top:90px;\">");
            writer.Write(FuKuanFang);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:60px;top:107px;\">");
            writer.Write("付款方代码：");
            writer.Write("</div>");


            writer.Write("<div class=\"juedui\" style=\"left:150px;top:107px;\">");
            writer.Write(FuKuanFangCode);
            writer.Write("</div>");


            writer.Write("<div class=\"juedui\" style=\"left:60px;top:125px;\">");
            writer.Write("收款方：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:128px;top:125px;\">");
            writer.Write(ShouKuanFang);
            writer.Write("</div>");


            writer.Write("<div class=\"juedui\" style=\"left:60px;top:145px;\">");
            writer.Write("纳税人识别码：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:160px;top:145px;\">");
            writer.Write(NaShuiRen);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:440px;top:90px;\">");
            writer.Write("机打代码：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:510px;top:90px;\">");
            writer.Write(JuanHao);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:440px;top:115px;\">");
            writer.Write("机打号码：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:510px;top:115px;\">");
            writer.Write(PiaoHao);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:440px;top:175px;\">");
            writer.Write("备注：");
            writer.Write("</div>");

            if (RemarkList != null)
            {
                for (int index = 0; index < RemarkList.Count && index < 5; index++)
                {
                    int top = 200 + 20 * index;
                    writer.Write("<div class=\"juedui\" style=\"left:440px;top:" + top.ToString() + "px;\">");
                    writer.Write((String)RemarkList[index]);
                    writer.Write("</div>");
                }
            }

            writer.Write("<div class=\"juedui\" style=\"left:60px;top:175px;\">");
            writer.Write("项目内容");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:290px;top:175px;\">");
            writer.Write("金额");
            writer.Write("</div>");
            if (ProjectList != null)
            {
                for (int index = 0; index < ProjectList.Count && index < 5; index++)
                {
                    int top = 200 + 20 * index;
                    String[] arrProject = (String[])ProjectList[index];
                    writer.Write("<div class=\"juedui\" style=\"left:60px;top:" + top.ToString() + "px;\">");
                    writer.Write(arrProject[0]);
                    writer.Write("</div>");

                    writer.Write("<div class=\"juedui\" style=\"left:290px;top:" + top.ToString() + "px;\">");
                    writer.Write(arrProject[1]);
                    writer.Write("</div>");
                }
            }

            writer.Write("<div class=\"juedui\" style=\"left:60px;top:276px;\">");
            writer.Write("合计（大写）：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:190px;top:276px;\">");
            writer.Write(JinEChina);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:440px;top:276px;\">");
            writer.Write("￥");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:470px;top:276px;\">");
            writer.Write(JinE);
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:60px;top:302px;\">");
            writer.Write("收款单位（盖章有效）：");
            writer.Write("</div>");

            //writer.Write("<div class=\"juedui\" style=\"left:220px;top:302px;\">");
            //writer.Write(ShouKuanFang);
            //writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:470px;top:302px;\">");
            writer.Write("开票人：");
            writer.Write("</div>");

            writer.Write("<div class=\"juedui\" style=\"left:520px;top:302px;\">");
            writer.Write(KaiPiaoRen);
            writer.Write("</div>");

            writer.Write("</div>");
        }
    }
}
