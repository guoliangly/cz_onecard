﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Master;
using System.Configuration;
using System.Data;
namespace Controls.Customer.Asp
{
    [
    AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal),
    AspNetHostingPermission(SecurityAction.InheritanceDemand,
        Level = AspNetHostingPermissionLevel.Minimal),
    DefaultProperty("List"),
    ToolboxData("<{0}:PrintPingZheng runat=\"server\"> </{0}:PrintPingZheng>")
    ]
    public class PrintPingZheng : WebControl
    {
        public PrintPingZheng()
        {
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(""),
        Localizable(true)
        ]

        public virtual String PrintArea
        {
            get { return (String)ViewState["PrintArea"]; }
            set { ViewState["PrintArea"] = value; }
        }

        public virtual String Year
        {
            get { return (String)ViewState["Year"]; }
            set { ViewState["Year"] = value; }
        }

        public virtual String Month
        {
            get { return (String)ViewState["Month"]; }
            set { ViewState["Month"] = value; }
        }

        public virtual String Day
        {
            get { return (String)ViewState["Day"]; }
            set { ViewState["Day"] = value; }
        }

        public virtual String TIME
        {
            get { return (String)ViewState["Time"]; }
            set { ViewState["Time"] = value; }
        }

        public virtual String SERVICEFEE
        {
            get { return (String)ViewState["ServiceFee"]; }
            set { ViewState["ServiceFee"] = value; }
        }
        //旧卡
        public virtual String CardNo
        {
            get { return (String)ViewState["CardNo"]; }
            set { ViewState["CardNo"] = value; }
        }
        //新卡
        public virtual String NewCardNo
        {
            get { return (String)ViewState["NewCardNo"]; }
            set { ViewState["NewCardNo"] = value; }
        }

        public virtual String StaffName
        {
            get { return (String)ViewState["StaffName"]; }
            set { ViewState["StaffName"] = value; }
        }

        public virtual String ZongJinEChina
        {
            get { return (String)ViewState["ZongJinEChina"]; }
            set { ViewState["ZongJinEChina"] = value; }
        }

        public virtual String ZongJinE
        {
            get { return (String)ViewState["ZongJinE"]; }
            set { ViewState["ZongJinE"] = value; }
        }

        public virtual String ZhangHao
        {
            get { return (String)ViewState["ZhangHao"]; }
            set { ViewState["ZhangHao"] = value; }
        }

        public virtual String JiaoYiFangShi
        {
            get { return (String)ViewState["JiaoYiFangShi"]; }
            set { ViewState["JiaoYiFangShi"] = value; }
        }

        public virtual String UserName
        {
            get { return (String)ViewState["UserName"]; }
            set { ViewState["UserName"] = value; }
        }

        public virtual String ZhengJianHaoMa
        {
            get { return (String)ViewState["ZhengJianHaoMa"]; }
            set { ViewState["ZhengJianHaoMa"] = value; }
        }

        public virtual String ZhengJianMingChen
        {
            get { return (String)ViewState["ZhengJianMingChen"]; }
            set { ViewState["ZhengJianMingChen"] = value; }
        }

        public virtual String JiaoYiLeiXing
        {
            get { return (String)ViewState["JiaoYiLeiXing"]; }
            set { ViewState["JiaoYiLeiXing"] = value; }
        }

        public virtual String JiJuHao
        {
            get { return (String)ViewState["JiJuHao"]; }
            set { ViewState["JiJuHao"] = value; }
        }

        public virtual String LiuShuiHao
        {
            get { return (String)ViewState["LiuShuiHao"]; }
            set { ViewState["LiuShuiHao"] = value; }
        }

        public virtual String JiaoYiJinE
        {
            get { return (String)ViewState["JiaoYiJinE"]; }
            set { ViewState["JiaoYiJinE"] = value; }
        }

        public virtual String ICKaYuE
        {
            get { return (String)ViewState["ICKaYuE"]; }
            set { ViewState["ICKaYuE"] = value; }
        }

        public virtual String ShouXuFei
        {
            get { return (String)ViewState["ShouXuFei"]; }
            set { ViewState["ShouXuFei"] = value; }
        }

        public virtual String KaYaJin
        {
            get { return (String)ViewState["KaYaJin"]; }
            set { ViewState["KaYaJin"] = value; }
        }

        public virtual String ZheJiuFei
        {
            get { return (String)ViewState["ZheJiuFei"]; }
            set { ViewState["ZheJiuFei"] = value; }
        }

        public virtual String Other
        {
            get { return (String)ViewState["Other"]; }
            set { ViewState["Other"] = value; }
        }

        public virtual String TipUser
        {
            get { return (String)ViewState["TipUser"]; }
            set { ViewState["TipUser"] = value; }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
           
            writer.Write("<div id=\"" + PrintArea + "\" style=\"display:none\">");
            writer.Write("<div class=\"juedui\" style=\"left:0px;top:-10px;\" >");
            writer.Write("<table><tr><td><img width=\"95px\" height=\"80px\" src=\"../../Images/pingzhenglogo.jpg\" /></td><td>业务回单</td></tr></table>");
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px;top:75px;\" >");
            writer.Write(Year + " 年 " + Month + " 月 " + Day + " 日 ");
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:95px;\" >");
            writer.Write("姓名："+UserName);
            writer.Write("</div>");
            if (NewCardNo != "")
            {
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:115px;\" >");
                writer.Write("卡号："+NewCardNo);
                writer.Write("</div>");
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:275px;\" >");
                writer.Write("旧卡号："+CardNo);
                writer.Write("</div>");
            }
            else
            {
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:115px;\" >");
                writer.Write("卡号："+CardNo);
                writer.Write("</div>");
            }

            writer.Write("<div class=\"juedui\" style=\"left:10px; top:135px;\" >");
            writer.Write("交易类型："+JiaoYiLeiXing);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:155px;\" >");
            writer.Write("交易时间："+TIME);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:175px;\" >");
            writer.Write("交易金额："+JiaoYiJinE);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:195px;\" >");
            writer.Write("卡 余 额：" + ICKaYuE);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:215px;\" >");
            writer.Write("服 务 费：" + SERVICEFEE);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:235px;\" >");
            writer.Write("经 办 人：" + ((Hashtable)HttpContext.Current.Session["LogonInfo"])["UserID"]);
            writer.Write("</div>");
            writer.Write("<div class=\"juedui\" style=\"left:10px; top:255px;\" >");
            writer.Write("流水号：" + Other);
            writer.Write("</div>");
            ArrayList al = (ArrayList)HttpContext.Current.Session["HDADVertise"];
            if (al.Count > 0)
            {
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:" + (NewCardNo!=""?295:275).ToString()+ "px;\" >");
                writer.Write("&nbsp;");
                writer.Write("</div>");
            }
            int i = 20;
            foreach (string str in al)
            {
                writer.Write("<div class=\"juedui\" style=\"left:10px; top:" + (i + (NewCardNo != "" ? 295 : 275)).ToString() + "px;\" >");
                writer.Write(str);
                writer.Write("</div>");
                i = i + 20;
            }
            writer.Write("</div>");

        }



    }
}
