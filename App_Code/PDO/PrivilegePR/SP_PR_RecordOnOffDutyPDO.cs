﻿using System;
using System.Collections.Generic;
using System.Web;
using Master;

/// <summary>
///SP_PR_RecordOnOffDutyPDO 的摘要说明
/// </summary>
public class SP_PR_RecordOnOffDutyPDO : PDOBase
{
    //考勤及考勤日志
    public SP_PR_RecordOnOffDutyPDO()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    protected override void Init()
    {
        InitBegin("SP_PR_RecordOnOffDuty", 5);

        AddField("@TRADETYPECODE", "string", "2", "input");

        InitEnd();
    }

    // 业务类型 "01"-签到 "02"-签退
    public string TRADETYPECODE
    {
        get { return Getstring("OPERCARDNO"); }
        set { Setstring("OPERCARDNO", value); }
    }
}