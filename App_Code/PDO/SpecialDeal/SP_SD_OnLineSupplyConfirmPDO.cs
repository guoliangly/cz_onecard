using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.SpecialDeal
{
     // 充值比对消费信息回收
     public class SP_SD_OnLineSupplyConfirmPDO : PDOBase
     {
         public SP_SD_OnLineSupplyConfirmPDO()
          {
          }

          protected override void Init()
          {
              InitBegin("SP_SD_OnLineSupplyConfirm", 6);

               AddField("@remark", "String", "150", "input");

               InitEnd();
          }

          // 回收说明
          public String renewRemark
          {
              get { return GetString("remark"); }
              set { SetString("remark", value); }
          }


     }
}


