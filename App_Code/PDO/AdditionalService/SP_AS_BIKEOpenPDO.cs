using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.AdditionalService
{
     // 自行车卡通
     public class SP_AS_BIKEOpenPDO : PDOBase
     {
         public SP_AS_BIKEOpenPDO()
          {
          }

          protected override void Init()
          {
              InitBegin("SP_AS_BIKEOpen", 23);

               AddField("@cardNo", "string", "16", "input");
               AddField("@companyno", "string", "4", "input");
               AddField("@deposit", "Int32", "", "input");
               AddField("@tradetypecode", "string", "2", "input");

               AddField("@phone", "string", "20", "input");
               AddField("@birth", "string", "8", "input");
               AddField("@custpost", "string", "6", "input");
               AddField("@addr", "string", "100", "input");
               AddField("@email", "string", "50", "input");

               AddField("@TRADEID", "string", "16", "output");

               InitEnd();
          }

          // 卡号
          public string cardNo
          {
              get { return  Getstring("cardNo"); }
              set { Setstring("cardNo",value); }
          }

          // 自行车公司编码
          public string companyno
          {
              get { return Getstring("companyno"); }
              set { Setstring("companyno", value); }
          }

          // 押金
          public Int32 deposit
          {
              get { return GetInt32("deposit"); }
              set { SetInt32("deposit", value); }
          }

          // 业务类型
          public string tradetypecode
          {
              get { return Getstring("tradetypecode"); }
              set { Setstring("tradetypecode", value); }
          }
          // 电话
          public string phone
          {
              get { return Getstring("phone"); }
              set { Setstring("phone", value); }
          }
          // 电话
          public string birth
          {
              get { return Getstring("birth"); }
              set { Setstring("birth", value); }
          }
          // 电话
          public string addr
          {
              get { return Getstring("addr"); }
              set { Setstring("addr", value); }
          }
          // 电话
          public string email
          {
              get { return Getstring("email"); }
              set { Setstring("email", value); }
          }

          // 电话
          public string custpost
          {
              get { return Getstring("custpost"); }
              set { Setstring("custpost", value); }
          }
     }
}


