using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.AdditionalService
{
     // 自行车卡通
     public class SP_AS_BIKEClosePDO : PDOBase
     {
         public SP_AS_BIKEClosePDO()
          {
          }

          protected override void Init()
          {
              InitBegin("SP_AS_BIKEClose", 23);

               AddField("@cardNo", "string", "16", "input");
               AddField("@companyno", "string", "4", "input");
               AddField("@deposit", "Int32", "", "input");
               AddField("@tradetypecode", "string", "2", "input");
               AddField("@cancelTradeid", "string", "16", "input");
               AddField("@tradeid", "string", "16", "input");

               InitEnd();
          }

          // 卡号
          public string cardNo
          {
              get { return  Getstring("cardNo"); }
              set { Setstring("cardNo",value); }
          }

          // 自行车公司编码
          public string companyno
          {
              get { return Getstring("companyno"); }
              set { Setstring("companyno", value); }
          }

          // 押金
          public Int32 deposit
          {
              get { return GetInt32("deposit"); }
              set { SetInt32("deposit", value); }
          }

          // 业务类型
          public string tradetypecode
          {
              get { return Getstring("tradetypecode"); }
              set { Setstring("tradetypecode", value); }
          }
          // 返销业务流水号
          public string cancelTradeid
          {
              get { return Getstring("cancelTradeid"); }
              set { Setstring("cancelTradeid", value); }
          }

          // 返销业务流水号
          public string tradeid
          {
              get { return Getstring("tradeid"); }
              set { Setstring("tradeid", value); }
          }
     }
}


