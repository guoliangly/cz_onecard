using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.AdditionalService
{
     // 自行车卡通
     public class SP_AS_BIKEChangePDO : PDOBase
     {
         public SP_AS_BIKEChangePDO()
          {
          }

          protected override void Init()
          {
              InitBegin("SP_AS_BIKEChange", 23);

               AddField("@newcardNo", "string", "16", "input");
               AddField("@oldcardNo", "string", "16", "input");
               AddField("@tradetypecode", "string", "2", "input");
               AddField("@changetype", "string", "2", "input");
               AddField("@TRADEID", "string", "16", "output");

               InitEnd();
          }

          // 新卡号
          public string newcardNo
          {
              get { return Getstring("newcardNo"); }
              set { Setstring("newcardNo", value); }
          }

          // 旧卡号
          public string oldcardNo
          {
              get { return Getstring("oldcardNo"); }
              set { Setstring("oldcardNo", value); }
          }

          // 业务类型
          public string tradetypecode
          {
              get { return Getstring("tradetypecode"); }
              set { Setstring("tradetypecode", value); }
          }

          // 换卡类型
          public string changetype
          {
              get { return Getstring("changetype"); }
              set { Setstring("changetype", value); }
          }
     }
}


