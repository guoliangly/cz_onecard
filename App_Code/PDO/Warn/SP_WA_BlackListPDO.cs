using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.Warn
{
     public class SP_WA_BlackListPDO : PDOBase
     {
          public SP_WA_BlackListPDO()
          {
          }

          protected override void Init()
          {
                InitBegin("SP_WA_BlackList",5 + 5);

               AddField("@funcCode", "String", "16", "input");
               AddField("@oldCardNo", "string", "16", "input");
               AddField("@cardNo", "string", "16", "input");
               AddField("@cardType", "string", "1", "input");
               AddField("@blackState", "string", "1", "input");
               AddField("@blackType", "string", "1", "input");
               AddField("@blackLevel", "string", "1", "input");
               AddField("@warnType", "String", "16", "input");
               AddField("@warnLevel", "String", "1", "input");
               AddField("@remark", "string", "100", "input");

               InitEnd();
          }

          public String funcCode
          {
              get { return  GetString("funcCode"); }
              set { SetString("funcCode",value); }
          }

          public String warnType
          {
              get { return  GetString("warnType"); }
              set { SetString("warnType",value); }
          }
         public string oldCardNo
         {
             get { return Getstring("oldCardNo"); }
             set { Setstring("oldCardNo", value); }
         }

          public string cardNo
          {
              get { return  Getstring("cardNo"); }
              set { Setstring("cardNo",value); }
          }

         public string cardType
         {
             get { return Getstring("cardType"); }
             set { Setstring("cardType", value); }
         }

         public string blackState
         {
             get { return Getstring("blackState"); }
             set { Setstring("blackState", value); }
         }

         public string blackType
         {
             get { return Getstring("blackType"); }
             set { Setstring("blackType", value); }
         }

         public string blackLevel
         {
             get { return Getstring("blackLevel"); }
             set { Setstring("blackLevel", value); }
         }

         public string warnLevel
          {
              get { return Getstring("warnLevel"); }
              set { Setstring("warnLevel", value); }
          }

          public string remark
          {
              get { return  Getstring("remark"); }
              set { Setstring("remark",value); }
          }


     }
}


