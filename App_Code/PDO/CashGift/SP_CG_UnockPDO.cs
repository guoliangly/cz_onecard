﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Master;

namespace PDO.CashGift
{
    /// <summary>
    ///SP_CG_UnockPDO 的摘要说明
    /// </summary>
    public class SP_CG_UnockPDO : PDOBase
    {
        public SP_CG_UnockPDO()
        {
            //
            //TODO: 在此处添加构造函数逻辑
            //
        }
        protected override void Init()
        {
            InitBegin("SP_CG_Unlock", 12);

            AddField("@CARDNO", "string", "16", "input");
            AddField("@ASN", "string", "16", "input");
            AddField("@CARDTYPECODE", "string", "2", "input");
            AddField("@CHECKSTAFFNO", "string", "6", "input");
            AddField("@CHECKDEPARTNO", "string", "4", "input");
            AddField("@OPERCARDNO", "string", "16", "input");
            AddField("@TRADEID", "string", "16", "output");

            InitEnd();
        }

        // 卡号
        public string CARDNO
        {
            get { return Getstring("CARDNO"); }
            set { Setstring("CARDNO", value); }
        }

        // 应用序列号
        public string ASN
        {
            get { return Getstring("ASN"); }
            set { Setstring("ASN", value); }
        }

        // 卡类型编码
        public string CARDTYPECODE
        {
            get { return Getstring("CARDTYPECODE"); }
            set { Setstring("CARDTYPECODE", value); }
        }

        // 审批员工编码
        public string CHECKSTAFFNO
        {
            get { return Getstring("CHECKSTAFFNO"); }
            set { Setstring("CHECKSTAFFNO", value); }
        }

        // 审批部门编码
        public string CHECKDEPARTNO
        {
            get { return Getstring("CHECKDEPARTNO"); }
            set { Setstring("CHECKDEPARTNO", value); }
        }

        // 操作员卡号
        public string OPERCARDNO
        {
            get { return Getstring("OPERCARDNO"); }
            set { Setstring("OPERCARDNO", value); }
        }

        // 返回交易序列号
        public string TRADEID
        {
            get { return Getstring("TRADEID"); }
            set { Setstring("TRADEID", value); }
        }
    }
}