using System;
using System.Data;
using System.Configuration;
using System.Collections;
using Master;

namespace PDO.PartnerShip
{
     // 单位信息增加
     public class SP_PS_UnitInfoChangeAddPDO : PDOBase
     {
          public SP_PS_UnitInfoChangeAddPDO()
          {
          }

          protected override void Init()
          {
                InitBegin("SP_PS_UnitInfoChangeAdd",13);

               AddField("@corpNo", "string", "4", "input");
               AddField("@corp", "String", "20", "input");
               AddField("@callingNo", "string", "2", "input");
               AddField("@corpAdd", "String", "50", "input");
               AddField("@corpMark", "String", "50", "input");
               AddField("@linkMan", "String", "10", "input");
               AddField("@corpPhone", "String", "40", "input");
               AddField("@remark", "String", "100", "input");

               InitEnd();
          }

          // 单位编码
          public string corpNo
          {
              get { return  Getstring("corpNo"); }
              set { Setstring("corpNo",value); }
          }

          // 单位名称
          public String corp
          {
              get { return  GetString("corp"); }
              set { SetString("corp",value); }
          }

          // 行业编码
          public string callingNo
          {
              get { return  Getstring("callingNo"); }
              set { Setstring("callingNo",value); }
          }

          // 单位地址
          public String corpAdd
          {
              get { return  GetString("corpAdd"); }
              set { SetString("corpAdd",value); }
          }

          // 单位说明
          public String corpMark
          {
              get { return  GetString("corpMark"); }
              set { SetString("corpMark",value); }
          }

          // 联系人
          public String linkMan
          {
              get { return  GetString("linkMan"); }
              set { SetString("linkMan",value); }
          }

          // 联系电话
          public String corpPhone
          {
              get { return  GetString("corpPhone"); }
              set { SetString("corpPhone",value); }
          }

          // 备注
          public String remark
          {
              get { return  GetString("remark"); }
              set { SetString("remark",value); }
          }

     }
}


