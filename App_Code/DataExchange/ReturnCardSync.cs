﻿//-----------------------------------------------------------------------
// <copyright file="CardReturnSync.cs" company="linkage">
//   * 功能名: 退卡。
// </copyright>
// <author>闵建仁</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2013/06/17    闵建仁         初次开发   
//-----------------------------------------------------------------------

namespace DataExchange
{
    using System.Data;
    using System.Text;

    /// <summary>
    /// 退卡请求类
    /// </summary>
    public class ReturnCardSync : SyncRequest
    {
        /// <summary>
        /// 构造函数<see cref="ReturnCardSync"/>.
        /// </summary>
        public ReturnCardSync()
        {
            this.bipCode = "0010"; //退卡同步接口
        }

        //操作类型
        private string tradeTypeCode;
        public string TradeTypeCode
        {
            get { return tradeTypeCode; }
            set { tradeTypeCode = value; }
        }

        //市民卡卡号
        private string cardNO;
        public string CardNO
        {
            get { return cardNO; }
            set { cardNO = value; }
        }

        //银行卡号
        private string bankCardNo;
        public string BankCardNo
        {
            get { return bankCardNo; }
            set { bankCardNo = value; }
        }

        //证件类型
        private string paperTypeCode;
        public string PaperTypeCode
        {
            get { return paperTypeCode; }
            set { paperTypeCode = value; }
        }


        //证件号码
        private string paperNo;
        public string PaperNo
        {
            get { return paperNo; }
            set { paperNo = value; }
        }


        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>
        /// 请求XML
        /// </returns>
        public override string SetupXML()
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<RETURNCARDREQ>");
            xml.AppendLine(string.Format("<TRADETYPECODE>{0}</TRADETYPECODE>", this.tradeTypeCode));
            xml.AppendLine(string.Format("<CARDNO>{0}</CARDNO>", this.cardNO));
            xml.AppendLine(string.Format("<BANKCARDNO>{0}</BANKCARDNO>", this.bankCardNo));
            xml.AppendLine(string.Format("<PAPERTYPECODE>{0}</PAPERTYPECODE>", this.paperTypeCode));
            xml.AppendLine(string.Format("<PAPERNO>{0}</PAPERNO>", this.paperNo));
            xml.AppendLine("</RETURNCARDREQ>");
            return xml.ToString();
        }

        /// <summary>
        /// DataRow转换
        /// </summary>
        /// <param name="row">DataRow</param>
        public override void ParseFormDataRow(DataRow row)
        {
            this.TradeID = row["TRADEID"].ToString();
            this.HomeDomain = row["SYNCSYSCODE"].ToString();
            this.TradeTypeCode = "00";
            this.CardNO = row["CARDNO"].ToString();
            this.BankCardNo = row["BANKCARDNO"].ToString();
            this.PaperTypeCode = row["PAPERTYPECODE"].ToString();
            this.PaperNo = row["PAPERNO"].ToString();
        }
    }
}