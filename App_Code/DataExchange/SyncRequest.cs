﻿//-----------------------------------------------------------------------
// <copyright file="SyncRequest.cs" company="linkage">
//   * 功能名: 同步请求类

// </copyright>
// <author>闵建仁</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2013/08/06    闵建仁         初次开发  

//-----------------------------------------------------------------------
namespace DataExchange
{
    using System;
    using System.Data;
    using System.Xml;

    /// <summary>
    /// 同步请求类

    /// </summary>
    public abstract class SyncRequest
    {
        /// <summary>
        /// 业务功能代码
        /// </summary>
        protected string bipCode;
        public string BIPCode
        {
            get { return this.bipCode; }
        }

        /// <summary>
        /// 应答方应用域代码
        /// </summary>
        private string homeDomain;
        public string HomeDomain
        {
            get { return this.homeDomain; }
            set { this.homeDomain = value; }
        }

        /// <summary>
        /// 业务流水号

        /// </summary>
        private string tradeID;
        public string TradeID
        {
            get { return this.tradeID; }
            set { this.tradeID = value; }
        }

        /// <summary>
        /// 同步状态

        /// </summary>
        private string syncCode;
        public string SyncCode
        {
            get { return this.syncCode; }
            set { this.syncCode = value; }
        }

        /// <summary>
        /// 同步异常信息
        /// </summary>
        private string syncErrInfo;
        public string SyncErrInfo
        {
            get { return this.syncErrInfo; }
            set { this.syncErrInfo = value; }
        }

        /// <summary>
        /// 同步完成时间
        /// </summary>
        private DateTime syncTime;
        public DateTime SyncTime
        {
            get { return this.syncTime; }
            set { this.syncTime = value; }
        }

        /// <summary>
        /// 卡号
        /// </summary>
        private string cardNO;
        public string CardNO
        {
            get { return this.cardNO; }
            set { this.cardNO = value; }
        }

        /// <summary>
        /// 任务编号
        /// </summary>
        private string batchID;
        public string BatchID
        {
            get { return this.batchID; }
            set { this.batchID = value; }
        }

        /// <summary>
        /// Gets the BIP code.
        /// </summary>
        /// <returns>BIP code</returns>
        public string GetBIPCode()
        {
            string result = string.Empty;
            switch (this.bipCode)
            {
                case "01": result = "0002";
                    break; ////领卡

                case "06":                        ////换卡
                case "07": result = "0004";
                    break; ////补卡
                case "02":                        ////口挂
                case "03":                        ////解挂
                case "04":                        ////书挂
                case "05":                        ////书挂解挂
                    result = "0003";
                    break;
                case "12":                        ////批量数据导入
                case "13":                        ////补卡数据导入
                case "14":                        ////换卡数据导入
                case "15": result = "0001";
                    break;  ////零星制卡数据导入
            }
            return result;
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>请求XML</returns>
        public abstract string SetupXML();

        /// <summary>
        /// DataRow转换.
        /// </summary>
        /// <param name="dr">DataRow</param>
        public abstract void ParseFormDataRow(DataRow dr);


        /// <summary>
        /// 获取返回参数.
        /// </summary>
        /// <param name="rsp">XML</param>
        public virtual void GetSyncResponse(XmlNodeList rsp)
        {

        }
    }
}