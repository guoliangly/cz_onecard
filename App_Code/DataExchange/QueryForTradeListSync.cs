﻿using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Xml;

/// <summary>
///AppOpenCloseSync 的摘要说明
/// </summary>
namespace DataExchange
{
    public class QueryForTradeListSync : SyncBikeRequest
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public QueryForTradeListSync()
        {
            this.bipCode = "0004"; 
        }
        //操作类型
        private string tradeTypeCode;
        public string TradeTypeCode
        {
            get { return tradeTypeCode; }
            set { tradeTypeCode = value; }
        }
        private string cardNO;
        public string CardNO
        {
            get { return cardNO; }
            set { cardNO = value; }
        }

        private string queryType;
        public string QueryType
        {
            get { return queryType; }
            set { queryType = value; }
        }


        /// <summary>
        /// 开始时间
        /// </summary>
        private string begintime;
        public string Begintime
        {
            get { return this.begintime; }
            set { this.begintime = value; }
        }

        /// <summary>
        /// 结束时间
        /// </summary>
        private string endtime;
        public string Endtime
        {
            get { return this.endtime; }
            set { this.endtime = value; }
        }

        //网点编号
        private string deptno;
        public string DEPTNO
        {
            get { return deptno; }
            set { deptno = value; }
        }
        //操作员编号
        private string operno;
        public string OPERNO
        {
            get { return operno; }
            set { operno = value; }
        }

        /// <summary>
        /// 自行车借还车记录明细

        /// </summary>
        private List<AppServiceRecordDetail> detail;
        public List<AppServiceRecordDetail> AppServiceRecordList
        {
            get { return detail; }
            set { detail = value; }
        }

        

        /// <summary>
        /// 获取返回参数.
        /// </summary>
        /// <param name="rsp">XML</param>
        public override void GetSyncResponse(XmlNodeList rsp)
        {
            detail = new List<AppServiceRecordDetail>();
            foreach (XmlNode fn in rsp)
            {
                if (fn.Name == "DATA")
                {
                    AppServiceRecordDetail asd = new AppServiceRecordDetail();
                    foreach (XmlNode n in fn.ChildNodes)
                    {
                        
                        if (n.Name == "LEASETIME")
                        {
                            asd.LeaseTime = n.InnerText;
                        }
                        else if (n.Name == "LEASESHED")
                        {
                            asd.LeaseShed = n.InnerText;
                        }
                        else if (n.Name == "LOCATIVEID")
                        {
                            asd.LocativeID = n.InnerText;
                        }
                        else if (n.Name == "RTTIME")
                        {
                            asd.RtTime = n.InnerText;
                        }
                        else if (n.Name == "RTSHED")
                        {
                            asd.RtShed = n.InnerText;
                        }
                        else if (n.Name == "RTLOCATIVEID")
                        {
                            asd.RtLocativeID = n.InnerText;
                        }
                        
                    }
                    detail.Add(asd);
                }
                
            }
        }


        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>
        /// 请求XML
        /// </returns>
        public override string SetupXML()
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<RENTQUERYREQ>");
            xml.AppendLine(string.Format("<TRADETYPECODE>{0}</TRADETYPECODE>", this.tradeTypeCode));
            xml.AppendLine(string.Format("<CARDNO>{0}</CARDNO>", this.cardNO));
            xml.AppendLine(string.Format("<QUERYTYPE>{0}</QUERYTYPE>", this.queryType));

            xml.AppendLine(string.Format("<BEGINDATE>{0}</BEGINDATE>", this.begintime));
            xml.AppendLine(string.Format("<ENDDATE>{0}</ENDDATE>", this.endtime));
            xml.AppendLine(string.Format("<DEPTNO>{0}</DEPTNO>", this.deptno));
            xml.AppendLine(string.Format("<OPERNO>{0}</OPERNO>", this.operno));
            xml.AppendLine("</RENTQUERYREQ>");
            return xml.ToString();
        }

        /// <summary>
        /// DataRow转换
        /// </summary>
        /// <param name="row">DataRow</param>
        public override void ParseFormDataRow(DataRow row)
        {
            this.HomeDomain = row["SYNCSYSCODE"].ToString();
            this.SIGN = "9000000001";
            this.TradeTypeCode = "00";//开通
            this.CardNO = row["CARDNO"].ToString();
            this.DEPTNO = "BKLR";
            this.OPERNO = "BKLR01";
            this.Begintime = row["BEGINTIME"].ToString();
            this.Endtime = row["ENDTIME"].ToString();
            this.QueryType = row["QUERYTYPE"].ToString();
        }
    }

    public class AppServiceRecordDetail
    {

        /// <summary>
        /// 借车时间
        /// </summary>
        private string leasetime;
        public string LeaseTime
        {
            get { return this.leasetime; }
            set { this.leasetime = value; }
        }

        /// <summary>
        /// 借车站点名称
        /// </summary>
        private string leaseshed;
        public string LeaseShed
        {
            get { return this.leaseshed; }
            set { this.leaseshed = value; }
        }

        /// <summary>
        /// 借车车桩号

        /// </summary>
        private string locativeid;
        public string LocativeID
        {
            get { return this.locativeid; }
            set { this.locativeid = value; }
        }

        /// <summary>
        /// 还车时间
        /// </summary>
        private string rttime;
        public string RtTime
        {
            get { return this.rttime; }
            set { this.rttime = value; }
        }

        /// <summary>
        /// 还车站点名称
        /// </summary>
        private string rtshed;
        public string RtShed
        {
            get { return this.rtshed; }
            set { this.rtshed = value; }
        }

        /// <summary>
        /// 还车车桩号

        /// </summary>
        private string rtlocativeid;
        public string RtLocativeID
        {
            get { return this.rtlocativeid; }
            set { this.rtlocativeid = value; }
        }
    }
}