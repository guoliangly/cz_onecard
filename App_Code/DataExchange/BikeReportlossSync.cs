﻿//-----------------------------------------------------------------------
// <copyright file="BikeOpenSync.cs" company="linkage">
//   * 功能名: 自行车开通。
// </copyright>
// <author>闵建仁</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2014/06/11    王定喜         初次开发   
//-----------------------------------------------------------------------

namespace DataExchange
{
    using System.Data;
    using System.Text;

    /// <summary>
    /// 自行车开通请求类
    /// </summary>
    public class BikeReportlossSync : SyncBikeRequest
    {
           /// <summary>
        /// 构造函数<see cref="BikeOpenSync"/>.
        /// </summary>
        public BikeReportlossSync()
        {
            this.bipCode = "0002"; //自行车同步接口
        }

        //操作类型
        private string tradeTypeCode;
        public string TradeTypeCode
        {
            get { return tradeTypeCode; }
            set { tradeTypeCode = value; }
        }

        //市民卡卡号
        private string cardNO;
        public string CardNO
        {
            get { return cardNO; }
            set { cardNO = value; }
        }


        //证件类型
        private string paperTypeCode;
        public string PaperTypeCode
        {
            get { return paperTypeCode; }
            set { paperTypeCode = value; }
        }


        //证件号码
        private string paperNo;
        public string PaperNo
        {
            get { return paperNo; }
            set { paperNo = value; }
        }

        //挂失原因
        private string reason;
        public string REASON
        {
            get { return reason; }
            set { reason = value; }
        }
        
        //网点编号
        private string deptno;
        public string DEPTNO
        {
            get { return deptno; }
            set { deptno = value; }
        }
        //操作员编号
        private string operno;
        public string OPERNO
        {
            get { return operno; }
            set { operno = value; }
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>
        /// 请求XML
        /// </returns>
        public override string SetupXML()
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<BLOCKREQ>");
            xml.AppendLine(string.Format("<TRADETYPECODE>{0}</TRADETYPECODE>", this.tradeTypeCode));
            xml.AppendLine(string.Format("<CARDNO>{0}</CARDNO>", this.cardNO));

            xml.AppendLine(string.Format("<PAPERTYPECODE>{0}</PAPERTYPECODE>", this.paperTypeCode));
            xml.AppendLine(string.Format("<PAPERNO>{0}</PAPERNO>", this.paperNo));
            xml.AppendLine(string.Format("<REASON>{0}</REASON>", this.reason));
            xml.AppendLine(string.Format("<DEPTNO>{0}</DEPTNO>", this.deptno));
            xml.AppendLine(string.Format("<OPERNO>{0}</OPERNO>", this.operno));
            xml.AppendLine("</BLOCKREQ>");
            return xml.ToString();
        }

        /// <summary>
        /// DataRow转换
        /// </summary>
        /// <param name="row">DataRow</param>
        public override void ParseFormDataRow(DataRow row)
        {
            this.TradeID = row["TRADEID"].ToString();
            this.HomeDomain = row["SYNCSYSCODE"].ToString();
            this.SIGN = "9000000001";
            this.TradeTypeCode = GetTradetypecode(row["TRADETYPECODE"].ToString());
            this.CardNO = row["CARDNO"].ToString();
            this.PaperTypeCode = row["PAPERTYPECODE"].ToString();
            this.PaperNo = row["PAPERNO"].ToString();
            this.REASON = row["REASON"].ToString();
            this.DEPTNO = "BKLR";
            this.OPERNO = "BKLR01";
        }

    }
}