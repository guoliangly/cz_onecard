﻿

namespace DataExchange
{
    using System.Data;
    using System.Text;

    /// <summary>
    ///LossCardSync 的摘要说明
    /// </summary>
    public class LossCardSync : SyncRequest
    {
        public LossCardSync()
        {
            this.bipCode = "0002"; //挂失解挂口挂同步接口
        }

        //操作类型
        private string tradeTypeCode;
        public string TradeTypeCode
        {
            get { return tradeTypeCode; }
            set { tradeTypeCode = value; }
        }

        //市民卡卡号
        private string cardNO;
        public string CardNO
        {
            get { return cardNO; }
            set { cardNO = value; }
        }

        //银行卡号
        private string bankCardNo;
        public string BankCardNo
        {
            get { return bankCardNo; }
            set { bankCardNo = value; }
        }

        //证件类型
        private string paperTypeCode;
        public string PaperTypeCode
        {
            get { return paperTypeCode; }
            set { paperTypeCode = value; }
        }


        //证件号码
        private string paperNo;
        public string PaperNo
        {
            get { return paperNo; }
            set { paperNo = value; }
        }

        //挂失原因
        private string reason;
        public string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        //网点编码
        private string deptNo;
        public string DeptNo
        {
            get { return deptNo; }
            set { deptNo = value; }
        }

        //操作员编码
        private string operNo;
        public string OperNo
        {
            get { return operNo; }
            set { operNo = value; }
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>请求XML</returns>
        public override string SetupXML()
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<BLOCKREQ>");
            xml.AppendLine(string.Format("<TRADETYPECODE>{0}</TRADETYPECODE>", this.tradeTypeCode));
            xml.AppendLine(string.Format("<CARDNO>{0}</CARDNO>", this.cardNO));
            xml.AppendLine(string.Format("<BANKCARDNO>{0}</BANKCARDNO>", this.bankCardNo));
            xml.AppendLine(string.Format("<PAPERTYPECODE>{0}</PAPERTYPECODE>", this.paperTypeCode));
            xml.AppendLine(string.Format("<PAPERNO>{0}</PAPERNO>", this.paperNo));
            xml.AppendLine(string.Format("<REASON>{0}</REASON>", this.reason));
            xml.AppendLine(string.Format("<DEPTNO>{0}</DEPTNO>", this.deptNo));
            xml.AppendLine(string.Format("<OPERNO>{0}</OPERNO>", this.operNo));
            xml.AppendLine("</BLOCKREQ>");
            return xml.ToString();
        }

        /// <summary>
        /// DataRow转换
        /// </summary>
        /// <param name="row">DataRow</param>
        public override void ParseFormDataRow(DataRow row)
        {
            this.TradeID = row["TRADEID"].ToString();
            this.HomeDomain = row["SYNCSYSCODE"].ToString();
            this.TradeTypeCode = "00";
            this.CardNO = row["CARDNO"].ToString();
            this.BankCardNo = row["BANKCARDNO"].ToString();
            this.PaperTypeCode = row["PAPERTYPECODE"].ToString();
            this.PaperNo = row["PAPERNO"].ToString();
            this.Reason = row["REASON"].ToString();
            this.DeptNo = row["OPERATEDEPARTNO"].ToString();
            this.OperNo = row["OPERATESTAFFNO"].ToString();
        }
    }

}