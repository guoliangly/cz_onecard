﻿//-----------------------------------------------------------------------
// <copyright file="BikeOpenSync.cs" company="linkage">
//   * 功能名: 自行车开通。
// </copyright>
// <author>闵建仁</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2014/06/11    王定喜         初次开发   
//-----------------------------------------------------------------------

namespace DataExchange
{
    using System.Data;
    using System.Text;

    /// <summary>
    /// 自行车开通请求类
    /// </summary>
    public class BikeCloseSync : SyncBikeRequest
    {
        /// <summary>
        /// 构造函数<see cref="BikeOpenSync"/>.
        /// </summary>
        public BikeCloseSync()
        {
            this.bipCode = "0001"; //自行车开通同步接口
        }

        //操作类型
        private string tradeTypeCode;
        public string TradeTypeCode
        {
            get { return tradeTypeCode; }
            set { tradeTypeCode = value; }
        }

        //市民卡卡号
        private string cardNO;
        public string CardNO
        {
            get { return cardNO; }
            set { cardNO = value; }
        }

        //姓名

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        //证件类型
        private string paperTypeCode;
        public string PaperTypeCode
        {
            get { return paperTypeCode; }
            set { paperTypeCode = value; }
        }


        //证件号码
        private string paperNo;
        public string PaperNo
        {
            get { return paperNo; }
            set { paperNo = value; }
        }

        //出生日期
        private string birth;
        public string BIRTH
        {
            get { return birth; }
            set { birth = value; }
        }
        //性别
        private string sex;
        public string SEX
        {
            get { return sex; }
            set { sex = value; }
        }
        //电话
        private string phone;
        public string PHONE
        {
            get { return phone; }
            set { phone = value; }
        }
        //邮政编码
        private string custpost;
        public string CUSTPOST
        {
            get { return custpost; }
            set { custpost = value; }
        }
        //联系地址
        private string addr;
        public string ADDR
        {
            get { return addr; }
            set { addr = value; }
        }
        //邮箱
        private string email;
        public string EMAIL
        {
            get { return email; }
            set { email = value; }
        }


        //网点编号
        private string deptno;
        public string DEPTNO
        {
            get { return deptno; }
            set { deptno = value; }
        }
        //操作员编号
        private string operno;
        public string OPERNO
        {
            get { return operno; }
            set { operno = value; }
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>
        /// 请求XML
        /// </returns>
        public override string SetupXML()
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<REQ>");
            xml.AppendLine(string.Format("<TRADETYPECODE>{0}</TRADETYPECODE>", this.tradeTypeCode));
            xml.AppendLine(string.Format("<CARDNO>{0}</CARDNO>", this.cardNO));
            xml.AppendLine(string.Format("<NAME>{0}</NAME>", this.name));

            xml.AppendLine(string.Format("<PAPERTYPECODE>{0}</PAPERTYPECODE>", this.paperTypeCode));
            xml.AppendLine(string.Format("<PAPERNO>{0}</PAPERNO>", this.paperNo));
            xml.AppendLine(string.Format("<BIRTH>{0}</BIRTH>", this.birth));
            xml.AppendLine(string.Format("<SEX>{0}</SEX>", this.sex));
            xml.AppendLine(string.Format("<PHONE>{0}</PHONE>", this.phone));
            xml.AppendLine(string.Format("<CUSTPOST>{0}</CUSTPOST>", this.custpost));
            xml.AppendLine(string.Format("<ADDR>{0}</ADDR>", this.addr));
            xml.AppendLine(string.Format("<EMAIL>{0}</EMAIL>", this.email));
            xml.AppendLine(string.Format("<DEPTNO>{0}</DEPTNO>", this.deptno));
            xml.AppendLine(string.Format("<OPERNO>{0}</OPERNO>", this.operno));
            xml.AppendLine("</REQ>");
            return xml.ToString();
        }

        /// <summary>
        /// DataRow转换
        /// </summary>
        /// <param name="row">DataRow</param>
        public override void ParseFormDataRow(DataRow row)
        {
            this.TradeID = row["TRADEID"].ToString();
            this.HomeDomain = row["SYNCSYSCODE"].ToString();
            this.SIGN = "9000000001";
            this.TradeTypeCode = "02";//关闭
            this.CardNO = row["CARDNO"].ToString();
            this.Name = row["NAME"].ToString();
            this.PaperTypeCode = row["PAPERTYPECODE"].ToString();
            this.PaperNo = row["PAPERNO"].ToString();
            this.BIRTH = row["BIRTH"].ToString();
            this.SEX = row["SEX"].ToString();
            this.PHONE = row["PHONE"].ToString();
            this.CUSTPOST = row["CUSTPOST"].ToString();
            this.ADDR = row["ADDR"].ToString();
            this.EMAIL = row["EMAIL"].ToString();
            this.DEPTNO = "BKLR";
            this.OPERNO = "BKLR01";
        }
    }
}