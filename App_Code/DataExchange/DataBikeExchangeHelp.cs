﻿//-----------------------------------------------------------------------
// <copyright file="DataExchangeHelp.cs" company="linkage">
//   * 功能名: 同步帮助类

// </copyright>
// <author>闵建仁</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2013/08/06    闵建仁          初次开发   

//-----------------------------------------------------------------------
namespace DataExchange
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using CenterProess;

    /// <summary>
    /// 同步帮助类  
    /// </summary>
    public static class DataBikeExchangeHelp
    {
        /// <summary>
        /// 同步请求
        /// </summary>
        /// <param name="SyncBikeRequest">同步请求集合 </param>
        /// <param name="syncResponse">同步响应集合 </param>
        /// <returns>同步是否成功 </returns>
        public static bool Sync(List<SyncBikeRequest> SyncBikeRequest, out List<SyncBikeRequest> syncResponse)
        {
            syncResponse = new List<SyncBikeRequest>();
            // 遍历请求，把请求按业务流水号 + 卡号分组
            Dictionary<string, List<SyncBikeRequest>> dictSync = GroupRequest(SyncBikeRequest);
            // 遍历分组，分别调用接口

            foreach (KeyValuePair<string, List<SyncBikeRequest>> kvp in dictSync)
            {
                List<SyncBikeRequest> tmpSync = kvp.Value;
                if (tmpSync.Count > 0)
                {
                    BikeWebservice.IuBikeWebSvrservice bikewebservice = new BikeWebservice.IuBikeWebSvrservice();
                    // 调用接口
                    bikewebservice.Url = System.Web.Configuration.WebConfigurationManager.AppSettings["BikeWebservice.IuBikeWebSvrservice"];
                    bikewebservice.Timeout = 30000;
                    string rsp = string.Empty;
                    try
                    {
                        rsp = bikewebservice.CITIZENBIKE(SetupXML(tmpSync));
                    }
                    catch (Exception ex)
                    {
                        rsp = "Exception: " + ex.Message;
                    }
                    Common.Log.Error("调用接口请求报文:" + SetupXML(tmpSync), null, "AppLog");
                    Common.Log.Error("调用接口响应报文:" + rsp, null, "AppLog");
                    // 分析及处理返回XML
                    syncResponse.AddRange(AnalyticXML(false, rsp, tmpSync[0].CardNO, tmpSync[0].TradeID, tmpSync));
                }
            }

            bool result = true;
            int succ = 0;
            foreach (SyncBikeRequest rsp in syncResponse)
            {
                if (rsp.SyncCode == "1")
                {
                    succ++;
                }
            }
            if (succ != syncResponse.Count)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 多个请求后台同步
        /// </summary>
        /// <param name="SyncBikeRequest">同步请求集合</param>
        public static void Async(List<SyncBikeRequest> SyncBikeRequest)
        {
            ThreadPool.QueueUserWorkItem(Sync, SyncBikeRequest);
        }

        /// <summary>
        /// DataTable转换
        /// </summary>
        /// <param name="dt">The DataTable.</param>
        /// <param name="SyncBikeRequest">同步请求集合</param>
        /// <returns>是否转换成功</returns>
        public static bool ParseFormDataTable(DataTable dt, out List<SyncBikeRequest> SyncBikeRequest)
        {
            return ParseFormDataTable(dt, "", out SyncBikeRequest);
        }



        /// <summary>
        /// 调用借还车查询DataTable转换
        /// </summary>
        /// <param name="dt">In DataTable</param>
        /// <param name="tradeID">任务编号</param>
        /// <param name="synRequest">转换结果集</param>
        /// <returns>成功标识位</returns>
        public static bool BikeVerifyForDataTable(DataTable dt, out List<SyncBikeRequest> syncRequest)
        {
            bool result = true;
            SyncBikeRequest req;
            syncRequest = new List<SyncBikeRequest>();
            if (dt.Rows.Count == 1)
            {
                try
                {
                    req = new BikeVerifySync();
                    req.ParseFormDataRow(dt.Rows[0]);
                    syncRequest.Add(req);
                }
                catch (ArgumentException ex)
                {
                    Common.Log.Error("调用接口转换错误", ex, "AppLog");
                    result = false;
                }
                catch (Exception ex)
                {
                    result = false;
                    throw ex;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }


        /// <summary>
        /// 调用借还车查询DataTable转换
        /// </summary>
        /// <param name="dt">In DataTable</param>
        /// <param name="tradeID">任务编号</param>
        /// <param name="synRequest">转换结果集</param>
        /// <returns>成功标识位</returns>
        public static bool BikeStateQueryForDataTable(DataTable dt, out List<SyncBikeRequest> syncRequest)
        {
            bool result = true;
            SyncBikeRequest req;
            syncRequest = new List<SyncBikeRequest>();
            if (dt.Rows.Count == 1)
            {
                try
                {
                    req = new BikeStateQuerySync();
                    req.ParseFormDataRow(dt.Rows[0]);
                    syncRequest.Add(req);
                }
                catch (ArgumentException ex)
                {
                    Common.Log.Error("调用接口转换错误", ex, "AppLog");
                    result = false;
                }
                catch (Exception ex)
                {
                    result = false;
                    throw ex;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

      

        /// <summary>
        /// 调用借还车查询DataTable转换
        /// </summary>
        /// <param name="dt">In DataTable</param>
        /// <param name="tradeID">任务编号</param>
        /// <param name="synRequest">转换结果集</param>
        /// <returns>成功标识位</returns>
        public static bool ParseQueryForTradeList(DataTable dt, out List<SyncBikeRequest> syncRequest)
        {
            bool result = true;
            SyncBikeRequest req;
            syncRequest = new List<SyncBikeRequest>();
            if (dt.Rows.Count == 1)
            {
                try
                {
                    req = new QueryForTradeListSync();
                    req.ParseFormDataRow(dt.Rows[0]);
                    syncRequest.Add(req);
                }
                catch (ArgumentException ex)
                {
                    Common.Log.Error("调用接口转换错误", ex, "AppLog");
                    result = false;
                }
                catch (Exception ex)
                {
                    result = false;
                    throw ex;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// DataTable转换
        /// </summary>
        /// <param name="dt">The DataTable.</param>
        /// <param name="batchID">任务编号</param>
        /// <param name="SyncBikeRequest">同步请求集合</param>
        /// <returns>是否转换成功</returns>
        public static bool ParseFormDataTable(DataTable dt, string batchID, out List<SyncBikeRequest> SyncBikeRequest)
        {
            SyncBikeRequest = new List<SyncBikeRequest>();
            bool result = true;
            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    SyncBikeRequest req;
                    if (row["TRADETYPECODE"].ToString() == "0D")//龙人自行车开通
                    {
                        // 数据收集
                        req = new BikeOpenSync();
                        req.ParseFormDataRow(row);
                        req.BatchID = batchID;
                        SyncBikeRequest.Add(req);
                    }
                    else if (row["TRADETYPECODE"].ToString() == "D0")//龙人自行车关闭
                    {
                        // 数据收集
                        req = new BikeCloseSync();
                        req.ParseFormDataRow(row);
                        req.BatchID = batchID;
                        SyncBikeRequest.Add(req);
                    }
                    else if (row["TRADETYPECODE"].ToString() == "0C")//龙人自行车换卡
                    {
                        // 数据收集
                        req = new BikeChangeSync();
                        req.ParseFormDataRow(row);
                        req.BatchID = batchID;
                        SyncBikeRequest.Add(req);
                    }
                    else if (row["TRADETYPECODE"].ToString() == "0E" || row["TRADETYPECODE"].ToString() == "08" || row["TRADETYPECODE"].ToString() == "09")//口挂/书挂/解挂
                    {
                        // 数据收集
                        req = new BikeReportlossSync();
                        req.ParseFormDataRow(row);
                        req.BatchID = batchID;
                        SyncBikeRequest.Add(req);
                    }
                 
                }
                catch (ArgumentException ex)
                {
                    Common.Log.Error("调用接口转换错误", ex, "AppLog");
                    result = false;
                    break;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }


       

        /// <summary>
        /// 多个请求后台同步
        /// </summary>
        /// <param name="obj">The obj.</param>
        private static void Sync(object obj)
        {
            lock (obj)
            {
                List<SyncBikeRequest> SyncBikeRequest = (List<SyncBikeRequest>)obj;
                int succNum = 0, errNum = 0;

                // 遍历请求，把请求按业务流水号 + 卡号分组
                Dictionary<string, List<SyncBikeRequest>> dictSync = GroupRequest(SyncBikeRequest);
                // 遍历分组，分别调用接口

                foreach (KeyValuePair<string, List<SyncBikeRequest>> kvp in dictSync)
                {
                    List<SyncBikeRequest> tmpSync = kvp.Value;
                    if (tmpSync.Count > 0)
                    {
                        CenterProcess centerProcess = new CenterProcess();
                        centerProcess.Url = System.Web.Configuration.WebConfigurationManager.AppSettings["centerProessURI"];
                        centerProcess.Timeout = 30000;


                        // 调用接口
                        string rsp = string.Empty;
                        try
                        {
                            rsp = centerProcess.callback(SetupXML(tmpSync));
                        }
                        catch (Exception ex)
                        {
                            rsp = "Exception: " + ex.Message;
                        }
                        Common.Log.Error("调用接口" + SetupXML(tmpSync), null, "AppLog");
                        Common.Log.Error("调用接口" + rsp, null, "AppLog");
                        // 分析及处理返回XML
                        List<SyncBikeRequest> syncResponse = AnalyticXML(true, rsp, tmpSync[0].CardNO, tmpSync[0].TradeID, tmpSync);
                        foreach (SyncBikeRequest response in syncResponse)
                        {
                            if (response.SyncCode == "1")
                            {
                                succNum++;
                            }
                            else
                            {
                                errNum++;
                            }
                        }
                    }
                }
                //更新批量接口任务表

                UpdateBatchAsync(SyncBikeRequest[0].BatchID, succNum, errNum);
            }
        }

        /// <summary>
        /// 遍历请求，把请求按业务流水号 + 卡号分组
        /// </summary>
        /// <param name="SyncBikeRequest">The sync request.</param>
        /// <returns>分组后的请求集合</returns>
        private static Dictionary<string, List<SyncBikeRequest>> GroupRequest(List<SyncBikeRequest> SyncBikeRequest)
        {
            Dictionary<string, List<SyncBikeRequest>> dictSync = new Dictionary<string, List<SyncBikeRequest>>();
            foreach (SyncBikeRequest req in SyncBikeRequest)
            {
                if (!dictSync.ContainsKey(req.TradeID + req.CardNO))
                {
                    List<SyncBikeRequest> tmpSync = new List<SyncBikeRequest>();
                    tmpSync.Add(req);
                    dictSync.Add(req.TradeID + req.CardNO, tmpSync);
                }
                else
                {
                    List<SyncBikeRequest> tmpSync = new List<SyncBikeRequest>();
                    dictSync.TryGetValue(req.TradeID + req.CardNO, out tmpSync);
                    tmpSync.Add(req);
                    dictSync.Remove(req.TradeID + req.CardNO);
                    dictSync.Add(req.TradeID + req.CardNO, tmpSync);
                }
            }

            return dictSync;
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <param name="listSync">The list sync.</param>
        /// <returns>请求的XML</returns>
        private static string SetupXML(List<SyncBikeRequest> listSync)
        {
            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<SVC>");
            xml.AppendLine("<SVCHEAD>");
            xml.AppendLine("<ORIGDOMAIN>0001</ORIGDOMAIN>");
            foreach (SyncBikeRequest req in listSync)
            {
                xml.AppendLine(string.Format("<HOMEDOMAIN>{0}</HOMEDOMAIN>", req.HomeDomain));
            }

            xml.AppendLine(string.Format("<BIPCODE>{0}</BIPCODE>", listSync[0].BIPCode));
            xml.AppendLine("<ACTIONCODE>0</ACTIONCODE>");
            //xml.AppendLine(string.Format("<TRANSIDO>{0}</TRANSIDO>", GetTRANSIDO()));
            xml.AppendLine(string.Format("<TRANSIDO>{0}</TRANSIDO>", listSync[0].TradeID));
            xml.AppendLine(string.Format("<PROCID>{0}</PROCID>", listSync[0].TradeID));
            xml.AppendLine(string.Format("<SIGN>{0}</SIGN>", listSync[0].SIGN));
            xml.AppendLine(string.Format("<PROCESSTIME>{0}</PROCESSTIME>", DateTime.Now.ToString("yyyyMMddHHmmss")));
            xml.AppendLine("</SVCHEAD>");
            xml.AppendLine("<SVCCONT>");
            xml.Append(listSync[0].SetupXML());
            xml.AppendLine("</SVCCONT>");
            xml.AppendLine("</SVC>");
            XmlDocument dom = new XmlDocument();
            dom.LoadXml(xml.ToString());
            byte[] utf8Buf = Encoding.UTF8.GetBytes(xml.ToString());
            byte[] gbkBuf = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("gb2312"), utf8Buf);
            string strGB2312 = Encoding.GetEncoding("gb2312").GetString(gbkBuf);
            return strGB2312;
        }

        /// <summary>
        /// 分析结果XML
        /// </summary>
        /// <param name="isAsync">是否为异步请求.</param>
        /// <param name="xml">响应XML.</param>
        /// <param name="cardno">卡号.</param>
        /// <param name="tradeID">流水号.</param>
        /// <param name="requestList">同步请求集合.</param>
        /// <returns>响应结果集合结果</returns>
        private static List<SyncBikeRequest> AnalyticXML(bool isAsync, string xml, string cardno, string tradeID, List<SyncBikeRequest> requestList)
        {
            List<SyncBikeRequest> result = new List<SyncBikeRequest>();
            string homeDomain, respCode, respDesc, syncTradeID;
            try
            {
                XmlDocument dom = new XmlDocument();
                dom.LoadXml(xml);
                XmlNodeList xnList = dom.SelectNodes(@"//BIPCODE");
                string bipCode = string.Empty;
                if (xnList.Count == 1)
                {
                    bipCode = xnList[0].InnerText;
                    xnList = dom.SelectNodes(@"//RENTQUERYRSP");
                    if (xnList==null || xnList.Count == 0)
                    {
                        xnList = dom.SelectNodes(@"//RSP");
                    }
                    if (xnList == null || xnList.Count == 0)
                    {
                        xnList = dom.SelectNodes(@"//BLOCKRSP");
                    }
                    if (xnList == null || xnList.Count == 0)
                    {
                        xnList = dom.SelectNodes(@"//CHANGECARDRSP");
                    }

                    if (xnList == null || xnList.Count == 0)
                    {
                        xnList = dom.SelectNodes(@"//STATEQUERYRSP");
                    }
                    foreach (XmlNode node in xnList)
                    {
                        homeDomain = "";
                        syncTradeID = "";
                        respCode = "";
                        respDesc = "";
                        XmlNodeList rsp = node.ChildNodes;
                        foreach (XmlNode n in rsp)
                        {
                            if (n.Name == "HOMEDOMAIN")
                            {
                                homeDomain = n.InnerText;
                            }
                            else if (n.Name == "TRANSIDH")
                            {
                                syncTradeID = n.InnerText;
                            }
                            else if (n.Name == "RESPCODE")
                            {
                                respCode = n.InnerText;
                            }
                            else if (n.Name == "RESPDESC")
                            {
                                respDesc = n.InnerText;
                            }
                        }
                        SyncBikeRequest request = requestList.Find(delegate(SyncBikeRequest r) { return r.HomeDomain == homeDomain; });
                        if (request != null)
                        {
                            if (respCode == "0000")
                            {
                                request.SyncCode = "1";
                                request.SyncTime = DateTime.Now;
                                request.GetSyncResponse(rsp);
                                result.Add(request);
                            }
                            else
                            {
                                request.SyncCode = "2";
                                request.SyncErrInfo = respDesc;
                                request.SyncTime = DateTime.Now;
                                result.Add(request);
                            }
                        }
                        else
                        {
                            throw new Exception("Exception:归属域错误");
                        }
                    }
                }
                else
                {
                    //// 请求报文格式错误
                    xnList = dom.SelectNodes(@"//RSP");
                    respCode = xnList[0].ChildNodes.Item(2).InnerText;
                    respDesc = xnList[0].ChildNodes.Item(3).InnerText;
                    foreach (SyncBikeRequest request in requestList)
                    {
                        request.SyncCode = "2";
                        request.SyncErrInfo = respDesc;
                        request.SyncTime = DateTime.Now;
                        result.Add(request);
                    }
                }
            }
            //// 未知异常捕获
            catch (Exception ex)
            {
                //// 异常信息
                string exceptionMsg = string.Empty;
                if (xml.StartsWith("Exception:"))
                {
                    exceptionMsg = xml;
                }
                else
                {
                    exceptionMsg = ex.Message;
                }
                foreach (SyncBikeRequest request in requestList)
                {
                    request.SyncCode = "2";
                    request.SyncErrInfo = exceptionMsg;
                    request.SyncTime = DateTime.Now;
                    result.Add(request);
                }
            }
            return result;
        }

        /// <summary>
        /// 更新同步台帐子表  数据量大的情况下，考虑改为批量同步
        /// </summary>
        /// <param name="tableType">操作表类型.</param>
        /// <param name="tradeID">流水号.</param>
        /// <param name="cardNo">卡号</param>
        /// <param name="syncSysCode">同步系统编码.</param>
        /// <param name="state">同步状态.</param>
        /// <param name="errorDesc">同步异常信息.</param>
        /// <param name="syncSysTradeID">响应流水号</param>
        /// <returns>更新是否成功</returns>
        private static bool UpdateSyncState(string tableType, string tradeID, string cardNo, string syncSysCode, string state, string errorDesc, string syncSysTradeID)
        {
            Master.DBConnection dBConnection = new Master.DBConnection();
            dBConnection.Open("StorePro");
            dBConnection.AddDBParameter("String", "P_TABLETYPE", tableType, "1", "input");
            dBConnection.AddDBParameter("String", "P_TRADEID", tradeID, "16", "input");
            dBConnection.AddDBParameter("String", "P_CARDNO", cardNo, "16", "input");
            dBConnection.AddDBParameter("String", "P_SYNCSYSCODE", syncSysCode, "20", "input");
            dBConnection.AddDBParameter("String", "P_STATE", state, "16", "input");
            dBConnection.AddDBParameter("String", "P_ERROR", errorDesc, "16", "input");
            dBConnection.AddDBParameter("String", "P_SYSCSYSTRADEID", syncSysTradeID, "30", "input");
            dBConnection.AddDBParameter("String", "p_retCode", syncSysCode, "10", "output");
            dBConnection.AddDBParameter("String", "p_retMsg", syncSysCode, "127", "output");
            dBConnection.ExecuteReader("SP_DE_UPDATEBIKESYNCSTATE");
            string retCode = dBConnection.GetParameterValue("p_retCode").ToString();
            string error = dBConnection.GetParameterValue("p_retMsg").ToString();
            dBConnection.Commit();
            dBConnection.Close();
            if (retCode != "0000000000")
            {
                Common.Log.Error(retCode + ":" + error + " P_TRADEID: " + tradeID + " P_CARDNO:" + cardNo, null, "AppLog");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 批量接口任务表

        /// </summary>
        /// <param name="batchID">任务编号.</param>
        /// <param name="succNum">成功条数.</param>
        /// <param name="errorNum">失败条数.</param>
        /// <returns>更新是否成功</returns>
        private static bool UpdateBatchAsync(string batchID, int succNum, int errorNum)
        {
            Master.DBConnection dBConnection = new Master.DBConnection();
            dBConnection.Open("StorePro");
            dBConnection.AddDBParameter("String", "P_TABLETYPE", "3", "1", "input");
            dBConnection.AddDBParameter("Int32", "P_BATCH_ID", batchID, "", "input");
            dBConnection.AddDBParameter("Int32", "P_SUCC_NUM", succNum.ToString(), "", "input");
            dBConnection.AddDBParameter("Int32", "P_FAIL_NUM", errorNum.ToString(), "", "input");
            dBConnection.AddDBParameter("String", "p_retCode", "", "10", "output");
            dBConnection.AddDBParameter("String", "p_retMsg", "", "127", "output");
            dBConnection.ExecuteReader("SP_DE_UPDATESYNCSTATE");
            string retCode = dBConnection.GetParameterValue("p_retCode").ToString();
            string error = dBConnection.GetParameterValue("p_retMsg").ToString();
            dBConnection.Commit();
            dBConnection.Close();
            if (retCode != "0000000000")
            {
                Common.Log.Error(retCode + ":" + error, null, "AppLog");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 获取交易流水号


        /// </summary>
        /// <returns>交易流水号</returns>
        private static string GetTRANSIDO()
        {
            Master.DBConnection dBConnection = new Master.DBConnection();
            dBConnection.Open("StorePro");
            dBConnection.AddDBParameter("String", "SEQ", "", "16", "output");
            dBConnection.ExecuteReader("SP_GETSEQ");
            string procID = dBConnection.GetParameterValue("SEQ").ToString();
            dBConnection.Commit();
            dBConnection.Close();
            return procID;
        }

        /// <summary>
        /// 同步方名称

        /// </summary>
        /// <param name="homeDomain">同步方代码</param>
        /// <returns>同步方名称</returns>
        public static string GetHomeDomainName(string homeDomain)
        {
            string result = string.Empty;
            switch (homeDomain)
            {
                case "0001": result = "市民卡公司";
                    break;
                case "BKLR": result = "龙人自行车";
                    break;
            }

            return result;
        }
    }
}