﻿//-----------------------------------------------------------------------
// <copyright file="SyncBikeRequest.cs" company="linkage">
//   * 功能名: 自行车同步请求类

// </copyright>
// <author>王定喜</author>
//   * 更改日期      姓名           摘要 
//   * ----------    -----------    --------------------------------
//   * 2014/06/13    王定喜         初次开发  

//-----------------------------------------------------------------------
namespace DataExchange
{
    using System;
    using System.Data;
    using System.Xml;

    /// <summary>
    /// 同步请求类

    /// </summary>
    public abstract class SyncBikeRequest
    {
        /// <summary>
        /// 业务功能代码
        /// </summary>
        protected string bipCode;
        public string BIPCode
        {
            get { return this.bipCode; }
        }

        /// <summary>
        /// 应答方应用域代码
        /// </summary>
        private string homeDomain;
        public string HomeDomain
        {
            get { return this.homeDomain; }
            set { this.homeDomain = value; }
        }

        /// <summary>
        /// 业务流水号

        /// </summary>
        private string tradeID;
        public string TradeID
        {
            get { return this.tradeID; }
            set { this.tradeID = value; }
        }

        /// <summary>
        /// 认证串


        /// </summary>
        private string sign;
        public string SIGN
        {
            get { return this.sign; }
            set { this.sign = value; }
        }

        /// <summary>
        /// 同步状态

        /// </summary>
        private string syncCode;
        public string SyncCode
        {
            get { return this.syncCode; }
            set { this.syncCode = value; }
        }

        /// <summary>
        /// 同步异常信息
        /// </summary>
        private string syncErrInfo;
        public string SyncErrInfo
        {
            get { return this.syncErrInfo; }
            set { this.syncErrInfo = value; }
        }

        /// <summary>
        /// 同步完成时间
        /// </summary>
        private DateTime syncTime;
        public DateTime SyncTime
        {
            get { return this.syncTime; }
            set { this.syncTime = value; }
        }

        /// <summary>
        /// 卡号
        /// </summary>
        private string cardNO;
        public string CardNO
        {
            get { return this.cardNO; }
            set { this.cardNO = value; }
        }

        /// <summary>
        /// 任务编号
        /// </summary>
        private string batchID;
        public string BatchID
        {
            get { return this.batchID; }
            set { this.batchID = value; }
        }

        /// <summary>
        /// 构造请求XML
        /// </summary>
        /// <returns>请求XML</returns>
        public abstract string SetupXML();

        /// <summary>
        /// DataRow转换.
        /// </summary>
        /// <param name="dr">DataRow</param>
        public abstract void ParseFormDataRow(DataRow dr);

        /// <summary>
        /// 获取返回参数.
        /// </summary>
        /// <param name="rsp">XML</param>
        public virtual void GetSyncResponse(XmlNodeList rsp)
        {

        }

        //业务类型转换
        public string GetTradetypecode(string tradetypecode)
        {
            string result = string.Empty;
            switch (tradetypecode)
            {
                case "0E": result = "00";
                    break;
                case "08": result = "01";
                    break;
                case "09":
                    result = "02";
                    break;

            }
            return result;

        }
    }
}