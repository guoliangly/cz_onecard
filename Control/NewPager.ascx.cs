﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
namespace Onecard.Control
{
    public delegate void NewPageClickEventHandler(object sender, EventArgs e);

    public partial class NewPager : System.Web.UI.UserControl
    {
        public event NewPageClickEventHandler PreClick;
        public event NewPageClickEventHandler NextClick;
        public event NewPageClickEventHandler FirstClick;
        public event NewPageClickEventHandler LastClick;
        public event NewPageClickEventHandler GoPageClick;

        public static int m_RecordCount;
        public static int m_PageCount;
        public static int m_PageIndex;
        public static int m_PageSize;
        public static int m_GoPageIndex;
        public static Unit m_Width;

        public int PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }
        public int PageIndex
        {
            get { return m_PageIndex; }
            set { m_PageIndex = value; }
        }
        public int RecordCount
        {
            get { return m_RecordCount; }
            set { m_RecordCount = value; }
        }
        public int PageCount
        {
            get { return m_PageCount; }
            set { m_PageCount = value; }
        }

        public Unit Width
        {
            get { return m_Width; }
            set { m_Width = value; }
        }

        /// <summary>
        /// 控制导航按钮或数字的状态
        /// </summary>
        public void SetPagingState()
        {
            if (PageCount <= 1)//( RecordCount <= PageSize )//小于等于一页
            {
                this.LBtnFirst.Visible = false; this.ltlFirst.Visible = true;
                this.LBtnPrev.Visible = false; this.ltlPre.Visible = true;
                this.LBtnNext.Visible = false; this.ltlNext.Visible = true;
                this.LBtnLast.Visible = false; this.ltlLast.Visible = true;
            }
            else //有多页
            {
                if (PageIndex == 0)//当前为第一页
                {
                    this.LBtnFirst.Visible = false; this.ltlFirst.Visible = true;
                    this.LBtnPrev.Visible = false; this.ltlPre.Visible = true;
                    this.LBtnNext.Visible = true; this.ltlNext.Visible = false;
                    this.LBtnLast.Visible = true; this.ltlLast.Visible = false;
                }
                else if (PageIndex == PageCount - 1)//当前为最后页 
                {
                    this.LBtnFirst.Visible = true; this.ltlFirst.Visible = false;
                    this.LBtnPrev.Visible = true; this.ltlPre.Visible = false;
                    this.LBtnNext.Visible = false; this.ltlNext.Visible = true;
                    this.LBtnLast.Visible = false; this.ltlLast.Visible = true;
                }
                else //中间页
                {
                    this.LBtnFirst.Visible = true; this.ltlFirst.Visible = false;
                    this.LBtnPrev.Visible = true; this.ltlPre.Visible = false;
                    this.LBtnNext.Visible = true; this.ltlNext.Visible = false;
                    this.LBtnLast.Visible = true; this.ltlLast.Visible = false;
                }
            }

            this.LtlPageSize.Text = PageSize.ToString();
            this.LtlRecordCount.Text = RecordCount.ToString();
            if (RecordCount == 0)
            {
                this.LtlPageCount.Text = "0";
                this.LtlPageIndex.Text = "0";
            }
            else
            {
                this.LtlPageCount.Text = PageCount.ToString();
                this.LtlPageIndex.Text = (PageIndex + 1).ToString();
            }

            ListBoxBind();
        }

        //设置listbox
        public void ListBoxBind()
        {
            txtPage.Text = Convert.ToString(PageIndex + 1);
        }

        protected void LBtnPrev_Click(object sender, System.EventArgs e)
        {
            if (PreClick != null)
                PreClick(this, e);
        }

        protected void LBtnLast_Click(object sender, System.EventArgs e)
        {
            if (LastClick != null)
                LastClick(this, e);
        }

        protected void LBtnNext_Click(object sender, System.EventArgs e)
        {
            if (NextClick != null)
                NextClick(this, e);
        }

        protected void LBtnFirst_Click(object sender, System.EventArgs e)
        {
            if (FirstClick != null)
                FirstClick(this, e);
        }

        protected void LBtnGoPage_Click(object sender, System.EventArgs e)
        {
            if (GoPageClick != null)
            {
                try
                {
                    PageIndex = int.Parse(txtPage.Text) - 1;
                    if (PageIndex > PageCount - 1)
                        PageIndex = PageCount - 1;
                    if (PageIndex < 0)
                        PageIndex = 0;
                }
                catch
                {
                    PageIndex = 0;
                }
                GoPageClick(this, e);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlPager.Width = Width;
        }
    }

}