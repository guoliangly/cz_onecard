﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewPager.ascx.cs" Inherits="Onecard.Control.NewPager" %>
<asp:Panel ID="pnlPager" Runat=server width="100%">
<TABLE id=Table15 cellSpacing=0 cellPadding=0 width="100%" align=center border=0 >
  <TR>
      <td align="center" colspan="2" style="height: 25px; width: 400px;" valign="baseline">          
        页次:
<font color="red"><b><asp:literal id=LtlPageIndex runat="server"></asp:literal></b></font> / <font color="black"><b><asp:literal id=LtlPageCount runat="server"></asp:literal></b></font>页&nbsp;每页:
<asp:literal id=LtlPageSize runat="server"></asp:literal> 记录:
<asp:literal id=LtlRecordCount runat="server"></asp:literal> </td>
    <TD align=right style="height: 25px; width: 250px;" valign="baseline" background="#F2F2F2">
<asp:linkbutton id=LBtnFirst runat="server" CommandName="First" OnClick="LBtnFirst_Click">首页</asp:linkbutton><asp:literal id=ltlFirst runat="server" Visible="False" Text="首页"></asp:literal>&nbsp;<asp:linkbutton id=LBtnPrev runat="server" CommandName="Prev" OnClick="LBtnPrev_Click">上页</asp:linkbutton><asp:literal id=ltlPre runat="server" Visible="False" Text="上页"></asp:literal>&nbsp;<asp:linkbutton id=LBtnNext runat="server" CommandName="Next" OnClick="LBtnNext_Click">下页</asp:linkbutton><asp:literal id=ltlNext runat="server" Visible="False" Text="下页"></asp:literal>&nbsp;<asp:linkbutton id=LBtnLast runat="server" CommandName="Last" OnClick="LBtnLast_Click">尾页</asp:linkbutton><asp:literal id=ltlLast runat="server" Visible="False" Text="尾页"></asp:literal>&nbsp;第<asp:TextBox
    ID="txtPage" runat="server" Width="30px" style="text-align:center" CssClass="inputText"></asp:TextBox>页&nbsp;<asp:Button ID="btnGoto" runat="server" Text="GO" OnClick="LBtnGoPage_Click"  CssClass="inputButton" /></TD></TR></TABLE>
</asp:Panel>
