﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM.UserManager;
using TDO.UserManager;
using Master;
using System.Drawing;
using Common;
using PDO.PrivilegePR;
using TM;
using System.Net;
using SSO;

public partial class Logontest : Master.MasterBase
{

    public override void ErrorMsgShow()
    {
        Msg.Text = "";
        ArrayList errorMessages = context.ErrorMessage;
        for (int index = 0; index < errorMessages.Count; index++)
        {
            if (index > 0)
                Msg.Text += "|";

            String error = errorMessages[index].ToString();
            int start = error.IndexOf(":");
            if(start > 0)
            {
                error = error.Substring(start + 1,error.Length - start - 1);
            }

            Msg.Text += error;
        }
    }

    /************************************************************************
     * 输入检查
     * @param
     * @return
     ************************************************************************/
    private Boolean InputCheck()
    {
        if (UserName.Text.Trim() == "")
        {
            context.AddError("A100001001", UserName);
        }

        if (UserPass.Text.Trim() == "")
            context.AddError("A100001002", UserPass);

        //卡有效性检查
        //context.AddError("A100001004");

        if (context.hasError())
            return false;
        else
            return true;
    }


    /************************************************************************
     * 对登录用户进行有效性判断，成功后向后续页面传递数据
     * @param
     * @return
     ************************************************************************/
    protected void LogonBtn_Click(object sender, EventArgs e)
    {
        if (!InputCheck())
            return;

        Session["STAFF"] = UserName.Text.Trim();
        // 更新令牌
        string token = Guid.NewGuid().ToString();
        bool ok = SSOHelper.UpdateToken(this.context, token, "TD_M_INSIDESTAFF", UserName.Text.Trim());
        if (ok)
        {
            Response.Redirect(string.Format("/LogonSSO.aspx?staff={0}&token={1}&type={2}", Session["STAFF"], token, "test"));
            return;
        }
        else
        {
            context.AddError("更新令牌失败，请联系管理员");
            return;
        }


        #region 注释
        ////从内部员工编码表中提取数据，并判断用户名、卡号、密码和有效性

        //TD_M_INSIDESTAFFTM tmTD_M_INSIDESTAFF = new TD_M_INSIDESTAFFTM();
        //TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        //TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFOut = null;

        //tdoTD_M_INSIDESTAFFIn.STAFFNO = UserName.Text.Trim();
        //tdoTD_M_INSIDESTAFFIn.OPERCARDNO = txtOperCardno.Text.Trim();
        //tdoTD_M_INSIDESTAFFIn.OPERCARDPWD = DecryptString.EncodeString(UserPass.Text.Trim());

        //tdoTD_M_INSIDESTAFFOut = tmTD_M_INSIDESTAFF.selByPK(context, tdoTD_M_INSIDESTAFFIn);
        //if (tdoTD_M_INSIDESTAFFOut == null)
        //    return;

        ////在内部员工登录限制表中判断是否有限制
        //TD_M_INSIDESTAFFLOGINTM tmTD_M_INSIDESTAFFLOGIN = new TD_M_INSIDESTAFFLOGINTM();
        //TD_M_INSIDESTAFFLOGINTDO tdoTD_M_INSIDESTAFFLOGINIn = new TD_M_INSIDESTAFFLOGINTDO();

        //tdoTD_M_INSIDESTAFFLOGINIn.STAFFNO = tdoTD_M_INSIDESTAFFIn.STAFFNO;
        //tdoTD_M_INSIDESTAFFLOGINIn.IPADDR = Request.UserHostAddress;
        //if (!tmTD_M_INSIDESTAFFLOGIN.isUsage(context, tdoTD_M_INSIDESTAFFLOGINIn))
        //    return;

        ////从内部部门表中读取数据

        //TD_M_INSIDEDEPARTTM tmTD_M_INSIDEDEPART = new TD_M_INSIDEDEPARTTM();
        //TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
        //tdoTD_M_INSIDEDEPARTIn.DEPARTNO = tdoTD_M_INSIDESTAFFOut.DEPARTNO;
        //TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTOut = tmTD_M_INSIDEDEPART.selByPK(context, tdoTD_M_INSIDEDEPARTIn);


        //context.s_UserID = tdoTD_M_INSIDESTAFFOut.STAFFNO;
        //context.s_DepartID = tdoTD_M_INSIDESTAFFOut.DEPARTNO;
        //SP_PR_LogonLogPDO pdo = new SP_PR_LogonLogPDO();
        //pdo.IPADDR = Request.UserHostAddress;
        //pdo.LOGONPAGE = "Logon.aspx";
        //pdo.OPERCARDNO = txtOperCardno.Text.Trim();

        //bool ok = TMStorePModule.Excute(context, pdo);

        //if (!ok)
        //{
        //    return;
        //}

        //SP_PR_RecordOnOffDutyPDO pdoSP_PR_OnOffDuty = new SP_PR_RecordOnOffDutyPDO();
        //pdoSP_PR_OnOffDuty.TRADETYPECODE = "01";

        //bool onDutyOk = TMStorePModule.Excute(context, pdoSP_PR_OnOffDuty);

        //if (!onDutyOk)
        //    return;

        ////回单广告
        //string sql = "select content from TD_M_HDAdvertise t where isshow='1'  order by sortno";
        //TM.TMTableModule tm = new TM.TMTableModule();
        //DataTable dt = tm.selByPKDataTable(context, sql, 0);
        //ArrayList al = new ArrayList();
        //foreach (DataRow dr in dt.Rows)
        //{
        //    al.Add(dr[0].ToString());
        //}
        //Session["HDADVertise"] = al;//保存回单广告的内容



        ////设定向后传递的数据
        //Hashtable hash = new Hashtable();
        //hash.Add("UserID", tdoTD_M_INSIDESTAFFOut.STAFFNO);
        //hash.Add("UserName", tdoTD_M_INSIDESTAFFOut.STAFFNAME);
        //hash.Add("DepartID", tdoTD_M_INSIDESTAFFOut.DEPARTNO);
        //hash.Add("DepartName", tdoTD_M_INSIDEDEPARTOut.DEPARTNAME);
        //hash.Add("CardID", tdoTD_M_INSIDESTAFFOut.OPERCARDNO);

        //String dtNow = DateTime.Now.Ticks.ToString();
        //String prePasswd = tdoTD_M_INSIDESTAFFIn.STAFFNO + dtNow;
        //String postPasswd = DecryptString.EncodeString(prePasswd);
        //hash.Add("DateTime", dtNow);
        //hash.Add("DecryptString", postPasswd);
        //hash.Add("Debugging", "True");
        //hash.Add("LogonLevel", "Normal");
        //hash.Add("City", selCity.SelectedValue);

        //Session["LogonInfo"] = hash;

        //Transfer("Default.aspx");
        #endregion
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["HDADVertise"] = null;
        Session["LogonInfo"] = null;
        LogonBtn.Focus();
        //LogonBtn_Click(sender, e);

    }
}
