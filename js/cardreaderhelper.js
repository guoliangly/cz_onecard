﻿
// alert('setup env');
var sourceWin = null;
var cardReader = null;

function setupEnv()
{
    var wo = window.opener;
    if (wo == null)
    {
        sourceWin = window.parent.frames[0];
    }
    else
    {
        sourceWin = wo.parent.frames[0];
    }
         
    cardReader = sourceWin.cardReader;
       
}

setupEnv();

function TargetWindowReadCard(showErr)
{
    return cardReader.readCard(window, showErr);
}


function CardRecord(tradeNo, tradeMoney, tradeType, tradeTerm, tradeDate,  tradeTime)
{
    this.tradeNo = tradeNo;
    this.tradeMoney = tradeMoney;
    this.tradeType = tradeType;
    this.tradeTerm = tradeTerm;
    this.tradeDate = tradeDate;
    this.tradeTime = tradeTime;
}

function CardInfo(cardNo, appType, appVersion, appSn, 
    appStartDate, appEndDate, FCI, cardType, tradeNo, balance, staffTag, wallet2)
{
    this.cardNo = cardNo;
    this.appType = appType;
    this.appVersion = appVersion;
    this.appSn = appSn;
    this.appStartDate = appStartDate;
    this.appEndDate = appEndDate;
    this.FCI = FCI;
    this.cardType = cardType;
    this.tradeNo = tradeNo;
    this.balance = balance;
    this.staffTag = staffTag;
    
    this.wallet2 = wallet2;
}

function IDCardInfo(name, sex, birth, addr, paperno) {
    this.name = name;
    this.sex = sex;
    this.birth = birth;
    this.addr = addr;
    this.paperno = paperno;
}

function IDCardInfo(name, sex, birth, addr, paperno, phone, post) {
    this.name = name;
    this.sex = sex;
    this.birth = birth;
    this.addr = addr;
    this.paperno = paperno;
    this.phone = phone;
    this.post = post;    
}

function inTestingMode(cardNoId) {
    if (!cardReader.Debugging) {
        cardReader.testingMode = false;
        return false;
    }

    var id = $get(cardNoId);
    if (id != null) {
        cardReader.testingMode = $get(cardNoId).value != ""
            && cardReader.Debugging;
    }
    else {
        cardReader.testingMode = cardReader.Debugging;
    }

    if (cardReader.testingMode && id != null) {
        cardReader.CardNo = $get(cardNoId).value;
    }

    return cardReader.testingMode;
}

function readCardRecord() {
    var record = null;
    var cardInfo = null;
    if (inTestingMode('txtCardno')) {
        record = new CardRecord('0000000000000000000000000000000000000000',
        '00000000000000000000000000000000000000000000000000000000000000000000000000000000',
        '00000000000000000000',
        '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
        '00000000000000000000000000000000000000000000000000000000000000000000000000000000',
        '000000000000000000000000000000000000000000000000000000000000'
        );
        cardInfo = new CardInfo($get('txtCardno').value, "02", "01", "00001122334455661000",
            "20020102", "20500101", "FCI", "01", "2222", 0, "FF", 1000);
        assignHiddenValues('txtCardno', cardInfo);
    }
    else {
        cardReader.readCardFunc = new Array(cardReader.getCardInfo, cardReader.getRecords);
        record = TargetWindowReadCard();

        if (record == null) return false;

        assignHiddenValues('txtCardno', cardReader.CardInfo);
    }
    
    assignValue('hidTradeno', record.tradeNo);
    assignValue('hidTradeMoney', record.tradeMoney);
    assignValue('hidTradeType', record.tradeType);
    assignValue('hidTradeTerm', record.tradeTerm);
    assignValue('hidTradeDate', record.tradeDate);
    assignValue('hidTradeTime', record.tradeTime);
    return true;
}  


function readIDCard(nameId, sexId, birthId, paperTypeId, papernoId, addrId)
{
    var idInfo = null;
    if (inTestingMode('txtCardno')) {
        idInfo = new IDCardInfo('wdx',
        '1',
        '19841221',
        '南京市',
        '321281111111111'
        );
    }
    else {
        cardReader.readCardFunc = cardReader.getIDCardInfo;
        idInfo = TargetWindowReadCard();
        if (idInfo == null) return null;
    }
    assignValue(nameId, idInfo.name);
    assignSexValue(sexId, idInfo);
    assignValue(birthId, idInfo.birth);
    assignValue(addrId, idInfo.addr);
    assignValue(paperTypeId, "00");
    assignValue(papernoId, idInfo.paperno);
    
    return idInfo;
}
//wdx
function readSingleID(papernoId)
{
	  cardReader.readCardFunc = cardReader.getIDCardInfo;
    var idInfo = TargetWindowReadCard();
    if (idInfo == null) return false;
    assignValue(papernoId, idInfo.paperno);
    return true;
}

function readIDCardEx(nameId, sexId, birthId, paperTypeId, papernoId, addrId,custPhone,custPost) {
    var idInfo = null;
    if (inTestingMode('txtCardno')) {
        idInfo = new IDCardInfo('wdx',
        '1',
        '19841221',
        '南京市',
        '321281111111111',
        '18949649971',
        '243000'
        );
    }
    else {
        cardReader.readCardFunc = cardReader.getIDCardInfo;
        idInfo = TargetWindowReadCard();
        if (idInfo == null) return false;
    }
    assignValue(nameId, idInfo.name);
    assignSexValue(sexId, idInfo);
    assignValue(birthId, idInfo.birth.substr(0,4) + '-' + idInfo.birth.substr(4, 2) + '-' + idInfo.birth.substr(6,2));
    assignValue(addrId, idInfo.addr);
    assignValue(paperTypeId, "00");
    assignValue(papernoId, idInfo.paperno);
    assignValue(custPhone, idInfo.phone);
    assignValue(custPost,idInfo.post);
    
    return true;
}
function readCardNo()
{
    if (inTestingMode('txtCardno'))
    {
        return true;
    }
    
    cardReader.readCardFunc = cardReader.getCardNo;
    var cardNo = TargetWindowReadCard();
    if (cardNo == null )
    {
        return false;
    }
    
    assignValue('txtCardno', cardNo);
    return true;
}



function readDriverRecord(hidCtrlId)
{
    if (inTestingMode('txtCardno'))
    {
        return true;
    }
    
    cardReader.readCardFunc = new Array(cardReader.getDriverRecord, cardReader.getCardNo);
    var cardNo = TargetWindowReadCard();
    if (cardNo == null )
    {
        return false;
    }
    
    assignValue('txtCardno', cardNo);
    assignValue(hidCtrlId, cardReader.DriverRecord);
        
    return true;
}

function readOperCardNo()
{
    if (cardReader.testingMode)
    {
        return '1234567890111002';
    }
    
    cardReader.readCardFunc = cardReader.getOperCardNo;
    var cardNo = TargetWindowReadCard();
    
    return cardNo;
}

function readDriverInfoEx(staffCtlId, cardNoId, taxiNoId, stateId, useTag)
{
    if (inTestingMode(cardNoId))
    {
        return true;
    }
    return readDriverInfoImpl(staffCtlId, cardNoId, taxiNoId, stateId, useTag) != null;
}

function readDriverInfo(staffCtlId, cardNoId, taxiNoId, stateId, useTag)
{
    if (inTestingMode(staffCtlId))
    {
        return true;
    }
    return readDriverInfoImpl(staffCtlId, cardNoId, taxiNoId, stateId, useTag) != null;
}

function readTelCard(telcardCtrl)
{
    cardReader.readCardFunc = new Array(cardReader.getCardNo, cardReader.getTelcard);
    var telcard = TargetWindowReadCard();
    if (telcard == null) return null;
    
    assignValue(telcardCtrl, telcard);
    
    return telcard;
}
     
function readDriverInfoImpl(staffCtlId, cardNoId, taxiNoId, stateId, useTag)
{
    cardReader.readCardFunc = cardReader.getDriverInfo;
    var driverInfo = TargetWindowReadCard();
    
    if (driverInfo == null ) return null;
    
    $get(staffCtlId).value = driverInfo.baseInfo.substr(0, 6);
    
    if (taxiNoId != null )
    {
        var taxiNo = driverInfo.baseInfo.substr(6, 12);
        if (taxiNo == 'FFFFFFFFFF') 
        {
            $get(taxiNoId).value = 'FFFFFFFFFF';
        }
        else
        {
            $get(taxiNoId).value = 
                // String.fromCharCode(parseInt(taxiNo.substr(0,2),16)) +
                String.fromCharCode(parseInt(taxiNo.substr(2,2),16)) +
                String.fromCharCode(parseInt(taxiNo.substr(4,2),16)) +
                String.fromCharCode(parseInt(taxiNo.substr(6,2),16)) +
                String.fromCharCode(parseInt(taxiNo.substr(8,2),16)) +
                String.fromCharCode(parseInt(taxiNo.substr(10,2),16)) ;
        }
    }
    
    if (stateId != null )
    {
        $get(stateId).value = driverInfo.specialInfo.substr(0,2);
    }
    
    if (useTag != null)
    {
        cardReader.readCardFunc = cardReader.getCardInfo;
        var cardInfo = TargetWindowReadCard();
        if (cardInfo == null )
        {
            return false;
        }      
        
        $get(useTag).value = cardInfo.staffTag;
        
        assignValue(cardNoId, cardInfo.cardNo);
    }
    else
    {
        if (cardNoId != null)
        {
            cardReader.readCardFunc = cardReader.getCardNo;
            var cardNo = TargetWindowReadCard();
            if (cardNo == null )
            {
                return false;
            }
            $get(cardNoId).value = cardNo;
        }
    }
    
    return driverInfo;
}

function ReadCardInfoEx()
{
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardno', 
        cardReader.getCardInfoEx, assignHiddenValues, true);
}
function ReadCardInfo(txtCardNo, showErr) {
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno', 
        cardReader.getCardInfo, assignHiddenValues, showErr);
}
function ReadOldCardInfoEx() {
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardno',
        cardReader.getOldCardInfoEx, assignHiddenValues, true);
}
function ReadOldCardInfo(txtCardNo, showErr) {
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno',
        cardReader.getOldCardInfo, assignHiddenValues, showErr);
}

function ReadCardInfoAndTag(txtCardNo, showErr) {
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno',
        cardReader.readMonthlyInfo, assignHiddenValuesAndTag, showErr);
}



function ReadCardInfoUnlimited(txtCardNo, showErr) {
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno',
        cardReader.getCardInfoUnlimited, assignUnlimitedHiddenValues, showErr);
}

function ReadParkCardInfo()
{
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardNo',
        cardReader.readMonthlyInfo, assignParkHiddenValues, true);
}

function ReadCashGiftInfo(assignFunc, cardNoId, showErr) {
    return ReadCardInfoImpl(cardNoId == null ? 'txtCardNo' : cardNoId, new Array(cardReader.getCardInfo), assignFunc, showErr);
}

function ReadCashGiftInfoUnlimited(assignFunc, cardNoId, showErr) {
    return ReadCardInfoImpl(cardNoId == null ? 'txtCardNo' : cardNoId, new Array(cardReader.getCardInfoUnlimited), assignFunc, showErr);
}
//read VerifyDate
function ReadVerifyDateInfo() {
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardno',
        cardReader.getVerifyCardInfo, assignVerifyHiddenValues, true);
}
function ReadCashGiftInfoEx(assignFunc, cardNoId, showErr){
    return ReadCardInfoImpl(cardNoId == null ? 'txtCardNo' : cardNoId, 
    new Array(cardReader.getCardInfoEx), assignFunc, showErr);
}


function ReadCardInfoForCheck(txtCardNo, showErr)
{
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno',  
        cardReader.getCardInfo, null, showErr);
}

function assignValue(ctrlId, value)
{
    var v = $get(ctrlId);
    if (v != null ) v.value = value;
}

function ReadCardInfoImpl(cardNoId, func, assignFunc, showErr)
{
    var cardInfo = null;
    if (inTestingMode(cardNoId)) {
        var cardNo = $get(cardNoId).value;
        cardInfo = new CardInfo(cardNo, "02", "01", "00001122334455661000",
            "20020102", "20500101", "FCI", "01", "AA22", 0, "FF", 2000);
        cardReader.CardPermit = "00";
        cardReader.CardRent = 3000;
        cardReader.VerifyDate = "20991010";
        cardReader.MonthlyInfo = "24";
        cardReader.CardInfo = cardInfo;
    }
    else {
        cardReader.readCardFunc = func;
        TargetWindowReadCard(showErr);
        cardInfo = cardReader.CardInfo;
    }
 
    cardReader.assignFunc = assignFunc;
    cardReader.cardNoId   = cardNoId;
    
    if (cardInfo == null ) 
    {
        return false;
    }


    if (assignFunc) {
        
        assignFunc(cardNoId, cardInfo);
    }
    
    return true;    
}

function assignHiddenValues(cardNoId, cardInfo)
{
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hiddenAsn', cardInfo.appSn);
    assignValue('hiddenLabCardtype', cardInfo.cardType);
    assignValue('hiddensDate', cardInfo.appStartDate);
    assignValue('hiddeneDate', cardInfo.appEndDate);
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hiddentradeno', cardInfo.tradeNo);

}
function assignHiddenValuesAndTag(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hiddenAsn', cardInfo.appSn);
    assignValue('hiddenLabCardtype', cardInfo.cardType);
    assignValue('hiddensDate', cardInfo.appStartDate);
    assignValue('hiddeneDate', cardInfo.appEndDate);
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hiddentradeno', cardInfo.tradeNo);
    assignValue('hiddenGjTag', cardReader.MonthlyInfo);

}

 
function assignParkHiddenValues(cardNoId, cardInfo)
{	    
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hidAsn', cardInfo.appSn);
    assignValue('hidNewCardType', cardInfo.cardType);
    assignValue('txtStartDate', cardInfo.appStartDate);
    assignValue('txtEndDate', cardInfo.appEndDate);
    assignValue('txtCardBalance', (cardInfo.balance/100).toFixed(2));
    assignValue('hidTradeNo', cardInfo.tradeNo);
    assignValue('hiddenGjTag', cardReader.MonthlyInfo);
}

function assignVerifyHiddenValues(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hiddenAsn', cardInfo.appSn);
    assignValue('hiddenLabCardtype', cardInfo.cardType);
    assignValue('hiddensDate', cardInfo.appStartDate);
    assignValue('hiddeneDate', cardInfo.appEndDate);
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hiddentradeno', cardInfo.tradeNo);
    assignValue('txtPreVerifyDate', cardReader.VerifyDate);
}

function assignCardTypeHiddenValues(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hiddenAsn', cardInfo.appSn);
    assignValue('hiddenLabCardtype', cardInfo.cardType);
    assignValue('hiddensDate', cardInfo.appStartDate);
    assignValue('hiddeneDate', cardInfo.appEndDate);
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hiddentradeno', cardInfo.tradeNo);
    assignValue('txtCardType', cardReader.MonthlyInfo);
}

function assignUnlimitedHiddenValues(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hiddenAsn', cardInfo.appSn);
    assignValue('hiddenLabCardtype', cardInfo.cardType);
    assignValue('hiddensDate', cardInfo.appStartDate);
    assignValue('hiddeneDate', cardInfo.appEndDate);
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hiddentradeno', cardInfo.tradeNo);
    assignValue('hiddenCardPermit', cardReader.CardPermit);
    assignValue('hiddenCardRent', cardReader.CardRent);
}

function readParkInfo()
{
    var ret = ReadCardInfoImpl('txtCardNo', cardReader.getCardInfo, assignParkHiddenValues);
    if (!ret ) return ret;
    
    if (cardReader.testingMode)
    {
        $get('hidParkInfo').value = '200812210164';
        return true;
    }
    
    cardReader.readCardFunc = cardReader.getParkInfo;
    var ret = TargetWindowReadCard();
    if (ret == null )
    {
        return false;
    }
    
    assignValue('hidParkInfo', cardReader.ParkInfo);
    return true;    
}

function readXXParkInfo()
{
    var ret = ReadCardInfoImpl('txtCardNo', cardReader.getCardInfo, assignParkHiddenValues);
    if (!ret ) return ret;
    
    if (cardReader.testingMode)
    {
        $get('hidParkInfo').value = '200812210164';
        return true;
    }
    
    cardReader.readCardFunc = cardReader.getXXParkInfo;
    var ret = TargetWindowReadCard();
    if (ret == null )
    {
        return false;
    }
    
    assignValue('hidParkInfo', cardReader.XXParkInfo);
    return true;    
}    


function startPark()
{
    cardReader.ParkInfo = $get('hidParkInfo').value;
    cardReader.writeCardFunc = cardReader.writeParkInfo;
    writeCard();
}

function startXXPark()
{
    cardReader.XXParkInfo = $get('hidParkInfo').value;
    cardReader.writeCardFunc = cardReader.writeXXParkInfo;
    writeCard();
}

function changeMonthlyCard() {

    cardReader.MonthlyInfo = $get('hidMonthlyFlag').value;
    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;

    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);

    cardReader.writeCardFunc = new Array(cardReader.writeMonthlyInfo,
            cardReader.startCard,
            cardReader.chargeCard, cardReader.modifyIDCardInfo);
    writeCard();
}
function changeMonthlyVerifyCard() {
    cardReader.MonthlyInfo = $get('hidMonthlyFlag').value;
    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);
    cardReader.VerifyDate = $get('txtVerifyDate').value;//verifydate
    cardReader.writeCardFunc = new Array(cardReader.writeMonthlyInfo,
            cardReader.startCard,
            cardReader.chargeCard, cardReader.verifyCard, cardReader.modifyIDCardInfo);
    writeCard();
}


function startMonthlyInfo()
{
    cardReader.MonthlyInfo = $get('hidMonthlyFlag').value;

    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;

    cardReader.writeCardFunc = new Array(cardReader.writeMonthlyInfo, cardReader.startCard, cardReader.modifyIDCardInfo);
    writeCard();
}



function modifyMonthlyInfo()
{
    cardReader.MonthlyInfo = $get('hidMonthlyFlag').value;
    cardReader.writeCardFunc = cardReader.writeMonthlyInfo;
    writeCard();
}

//function endMonthlyInfo() {
//    cardReader.MonthlyInfo = '00';//card type
//    //cardReader.MonthlyInfo = '02FF';//02 card type .FF sex
//    cardReader.writeCardFunc = new Array(cardReader.endCard, cardReader.writeMonthlyInfo);
//    writeCard();
//}    


function endMonthlyInfo() {
   // cardReader.MonthlyInfo = '00'; //card type
    //cardReader.MonthlyInfo = '02FF';//02 card type .FF sex
    cardReader.writeCardFunc = new Array(cardReader.endCard);
    writeCard();
}  


function rollbackMonthlyInfo() {

    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);
    cardReader.MonthlyInfo = '00';
    cardReader.writeCardFunc = new Array(
            cardReader.unchargeCard,
            cardReader.endCard, cardReader.writeMonthlyInfo);
    writeCard();
}  

function rewriteCard(btn)
{
    if (btn == 'yes' )
    {
        writeCard();
    }
    else
    {
        $get('hidWarning').value = 'writeFail';
        $get('btnConfirm').click();
    }
}

function saleCard() {
    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;
    cardReader.writeCardFunc = new Array(
            cardReader.startCard,
            cardReader.modifyIDCardInfo);
 //   cardReader.writeCardFunc = cardReader.startCard;
    writeCard();
}

function saleMakeCard() { 
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);
//    cardReader.writeCardFunc = new Array();
//    cardReader.writeCardFunc = cardReader.startCard;
//    cardReader.writeCardFunc = cardReader.chargeCard;

    cardReader.writeCardFunc = new Array(
            cardReader.startCard,
            cardReader.chargeCard);
    writeCard();
}


function chargeCard() {
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);

    cardReader.readCallback = function () {
        ReadCardInfo(null, false);
        if (cardReader.ErrRet == 0) {
            chargeCardConfirm();
        }

        return false;
    };
    cardReader.writeCardFunc = cardReader.chargeCard;

    writeCard();
}

function startCashGiftCard(wallet1) {
    cardReader.preMoney = 0;
    cardReader.chargeMoney = wallet1;
    cardReader.readCallback = function () {
        ReadCashGiftInfo(cardReader.assignFunc, cardReader.cardNoId, false);
        return true;
    };
    cardReader.writeCardFunc = new Array();
    if (cardReader.CardInfo.wallet2 == 0)//chongzhuang PIN1
        cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.unlockCard;
    cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.startCardGift;
    cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.chargeCard;
    writeCard();
}

function saleRollback()
{
    cardReader.writeCardFunc = cardReader.endCard;
    writeCard();
} 

function changeCard() {

    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;

    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);

    cardReader.writeCardFunc = new Array(
            cardReader.startCard,
            cardReader.chargeCard, cardReader.modifyIDCardInfo);
            
    writeCard();
}

function changeCardRollback()
{
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);
    
    cardReader.writeCardFunc = new Array(
            cardReader.unchargeCard, 
            cardReader.endCard);
            
    writeCard();
}

function changeOldCardRollback() {
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidSupplyMoney').value);

    cardReader.writeCardFunc = new Array(
            cardReader.unchargeCard);

    writeCard();
}

function lockCard()
{
    cardReader.writeCardFunc = cardReader.lockCard;
    writeCard();
}

function lockOldCard() {
    cardReader.writeCardFunc = cardReader.lockOldCard;
    writeCard();
}

function unlockCard()
{
    cardReader.writeCardFunc = cardReader.unlockCard;
    writeCard();
}  
 


function unchargeCardEx(){ unchargeCardImpl(cardReader.unchargeCardEx); }
function unchargeCard()  { unchargeCardImpl(cardReader.unchargeCard  ); }

function unchargeCardImpl(func)
{
    cardReader.preMoney = parseInt($get('hiddencMoney').value);
    cardReader.chargeMoney = parseInt($get('hidUnSupplyMoney').value);
    cardReader.writeCardFunc = func;
    writeCard();
}  
 
function realRecvChanging(realRecv, testId, hidAccRecv)
{
    realRecv.value = realRecv.value.replace(/[^\d]/g, '');
    var test = $get(testId != null ? testId : 'txtChanges');
    test.innerHTML = (parseFloat(realRecv.value) - 
        parseFloat($get(hidAccRecv != null ? hidAccRecv : 'hidAccRecv').value) ).toFixed(2);
    if (test.innerHTML == 'NaN' )
    {
        test.innerHTML = '';
    }
}
function RecvChanging(realRecv, Total, txtRealRecv, testId, hidAccRecv)
{
    realRecv.value = realRecv.value.replace(/[^\d]/g, '');
    if (realRecv.value == '' )
    {
        realRecv.value = '0';
    }
    
    $get(Total).innerHTML = parseFloat(realRecv.value).toFixed(2);
    $get(hidAccRecv).value = $get(Total).innerHTML;
    $get(txtRealRecv).value = parseFloat(realRecv.value);
    $get(testId).innerHTML = '0.00';
}
function changemoney(realmoney)
{
    if($get('Cash').checked)
    {
        //realmoney.value = realmoney.value.replace(^[0-9]+(.[0-9]{1,2})?$, '');
        $get('SupplyFee').innerHTML = (parseFloat(realmoney.value)).toFixed(2);
        
        if ($get('SupplyFee').innerHTML == 'NaN' )
        {
            $get('SupplyFee').innerHTML = '0.00';
        }

        $get('Total').innerHTML = (parseFloat(realmoney.value)).toFixed(2);
        if ($get('Total').innerHTML == 'NaN' )
        {
            $get('Total').innerHTML = '0.00';
        }
    }
    else if ($get('BKCard').checked || $get('WCard').checked) 
    {
        $get('SupplyFee').innerHTML = (parseFloat(realmoney.value)).toFixed(2);

        if ($get('SupplyFee').innerHTML == 'NaN') {
            $get('SupplyFee').innerHTML = '0.00';
        }

        $get('Total').innerHTML = (parseFloat(realmoney.value)).toFixed(2);
        if ($get('Total').innerHTML == 'NaN') {
            $get('Total').innerHTML = '0.00';
        }
    }
}
function changsaleemoney(realmoney)
{
    
    realmoney.value = realmoney.value.replace(/[^\d]/g, '');
    $get('SupplyFee').innerHTML = (parseFloat(realmoney.value)).toFixed(2);
    
    if ($get('SupplyFee').innerHTML == 'NaN' )
    {
        $get('SupplyFee').innerHTML = '0.00';
    }
    
    $get('TotalSupply').innerHTML = (parseFloat(realmoney.value)).toFixed(2);
    if ($get('TotalSupply').innerHTML == 'NaN' )
    {
        $get('TotalSupply').innerHTML = '0.00';
    }

}

//change user password
function changePassword2() {
    cardReader.OldPassword = $get('hiddenOldPassword').value;
    cardReader.NewPassword = $get('hiddenNewPassword').value;
    cardReader.writeCardFunc = cardReader.changeUserPassword2;//modify user password
    writeCard();
}
function resetPassword2() {
    cardReader.writeCardFunc = cardReader.resetPassword2; //modify user password
    writeCard();
}

function showResult(btn)
{
    var hidw = $get('hidWarning');
    var btnCf = $get('btnConfirm');

    if (hidw != null) hidw.value = btn;
    if (btn == 'yes' )
    {
        if (btnCf != null) btnCf.click();
    }
}
      
function writeCardWithCheck()
{ 
    cardReader.readCallback = function(){
        ReadCardInfo(null, false);
        return true;
    };
    writeCard();
}

function writeCardDelay() {      
    if (!cardReader.testingMode) {
        cardReader.writeCard(window);
    }
    else {
        writeCompleteCallBack();
    }
 }

function RecoverCheckConfirm(btn)
{
    if (btn == 'yes' )
        {
            $get('hidWarning').value = 'RevocerConfirm';
            $get('btnConfirm').click();
        }
}

function DBRecoverCheckConfirm(btn)
{
    if (btn == 'yes' )
        {
            $get('hidWarning').value = 'writeSuccess';
            $get('btnConfirm').click();
        }
}

function SupplyCheckConfirm(btn)
{
    if (btn == 'yes' )
    {
        $get('hidWarning').value = 'CashChargeConfirm';
        $get('btnConfirm').click();
    }
}

function ChangeSpecialCardConfirm(btn) {
    if (btn == 'yes') {
        $get('hidWarning').value = 'yes';
        $get('btnConfirm').click();
    }
}
function FaPiaoCheckConfirm(btn) {
    if (btn == 'yes') {
        $get('btnConfirm').click();
    }
}

function SupplyFaPiaoCheckConfirm(btn) {
    if (btn == 'yes') {
            $get('chkFaPiao').checked = !$get('chkFaPiao').checked;
        }
    }

    function SupplyFaPiaoCheck() {
        if ($get('chkFaPiao').checked) {
            MyExtConfirm('提示', '是否自动打印发票', SupplyFaPiaoCheckConfirm);
            return false;
        }
    }

function FapiaoCheck() {
    MyExtConfirm('提示', '是否确认提交，请检查', FaPiaoCheckConfirm);
    return false;
}



function endDriverCard(staffTag)
{
    cardReader.StaffTag = $get(staffTag == null ? 'selUseState' : staffTag).value;
    cardReader.writeCardFunc = cardReader.WirteStaffTag;
    writeCard();
}

function writeTelcard()
{
    cardReader.Telcard = $get('strName').value;
    cardReader.writeCardFunc = cardReader.writeTelcard;
    writeCard();
}



function newDriverCard(hidStaffNo, hidCarNo, hidState)
{
    cardReader.DriverNo = $get(hidStaffNo == null ? 'hidStaffNo' : hidStaffNo).value;
    cardReader.CarNo = $get(hidCarNo == null ? 'hidCarNo' : hidCarNo).value;
    cardReader.DriverState = $get(hidState == null ? 'hidState' : hidState).value;
    
//    alert('DriverNo:' + cardReader.DriverNo + ', CarNo:' + 
//        cardReader.CarNo + ', DriverState:' +
//         cardReader.DriverState);
    cardReader.writeCardFunc = new Array(
            cardReader.initDriverCard, 
            cardReader.writeDriverCard,
            cardReader.modifyDriverState);
            
    writeCard();
}



function endCashGiftCard()
{
    ReadCashGiftInfoUnlimited(cardReader.assignFunc, cardReader.cardNoId, false);
  
    cardReader.writeCardFunc = new Array();
        
    if (cardReader.CardInfo.balance > 0) 
    {
        cardReader.preMoney = cardReader.CardInfo.balance;
        cardReader.chargeMoney = cardReader.CardInfo.balance;
        cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.unchargeCard;
    }
    cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.endCardGift;
    //lockcard
    if(cardReader.CardInfo.wallet2!=0)
        cardReader.writeCardFunc[cardReader.writeCardFunc.length] = cardReader.lockCard;
    writeCard();
}

function delayCashGiftCard(endDate)
{
    cardReader.endDate = endDate;
    
    cardReader.writeCardFunc = new Array(cardReader.modifyEndDate);
    writeCard();
}

//verify Card
function VerifyCard() {
    cardReader.VerifyDate = $get('txtVerifyDate').value;
    cardReader.writeCardFunc = cardReader.verifyCard;
    writeCard();
}


function DealCardTypeAndVerifyDate() {
    cardReader.VerifyDate = $get('txtVerifyDate').value;
    cardReader.MonthlyInfo = $get('hidMonthlyFlag').value;

    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;

    cardReader.writeCardFunc = new Array(cardReader.startCard,
            cardReader.writeMonthlyInfo,
            cardReader.verifyCard,cardReader.modifyIDCardInfo);

    //cardReader.writeCardFunc = cardReader.dealCardTypeAndVerifyDate;
    writeCard();
}

function ModifyIDCardInfo() {


    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.UserPhone = $get('hidUserPhone').value;
    cardReader.writeCardFunc = cardReader.modifyIDCardInfo;
    writeCard();
}

//read card type
function ReadCardType() {
    if (inTestingMode('txtCardNo')) {
        return true;
    }
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardNo',
        cardReader.readMonthlyInfo, assignCardTypeHiddenValues, true);
}

function printAll() {
    setTimeout(printAllDelay, 50);
}

function printAllDelay() {
    printPingZhengDelay();
    printShouJuDelay();
}

function printPingZAndFaPiao() {
    printPingZhengDelay();
    if ($get('hidFaPiaoFlag').value == "writeSuccess") {
        printFaPiaoDelay();
    }
    
}

function printFaPiao() {
    if ($get('hidFaPiaoFlag').value == "writeSuccess") {
        setTimeout(printFaPiaoDelay, 50);
    }
    //printdiv('printfapiao');
    
}

function printFaPiaoDelay() {
    printdiv('printfapiao');
}

function printInvoice() {
    setTimeout(printPingZhengDelay, 50);
}

     

function printShouJu() {
    setTimeout(printShouJuDelay, 50);
}
function printCommonPrintPingZheng() {
    setTimeout(printCommonPrintPingZhengDelay, 50);
}
function printCommonPrintPingZhengDelay() {
    var invoice = $get('chkCommonPrintPingZheng');

    if (invoice != null && invoice.checked) {
        printdiv('ptnCommonPrintPingZheng1');
    }
}
function printShouJuDelay() {
    var invoice = $get('chkShouju');

    if (invoice != null && invoice.checked) {
        printdiv('ptnShouJu1');
    }
}

// deprecated
function printPingZhengDelay() {
    var invoice = $get('chkPingzheng');

    if (invoice != null && invoice.checked) {
        printdiv('ptnPingZheng1');
    }
}

function printShouJuDelayX() {
    var invoice = $get('chkShouju');
    
    if (invoice != null && invoice.checked)
    {
        printArea('ptnShouJu1'); // document.all.item('ptnShouJu1').innerHTML);
    }
}

function printPingZhengDelayX() {
    var invoice = $get('chkPingzheng');

    if (invoice != null && invoice.checked) {
        printArea('ptnPingZheng1'); // document.all.item('ptnPingZheng1').innerHTML);
    }
}

function startSpecialInfo1() {
    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.writeCardFunc = cardReader.writeSpecialInfo;
    cardReader.readCallback = function () {
        $get('hidCardNo').value = cardReader.CardNo;
        $get('hidAsn').value = cardReader.SpecialCardAsn;
        $get('hidTradeNo').value = cardReader.SpecialCardTradeNo;
        $get('hidSpecialCardType').value = cardReader.SpecialCardType;
        $get('hidSpecialCardName').value = cardReader.SpecialCardName;
        return true;
    };
    writeCard();  
}

function startSpecialInfo2() {
    cardReader.UserName = $get('hidUserName').value;
    cardReader.UserSex = $get('hidUserSex').value;
    cardReader.UserPaperno = $get('hidUserPaperno').value;
    cardReader.VerifyDate = $get('txtVerifyDate').value;
    cardReader.writeCardFunc = new Array(cardReader.writeSpecialInfo, cardReader.verifyCard, cardReader.loadNumber);
    cardReader.readCallback = function () {
        $get('hidCardNo').value = cardReader.CardNo;
        $get('hidAsn').value = cardReader.SpecialCardAsn;
        $get('hidTradeNo').value = cardReader.SpecialCardTradeNo;
        $get('hidSpecialCardType').value = cardReader.SpecialCardType;
        $get('hidSpecialCardName').value = cardReader.SpecialCardName;
        return true;
    };
    writeCard();
}

function ReadSpecialCardInfoEx(txtCardNo) {
    return ReadCardInfoImpl(txtCardNo != null ? arguments[0] : 'txtCardno',
        cardReader.getSpecialCardInfo, assignSpecialHiddenValues, true);
}

function ReadSpecialCardCheckInfo() {
    return ReadCardInfoImpl(arguments.length > 0 ? arguments[0] : 'txtCardNo',
        cardReader.getSpecialCardInfo, assignSpecialCheckValues, true);
}

function assignSpecialHiddenValues(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('hidReadAsn', cardInfo.appSn);
    assignValue('hidReadTradeNo', cardInfo.tradeNo);
    assignValue('txtStartDate', cardInfo.appStartDate);
    assignValue('txtEndDate', cardInfo.appEndDate);
    assignValue('txtCardBalance', (cardInfo.balance / 100).toFixed(2));
    assignValue('hiddencMoney', cardInfo.balance);
    assignValue('hidReadSpecialCardType', cardReader.SpecialCardType);
    assignValue('hidReadSpecialCardName', cardReader.SpecialCardName);
    assignValue('txtPreVerifyDate', cardReader.VerifyDate);
}

function assignSpecialCheckValues(cardNoId, cardInfo) {
    assignValue(cardNoId, cardInfo.cardNo);
    assignValue('txtPreVerifyDate', cardReader.VerifyDate);
}

function VerifySpecialCard() {
    cardReader.VerifyDate = $get('txtVerifyDate').value;
    cardReader.writeCardFunc = new Array(cardReader.verifyCard, cardReader.loadNumber);
    writeCard();
}