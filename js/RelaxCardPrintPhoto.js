﻿ 
function loadWebcam() { 
		var btnState = $('#btnShowWebCam').attr('disabled');
		if(!btnState){
			$('#btnShowWebCam').attr('disabled',true);
		} 
        $("#webcam").webcam({
            width: 150,
            height: 200,
            mode: "save",
            swffile: "../../js/webcam/jscam_240320.swf",
            onSave: function (data) {
                var d = new Date();
                $('#imgCustomerPic').attr('src', "../../tmp/printImg" + $('#hidStaffno').val() + ".jpg?time=" + d.getUTCMilliseconds());
                $('#hidIsCapture').val('1');
                MyExtHide();
            },
            onCapture: function () { 
                var result = webcam.save("AS_RelaxcardWebCam.ashx");
            },
            onLoad: function() {
				if(!btnState){
					$('#btnShowWebCam').attr('disabled',false);
				} 
			}
        });
        $('#webcamDiv').hide();
}


function Capture()
{
    MyExtShow('请等待', '正在处理中...');
    setTimeout('webcam.capture()',200);
    return false;
}

//切换模板
function ChangeTemplate(template) {
    $('.tdTemplate').css('background', '#FFFFFF');
    $('#tdTemplate' + template).css('background', '#E0E0E0'); 
    $('#rdoPrintTemplate' + template).attr("checked",true);
}

 

//打印点击事件
function PrintCard() {
    var cardTemplate;
    var templates = document.getElementsByName("rdoPrintTemplate");
    if ($('#hidIsCapture').val() == '1') {
        var d = new Date();
        $('#imgPrint').attr('src', "../../tmp/printImgH" + $('#hidStaffno').val() + ".jpg?time=" + d.getUTCMilliseconds());
    } 
    for (var i = 0; i < templates.length; i++) {
        if (templates[i].checked) cardTemplate = (i + 1);
    }
    if (!cardTemplate) {
        MyExtAlert("警告", "请选择打印模板！"); 
    }
    else {
        PrintTemplate(cardTemplate);
        $('#btnPrint').attr("disabled", 'disabled');
    }
    return false;
}

//打印模板
function PrintTemplate(template) { 

    document.getElementById('divImg').className = 'juedui' + ' template' + template.toString();
   
    var headarr = [];
    headarr.push("<html><head><title>打印</title>");
    headarr.push("<style type='text/css'>");
    headarr.push(".juedui {");
    headarr.push("margin-left: 0px; ");
    headarr.push("margin-top:  0px;  ");
    headarr.push("margin-right: 0px;");
    headarr.push("margin-bottom: 0px;");
    headarr.push("background-repeat: no-repeat;");
    headarr.push("position: absolute; }");
    headarr.push(".template1{top:210px;left:28px;}");
    headarr.push(".template1 img{width:111px !important; height:91px !important;}");
    headarr.push(".template2{top:22px;left:28px;}");
    headarr.push(".template2 img{width:148px !important; height:118px !important;}");
    headarr.push(".template3{top:10px;left:40px;}");
    headarr.push(".template3 img{width:117px !important; height:84px !important;}");
    headarr.push(".template4{top:21px;left:32px;}");
    headarr.push(".template4 img{width:138px !important; height:100px !important;}");
    headarr.push(".template5{top:22px;left:14px;}");
    headarr.push(".template5 img{width:75px !important; height:75px !important;}");
    headarr.push(".template6{top:197px;left:41px;}");
    headarr.push(".template6 img{width:116px !important; height:96px !important;}");
    headarr.push("</style>");
    headarr.push("</head><body>");
    var headstr = headarr.join('');
    var footstr = "</body></html>";
    var newstr = document.all('PrintArea').innerHTML; 
    var printWin = open('', 'printWindow', 'left=50000,top=50000,width=220,height=375');
    printWin.document.write(headstr + newstr + footstr); 
    printWin.document.close();

    printWin.document.body.insertAdjacentHTML("beforeEnd", " \
        <object id=\"printFactory\" style=\"display:none\"   \
         classid=\"clsid:1663ED61-23EB-11D2-B92F-008048FDD814\"> \
        </object>");

    printWin.focus(); 
    if (printWin.printFactory.object) {
        printWin.printFactory.printing.Print(false);
    }
    else {
        printWin.print();
    }
    printWin.close();

    if ($('#hidIsCapture').val() == '1')
        setTimeout("$('#btnSave').click()", 1000);
    return false;
}