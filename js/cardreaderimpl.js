﻿
try {
    document.body.insertAdjacentHTML("beforeEnd", " \
    <object id=\"SX_CARDOCX1\" classid=\"clsid:0362744E-0794-4020-B5B0-355ED58A736D\" width=\"0\" height=\"0\"> \
    </object>");

    document.body.insertAdjacentHTML("beforeEnd", " \
    <object id=\"SynIDCard1\" classid=\"clsid:E6E0A751-541A-4855-9A8D-35EB7122C950\" codeBase=\"SynIDCard.Cab#version=1,0,0,1\"  width=\"0\" height=\"0\"> \
    </object>");
    document.body.insertAdjacentHTML("beforeEnd", " \
    <object id=\"CZICCARD\" classid=\"clsid:A70CDE95-961C-40CA-BB44-E356184DD2A0\"   width=\"0\" height=\"0\"> \
    </object>");
}
catch (e) {
}


//resolve old bus 
function getOldBusOCX() {
    if (window.parent.frames[1].location.href.indexOf('ASP/BusService/') > -1) {
        if (window.parent.frames[1].location.href.indexOf('ASP/BusService/BS_SaleCard') > -1) {
            return false;
        }
        if (window.parent.frames[1].location.href.indexOf('ASP/Bus/Service/BS_SaleCardRollback') > -1) {
            return false;
        }
        return true;
    }
    else {
       return false;
    }
}

function readLogonTestCardNo(ctrlId) {
    try {
        var txtOperCardNo = document.getElementById(ctrlId);
        if (txtOperCardNo.value != "") return true;

        var errRet = SX_CARDOCX1.ReadOperCardNo();
        if (errRet != 0) {
            alert('ErrInfo:' + SX_CARDOCX1.ErrInfo + ', ErrCode:' + errRet);
            return false;
        }

        txtOperCardNo.value = SX_CARDOCX1.OperCardNo;
        return true;
    }
    catch (e) {
        return false;
    }
   
}

function readLogonCardNo(ctrlId) {
    try {
        var txtOperCardNo = document.getElementById(ctrlId);

        var errRet = SX_CARDOCX1.ReadOperCardNo();
        if (errRet != 0) {
            alert('ErrInfo:' + SX_CARDOCX1.ErrInfo + ', ErrCode:' + errRet);
            return false;
        }

        txtOperCardNo.value = SX_CARDOCX1.OperCardNo;
        return true;
    }
    catch (e) {
        return false;
    }
    
}

function CardRecord(tradeNo, tradeMoney, tradeType, tradeTerm, tradeDate, tradeTime) {
    this.tradeNo = tradeNo;
    this.tradeMoney = tradeMoney;
    this.tradeType = tradeType;
    this.tradeTerm = tradeTerm;
    this.tradeDate = tradeDate;
    this.tradeTime = tradeTime;
}

function CardInfo(cardNo, appType, appVersion, appSn,
    appStartDate, appEndDate, FCI, cardType, tradeNo, balance, staffTag, wallet2) {
    this.cardNo = cardNo;
    this.appType = appType;
    this.appVersion = appVersion;
    this.appSn = appSn;
    this.appStartDate = appStartDate;
    this.appEndDate = appEndDate;
    this.FCI = FCI;
    this.cardType = cardType;
    this.tradeNo = tradeNo;
    this.balance = balance;
    this.staffTag = staffTag;

    this.wallet2 = wallet2;
}

function DriverCardInfo(driverBaseInfo, driverSpecialInfo, driverBlackInfo) {
    this.baseInfo = driverBaseInfo;
    this.specialInfo = driverSpecialInfo;
    this.blackInfo = driverBlackInfo;
}

function IDCardInfo(name, sex,birth, addr, paperno) {
    this.name = name;
    this.sex = sex;
    this.birth = birth;
    this.addr = addr;
    this.paperno = paperno;
}

function CardReader() {
    this.OperateCardNo = null;

    this.readCardFunc = null;

    this.readCard = function (targetWindow, showErr) {
        var ret = this.readCardImpl();

        if (this.ErrRet != 0) {
            targetWindow.showReadCardErr(this.ErrInfo, this.ErrRet, showErr == null ? true : showErr);
        }

        return ret;
    }

    this.readCardImpl = function () {
        var ret = null;
        if (this.readCardFunc == null) return null;

        try {
            //this.setToken();

            if (this.readCardFunc.length == null || this.readCardFunc.length == 0) {
                ret = this.readCardFunc();
            }
            else {
                var func;
                for (var i = 0; i < this.readCardFunc.length; ++i) {
                    func = this.readCardFunc[i];
                    ret = func.call(this);

                    if (this.ErrRet != 0) break;
                }
            }
        }
        catch (ex)//catch the ex 
        {
            // alert(ex.number+"\n"+ex.description); 
            this.ErrRet = ex.number;
            this.ErrInfo = ex.description;
        }

        return ret;
    }

    this.Telcard = null;

    this.getTelcard = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ReadTelcard(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.Telcard = null;
            }
            else {
                this.Telcard = SX_CARDOCX1.Telcard;
            }

            return this.Telcard;
        }
        catch (e) {
            return null;
        }


    }

    this.IDInfo = null;
    //du er dai zheng
    this.getIDCardInfo = function () {
        try {
            SynIDCard1.Port = 1001;
            this.ErrRet = SynIDCard1.Init();
            if (this.ErrRet != 0) {
                this.ErrInfo = "error,please check";
                this.IDInfo = null;
            }
            else {
                this.ErrInfo = SynIDCard1.ReadCard();

                this.IDInfo = new IDCardInfo(SynIDCard1.NameA,
                SynIDCard1.SexL, SynIDCard1.Born, SynIDCard1.Address, SynIDCard1.CardNo);

            }
            return this.IDInfo;
        }
        catch (e) {
            return null;
        }



    }
    /* this.getIDCardInfo = function()
    {
    this.ErrRet = SX_CARDOCX1.ReadIdentifyCard();
    if (this.ErrRet != 0) 
    {
    this.ErrInfo = SX_CARDOCX1.ErrInfo;
    this.IDInfo = null;
    } 
    else
    {
    this.IDInfo = new IDCardInfo(SX_CARDOCX1.Name, 
    SX_CARDOCX1.Gender, SX_CARDOCX1.Birth, SX_CARDOCX1.Addr, SX_CARDOCX1.Paperno);
    }
    return this.IDInfo;
    }*/

    this.modifyIDCardInfo = function () {
        try {
            this.UserNation = "01";
            //verify is jimingka ?
            if (this.UserName == null || this.UserName == "") {
                return true;
            }
            if (this.UserPhone == null || this.UserPhone == "") {
                this.UserPhone = "12345678901";
            }
            if (this.UserSex == null || this.UserSex == "") {
                this.UserSex = "01";
            }
            if (this.UserPaperno == null || this.UserPaperno == "") {
                this.UserPaperno = "321281195511213898";
            }
            if (this.UserPhone.length > 20) {
                this.UserPhone = this.UserPhone.substr(0, 20);
            }
            if (this.UserPaperno.length > 18) {
                this.UserPhone = this.UserPhone.substr(0, 18);
            }
            this.UserPhone = "12345678901"; //CZSMK-XQ-20130710_001
            if (this.UserName.length > 10) {
                this.UserName = this.UserPhone.substr(0, 10);
            }
            this.ErrRet = SX_CARDOCX1.WriteIdentifyCard(this.OperateCardNo, this.UserName, this.UserSex, this.UserNation, this.UserPaperno, this.UserPhone);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;

        }
        catch (e) {
            return false;
        }
    }

    this.DriverRecord = null;

    this.getDriverRecord = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ReadDriverRecord(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.DriverRecord = null;
            }
            else {
                this.DriverRecord = SX_CARDOCX1.DriverRecord;
            }
            return this.DriverRecord;
        }
        catch (e) {
            return null;
        }

    }
    this.CardPermit = null; //old bus lock state
    this.getCardNo = function () {
        try {
            //oldbus
            if (getOldBusOCX()) {
                this.ErrRet = CZICCARD.ReadCardNo();
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    this.CardNo = null;
                }
                else {
                    this.CardPermit = CZICCARD.CardPermit;
                    this.CardNo = CZICCARD.CardNo;
                }
                return this.CardNo;

            }

            this.ErrRet = SX_CARDOCX1.ReadCardNo();
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.CardNo = null;
            }
            else {
                this.CardNo = SX_CARDOCX1.CardNo;
            }
            return this.CardNo;
        }
        catch (e) {
            return null;
        }

    }

    this.DriverInfo = null;
    this.getDriverInfo = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ReadDriverInfo(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.DriverInfo = null;
            }
            else {
                this.DriverInfo = new DriverCardInfo(SX_CARDOCX1.DriverBaseInfo,
                SX_CARDOCX1.DriverSpecialInfo,
                SX_CARDOCX1.DriverBlackInfo);
            }

            return this.DriverInfo;
        }
        catch (e) {
            return null;
        }

    }

    this.getParkInfo = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ReadParkInfoEx(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.ParkInfo = null;
            }
            else {
                this.ParkInfo = SX_CARDOCX1.ParkEndDate;
            }
            return this.ParkInfo;
        }
        catch (e) {
            return null;
        }

    }

    this.getXXParkInfo = function () {
    try{
        this.ErrRet = SX_CARDOCX1.ReadXiuXianInfo(this.OperateCardNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            this.XXParkInfo = null;
        }
        else {
            this.XXParkInfo = SX_CARDOCX1.XXParkEndDate;
        }
        return this.XXParkInfo;
    }
    catch (e) {
        return null;
    }
       
    }

    this.getMonthlyInfo = function () {
        var cardinfo = getCardInfo();

        this.MonthlyInfo = null;
        if (cardinfo == null) return null;

        this.MonthlyInfo = cardinfo.appType;

        return this.MonthlyInfo;
    }

    this.getOperCardNo = function () {
    try{
        this.ErrRet = SX_CARDOCX1.ReadOperCardNo();
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            this.OperCardNo = null;
        }
        else {
            this.OperCardNo = SX_CARDOCX1.OperCardNo;
        }
        return this.OperCardNo;
    }
    catch (e) {
        return null;
    }
       
    }

    this.getRecords = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ReadRecord(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.CardRecord = null;
            }
            else {
                this.CardRecord = new CardRecord(SX_CARDOCX1.TradenoRec, SX_CARDOCX1.TradeMoneyRec, SX_CARDOCX1.TradeTypeRec,
	            SX_CARDOCX1.TradeTermRec, SX_CARDOCX1.TradeDateRec, SX_CARDOCX1.TradeTimeRec);
            }

            return this.CardRecord;
        }
        catch (e) {
            return null;
        }

    }

    this.getCardInfoEx = function () {
        return this.getCardInfoImpl(true);
    }

    this.getCardInfo = function () {
        return this.getCardInfoImpl(false);
    }

    this.getOldCardInfo = function () {
        return this.getOldCardInfoImpl(false);
    }

    this.getOldCardInfoEx = function () {
        return this.getOldCardInfoImpl(true);
    }

    this.getCardInfoUnlimited = function () {
        return this.getCardInfoImplUnlimited();
    }
    this.CardInfo = null;

    this.CardRent = null;
    this.getCardInfoImplUnlimited = function () {
        try {
            if (getOldBusOCX()) {
                this.ErrRet = CZICCARD.ReadInfoUnLimit(this.OperateCardNo);
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    this.CardInfo = null;
                }
                else {
                    this.CardInfo = new CardInfo(CZICCARD.CARDNO, CZICCARD.AppType, CZICCARD.AppVersion,
                CZICCARD.AppSn, CZICCARD.AppStartDate, "20501231", CZICCARD.FCI,
                CZICCARD.CardType, CZICCARD.Tradeno, CZICCARD.Balance, CZICCARD.StaffTag);
                    this.CardNo = CZICCARD.CARDNO;
                    this.CardPermit = CZICCARD.CardPermit;
                    this.CardRent = CZICCARD.CardRent;
                }
                return this.CardInfo;
            }
            else {
                this.ErrRet = SX_CARDOCX1.ReadInfo(this.OperateCardNo);
                if (this.ErrRet != 0) {
                    this.ErrRet = SX_CARDOCX1.ReadInfoEx(this.OperateCardNo)
                    if (this.ErrRet != 0) {
                        this.ErrInfo = SX_CARDOCX1.ErrInfo;
                        this.CardInfo = null;
                    }
                    else {
                        this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                        SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                        SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag,0);
                        this.CardNo = SX_CARDOCX1.CARDNO;
                    }
                }
                else {
                    this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                        SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                        SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag,2000);
                    this.CardNo = SX_CARDOCX1.CARDNO;
                }
                return this.CardInfo;
            }
        }
        catch (e) {
            return null;
        }

    }

    this.getOldCardInfoImpl = function (ex) {
        try {
            this.ErrRet = ex
            ? CZICCARD.ReadInfoEx(this.OperateCardNo)
            : CZICCARD.ReadInfo(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                this.CardInfo = null;
            }
            else {
                this.CardInfo = new CardInfo(CZICCARD.CARDNO, CZICCARD.AppType, CZICCARD.AppVersion,
                CZICCARD.AppSn, CZICCARD.AppStartDate, "20501231", CZICCARD.FCI,
                CZICCARD.CardType, CZICCARD.Tradeno, CZICCARD.Balance, CZICCARD.StaffTag);
                this.CardNo = CZICCARD.CARDNO;
                this.CardPermit = CZICCARD.CardPermit;
            }
            return this.CardInfo;
        }
        catch (e) {
            return null;
        }

    }

    this.getCardInfoImpl = function (ex) {
        try {
            //oldbus
            if (getOldBusOCX()) {
                this.ErrRet = ex
            ? CZICCARD.ReadInfoEx(this.OperateCardNo)
            : CZICCARD.ReadInfo(this.OperateCardNo);
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    this.CardInfo = null;
                }
                else {
                    this.CardInfo = new CardInfo(CZICCARD.CARDNO, CZICCARD.AppType, CZICCARD.AppVersion,
                CZICCARD.AppSn, CZICCARD.AppStartDate, "20501231", CZICCARD.FCI,
                CZICCARD.CardType, CZICCARD.Tradeno, CZICCARD.Balance, CZICCARD.StaffTag);
                    this.CardNo = CZICCARD.CARDNO;
                    this.CardPermit = CZICCARD.CardPermit;
                }

                return this.CardInfo;
            }


            this.ErrRet = ex
            ? SX_CARDOCX1.ReadInfoEx(this.OperateCardNo)
            : SX_CARDOCX1.ReadInfo(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.CardInfo = null;
            }
            else {
                if (ex) {//jin fu ka di yong jin:2000
                    this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                        SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                        SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag,0);
                    this.CardNo = SX_CARDOCX1.CARDNO;
                }
                else {
                    this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                        SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                        SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag,2000);
                    this.CardNo = SX_CARDOCX1.CARDNO;
                }
                
            }

            return this.CardInfo;
        }
        catch (e) {
            return null;
        }

    }

    this.startCard = function () {
        try {
            this.ErrRet = 0; //SX_CARDOCX1.StartCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return null;
        }

    }

    this.endCard = function () {
        try {
            this.ErrRet = 0; //SX_CARDOCX1.EndCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return false;
        }

    }

    this.startCardGift = function () {
        try {
            this.ErrRet =SX_CARDOCX1.StartCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return null;
        }

    }

    this.endCardGift = function () {
        try {
            this.ErrRet =SX_CARDOCX1.EndCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return false;
        }

    }

this.writeParkInfo = function () {
    try{
        this.ErrRet = SX_CARDOCX1.WriteParkInfoEx(this.OperateCardNo, this.ParkInfo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }
        return true;
    }
    catch (e) {
        return false;
    }
       
    }

this.writeXXParkInfo = function () {
    try{
        this.ErrRet = SX_CARDOCX1.WriteXiuXianInfo(this.OperateCardNo, this.XXParkInfo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
       
    }

this.writeMonthlyInfo = function () {
    try{
        var apptype = "00"+this.MonthlyInfo.substr(0, 2);
        this.ErrRet = SX_CARDOCX1.WriteGjTag(this.OperateCardNo, this.CardNo, apptype);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }
        return true;
    }
    catch (e) {
        return false;
    }
       
}

    this.MonthlyInfo = null;
    this.readMonthlyInfo=function(){
    	 return this.readMonthlyInfoImpl(false);
    	}
    this.readMonthlyInfoEx=function(){
    		 return this.readMonthlyInfoImpl(true);
    		}
   this.readMonthlyInfoImpl = function (ex) {
    try{
    this.ErrRet = ex
            ? SX_CARDOCX1.ReadInfoEx(this.OperateCardNo)
            : SX_CARDOCX1.ReadInfo(this.OperateCardNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            this.CardInfo = null;
        }
        else {
            this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag);
            this.CardNo = SX_CARDOCX1.CARDNO;
            this.ErrRet = SX_CARDOCX1.ReadGjTag(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.CardInfo = null;
                this.MonthlyInfo = null;
            }
            else {
                this.MonthlyInfo = SX_CARDOCX1.GjTag.substr(2,2);
            }
            return this.CardInfo;
        }
    }
    catch (e) {
        return null;
    } 



        
    }


    this.StaffTag = null;
    this.WirteStaffTag = function () {
    try{
        this.ErrRet = SX_CARDOCX1.ModifyStaffTag(this.OperateCardNo, this.CardNo, this.StaffTag);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    } 
       
    }

    this.lockOldCard = function () {
        try {
            this.ErrRet = CZICCARD.LockCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return false;
        }

    }

this.lockCard = function () {
    try {
        //oldbus
        if (getOldBusOCX()) {
            this.ErrRet = CZICCARD.LockCard(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }

            return true;
        }
        this.ErrRet = SX_CARDOCX1.Lock(this.OperateCardNo, this.CardNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }

}

this.unlockCard = function () {
    try{
        //oldbus
        if (getOldBusOCX()) {
            this.ErrRet = CZICCARD.UnLockCard(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }

            return true;
        }

        this.ErrRet = SX_CARDOCX1.UnLock(this.OperateCardNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true; 
    }
    catch (e) {
        return false;
    }
    }

this.initDriverCard = function () {
    try{
        this.ErrRet = SX_CARDOCX1.InitDriverCard(this.OperateCardNo, this.CardNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
        
    }

    this.DriverNo = null;
    this.CarNo = null;
    this.writeDriverCard = function () {
    try{
        this.ErrRet = SX_CARDOCX1.WriteDriverInfo(this.OperateCardNo, this.CardNo,
            this.DriverNo, this.CarNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
        
    }

    this.DriverState = null;
    this.modifyDriverState = function () {
    try{
        this.ErrRet = SX_CARDOCX1.ModifyDriverState(this.OperateCardNo, this.CardNo,
            this.DriverState);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
       
    }

this.writeTelcard = function () {
    try{
        this.ErrRet = SX_CARDOCX1.WriteTelcard(this.OperateCardNo, this.CardNo,
            this.Telcard);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
       
    }


    this.preMoney = 0;
    this.chargeMoney = 0;
    this.terminalNo = '112233445566';


    this.chargeCard = function () {
    try{
        //oldbus
        if (getOldBusOCX()) {
            this.ErrRet = CZICCARD.Load(this.OperateCardNo, this.CardNo,
            this.preMoney, this.chargeMoney, this.terminalNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }

            return true;
        }
        this.ErrRet = SX_CARDOCX1.Load(this.OperateCardNo, this.CardNo,
            this.preMoney, this.chargeMoney, this.terminalNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }
       
    }

    this.unchargeCardEx = function () { return this.unchargeCardImpl(true); }
    this.unchargeCard = function () { return this.unchargeCardImpl(false); }

    this.unchargeCardImpl = function (ex) {
        try{
            if (getOldBusOCX()) {
                this.ErrRet = CZICCARD.Load(this.OperateCardNo, this.CardNo,
            this.preMoney, parseInt("-" + this.chargeMoney), this.terminalNo);
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    return false;
                }

                return true;

            }
            this.ErrRet = ex
            ? SX_CARDOCX1.UnLoadEx(this.OperateCardNo, this.CardNo,
            this.preMoney, this.chargeMoney, this.terminalNo)
            : SX_CARDOCX1.UnLoad(this.OperateCardNo, this.CardNo,
            this.preMoney, this.chargeMoney, this.terminalNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return false;
        }
        
    }

    this.writeCardFunc = null;
    this.targetWin = null;

    this.writeCard = function (targetWindow) {
        this.targetWin = targetWindow;
        $get('hidWarning').value = "generateToken";
        $get('btnConfirm').click();
    }

    this.writeCardImpl = function () {
        var ret = null;
        if (this.writeCardFunc == null) return null;

        try {
            this.setToken();

            if (this.writeCardFunc.length == null || this.writeCardFunc.length == 0) {
                ret = this.writeCardFunc();
            }
            else {
                var func;
                for (var i = 0; i < this.writeCardFunc.length; ++i) {
                    func = this.writeCardFunc[i];
                    ret = func.call(this);

                    if (this.ErrRet != 0) break;
                }
            }
        }
        catch (ex)//catch the ex 
        {
            // alert(ex.number+"\n"+ex.description); 
            this.ErrRet = ex.number;
            this.ErrInfo = ex.description;
        }

        this.targetWin.writeCompleteCallBack();
        this.targetWin = null;

        return ret;
    }


    this.testingMode = false;

    this.setToken = function () {
        var token = $get('hidCardReaderToken');
        SX_CARDOCX1.Token = token.value;

        //if (token != null && token.value != "")
        //{
        //    alert(token.value);
        //}
        // todo: call the activex's token set function
    }

    // read callback function
    this.readCallback = null;

    this.endDate = null;

    // ModifyEndDate
    this.modifyEndDate = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ModifyEndDate(this.OperateCardNo, this.CardNo,
            this.endDate);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }

            return true;
        }
        catch (e) {
            return false;
        }

    }

    // ReadDeposit
    this.readwallet2 = function () {
        try {
            //lock 0,unlock 20
            this.ErrRet = SX_CARDOCX1.ReadInfo(this.OperateCardNo, this.CardNo);
            if (this.ErrRet != 0) {
                this.CardInfo.wallet2 = 0;
            }
            else {
                this.CardInfo.wallet2 = 2000;
            }
            return this.CardInfo.wallet2;
        }
        catch (e) {
            return 0;
        }

    }

    this.chargeWallet2 = null;

    // LoadDeposit
    this.loadWallet2 = function () {
    try{
        this.ErrRet = SX_CARDOCX1.LoadDeposit(this.OperateCardNo, this.CardNo,
            this.wallet2, this.chargeWallet2, this.terminalNo);
        if (this.ErrRet != 0) {
            this.ErrInfo = SX_CARDOCX1.ErrInfo;
            return false;
        }

        return true;
    }
    catch (e) {
        return false;
    }

        
    }
    //card verify
    this.VerifyDate = null;
    this.verifyCard = function () {
        try {
            //oldbus
            if (getOldBusOCX()) {
                this.ErrRet = CZICCARD.VerifyCard(this.OperateCardNo, this.CardNo,
                this.VerifyDate);
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    return false;
                }
                return true;
            }

            this.ErrRet = SX_CARDOCX1.ModifyStuTag(this.OperateCardNo, this.CardNo,
                this.VerifyDate);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }
            return true;
        }
        catch (e) {
            return false;
        }
    
    }


    this.loadNumber = function () {
        try {
            this.ErrRet = CZICCARD.LoadNumber(this.OperateCardNo, this.CardNo, 240000, this.terminalNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }
            return true;
        }
        catch (e) {
            return false;
        }
    }

    this.getVerifyCardInfo=function(){
    	return this.getVerifyCardInfoImpl(false);
    }

    this.getVerifyCardInfoEx=function(){
    		 return this.getVerifyCardInfoImpl(true);
    		}

    this.SpecialCardType = null;
    this.SpecialCardName = null;
    this.getSpecialCardInfo = function () {
        try {

            this.ErrRet = CZICCARD.ReadInfo(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                this.CardInfo = null;
            }
            else {
                this.CardInfo = new CardInfo(CZICCARD.CardNo, '', '', CZICCARD.AppSn, CZICCARD.AppStartDate, CZICCARD.AppEndDate, '',
                 CZICCARD.CardType, CZICCARD.Tradeno.substr(0, 4), CZICCARD.Balance, '',0);
                this.CardNo = CZICCARD.CardNo;
                this.CardPermit = CZICCARD.CardPermit;
                this.VerifyDate = CZICCARD.AppEndDate;
                this.SpecialCardType = CZICCARD.CardType;
                this.SpecialCardName = CZICCARD.CardName;
            }
            return this.CardInfo;

        }
        catch (e) {
            return null;
        }
    }

    this.SpecialCardAsn = null;
    this.SpecialCardTradeNo = null;
    this.writeSpecialInfo = function () {
        try {
            this.ErrRet = CZICCARD.SaleCard(this.OperateCardNo, this.UserName, this.UserSex, "00", this.UserPaperno, 0);
            //this.ErrRet = 0;
         
            if (this.ErrRet != 0) {
                this.ErrInfo = CZICCARD.ErrInfo;
                return false;
            }
          
            this.CardNo = CZICCARD.CardNo;
            this.SpecialCardType = CZICCARD.CardType;
            this.SpecialCardName = CZICCARD.CardName;
            this.SpecialCardAsn = CZICCARD.AppSn;
            this.SpecialCardTradeNo = CZICCARD.Tradeno;

            //this.CardNo = "44001102";
            //this.SpecialCardType = "P";
            //this.SpecialCardName = "残疾人卡";
            //this.SpecialCardAsn = "12FEA9E2";
            //this.SpecialCardTradeNo = "0000";

            //this.CardNo = "00077040";
            //this.SpecialCardType = "P";
            //this.SpecialCardName = "残疾人卡";
            //this.SpecialCardAsn = "725E5695";
            //this.SpecialCardTradeNo = "005E";

            //this.CardNo = "50054546";
            //this.SpecialCardType = "J";
            //this.SpecialCardName = "高龄人卡";
            //this.SpecialCardAsn = "12FEA9E2";
            //this.SpecialCardTradeNo = "0000";

            //this.CardNo = "00077031";
            //this.SpecialCardType = "J";
            //this.SpecialCardName = "高龄老人卡";
            //this.SpecialCardAsn = "EC59E96A";
            //this.SpecialCardTradeNo = "0000";

            //this.CardNo = "44001115";
            //this.SpecialCardType = "I";
            //this.SpecialCardName = "优惠卡";
            //this.SpecialCardAsn = "EC59E96A";
            //this.SpecialCardTradeNo = "0000";

            //this.CardNo = "00077043";
            //this.SpecialCardType = "I";
            //this.SpecialCardName = "优惠卡";
            //this.SpecialCardAsn = "EC59E96A";
            //this.SpecialCardTradeNo = "0000";
            return true;
        }
        catch (e) {
            return false;
        }
    }

    this.OldPassword = null;
    this.NewPassword = null;
    this.changeUserPassword2 = function () {
        try {
            this.ErrRet = SX_CARDOCX1.ChangeLock2(this.OperateCardNo, this.CardNo,
            this.OldPassword, this.NewPassword);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                $get('hidWarning').value = "writeFail";
                return false;
            }
            $get('hidWarning').value = "writeSuccess";
            return true;
        }
        catch (e) {
            return false;
        }
       
    }

    this.resetPassword2 = function () {
        try {
            this.ErrRet = SX_CARDOCX1.UnLock2(this.OperateCardNo);
            if (this.ErrRet != 0) {
                $get('hidWarning').value = "writeFail";
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                return false;
            }
            $get('hidWarning').value = "writeSuccess";
            return true;
        }
        catch (e) {
            return false;
        }
       
    }

    this.getVerifyCardInfoImpl = function (ex) {
        try {
            //oldbus
            if (getOldBusOCX()) {
                this.ErrRet = ex
            ? CZICCARD.ReadInfoEx(this.OperateCardNo)
            : CZICCARD.ReadInfo(this.OperateCardNo);
                this.ErrRet = CZICCARD.ReadInfo(this.OperateCardNo);
                if (this.ErrRet != 0) {
                    this.ErrInfo = CZICCARD.ErrInfo;
                    this.CardInfo = null;
                }
                else {
                    this.CardInfo = new CardInfo(CZICCARD.CARDNO, CZICCARD.AppType, CZICCARD.AppVersion,
                CZICCARD.AppSn, CZICCARD.AppStartDate, "20501231", CZICCARD.FCI,
                CZICCARD.CardType, CZICCARD.Tradeno.substr(0, 4), CZICCARD.Balance, CZICCARD.StaffTag);
                    this.CardNo = CZICCARD.CARDNO;
                    this.CardPermit = CZICCARD.CardPermit;
                    this.VerifyDate = CZICCARD.StuTag;
                }
                return this.CardInfo;
            }


            this.ErrRet = ex
            ? SX_CARDOCX1.ReadInfoEx(this.OperateCardNo)
            : SX_CARDOCX1.ReadInfo(this.OperateCardNo);
            if (this.ErrRet != 0) {
                this.ErrInfo = SX_CARDOCX1.ErrInfo;
                this.CardInfo = null;
            }
            else {
                this.CardInfo = new CardInfo(SX_CARDOCX1.CARDNO, SX_CARDOCX1.AppType, SX_CARDOCX1.AppVersion,
                SX_CARDOCX1.AppSn, SX_CARDOCX1.AppStartDate, SX_CARDOCX1.AppEndDate, SX_CARDOCX1.FCI,
                SX_CARDOCX1.CardType, SX_CARDOCX1.Tradeno, SX_CARDOCX1.Balance, SX_CARDOCX1.StaffTag);
                this.CardNo = SX_CARDOCX1.CARDNO;

                this.ErrRet = SX_CARDOCX1.ReadStuTag(this.OperateCardNo);
                if (this.ErrRet != 0) {
                    this.ErrInfo = SX_CARDOCX1.ErrInfo;
                    this.VerifyDate = null;
                    this.CardInfo = null;
                }
                else {
                    this.VerifyDate = SX_CARDOCX1.StuTag;

                }
            }
            return this.CardInfo;
        }
        catch (e) {
            return null;
        }

    }

}

var cardReader = new CardReader();


function writeCardImpl() {
    setTimeout(writeCardImplDelay, 50);
}


function writeCardImplDelay() {
    cardReader.writeCardImpl();
}

