﻿

/*//
标题:悬浮div的设计
设计:wdx
日期：2011-04-10
//*/

function showDivClass() {
    this.hot = false;

    this.div_card = document.createElement("div");
    with (this.div_card.style) {
        borderStyle = "none";
        borderWidth = "0";
        display = "none";
        width = "357px";
        height = "194px";
        position = "absolute";
        textAlign = "left";
        background = "transparent url(../../Images/bgUserCard.gif) no-repeat scroll 0 0";
        fontFamily = "Verdana,Arial,Helvetica,sans-serif";
    }
    this.div_card.card = this;
    document.body.appendChild(this.div_card);

    this.subdiv_card = document.createElement("div");
    with (this.subdiv_card.style) {
        top = "12px";
        left = "56px";
        position = "relative";
        width = "290px";
    }

    this.div_card.appendChild(this.subdiv_card);
   
   
}

showDivClass.prototype.close = function () {
    this.div_card.style.display = "none";
    if (typeof this.onclose == "function") this.onclose(this);
}

showDivClass.prototype.show = function (left, top, sender, param1, param2) {
    this.typecode = param1;
    this.typename = param2;
    var userCard = this;
    this.textvalue = "";
    this.div_card.style.left = left + "px";
    this.div_card.style.top = top + "px";
    this.div_card.style.display = "";

    this.subdiv_card.innerHTML = "加载中...";
    try {
        var showvalues = "<div>" + param2 + "</div>";
        var Table = ASP_Warn_WA_BackWarn.LoadDataTable(this.typecode).value;
        var rows = Table.Rows; //注意区分大小写
        var finishflag = 0;
        if (rows.length == 0) {
            this.subdiv_card.innerHTML = "<div>监控出错或未绑定监控对象或监控程序未监控此对象，请速速查看!!!!</div>";
        }
        else {
            for (var i1 = 0, j1 = 0; j1 < rows.length; j1++) {
                if (rows[i1].处理状态 == "监控出错") {
                    finishflag = 2;
                    break;
                }
                if (rows[i1])
                    if (rows[i1].处理状态 != "未完成") {
                        showvalues += "<div style=\"padding:1px;\"><a target=\"_black\" href='WA_BackWarnDetailInfo.aspx?TypeCode=" + this.typecode + "&ObjectID=" + rows[i1].对象ID + "&NodeID=" + rows[i1].节点ID + "&INSTTIME=NOW'>" + (i1 + 1) + "." + rows[i1].告警级别 + ":" + (rows[i1].异常原因 == null ? "无" : rows[i1].异常原因) + ".处理状态:" + rows[i1].处理状态 + "</a></div>";
                        i1++;
                        finishflag = 1;
                    }
                if (i1 > 3) {
                    showvalues += "<div>...</div>";
                    break;
                }
                if (!rows[i1]) {
                    break;
                }
            }
            if (finishflag == 0) {
                this.subdiv_card.innerHTML = "<div>监控未完成</div>";
            }
            else {
                if (finishflag == 2) {
                    this.subdiv_card.innerHTML = "<div>监控出错或未绑定监控对象或监控程序未监控此对象，请速速查看!!!!</div>";
                }
                else {
                    if (showvalues == "")
                        this.subdiv_card.innerHTML = "<div>监控未开始或正常</div>";
                    else
                        this.subdiv_card.innerHTML = showvalues + "<div><a target=\"_black\" href=\"WA_BackWarnDetail.aspx?TypeCode=" + param1 + "\">查看详情</a></div>";

                }

            }
        
        }
      
    }
    catch (ex) {
        this.subdiv_card.innerHTML = ex.description + this.typecode;
    }


    if (top + this.div_card.clientHeight > document.body.clientHeight) {
        this.div_card.style.posTop = document.body.scrollTop + document.body.clientHeight - this.div_card.clientHeight;
    }
    else {
        this.div_card.style.posTop = document.body.scrollTop + top;
    }

    if (this.div_card.clientWidth + left > document.body.clientWidth) {
        this.div_card.style.posLeft = document.body.scrollLeft + document.body.clientWidth - this.div_card.clientWidth;
    }
    else {
        this.div_card.style.posLeft = document.body.scrollLeft + left;
    }



}

showDivClass.userInfos = {};

var currentShowDiv = null;

function showDiv(sender, param1,param2) {
    //if (!window.loaded) return; // 未加载
    if (!currentShowDiv) {
        //if (!panelTopicAdmin) return; 
        currentShowDiv = new showDivClass(); //*/
    }
    var point = absolutePoint(sender);
    currentShowDiv.owner = sender;
    currentShowDiv.show(point.x + 130, point.y,  sender,param1,param2);
    currentShowDiv.mouseover = function (e) {
        var element = typeof event != "undefined" ? event.srcElement : e.target;
        var hotCard = false;
        while (element) {
            hotCard = element == currentShowDiv.owner || element == currentShowDiv.div_card;
            if (hotCard) break;
            element = element.parentNode;
        }
        if (!hotCard) {
            removeEventHandler(document, "mouseover", currentShowDiv.mouseover);
            currentShowDiv.close();
        }
    }
    addEventHandler(document, "mouseover", currentShowDiv.mouseover);
}

/// <summary>
/// 获得元素的绝对坐标
/// </summary>
/// <param name="element">HTML元素</param>
function absolutePoint(element) {
    var result = { x: element.offsetLeft, y: element.offsetTop };
    element = element.offsetParent;
    while (element) {
        result.x += element.offsetLeft;
        result.y += element.offsetTop;
        element = element.offsetParent;
    }
    return result;
}

/// <summary>
/// 添加事件
/// </summary>
/// <param name="target">载体</param>
/// <param name="type">事件类型</param>
/// <param name="func">事件函数</param>
function addEventHandler(target, type, func) {
    if (target.addEventListener)
        target.addEventListener(type, func, false);
    else if (target.attachEvent)
        target.attachEvent("on" + type, func);
    else target["on" + type] = func;
}

/// <summary>
/// 移除事件
/// </summary>
/// <param name="target">载体</param>
/// <param name="type">事件类型</param>
/// <param name="func">事件函数</param>
function removeEventHandler(target, type, func) {
    if (target.removeEventListener)
        target.removeEventListener(type, func, false);
    else if (target.detachEvent)
        target.detachEvent("on" + type, func);
    else delete target["on" + type];
}

 ///告警级别
 function GetYanse(type)
    {
        if (type == "0")
            return "正常";
        else if (type == "1")
            return "警告";
        else if (type == "2")
            return "严重";
        else
            return "未开始";

    }


addEventHandler(window, "load", function () {
    window.loaded = true;
    window.reply_items = null;
});


