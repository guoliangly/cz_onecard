﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.Warn;
using System.Text;

/***************************************************************
 * PR_HDAdvertise.aspx.cs
 * 系统名  : 回单广告

 * 子系统名: 系统管理 -回单广告 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/4/28    王定喜           初次开发
 ***************************************************************
 */
public partial class ASP_Warn_PR_HDAdvertise : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        lvwResult.DataKeyNames = new string[] { "ID","SORTNO", "CONTENT", "ISSHOW" };
        DataBind();
    }


    private void DataBind()
    {
        string sql = "select * from TD_M_HDAdvertise t   order by sortno,isshow desc";
        TMTableModule tm = new TMTableModule();
        UserCardHelper.resetData(lvwResult, tm.selByPKDataTable(context, sql, 0));
    }

    protected void lvwResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwResult','Select$" + e.Row.RowIndex + "')");
        }
    }


    protected void lvwResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSort.Text = getDataKeys2("SORTNO");
        txtContent.Text = getDataKeys2("CONTENT");
        chkIsshow.Checked = getDataKeys2("ISSHOW") == "1" ? true : false;
     
    }

    public String getDataKeys2(String keysname)
    {
        return lvwResult.DataKeys[lvwResult.SelectedIndex][keysname].ToString();
    }

    public void validate()
    {
        if (txtSort.Text != "")
        {
            if (!Common.Validation.isNum(txtSort.Text))
            {
                context.AddError("排序号应为数字",txtSort);
            }
        }
        if (string.IsNullOrEmpty(txtContent.Text))
        {
            context.AddError("回单内容不能为空", txtContent);
        }
        else if ( txtContent.Text.Length > 50)
        {
            context.AddError("回单内容不能大于50个字符", txtContent);
        }
      
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Insert");
            context.ExecuteNonQuery(string.Format("insert into td_m_hdadvertise (SORTNO,CONTENT,ISSHOW,Updatetime,staffno,departno) values('{0}','{1}',{2},sysdate,'{3}','{4}')", txtSort.Text.Trim(),txtContent.Text.Trim(), chkIsshow.Checked ? "1" : "0", context.s_UserID, context.s_DepartID));
            context.DBCommit();
            AddMessage("回单内容添加成功");
            DataBind();
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message);
        }
        
    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
        try
        {
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Update");
            context.ExecuteNonQuery(string.Format("update  td_m_hdadvertise set sortno='{1}',content='{2}',isshow='{3}',updatetime=sysdate,staffno='{4}',departno='{5}' where id={0}", getDataKeys2("ID"), txtSort.Text.Trim(), txtContent.Text.Trim(), chkIsshow.Checked ? "1" : "0",context.s_UserID,context.s_DepartID));
            context.DBCommit();
            AddMessage("回单内容修改成功");
            DataBind();
            ClearType();

        }
        catch (Exception ex)
        {
            context.AddError(ex.Message);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Delete");
            context.ExecuteNonQuery(string.Format("delete from  td_m_hdadvertise where id={0}", getDataKeys2("ID")));
            context.DBCommit();
            AddMessage("回单内容删除成功");
            DataBind();
            ClearType();
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message);
        }
    }

    private void ClearType()
    {
        //清除输入的员工信息

        txtSort.Text = "";
        txtContent.Text = "";
       // txtTypeName.Text = "";
        chkIsshow.Checked = false;
    }
}
