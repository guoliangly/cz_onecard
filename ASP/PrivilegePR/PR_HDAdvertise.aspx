﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PR_HDAdvertise.aspx.cs" Inherits="ASP_Warn_PR_HDAdvertise"  EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>监控任务</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
   <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <div class="tb">
        <table cellpadding="0" cellspacing="0"  style="  width:100%;">
        <tr>
        <td> 系统管理->回单广告</td>
        <td align="right" style=" height:13px;">&nbsp;</td>
        </tr>
        </table>
       
        </div>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

        <div class="con">
  <div class="base">回单广告列表</div>
  <div class="kuang5">
  <div class="gdtb" style="height:300px">
  <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1" >
      
      
      <asp:GridView ID="lvwResult" runat="server"
         Width = "98%"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="false"
        OnRowCreated="lvwResult_RowCreated" 
        OnSelectedIndexChanged="lvwResult_SelectedIndexChanged"
        >
         <Columns>
                <asp:BoundField HeaderText="排序号" DataField="SORTNO"/>
                <asp:BoundField HeaderText="回单内容" DataField="CONTENT"/>
                 <asp:TemplateField HeaderText="是否显示">
                       <ItemTemplate>
                         <asp:Label ID="labDimTag" Text='<%# Eval("ISSHOW").ToString() == "0" ? "否" : "是" %>' runat="server"></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateField>
                <asp:BoundField HeaderText="更新时间" DataField="UPDATETIME"/>
                 <asp:BoundField HeaderText="更新人员" DataField="STAFFNO"/>
                <asp:BoundField HeaderText="更新部门" DataField="DEPARTNO"/>
               
               
                     
            </Columns>   
            
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />        
        </asp:GridView>
    </table>
  </div>
  </div>
  <div class="kuang5">
   <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
     <tr>
       <td width="8%"><div align="right">排列序号:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtSort" runat="server" CssClass="inputmid" MaxLength="16" Width="96px" ></asp:TextBox></td>
       <td width="14%"><div align="right">回单内容:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtContent" runat="server" CssClass="inputmid" MaxLength="50" Width="96px" ></asp:TextBox><span class="red">*</span></td>
       <td width="8%"><div align="right">是否显示:</div></td>
       <td style="height: 37px"><asp:CheckBox ID="chkIsshow" runat="server" /></td>
       <td width="8%"><div align="right">&nbsp;</div></td>
       <td style="height: 37px">&nbsp;</td>
        <td width="2%"><div align="right">&nbsp;</div></td>
       <td style="height: 37px">&nbsp;</td>
     
     </tr>
    
     <tr>
       <td><div align="right"></div></td>
       <td>&nbsp;</td>
       <td colspan="4"><table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
         <tr>
           
           <td><asp:Button ID="btnStaffAdd" runat="server" Text="增加" CssClass="button1" OnClick="btnAdd_Click" /></td>
           <td><asp:Button ID="btnStaffModify" runat="server" Text="修改" CssClass="button1" OnClick="btnModify_Click" /></td>
           <td><asp:Button ID="btnStaffDelete" runat="server" Text="删除" CssClass="button1" OnClick="btnDelete_Click" /></td>
         </tr>
            </table></td>
      </tr>
   </table>
 </div>
         </div>


   <div class="footall"></div>
  
<div class="btns">
     

</div>

</ContentTemplate>
</asp:UpdatePanel>

    </form>
</body>
</html>
