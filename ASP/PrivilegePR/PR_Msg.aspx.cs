﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDO.PrivilegePR;
using Master;
using System.Data;
using TM;

public partial class ASP_PrivilegePR_PR_Msg : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QueryDept";

        DataTable dt = sp.Execute(context, pdo);
        UserCardHelper.resetData(gvDept, dt);

        pdo = new SP_PR_QueryPDO();
        pdo.funcCode = "QueryRole";
        dt = sp.Execute(context, pdo);
        UserCardHelper.resetData(gvRole, dt);

        pdo = new SP_PR_QueryPDO();
        pdo.funcCode = "QueryStaff";
        dt = sp.Execute(context, pdo);
        UserCardHelper.resetData(gvStaff, dt);

        queryRecvMsg();
    }

    protected void querySentMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QuerySentMsg";
        pdo.var1 = context.s_DepartID;
        pdo.var2 = context.s_UserID;

        DataTable dt = sp.Execute(context, pdo);
        UserCardHelper.resetData(gvRecvList, dt);
    }
    protected void queryRecvMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QueryRecvMsg";
        pdo.var1 = context.s_DepartID;
        pdo.var2 = context.s_UserID;

        DataTable dt = sp.Execute(context, pdo);
        UserCardHelper.resetData(gvRecvList, dt);
    }

    protected void transferMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QuerySingleMsg";
        pdo.var1 = hidShowMsgId.Value;

        DataTable dt = sp.Execute(context, pdo);
        if (dt.Rows.Count > 0)
        {
            Object[] row = dt.Rows[0].ItemArray;

            txtTitle.Text = "FW: " + row[3];
            radLevel.SelectedValue = "" + row[4];
            txtBody.Text = "\r\n\r\n---原始消息----\r\n" + row[6];
        }
    }

    protected void replyMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QuerySingleMsg";
        pdo.var1 = hidShowMsgId.Value;

        DataTable dt = sp.Execute(context, pdo);
        if (dt.Rows.Count > 0)
        {
            Object[] row = dt.Rows[0].ItemArray;

            txtRecv.Text = "员工: " + row[1];
            hidMsgToDepts.Value = "";
            hidMsgToRoles.Value = "";
            hidMsgToStaffs.Value = "" + row[2];

            txtTitle.Text = "Re: " + row[3];
            radLevel.SelectedValue = "" + row[4];
            txtBody.Text = "\r\n\r\n---原始消息----\r\n" + row[6];
        }
    }

    protected void showMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "QuerySingleMsg";
        pdo.var1 = hidShowMsgId.Value;
        pdo.var2 = context.s_DepartID;
        pdo.var3 = context.s_UserID;

        DataTable dt = sp.Execute(context, pdo);
        if (dt.Rows.Count > 0)
        {
            Object[] row = dt.Rows[0].ItemArray;

            txtShowMsgger.Text = "" + row[1] + " 发送于 " + ((DateTime)row[5]).ToString("yyyy-MM-dd HH:mm:ss");
            txtShowTitle.Text = "" + row[3];
            selShowLevel.SelectedValue = "" + row[4];
            txtShowBody.Text = "" + row[6];
        }
    }

    protected void delMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "DelMsgList";
        pdo.var1 = hidDelMsgIdList.Value;
        pdo.var2 = context.s_DepartID;
        pdo.var3 = context.s_UserID;

        sp.Execute(context, pdo);
        
        queryRecvMsg();
    }

    protected void delSentMsg()
    {
        SP_PR_QueryPDO pdo = new SP_PR_QueryPDO();
        StoreProScene sp = new StoreProScene();
        pdo.funcCode = "DelSentList";
        pdo.var1 = hidDelMsgIdList.Value;
        pdo.var2 = context.s_DepartID;
        pdo.var3 = context.s_UserID;

        sp.Execute(context, pdo);

        querySentMsg();
    }
    protected void sendMsg()
    {
        SP_PR_SendMsgPDO pdo = new SP_PR_SendMsgPDO();
        pdo.msgtitle = txtTitle.Text;
        pdo.msgbody = txtBody.Text;
        pdo.msglevel = Convert.ToInt32(radLevel.SelectedValue);

        pdo.depts = hidMsgToDepts.Value;
        pdo.roles = hidMsgToRoles.Value;
        pdo.staffs = hidMsgToStaffs.Value;

        pdo.msgpos = 1;
        PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if(!ok) 
        {
            labSentResult.Text = pdoOut.retMsg;
        }
        else
        {
            labSentResult.Text = "消息发送成功!";
        }

    }
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "sendMsg")
        {
            sendMsg();
        }
        else if (hidWarning.Value == "showMsg")
        {
            showMsg();
        }
        else if (hidWarning.Value == "delMsg")
        {
            delMsg();
        }
        else if (hidWarning.Value == "queryRecvMsg")
        {
            queryRecvMsg();
        }
        else if (hidWarning.Value == "transferMsg")
        {
            transferMsg();
        }
        else if (hidWarning.Value == "replyMsg")
        {
            replyMsg();
        }
        else if (hidWarning.Value == "querySentMsg")
        {
            querySentMsg();
        }
        else if (hidWarning.Value == "delSentMsg")
        {
            delSentMsg();
        }

        hidWarning.Value = "";
    }


    protected void gvRecvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[2].Text == "0")
            {
                e.Row.CssClass = "tabsel";
            }

            e.Row.Cells[0].Text = e.Row.Cells[0].Text.Replace("xxxyyy", e.Row.Cells[1].Text);

            string msglevel = e.Row.Cells[5].Text;
            e.Row.Cells[5].Text = msglevel == "0" ? "普通"
                : msglevel == "1" ? "重要"
                : msglevel == "2" ? "紧急"
                : msglevel == "3" ? "特急"
                : "未知";

            e.Row.Attributes.Add("OnDblClick", "showSingleMsg('" + e.Row.Cells[1].Text + "')"); 
        }

        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow ||
            e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
        }

    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        if (dataTable == null)
        {
            return new DataView();
        }

        DataView dataView = new DataView(dataTable);
        if (GridViewSortExpression != string.Empty)
        {
            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression,
                isPageIndexChanging ? GridViewSortDirection : GetSortDirection());
        }
        return dataView;
    }

    protected void gvRecvList_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (hidIsInRecvBox.Value == "true")
        {
            this.queryRecvMsg();
        }
        else
        {
            this.querySentMsg();
        }

        GridViewSortExpression = e.SortExpression;
        gvRecvList.DataSource = SortDataTable(gvRecvList.DataSource as DataTable, false);
        gvRecvList.DataBind();
    }
}
