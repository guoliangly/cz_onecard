﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.BusinessCode;
using TDO.UserManager;
using Common;
using TM;
using System.Data;

public partial class ASP_GroupCard_GC_OrderSubmission : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        //初始化部门

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
        TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
        ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
     
        InitStaffList(context.s_DepartID);
   
        gvOrderList.DataKeyNames = new string[] { "ORDERNO", "GROUPNAME", "NAME", "PHONE", "IDCARDNO", "TOTALMONEY", 
            "TRANSACTOR", "INPUTTIME", "FINANCEAPPROVERNO",  "financeremark",
            "ORDERSTATE","REMARK","cashgiftmoney","CHARGECARDMONEY","CUSTOMERACCMONEY","isrelated"};
    }

    protected void selDept_Changed(object sender, EventArgs e)
    {
        InitStaffList(selDept.SelectedValue);
    }

    private void InitStaffList(string deptNo)
    {
        if (deptNo == "")
        {       
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";
            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
            selStaff.SelectedValue = context.s_UserID;
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";
            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }

    //查询单位名称
    protected void queryCompany(object sender, EventArgs e)
    {
      OrderHelper.queryCompany(context, txtCompany, selCompany);
    }

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selCompany.SelectedIndex > 0)
        {
            txtCompany.Text = selCompany.SelectedItem.Text;
        }
        else
        {
            txtCompany.Text = string.Empty;
        }
    }


    private void queryCompany(TextBox txtCompanyPar, DropDownList selCompanyPar)
    {
        //模糊查询单位名称，并在列表中赋值

        string name = txtCompanyPar.Text.Trim();
        if (name == "")
        {
            selCompanyPar.Items.Clear();
            return;
        }
        TD_M_BUYCARDCOMINFOTDO tdoTD_M_BUYCARDCOMINFOIn = new TD_M_BUYCARDCOMINFOTDO();
        TD_M_BUYCARDCOMINFOTDO[] tdoTD_M_BUYCARDCOMINFOOutArr = null;
        tdoTD_M_BUYCARDCOMINFOIn.COMPANYNAME = "%" + name + "%";
        tdoTD_M_BUYCARDCOMINFOOutArr = (TD_M_BUYCARDCOMINFOTDO[])tm.selByPKArr(context, tdoTD_M_BUYCARDCOMINFOIn, typeof(TD_M_BUYCARDCOMINFOTDO), null, "TD_M_BUYCARDCOMINFO_NAME", null);

        selCompanyPar.Items.Clear();
        if (tdoTD_M_BUYCARDCOMINFOOutArr.Length > 0)
        {
            selCompanyPar.Items.Add(new ListItem("---请选择---", ""));
        }
        foreach (TD_M_BUYCARDCOMINFOTDO ddoComInfo in tdoTD_M_BUYCARDCOMINFOOutArr)
        {
            selCompanyPar.Items.Add(new ListItem(ddoComInfo.COMPANYNAME, ddoComInfo.COMPANYNO));
        }
    }

    /// <summary>
    /// 查询验证
    /// </summary>
    /// <returns></returns>
    private bool ValidInput()
    {
        //校验单位名称长度
        if (!string.IsNullOrEmpty(txtCompany.Text.Trim()))
        {
            if (txtCompany.Text.Trim().Length > 50)
            {
                context.AddError("单位名称长度不能超过50个字符长度");
            }
        }
        //校验联系人长度

        if (!string.IsNullOrEmpty(txtName.Text.Trim()))
        {
            if (txtName.Text.Trim().Length > 50)
            {
                context.AddError("联系人长度不能超过25个字符长度");
            }
        }

        if (txtTotalMoney.Text.Trim().Length > 0) //金额不为空时
        {
            if (!Validation.isPrice(txtTotalMoney.Text.Trim()))
            {
                context.AddError("A094391334:金额输入不正确", txtTotalMoney);
            }
            else if (Convert.ToDecimal(txtTotalMoney.Text.Trim()) <= 0)
            {
                context.AddError("A094391335:金额必须是正数", txtTotalMoney);
            }
        }
        if (txtName.Text.Trim().Length > 10)
        {
            context.AddError("A094391336:联系人长度不超过8位", txtName);
        }
        //对开始日期和结束日期的判断

        UserCardHelper.validateDateRange(context, txtFromDate, txtToDate, false);
        return !(context.hasError());
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!ValidInput())
        {
            return;
        }
        string groupName = txtCompany.Text.Trim();
        string name = txtName.Text.Trim();
        string staff = "";
        if (selStaff.SelectedIndex > 0)
        {
            staff = selStaff.SelectedValue;
        }
        string money = "";
        if (txtTotalMoney.Text.Trim().Length > 0)
        {
            money = (Convert.ToDecimal(txtTotalMoney.Text.Trim()) * 100).ToString();
        }
        string fromDate = txtFromDate.Text.Trim();
        string endDate = txtToDate.Text.Trim();

        DataTable dt = GroupCardHelper.callOrderQuery(context, "GotCardOrderQuery", groupName, name, staff, money, fromDate, endDate, selDept.SelectedValue,context.s_DepartID);
        if (dt == null || dt.Rows.Count < 1)
        {
            gvOrderList.DataSource = new DataTable();
            gvOrderList.DataBind();
            context.AddError("A094391337:未查出领用完成的记录");
            return;
        }
        gvOrderList.DataSource = dt;
        gvOrderList.DataBind();
    }

    protected void gvOrderList_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvOrderList','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void gvOrderList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择员工GRIDVIEW中的一行记录
        string orderno = getDataKeys2("ORDERNO");
        ViewState["orderno"] = orderno;
        string groupName = getDataKeys2("GROUPNAME");
        string name = getDataKeys2("NAME");
        string phone = getDataKeys2("PHONE");
        string idCardNo = getDataKeys2("IDCARDNO");
        string totalMoney = getDataKeys2("TOTALMONEY");
        string transactor = getDataKeys2("TRANSACTOR");
        string remark = getDataKeys2("REMARK");
        string totalCashGiftChargeMoney = getDataKeys2("cashgiftmoney");
        string customeraccmoney = getDataKeys2("CUSTOMERACCMONEY");
        string financeRemark = getDataKeys2("financeremark");
        string approver = getDataKeys2("FINANCEAPPROVERNO");
        string chargecardmoney = getDataKeys2("CHARGECARDMONEY");
     
        divInfo.InnerHtml = OrderHelper.GetOrderHtmlString(context, orderno, groupName,
            name, phone, idCardNo, totalMoney, transactor,
            remark, "0", financeRemark, totalCashGiftChargeMoney, approver, customeraccmoney, "", "", false, false,"1");
    }

    public String getDataKeys2(String keysname)
    {
        return gvOrderList.DataKeys[gvOrderList.SelectedIndex][keysname].ToString();
    }

    protected void btnGetCard_Click(object sender, EventArgs e)
    {
        if (gvOrderList.SelectedIndex < 0)
        {
            context.AddError("A007P02020: 请选择一个订单");
            return;
        }
        string isrelated = getDataKeys2("isrelated");//判断是否关联
        string orderstate = getDataKeys2("ORDERSTATE").Substring(0,2);
        if (isrelated == "0" && orderstate == "07")//如果是领卡完成状态且isrelated == "0"时
        {
            context.AddError("A007P02021: 此订单需要先完成领卡补关联");
            return;
        }

        if (context.hasError())
        {
            return;
        }
        string orderno = getDataKeys2("ORDERNO");
        string transactor = getDataKeys2("TRANSACTOR");
        string groupName = getDataKeys2("GROUPNAME");
        transactor = transactor.Split(':')[0];
        
        //查询订单付款方式
        string sql = "select PAYTYPECODE from TF_F_PAYTYPE where ORDERNO = '" + getDataKeys2("ORDERNO") + "'";
        context.DBOpen("Select");
        DataTable data = context.ExecuteReader(sql);
        string payMode = "1";  // 默认现金
        for (int i = 0; i < data.Rows.Count; i++)
        {
            //如果订单中付款方式有选择呈批单/特殊业务申请单，则为报销
            if (data.Rows[i]["PAYTYPECODE"].ToString() == "4")
            {
                payMode = "2";//报销
            }
        }
        for (int i = 0; i < data.Rows.Count; i++)
        {
            //如果订单中付款方式有选择转账，则为转账

            if (data.Rows[i]["PAYTYPECODE"].ToString() == "1")
            {
                payMode = "0";//转账
            }
        }

        //查询到账单
        string sqldate = "select b.TRADEDATE  from TF_F_ORDERCHECKRELATION a,TF_F_CHECK b where a.ORDERNO = '" + getDataKeys2("ORDERNO") + "' and a.CHECKID = b.CHECKID";
        context.DBOpen("Select");
        data = context.ExecuteReader(sqldate);

        string accRecv = data.Rows.Count > 0 ? "1" : "0"; // 到账标记

        string recvDate = data.Rows.Count > 0 ? data.Rows[0]["TRADEDATE"].ToString() : DateTime.Today.ToString("yyyyMMdd"); // 到账日期

        context.DBCommit();

        context.SPOpen();
        context.AddField("p_orderNo").Value = orderno;
        context.AddField("p_transactor").Value = transactor;
        context.AddField("p_custName").Value = groupName;
        context.AddField("p_payMode").Value = payMode;
        context.AddField("p_accRecv").Value = accRecv;
        context.AddField("p_recvDate").Value = recvDate;
        context.AddField("p_isremind").Value = chkRemind.Checked ? "1" : "0";
        context.AddField("p_remark").Value = "";
        bool ok = context.ExecuteSP("SP_GC_GetCardConfirm");
        if (ok)
        {
            context.DBCommit();
            context.AddMessage("A007P02021:订单完成确认成功");
            SynReaderData();
            btnQuery_Click(sender, e);
        }
    }

    private void SynReaderData()
    {
        DataTable dt = GetOrderReaderData();

        if (null == dt || dt.Rows.Count <= 0)
            return;

        string BeginReaderNo = dt.Rows[0]["BEGINSERIALNUMBER"].ToString();
        string EndReaderNo = dt.Rows[0]["ENDSERIALNUMBER"].ToString();
        int Num = Convert.ToInt32(dt.Rows[0]["NUM"].ToString());

        long ReaderNo = 0;
        bool ISAllSuc = true;
        string RetMsg = string.Empty;
        for (long i = Convert.ToInt64(BeginReaderNo); i <= Convert.ToInt64(EndReaderNo); i++)
        {
            string Datagram = ReaderHelper.GetSaleDatagram(context, new string('0', 16 - i.ToString().Length) + i.ToString());
            bool ISSuc = false;
            RetMsg = ReaderHelper.ReaderDataSYN(Datagram, ref ISSuc);
            if (!ISSuc)
            {
                ReaderNo = i;
                ISAllSuc = false;
                break;
            }
        }
        if (!ISAllSuc)
        {
            context.AddError(ReaderNo + "-" + EndReaderNo + "号段的充付器同步出现异常:" + RetMsg);
        }
        else
        {
            context.AddMessage("全部同步成功");
        }
    }

    private DataTable GetOrderReaderData()
    {
        string orderno = getDataKeys2("ORDERNO");
        string strSql = string.Format("select ORDERNO,BEGINSERIALNUMBER,ENDSERIALNUMBER,VALUE PRICE,COUNT NUM from TF_F_READERRELATION where ORDERNO = '{0}'", orderno);
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();

        return dt;
    }
}