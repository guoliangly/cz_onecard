﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using Common;
using TDO.ChargeCard;
using Master;
using PDO.ChargeCard;
using TDO.UserManager;

public partial class ASP_ChargeCard_CC_Query : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        DataTable data = ChargeCardHelper.callQuery(context, "F8");
        GroupCardHelper.fill(selCardState, data, true);

        UserCardHelper.resetData(gvResult, null);

        //领用部门
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTTDOIn = new TD_M_INSIDEDEPARTTDO();
        TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTTDOOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTTDOIn, typeof(TD_M_INSIDEDEPARTTDO), null, null, null);
        ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTTDOOutArr, "DEPARTNAME", "DEPARTNO", true);
    
    }
    /// <summary>
    /// 初始化领用人
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void selDept_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        tdoTD_M_INSIDESTAFFIn.DEPARTNO = selDept.SelectedValue;
        TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFFROLE1010", null);
        ControlDeal.SelectBoxFill(selAssignedStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        if (selAssignedStaff.Items.Count > 1)
        {
            selAssignedStaff.Items[1].Selected = true;
        }
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        ChargeCardHelper.validateCardNoRange(context, txtFromCardNo, txtToCardNo, false);

        if (context.hasError())
        {
            UserCardHelper.resetData(gvResult, null);

            return;
        }

        DataTable data = ChargeCardHelper.callQuery(context, "F6",
            txtFromCardNo.Text, txtToCardNo.Text, selCardState.SelectedValue,selDept.SelectedValue,selAssignedStaff.SelectedValue);
        UserCardHelper.resetData(gvResult, data);

        if (data == null || data.Rows.Count == 0 )
        {
            AddMessage("N007P00001: 查询结果为空");
        }
    }

    protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void lvwQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ControlDeal.RowDataBound(e);
    }
}
