﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.ChargeCard;
using TDO.ChargeCard;
using Master;
using System.IO;
using System.Text;

// 充值卡前台售卡
public partial class ASP_ChargeCard_CC_Produce : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化面值
            string sqlPROJ = "select valuecode,valuecode||':'||value valuecontent from TP_XFC_CARDVALUE order by valuecode asc";
            TMTableModule tmTMTableModule = new TMTableModule();
            DataTable dt = tm.selByPKDataTable(context, sqlPROJ, 0);
            FillDropDownList(drpCardVlaue, dt, "valuecontent", "valuecode");
            

            //初始化厂家
            sqlPROJ = "select corpcode,corpcode||':'||corpname corpname from TP_XFC_CORP order by corpcode asc";
            dt = tm.selByPKDataTable(context, sqlPROJ, 0);
            FillDropDownList(drpCorp, dt, "corpname", "corpcode");

            //起始卡号生成
            sqlPROJ = "select nvl(max(nvl(substr( XFCARDNO,7,8)+1,'00000000')),'00000000') from TD_XFC_INITCARD";
            dt = tm.selByPKDataTable(context, sqlPROJ, 0);
            txtFromCardNo.Text = dt.Rows[0][0].ToString();
        }
    }

    private void FillDropDownList(DropDownList control, DDOBase[] data, string label, string value)
    {
        control.Items.Add(new ListItem("---请选择---", ""));
        foreach (DDOBase ddoDDOBase in data)
        {
            control.Items.Add(new ListItem(ddoDDOBase.GetString(label), ddoDDOBase.GetString(value)));
        }
    }

    private void FillDropDownList(DropDownList control, DataTable data, string label, string value)
    {
        control.Items.Add(new ListItem("---请选择---", ""));
        foreach(DataRow ddoDDOBase in data.Rows)
        {
            control.Items.Add(new ListItem(ddoDDOBase[label].ToString(), ddoDDOBase[value].ToString()));
        }
    }

    // 查询/修改 按钮点击事件
    protected void btnQuery_Click(object sender, EventArgs e)
    {

        setReadOnly(txtFromCardNo, null);
        btnSubmit.Enabled = true;
    }

    // 点击“提交”按钮    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        //判断私钥是否上传
        if (!FileUpload1.HasFile)
        {
            context.AddError("A004P01F00: 没有上传任何文件");
            return;
        }

        int len = FileUpload1.FileBytes.Length;

        if (len > 5 * 1024 * 1024) // 5M
        {
            context.AddError("A004P01F02: 上传文件大小不能超过5M");
            return;
        }

        // 调用充值卡销售存储过程
        SP_CC_StockOutPDO pdo = new SP_CC_StockOutPDO();
        pdo.fromCardNo = txtFromCardNo.Text;            // 起始卡号
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("D007P04C01: 充值卡出库成功");
        }

        foreach (Control con in this.Page.Controls)
        {
            ClearControl(con);
        }

        // 重置页面
        btnSubmit.Enabled = false;
        clearReadOnly(txtFromCardNo, null);
    }

}
