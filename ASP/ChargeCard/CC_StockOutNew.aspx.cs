﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TM;
using Common;
using Master;
using PDO.ChargeCard;

/*************************************
 * chenwentao 201409
 * 充值卡出库
 * ***********************************/

public partial class ASP_ChargeCard_CC_StockOutNew : Master.Master
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDL();
            UserCardHelper.selectDepts(context,selDept, false); 
        }
    }

    /// <summary>
    /// 绑定面额
    /// </summary>
    private void BindDDL()
    {
        string sqlPROJ = "select valuecode,valuecode||':'||value valuecontent from TP_XFC_CARDVALUE order by valuecode asc";
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable dt = tm.selByPKDataTable(context, sqlPROJ, 0);
        ddlMoney.DataTextField = "valuecontent";
        ddlMoney.DataValueField = "valuecode";
        ddlMoney.DataSource = dt;
        ddlMoney.DataBind();

        ddlMoney.Items.Insert(0,new ListItem("---请选择---","0"));

        ddlValue.DataTextField = "valuecontent";
        ddlValue.DataValueField = "valuecode";
        ddlValue.DataSource = dt;
        ddlValue.DataBind();

        ddlValue.Items.Insert(0, new ListItem("---请选择---", "0"));
    }

    #endregion

    #region Event

    /// <summary>
    /// 根据输入的条件查询出可以出库的充值卡
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        // 检查起始卡号和终止卡号的格式（非空、14位、英数）
       /* long quantity = ChargeCardHelper.validateCardNoRange(context,txtBeginNo_Sel,txtEndNo_Sel);
        if (context.hasError())
        {
            return;
        }
        //起始卡号和终止卡号都存在
        int countE = ChargeCardHelper.queryCountByState(context,txtBeginNo_Sel,txtEndNo_Sel, "");
        if (countE != quantity)
        {
            context.AddError("部分充值卡卡号库存中不存在");
            return;
        }*/

        DataTable data = ChargeCardHelper.callQuery(context,"StockOut",ddlMoney.SelectedValue,txtBeginNo_Sel.Text.Trim(),txtEndNo_Sel.Text.Trim());
        UserCardHelper.resetData(gvResult, data);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
        }
    }

    /// <summary>
    /// 选择面额后查询当前面额下可以出库的最小充值卡卡号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlMoney_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtBeginNo.Text = "";
        txtCount.Text = "";
        txtEndNo.Text = "";
        if (!("0".Equals(ddlValue.SelectedValue)))
        {
            string strSql = string.Format("select rownum,tt.XFCARDNO from(select t.* from TD_XFC_INITCARD t where valuecode='{0}' and cardstatecode='2' order by xfcardno asc)tt WHERE rownum='1'", ddlValue.SelectedValue);
            context.DBOpen("Select");
            DataTable dt = context.ExecuteReader(strSql);
            context.DBCommit();
            if (dt.Rows.Count > 0)
            {
                txtBeginNo.Text = dt.Rows[0]["XFCARDNO"].ToString();
            }
            else
            {
                txtBeginNo.Text = "";
                context.AddError("当前面额不存在可以出库的充值卡！", ddlValue);
                return;
            }
        }
    }

    /// <summary>
    /// 出库充值卡
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (!ValidateCardNo())
        {
            return;
        }
        SP_CC_StockOutPDO pdo = new SP_CC_StockOutPDO();
        pdo.fromCardNo = txtBeginNo.Text.Trim();
        pdo.toCardNo = txtEndNo.Text.Trim();
        pdo.assignDepartNo = selDept.SelectedValue;
        bool ok = TMStorePModule.Excute(context, pdo);
        if (ok)
        {
            AddMessage("D007P04C01: 充值卡出库成功");
        }

        foreach (Control con in this.Page.Controls)
        {
            ClearControl(con);
        }
    }

    /// <summary>
    /// 计算出终止卡号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtCount_TextChanged(object sender, EventArgs e)
    {
        Validation valid = new Validation(context);
        bool b1 = true? valid.notEmpty(txtBeginNo, "A007P01001: 起始卡号不能为空"): !Validation.isEmpty(txtBeginNo);
        if (b1)
        {
            b1 = valid.fixedLength(txtBeginNo, 14, "A007P01002: 起始卡号长度必须是14位");
        }
        if (b1)
        {
            ChargeCardHelper.validateCardNo(context, txtBeginNo, "A007P01003: 起始卡号格式不正确");
        }
        if (string.IsNullOrEmpty(txtCount.Text.Trim()))
        {
            context.AddError("出库充值卡数量不能为空！",txtCount);
        }
        if (!IsNum(txtCount.Text.Trim()))
        {
            context.AddError("出库充值卡数量必须是数字！", txtCount);
        }
        if (context.hasError())
        {
            return;
        }
        hidValue.Value = GetCardValue();
        txtEndNo.Text = GetEndCardNo();
    }

    /// <summary>
    /// 选择起始卡号后计算出终止卡号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtBeginNo_TextChanged(object sender, EventArgs e)
    {
        hidValue.Value=GetCardValue();
        if (!"0".Equals(ddlValue.SelectedValue))
        {
            string ValueSql = string.Format("select Value from TP_XFC_CARDVALUE where valuecode='{0}'", ddlValue.SelectedValue);
            context.DBOpen("Select");
            DataTable dt = context.ExecuteReader(ValueSql);
            context.DBCommit();
            if (dt.Rows[0]["Value"].ToString() != hidValue.Value)
            {
                context.AddMessage("输入的起始充值卡号和选择的面额金额不同，请确认是否卡号输入有误,如无误请继续操作！");
            }
        }
        if (string.IsNullOrEmpty(txtCount.Text.Trim()))
        {
            return;
        }
        Validation valid = new Validation(context);
        bool b1 = valid.fixedLength(txtBeginNo, 14, "A007P01002: 起始卡号长度必须是14位");
        if (b1)
        {
            ChargeCardHelper.validateCardNo(context, txtBeginNo, "A007P01003: 起始卡号格式不正确");
        }
        if (context.hasError())
        {
            return;
        }
        txtEndNo.Text = GetEndCardNo();
    }

    #endregion

    #region Function

    /// <summary>
    /// 提交前验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateCardNo()
    {
        // 检查起始卡号和终止卡号的格式（非空、14位、英数）
        long quantity = ChargeCardHelper.validateCardNoRange(context,txtBeginNo,txtEndNo);
        if (context.hasError())
        {
            return false;
        }
        //起始卡号和终止卡号都存在
        int countE = ChargeCardHelper.queryCountByState(context, txtBeginNo, txtEndNo, "");
        if (countE != quantity)
        {
            context.AddError("部分充值卡卡号库存中不存在！");
            return false;
        }

        if (string.IsNullOrEmpty(selDept.SelectedValue))
        {
            context.AddError("领用部门必须选择！",selDept);
            return false;
        }
        if (string.IsNullOrEmpty(txtCount.Text))
        {
            context.AddError("出库充值卡数量不能为空!");
            return false;
        }
        return true;
    }

    /// <summary>
    /// 计算终止卡号
    /// </summary>
    /// <returns></returns>
    private string GetEndCardNo()
    {
        long Start = Convert.ToInt64(txtBeginNo.Text.Trim().Substring(6, 8));
        long EndNum = Start + (Convert.ToInt64(txtCount.Text.Trim()) - 1);
        int EndLength = EndNum.ToString().Length;
        //充值卡后面位数不足8位前面补0
        string EndStr = (EndLength == 8) ? (EndNum.ToString()) : (new string('0', (8 - EndLength)) + EndNum.ToString());
        string EndNo = txtBeginNo.Text.Trim().Substring(0, 6) + EndStr.ToString();

        return EndNo;
    }

    /// <summary>
    /// 查询面额
    /// </summary>
    private string GetCardValue()
    {
        //查询面额
        string ValueSql = string.Format("select Value from TP_XFC_CARDVALUE t1,TD_XFC_INITCARD t2 where t2.xfcardno='{0}' and t2.valuecode=t1.valuecode", txtBeginNo.Text.Trim());
        context.DBOpen("Select");
        DataTable DataValue = context.ExecuteReader(ValueSql);
        context.DBCommit();
        if (DataValue.Rows.Count > 0)
        {
            return DataValue.Rows[0]["Value"].ToString();
        }
        return "";
    }

    /// <summary>
    /// 判断字符串是否为数字，是返回true
    /// </summary>
    /// <param name="text">输入的字符串</param>
    /// <returns></returns>
    private bool IsNum(string text)
    {
        for (int i = 0; i < text.Length; i++)
        {
            if (!Char.IsDigit(text, i))
            {
                return false;
            }
        }
        return true;
    }

    #endregion
}