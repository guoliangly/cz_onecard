﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CC_FinanceQuery.aspx.cs" Inherits="ASP_ChargeCard_CC_FinanceQuery" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <title>财务查询</title>
    <style type="text/css">
    .breakInvoice{word-break:break-all; width:150px;}
    </style>
    <script type="text/javascript">
    <!--
    function printFaPiao()
    {
        printdiv('printfapiao');
    }
       //-->
    </script>
</head>
<body> 
    <form id="form1" runat="server">
    
    <div class="tb">
		充值卡 -> 财务查询
	</div>
	
	<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager2" />
        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
			function BeginRequestHandler(sender, args){
			    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
			}
			function EndRequestHandler(sender, args) {
			    try {MyExtHide(); } catch(ex){}
			}
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="inline">  
            <ContentTemplate>  
	
	<!-- #include file="../../ErrorMsg.inc" --> 
	<aspControls:PrintFaPiao ID="ptnFaPiao" runat="server" PrintArea="printfapiao" />
	<div class="con">
	
		<div class="card">
            财务查询
		</div>
  
		<div class="kuang5">
		
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                <tr>
                    <td width="5%"><div align="right">充值卡号:</div></td>
                    <td width="20%" colspan="2">
                            <asp:TextBox ID="txtXFBeginNo" CssClass="input" runat="server" MaxLength="14"></asp:TextBox>
                                -
                            <asp:TextBox ID="txtXFEndNo" CssClass="input" runat="server" MaxLength="14"></asp:TextBox>
                    </td>
                    <td width="5%"><div align="right">归属部门:</div></td>
                    <td width="15%">
                    <asp:DropDownList ID="selDept"  runat="server"  CssClass="inputmid"  AutoPostBack="true" OnSelectedIndexChanged="selDept_Changed"></asp:DropDownList>
                       </td> 
                   <td width="5%"><div align="right"> 归属人:</div></td>
                   <td width="15%" align="left">
                     <asp:DropDownList ID="selStaff" CssClass="inputmid"  runat="server"></asp:DropDownList>
                    </td>
                    <td width="5%"></td>
                    <td width="10%"></td>
                </tr>
                <tr>
                    <td width="5%"><div align="right">截至日期:</div></td>
                    <td width="20%" colspan="2">
                        <asp:TextBox ID="txtBeginDate" CssClass="input" runat="server" MaxLength="10"></asp:TextBox>
                            -
                        <asp:TextBox ID="txtEndDate" CssClass="input" runat="server" MaxLength="10"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="BeginCalendar" runat="server" TargetControlID="txtBeginDate" Format="yyyyMMdd" />
                        <ajaxToolkit:CalendarExtender ID="EndCalendar" runat="server" TargetControlID="txtEndDate" Format="yyyyMMdd" />
                    </td>
                    <td align="right">面值:</td>
                    <td>
                       <asp:DropDownList ID="selCardValue" CssClass="input" runat="server">
                       </asp:DropDownList>
                   </td>
                   <td width="5%"><div align="right">状态:</div></td>
                   <td width="15%">
                   <asp:DropDownList ID="selState" CssClass="input" runat="server">
                   <asp:ListItem Text="---请选择---" Value="99"></asp:ListItem>
                   <asp:ListItem Text="2:入库" Value="2"></asp:ListItem>
                   <asp:ListItem Text="3:出库" Value="3"></asp:ListItem>
                   <asp:ListItem Text="4:激活" Value="4"></asp:ListItem>
                   <asp:ListItem Text="5:使用" Value="5"></asp:ListItem>
                   </asp:DropDownList>
                   </td>
                   <td></td>
                    <td>
                    <asp:Button ID="Button1" CssClass="button1" Text="查询" OnClick="btnQuery_Click" runat="server" />
                    </td>
                    
                </tr>
            </table>		
			
            <div class="gdtb" style="height:400px;padding:5px 0 0 0;">
                <asp:GridView ID="gvResult" runat="server"
                    Width = "98%"
                    CssClass="tab1"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="200"
                    OnPageIndexChanging="gvResult_Page"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="False"
                    OnRowDataBound="gvResult_RowDataBound">
                    <Columns>
                          <asp:TemplateField>
                            <HeaderTemplate>#</HeaderTemplate>
                            <ItemTemplate>
                              <%#Container.DataItemIndex + 1%>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:BoundField DataField="XFCARDNO"   HeaderText="卡号"/>
                        <asp:BoundField DataField="MONEY"   HeaderText="面值"/>
                        <asp:BoundField DataField="CARDSTATE"    HeaderText="状态"/>
                        <asp:BoundField DataField="DEPARTNAME"  HeaderText="归属部门"/>
                        <asp:BoundField DataField="STAFFNAME"  HeaderText="归属员工"/>
                        <asp:BoundField DataField="OPTIME"  HeaderText="操作时间"/>
                        <asp:BoundField DataField="TRADEID"  HeaderText="流水号"/>
                        <asp:BoundField DataField="CARDSTATECODE" HeaderText="状态编码" Visible="false"/>
                    </Columns>
                    <EmptyDataTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
	                        <tr class="tabbt">
                                <td>#</td>
                                <td>卡号</td>
                                <td>面值</td>
                                <td>状态</td>
                                <td>归属部门</td>
                                <td>归属员工</td>
                                <td>操作时间</td>
                                <td>流水号</td>
                           </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            
            <div style="padding:5px 0 0 0">
                <table width="95%" border="0" cellpadding="0" cellspacing="0" class="tab1">
	                <tr>
                     <td width="8%"><div align="right">充值卡总数量：</div></td>
                        <td width="7%">
                            <asp:Label ID="labXFCnt" runat="server"></asp:Label>
                        </td>
                        <td width="8%"><div align="right">总金额：</div></td>
                        <td width="7%">
                            <asp:Label ID="labXFMoney" runat="server"></asp:Label>
                        </td>
                        <td width="8%"><div align="right">入库数：</div></td>
                        <td width="7%">
                            <asp:Label ID="labStIn" runat="server"></asp:Label>
                        </td>
                        <td width="8%"><div align="right">出库数：</div></td>
                        <td width="7%">
                            <asp:Label ID="labStOut" runat="server"></asp:Label>
                        </td>
                        <td width="8%"><div align="right">激活数：</div></td>
                        <td width="7%">
                            <asp:Label ID="labStAc" runat="server"></asp:Label>
                        </td>
                        <td width="8%"><div align="right">使用数：</div></td>
                        <td width="7%">
                            <asp:Label ID="labStUse" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
			
		</div>
		
    </div>

    	        </ContentTemplate>  
        </asp:UpdatePanel>  
     
    </form>
             
</body>
</html>
