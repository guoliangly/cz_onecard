﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using Common;
using TDO.InvoiceTrade;
using PDO.UserCard;
using Master;
using TDO.UserManager;

public partial class ASP_ChargeCard_CC_FinanceQuery : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            //初始化表头

            initTable();

            //初始化面值
            string sqlPROJ = "select valuecode,valuecode||':'||value valuecontent from TP_XFC_CARDVALUE order by valuecode asc";
            TMTableModule tmTMTableModule = new TMTableModule();
            DataTable dt = tm.selByPKDataTable(context, sqlPROJ, 0);
            FillDropDownList(selCardValue, dt, "valuecontent", "valuecode");

            //初始化部门
            TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTTDOIn = new TD_M_INSIDEDEPARTTDO();
            TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTTDOOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTTDOIn, typeof(TD_M_INSIDEDEPARTTDO), null, null, null);
            ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTTDOOutArr, "DEPARTNAME", "DEPARTNO", true);


            selDept.SelectedValue = context.s_DepartID;
            selDept.Enabled = true;

            selDept_Changed(sender, e);
        }
    }
    //初始化表头

    public void initTable()
    {
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
        gvResult.SelectedIndex = -1;

    }

    private void FillDropDownList(DropDownList control, DataTable data, string label, string value)
    {
        control.Items.Add(new ListItem("---请选择---", ""));
        foreach (DataRow ddoDDOBase in data.Rows)
        {
            control.Items.Add(new ListItem(ddoDDOBase[label].ToString(), ddoDDOBase[value].ToString()));
        }
    }

    //查询
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!InputValidate())
            return;

        DataTable data = ChargeCardHelper.callQuery(context, "FinanceQuery", txtXFBeginNo.Text, txtXFEndNo.Text, selDept.SelectedValue, selStaff.Text, txtBeginDate.Text, txtEndDate.Text, selCardValue.SelectedValue, selState.SelectedValue);

        gvResult.DataSource = data.DefaultView;
        gvResult.DataBind();

        //DataTable dataLog = ChargeCardHelper.callQuery(context, "XFCOUNT", "", "", "", txtEndDate.Text);

        gvResult.SelectedIndex = -1;

        int XFCnt = 0;                  //充值卡总数量
        int XFMoney = 0;                //总金额
        long StIn = 0;                   //入库数
        long StOut = 0;                  //出库数
        long StAc = 0;                   //激活数
        long StUse = 0;                  //使用数

        foreach (DataRow item in data.Rows)
        {
            if (item["CARDSTATE"].ToString() == "入库")      //入库
            {
                StIn = StIn + 1;
            }
            else if (item["CARDSTATE"].ToString() == "出库")      //出库
            {
                StOut = StOut + 1;
            }
            else if (item["CARDSTATE"].ToString() == "激活")      //激活
            {
                StAc = StAc + 1;
            }
            else if (item["CARDSTATE"].ToString() == "使用")      //使用
            {
                StUse = StUse + 1;
            }

            XFCnt = XFCnt + 1;
            XFMoney = XFMoney + Int32.Parse(item["MONEY"].ToString());
        }
        labXFCnt.Text = XFCnt.ToString();
        labXFMoney.Text = (Convert.ToDouble(XFMoney) / 100).ToString("0.00");
        labStIn.Text = StIn.ToString();
        labStOut.Text = StOut.ToString();
        labStAc.Text = StAc.ToString();
        labStUse.Text = StUse.ToString();

        //foreach (DataRow item in dataLog.Rows)
        //{
        //    if (item["OPERTYPECODE"].ToString() == "02")      //入库
        //    {
        //        StIn = StIn + GetCountForXFCard(item["STARTCARDNO"].ToString(), item["ENDCARDNO"].ToString());
        //    }
        //    else if (item["OPERTYPECODE"].ToString() == "03")      //出库
        //    {
        //        StOut = StOut + GetCountForXFCard(item["STARTCARDNO"].ToString(), item["ENDCARDNO"].ToString());
        //    }
        //    else if (item["OPERTYPECODE"].ToString() == "04")      //激活
        //    {
        //        StAc = StAc + GetCountForXFCard(item["STARTCARDNO"].ToString(), item["ENDCARDNO"].ToString());
        //    }
        //    else if (item["OPERTYPECODE"].ToString() == "05")      //使用
        //    {
        //        StUse = StUse + GetCountForXFCard(item["STARTCARDNO"].ToString(), item["ENDCARDNO"].ToString());
        //    }

        //    XFCnt = XFCnt + 1;
        //    XFMoney = XFMoney + Int32.Parse(item["MONEY"].ToString());
        //    if (item["CARDSTATE"].ToString() == "入库")
        //    {
        //        StIn = StIn + 1;
        //    }
        //    else if (item["CARDSTATE"].ToString() == "出库")
        //    {
        //        StOut = StOut + 1;
        //    }
        //    else if (item["CARDSTATE"].ToString() == "激活")
        //    {
        //        StAc = StAc + 1;
        //    }
        //    else if (item["CARDSTATE"].ToString() == "使用")
        //    {
        //        StUse = StUse + 1;
        //    }
        //}
        //labXFCnt.Text = XFCnt.ToString();
        //labXFMoney.Text = (Convert.ToDouble(XFMoney)).ToString("0.00");
        //labStIn.Text = StIn.ToString();
        //labStOut.Text = StOut.ToString();
        //labStAc.Text = StAc.ToString();
        //labStUse.Text = StUse.ToString();


    }

    private long GetCountForXFCard(string stateCardNo, string endCardNo)
    {
        long fromCard = -1; long endCard = -1;
        fromCard = Convert.ToInt64(stateCardNo.Substring(6, 8));

        endCard = Convert.ToInt64(endCardNo.Substring(6, 8));

        long quantity = endCard - fromCard + 1;

        return quantity;
    }




    //初始化领用人
    protected void selDept_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        tdoTD_M_INSIDESTAFFIn.DEPARTNO = selDept.SelectedValue;
        TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDEDEPART", null);
        ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[2].Text = (Convert.ToDouble(e.Row.Cells[2].Text) / 100).ToString("0.00");
        }
    }


    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);

        gvResult.SelectedIndex = -1;
    }

    private string getDataKeys(string keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    private string getDateDataKeys(string keysname)
    {
        try
        {
            DateTime date = (DateTime)gvResult.DataKeys[gvResult.SelectedIndex][keysname];
            return date.ToShortDateString();
        }
        catch (Exception)
        {
            return "";
        }
    }

    private bool InputValidate()
    {
        Validation valid = new Validation(context);

        if (!string.IsNullOrEmpty(txtXFBeginNo.Text))
        {
            bool b1 = valid.fixedLength(txtXFBeginNo, 14, "A007P01002: 起始卡号长度必须是14位");
            if (b1) ChargeCardHelper.validateCardNo(context, txtXFBeginNo, "A009478007:充值卡起始卡号格式不正确");
        }

        if (!string.IsNullOrEmpty(txtXFEndNo.Text))
        {
            bool b2 = valid.fixedLength(txtXFEndNo, 14, "A007P01005: 终止卡号长度必须是14位");
            if (b2) ChargeCardHelper.validateCardNo(context, txtXFEndNo, "A009478008:充值卡终止卡号格式不正确");
        }

        //if (!string.IsNullOrEmpty(txtXFBeginNo.Text) && !ChargeCardHelper.validateCardNo(txtXFBeginNo.Text))
        //{
        //    context.AddError("A009478007:充值卡起始卡号格式不正确", txtXFBeginNo);
        //}

        //if (!string.IsNullOrEmpty(txtXFEndNo.Text) && !ChargeCardHelper.validateCardNo(txtXFEndNo.Text))
        //{
        //    context.AddError("A009478008:充值卡终止卡号格式不正确", txtXFEndNo);
        //}

        if (!context.hasError() && !string.IsNullOrEmpty(txtXFBeginNo.Text) && !string.IsNullOrEmpty(txtXFEndNo.Text))
        {
            long fromCard = Convert.ToInt64(txtXFBeginNo.Text.Substring(6, 8));

            long toCard = Convert.ToInt64(txtXFEndNo.Text.Substring(6, 8));

            if (fromCard >= 0 && toCard >= 0)
            {
                long quantity = toCard - fromCard + 1;
                if (quantity < 1)
                {
                    context.AddError("A007P01007: 终止卡号不能小于起始卡号");
                }
            }
        }

        DateTime? dateEff = null, dateExp = null;

        string strBeginDate = txtBeginDate.Text.Trim();
        string strEndDate = txtEndDate.Text.Trim();

        //终止日期非空时格式判断
        if (strBeginDate != "")
        {
            dateEff = valid.beDate(txtBeginDate, "A002P01014");
        }

        if (strEndDate != "")
            dateExp = valid.beDate(txtEndDate, "A002P01016");

        //起始日期不能大于终止日期
        if (dateEff != null && dateExp != null)
        {
            valid.check(dateEff.Value.CompareTo(dateExp.Value) <= 0, "A002P01017: 起始日期不能大于结束日期");
        }

        if (context.hasError())
            return false;
        else
            return true;
    }

    public DateTime? beDate(TextBox tb, String errCode)
    {
        tb.Text = tb.Text.Trim();

        if (!Validation.isDate(tb.Text))
        {
            context.AddError(errCode, tb);
            return null;
        }

        return DateTime.ParseExact(tb.Text, "yyyy-MM-dd", null); ;
    }

    private string getMoneyString(string str)
    {
        if (str == null || str == "")
            return "";
        return (Convert.ToDouble(str) / 100).ToString("0.00");
    }
}
