﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CC_Produce.aspx.cs" Inherits="ASP_ChargeCard_CC_Produce" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>充值卡生成</title>

    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="tb">
            充值卡->生成
        </div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
            <ContentTemplate>
    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>          


 <div class="con">
  <div class="base"></div>
  <div class="kuang5">
    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
      
      <tr>
        <td width="10%" align="right">私钥文件:</td>
        <td width="30%" colspan="3">
        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="inputlong"  Width="70%"/>
        <span class="red">*</span>
        </td>
      </tr>

      <tr>
        <td width="10%" align="right">起始卡号:</td>
        <td width="30%"><asp:TextBox runat="server" ID="txtFromCardNo" MaxLength="8" CssClass="inputmid"/><span class="red">*</span>
        </td>
        <td width="10%" align="right">数量:</td>
        <td width="30%"><asp:TextBox runat="server" ID="txtNum" MaxLength="5" CssClass="inputmid"/><span class="red">*</span>
        <td align="right">
            &nbsp;
        </td>
      </tr>
      <tr>
        <td width="10%" align="right">年份:</td>
        <td width="20%"><asp:TextBox runat="server" ID="txtYear" MaxLength="2" CssClass="inputmid"/><span class="red">*</span></td>
        <td width="10%" align="right">批次:</td>
        <td width="20%"><asp:TextBox runat="server" ID="txtBatchNo" MaxLength="2" CssClass="inputmid"/><span class="red">*</span></td>
        <td>&nbsp;</td>
      </tr>
        <tr>
        <td width="10%" align="right">面值:</td>
        <td width="30%"> <asp:DropDownList ID="drpCardVlaue" CssClass="input" runat="server"></asp:DropDownList></td>
        <td width="10%" align="right">厂家:</td>
        <td width="30%"> <asp:DropDownList ID="drpCorp" CssClass="input" runat="server"></asp:DropDownList></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
  </div>
  <div class="footall"></div>
  
<div class="btns">
     <table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="生成" OnClick="btnSubmit_Click"/></td>
  </tr>
</table>

</div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
