﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CC_StockOutNew.aspx.cs" Inherits="ASP_ChargeCard_CC_StockOutNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/JavaScript" src="../../js/mootools.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <script type="text/javascript" src="../../js/cardreaderhelper.js"></script>
    <title>充值卡出库</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="tb">
            充值卡 -> 出库</div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <script type="text/javascript">
            function submitConfirm() {
                var selDept = "";
                var obj = document.getElementById("selDept");
                if (obj.value != "") {
                    for (i = 0; i < obj.length; i++) {
                        if (obj.options[i].selected) {
                            selDept = obj.options[i].text + "";
                            break;
                        }
                    }
                } 
                if ($get('txtBeginNo').value != ""
                && $get('txtCount').value != ""
                && selDept != "") {
                    var hid = document.getElementById("hidValue");
                    var totalValue = parseFloat(hid.value) * parseFloat($get('txtCount').value);
                    MyExtConfirm('确认',
                 '充值卡面额为：' + hid.value + '<br>' +
		        '充值卡起始号码为： ' + $get('txtBeginNo').value + '<br>' +
		        '充值卡终止号码为： ' + $get('txtEndNo').value + '<br>' +
		        '充值卡张数为：' + $get('txtCount').value + '<br>' +
                '充值卡总面额为：' + totalValue + (hid.value).substr((hid.value).length-1, 1) + '<br>' +
		        '领用部门：' + selDept + '<br>' +
		        '是否确认？'
		        ,submitConfirmCallback);
                    return false;
                }
                return true;
            }

            function submitConfirmCallback(btn) {
                if (btn == 'yes') {
                    $get('btnConfirm').click();
                }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
             <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script> 

                <div class="con">
                    <div class="base">
                    </div>
                    <div class="kuang5">
                        <table class="text25" cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 10%; height: 25px" align="right">
                                        充值卡面额:
                                    </td>
                                    <td style="width: 20%; height: 25px" valign="middle">
                                     <asp:DropDownList runat=server ID="ddlMoney" CssClass=input 
                                     ></asp:DropDownList>&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 10%; height: 25px" align="right">
                                        起讫充值卡号:
                                    </td>
                                    <td style="width: 40%; height: 25px" valign="middle">
                                        <asp:TextBox ID="txtBeginNo_Sel" CssClass="inputmid" runat="server" MaxLength="14"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtEndNo_Sel" CssClass="inputmid" runat="server" MaxLength="14"></asp:TextBox>
                                    </td>
                                    <td style="width: 5%; height: 25px" align="right">
                                        &nbsp;</td>
                                    <td style="width: 15%; height: 25px">
                                        <asp:Button ID="btnQuery" runat="server" CssClass="button1"
                                            Text="查询" onclick="btnQuery_Click"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 300px">
                            <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="False" Width="98%"
                                CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" EmptyDataText="没有数据记录!" AllowPaging="True" PageSize="20">
                                <PagerSettings Mode="NumericFirstLast" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="pip">
                        充值卡出库</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                        <td>
                            <div class="right">
                                充值卡面额:
                            </div>
                        </td>
                        <td style="width: 20%; height: 25px" valign="middle">
                            <input type="hidden" id="hidValue" runat="server" />
                                     <asp:DropDownList runat=server ID="ddlValue" CssClass=input AutoPostBack="true"
                                      onselectedindexchanged="ddlMoney_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;
                         </td>
                                <td>
                                    <div align="right">
                                        起始卡号:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBeginNo" runat="server" CssClass="inputmid" MaxLength="14" AutoPostBack="true" OnTextChanged="txtBeginNo_TextChanged"></asp:TextBox>
                                    <input type="hidden" runat="server" id="hidEndNo" value=""/>
                                </td>
                                <td>
                                    <div align="right">
                                        充值卡数量:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCount" runat="server" CssClass="input" MaxLength="6" 
                                        ontextchanged="txtCount_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td>
                            <div align="right">
                                领用部门:</div>
                              </td>
                              <td>
                            <asp:DropDownList ID="selDept" runat="server" CssClass="input">
                            </asp:DropDownList>
                              </td>
                             <td>
                            <div align="right">
                             结束卡号:</div>
                            </td>
                        <td>
                            <asp:TextBox runat=server ReadOnly=true ID="txtEndNo" CssClass="labeltextmid"></asp:TextBox>
                            </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click"/>
                                <asp:Button ID="btnStockOut" CssClass="button1" runat="server" Text="充值卡出库" 
                                 OnClientClick="return submitConfirm()"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
