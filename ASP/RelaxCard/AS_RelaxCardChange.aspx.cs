﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using Common;
using PDO.AdditionalService;
using TM;

// 休闲年卡补换卡
//modify by jiangbb 2015-04-20 开通方式


public partial class ASP_AddtionalService_AS_RelaxCardChange : Master.FrontMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        hidStaffNo.Value = Session["STAFF"].ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "js",
              "loadWebcam();", true);

        if (Page.IsPostBack) return;
        Session["PicData"] = null;
        Session["Cardno"] = null;
        Session["newCardno"] = null;
        Session["PicDataOther"] = null;

        // 设置可读属性
        if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

        setReadOnly(txtCardBalance, txtStartDate);

        //初始化套餐类型
        ASHelper.initPackageTypeList(context, selPackageType);

        // 初始化费用列表
        decimal total = initFeeList(gvResult, "33");
        hidAccRecv.Value = total.ToString("n");

        // 初始化旧卡信息查询结果gridview
        UserCardHelper.resetData(gvOldCardInfo, null);

    }

    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnPrintPZ.Enabled = false;
        preview_size_fake.Src = "";

        if(!IsState())
        {
            context.AddError("卡未售出", txtCardNo);
        }

        #region add by shil 20130909,如果是旅游年卡，则不允许在该页面办理业务
        //验证卡片是否旅游年卡(51)，如果是旅游年卡，则不允许在该页面办理业务

        //bool cardTypeOk = CommonHelper.allowCardtype(context, txtCardNo.Text, "5101", "5103");
        //if (cardTypeOk == false)
        //{
        //    return;
        //}
        #endregion

        // 检查帐户信息


        checkAccountInfo(txtCardNo.Text);
        Session["newCardno"] = txtCardNo.Text.Trim();
        // 检查惠民休闲年卡特征

        checkRelaxFeature();

        checkGardenIsInvalid();//判断当前卡是否是是园林账户表中无效的卡 add by youyue 20141229

        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        //add by jiangbb 2012-10-09  加入判断 旧卡或新卡为市民卡不会修改客户资料 
        //if (!context.hasError() && txtCardNo.Text.Substring(4, 2).ToString() == "18")
        //{
        //    context.AddMessage("提示：新卡卡号为市民卡，客户资料不会被修改");
        //}

        hidReadCardOK.Value = !context.hasError() && txtCardNo.Text.Length > 0 ? "ok" : "fail";

        btnSubmit.Enabled = hidReadCardOK.Value == "ok"
            && gvOldCardInfo.SelectedIndex >= 0;

    }

    // 对话框确认处理

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "writeSuccess")  // 写卡成功
        {
            clearCustInfo(txtCardNo, txtCustName, txtCustBirth, selPaperType, txtPaperNo,
               selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);

            AddMessage("D005060002: 惠民休闲年卡前台写卡成功，补换卡成功");
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("A00506C001: 惠民休闲年卡前台写卡失败，补换卡失败");
        }

        hidWarning.Value = "";  // 清除警告信息
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        // 输入项判断处理（证件号码、旧卡号码、客户姓名）
        // ASHelper.changeCardQueryValidate(context, txtId, txtOldCardNo, txtName);
        // if (context.hasError()) return;

        // 从休闲卡资料表中查询
        string strValue = txtCondition.Text;
        if (selQueryType.SelectedValue == "02" || selQueryType.SelectedValue == "03" || selQueryType.SelectedValue == "04")   //证件号码
        {
            StringBuilder strBuilder = new StringBuilder();
            //AESHelp.AESEncrypt(strValue, ref strBuilder);
            strValue = strBuilder.ToString();
        }
        DataTable data = ASHelper.callQuery(context, "XXQueryOldCards", selQueryType.SelectedValue,
            strValue);


        if (data == null || data.Rows.Count == 0)
        {
            UserCardHelper.resetData(gvOldCardInfo, data);
            AddMessage("N005030001: 查询结果为空");
        }
        else
        {
            //add by jiangbb 解密
            //CommonHelper.AESDeEncrypt(data, new List<string>(new string[] { "CUSTNAME", "CUSTPHONE", "CUSTADDR", "PAPERNO" }));

            UserCardHelper.resetData(gvOldCardInfo, data);

            gvOldCardInfo.SelectedIndex = 0;
            gvOldCardInfo_SelectedIndexChanged(sender, e);
        }
    }

    // 检查休闲卡特征值


    void checkRelaxFeature()
    {
        // 从休闲卡资料表中检查USETAG值


        DataTable data = ASHelper.callQuery(context, "XXParkCardUseTag", txtCardNo.Text);
        if (data.Rows.Count > 0)
        {
            context.AddError("A005060001: 当前卡已经是旅游年卡");
        }
    }

    // 旧卡信息查询结果gridview行创建事件


    protected void gvOldCardInfo_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件


            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvOldCardInfo','Select$" + e.Row.RowIndex + "')");
        }
    }

    //没有客户信息查看权则证件号码加*显示
    protected void gvOldCardInfo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!CommonHelper.HasOperPower(context))
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[5].Text = CommonHelper.GetPaperNo(e.Row.Cells[5].Text);
            }
        }
    }

    // 旧卡信息查询结果行选择事件
    public void gvOldCardInfo_SelectedIndexChanged(object sender, EventArgs e)
    {
        hidServiceFor.Value = "0";

        // 得到选择行
        GridViewRow selectRow = gvOldCardInfo.SelectedRow;

        // 根据选择行卡号读取用户信息
        readCustInfo(selectRow.Cells[0].Text, txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);
        Session["Cardno"] = selectRow.Cells[0].Text;
        readCardPic(selectRow.Cells[0].Text);

        hidForPaperNo.Value = txtPaperNo.Text.Trim();
        hidForPhone.Value = txtCustPhone.Text.Trim();
        hidForAddr.Value = txtCustAddr.Text.Trim();

        //add by jiangbb 2012-10-15 客户信息隐藏显示 201015：客户信息查看权
        //if (!CommonHelper.HasOperPower(context))
        //{
        //    txtPaperNo.Text = CommonHelper.GetPaperNo(txtPaperNo.Text);
        //    txtCustPhone.Text = CommonHelper.GetCustPhone(txtCustPhone.Text);
        //    txtCustAddr.Text = CommonHelper.GetCustAddress(txtCustAddr.Text);
        //}

        DataTable data = ASHelper.callQuery(context, "QueryXXPackage", selectRow.Cells[0].Text);

        if (data != null && data.Rows.Count > 0)
        {
            selPackageType.SelectedValue = data.Rows[0][0].ToString();
        }

        hidEndDate.Value = selectRow.Cells[1].Text;
        hidUsabelTimes.Value = selectRow.Cells[3].Text;

        //读取账户类型
        //ASHelper.readAccountType(context, selectRow.Cells[0].Text, labAccountType);

        if (!context.hasError() && gvOldCardInfo.SelectedRow.Cells[0].Text.Substring(4, 2).ToString() == "18")
        {
            context.AddMessage("提示：旧卡卡号为市民卡，客户资料不会被修改");
        }
        btnSubmit.Enabled = !context.hasError() && hidReadCardOK.Value == "ok";

        if (selectRow.Cells[1].Text.ToString().CompareTo(DateTime.Now.ToString("yyyyMMdd")) >= 0)
        {
            hidServiceFor.Value = "1";
        }
        //新卡卡面没有照片，并且旧卡库内也没有照片，新卡必须采集照片
        if(!IsHavePhoto()&&!DBHavePhoto(selectRow.Cells[0].Text.Trim()))
        {
            context.AddMessage("新卡卡面和旧卡库内都没有照片，请做照片采集");
        }
    } 

    // 提交前输入项校验
    private void submitValidate()
    {
        if (selPackageType.SelectedValue == "")
        {
            context.AddError("套餐类型不能为空", selPackageType);
        }
        if (!IsHavePhoto() && !DBHavePhoto(Session["Cardno"].ToString()))
        {
            if(Session["PicData"] == null)
            {
                context.AddError("请先采集照片");
            }
        }
        if (IsHaveInfo(txtCardNo.Text.Trim()))      //新卡是否有客户资料
        {
            if (!CustIsSame(txtCardNo.Text.Trim(), Session["Cardno"].ToString()))
            {
                context.AddError("新旧卡证件类型或号码不同！");
            }
        }
    }

    private void custInfoForValidate(TextBox txtCustName, TextBox txtCustBirth,
            DropDownList selPaperType, TextBox txtPaperNo,
            DropDownList selCustSex, TextBox txtCustPhone,
            TextBox txtCustPost, TextBox txtCustAddr, TextBox txtCustEmail, TextBox txtRemark)
    {
        Validation valid = new Validation(context);
        txtCustName.Text = txtCustName.Text.Trim();
        valid.check(Validation.strLen(txtCustName.Text) <= 50, "A005010001, 客户姓名长度不能超过50", txtCustName);
        valid.check(!Validation.isEmpty(txtCustName), "客户姓名不能为空", txtCustName);

        bool b = Validation.isEmpty(txtCustBirth);
        if (!b)
        {
            b = valid.fixedLength(txtCustBirth, 8, "A005010002: 出生日期必须为8位");
            if (b)
            {
                valid.beDate(txtCustBirth, "A005010003: 出生日期格式必须是yyyyMMdd");
            }
        }

        b = Validation.isEmpty(txtCustPost);
        if (!b)
        {
            b = valid.fixedLength(txtCustPost, 6, "A005010004: 邮政编码必须为6位");
            if (b)
            {
                valid.beNumber(txtCustPost, "A005010005: 邮政编码必须是数字");
            }
        }

        if (selPaperType.SelectedValue == "")
        {
            context.AddError("证件类型不能为空", selPaperType);
        }

        b = Validation.isEmpty(txtPaperNo);
        valid.check(!b, "证件号码不能为空", txtPaperNo);

        if (!b)
        {
            b = valid.check(Validation.strLen(txtPaperNo.Text) <= 20, "A005010006: 证件号码位数必须小于等于20", txtPaperNo);
            //判断是否有客户信息查看权限

            if (CommonHelper.HasOperPower(context) || CommonHelper.GetPaperNo(hidForPaperNo.Value) != txtPaperNo.Text.Trim())
            {
                if (b)
                {
                    valid.beAlpha(txtPaperNo, "A005010007: 证件号码必须是英文或者数字");
                }
            }
        }

        b = Validation.isEmpty(txtCustPhone);
        if (!b)
        {
            b = valid.check(Validation.strLen(txtCustPhone.Text) <= 20, "A005010008: 联系电话位数必须小于等于20", txtCustPhone);
            //if (b)
            //{
            //    valid.beNumber(txtCustPhone, "A005010009: 联系电话必须是数字");
            //}
        }
        b = Validation.isEmpty(txtCustAddr);
        if (!b)
        {
            valid.check(Validation.strLen(txtCustAddr.Text) <= 50, "A005010010: 联系地址位数必须小于等于50", txtCustAddr);
        }

        valid.isEMail(txtCustEmail);

        b = Validation.isEmpty(txtRemark);
        if (!b)
        {
            valid.check(Validation.strLen(txtRemark.Text) <= 100, "A005010011: 备注位数必须小于等于100", txtRemark);
        }
    }

    // 休闲年卡补换卡提交处理


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 提交前输入校验
        submitValidate();


        if (context.hasError()) return;

        int usableTimes = int.Parse(hidUsabelTimes.Value);

        //hidParkInfo.Value = hidEndDate.Value + selPackageType.SelectedValue + usableTimes.ToString("X2");

        //if (hidParkInfo.Value.Trim().Length != 12)
        //{
        //    context.AddError("写卡数据有误");
        //    return;
        //}

        //获取身份证Code
        string paperTypeCode = string.Empty;
        DataTable data = ASHelper.callQuery(context, "ReadPaperCode", selPaperType.Text);
        if (data.Rows.Count > 0)
        {
            paperTypeCode = (string)data.Rows[0].ItemArray[0];
        }

        //add by jiangbb 2012-10-17 判断页面上的证件号码、联系电话、联系地址是否修改 并取值

        string custPaperNo = CommonHelper.GetPaperNo(hidForPaperNo.Value) == txtPaperNo.Text.Trim() ? hidForPaperNo.Value : txtPaperNo.Text.Trim();
        string custPhone = CommonHelper.GetCustPhone(hidForPhone.Value) == txtCustPhone.Text.Trim() ? hidForPhone.Value : txtCustPhone.Text.Trim();
        string custAddr = CommonHelper.GetCustAddress(hidForAddr.Value) == txtCustAddr.Text.Trim() ? hidForAddr.Value : txtCustAddr.Text.Trim();

        //StringBuilder strBuilder = new StringBuilder();
        // 调用休闲年卡补换卡存储过程

        hidNewCardNo.Value = txtCardNo.Text;
        SP_AS_RelaxCardChangePDO pdo = new SP_AS_RelaxCardChangePDO();
        pdo.ID = DealString.GetRecordID(hidTradeNo.Value, hidAsn.Value);
        pdo.oldCardNo = gvOldCardInfo.SelectedRow.Cells[0].Text;
        pdo.newCardNo = txtCardNo.Text;
        pdo.asn = hidAsn.Value.Substring(4, 16);
        pdo.operCardNo = context.s_CardID; // 操作员卡
        pdo.terminalNo = "112233445566";   // 目前固定写成112233445566
        pdo.packageTypeCode = selPackageType.SelectedValue;

        //获取卡有效期限

        context.DBOpen("Select");
        string sql = @" SELECT ENDDATE  FROM TF_F_CARDXXPARKACC_SZ WHERE CARDNO = '" + pdo.oldCardNo + "'";
        DataTable table = context.ExecuteReader(sql);
        string cardUseEndTime = Convert.ToString(table.Rows[0][0]);

        // 12位,年月日8位+标志位2位+次数2位


        // 园林年卡的标志位为'01',休闲年卡的标志位为套餐类型编号.次数都是16进制.
        pdo.endDateNum = hidEndDate.Value + selPackageType.SelectedValue + usableTimes.ToString("X2");


        //加密 ADD BY JIANGBB 2012-04-19
        pdo.custName = txtCustName.Text;
       //AESHelp.AESEncrypt(pdo.custName, ref strBuilder);
        //pdo.custName = strBuilder.ToString();

        //strBuilder = new StringBuilder();
        //AESHelp.AESEncrypt(custPhone, ref strBuilder);
        pdo.custPhone = custPhone;

        //strBuilder = new StringBuilder();
        //AESHelp.AESEncrypt(custAddr, ref strBuilder);
        pdo.custAddr = custAddr;

        pdo.custBirth = txtCustBirth.Text;
        pdo.paperType = paperTypeCode;

        //strBuilder = new StringBuilder();
        //AESHelp.AESEncrypt(custPaperNo, ref strBuilder);
        pdo.paperNo = custPaperNo;
        pdo.custSex = selCustSex.Text == "男" ? "0" : "1";
        pdo.custPost = txtCustPost.Text;
        pdo.custEmail = txtEmail.Text;
        pdo.remark = txtRemark.Text;
        pdo.passPaperNo = custPaperNo;          //明文证件号码
        pdo.passCustName = txtCustName.Text;    //明文姓名
        pdo.serviceFor = hidServiceFor.Value;   //是否同步休闲标识位 0不同步 1同步

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);
        txtCardNo.Text = "";
        UserCardHelper.resetData(gvOldCardInfo, null);
        gvOldCardInfo.SelectedIndex = -1;
        hidReadCardOK.Value = "fail";
        btnSubmit.Enabled = false;

        // 调用成功，显示成功信息


        if (ok)
        {
            context.AddMessage("补换卡成功！");
            //保存照片
            try
            {
                if (!IsHavePhoto() && !DBHavePhoto(Session["Cardno"].ToString()))
                {
                    SavePic();
                    readCardPic(Session["newCardno"].ToString());
                    context.AddMessage("照片上传成功");
                    hidPicType.Value = null;
                    Session["PicData"] = null;
                }
            }
            catch(Exception ex)
            {
                context.AddMessage("S00501B014：保存照片失败"+ex.ToString());//系统是否存在照片不做为园林开通/续费的必要条件
            }

            //add by jiangbb 2015-04-24 只有脱机方式才写卡，联机方式不写卡
            //if (labAccountType.Text == "线下开通")
            //{
            //    ScriptManager.RegisterStartupScript(
            //        this, this.GetType(), "writeCardScript",
            //        "startXXPark();", true);
            //}
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCustName.Text.Trim(), context.s_UserID,
                    "旅游年卡补换卡", pdo.ID,
                    hidAccRecv.Value, "0.00", pdo.newCardNo, cardUseEndTime);
            //ASHelper.preparePingZheng(ptnPingZheng, pdo.oldCardNo, txtCustName.Text, "休闲年卡补换卡", "0.00"
            //    , "0.00", pdo.newCardNo, txtPaperNo.Text, "0.00", "0.00", hidAccRecv.Value, context.s_UserID,
            //    context.s_DepartName,
            //    selPaperType.SelectedValue == "" ? "" : selPaperType.SelectedItem.Text, "0.00", hidAccRecv.Value);
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }
        }
        readCardPic(hidNewCardNo.Value);

        hfPic.Value = "";
        Session["PicData"] = null;
        preview_size_fake.Src = "";
        hidNewCardNo.Value = "";
        hidWarning.Value = "";  // 清除警告信息
    }

    //显示照片
    protected void BtnShowPic_Click(object sender, EventArgs e)
    {
        preview_size_fake.Src = "";

        string str = hfPic.Value;

        if (string.IsNullOrEmpty(str))
            return;

        int len = str.Length / 2;
        byte[] pic = new byte[len];
        HexStrToBytes(str, ref pic, len);

        preview_size_fake.Src = "../../picture.aspx?id=" + DateTime.Now.ToString();
    }

    int HexStrToBytes(string strSou, ref byte[] BytDes, int bytCount)
    {
        int i;
        int len;
        int HighByte, LowByte;

        len = strSou.Length;
        if (bytCount * 2 < len) len = bytCount * 2;

        if ((len - len / 2 * 2) == 0)
        {
            for (i = 0; i < len; i += 2)
            {
                HighByte = (byte)strSou[i];
                LowByte = (byte)strSou[i + 1];

                if (HighByte > 0x39) HighByte -= 0x37;
                else HighByte -= 0x30;

                if (LowByte > 0x39) LowByte -= 0x37;
                else LowByte -= 0x30;

                BytDes[i / 2] = (byte)((HighByte << 4) | LowByte);
            }
            for (; i < bytCount * 2; i += 2)
            {
                BytDes[i / 2] = 0;
            }
            return (len / 2);
        }
        else
            return 0;
    }

    private void readCardPic(string cardNo)
    {
        byte[] imageData = ReadImage(cardNo);

        if (imageData != null)
        {
            Session["PicData"] = imageData;
            preview_size_fake.Src = "../../picture.aspx?id=" + DateTime.Now.ToString();

        }
        //else
        //{
        //    context.SPOpen();
        //    context.AddField("p_CARDNO").Value = cardNo;
        //    context.AddField("p_LENGTH", "String", "output", "16", null);
        //    bool ok = context.ExecuteSP("SP_AS_SMK_PICTURELENGTH");
        //    {
        //        string length = context.GetFieldValue("p_LENGTH").ToString().Trim();

        //    }
        //}
    }

    private byte[] ReadImage(string cardNo)
    {
        string selectSql = "Select PICTURE From TF_F_CARDPARKPHOTO_SZ Where CARDNO=:CARDNO";
        context.DBOpen("Select");
        context.AddField(":CARDNO").Value = cardNo;
        DataTable dt = context.ExecuteReader(selectSql);
        if (dt != null && dt.Rows.Count > 0 && dt.DefaultView[0]["PICTURE"].ToString() != "")
        {
            return (byte[])dt.Rows[0].ItemArray[0];
        }

        return null;
    }

    /// <summary>
    /// 照片导入
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.PostedFile.FileName != "")       //导入方式
        {
            System.IO.Stream fileStream = Stream.Null;
            int flag = validateForPicture(FileUpload1, out fileStream);
            if (flag == -1) return;
            string path = HttpContext.Current.Server.MapPath("../../tmp/pic_thu_" + Session["STAFF"].ToString() + ".jpg");
            bool bl = GetPicThumbnail(fileStream, path, flag);
            Session["PicData"] = GetPicture(path);
        }

        if (Session["PicData"] == null)
        {
            context.AddError("没有照片信息！");
            return;
        }

        //ValidatePicFormat();
        preview_size_fake.Src = "../../picture.aspx?id=" + DateTime.Now.ToString();
        context.AddMessage("照片已导入，点击提交按钮后生效！");

        return;
    }

    /// <summary>
    /// 校验上传图片的大小以及各式
    /// </summary>
    /// <param name="file">FileUpload控件</param>
    /// <returns></returns>
    private int validateForPicture(FileUpload file, out Stream fileStream)
    {

        string[] strPics = { ".jpg", ".bmp", ".jpeg", ".png" };
        fileStream = Stream.Null;
        int index = Array.IndexOf(strPics, Path.GetExtension(FileUpload1.FileName).ToLower());
        if (index == -1)
        {
            context.AddError("A094780002:上传文件格式必须为jpg|bmp|jpeg|png");
            return -1;
        }
        fileStream = file.PostedFile.InputStream;
        int len = file.FileBytes.Length;
        if (len < 1024 * 10)
        {
            context.AddError("A094780014：上传文件不能小于10KB");
        }
        else if (len > 1024 * 1024 * 5)
        {
            context.AddError("A094780014：上传文件不能大于5M");
        }

        //文件大小返回不同压缩等级
        if (len >= 1024 * 500 && len < 1024 * 1024)
        {
            return 30;
        }
        else if (len >= 1024 * 1024 && len < 1024 * 1024 * 2)
        {
            return 10;
        }
        else if (len >= 1024 * 1024 * 2 && len < 1024 * 1024 * 5)
        {
            return 5;
        }
        else if (len >= 1024 * 10 && len < 1024 * 500)
        {
            return 50;
        }
        else
        {
            return -1;
        }
    }

    /// <summary>
    /// 获取图片二进制流文件 add by youyue 20140414
    /// </summary>
    /// <param name="FileUpload1">upload控件</param>
    /// <returns>二进制流</returns>
    private byte[] GetPicture(string path)
    {
        FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);

        byte[] buffer = new byte[file.Length];
        file.Read(buffer, 0, (int)file.Length);

        file.Dispose();
        FileInfo fi = new FileInfo(path);
        fi.Delete();
        return buffer;

    }

    /// <summary>
    /// 图片压缩
    /// </summary>
    /// <param name="sFile">图片原路径</param>
    /// <param name="outPath">图片输出路径</param>
    /// <param name="flag">压缩比例1-100</param>
    /// <returns></returns>
    private bool GetPicThumbnail(Stream fileStream, string outPath, int flag)
    {
        //FileStream fs = new FileStream(FileUpload1.PostedFile.InputStream, FileMode.Open, FileAccess.Read);
        System.Drawing.Image iSource = System.Drawing.Image.FromStream(fileStream);
        //System.Drawing.Image iSource = System.Drawing.Image.FromFile(sFile);
        ImageFormat tFormat = iSource.RawFormat;

        //以下代码为保存图片时，设置压缩质量  
        EncoderParameters ep = new EncoderParameters();
        long[] qy = new long[1];
        qy[0] = flag;//设置压缩的比例1-100  
        EncoderParameter eParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qy);
        ep.Param[0] = eParam;
        try
        {
            ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo jpgICIinfo = null;
            for (int x = 0; x < arrayICI.Length; x++)
            {
                if (arrayICI[x].FormatDescription.Equals("JPEG"))
                {
                    jpgICIinfo = arrayICI[x];
                    break;
                }
            }

            if (jpgICIinfo != null)
            {
                int width = (int)Math.Round((decimal)320 * iSource.Width / iSource.Height, 0);
                int height = 320;   //定高 宽度按照原始图片比例缩小
                Bitmap map = new Bitmap(width, height);
                Graphics gra = Graphics.FromImage(map);

                gra.DrawImage(iSource, new Rectangle(0, 0, width, height), new Rectangle(0, 0, iSource.Width, iSource.Height), GraphicsUnit.Pixel);
                iSource.Dispose();
                //gra.Clear(Color.Transparent);
                gra.Dispose();
                map.Save(outPath, jpgICIinfo, ep);
                map.Dispose();
            }
            else
            {
                iSource.Save(outPath, tFormat);
            }
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            iSource.Dispose();
            iSource.Dispose();
        }
    }

    /// <summary>
    /// 校验图片格式，并且转换成JPG
    /// </summary>
    private void ValidatePicFormat()
    {
        MemoryStream ms = new MemoryStream((byte[])Session["PicData"]);

        System.Drawing.Image img = System.Drawing.Image.FromStream(ms);

        //判断图片是否是JPG格式
        if (img.RawFormat.Guid != ImageFormat.Jpeg.Guid)
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                Bitmap bit = new Bitmap(img);
                bit.Save(mstream, ImageFormat.Jpeg);

                Session["PicData"] = mstream.ToArray();
                mstream.Dispose();
            }
        }
        img.Dispose();
        ms.Dispose();
    }

    //查询新卡是否是园林账户表中无效状态的卡(即通过功能补换页面将旧卡置为无效的卡)
    private void checkGardenIsInvalid()
    {
        // 从园林卡资料表中检查USETAG值是否是0
        DataTable data = ASHelper.callQuery(context, "RelaxCardUseIsInvalid", txtCardNo.Text);
        if (data.Rows.Count > 0)
        {
            context.AddError("A005020002: 当前卡已经是旅游账户表中无效的卡，不可再次开通");
        }
    }

    /// <summary>
    /// 判断卡面上是否已经有照片
    /// </summary>
    /// <returns></returns>
    private bool IsHavePhoto()
    {
        bool isCan = false;
        string CardType = DealString.GetResourceValue(context.ResourcePath, "CardLossRepair", "UnablePhoto");
        string[] TypeCode = CardType.Split(',');
        if (Session["newCardno"] == null)
        {
            return true;
        }
        string OldCardType = Session["newCardno"].ToString().Substring(4, 4);
        for (int i = 0; i < TypeCode.Length; i++)
        {
            if (TypeCode[i] == OldCardType)
            {
                isCan = true;
                break;
            }
        }
        return isCan;
    }

    /// <summary>
    /// 库内是否已经有照片
    /// </summary>
    /// <returns></returns>
    private bool DBHavePhoto(string cardno)
    {
        bool has = true;
        string strSql = string.Format("select * from TF_F_CARDPARKPHOTO_SZ where cardno='{0}'", cardno);
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        if (null == dt || dt.Rows.Count == 0)
            has = false;

        return has;
    }

    private void SavePic()
    {
        //string str = hfPic.Value;

        if (Session["PicData"] == null)
            return;

        ValidatePicFormat();

        context.DBOpen("Select");
        context.AddField(":p_cardNo").Value = Session["newCardno"].ToString();
        DataTable dt = context.ExecuteReader(@"
                                        SELECT * 
                                        From TF_F_CARDPARKPHOTO_SZ
                                        WHERE CARDNO = :p_cardNo
                            ");


        string sql = "";
        if (dt.Rows.Count == 0)
        {
            context.DBOpen("Insert");
            sql = "INSERT INTO TF_F_CARDPARKPHOTO_SZ (CARDNO  , PICTURE ,OPERATETIME,OPERATEDEPARTID,OPERATESTAFFNO) VALUES(:p_cardNo, :BLOB,:OPERATETIME,:OPERATEDEPARTID,:OPERATESTAFFNO)";
        }
        else
        {
            InsertCardHistory();

            context.DBOpen("Update");
            sql = "Update TF_F_CARDPARKPHOTO_SZ Set PICTURE = :BLOB,OPERATETIME = :OPERATETIME,OPERATEDEPARTID = :OPERATEDEPARTID,OPERATESTAFFNO = :OPERATESTAFFNO Where CARDNO = :p_cardNo";

        }

        context.AddField(":p_cardNo", "String").Value = Session["newCardno"].ToString();
        context.AddField(":BLOB", "Blob").Value = Session["PicData"];
        context.AddField(":OPERATETIME", "DateTime").Value = DateTime.Now;
        context.AddField(":OPERATEDEPARTID", "String").Value = context.s_DepartID;
        context.AddField(":OPERATESTAFFNO", "String").Value = context.s_UserID;

        context.ExecuteNonQuery(sql);
        context.DBCommit();
    }

    private void InsertCardHistory()
    {
        context.DBOpen("Insert");
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.Append("Insert Into TB_F_CARDPARKPHOTO_SZ(CARDNO,PICTURE,OPERATETIME,OPERATEDEPARTID,OPERATESTAFFNO) ");
        strBuilder.Append("Select CARDNO,PICTURE,OPERATETIME,OPERATEDEPARTID,OPERATESTAFFNO From TF_F_CARDPARKPHOTO_SZ WHERE CARDNO =:p_cardNo");

        context.AddField(":p_cardNo", "String").Value = Session["newCardno"].ToString();
        context.ExecuteNonQuery(strBuilder.ToString());
        context.DBCommit();
    }

    /// <summary>
    /// 是否售出状态
    /// </summary>
    /// <returns>是：true</returns>
    private bool IsState()
    {
        string str = string.Format("select * from tf_f_cardrec where cardno='{0}' and cardstate in('10','11')", txtCardNo.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(str);
        if (null == dt || dt.Rows.Count <= 0)
        {
            return false;
        }
        return true;
    }

    private bool IsHaveInfo(string cardno)
    {
        string str = string.Format("select custname from tf_f_customerrec where cardno='{0}' ", cardno);
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(str);
        if (null == dt || string.IsNullOrEmpty(dt.Rows[0]["custname"].ToString().Trim()))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// 新旧卡证件号码是否相同
    /// </summary>
    /// <param name="oldCardNo"></param>
    /// <param name="newCardNo"></param>
    /// <returns></returns>
    private bool CustIsSame(string oldCardNo,string newCardNo)
    {
        bool same = true;

        string strSql = string.Format("select * from tf_f_customerrec t1,tf_f_customerrec t2 where t1.cardno='{0}' "
            + "and t2.cardno='{1}' and t1.papertypecode=t2.papertypecode and t1.paperno=t2.paperno",oldCardNo,newCardNo);
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        if(null==dt|| dt.Rows.Count==0)
        {
            same = false;
        }

        return same;
    }
}
