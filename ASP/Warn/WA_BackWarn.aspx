﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WA_BackWarn.aspx.cs" Inherits="ASP_Warn_WA_BackWarn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <title>监控任务</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
     <script type="text/javascript" src="../../js/float.js"></script>
   <link href="../../css/style.css" rel="stylesheet" type="text/css" />


</head>

<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
       <%-- <div class="tb">
        <table  cellpadding="0" cellspacing="0" style=" width:100%;">
        <tr>
        <td> 账务监控->后台监控</td>
        <td align="right" valign=middle ><font color="red"><strong><a href="WA_BackWarnDetail.aspx?level=2" target="content"><img src="../../Images/icon-error.gif" alt="严重" style=" border:0px;" width="14px" height="14px" />(<asp:Label ID="lblerror" runat="server"></asp:Label>)</a></strong></font>|<font color="yellow"><a href="WA_BackWarnDetail.aspx?level=1" target="content"><img src="../../Images/icon-warning.gif" alt="警告" style=" border:0px;" width="14px" height="14px" />(<asp:Label ID="lblWarn" runat="server"></asp:Label>)</a></font>&nbsp;</td>
        </tr>
        </table>
        </div>--%>
        
        <div class="main_top"><!--top start-->
        <div class="main_top_box">
        <div class="main_top_tu"><img src="../../Images/main_top_title.png" width="181" height="41"/></div>
        <div class="main_top_rightbox">
        	<div class="main_top_false"></div>
            <div class="main_top_txt"><a href="WA_BackWarnDetail.aspx?level=2" target="content">(<asp:Label ID="lblerror" runat="server"></asp:Label>)</a></div>
            <div class="main_top_warning"></div>
            <div class="main_top_txt"><a href="WA_BackWarnDetail.aspx?level=1" target="content">(<asp:Label ID="lblWarn" runat="server"></asp:Label>)</a></div>
        </div>
        </div>
    </div><!--top over-->
    <div class="main_box">
    	<div class="main_box_left"></div>
        <div class="main_box_right"></div>
        <div class="c"></div>
    </div>
    
    <div class="main_top_linebox">
    	<div class="main_top_line"><img src="../../Images/main_top_line.png"/></div> 
    </div>
    	 
        <div id="div2" runat="server"></div>


    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
    
    <div class="main">
    <div class="main_box1">
    	<div class="main_box_left"></div>
        <div class="main_box_right"></div>
        <div class="c"></div>
    </div>
    <div class="main_box2">
    
        <div id="div1" runat="server">
        </div>
        
    </div>
	<div class="main_box1">
    	<div class="main_box_left"></div>
        <div class="main_box_right"></div>
        <div class="c"></div>
    </div>
    <div class="main_box2">
    <div class="main_body_bottom">
    	<div class="main_body_bottom_left"></div>
        <div class="main_body_bottom_right"></div>
        <div class="c"></div>
    </div></div>   
    </div><!--main over-->
         </div>
          <div class="c"></div>
        <asp:Timer ID="Timer1" runat="server" Interval="100000" OnTick="InitData"></asp:Timer>


</ContentTemplate>
</asp:UpdatePanel>
 <asp:UpdateProgress ID="UpdateProgress1"  runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <input type="button" value="STOP" onclick="if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) { Sys.WebForms.PageRequestManager.getInstance().abortPostBack(); }" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    <div class="body_bottom"></div>   
    </form>

    <script>
        function flashBack() {
            try {
                beforediv1.innerHTML = ASP_Warn_WA_BackWarn.InitData_Ajax("").value;
                var table1 = ASP_Warn_WA_BackWarn.InitData_Ajax1().value;
                var rows1 = rable1.Rows;
                var errorflag = false;
                var warnflag = false;
                for (var i = 0; i < rows1.length; i++) {
                    if (rows[i].fwarnlevel == "1") {
                        errorflag = true;
                        document.getElementById('lblerror').value = rows[i].num;
                    }
                    else if (rows[i].fwarnlevel == "2") {
                        warnflag = true;
                        document.getElementById('lblWarn').value = rows[i].num;
                    }

                }
                if (!errorflag) {
                    document.getElementById('lblerror').value = "0";
                }
                if (!warnflag) {
                    document.getElementById('lblWarn').value = "0";
                }
            }
            catch (ex) {
                alert(ex.description);
            }

        }
       // var beforediv1 = document.getElementById('<%=div1.ClientID %>');
       // setTimeout(flashBack, 2000);
    </script>
</body>
</html>
