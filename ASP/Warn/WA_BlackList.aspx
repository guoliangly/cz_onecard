﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WA_BlackList.aspx.cs" Inherits="ASP_Warn_WA_BlackList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>黑名单</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
   <link href="../../css/card.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="../../js/myext.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
       
       
       <ContentTemplate>
            
        <div class="tb" > 账务监控->黑名单 </div>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

        <div class="con">
          <div class="base">查询条件</div>
          <div class="kuang5">
         <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
          <tr>
            <td >卡号:</td>
            <td><asp:TextBox runat="server"  CssClass="inputmid"  ID="txtCardNo1" MaxLength="16"/></td>
            
            <td ><div align="right">卡类型:</div></td>
            <td>
            <asp:DropDownList runat="server" ID="qryCardType">
                <asp:ListItem Text="" Value=""  Selected="True"></asp:ListItem>
                <asp:ListItem Text="0:老公交卡" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:新CPU卡" Value="1"></asp:ListItem>
            </asp:DropDownList>
            
            <td><div align="right">黑名单状态:</div></td>
            <td  >
            <asp:DropDownList runat="server" ID="qryBlackState">
                <asp:ListItem Text="" Value=""  Selected="True"></asp:ListItem>
                <asp:ListItem Text="0:黑名单" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:已锁定卡" Value="1"></asp:ListItem>
                <asp:ListItem Text="2:正常卡" Value="2" ></asp:ListItem>
            </asp:DropDownList>
            
            
            <td ><div align="right">黑名单类型:</div></td>
            <td  >
            <asp:DropDownList runat="server" ID="qryBlackType">
                <asp:ListItem Text="" Value=""  Selected="True"></asp:ListItem>
                <asp:ListItem Text="0:挂失黑名单" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:消费异常生成的黑名单" Value="1"></asp:ListItem>
            </asp:DropDownList>
            </td>
            
          </tr>
        </table>
        
        </div>
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="12text">
              <tr>
                <td width="30%"><div class="jieguo">查询结果</div></td>
                <td width="*">&nbsp;</td>
                <td  align="right">
                  <asp:Button ID="btnQuery" Width="50px" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                  <asp:Button ID="btnDown" Width="50px" CssClass="button1" runat="server" Text="导出" OnClick="btnDown_Click"  Visible="false"/>
                  <asp:Button ID="btnDel" runat="server" Text="删除选中" CssClass="button1" OnClick="btnDel_Click" />
                  <asp:HiddenField runat="server" ID="hidCardNoForDelAndMod" />
                </td>
                <td width="10">&nbsp;</td>
              </tr>
            </table>
        
      
  <div class="kuang5">
  <div class="gdtb" style="height:260px">

         <asp:GridView ID="gvResult" runat="server"
        Width = "97%"
        CssClass="tab1"
        AutoGenerateSelectButton="true"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        AllowPaging="True"
        PageSize="10"
        PagerSettings-Mode="NumericFirstLast"
        PagerStyle-HorizontalAlign="left"
        PagerStyle-VerticalAlign="Top"
        AutoGenerateColumns="false"
        OnPageIndexChanging="gvResult_Page"
        OnSelectedIndexChanged="gvResult_SelectedIndexChanged"
        EmptyDataText="没有数据记录!">
         
         <Columns>
               <asp:BoundField  HeaderText="卡号" DataField="卡号" />
               <asp:BoundField  HeaderText="生成时间" DataField="生成时间"/>
               <asp:BoundField  HeaderText="下载时间" DataField="下载时间"/>
               <asp:BoundField  HeaderText="备注" DataField="备注"/>
               <asp:BoundField  HeaderText="更新员工"  DataField="更新员工"/>
               <asp:BoundField  HeaderText="更新时间" DataField="更新时间" />
               <asp:BoundField  HeaderText="告警类型"  DataField="告警类型"/>
               <asp:BoundField  HeaderText="告警级别"  DataField="告警级别"/>
               <asp:BoundField  HeaderText="卡类型"  DataField="卡类型" />
               <asp:BoundField  HeaderText="黑名单状态" DataField="黑名单状态"/>
               <asp:BoundField  HeaderText="黑名单类型"  DataField="黑名单类型"/>
         </Columns>   
           
         <EmptyDataTemplate>
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
             <tr class="tabbt">
                <td>卡号</td>
                <td>生成时间</td>
                <td>下载时间</td>
                <td>备注</td>
                <td>更新员工</td>
                <td>更新时间</td>
                <td>告警类型</td>
                <td>告警级别</td>
                <td>卡类型</td>
                <td>黑名单状态</td>
                <td>黑名单类型</td>
            </tr>
           </table>
          </EmptyDataTemplate>
          
        </asp:GridView>
        </div>
  </div>   
  
  <!-- 手工添加页面-->

  <div class="base">手工添加&amp;修改黑名单</div> 
   <div class="kuang5">
       
         <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
          <tr>
            <td width="10%" align="right">卡号:</td>
            <td width="20%"><asp:TextBox runat="server" ID="txtAddCardNo" CssClass="inputmid"  MaxLength="16"/><span class="red">*</span></td>
            <td align="right">生成时间:</td>
            <td><asp:Label runat="server" ID="labAddCreateTime"  /></td>
            <td align="right">下载时间:</td>
            <td><asp:Label runat="server" ID="labAddDownTime"/></td>
          
          </tr>
          
          <tr>
            <td align="right">更新员工:</td>
            <td><asp:Label runat="server" ID="labAddUpdateStaff"  /></td>
            <td align="right">更新时间:</td>
            <td><asp:Label runat="server" ID="labAddUpdateTime"/></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
           <td align="right">卡类型:</td>
            <td  >
            <asp:DropDownList runat="server" ID="selAddCardType" >
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:老公交卡" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:新CPU卡" Value="1" Selected="True"></asp:ListItem>
            </asp:DropDownList><span class="red">*</span> </td>
            
            <td align="right">黑名单状态:</td>
            <td  >
            <asp:DropDownList runat="server" ID="selAddBlackState" >
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:黑名单" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1:已锁定卡" Value="1"></asp:ListItem>
                <asp:ListItem Text="2:正常卡" Value="2" ></asp:ListItem>
            </asp:DropDownList><span class="red">*</span> 
            </td>
            
            <td align="right">黑名单类型:</td>
            <td  >
            <asp:DropDownList runat="server" ID="selAddBlackType" >
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:挂失黑名单" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:消费黑名单" Value="1" Selected="True"></asp:ListItem>
            </asp:DropDownList><span class="red">*</span> 
            </td>
          </tr>
          <tr>
            <td width="10%" align="right"> 告警类型:</td>
            <td width="20%"><asp:DropDownList ID="selAddWarnType" CssClass="inputmid" runat="server"/><span class="red">*</span></td>
            <td width="10%" align="right">告警级别:</td>
            <td width="20%"><asp:DropDownList ID="selAddWarnLevel" CssClass="inputmid" runat="server">
                <asp:ListItem Text="---请选择---" Value=""/>
                <asp:ListItem Text="0: 普通告警" Value="0"/>
                <asp:ListItem Text="1: 一级告警" Value="1"/>
                <asp:ListItem Text="2: 二级告警" Value="2"/>
                <asp:ListItem Text="3: 三级告警" Value="3"/>
                </asp:DropDownList><span class="red">*</span>   </td>
           </tr> 
          <tr>
           <td align="right">备注:</td>
           <td colspan="3"><asp:TextBox runat="server" ID="txtAddRemark" CssClass="inputmax" MaxLength="100" Width="400px" /></td>
           <td >&nbsp;</td>
           <td ><asp:Button ID="btnAdd" runat="server" Text="新增" CssClass="button1" OnClick="btnAdd_Click" />
                <asp:Button ID="btnMod" runat="server" Text="修改" CssClass="button1" OnClick="btnMod_Click" />
           </td>
          </tr>       
       </table>
  </div> 

  
 <%-- <!-- 编辑页面-->
  <div class="con" id="editpane">
         <div class="base">编辑&amp;删除黑名单</div>     
         <div class="kuang5" >
         <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
          <tr>
            <td align="right">卡号:</td>
            <td><asp:TextBox runat="server" ID="txtCardNo" CssClass="inputmid"  MaxLength="16"/><span class="red">*</span></td>
                <asp:HiddenField runat="server" ID="hidCardNo" />
                
            <td align="right">告警类型:</td>
            <td><asp:DropDownList ID="selWarnType" CssClass="inputmid" runat="server"/><span class="red">*</span></td>
            <td align="right">告警级别</td>
            <td><asp:DropDownList ID="selWarnLevel" CssClass="inputmid" runat="server">
                <asp:ListItem Text="---请选择---" Value=""/>
                <asp:ListItem Text="0: 普通告警" Value="0"/>
                <asp:ListItem Text="1: 一级告警" Value="1"/>
                <asp:ListItem Text="2: 二级告警" Value="2"/>
                <asp:ListItem Text="3: 三级告警" Value="3"/>
                </asp:DropDownList><span class="red">*</span>   </td>
            <td>&nbsp;</td>
          </tr>
           <tr>
           <td ><div align="right">卡类型:</div></td>
            <td  >
            <asp:DropDownList runat="server" ID="selEditCardType" Enabled="false">
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:老公交卡" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:新CPU卡" Value="1" Selected="True"></asp:ListItem>
            </asp:DropDownList></td>
            
            <td ><div align="right">黑名单状态:</div></td>
            <td  >
            <asp:DropDownList runat="server" ID="selEditBlackState" Enabled="false" >
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:黑名单" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1:已锁定卡" Value="1"></asp:ListItem>
                <asp:ListItem Text="2:正常卡" Value="2" ></asp:ListItem>
            </asp:DropDownList>
            </td>
            
            <td ><div align="right">黑名单类型:</div></td>
            <td  >
            <asp:DropDownList runat="server" ID="selEditBlackType" Enabled="false">
                <asp:ListItem Text="" Value=""  ></asp:ListItem>
                <asp:ListItem Text="0:挂失黑名单" Value="0"></asp:ListItem>
                <asp:ListItem Text="1:消费黑名单" Value="1" Selected="True"></asp:ListItem>
            </asp:DropDownList>
            </td>
          </tr>
          <tr>
            <td align="right">生成时间:</td>
            <td><asp:Label runat="server" ID="labCreateTime"  /></td>
            <td align="right">下载时间:</td>
            <td><asp:Label runat="server" ID="labDownTime"/></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
      <tr>
       <td align="right">备注:</td>
       <td colspan="4"><asp:TextBox runat="server" ID="txtRemark" CssClass="inputmax" MaxLength="100" /></td>
       <td colspan="2">
            
      </tr>       
       </table>
  </div>
  </div>--%>
  
  
</ContentTemplate>
     <%-- <Triggers>
        <asp:PostBackTrigger ControlID="btnDown" />
      </Triggers>--%>

</asp:UpdatePanel>
    </form>
</body>
</html>
