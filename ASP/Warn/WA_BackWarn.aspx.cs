﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.Warn;
using System.Text;
using Master;
/***************************************************************
 * WA_BackWarn.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 后台监控子系统 - 后台监控 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/4/18    王定喜           初次开发           
 ***************************************************************
 */
public partial class ASP_Warn_WA_BackWarn : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(ASP_Warn_WA_BackWarn));
        if (Page.IsPostBack) return;
        InitData(sender, e);
    }

    [AjaxPro.AjaxMethod]
    public DataTable LoadDataTable(string typecode)
    {
        try
        {
            CmnContext context1 = new CmnContext();
            context1.DataBaseType = ConfigurationManager.AppSettings.Get("DataBaseType");
            if (context1.DataBaseType != null)
                context1.DataBaseType = context1.DataBaseType.ToLower();
            string sql = string.Format("SELECT OBJECTID FROM TD_M_MONTIOR_PAGE", typecode) ;
            TMTableModule tm = new TMTableModule();
            DataTable dt = SPHelper.callWAQuery(context1, "QueryBackWarnDetail",
                  "", "", "", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd"), typecode);
            if (dt == null || dt.Rows.Count < 1)
            {
                return new DataTable();
            }
            else
            {
                return dt;
            }
        }
        catch (Exception ex)
        {
            Common.Log.Error(ex.Message, null,"AppLog");
            return new DataTable();
        }
       
    }

    [AjaxPro.AjaxMethod]
    protected string InitData_Ajax(string p)
    {
        return GetModelByDIV("", 4);
    }

    [AjaxPro.AjaxMethod]
    protected DataTable InitData_Ajax1()
    {
        Master.CmnContext context1 = new Master.CmnContext();
        context1.DataBaseType = ConfigurationManager.AppSettings.Get("DataBaseType");
        if (context1.DataBaseType != null)
            context1.DataBaseType = context1.DataBaseType.ToLower();
        return SPHelper.callWAQuery(context1, "LevelCount",
           "", "");
    }

    protected void InitData(object sender, EventArgs e)
    {

        DataTable dt = SPHelper.callWAQuery(context, "LevelCount",
           "", "");
        foreach (DataRow dr in dt.Rows)
        {
            if (dr[0].ToString() == "1")
            {
                lblWarn.Text = dr[1].ToString();
            }
            else if (dr[0].ToString() == "2")
            {
                lblerror.Text = dr[1].ToString();
            }
        }
        if (lblerror.Text == "")
        {
            lblerror.Text = "0";
        }
        if (lblWarn.Text == "")
        {
            lblWarn.Text = "0";
        }

        div1.InnerHtml = GetModelByDIV("", 4);
        div2.InnerHtml = GetModelByDIVByFlow("", 4);
        //div3.InnerHtml = GetModelByDIV("03", 4);
        //div4.InnerHtml = GetModelByDIV("04", 4);
    }

    public string GetModel(string type,int num)
    {
        try {
            DataTable source = null;

            source = SPHelper.callWAQuery(context, "MonitorModelList",  type);

            if (source.Rows.Count == 0)
            {
                return "";
            }
            
            StringBuilder text = new StringBuilder();
            text.Append("<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"5\" class=\"text25\">");
            DataRow row = null;

            int i = source.Rows.Count;
            int j = 0;
            if(i%num==0)
            {
               j=i;
            }
            else
            {
               j=num - i % num + i;
            }
            
            for (int m = 0; m < j; )
            {

                if (m % num == 0)
                {
                    text.Append("<tr>");
                }

                if (m < i)
                {
                    row = source.Rows[m];
                   // text.Append("<td align=\"right\" width='"+(100/num).ToString()+"%'>" + row[1].ToString() + ":</td>");
                   // text.Append("<td><div class=\"but_" + GetYanse(row[2].ToString()) + "_l\"; width:33px; height:33px;\"></div> </td>");
                    text.Append("<td><div class=\"but_" + GetYanse(row[2].ToString()) + "_l\">" + row[1].ToString() + "</div> </td>");
                }
                else
                {
                    text.Append("<td>&nbsp;</td>");
                    text.Append("<td>&nbsp;</td>");

                }
                m++;
                if (m % num == 0)
                {
                    text.Append("</tr>");
                }

            }
            text.Append("</table>");
            return text.ToString();
        }
        catch(Exception ex)
        {
            return ex.Message;
        }
        


    }

    //获取子模块列表
    public string GetModelByDIVByFlow(string type, int num)
    {
        try
        {
            Master.CmnContext context1 = new Master.CmnContext();
            context1.DataBaseType = ConfigurationManager.AppSettings.Get("DataBaseType");
            if (context1.DataBaseType != null)
                context1.DataBaseType = context1.DataBaseType.ToLower();
            DataTable source = null;
            DataTable source2 = null;
            StringBuilder text = new StringBuilder();
            source2 = SPHelper.callWAQuery(context1, "MonitorModelList2");//取编码是2位的类型列表

            foreach (DataRow dr2 in source2.Rows)
            {
                if (dr2["ISFLOW"].ToString() == "1")//走流程
                {
                    source = SPHelper.callWAQuery(context1, "MonitorModelListByFlow", dr2[0].ToString());//取编码是4位的类型列表
                    //text.Append("<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"5\" class=\"text25\">");
                    DataRow row = null;
                    //text.Append("<div class=\"card\">" + dr2[1].ToString() + "</div><div class=\"kuang6\" style=\" height:100%\">");
                    //text.Append("<div class=\"main_content_title\"><div class=\"main_content_title1\"><div class=\"main_content_smallbox\"></div><div class=\"main_content_title_txt\">" + dr2[1].ToString() + "</div></div><div class=\"main_content_title2\"></div><div class=\"c\"></div></div>");
                    text.Append("<div class=\"main_top_content\"><div class=\"main_top_content_center\">");
                    if (source.Rows.Count == 0)
                    {
                        //text.Append("</div>");
                        text.Append("<div class=\"c\"></div></div></div><div class=\"main_top_line1\"></div>");
                        continue;
                    }
                    int i = source.Rows.Count;
                    //text.Append("<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"row\"><tr><td background=\"../../Images/main_content_mid_left.png\" width=\"38px\"></td><td><div class=\"main_content_box\">");
                    for (int m = 0; m < i; )
                    {
                        row = source.Rows[m];
                        text.Append("<div id='"+m.ToString()+"' onmouseover=\"try{showDiv(this,'" + row[0].ToString() + "','" + row[1].ToString() + "')}catch(ex){};\" class=\"main_top_content_" + GetYanseByFlow(row[2].ToString()) + "\"><a href='WA_BackWarnDetail.aspx?TypeCode=" + row[0].ToString() + "' target='_black'>" + row[1].ToString() + "</a></div>");
                        //text.Append("<a href='WA_BackWarnDetail.aspx?TypeCode=" + row[0].ToString() + "' target='_black'><div  onmouseover=\"try{showDiv(this,'" + row[0].ToString() + "','" + row[1].ToString() + "')}catch(ex){};\" class=\"main_top_content_" + GetYanseByFlow(row[2].ToString()) + "\">" +row[1].ToString() + "</div></a> ");
                        m++;
                        if (m % num == 0)
                        {
                            text.Append("<div class=\"c\"></div>");
                        }
                    }
                    text.Append("<div class=\"c\"></div></div></div><div class=\"main_top_line1\"></div>");
                    //text.Append("<div class=\"c\"></div></div></td><td background=\"../../Images/main_content_mid_right.png\" width=\"13px\"></td></tr></table>");
                    //text.Append("<div class=\"main_content_bottom\"><div class=\"main_content_bottom_left\"></div><div class=\"main_content_bottom_right\"></div></div>");
                    //text.Append("</div>");
                }
                else
                {
                    continue;
                }

            }

            return text.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
    public string GetModelByDIV(string type, int num)
    {
        try
        {
            Master.CmnContext context1 = new Master.CmnContext();
            context1.DataBaseType = ConfigurationManager.AppSettings.Get("DataBaseType");
            if (context1.DataBaseType != null)
                context1.DataBaseType = context1.DataBaseType.ToLower();
            DataTable source = null;
            DataTable source2 = null;
            StringBuilder text = new StringBuilder();
            source2 = SPHelper.callWAQuery(context1, "MonitorModelList2");//取编码是2位的类型列表
            //return source2.Rows.Count.ToString();
            //int temp = 0;
            foreach (DataRow dr2 in source2.Rows)
            {
                if (dr2["ISFLOW"].ToString() == "1")
                {
                    continue;
                }
                   // temp++;
                    source = SPHelper.callWAQuery(context, "MonitorModelList", dr2[0].ToString());//取编码是4位的类型列表
                    //text.Append("<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"5\" class=\"text25\">");
                    DataRow row = null;
                    //text.Append("<div class=\"card\">" + dr2[1].ToString() + "</div><div class=\"kuang6\" style=\" height:100%\">");
                    text.Append("<div class=\"main_content_title\"><div class=\"main_content_title1\"><div class=\"main_content_smallbox\"></div><div class=\"main_content_title_txt\">" + dr2[1].ToString() + "</div></div><div class=\"main_content_title2\"></div><div class=\"c\"></div></div>");
                    text.Append("<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"row\"><tr><td background=\"../../Images/main_content_mid_left.png\" width=\"38px\"></td><td><div class=\"main_content_box\">");
                    if (source.Rows.Count == 0)
                    {
                        //text.Append("</div>");
                        text.Append("<div class=\"c\"></div></div></td><td background=\"../../Images/main_content_mid_right.png\" width=\"13px\"></td></tr></table>");
                        text.Append("<div class=\"main_content_bottom\"><div class=\"main_content_bottom_left\"></div><div class=\"main_content_bottom_right\"></div></div>");
                        continue;
                    }
                    int i = source.Rows.Count;
                    
                    for (int m = 0; m < i; )
                    {
                        row = source.Rows[m];
                        text.Append("<div onmouseover=\"try{showDiv(this,'" + row[0].ToString() + "','" + row[1].ToString() + "')}catch(ex){};\" class=\"main_content_col_" + GetYanse(row[2].ToString()) + "\"><a href='WA_BackWarnDetail.aspx?TypeCode=" + row[0].ToString() + "' target='_black'>" + row[1].ToString() + "</a></div>");
                        //text.Append("<a href='WA_BackWarnDetail.aspx?TypeCode=" + row[0].ToString() + "' target='_black'><div  onmouseover=\"try{showDiv(this,'" + row[0].ToString() + "','" + row[1].ToString() + "')}catch(ex){};\" class=\"but_" + GetYanse(row[2].ToString()) + "_l\">" + row[1].ToString() + "</div></a> ");
                        m++;
                        if (m % num == 0)
                        {
                            text.Append("<div class=\"c\"></div>");
                        }
                    }
                    text.Append("<div class=\"c\"></div></div></td><td background=\"../../Images/main_content_mid_right.png\" width=\"13px\"></td></tr></table>");
                    text.Append("<div class=\"main_content_bottom\"><div class=\"main_content_bottom_left\"></div><div class=\"main_content_bottom_right\"></div></div>");
                    //text.Append("<div class=\"c\"></div>");
                    //text.Append("</div>");
             
            }
           //return temp.ToString();
            return text.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }



    }

    public string GetClass(string type)
    {
        if (type == "0")
            return "green";
        else if (type == "1")
            return "yellow";
        else if (type == "2")
            return "red";
        else
            return "gray";
    }

    public string GetYanse(string type)
    {
        if (type == "0")
            return "green";
        else if (type == "1")
            return "yellow";
        else if (type == "2")
            return "red";
        else
            return "gray";
    }

    public string GetYanseByFlow(string type)
    {
        if (type == "0")
            return "t4";
        else if (type == "1")
            return "t3";
        else if (type == "2")
            return "t2";
        else
            return "t1";
    }
}
