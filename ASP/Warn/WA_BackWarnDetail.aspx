﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WA_BackWarnDetail.aspx.cs" Inherits="ASP_Warn_WA_BackWarnDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>监控详情列表</title>

    <script type="text/javascript" src="../../js/myext.js"></script>

    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ScriptManager1" runat="server" />

        <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="tb">
                    账务监控->后台监控日志
                </div>
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                            <td align="right">
                                    监控项目:</td>
                                <td>
                                     <asp:DropDownList ID="ddlTypeName" CssClass="inputmid" runat="server">
                                     </asp:DropDownList></td>
                                <td align="right">
                                    监控对象:</td>
                                <td>
                                     <asp:DropDownList ID="ddlMoniterObject" CssClass="inputmid" runat="server">
                                     </asp:DropDownList></td>
                                <td align="right">
                                    告警级别:</td>
                                <td>
                                    <asp:DropDownList ID="ddlMoniterLevel" CssClass="inputshort" runat="server">
                                    <asp:ListItem Value="">--请选择--</asp:ListItem>
                                       <asp:ListItem Value="0">正常</asp:ListItem>
                                          <asp:ListItem Value="1">警告</asp:ListItem>
                                             <asp:ListItem Value="2">严重</asp:ListItem>
                                    </asp:DropDownList></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                               <td align="right">
                                     扫描开始时间:</td>
                                <td>
                                  <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="inputshort"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                      
                                   </td>
                                <td align="right">
                                     扫描结束时间:</td>
                                <td>
                                     <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="inputshort"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                      
                                    </td>
                                      <td align="right">
                                     处理状态:</td>
                                <td>
                                    <asp:DropDownList ID="ddlFinStatus" CssClass="inputshort" runat="server">
                                    <asp:ListItem Value="">--请选择--</asp:ListItem>
                                     <asp:ListItem Value="0">未完成</asp:ListItem>
                                      <asp:ListItem Value="1">已完成</asp:ListItem>
                                       <asp:ListItem Value="2">已处理-</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Button ID="Button2" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" /></td>
                            </tr>
                        </table>
                    </div>
                    <div class="jieguo">
                        查询结果
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 260px">
                            <asp:GridView ID="gvResult" runat="server" Width="1580" CssClass="tab1" 
                                HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel"
                                AllowPaging="False" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" AutoGenerateColumns="True" OnSelectedIndexChanged="gvResult_SelectedIndexChanged"
                                EmptyDataText="没有数据记录!"
                                  OnRowDataBound="gvResult_RowDataBound"
                                >
                                <Columns>
                                <asp:TemplateField>
                            <HeaderTemplate>
                              <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="true" OnCheckedChanged="CheckAll" />
                            </HeaderTemplate>
                            <ItemTemplate>
                              <asp:CheckBox ID="ItemCheckBox" runat="server"/>
                            </ItemTemplate>
                          </asp:TemplateField>
                           <asp:TemplateField>
                            <HeaderTemplate>查看详情</HeaderTemplate>
                            <ItemTemplate>
                            <a href='WA_BackWarnDetailInfo.aspx?ObjectID=<%#Eval("对象ID") %>&NodeID=<%#Eval("节点ID") %>&INSTTIME=<%#Eval("扫描时间") %>'>查看详情</a>
                            </ItemTemplate>
                          </asp:TemplateField>
                          </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        
                         
                            <tr>
                                <td align="right">
                                    &nbsp;</td>
                                <td colspan="3">
                                    &nbsp;</td>
                                <td colspan="2">
                                    <table width="240px" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAdd" runat="server" Text="处理" CssClass="button1" OnClick="btnClose_Click" /></td>
                                          
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>




                   

            
             </div>




            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
