﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WA_BackWarnConfigure.aspx.cs" Inherits="ASP_Warn_WA_BackWarnConfigure"  EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>监控任务</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
   <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <div class="tb">
        <table cellpadding="0" cellspacing="0"  style="  width:100%;">
        <tr>
        <td> 账务监控->后台监控配置</td>
        <td align="right" style=" height:13px;">&nbsp;</td>
        </tr>
        </table>
       
        </div>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

        <div class="con">
          <div class="base">监控项目分配</div>
         <div class="kuang5">
   <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
     <tr>
       <td width="12%"><div align="right">监控项目:</div></td>
       <td width="23%"><asp:DropDownList ID="ddlTypeName" CssClass="inputmid" runat="server" 
        AutoPostBack="true" OnSelectedIndexChanged="ddlTypeName_SelectedIndexChanged" ></asp:DropDownList>
       </td>
       <td width="9%">&nbsp;</td>
       <td width="27%">&nbsp;</td>
     </tr>
   </table>
 </div>
  <div class="kuang5">
  <div class="gdtb1" style="height:300px">
  <asp:GridView ID="gvResult" runat="server"
        Width = "98%"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        >
           <Columns>
               <asp:TemplateField HeaderText="选定">
                <ItemTemplate>
                  <asp:CheckBox ID="ItemCheckBox" runat="server"/>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="code" HeaderText="编码"/>
                <asp:BoundField DataField="OBJECTNODE" HeaderText="对象:节点"/>
           </Columns>           
     <PagerSettings Mode="NumericFirstLast" />
     <SelectedRowStyle CssClass="tabsel" />
     <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
     <HeaderStyle CssClass="tabbt" />
     <AlternatingRowStyle CssClass="tabjg" />
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                        <td>
                            选定</td>
                        <td>
                            名称</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
</asp:GridView>
</div>
<div class="btns">
  <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td><asp:Button ID="btnConres" CssClass="button1" runat="server" Text="确定" OnClick="btnConres_Click" />
      </td>
    </tr>
  </table>
  <label></label>
  <label></label>
</div>
</div>
  <div class="base">监控项目列表(类型编码为2位的代表项目类别，4位代表要监控的项目)</div>
  <div class="kuang5">
  <div class="gdtb" style="height:300px">
  <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1" >
      
      
      <asp:GridView ID="lvwTypeList" runat="server"
         Width = "98%"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="false"
        OnRowCreated="lvwStaff_RowCreated" 
        OnSelectedIndexChanged="lvwStaff_SelectedIndexChanged"
        >
         <Columns>
                <asp:BoundField HeaderText="类型编码" DataField="TYPECODE"/>
                <asp:BoundField HeaderText="类型名称" DataField="TYPENAME"/>
                <asp:BoundField HeaderText="排序号" DataField="SORTNO"/>
                <asp:TemplateField HeaderText="是否显示">
                       <ItemTemplate>
                         <asp:Label ID="labDimTag" Text='<%# Eval("ISSHOW").ToString() == "0" ? "否" : "是" %>' runat="server"></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateField>
                       <asp:TemplateField HeaderText="是否走流程">
                       <ItemTemplate>
                         <asp:Label ID="labDimTag" Text='<%# Eval("ISFLOW").ToString() == "1" ? "是" : "否" %>' runat="server"></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateField>
            </Columns>   
            
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />        
        </asp:GridView>
    </table>
  </div>
  </div>
  <div class="kuang5">
   <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
     <tr>
       <td width="8%"><div align="right">排列序号:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtSort" runat="server" CssClass="inputmid" MaxLength="16" Width="96px" ></asp:TextBox></td>
     
       <td width="8%"><div align="right">类型编码:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtTypecode" runat="server" CssClass="inputmid" MaxLength="6" Width="96px" ></asp:TextBox><span class="red">*</span></td>
       <td width="8%"><div align="right">类型名称:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtTypeName" runat="server" CssClass="inputmid" MaxLength="40" Width="96px" ></asp:TextBox><span class="red">*</span></td>
       <td width="8%"><div align="right">是否显示:</div></td>
       <td style="height: 37px"><asp:CheckBox ID="chkIsshow" runat="server" /></td>
        <td width="8%"><div align="right">是否走流程:</div></td>
       <td style="height: 37px"><asp:CheckBox ID="chkIsFlow" runat="server" /></td>
     
     </tr>
    
     <tr>
       <td><div align="right"></div></td>
       <td>&nbsp;</td>
       <td colspan="4"><table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
         <tr>
           
           <td><asp:Button ID="btnStaffAdd" runat="server" Text="增加" CssClass="button1" OnClick="btnAdd_Click" /></td>
           <td><asp:Button ID="btnStaffModify" runat="server" Text="修改" CssClass="button1" OnClick="btnModify_Click" /></td>
           <td><asp:Button ID="btnStaffDelete" runat="server" Text="删除" CssClass="button1" OnClick="btnDelete_Click" /></td>
         </tr>
            </table></td>
      </tr>
   </table>
 </div>



         </div>


   <div class="footall"></div>
  
<div class="btns">
     

</div>

</ContentTemplate>
</asp:UpdatePanel>

    </form>
</body>
</html>
