﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
/***************************************************************
 * WA_BackWarnDetail.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 后台监控子系统 - 后台监控 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/4/18    王定喜           初次开发 
 ***************************************************************
 */
public partial class ASP_Warn_WA_BackWarnDetail : Master.Master
{
    public string Level
    {
        get {
            if (Request["level"] != null)
            {
                return Request["level"].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    public string TypeCode
    {
        get
        {
            if (Request["TypeCode"] != null)
            {
                return Request["TypeCode"].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(gvResult, null);

        SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGEPROJECTNAME");
        SPHelper.fillDDL(context, ddlMoniterObject, true, "SP_WA_Query", "ObjectList");
        ddlMoniterLevel.SelectedValue = Level;
        ddlFinStatus.SelectedValue = "1";
        ddlTypeName.SelectedValue = TypeCode;
        //txtFromDate.Text = DateTime.Now.ToString("yyyyMMdd");
        //txtToDate.Text = DateTime.Now.ToString("yyyyMMdd");
        UserCardHelper.resetData(gvResult,
           SPHelper.callWAQuery(context, "QueryBackWarnDetail",
              "", "1", Level, txtFromDate.Text, txtFromDate.Text, TypeCode));

        
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        UserCardHelper.resetData(gvResult,
            SPHelper.callWAQuery(context, "QueryBackWarnDetail",
               ddlMoniterObject.SelectedValue,ddlFinStatus.SelectedValue,ddlMoniterLevel.SelectedValue,txtFromDate.Text,txtToDate.Text));
    }


    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

           // e.Row.Cells[11].Text = GetYanse(e.Row.Cells[11].Text) ;
        }
    }

    public string GetYanse(string type)
    {
        if (type == "0")
            return "正常";
        else if (type == "1")
            return "警告";
        else if (type == "2")
            return "严重";
        else
            return "未开始";

    }


    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }



    protected void btnClose_Click(object sender, EventArgs e)
    {
        try {

            foreach (GridViewRow viewRow in gvResult.Rows)
            {
                CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
                if (ch != null && ch.Checked)
                {
                    if (viewRow.Cells[7].Text != "已完成")
                    {
                        context.AddError("存在处理状态不为已完成的记录，请查看");
                        return;
                    }
                }
            }

            foreach (GridViewRow viewRow in gvResult.Rows)
            {
                CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
                if (ch != null && ch.Checked)
                {
                    //context.AddError(string.Format("update TF_B_MONITER t set t.ffinstatus='2' where t.monitorid='{0}'", viewRow.Cells[1].Text));
                    //return;
                    context.DBOpen("Update");
                    context.ExecuteNonQuery(string.Format("update TF_B_MONITER t set t.ffinstatus='2',t.dealtime=sysdate where t.monitorid='{0}'", viewRow.Cells[2].Text));
                  
                    string str = "UPDATE TF_FILE_ENVIRONMENT_DETAIL D "
                                + "SET D.DEALCODE='1',D.DEALTIME=SYSDATE  "
                                + "WHERE D.DETAILID IN (   "
                                + "SELECT DE.DETAILID     "
                                + "FROM TD_M_FILE_REFRENCE R,TF_FILE_ENVIRONMENT_DETAIL DE  "
                                + "WHERE DE.FILEID=R.FILEID    "
                                + "AND R.FILETYPE='2'    "
                                + "AND R.OBJECTID='{0}'    "
                                + "AND R.NODEID='{1}'    "
                                + "AND TRUNC(DE.INSTTIME)=TRUNC(TO_DATE('{2}','YYYY-MM-DD HH24:MI:SS'))    )";
                    context.ExecuteNonQuery(string.Format(str, viewRow.Cells[3].Text, viewRow.Cells[4].Text, viewRow.Cells[9].Text));
                    context.DBCommit();
                    UserCardHelper.resetData(gvResult,
                    SPHelper.callWAQuery(context, "QueryBackWarnDetail",
                    ddlMoniterObject.SelectedValue, ddlFinStatus.SelectedValue, ddlMoniterLevel.SelectedValue, txtFromDate.Text, txtToDate.Text));
                }
            }
            AddMessage("关闭成功");
        }
        catch (Exception ex) {
            context.AddError(ex.Message);
        }
    }

    public String getDataKeys2(String keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }
    public void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行
        GridViewRow selectRow = gvResult.SelectedRow;
       
    }


    protected void CheckAll(object sender, EventArgs e)
    {
        //全选审核信息记录

        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            if (!gvr.Cells[0].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }




}
