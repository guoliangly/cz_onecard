﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;

public partial class ASP_Warn_WA_BlackList : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(gvResult, null);

        //SPHelper.fillDDL(context, selWarnType, true, "SP_WA_Query", "WarnTypeDDL");
        SPHelper.fillDDL(context, selAddWarnType, true,"SP_WA_Query", "WarnTypeDDL");
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //UserCardHelper.validateCardNo(context, txtCardNo1, true);

        if (context.hasError()) return;

        UserCardHelper.resetData(gvResult,
            SPHelper.callWAQuery(context, "QueryBlackList", txtCardNo1.Text, 
                                qryCardType.SelectedValue, qryBlackState.SelectedValue, qryBlackType.SelectedValue));
        //hidCardNo.Value = txtCardNo1.Text;
    }

    protected void btnDown_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            //SPHelper.callWAQuery(context, "UpdateDownloadTime", hidCardNo.Value);
            ExportToFile(gvResult, "黑名单列表_" + DateTime.Today.ToString("yyyyMMdd") + ".txt");
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    protected void ExportToFile(GridView gv, string filename)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "GB2312";
        Response.ContentType = "application/vnd.text"; Response.ContentType = "text/plain";
        Response.AddHeader("Content-disposition", "attachment; filename=" + Server.UrlEncode(filename));
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");

        foreach (GridViewRow gvr in gv.Rows)
        {
            Response.Write(gvr.Cells[1].Text);
            Response.Write("\r\n");
        }

        Response.Flush();
        Response.End();
    }

    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        btnQuery_Click(sender, e);

        gvResult.DataSource = SortDataTable(gvResult.DataSource as DataTable, true);
        gvResult.PageIndex = e.NewPageIndex;
        gvResult.DataBind();
    }
    protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
    {
        if (dataTable == null)
        {
            return new DataView();
        }

        DataView dataView = new DataView(dataTable);
        if (GridViewSortExpression != string.Empty)
        {
            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression,
                isPageIndexChanging ? GridViewSortDirection : GetSortDirection());
        }
        return dataView;
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    public void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行

        GridViewRow selectRow = gvResult.SelectedRow;
        txtAddCardNo.Text = selectRow.Cells[1].Text;
        hidCardNoForDelAndMod.Value = txtAddCardNo.Text;

        labAddCreateTime.Text = selectRow.Cells[2].Text;
        labAddDownTime.Text = selectRow.Cells[3].Text;
        txtAddRemark.Text = Server.HtmlDecode(selectRow.Cells[4].Text).Trim();

        labAddUpdateStaff.Text = selectRow.Cells[5].Text;
        labAddUpdateTime.Text = selectRow.Cells[6].Text;

        if (selectRow.Cells[7].Text=="&nbsp;") {
            selAddWarnType.SelectedIndex = -1;
        }else{
            selAddWarnType.SelectedValue = selectRow.Cells[7].Text.Substring(0, 1);
        }
        selAddWarnLevel.SelectedValue = selectRow.Cells[8].Text.Substring(0, 1);

        //新添加部分参数
        if (selectRow.Cells[11].Text == "&nbsp;"){
            selAddCardType.SelectedIndex = -1;
        }else{
            selAddCardType.SelectedValue = selectRow.Cells[9].Text.Substring(0, 1);
        }

        selAddBlackState.SelectedValue=selectRow.Cells[10].Text.Substring(0, 1);
        if (selectRow.Cells[11].Text=="&nbsp;") {
            selAddBlackType.SelectedIndex = -1;
        }else{
            selAddBlackType.SelectedValue = selectRow.Cells[11].Text.Substring(0, 1);
        }

        

        
    }

    private void validate(bool onlyCardNo)
    {
        Validation val = new Validation(context);

        //UserCardHelper.validateCardNo(context, txtAddCardNo, false);

        if (onlyCardNo) return;

        val.notEmpty(selAddWarnLevel, "A860P01005: 告警级别必须选择");
        val.notEmpty(selAddWarnType, "A860P010109: 告警类型必须选择");

        val.notEmpty(selAddCardType, "A860P010110: 卡类型必须选择");
        val.notEmpty(selAddBlackState, "A860P010111: 黑名单状态必须选择");
        val.notEmpty(selAddBlackType, "A860P010112: 黑名单类型必须选择");

        val.maxLength(txtAddRemark, 100, "A860P01009: 备注不能超过100位");

    }

    private void validateDelete()
    {
        Validation val = new Validation(context);

        val.notEmpty(txtAddCardNo, "A860P01013: 未选黑名单信息");

    }

    private void validateAdd()
    {
        Validation val = new Validation(context);

        //UserCardHelper.validateCardNo(context, txtAddCardNo, false);

        val.notEmpty(selAddWarnLevel, "A860P01005: 告警级别必须选择");
        val.notEmpty(selAddWarnType, "A860P010109: 告警类型必须选择");

        val.notEmpty(selAddCardType, "A860P010110: 卡类型必须选择");
        val.notEmpty(selAddBlackState, "A860P010111: 黑名单状态必须选择");
        val.notEmpty(selAddBlackType, "A860P010112: 黑名单类型必须选择");

        val.maxLength(txtAddRemark, 100, "A860P01009: 备注不能超过100位");
    }

    private void prepareSp()
    {
        context.DBOpen("StorePro");

        context.AddField("p_funcCode");
        context.AddField("p_oldCardNo");
        context.AddField("p_cardNo");

        //新增部分
        context.AddField("p_cardType", "Int32");
        context.AddField("p_blackState", "Int32");
        context.AddField("p_blackType", "Int32");
        context.AddField("p_blackLevel", "Int32");

        context.AddField("p_warnType");
        context.AddField("p_warnLevel", "Int32");
        context.AddField("p_remark");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        validateAdd();
        if (context.hasError()) return;

        prepareSp();
        context.SetFieldValue("p_funcCode", "Add");
        context.SetFieldValue("p_cardNo", txtAddCardNo.Text);

        //新增部分
        context.SetFieldValue("p_cardType", selAddCardType.SelectedValue);
        context.SetFieldValue("p_blackState", 0);//0:黑名单 1:锁定 2:正常 手工添加为0
        context.SetFieldValue("p_blackType", 1); //0 挂失黑名单 1 :消费黑名单 手工添加为1
        context.SetFieldValue("p_blackLevel", 1);   //以0,1,2,...往上，数值越大，级别越高，手工添加为1

        context.SetFieldValue("p_warnType", selAddWarnType.SelectedValue);
        context.SetFieldValue("p_warnLevel", 3); //手工添加为3级最高
        context.SetFieldValue("p_remark", txtAddRemark.Text);

        bool ok = context.ExecuteSP("SP_WA_BlackList");

        if (ok)
        {
            AddMessage("D860P02001: 新增黑名单成功");
            btnQuery_Click(sender, e);
            clearInfo();
        }
    }

    protected void btnMod_Click(object sender, EventArgs e)
    {
        validate(false);
        if (context.hasError()) return;

        prepareSp();
        context.SetFieldValue("p_funcCode", "Mod");
        context.SetFieldValue("p_oldCardNo", hidCardNoForDelAndMod.Value);
        context.SetFieldValue("p_cardNo", txtAddCardNo.Text);

        //新增部分
        context.SetFieldValue("p_cardType", selAddCardType.SelectedValue);
        context.SetFieldValue("p_blackState", selAddBlackState.SelectedValue);//0:黑名单 1:锁定 2:正常
        context.SetFieldValue("p_blackType", selAddBlackType.SelectedValue); //0 挂失黑名单 1 :消费黑名单
        context.SetFieldValue("p_blackLevel", 1);   //以0,1,2,...往上，数值越大，级别越高


        context.SetFieldValue("p_warnType", selAddWarnType.SelectedValue);
        context.SetFieldValue("p_warnLevel", selAddWarnLevel.SelectedValue);
        context.SetFieldValue("p_remark", txtAddRemark.Text);
        bool ok = context.ExecuteSP("SP_WA_BlackList");

        if (ok)
        {
            AddMessage("D860P02002: 修改黑名单成功");
            btnQuery_Click(sender, e);
            clearInfo();
        }
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        validateDelete();
        if (context.hasError()) return;

        prepareSp();
        context.SetFieldValue("p_funcCode", "Del");
        context.SetFieldValue("p_oldCardNo", hidCardNoForDelAndMod.Value);
        bool ok = context.ExecuteSP("SP_WA_BlackList");

        if (ok)
        {
            AddMessage("D860P02003: 删除黑名单成功");
            btnQuery_Click(sender, e);
            clearInfo();
        }
    }

    private void clearInfo(){
        txtAddCardNo.Text = "";
        hidCardNoForDelAndMod.Value = "";

        labAddCreateTime.Text = "";
        labAddDownTime.Text = "";
        txtAddRemark.Text = "";

        labAddUpdateStaff.Text = "";
        labAddUpdateTime.Text = "";

        selAddWarnType.SelectedIndex = -1;
        selAddWarnLevel.SelectedIndex = -1;

        //新添加部分参数
        selAddCardType.SelectedIndex = -1;
        selAddBlackState.SelectedIndex = -1;
        selAddBlackType.SelectedIndex = -1;
       
    }
}
