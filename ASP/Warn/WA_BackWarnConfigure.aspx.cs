﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.Warn;
using System.Text;

/***************************************************************
 * WA_BackWarnConfigure.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 后台监控子系统 - 后台监控 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/4/18    王定喜           初次开发 
 * 2011/4/21    王定喜           类型表里面加一个是否走流程，如果走流程，则节点间是有一个流向的
 ***************************************************************
 */
public partial class ASP_Warn_WA_BackWarnConfigure : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        lvwTypeList.DataKeyNames = new string[] { "TYPECODE", "TYPENAME", "SORTNO", "ISSHOW","ISFLOW","ID" };
        SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGEPROJECTNAME");
        //获取监控对象和节点列表
        UserCardHelper.resetData(gvResult,SPHelper.callWAQuery(context, "MonitorObjectNode"));
        //获取业务类型列表
        UserCardHelper.resetData(lvwTypeList,SPHelper.callWAQuery(context, "PAGETYPENAME"));
    }
    //监控配置
    protected void ddlTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        string sql = string.Format("select objectid from td_m_montior_page t where t.typecode='{0}'",ddlTypeName.SelectedValue);
        string objectandnode = "";
        DataTable data = tmTMTableModule.selByPKDataTable(context, sql.ToString(), 0);
        if (data != null && data.Rows.Count > 0)
        {
            objectandnode= data.Rows[0][0].ToString();
        }
        if (!string.IsNullOrEmpty(objectandnode))
        {
            for (int i = 0; i < gvResult.Rows.Count; i++)
            {
                GridViewRow viewRow = gvResult.Rows[i];
                String roleNo = viewRow.Cells[1].Text;
                CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
                if (!objectandnode.Contains(roleNo))
                    ch.Checked = false;
                else
                {
                    ch.Checked = true;
                }
            }
        }
        else
        {
            for (int i = 0; i < gvResult.Rows.Count; i++)
            {
                GridViewRow viewRow = gvResult.Rows[i];
                CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
                ch.Checked = false;
            }
        }
    }

    //类型对应后台对象和节点
    protected void btnConres_Click(object sender, EventArgs e)
    {
        try
        {
            string objectandid = "";
            foreach (GridViewRow viewRow in gvResult.Rows)
            {
                CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
                if (ch != null && ch.Checked)
                {
                    objectandid += viewRow.Cells[1].Text + ",";
                }
            }
            objectandid = objectandid.TrimEnd(',');
            context.DBOpen("Update");
            context.ExecuteNonQuery(string.Format("update td_m_montior_page t set t.objectid='{0}' where t.typecode='{1}'", objectandid, ddlTypeName.SelectedValue));
            context.DBCommit();
            AddMessage("更新配置成功");
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message);
        }
       
    }


    protected void lvwStaff_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwTypeList','Select$" + e.Row.RowIndex + "')");
        }
    }


    protected void lvwStaff_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSort.Text = getDataKeys2("SORTNO");
        txtTypecode.Text = getDataKeys2("TYPECODE");
        txtTypeName.Text = getDataKeys2("TYPENAME");
        chkIsshow.Checked = getDataKeys2("ISSHOW") == "0" ? false : true;
        chkIsFlow.Checked = getDataKeys2("ISFLOW") == "1" ? true : false;
    }

    public String getDataKeys2(String keysname)
    {
        try {

            return lvwTypeList.DataKeys[lvwTypeList.SelectedIndex][keysname].ToString();
        }
        catch (Exception ex)
        {
            return "";
        }
        
    }

    public void validate()
    {
        if (txtSort.Text != "")
        {
            if (!Common.Validation.isNum(txtSort.Text))
            {
                context.AddError("排序号应为数字",txtSort);
            }
        }
        if (string.IsNullOrEmpty(txtTypecode.Text))
        {
            context.AddError("类型编码不能为空",txtTypecode);
        }
        else if (!Common.Validation.isNum(txtTypecode.Text)|| txtTypecode.Text.Length > 4 || txtTypecode.Text.Length % 2 != 0)
        {
            context.AddError("类型编码应为2位或4位数字", txtTypecode);
        }
        if (string.IsNullOrEmpty(txtTypeName.Text))
        {
            context.AddError("类型名称不能为空", txtTypeName);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Insert");
            context.ExecuteNonQuery(string.Format("insert into td_m_montior_page (TYPECODE,TYPENAME,SORTNO,ISSHOW,Updatetime,Updatestaffno,Updatedepartno,ISFLOW) values('{0}','{1}',{2},'{3}',to_date('{4}','yyyy-mm-dd hh24:mi:ss'),'{5}','{6}','{7}')", txtTypecode.Text.Trim(), txtTypeName.Text.Trim(), txtSort.Text.Trim(), chkIsshow.Checked ? "1" : "0", DateTime.Now.ToString(), context.s_UserID, context.s_DepartID, chkIsFlow.Checked ? "1" : "0"));
            context.DBCommit();
            AddMessage("类型添加成功");
            SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGETYPENAME");
            UserCardHelper.resetData(lvwTypeList,
             SPHelper.callWAQuery(context, "PAGETYPENAME"));
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message.Contains("ORA-00001")?"类型编码不能重复添加":ex.Message);
        }
        
    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(getDataKeys2("ID")))
            {
                context.AddError("请选择一条记录");
                return;
            }
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Update");
            context.ExecuteNonQuery(string.Format("update  td_m_montior_page set typecode='{1}',typename='{2}',sortno={3},isshow='{4}',isflow='{5}' where id={0}", getDataKeys2("ID"), txtTypecode.Text.Trim(), txtTypeName.Text.Trim(), txtSort.Text.Trim(), chkIsshow.Checked ? "1" : "0", chkIsFlow.Checked ? "1" : "0"));
            context.DBCommit();
            AddMessage("类型修改成功");
            SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGETYPENAME");
            UserCardHelper.resetData(lvwTypeList,
             SPHelper.callWAQuery(context, "PAGETYPENAME"));
            ClearType();

        }
        catch (Exception ex)
        {
            context.AddError(ex.Message.Contains("ORA-00001") ? "类型编码不能重复添加" : ex.Message);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(getDataKeys2("ID")))
            {
                context.AddError("请选择一条记录");
                return;
            }
            validate();
            if (context.hasError())
                return;
            context.DBOpen("Delete");
            context.ExecuteNonQuery(string.Format("delete from  td_m_montior_page where id={0}", getDataKeys2("ID")));
            context.DBCommit();
            AddMessage("类型删除成功");
            SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGETYPENAME");
            UserCardHelper.resetData(lvwTypeList,
             SPHelper.callWAQuery(context, "PAGETYPENAME"));
            ClearType();
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message.Contains("ORA-00001") ? "类型编码不能重复添加" : ex.Message);
        }
    }

    private void ClearType()
    {
        //清除输入的员工信息

        txtSort.Text = "";
        txtTypecode.Text = "";
        txtTypeName.Text = "";
        chkIsshow.Checked = false;
    }
}
