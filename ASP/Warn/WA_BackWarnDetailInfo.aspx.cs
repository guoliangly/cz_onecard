﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
/***************************************************************
 * WA_BackWarnDetailInfo.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 后台监控子系统 - 后台监控日志详情 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/4/18    王定喜           初次开发
 * 2010/4/26    王定喜           处理按钮点击后的处理过程的变更。           
 ***************************************************************
 */
public partial class ASP_Warn_WA_BackWarnDetailInfo : Master.Master
{
    public string Level
    {
        get {
            if (Request["level"] != null)
            {
                return Request["level"].ToString();
            }
            else
            {
                return "";
            }
        }
    }
    public string ObjectID
    {
        get
        {
            if (Request["ObjectID"] != null)
            {
                return Request["ObjectID"].ToString();
            }
            else
            {
                return "";
            }
        }
    }
    public string NodeID
    {
        get
        {
            if (Request["NodeID"] != null)
            {
                return Request["NodeID"].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    public string TypeCode
    {
        get
        {
            if (Request["TypeCode"] != null)
            {
                return Request["TypeCode"].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    public string INSTTIME
    {
        get
        {
            if (Request["INSTTIME"] != null)
            {
                if (Request["INSTTIME"].ToString() == "NOW")
                    return DateTime.Now.ToString("yyyyMMdd");
                return Request["INSTTIME"].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        gvResult.DataKeyNames = new string[] { "MONITORID", "OBJECTID", "NODEID", "FILEID", "DETAILID" };
        UserCardHelper.resetData(gvResult, null);
        SPHelper.fillDDL(context, ddlTypeName, true, "SP_WA_Query", "PAGEPROJECTNAME");
        SPHelper.fillDDL(context, ddlMoniterObject, true, "SP_WA_Query", "ObjectList");
        ddlNode.Items.Add(new ListItem("--请选择--", ""));
        ddlMoniterLevel.SelectedValue = Level;
        if(ObjectID=="")
        ddlFinStatus.SelectedValue = "1";
        ddlTypeName.SelectedValue = TypeCode;
        ddlMoniterObject.SelectedValue = ObjectID;
        ddlNode_SelectedIndexChanged(sender, e);
        ddlNode.SelectedValue = NodeID;
        txtFromDate.Text = INSTTIME;
        txtToDate.Text = INSTTIME;
        UserCardHelper.resetData(gvResult,
           SPHelper.callWAQuery(context, "MonitorDetailInfo", ddlMoniterObject.SelectedValue, ddlNode.SelectedValue, ddlFinStatus.SelectedValue, ddlMoniterLevel.SelectedValue, txtFromDate.Text, txtToDate.Text, ddlTypeName.SelectedValue));

        
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        UserCardHelper.resetData(gvResult,
          SPHelper.callWAQuery(context, "MonitorDetailInfo", ddlMoniterObject.SelectedValue, ddlNode.SelectedValue, ddlFinStatus.SelectedValue, ddlMoniterLevel.SelectedValue, txtFromDate.Text, txtToDate.Text, ddlTypeName.SelectedValue));


    }


    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            e.Row.Cells[4].Text = GetYanse(e.Row.Cells[4].Text) ;
            e.Row.Cells[5].Text = GetDealState(e.Row.Cells[5].Text);
            e.Row.Cells[22].Text = GetDealState1(e.Row.Cells[22].Text);
        }
    }

    public string GetYanse(string type)
    {
        if (type == "0")
            return "正常";
        else if (type == "1")
            return "警告";
        else if (type == "2")
            return "严重";
        else
            return "未开始";

    }
    public string GetDealState1(string type)
    {
        if (type == "0")
            return "未处理";
        else if (type == "1")
            return "已处理";
        else
            return "";

    }
    public string GetDealState(string type)
    {
        if (type == "0")
            return "未完成";
        else if (type == "1")
            return "已完成";
        else if (type == "2")
            return "已处理";
        else
            return "";

    }


    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }



    protected void btnClose_Click(object sender, EventArgs e)
    {
        //判断是否存在处理状态是未处理的记录
        foreach (GridViewRow viewRow in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
            if (ch != null && ch.Checked)
            {
                if (viewRow.Cells[5].Text != "已完成")
                {
                    context.AddError("存在处理状态不为已完成的记录，请查看");
                    return;
                }
            }
        }

        //先判断是否有记录未处理
        foreach (GridViewRow viewRow in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)viewRow.FindControl("ItemCheckBox");
            if (ch != null && ch.Checked)
            {
                //context.AddError(string.Format("update TF_B_MONITER t set t.ffinstatus='2' where t.monitorid='{0}'", viewRow.Cells[1].Text));
                //return;
                string sql="";
                //文件环境详情ID不为空
               if(Common.Validation.isNum( viewRow.Cells[23].Text))
               {
                   //文件环境实时明细表的处理状态变为已处理
                  context.DBOpen("Update");
                  sql= string.Format("UPDATE TF_FILE_ENVIRONMENT_DETAIL D SET D.DEALCODE='1',D.DEALTIME=SYSDATE WHERE D.DETAILID={0}", viewRow.Cells[23].Text);
                  context.ExecuteNonQuery(sql);
                  context.DBCommit();
                   //是否存在未处理的文件
                  string sql1 = string.Format("SELECT COUNT(*) FROM TD_M_FILE_REFRENCE R,TF_FILE_ENVIRONMENT_DETAIL D"
                          + " WHERE D.FILEID=R.FILEID　AND R.FILETYPE='2' AND D.DEALCODE='0' AND R.OBJECTID={0} "
                          + " AND R.NODEID={1} AND TRUNC(D.INSTTIME)=TRUNC(TO_DATE('{2}','YYYY-MM-DD  HH24:MI:SS')) ", viewRow.Cells[25].Text, viewRow.Cells[26].Text, viewRow.Cells[7].Text);
                  TMTableModule tm = new TMTableModule();
                  DataTable dt = tm.selByPKDataTable(context, sql1, 0);
                  Object obj = dt.Rows[0].ItemArray[0];
                  if (Convert.ToInt32(obj) == 0)
                  {
                      //如果文件已全部处理完，则关联的监控主表日志也要一并处理
                      context.DBOpen("Update");
                      sql = string.Format("UPDATE TF_B_MONITER T SET T.DEALTIME=SYSDATE,T.FFINSTATUS='2' WHERE T.MONITORID={0}", viewRow.Cells[24].Text);
                      context.ExecuteNonQuery(sql);
                      context.DBCommit();
                  }
               }
               else//文件环境详情ID为空，则直接处理关联的主表日志记录
               {
                  context.DBOpen("Update");
                  sql= string.Format("UPDATE TF_B_MONITER T SET T.DEALTIME=SYSDATE,T.FFINSTATUS='2' WHERE T.MONITORID={0}", viewRow.Cells[24].Text);
                  context.ExecuteNonQuery(sql);
                  context.DBCommit();
               }
               UserCardHelper.resetData(gvResult,
               SPHelper.callWAQuery(context, "MonitorDetailInfo", ddlMoniterObject.SelectedValue, ddlNode.SelectedValue, ddlFinStatus.SelectedValue, ddlMoniterLevel.SelectedValue, txtFromDate.Text, txtToDate.Text,ddlTypeName.SelectedValue));
            }
        }
        AddMessage("关闭成功");

    }
    protected void ddlNode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMoniterObject.SelectedValue != "")
        {
            SPHelper.fillDDL(context, ddlNode, true, "SP_WA_Query", "NODEBYOBJECTID", ddlMoniterObject.SelectedValue);
        }
        else
        {
            ddlNode.Items.Clear();
            ddlNode.Items.Add(new ListItem("--请选择--", ""));
        }
        
    }

    public void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行
        GridViewRow selectRow = gvResult.SelectedRow;
       
    }


    protected void CheckAll(object sender, EventArgs e)
    {
        //全选审核信息记录

        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            if (!gvr.Cells[0].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }




}
