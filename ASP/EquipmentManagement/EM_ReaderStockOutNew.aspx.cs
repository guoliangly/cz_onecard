﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using PDO;

/*******************************************
 * create:chenwentao 2014-11-12
 * desc:充付器出库
 * *****************************************/
public partial class ASP_EquipmentManagement_EM_ReaderStockOutNew : Master.Master
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDL();
        }
    }

    /// <summary>
    /// dropdownlist绑定数据
    /// </summary>
    private void BindDDL()
    {
        //充付器类型
        ReaderHelper.ReaderTypeBindDDL(context,ddlReaderType);

        //出库充付器类型
        ReaderHelper.ReaderTypeBindDDL(context,ddlOutType);

        //领用部门
        UserCardHelper.selectDepts(context,ddlDept, true); 
    }

    #endregion

    #region Event

    /// <summary>
    /// 根据条件查询出符合条件可以出库的充付器
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        gvResult.DataSource = null;
        gvResult.DataBind();
        DataTable ReaderData = SPHelper.callQuery("SP_EM_ReaderQuery", context, "StockOut", ddlReaderType.SelectedValue, txtQryBeginNo.Text, txtQryEndNo.Text);
        gvResult.DataSource = ReaderData;
        gvResult.DataBind();
    }

    /// <summary>
    /// 分页
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_Page(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    /// <summary>
    /// 改变起始序列号计算结束序列号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtBeginNo_TextChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtBeginNo.Text.Trim()))
        {
            context.AddError("序列号不能为空", txtBeginNo);
            return;
        }
        if (!Validation.isNum(txtBeginNo.Text.Trim()))
        {
            context.AddError("起始序列号必须为数字",txtBeginNo);
            return;
        }
        if (txtBeginNo.Text.Trim().Length != 16)
        {
            context.AddError("序列号长度必须是16位",txtBeginNo);
            return;
        }

        if (string.IsNullOrEmpty(txtCount.Text))
            return;
        if (!Validation.isNum(txtCount.Text.Trim()))
        {
            context.AddError("出库数量必须是数字",txtCount);
            return;
        }

        if (string.IsNullOrEmpty(GetEndReaderNo()))
        {
            context.AddError("出库数量太大，请确认", txtCount);
            return;
        }
        txtEndNo.Text = GetEndReaderNo();
        hidType.Value = GetReaderTypeName();
    }

    /// <summary>
    /// 改变数量计算结束序列号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtCount_TextChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtBeginNo.Text.Trim()))
        {
            context.AddError("序列号不能为空", txtBeginNo);
            return;
        }
        if (!Validation.isNum(txtBeginNo.Text.Trim()))
        {
            context.AddError("起始序列号必须为数字", txtBeginNo);
            return;
        }
        if (txtBeginNo.Text.Trim().Length != 16)
        {
            context.AddError("序列号长度必须是16位", txtBeginNo);
            return;
        }

        if (string.IsNullOrEmpty(txtCount.Text))
        {
            context.AddError("出库数量不能为空",txtCount);
            return;
        }
        if (!Validation.isNum(txtCount.Text.Trim()))
        {
            context.AddError("出库数量必须是数字",txtCount);
            return;
        }

        if (string.IsNullOrEmpty(GetEndReaderNo()))
        {
            context.AddError("出库数量太大，请确认",txtCount);
            return;
        }
        txtEndNo.Text = GetEndReaderNo();
        hidType.Value = GetReaderTypeName();
    }

    /// <summary>
    /// 确认提交事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        context.SPOpen();
        context.AddField("P_FROMREADERNO").Value = txtBeginNo.Text.Trim();
        context.AddField("P_TOREADERNO").Value = txtEndNo.Text.Trim();
        context.AddField("P_READERNUMBER").Value = txtCount.Text.Trim();
        context.AddField("p_DEPT").Value = ddlDept.SelectedValue;
        context.AddField("p_MONEY").Value = (Convert.ToInt32(txtMoney.Text.Trim()) * 100).ToString();
        context.AddField("P_REMARK").Value = txtRemark.Text.Trim();

        bool ok = context.ExecuteSP("SP_EM_READERSTOCKOUT");
        if (ok)
        {
            context.AddMessage("出库成功");
            ClearInput();
        }
    }

    /// <summary>
    /// 出库按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnStockOut_Click(object sender, EventArgs e)
    {
        if (!ValidateInput())
            return;

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Check", "submitConfirm();", true);
    }

    /// <summary>
    /// 选择类型之后把当前类型下，可以出库的最小序列号显示到起始文本框中
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlOutType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtBeginNo.Text = "";
        txtEndNo.Text = "";
        txtMoney.Text = "";
        if(string.IsNullOrEmpty(ddlOutType.SelectedValue))
            return;
        string OutMinNoSql = string.Format("select * from tl_r_reader where typeid='{0}' and readerstate='0' order by SERIALNUMBER asc",Convert.ToInt32(ddlOutType.SelectedValue));
        context.DBOpen("Select");
        DataTable OutMinNoData = context.ExecuteReader(OutMinNoSql);
        context.DBCommit();
        if (OutMinNoData == null || OutMinNoData.Rows.Count == 0)
        {
            context.AddMessage("当前型号下不存在可以出库的充付器");
            return;
        }
        txtBeginNo.Text = OutMinNoData.Rows[0]["SERIALNUMBER"].ToString();
    }

    #endregion

    #region Function

    /// <summary>
    /// 取出充付器类型
    /// </summary>
    /// <returns></returns>
    private string GetReaderTypeName()
    {
        string TypeSql = string.Format("select t2.name from TL_R_READER t1,tf_f_readertype t2 where t1.SERIALNUMBER='{0}' and t1.typeid=t2.typeid",txtBeginNo.Text.Trim());
        context.DBOpen("Select");
        DataTable TypeData = context.ExecuteReader(TypeSql);
        context.DBCommit(); ;
        if (TypeData == null || TypeData.Rows.Count == 0)
            return "";
        return TypeData.Rows[0]["name"].ToString();
    }

    /// <summary>
    /// 出库条件验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateInput()
    {
        string ReaderStartNo = txtBeginNo.Text.Trim();
        string ReaderCount = txtCount.Text;
        string ReaderDept = ddlDept.SelectedValue;
        string Remark = txtRemark.Text.Trim();
        string Money = txtMoney.Text;

        //验证起始序列号
        if (string.IsNullOrEmpty(ReaderStartNo))
        {
            context.AddError("起始序列号不能为空",txtBeginNo);
        }
        else if (ReaderStartNo.Length != 16)
        {
            context.AddError("起始序列号长度必须为16位",txtBeginNo);
        }
        else if (!Validation.isNum(ReaderStartNo))
        {
            context.AddError("起始序列号中存在字符", txtBeginNo);
        }

        //数量
        if (string.IsNullOrEmpty(ReaderCount))
        {
            context.AddError("出库数量不能为空",txtCount);
        }
        else if (!Validation.isNum(ReaderCount))
        {
            context.AddError("出库数量必须是数字",txtCount);
        }

        //部门
        if (string.IsNullOrEmpty(ReaderDept))
        {
            context.AddError("领用部门必须选择",ddlDept);
        }

        //销售金额
        if (string.IsNullOrEmpty(Money))
        {
            context.AddError("销售金额不能为空", txtMoney);
        }
        else if (!Validation.isNum(Money))
        {
            context.AddError("销售金额必须是数字",txtMoney);
        }

        //备注
        if (!string.IsNullOrEmpty(Remark))
            if (Remark.Length > 50)
                context.AddError("备注不能超过50位",txtRemark);

        if (context.hasError())
            return false;
        return true;
    }

    /// <summary>
    /// 根据起始序列号和数量计算出结束序列号
    /// </summary>
    /// <returns>返回""表示出库数量太大</returns>
    private string GetEndReaderNo()
    {
        string ReaderNoStart = txtBeginNo.Text.Trim().Substring(0, 10);
        string ReaderNoNum = txtBeginNo.Text.Trim().Substring(10,6);
        int EndNum = Convert.ToInt32(ReaderNoNum) + Convert.ToInt32(txtCount.Text.Trim())-1;
        if (EndNum.ToString().Length > 6)
        {
            return "";
        }
        string EndReaderNo = ReaderNoStart + new string('0', 6 - EndNum.ToString().Length) + EndNum.ToString();
        return EndReaderNo;
    }

    /// <summary>
    /// 清空输入条件
    /// </summary>
    private void ClearInput()
    {
        ddlDept.SelectedValue = "";
        ddlOutType.SelectedValue = "";
        txtBeginNo.Text = "";
        txtCount.Text = "";
        txtMoney.Text = "";
        txtEndNo.Text = "";
        txtRemark.Text = "";
    }

    #endregion
    
}