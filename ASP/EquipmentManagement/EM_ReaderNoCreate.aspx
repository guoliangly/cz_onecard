﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EM_ReaderNoCreate.aspx.cs" Inherits="ASP_EquipmentManagement_EM_ReaderNoCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
     充付器模块->序列号生成
    </div>
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
    ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
    <ContentTemplate>
        <asp:BulletedList ID="bulMsgShow" runat="server">
        </asp:BulletedList>
        <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
        <div class="con">
                <div class="card">
                    充付器序列号生成</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="12%" align="right">
                                厂商:
                            </td>
                            <td width="35%">
                                <asp:DropDownList runat="server" ID="ddlManu" CssClass="inputmidder"></asp:DropDownList>
                                <span class="red">*</span>
                            </td>
                            <td width="12%">
                                <div align="right">
                                     年份:</div>
                            </td>
                            <td width="15%">
                                <asp:TextBox ID="txtYear" runat="server" CssClass="input" MaxLength="2"></asp:TextBox>
                                <span class="red">*</span>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    批次号:</div>
                            </td>
                            <td width="25%">
                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="input" MaxLength="2" OnTextChanged="txtBatchNo_TextChanged"
                                     AutoPostBack="true"></asp:TextBox>
                                <span class="red">*</span>
                            </td>
                        </tr>
                        <tr>
                           <td width="12%" align="right">
                                数量:
                            </td>
                           <td width="12%" align="left">
                               <asp:TextBox runat="server" ID="txtNumber" CssClass="input"></asp:TextBox><span class="red">*</span>
                                <%--<asp:TextBox runat="server" ID="txtNo" CssClass="input" MaxLength="6"></asp:TextBox>
                                <span class="red">*</span>--%>
                            </td>
                            <td width="12%" align="right">
                                应用平台类型:
                            </td>
                            <td width="12%" align="left">
                                <asp:DropDownList runat="server" ID="ddlPlatform" CssClass="input"></asp:DropDownList>
                                <span class="red">*</span>
                            </td>
                            <td width="12%" align="right"></td>
                            <td width="12%" align="left">
                                <asp:Button ID="btnCreate" CssClass="button1" runat="server" Text="生成" OnClick="btnCreate_Click"/>
                            </td>
                            <td width="12%" align="right"></td>
                            <td width="12%" align="left">
                                <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出" OnClick="btnExport_Click"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        <div id="printarea" class="kuang5" style="display:none">
                <div id="gdtbfix" style="height:380px">
           <asp:GridView ID="gvResult" runat="server"
                            Width = "95%"
                            CssClass="tab2"
                            HeaderStyle-CssClass="tabbt" 
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            EmptyDataText="查询结果为空"
                            EmptyDataRowStyle-Font-Bold="true"
                            EmptyDataRowStyle-BorderWidth="0"
                            ShowFooter="false" AutoGenerateColumns="false"
                            >
                <Columns>
                <asp:BoundField HeaderText="序号" DataField="序号" />
                <asp:BoundField HeaderText="序列号" DataField="序列号"/>
                <asp:BoundField HeaderText="密钥" DataField="密钥"/>
                </Columns>
               </asp:GridView>
                </div>
              </div>
    </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
