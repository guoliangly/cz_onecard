﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data;
using Common;

/****************************************
 * create:chenwentao 2014-11-07
 * content:充付器序列号和密钥生成
 * **************************************/
public partial class ASP_EquipmentManagement_EM_ReaderNoCreate : Master.ExportMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDL();
            txtBatchNo.Text = GetMaxBatchNo();

            //初始化年份
            string Year = System.DateTime.Now.Year.ToString();
            txtYear.Text = Year.Substring(2,2);

            //存储本次生成的序列号开始、结束值
            ViewState["startreaderno"] = null;
            ViewState["endreaderno"] = null;
        }
    }

    /// <summary>
    /// 下拉列表绑定数据
    /// </summary>
    private void BindDDL()
    {
        //充付器厂商
        ReaderHelper.ManuBindDDL(context,ddlManu);

        //平台类型
        ReaderHelper.PlatFormBindDDL(context,ddlPlatform);
    }

    #endregion

    #region Event

    /// <summary>
    /// 生成序列号
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (!ValidateInput())
        {
            return;
        }

        string SessionID = Session.SessionID;
        ClearTempTable(SessionID);
        int TempNo = Convert.ToInt32(GetReaderNo());
        for (int i = 0; i < Convert.ToInt32(txtNumber.Text); i++)
        {
            //构造充付器卡号serialnumber
            StringBuilder SerialNumber = new StringBuilder();
            SerialNumber.Append(ddlManu.SelectedValue); //厂商
            SerialNumber.Append(new string('0',4-ddlPlatform.SelectedValue.Length));
            SerialNumber.Append(ddlPlatform.SelectedValue); //应用平台
            SerialNumber.Append(txtYear.Text);  //年份
            SerialNumber.Append(txtBatchNo.Text);  //批次

            TempNo = TempNo + 1;
            if (TempNo.ToString().Length > 6)
            {
                context.AddError("充付器序号已超出所分配的位数!");
                return;
            }
            string ReaderNo = new string('0',6-TempNo.ToString().Length)+TempNo.ToString();
            SerialNumber.Append(ReaderNo);
            if (i == 0)
            {
                ViewState["startreaderno"] = SerialNumber.ToString();
            }
            if (i == Convert.ToInt32(txtNumber.Text)-1)
            {
                ViewState["endreaderno"] = SerialNumber.ToString();
            }
            string ReaderKey = GetKey(SerialNumber.ToString());

            string InsertSql = string.Format("insert into TMP_COMMON(f0,f1,f2,f3,f4,f5,f6) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",SessionID,SerialNumber,ddlManu.SelectedValue,ReaderKey,txtYear.Text,txtBatchNo.Text,ddlPlatform.SelectedValue);
            context.DBOpen("Insert");
            context.ExecuteNonQuery(InsertSql);
        }
        if (!context.hasError())
        {
            context.DBCommit();
        }
        else
        {
            context.RollBack();
        }

        context.SPOpen();
        context.AddField("p_sessionid").Value = SessionID;
        bool ok=context.ExecuteSP("SP_EM_ReaderNoCreate");
        if (ok)
        {
            context.DBCommit();
            context.AddMessage("充付器序列号生成成功!");
            ClearInput();
        }
    }

    /// <summary>
    /// 导出序列号和密钥
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (GetExportData() == null)
        {
            return;
        }
        gvResult.DataSource = GetExportData();
        gvResult.DataBind();

        //文件名
        string FileName = "常州市民卡充付器批量定制信息(NO." + DateTime.Now.ToString("yyyyMMdd")+"-"+DateTime.Now.ToString("hhmmss")+")";

        ExportGridView(gvResult,FileName);
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
    }

    /// <summary>
    /// 批次输入改变事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtBatchNo_TextChanged(object sender, EventArgs e)
    {
        if (IsExistBatchNo(txtBatchNo.Text.Trim()))
        {
            context.AddMessage("输入的批次号库存中已经存在，请确认是否批次是否输入正确！");
        }
    }

    #endregion

    #region Function

    /// <summary>
    /// 查询出要导出的充付器序列号和密钥
    /// </summary>
    /// <returns></returns>
    private DataTable GetExportData()
    {
        if (ViewState["startreaderno"] == null || ViewState["endreaderno"]==null)
        {
            context.AddError("还没生成充付器序列号，不能导出！");
            return null;
        }
        string StartReaderNo = ViewState["startreaderno"].ToString();
        string EndReaderNo = ViewState["endreaderno"].ToString();

        string QryData = string.Format("select rownum 序号,to_char(serialnumber) 序列号,key 密钥 from tl_r_reader where serialnumber between '{0}' and '{1}'",StartReaderNo,EndReaderNo);
        context.DBOpen("Select");
        DataTable Data = context.ExecuteReader(QryData);
        context.DBCommit();
        return Data;
    }

    /// <summary>
    /// 获取充付器起始序号
    /// </summary>
    /// <returns></returns>
    private string GetReaderNo()
    {
        string Year=txtYear.Text;
        string Manu=ddlManu.SelectedValue;
        string ReaderNo = "000000";
        string QryNo = string.Format("select substr(t.serialnumber,11,6) no from tl_r_reader t where substr(t.serialnumber,7,2)='{0}' and t.manucode='{1}' order by no desc", Year, Manu);
        context.DBOpen("Select");
        DataTable ReaderNoData = context.ExecuteReader(QryNo);
        if (ReaderNoData != null && ReaderNoData.Rows.Count > 0)
            ReaderNo = ReaderNoData.Rows[0]["no"].ToString();
        return ReaderNo;
    }

    /// <summary>
    /// 输入条件验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateInput()
    {
        string ReaderManu = ddlManu.SelectedValue;
        string ReaderYear = txtYear.Text;
        string ReaderBatchNo = txtBatchNo.Text;
        string ReaderNumber = txtNumber.Text;
        string ReaderPlatFormType = ddlPlatform.SelectedValue;
        //string ReaderNo = txtNo.Text;
        
        //厂商验证
        if (string.IsNullOrEmpty(ReaderManu))
        {
            context.AddError("充付器厂商必选",ddlManu);
        }

        //年份验证
        if (string.IsNullOrEmpty(ReaderYear))
        {
            context.AddError("年份不能为空", txtYear);
        }
        else if (ReaderYear.Length != 2)
        {
            context.AddError("年份必须是两位",txtYear);
        }
        else if (!IsNum(ReaderYear))
        {
            context.AddError("年份必须是数字",txtYear);
        }

        //批次号验证
        if (string.IsNullOrEmpty(ReaderBatchNo))
        {
            context.AddError("批次号不能为空",txtBatchNo);
        }
        else if (ReaderBatchNo.Length != 2)
        {
            context.AddError("批次号必须是两位",txtBatchNo);
        }
        else if (!IsNum(ReaderBatchNo))
        {
            context.AddError("批次号必须是数字",txtBatchNo);
        }
        else if (Convert.ToInt32(ReaderBatchNo) == 0)
        {
            context.AddError("批次号从01开始", txtBatchNo);
        }

        //数量验证
        if (string.IsNullOrEmpty(ReaderNumber))
        {
            context.AddError("充付器数量不能为空",txtNumber);
        }
        else if (!IsNum(ReaderNumber))
        {
            context.AddError("充付器数量必须是数字",txtNumber);
        }
        else if (Convert.ToInt32(ReaderNumber) <= 0)
        {
            context.AddError("充付器数量必须大于0", txtNumber);
        }

        //平台类型
        if (string.IsNullOrEmpty(ReaderPlatFormType))
        {
            context.AddError("平台类型不能为空",ddlPlatform);
        }

        //序号验证
       /* if (string.IsNullOrEmpty(ReaderNo))
        {
            context.AddError("充付器序号不能为空",txtNo);
        }
        else if (ReaderNo.Length > 6)
        {
            context.AddError("充付器序号长度不能超过6位", txtNo);
        }
        else if (!IsNum(ReaderNo))
        {
            context.AddError("充付器序号必须是数字",txtNo);
        }*/

        if (context.hasError())
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// 判断字符串是否为数字，是返回true
    /// </summary>
    /// <param name="text">输入的字符串</param>
    /// <returns>都是数字:true,否则false</returns>
    private bool IsNum(string text)
    {
        for (int i = 0; i < text.Length; i++)
        {
            if (!Char.IsDigit(text, i))
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// 根据充付器序列号散列出32位密钥
    /// </summary>
    /// <param name="readerno">充付器序列号</param>
    /// <returns>32位密钥</returns>
    private string GetKey(string readerno)
    {
        string Key = string.Empty;
        MD5 KeyMd5 = MD5.Create();//实例化一个md5对像
        // 加密后是一个字节类型的数组
        byte[] KeyByte = KeyMd5.ComputeHash(Encoding.UTF8.GetBytes(readerno));
        // 通过使用循环，将字节类型的数组转换为字符串
        for (int i = 0; i < KeyByte.Length; i++)
        {
            // 将得到的字符串使用十六进制类型格式
            Key = Key + KeyByte[i].ToString("X2");
        }
        return Key;
    }

    /// <summary>
    /// 清空输入
    /// </summary>
    private void ClearInput()
    {
        ddlManu.SelectedValue = "";
        txtBatchNo.Text = "";
        txtNumber.Text = "";
        ddlPlatform.SelectedValue = "";
        string Year = System.DateTime.Now.Year.ToString();
        txtYear.Text = Year.Substring(2, 2);
        txtBatchNo.Text = GetMaxBatchNo();
    }

    /// <summary>
    /// 根据条件清空临时表
    /// </summary>
    /// <param name="key">条件</param>
    private void ClearTempTable(string key)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery(string.Format("delete from TMP_COMMON where f0='{0}'", key));
        context.DBCommit();
    }

    /// <summary>
    /// 返回系统中最大的批次号
    /// </summary>
    /// <returns></returns>
    private string GetMaxBatchNo()
    {
        string BatchSql = string.Format("select max(batchno) batchno from tl_r_reader where year='{0}'", System.DateTime.Now.Year.ToString().Substring(2, 2));
        context.DBOpen("Select");
        DataTable MaxBatcnNoData = context.ExecuteReader(BatchSql);
        context.DBCommit();
        if (null == MaxBatcnNoData || MaxBatcnNoData.Rows.Count == 0)
            return "01";
        if (string.IsNullOrEmpty(MaxBatcnNoData.Rows[0]["batchno"].ToString()))
            return "01";

        int NowBatch = Convert.ToInt32(MaxBatcnNoData.Rows[0]["batchno"])+1;
        if (NowBatch >= 100)
        {
            context.AddMessage("充付器批次号已经最大,当前库存中最大批次是" + MaxBatcnNoData.Rows[0]["batchno"].ToString());
            return MaxBatcnNoData.Rows[0]["batchno"].ToString();
        }
        return new string('0',2-NowBatch.ToString().Length)+NowBatch.ToString();
    }

    /// <summary>
    /// 当前批次号系统中是否已经存在
    /// </summary>
    /// <param name="BatchNo"></param>
    /// <returns></returns>
    private bool IsExistBatchNo(string BatchNo)
    {
        string BatchSql = string.Format("select * from tl_r_reader where batchno='{0}'",BatchNo);
        context.DBOpen("Select");
        DataTable BatchData = context.ExecuteReader(BatchSql);
        context.DBCommit();
        if (null == BatchData || BatchData.Rows.Count == 0)
            return false;
        return true;
    }

    #endregion
}