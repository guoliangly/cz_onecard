﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EM_ReaderStockOutDepartChange.aspx.cs" Inherits="ASP_EquipmentManagement_EM_ReaderStockOutDepartChange" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充付器取消出库/调换</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/mootools.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <script type="text/javascript" src="../../js/checkall.js"></script>
    <script type="text/javascript">
        function deplist() {
            var ddlid = document.getElementById("ddlDepart");
            var radout = document.getElementById("radSockOutRoll");
            var radchange = document.getElementById("radChange");
            if (radout.checked == true) {
                ddlid.disabled = true;
            }
            if (radchange.checked == true) {
                ddlid.disabled = false;
            }
        }
    </script>
    <script type="text/javascript">
        function submitConfirm() {
            if (true) {
                var selDept = "";
                var obj = document.getElementById("ddlDepart");
                var radout = document.getElementById("radSockOutRoll");
                var radchange = document.getElementById("radChange");
                if (obj.value != "" && radchange.checked == true) {
                    for (i = 0; i < obj.length; i++) {
                        if (obj.options[i].selected) {
                            selDept = obj.options[i].text + "";
                            break;
                        }
                    }
                }
                //获取选中数量
                /*var gv = document.getElementById("gvResult");
                var chk = document.getElementsByName("chRow");
                var i = 0;
                for (var i = 0; i < chk.length; i++) {
                    if (chk[i].type == 'checkbox') {
                        if (chk[i].checked)
                            i++;
                    }
                }*/

                var num = document.getElementById("hidNum").value;
                var operatetype = "";
                operatetype = (radout.checked == true) ? "取消出库" : "调换";
                if (radout.checked == true) {
                    MyExtConfirm('确认',
                    '操作类型:' + operatetype + '<br>' +
                    '充付器数量:' + num + '<br>' +
                    '是否确认?', submitConfirmCallback);
                }
                else {
                    MyExtConfirm('确认',
                    '操作类型:' + operatetype + '<br>' +
                    '调换部门:' + selDept + '<br>' +
                    '充付器数量:' + num + '<br>' +
                    '是否确认?', submitConfirmCallback);
                }
            }
            return false;
        }
        function submitConfirmCallback(btn) {
            if (btn == 'yes') {
                $get('btnConfirm').click();
            }
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
            充付器模块 -> 取消出库/调换</div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
             <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script> 

                <div class="con">
                    <div class="base">
                    </div>
                    <div class="kuang5">
                        <table class="text25" cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 8%" align="right">
                                        操作类型:
                                    </td>
                                    <td style="width: 12%"  align="left">
                                   <input type="radio" id="radSockOutRoll" name="operateType" checked="true" runat="server" value="09" onclick="deplist()"/>
                                        <label for="radSockOutRoll">取消出库</label>
                                   <input type="radio" id="radChange" name="operateType" style="margin-left:10px" runat="server" value="08" onclick="deplist()"/>
                                        <label for="radChange">调换</label>
                                    </td>
                                   <td style="width: 8%" align="right">
                                        起讫序列号:
                                    </td>
                                    <td style="width: 30%"  align="left">
                                     <asp:TextBox  runat="server" ID="txtBeginNo" CssClass="inputmid"></asp:TextBox>
                                      -
                                    <asp:TextBox runat="server" ID="txtEndNo" CssClass="inputmid"></asp:TextBox>
                                    </td>
                                    <td style="width: 8%" align="right">
                                       出库日期:
                                    </td>
                                    <td style="width: 12%"  align="left">
                                    <ajaxToolkit:CalendarExtender ID="BeginCalendar" runat="server" TargetControlID="txtStockOutDate"
                                    Format="yyyyMMdd" PopupPosition="BottomLeft" />
                                    <asp:TextBox runat="server" CssClass="input" ID="txtStockOutDate"></asp:TextBox>
                                    </td>
                                <td style="width: 8%" align="right">
                                       领用部门:
                                </td>
                                <td style="width: 12%" align="left">
                                <asp:DropDownList runat="server" ID="ddlStockOutDepart" CssClass="input"></asp:DropDownList>
                                </td> 
                                </tr>
                                <tr>
                                    <td style="width: 8%" align="right">
                                    </td>
                                    <td style="width: 12%" align="right">
                                        &nbsp;</td>
                                    <td style="width: 8%" align="right">
                                    </td>
                                    <td style="width: 12%" align="right">
                                        &nbsp;</td>
                                    <td style="width: 8%" align="right">
                                    </td>
                                    <td style="width: 12%" align="right">
                                        &nbsp;</td>
                                   <td style="width: 8%" align="right">
                                    </td>
                                    <td style="width: 15%">
                                        <asp:Button ID="btnQuery" runat="server" CssClass="button1"
                                            Text="查询" OnClick="btnQuery_Click"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 350px">
                            <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="False" Width="98%"
                                CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" EmptyDataText="没有数据记录!" AllowPaging="True" PageSize="20"
                                 AutoGenerateColumns="false" DataKeyNames="serialnumber"
                                OnPageIndexChanging="gvResult_Page">
                                <PagerSettings Mode="NumericFirstLast" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input type="checkbox" id="chAll" onclick="CheckAll(this, 'gvResult')"/>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <input type="checkbox" id="chRow" onclick="ItemCheck(this, 'gvResult', 'chAll')" name="chRow" runat="server"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>序号</HeaderTemplate>
                                    <ItemTemplate>
                                    <%#Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="序列号" DataField="serialnumber" />
                                <asp:BoundField HeaderText="类型" DataField="typename" />
                                <asp:BoundField HeaderText="厂商" DataField="manuname" />
                                <asp:BoundField HeaderText="所属部门" DataField="departname" />
                                <asp:BoundField HeaderText="状态" DataField="state" />
                                <asp:BoundField HeaderText="操作时间" DataField="operatetime" />
                            </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div class="pip">
                        调换部门</div>
                    <div class="kuang5">
                        <table width="98%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                        <td style="width: 8%" align="right">
                          部门:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDepart" CssClass="input" Enabled="false"></asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                         </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <input type="hidden" runat="server" id="hidNum" />
                                <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click"/>
                                <asp:Button ID="btnSubmit" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click"
                                 />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
