﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using System.Text;

/****************************************
 * create:chenwentao 2014-11-07
 * content:充付器类型维护
 * *************************************/
public partial class ASP_EquipmentManagement_EM_ReaderType : Master.Master
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGird();
        }
    }

    private void BindGird()
    {
        string StrSql = string.Format("select * from TF_F_ReaderType order by typeid");
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(StrSql);
        lvwReaderType.DataSource = dt;
        lvwReaderType.DataBind();
    }

    #endregion

    #region Event

    protected void lvwReaderType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwReaderType','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void lvwReaderType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCode.Text = GetDataKeys("Code");
        txtName.Text = GetDataKeys("Name");
        txtNote.Text = GetDataKeys("DESCRIPTION");
    }

    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (!ValidateInput())
            return;

        //string DataGram = GetDatagram(ReaderHelper.ReaderTypeOperate.Add,GetTypeId());
        //if (Encoding.Default.GetByteCount(DataGram) != 280)
        //{
        //    context.AddError("同步报文长度有误，不能操作");
        //    return;
        //}

        context.SPOpen();
        context.AddField("p_functioncode").Value="add";
        context.AddField("p_typeid").Value = 0;
        context.AddField("p_code").Value = txtCode.Text.Trim();
        context.AddField("p_name").Value = txtName.Text.Trim();
        context.AddField("p_description").Value = txtNote.Text.Trim();
        bool ok = context.ExecuteSP("SP_EM_ReaderType");
        if (ok)
        {
            //context.DBCommit();
            //bool ISSuc = false;
            //string RetMsg=ReaderHelper.ReaderDataSYN(DataGram,ref ISSuc);
            //if (ISSuc)
            //{
            //    context.AddMessage("新增充付器类型成功");
            //    context.AddMessage(RetMsg);
            //}
            //else
            //{
            //    context.AddMessage("新增充付器类型成功");
            //    context.AddError(RetMsg);
            //}
            context.AddMessage("新增充付器类型成功");
            ClearInput();
            BindGird();
        }
    }

    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModify_Click(object sender, EventArgs e)
    {
        if (lvwReaderType.SelectedIndex == -1)
        {
            context.AddError("未选择记录,不能更新");
            return;
        }
        if (!ValidateInput())
            return;

        context.SPOpen();
        context.AddField("p_functioncode").Value = "update";
        context.AddField("p_typeid").Value = GetDataKeys("TypeId");
        context.AddField("p_code").Value = txtCode.Text.Trim();
        context.AddField("p_name").Value = txtName.Text.Trim();
        context.AddField("p_description").Value = txtNote.Text.Trim();
        bool ok = context.ExecuteSP("SP_EM_ReaderType");
        if (ok)
        {
            context.DBCommit();
            context.AddMessage("更新充付器类型成功");
            ClearInput();
            BindGird();
        }
    }

    /// <summary>
    /// 删除
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (lvwReaderType.SelectedIndex == -1)
        {
            context.AddError("未选择记录,不能删除");
            return;
        }

        if (!IsCanDelete())
            return;

        //string DataGram = GetDatagram(ReaderHelper.ReaderTypeOperate.Delete, GetDataKeys("TypeId"));
        //if (Encoding.Default.GetByteCount(DataGram) != 280)
        //{
        //    context.AddError("同步报文长度有误，不能操作");
        //    return;
        //}

        context.SPOpen();
        context.AddField("p_functioncode").Value = "delete";
        context.AddField("p_typeid").Value = GetDataKeys("TypeId");
        context.AddField("p_code").Value = "";
        context.AddField("p_name").Value = "";
        context.AddField("p_description").Value = "";
        bool ok = context.ExecuteSP("SP_EM_ReaderType");
        if (ok)
        {
            context.DBCommit();
            //bool ISSuc = false;
            //string RetMsg = ReaderHelper.ReaderDataSYN(DataGram, ref ISSuc);
            //if (ISSuc)
            //{
            //    context.AddMessage("删除充付器类型成功");
            //    context.AddMessage(RetMsg);
            //}
            //else
            //{
            //    context.AddMessage("删除充付器类型成功");
            //    context.AddError(RetMsg);
            //}
            context.AddMessage("删除充付器类型成功");
            ClearInput();
            BindGird();
        }
    }

    #endregion

    #region Function

    /// <summary>
    /// 获取datakeynames对应的值
    /// </summary>
    /// <param name="keysname"></param>
    /// <returns></returns>
    private string GetDataKeys(string keysname)
    {
        return lvwReaderType.DataKeys[lvwReaderType.SelectedIndex][keysname].ToString();
    }

    /// <summary>
    /// 判断充付器类型是否能删除
    /// </summary>
    /// <returns></returns>
    private bool IsCanDelete()
    {
        string TypeId = GetDataKeys("TypeId");
        string strSql = string.Format("select * from tl_r_reader where typeid='{0}'",TypeId);
        context.DBOpen("Select");
        DataTable data = context.ExecuteReader(strSql);
        context.DBCommit();
        if (null == data || data.Rows.Count == 0)
        {
            return true;
        }
        context.AddError("该类型已有充付器使用不能删除");
        return false;
    }

    /// <summary>
    /// 提交验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateInput()
    {
        string strName = txtName.Text.Trim();
        if (strName == "")
        {
            context.AddError("充付器类型名称不能为空", txtName);
        }
        else if (Validation.strLen(strName) > 30)
        {
            context.AddError("充付器类型名称太长", txtName);
        }

        string strNote = txtNote.Text.Trim();
        if (strNote != "" && Validation.strLen(strNote) > 150)
        {
            context.AddError("A006113006", txtNote);
        }

        string strCode = txtCode.Text.Trim();
        if (strCode != "" && Validation.strLen(strCode)!=4)
        {
            context.AddError("类型编码必须是4位", txtCode);
        }

        if (context.hasError())
            return false;
        return true;
    }

    /// <summary>
    /// 清空页面控件
    /// </summary>
    private void ClearInput()
    {
        txtCode.Text = "";
        txtName.Text = "";
        txtNote.Text = "";
    }

    /// <summary>
    /// 返回同步操作所需的报文
    /// </summary>
    /// <returns></returns>
    private string GetDatagram(ReaderHelper.ReaderTypeOperate operate,string typeid)
    {
        StringBuilder Datagram = new StringBuilder(); ;
        int Length = 280;
        string MethondName = "CARDREADERTYPESYNC";
        string TypeId = typeid;
        string SyncType = ReaderHelper.GetReaderTypeOperateCode(operate);
        string Code = txtCode.Text.Trim();
        string Name = txtName.Text.Trim();
        string Description=txtNote.Text.Trim();

        //拼接报文
        Datagram.Append(new string('0',6-Length.ToString().Length));
        Datagram.Append(Length.ToString());
        Datagram.Append(new string(' ',32-MethondName.Length));
        Datagram.Append(MethondName);
        Datagram.Append(new string('0',4-TypeId.Length));
        Datagram.Append(TypeId);
        Datagram.Append(SyncType);
        int l = Encoding.Default.GetByteCount(Name);
        Datagram.Append(new string(' ', 64 - Encoding.Default.GetByteCount(Name)));
        Datagram.Append(Name);
        Datagram.Append(new string(' ', 8 - Code.Length));
        Datagram.Append(Code);
        Datagram.Append(new string(' ',64));
        Datagram.Append(new string(' ', 100 - Encoding.Default.GetByteCount(Description)));
        Datagram.Append(Description);

        return Datagram.ToString();
    }

    /// <summary>
    /// 获取类型Id
    /// </summary>
    /// <returns></returns>
    private string GetTypeId()
    {
        string TypeId = "";
        string QryId = string.Format("select nvl(max(typeid),0)+1 typeid from tf_f_readertype");
        context.DBOpen("Select");
        DataTable IdData = context.ExecuteReader(QryId);
        context.DBCommit();
        TypeId = IdData.Rows[0]["typeid"].ToString();

        return TypeId;
    }

    #endregion

}