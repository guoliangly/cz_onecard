﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using TM;
using TM.EquipmentManagement;
using Common;
using TDO.CardManager;

public partial class ASP_EquipmentManagement_EM_CardSurface : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化有效标志
            TSHelper.initUseTag(selUseTag);

            //初始化卡面类型列表            CardSurfaceQuery();
        }
    }

    //查询卡面类型并初始化列表
    private void CardSurfaceQuery()
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CARDSURFACETDO tdoCardSurface = new TD_M_CARDSURFACETDO();

        DataTable data = tmTMTableModule.selByPKDataTable(context, tdoCardSurface, null, "", 0);
        DataView dataView = new DataView(data);

        lvwCardSurface.DataKeyNames = new string[] { "CARDSURFACECODE", "CARDSURFACENAME", "CARDSURFACENOTE", "USETAG" };
        lvwCardSurface.DataSource = dataView;
        lvwCardSurface.DataBind();

    }

    //增加
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (!ValidateInputData())
            return;

        //判断卡面类型编码是否已存在
        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACETDOIn = new TD_M_CARDSURFACETDO();
        TD_M_CARDSURFACETM tmTD_M_CARDSURFACETM = new TD_M_CARDSURFACETM();
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACECODE = txtCode.Text.Trim();

        if (!tmTD_M_CARDSURFACETM.chkCardSurface(context, ddoTD_M_CARDSURFACETDOIn))
        {
            context.AddError("A006114010", txtCode);
            return;
        }

        //插入新记录
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACENAME = txtName.Text.Trim();
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACENOTE = txtNote.Text.Trim();
        ddoTD_M_CARDSURFACETDOIn.USETAG = selUseTag.SelectedValue;
        ddoTD_M_CARDSURFACETDOIn.UPDATESTAFFNO = context.s_UserID;
        ddoTD_M_CARDSURFACETDOIn.UPDATETIME = DateTime.Now;

        int AddSum = tmTD_M_CARDSURFACETM.insAdd(context, ddoTD_M_CARDSURFACETDOIn);
        if(AddSum == 0)
            context.AppException("S006114011");

        context.DBCommit();

        CardSurfaceQuery();
        lvwCardSurface.SelectedIndex = -1;

        clearInput();
    }

    //修改
    protected void btnModify_Click(object sender, EventArgs e)
    {
        //当没有选择要修改的卡面类型信息时
        if (lvwCardSurface.SelectedIndex == -1)
        {
            context.AddError("A006114020");
            return;
        }

        //当卡面类型编码被修改时
        if (txtCode.Text.Trim() != getDataKeys("CARDSURFACECODE"))
        {
            context.AddError("A006114021", txtCode);
            return;
        }

        if (!ValidateInputData())
            return;

        //更新记录
        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACETDOIn = new TD_M_CARDSURFACETDO();
        TD_M_CARDSURFACETM tmTD_M_CARDSURFACETM = new TD_M_CARDSURFACETM();
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACECODE = txtCode.Text.Trim();
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACENAME = txtName.Text.Trim();
        ddoTD_M_CARDSURFACETDOIn.CARDSURFACENOTE = txtNote.Text.Trim();
        ddoTD_M_CARDSURFACETDOIn.USETAG = selUseTag.SelectedValue;
        ddoTD_M_CARDSURFACETDOIn.UPDATESTAFFNO = context.s_UserID;
        ddoTD_M_CARDSURFACETDOIn.UPDATETIME = DateTime.Now;

        int UpdSum = tmTD_M_CARDSURFACETM.updRecord(context, ddoTD_M_CARDSURFACETDOIn);
        if(UpdSum == 0)
            context.AppException("S006114013");

        context.DBCommit();

        CardSurfaceQuery();
        //clearInput();

    }
    public void lvwCardSurface_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCode.Text = getDataKeys("CARDSURFACECODE");
        txtName.Text = getDataKeys("CARDSURFACENAME");
        txtNote.Text = getDataKeys("CARDSURFACENOTE");
        selUseTag.SelectedValue = getDataKeys("USETAG");
    }
    protected void lvwCardSurface_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwCardSurface','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void lvwCardSurface_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwCardSurface.PageIndex = e.NewPageIndex;
        CardSurfaceQuery();

        lvwCardSurface.SelectedIndex = -1;
        clearInput();
    }

    private string getDataKeys(string keysname)
    {
        return lvwCardSurface.DataKeys[lvwCardSurface.SelectedIndex][keysname].ToString();
    }

    private void clearInput()
    {
        txtCode.Text = "";
        txtName.Text = "";
        txtNote.Text = "";
        selUseTag.SelectedValue = "";
    }

    //输入判断处理
    private bool ValidateInputData()
    {
        //卡面编码非空、数字、长度判断
        string strCode = txtCode.Text.Trim();
        if (strCode == "")
        {
            context.AddError("A006114001", txtCode);
        }
        else
        {
            if (!Validation.isNum(strCode))
            {
                context.AddError("A006114002", txtCode);
            }
            if (Validation.strLen(strCode) != 4)
            {
                context.AddError("A006114003", txtCode);
            }

        }

        //卡面编码名称非空、长度判断
        string strName = txtName.Text.Trim();
        if (strName == "")
        {
            context.AddError("A006114004", txtName);
        }
        else if (Validation.strLen(strName) > 40)
        {
            context.AddError("A006114005", txtName);
        }

        //卡面编码说明非空时，长度判断
        string strNote = txtNote.Text.Trim();
        if (strNote != "")
        {
            if (Validation.strLen(strNote) > 150)
            {
                context.AddError("A006114006", txtNote);
            }
        }

        //有效标志非空判断
        string strUseTag = selUseTag.SelectedValue;
        if (strUseTag == "")
        {
            context.AddError("A006114007", selUseTag);
        }

        if (context.hasError())
            return false;
        else
            return true;

    }
}
