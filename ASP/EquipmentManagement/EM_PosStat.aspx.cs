﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.UserCard;
using Master;

public partial class ASP_EquipmentManagement_EM_PosStat : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StateSubmit();
        }
    }

    private void StateSubmit()
    {
        total = 0;
        SP_EM_QueryPDO pdo = new SP_EM_QueryPDO();
        pdo.funcCode = "POSSTAT";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        gvResult.DataSource = data;
        gvResult.DataBind();
    }
    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 5);
    }

    private int total = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            total += Convert.ToInt32(e.Row.Cells[6].Text);
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].ColumnSpan = 6;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[6].Text = "" + total;
        }
    }

    protected void gvResult_DataBound(object sender, EventArgs e)
    {
        GridViewMergeHelper.DataBoundWithEmptyRows(gvResult);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportGridView(gvResult);
    }


    protected void btnQuery_Click(object sender, EventArgs e)
    {
        StateSubmit();
    }
}
