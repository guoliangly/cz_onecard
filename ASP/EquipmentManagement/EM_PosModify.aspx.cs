﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using TM;
using TDO.ResourceManager;
using TDO.BalanceChannel;
using Common;
using PDO.EquipmentManagement;

public partial class ASP_EquipmentManagement_EM_PosModify : Master.Master
{
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化行业
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), null);

            ControlDeal.SelectBoxFill(selCalling1.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

            // 初始化状态
            TD_M_RESOURCESTATETDO tdoTD_M_RESOURCESTATETDOIn = new TD_M_RESOURCESTATETDO();
            TD_M_RESOURCESTATETDO[] tdoTD_M_RESOURCESTATETDOOutArr = (TD_M_RESOURCESTATETDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_RESOURCESTATETDOIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATETDO", null);
            ControlDeal.SelectBoxFill(selPosState.Items, tdoTD_M_RESOURCESTATETDOOutArr, "RESSTATE", "RESSTATECODE", true);

            //初始化POS来源
            EMHelper.setSource(selSource);

            //POS库存查询表头
            initPosList();


            /*  POS入库初始化  */

            //初始化POS来源  
            EMHelper.setSource(selEquipSource);

            //初始化POS厂商
            TD_M_POSMANUTDO ddoTD_M_POSMANUTDOIn = new TD_M_POSMANUTDO();
            TD_M_POSMANUTDO[] tdoTD_M_POSMANUTDOOutArr = (TD_M_POSMANUTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_POSMANUTDOIn, typeof(TD_M_POSMANUTDO), null, "TD_M_POSMANU", null);
            ControlDeal.SelectBoxFillWithCode(selEquipManu.Items, tdoTD_M_POSMANUTDOOutArr,
                "POSMANUNAME", "POSMANUCODE", true);

            //初始化POS型号
            TD_M_POSMODETDO ddoTD_M_POSMODETDOIn = new TD_M_POSMODETDO();
            TD_M_POSMODETDO[] tdoTD_M_POSMODETDOOutArr = (TD_M_POSMODETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_POSMODETDOIn, typeof(TD_M_POSMODETDO), null, "TD_M_POSMODE", null);
            ControlDeal.SelectBoxFillWithCode(selEquipMode.Items, tdoTD_M_POSMODETDOOutArr,
                "POSMODE", "POSMODECODE", true);

            //初始化接触类型

            TD_M_POSTOUCHTYPETDO ddoTD_M_POSTOUCHTYPETDOIn = new TD_M_POSTOUCHTYPETDO();
            TD_M_POSTOUCHTYPETDO[] tdoTD_M_POSTOUCHTYPETDOOutArr = (TD_M_POSTOUCHTYPETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_POSTOUCHTYPETDOIn, typeof(TD_M_POSTOUCHTYPETDO), null, "TD_M_POSTOUCHTYPE", null);
            ControlDeal.SelectBoxFillWithCode(selTouchType.Items, tdoTD_M_POSTOUCHTYPETDOOutArr,
                "TOUCHTYPE", "TOUCHTYPECODE", true);

            //初始化放置类型

            TD_M_POSLAYTYPETDO ddoTD_M_POSLAYTYPETDOIn = new TD_M_POSLAYTYPETDO();
            TD_M_POSLAYTYPETDO[] tdoTD_M_POSLAYTYPETDOOutArr = (TD_M_POSLAYTYPETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_POSLAYTYPETDOIn, typeof(TD_M_POSLAYTYPETDO), null, "TD_M_POSLAYTYPE", null);
            ControlDeal.SelectBoxFillWithCode(selLayType.Items, tdoTD_M_POSLAYTYPETDOOutArr,
                "LAYTYPE", "LAYTYPECODE", true);

            //初始化通信类型
            TD_M_POSCOMMTYPETDO ddoTD_M_POSCOMMTYPETDOIn = new TD_M_POSCOMMTYPETDO();
            TD_M_POSCOMMTYPETDO[] tdoTD_M_POSCOMMTYPETDOOutArr = (TD_M_POSCOMMTYPETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_POSCOMMTYPETDOIn, typeof(TD_M_POSCOMMTYPETDO), null, "TD_M_POSCOMMTYPE", null);
            ControlDeal.SelectBoxFillWithCode(selCommType.Items, tdoTD_M_POSCOMMTYPETDOOutArr,
                "COMMTYPE", "COMMTYPECODE", true);
        }  
    }

    //表头
    private void initPosList()
    {
        DataTable posData = new DataTable();
        DataView posDataView = new DataView(posData);

        lvwPos.DataKeyNames = new string[] { "POSNO", "POSSOURCE", "POSMODE", "TOUCHTYPE", "LAYTYPE", "COMMTYPE", "EQUPRICE", "HARDWARENUM", "RESSTATE", "CALLING", "CORP", "DEPT", "INSTIME", "OUTTIME", "REINTIME", "DESTROYTIME", "ASSIGNEDSTAFF" };
        lvwPos.DataSource = posDataView;
        lvwPos.DataBind();
    }
    
    //选择行业时，初始化单位和部门
    protected void selCalling1_SelectedIndexChanged(object sender, EventArgs e)
    {
        selDept1.Items.Clear();
        if (selCalling1.SelectedValue == "")
        {
            selCorp1.Items.Clear();
            return;
        }
        InitCorp(selCalling1, selCorp1, "TD_M_CORPCALLUSAGE");
    }

    //选择单位时，初始化部门
    protected void selCorp1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selCorp1.SelectedValue == "")
        {
            selDept1.Items.Clear();
            return;
        }
        InitDepart(selCorp1, selDept1, "TD_M_DEPARTUSAGE");
    }

    //初始化单位
    protected void InitCorp(DropDownList father, DropDownList target, String sqlCondition)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = father.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFill(target.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }

    //初始化部门
    private void InitDepart(DropDownList corp, DropDownList dept, String sqlCondition)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从部门编码表(TD_M_CDEPART)中读取数据，放入下拉列表中

        TD_M_DEPARTTDO tdoTD_M_DEPARTIn = new TD_M_DEPARTTDO();
        tdoTD_M_DEPARTIn.CORPNO = corp.SelectedValue;

        TD_M_DEPARTTDO[] tdoTD_M_DEPARTOutArr = (TD_M_DEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_DEPARTIn, typeof(TD_M_DEPARTTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFill(dept.Items, tdoTD_M_DEPARTOutArr, "DEPART", "DEPARTNO", true);

    }

    protected void lvwPos_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwPos','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void lvwPos_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwPos.PageIndex = e.NewPageIndex;
        btnPosQuery_Click(sender, e);
    }

    protected void btnPosQuery_Click(object sender, EventArgs e)
    {
        //输入判断
        if (!ValidateForPosQuery())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();
        TL_R_EQUATDO ddoTL_R_EQUATDOIn = new TL_R_EQUATDO();

        string strSql = "SELECT p.POSNO POSNO,p.EQUSOURCE POSSOURCE,m.POSMODE POSMODE,t.TOUCHTYPE TOUCHTYPE,l.LAYTYPE LAYTYPE,c.COMMTYPE COMMTYPE,p.EQUPRICE EQUPRICE,p.POSHARDWARENUM HARDWARENUM," +
            "s.RESSTATE RESSTATE,h.CALLING CALLING,a.CORP CORP,r.DEPART DEPT,p.INSTIME INSTIME,p.OUTTIME OUTTIME,p.REINTIME REINTIME,p.DESTROYTIME DESTROYTIME,e.STAFFNAME ASSIGNEDSTAFF " +
            "FROM TL_R_EQUA p left join TD_M_POSTOUCHTYPE t on p.TOUCHTYPECODE=t.TOUCHTYPECODE " +
            "left join TD_M_POSMODE m on p.POSMODECODE=m.POSMODECODE " +
            "left join TD_M_POSLAYTYPE l on p.LAYTYPECODE=l.LAYTYPECODE " +
            "left join TD_M_POSCOMMTYPE c on p.COMMTYPECODE=c.COMMTYPECODE " +
            "left join TD_M_RESOURCESTATE s on p.RESSTATECODE=s.RESSTATECODE " +
            "left join TD_M_CALLINGNO h on p.CALLINGNO=h.CALLINGNO " +
            "left join TD_M_CORP a on p.CORPNO=a.CORPNO " +
            "left join TD_M_DEPART r on p.DEPARTNO=r.DEPARTNO " +
            "left join TD_M_INSIDESTAFF e on p.ASSIGNEDSTAFFNO=e.STAFFNO ";

        ArrayList list = new ArrayList();
        if (txtPosNo.Text.Trim() != "")
            list.Add("p.POSNO like '"+ GetSearchString(txtPosNo.Text.Trim())+"'");

        if (selCalling1.SelectedValue != "")
            list.Add("p.CALLINGNO='" + selCalling1.SelectedValue + "'");

        if (selCorp1.SelectedValue != "")
            list.Add("p.CORPNO='" + selCorp1.SelectedValue + "'");

        if (selDept1.SelectedValue != "")
            list.Add("p.DEPARTNO='" + selDept1.SelectedValue + "'");

        if (selPosState.SelectedValue != "")
            list.Add("p.RESSTATECODE='" + selPosState.SelectedValue + "'");

        if (selSource.SelectedValue != "")
            list.Add("p.EQUSOURCE='" + selSource.SelectedValue + "'");

        list.Add("rownum<200");

        strSql += DealString.ListToWhereStr(list);

        DataTable data = tmTMTableModule.selByPKDataTable(context, ddoTL_R_EQUATDOIn, null, strSql, 0);
        DataView dataView = new DataView(data);

        lvwPos.DataSource = dataView;
        lvwPos.DataBind();
    }


    //输入验证
    private bool ValidateForPosQuery()
    {
        //POS编号必须为数字、6位
        string strPosNo = txtPosNo.Text.Trim();
        if (strPosNo != "")
        {
            if (!Validation.isNum(strPosNo))
                context.AddError("A006400001", txtPosNo);

            if (Validation.strLen(strPosNo) > 6)
                context.AddError("A006400002", txtPosNo);
        }

        if (context.hasError())
            return false;
        else
            return true;
    }

    protected void lvwPos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string s = e.Row.Cells[15].Text;
            if (Validation.isPrice(s))
                e.Row.Cells[15].Text = (Convert.ToDouble(s) / 100).ToString("0.00");
            else
                e.Row.Cells[15].Text = "";
            
        }
    }

    private string GetSearchString(string str)
    {
        return str.Trim() + "%";
    }

    public void lvwPos_SelectedIndexChanged(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        
        TL_R_EQUATDO ddoTL_R_EQUAIn = new TL_R_EQUATDO();
        ddoTL_R_EQUAIn.POSNO = getDataKeys("POSNO");

        TL_R_EQUATDO ddoTL_R_EQUAOut = (TL_R_EQUATDO)tmTMTableModule.selByPK(context, ddoTL_R_EQUAIn, typeof(TL_R_EQUATDO), null, "TL_R_EQUA", null);
        txtEquipNo.Text = getDataKeys("POSNO");
        selEquipMode.SelectedValue = ddoTL_R_EQUAOut.POSMODECODE;
        selTouchType.SelectedValue=ddoTL_R_EQUAOut.TOUCHTYPECODE;
        selLayType.SelectedValue=ddoTL_R_EQUAOut.LAYTYPECODE;
        selCommType.SelectedValue=ddoTL_R_EQUAOut.COMMTYPECODE;
        txtEquipPrice.Text=(ddoTL_R_EQUAOut.EQUPRICE/100).ToString("0.00");
        selEquipManu.SelectedValue=ddoTL_R_EQUAOut.POSMANUCODE;
        selEquipSource.SelectedValue=ddoTL_R_EQUAOut.EQUSOURCE;
        txtHardwareNum.Text=ddoTL_R_EQUAOut.POSHARDWARENUM;
    }
    public String getDataKeys(string keysname)
    {
        string value = lvwPos.DataKeys[lvwPos.SelectedIndex][keysname].ToString();

        return value == "" ? "" : value;
    }
    private bool ValidateForPos()
    {
        //POS编号非空、数字、长度判断


        string strEquipNo = txtEquipNo.Text.Trim();
        if (strEquipNo == "")
            context.AddError("A006001135", txtEquipNo);
        else
        {
            if (!Validation.isNum(strEquipNo))
                context.AddError("A006001136", txtEquipNo);
            if (Validation.strLen(strEquipNo) != 6)
                context.AddError("A006001137", txtEquipNo);
        }

        //POS来源非空
        string strPosSource = selEquipSource.SelectedValue;
        if (strPosSource == "")
            context.AddError("A006001020", selEquipSource);

        //POS厂商非空
        string strEquipManu = selEquipManu.SelectedValue;
        if (strEquipManu == "")
            context.AddError("A006001028", selEquipManu);

        //POS型号非空
        string strEquipMode = selEquipMode.SelectedValue;
        if (strEquipMode == "")
            context.AddError("A006001024", selEquipMode);

        //接触类型非空
        string strTouchType = selTouchType.SelectedValue;
        if (strTouchType == "")
            context.AddError("A006001025", selTouchType);

        //放置类型非空
        string strLayType = selLayType.SelectedValue;
        if (strLayType == "")
            context.AddError("A006001033", selLayType);

        //通信类型非空
        string strCommType = selCommType.SelectedValue;
        if (strCommType == "")
            context.AddError("A006001033", selCommType);

        //硬件序列号非空、英数、长度判断


        string strHardwareNum = txtHardwareNum.Text.Trim();
        if (strHardwareNum == "")
            context.AddError("A006001029", txtHardwareNum);
        else
        {
            if (!Validation.isCharNum(strHardwareNum))
                context.AddError("A006001030", txtHardwareNum);
            if (Validation.strLen(strHardwareNum) > 50)
                context.AddError("A006001031", txtHardwareNum);
        }

        //POS价格非空、数字判断


        string strEquipPrice = txtEquipPrice.Text.Trim();
        if (strEquipPrice == "")
            context.AddError("A006001082", txtEquipPrice);
        else
        {
            if (!Validation.isPrice(strEquipPrice))
                context.AddError("A006001078", txtEquipPrice);
        }

        if (context.hasError())
            return false;
        else
            return true;
    }
    //POS修改
    protected void btnPos_Click(object sender, EventArgs e)
    {
        //POS入库输入验证
        if (!ValidateForPos())
            return;

        //调用POS入库存储过程
        SP_EM_PosChangePDO pdo = new SP_EM_PosChangePDO();
        pdo.posNo = txtEquipNo.Text.Trim();
        pdo.posSort = "00"; //POS
        pdo.posModel = selEquipMode.SelectedValue;
        pdo.touchType = selTouchType.SelectedValue;
        pdo.layType = selLayType.SelectedValue;
        pdo.commType = selCommType.SelectedValue;
        pdo.posPrice = (int)(float.Parse(txtEquipPrice.Text.Trim()) * 100);
        pdo.posManu = selEquipManu.SelectedValue;
        pdo.posSource = selEquipSource.SelectedValue;
        pdo.hardwareNum = txtHardwareNum.Text.Trim();

        bool ok = TMStorePModule.Excute(context, pdo);
        if (ok)
        {
            AddMessage("M006001052");
            btnPosQuery_Click(sender, e);
        }
    }
}
