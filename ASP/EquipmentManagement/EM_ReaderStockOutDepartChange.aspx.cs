﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using System.Data;

public partial class ASP_EquipmentManagement_EM_ReaderStockOutDepartChange : Master.Master
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDL();
        }
        if (radChange.Checked)
            ddlDepart.Enabled = true;
    }

    /// <summary>
    /// dropdownlist初始化数据
    /// </summary>
    private void BindDDL()
    {
        UserCardHelper.selectDepts(context,ddlStockOutDepart,true);
        UserCardHelper.selectDepts(context,ddlDepart,true);
    }

    #endregion

    #region Event

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!ValidateQueryInput())
            return;
        string OperateType = "";
        if (radSockOutRoll.Checked)
            OperateType = radSockOutRoll.Value;
        if (radChange.Checked)
            OperateType =radChange.Value;

        DataTable ReaderData = SPHelper.callQuery("SP_EM_ReaderQuery", context, "QueryOperateReader", OperateType, txtBeginNo.Text.Trim(), txtEndNo.Text.Trim(), txtStockOutDate.Text.Trim(),ddlStockOutDepart.SelectedValue);

        gvResult.DataSource = ReaderData;
        gvResult.DataBind();
    }

    /// <summary>
    /// 提交按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!ValidateSubmitInput())
            return;
        int Num = 0;
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            System.Web.UI.HtmlControls.HtmlInputCheckBox cb = gvResult.Rows[i].Cells[0].FindControl("chRow") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (cb.Checked)
            {
                Num++;
            }
        }
        hidNum.Value = Num.ToString();

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Check", "submitConfirm();", true);
    }

    /// <summary>
    /// 确认事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        string SessionID = Session.SessionID;
        ClearTempTable(SessionID);
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            System.Web.UI.HtmlControls.HtmlInputCheckBox cb = gvResult.Rows[i].Cells[0].FindControl("chRow") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (cb.Checked)
            {
                string ReaderNo = GetDataKeyValue(gvResult, i);
                string InsertSql = string.Format("insert into TMP_COMMON(f0,f1) values('{0}','{1}')", SessionID, ReaderNo);
                context.DBOpen("Insert");
                context.ExecuteNonQuery(InsertSql);
            }
        }
        if (!context.hasError())
        {
            context.DBCommit();
        }
        else
        {
            context.RollBack();
        }

        //执行存储过程
        string OperateType = string.Empty;
        if (radSockOutRoll.Checked)
            OperateType = radSockOutRoll.Value;
        if (radChange.Checked)
            OperateType = radChange.Value;

        context.SPOpen();
        context.AddField("p_sessionid").Value = SessionID;
        context.AddField("p_operatetypecode").Value = OperateType;
        context.AddField("p_channgedepart").Value = ddlDepart.SelectedValue;
        bool ok = context.ExecuteSP("SP_EM_ReaderStockOutRollChange");
        if (ok)
        {
            context.DBCommit();
            context.AddMessage("提交成功");
            btnQuery_Click(sender, e);

            ddlDepart.SelectedValue = "";
        }
    }

    /// <summary>
    /// 分页
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_Page(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    #endregion

    #region Function

    /// <summary>
    /// 查询输入条件验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateQueryInput()
    {
        //序列号
        if (!string.IsNullOrEmpty(txtBeginNo.Text.Trim()))
        {
            if (!Validation.isNum(txtBeginNo.Text.Trim()))
            {
                context.AddError("序列号必须是数字", txtBeginNo);
            }
        }
        if (!string.IsNullOrEmpty(txtEndNo.Text.Trim()))
        {
            if (!Validation.isNum(txtEndNo.Text))
            {
                context.AddError("序列号必须是数字", txtEndNo);
            }
        }

        //日期
        ReaderHelper.CheckDate(context,txtStockOutDate,"日期格式不符合yyyyMMdd");

        if (context.hasError())
            return false;
        return true;
    }

    /// <summary>
    /// 是否有记录被选中
    /// </summary>
    /// <returns></returns>
    private bool IsChkRow()
    {
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            System.Web.UI.HtmlControls.HtmlInputCheckBox cb = gvResult.Rows[i].Cells[0].FindControl("chRow") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (cb.Checked)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 根据条件清空临时表
    /// </summary>
    /// <param name="key">条件</param>
    private void ClearTempTable(string key)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery(string.Format("delete from TMP_COMMON where f0='{0}'", key));
        context.DBCommit();
    }

    /// <summary>
    /// 返回指定行的datakeynames的值
    /// </summary>
    /// <param name="grid">gridview的id</param>
    /// <param name="index">行索引</param>
    /// <returns></returns>
    private string GetDataKeyValue(GridView grid, int index)
    {
        string ReaderNo = string.Empty;
        ReaderNo = grid.DataKeys[index]["serialnumber"].ToString();//序列号
        return ReaderNo;
    }

    /// <summary>
    /// 提交验证
    /// </summary>
    /// <returns></returns>
    private bool ValidateSubmitInput()
    {
        if (!IsChkRow())
        {
            context.AddError("未选择数据，不能提交！");
        }

        if (radChange.Checked)
        {
            if (string.IsNullOrEmpty(ddlDepart.SelectedValue))
            {
                context.AddError("调换部门必须选择",ddlDepart);
            }
        }

        if (context.hasError())
            return false;
        return true;
    }

    #endregion

}