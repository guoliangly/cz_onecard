﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EM_ReaderType.aspx.cs" Inherits="ASP_EquipmentManagement_EM_ReaderType" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <title>充付器类型维护</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
		    充付器模块->充付器类型维护
	</div>
     <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
    <!-- #include file="../../ErrorMsg.inc" -->
    <div class="con">
		        <div class="card">
			        充付器类型维护
		        </div>
        		
		        <div class="kuang5">
			        <div class="gdtb" style="height:280px">
			        
			            <asp:GridView ID="lvwReaderType" runat="server"
                                Width = "1000"
                                CssClass="tab1"
                                HeaderStyle-CssClass="tabbt"
                                AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel"
                                AllowPaging="false"
                                PagerSettings-Mode="NumericFirstLast"
                                PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="False"
                                 OnSelectedIndexChanged="lvwReaderType_SelectedIndexChanged"
                                DataKeyNames="TypeId,Code,Name,DESCRIPTION"
                                OnRowCreated="lvwReaderType_RowCreated">
                            <Columns>
                                <asp:BoundField DataField="TypeId"   HeaderText="类型ID"/>
                                <asp:BoundField DataField="Code" HeaderText="类型编码" />
                                <asp:BoundField DataField="Name"    HeaderText="类型名称"/>
                                <asp:BoundField DataField="DESCRIPTION"  HeaderText="类型说明"/>
                            </Columns>
                        </asp:GridView>

			        </div>
		        </div>
        	
		        <div class="kuang5">
			        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
				        <tr>
				            <td width="15%"><div align="right">类型编码:</div></td>
				            <td width="85%">
				                <asp:TextBox ID="txtCode" CssClass="input" MaxLength="4" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td><div align="right">类型名称:</div></td>
				            <td>
				                <asp:TextBox ID="txtName" CssClass="inputlong" MaxLength="20" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td><div align="right">类型说明:</div></td>
				            <td>
				                <asp:TextBox ID="txtNote" CssClass="inputlong" MaxLength="150" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
						        <table width="30%" border="0" align="right" cellpadding="0" cellspacing="0">
							        <tr>
						                <td>
						                    <asp:Button ID="btnAdd" CssClass="button1" Text="增加" runat="server" OnClick="btnAdd_Click"/>
						                </td>
						                <td>
						                    <asp:Button ID="btnModify" CssClass="button1" Text="修改" runat="server" OnClick="btnModify_Click"/>
						                </td>
                                        <td>
                                            <asp:Button ID="btnDelete" CssClass="button1" Text="删除" runat="server" OnClick="btnDelete_Click" OnClientClick="return confirm('确认删除？');" Visible="false"/>
                                        </td>
							        </tr>
						        </table>
					        </td>
				        </tr>
			        </table>
        			
		        </div>	
	        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
