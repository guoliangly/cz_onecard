﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EM_ReaderStockOutNew.aspx.cs" Inherits="ASP_EquipmentManagement_EM_ReaderStockOutNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>充付器出库</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/mootools.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <script type="text/javascript">
        function submitConfirm() {
            if (true) {
                var selDept = "";
                var obj = document.getElementById("ddlDept");
                if (obj.value != "") {
                    for (i = 0; i < obj.length; i++) {
                        if (obj.options[i].selected) {
                            selDept = obj.options[i].text + "";
                            break;
                        }
                    }
                }
                var totalValue = parseFloat($get('txtMoney').value) * parseFloat($get('txtCount').value);
                MyExtConfirm('确认',
                '型号:'+$get('hidType').value+'<br>'+
		        '起始序列号:' + $get('txtBeginNo').value + '<br>' +
                '结束序列号:' + $get('txtEndNo').value + '<br>' +
                '出库数量:'+$get('txtCount').value+'<br>'+
                '领用部门:' + selDept + '<br>' +
                '销售总金额:'+totalValue+'<br>'+
                '是否确认?'
		        , submitConfirmCallback);
            }
            return false;
        }
        function submitConfirmCallback(btn) {
            if (btn == 'yes') {
                $get('btnConfirm').click();
            }
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
            充付器模块 -> 出库</div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
             <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script> 

                <div class="con">
                    <div class="base">
                    </div>
                    <div class="kuang5">
                        <table class="text25" cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 10%; height: 25px" align="right">
                                        型号:
                                    </td>
                                    <td style="width: 20%; height: 25px" valign="middle">
                                     <asp:DropDownList runat=server ID="ddlReaderType" CssClass=input 
                                     ></asp:DropDownList>&nbsp;&nbsp;
                                    </td>
                                    <td style="width: 10%; height: 25px" align="right">
                                        起讫充付器序列号:
                                    </td>
                                    <td style="width: 40%; height: 25px" valign="middle">
                                        <asp:TextBox ID="txtQryBeginNo" CssClass="inputmid" runat="server" MaxLength="16"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtQryEndNo" CssClass="inputmid" runat="server" MaxLength="16"></asp:TextBox>
                                    </td>
                                    <td style="width: 5%; height: 25px" align="right">
                                        &nbsp;</td>
                                    <td style="width: 15%; height: 25px">
                                        <asp:Button ID="btnQuery" runat="server" CssClass="button1"
                                            Text="查询" OnClick="btnQuery_Click"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 300px">
                            <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="False" Width="98%"
                                CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" EmptyDataText="没有数据记录!" AllowPaging="True" PageSize="20"
                                OnPageIndexChanging="gvResult_Page">
                                <PagerSettings Mode="NumericFirstLast" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="pip">
                        充付器出库</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                        <td>
                            <div class="right">
                                型号:
                            </div>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlOutType" CssClass="input" AutoPostBack="true" OnSelectedIndexChanged="ddlOutType_SelectedIndexChanged" 
                                ></asp:DropDownList>
                        <input type="hidden" id="hidType" runat="server" />
                        </td>
                        <td>
                            <div class="right">
                                起始序列号:
                            </div>
                        </td>
                        <td style="width: 20%; height: 25px" valign="middle">
                            <input type="hidden" id="hidValue" runat="server" />
                                    <asp:TextBox ID="txtBeginNo" runat="server" CssClass="inputmid" MaxLength="16"
                                        AutoPostBack="true" OnTextChanged="txtBeginNo_TextChanged"></asp:TextBox>
                         </td>
                            <td>
                                <div align="right">
                                    出库数量:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCount" runat="server" CssClass="input" MaxLength="6" 
                                AutoPostBack="true" OnTextChanged="txtCount_TextChanged"></asp:TextBox>  
                            </td>
                            </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    领用部门:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDept" runat="server" CssClass="input">
                        </asp:DropDownList>
                            </td>
                            <td>
                                <div align="right">
                                    销售金额:
                                </div>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtMoney" CssClass="input"></asp:TextBox>
                                <span class="red">*100分</span>
                            </td>
                            <td><div align="right">结束序列号:</div></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtEndNo" CssClass="labeltext"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                             <div align="right">
                                    备注:</div>
                            </td>
                           <td colspan="3">
                               <asp:TextBox runat="server" ID="txtRemark" CssClass="inputmidder"></asp:TextBox>
                           </td>
                        </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click"/>
                                <asp:Button ID="btnStockOut" CssClass="button1" runat="server" Text="充付器出库" OnClick="btnStockOut_Click" 
                                 />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
