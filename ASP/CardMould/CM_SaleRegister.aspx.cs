﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;

public partial class ASP_CardMould_CM_SaleRegister : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //卡套入库判断
    private Boolean badCardbookinValidation()
    {
        //对卡套数量进行非空、数字校验

        if (txtStockOutNum.Text.Trim() == "")
        {
            context.AddError("A094780120：卡套数量不能为空", txtStockOutNum);
        }
        else
        {
            if (!Validation.isNum(txtStockOutNum.Text.Trim()))
            {
                context.AddError("A094780121：卡套数量必须为数字", txtStockOutNum);
            }
        }

        return !(context.hasError());
    }
    protected void btnStock_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        context.SPOpen();
        context.AddField("p_StockNum").Value = txtStockOutNum.Text.Trim();
        bool ok = context.ExecuteSP("SP_CM_SaleRegister");

        if (ok)
        {
            AddMessage("M094780987：卡套出售成功");

            txtStockOutNum.Text = "";
        }
    }
}
