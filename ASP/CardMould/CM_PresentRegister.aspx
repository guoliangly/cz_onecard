﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CM_PresentRegister.aspx.cs" Inherits="ASP_CardMould_CM_PresentRegister" %>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>赠送登记</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
<cr:CardReader id="cardReader" Runat="server"/> 
    <form id="form1" runat="server">
    <div class="tb">
卡套管理->赠送登记
</div>
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
<asp:BulletedList ID="bulMsgShow" runat="server">
</asp:BulletedList>
<script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>    
<div class="con">
<div class="card">卡套信息</div>
  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td width="11%"><div align="right">数量:</div></td>
    <td width="14%"><asp:TextBox ID="txtStockOutNum" CssClass="input" runat="server"></asp:TextBox></td>
    <td width="11%"><div align="right">客户姓名:</div></td>
    <td width="14%"><asp:TextBox ID="txtClient" CssClass="input" runat="server" MaxLength="30"></asp:TextBox></td>
    <td width="11%"><div align="right">备注:</div></td>
    <td width="14%"><asp:TextBox ID="txtRemark" CssClass="input" runat="server" MaxLength="20"></asp:TextBox></td>
    <td width="11%"><div align="right">赠送原因:</div></td>
    <td width="14%"><asp:TextBox ID="txtGiveMsg" CssClass="inputmid" runat="server" MaxLength="60"></asp:TextBox></td>
  </tr>
  <tr>
    <td width="11%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="14%" align="center"><asp:Button ID="btnStock" CssClass="button1" runat="server" Text="赠送登记" OnClick="btnStock_Click" /></td>
  </tr>
</table>

 </div>

     </div>
     </ContentTemplate>          
        </asp:UpdatePanel>
    </form>
</body>
</html>
