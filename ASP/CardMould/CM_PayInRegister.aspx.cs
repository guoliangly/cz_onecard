﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;

public partial class ASP_CardMould_CM_PayInRegister : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetPayMsg();
        }
    }

    private void GetPayMsg()
    {
        DataTable data = SPHelper.callCMQuery(context, "CM_payMsg", context.s_DepartID);

        double saleMoney = 0.00;
        double payinMoney = 0.00;
        double realMoney = 0.00;
        if (data != null && data.Rows.Count != 0)
        {
            saleMoney = Convert.ToDouble(data.Rows[0][0].ToString());
            payinMoney = Convert.ToDouble(data.Rows[0][1].ToString());
            realMoney = saleMoney - payinMoney;
        }
        txtSale.Text = saleMoney.ToString();
        txtPayIn.Text = payinMoney.ToString();
        txtMay.Text = realMoney.ToString();
    }

    //卡套入库判断
    private Boolean badCardbookinValidation()
    {
        //对卡套数量进行非空、数字校验

        if (txtPrice.Text.Trim() == "")
        {
            context.AddError("A094780131：金额不能为空", txtPrice);
        }
        else
        {
            if (!Validation.isPosRealNum(txtPrice.Text.Trim()))
            {
                context.AddError("A094780132：金额必须为数字", txtPrice);
            }
        }

        return !(context.hasError());
    }
    protected void btnStock_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        context.SPOpen();
        context.AddField("p_Price").Value = decimal.Parse(txtPrice.Text.Trim()) * 100;
        bool ok = context.ExecuteSP("SP_CM_PayInRegister");

        if (ok)
        {
            AddMessage("M094780984：解款成功");
            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
        }
    }
}
