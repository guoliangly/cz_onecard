﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;
using TDO.UserManager;
using PDO.Financial;
using Master;

public partial class ASP_CardMould_CM_StockOut : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            selDepart.Items.Add(new ListItem("---请选择---", ""));

            SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
            pdo.funcCode = "TD_M_INSIDEDEPARTITEM";
            StoreProScene storePro = new StoreProScene();
            DataTable data = storePro.Execute(context, pdo);
            if (data == null || data.Rows.Count == 0)
                return;

            for (int i = 0; i < data.Rows.Count; i++)
            {
                selDepart.Items.Add(new ListItem(data.Rows[i]["DEPARTNAME"].ToString(), data.Rows[i]["DEPARTNO"].ToString()));
            }
        }
    }

    protected void selStockType_changed(object sender, EventArgs e)
    {
        btnStock.Text = selStockType.SelectedValue == "03" ? "出库" : "取消出库";
    }
    //卡套入库判断
    private Boolean badCardbookinValidation()
    {
        //对卡套数量进行非空、数字校验

        if (txtStockOutNum.Text.Trim() == "")
        {
            context.AddError("A094780120：卡套数量不能为空", txtStockOutNum);
        }
        else
        {
            if (!Validation.isNum(txtStockOutNum.Text.Trim()))
            {
                context.AddError("A094780121：卡套数量必须为数字", txtStockOutNum);
            }
        }

        //对单价进行校验
        if (selDepart.SelectedValue == "")
        {
            context.AddError("A094780126：请选择营业网点", selDepart);
        }

        return !(context.hasError());
    }
    protected void btnStock_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        context.SPOpen();
        context.AddField("p_StockType").Value = selStockType.SelectedValue;
        context.AddField("p_StockNum").Value = txtStockOutNum.Text.Trim();
        context.AddField("p_DepartNo").Value = selDepart.SelectedValue;
        bool ok = context.ExecuteSP("SP_CM_StockOut");

        if (ok)
        {
            AddMessage("M094780998：卡套" + (selStockType.SelectedValue == "03" ? "出库" : "取消出库") + "成功");

            txtStockOutNum.Text = "";
        }
    }
}
