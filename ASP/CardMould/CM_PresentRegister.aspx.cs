﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;

public partial class ASP_CardMould_CM_PresentRegister : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //卡套入库判断
    private Boolean badCardbookinValidation()
    {
        //对卡套数量进行非空、数字校验

        if (txtStockOutNum.Text.Trim() == "")
        {
            context.AddError("A094780120：卡套数量不能为空", txtStockOutNum);
        }
        else
        {
            if (!Validation.isNum(txtStockOutNum.Text.Trim()))
            {
                context.AddError("A094780121：卡套数量必须为数字", txtStockOutNum);
            }
        }

        //对客户姓名进行非空、长度校验
        if (txtClient.Text.Trim() == "")
        {
            context.AddError("A094780127：客户姓名不能为空", txtClient);
        }
        else
        {
            if (Validation.strLen(txtClient.Text.Trim()) > 60)
            {
                context.AddError("A094780128：客户姓名长度不能大于30个汉字", txtClient);
            }
        }

        //对赠送原因进行长度校验
        if (txtGiveMsg.Text.Trim() != "")
        {
            if (Validation.strLen(txtGiveMsg.Text.Trim()) > 60)
            {
                context.AddError("A094780129：赠送原因长度不能大于30个汉字", txtGiveMsg);
            }
        }

        //对备注进行长度校验
        if (txtRemark.Text.Trim() != "")
        {
            if (Validation.strLen(txtRemark.Text.Trim()) > 40)
            {
                context.AddError("A094780130：备注长度不能大于20个汉字", txtRemark);
            }
        }

        return !(context.hasError());
    }
    protected void btnStock_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        context.SPOpen();
        context.AddField("p_StockNum").Value = txtStockOutNum.Text.Trim();
        context.AddField("p_ClientName").Value = txtClient.Text.Trim();
        context.AddField("p_GiveMsg").Value = txtGiveMsg.Text.Trim();
        context.AddField("p_Remark").Value = txtRemark.Text.Trim();
        bool ok = context.ExecuteSP("SP_CM_PresentRegister");

        if (ok)
        {
            AddMessage("M094780986：赠送卡套成功");
            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
        }
    }
}
