﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CM_StockOut.aspx.cs" Inherits="ASP_CardMould_CM_StockOut" %>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>卡套出库</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
<cr:CardReader id="cardReader" Runat="server"/> 
    <form id="form1" runat="server">
    <div class="tb">
卡套管理->卡套出库
</div>
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
<asp:BulletedList ID="bulMsgShow" runat="server">
</asp:BulletedList>
<script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>    
<div class="con">
<div class="card">卡套信息</div>
  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td width="11%"><div align="right">操作类型:</div></td>
    <td width="14%"><asp:DropDownList ID="selStockType" CssClass="input" runat="server" OnSelectedIndexChanged="selStockType_changed" AutoPostBack="true">
                     <asp:ListItem Text="03:出库" Value="03" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="04:取消出库" Value="04"></asp:ListItem>
                </asp:DropDownList></td>
    <td width="11%"><div align="right">营业网点:</div></td>
    <td width="14%"><asp:DropDownList ID="selDepart" CssClass="input" runat="server">
                </asp:DropDownList></td>
    <td width="11%"><div align="right">数量:</div></td>
    <td width="14%"><asp:TextBox ID="txtStockOutNum" CssClass="input" runat="server"></asp:TextBox></td>
    
    <td width="11%">&nbsp;</td>
    <td width="14%" align="center"><asp:Button ID="btnStock" CssClass="button1" runat="server" Text="出库" OnClick="btnStock_Click" /></td>
  </tr>
</table>

 </div>

     </div>
     </ContentTemplate>          
        </asp:UpdatePanel>
    </form>
</body>
</html>
