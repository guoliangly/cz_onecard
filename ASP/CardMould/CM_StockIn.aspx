﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CM_StockIn.aspx.cs" Inherits="ASP_CardMould_CM_StockIn" %>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>卡套入库</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
<cr:CardReader id="cardReader" Runat="server"/> 
    <form id="form1" runat="server">
    <div class="tb">
卡套管理->卡套入库
</div>
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
<asp:BulletedList ID="bulMsgShow" runat="server">
</asp:BulletedList>
<script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>    
<div class="con">
<div class="card">卡套信息</div>
  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td width="11%"><div align="right">入库类型:</div></td>
    <td width="14%"><asp:DropDownList ID="selStockInType" CssClass="input" runat="server">
                     <asp:ListItem Text="01:采购入库" Value="01" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="02:换货入库" Value="02"></asp:ListItem>
                </asp:DropDownList></td>
    <td width="11%"><div align="right">入库数量:</div></td>
    <td width="14%"><asp:TextBox ID="txtStockInNum" CssClass="input" runat="server"></asp:TextBox></td>
    <td width="11%"><div align="right">到货时间:</div></td>
    <td width="14%"><asp:TextBox ID="txtStockInDate" CssClass="input" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="Toolkit1" runat="server" TargetControlID="txtStockInDate" Format="yyyyMMdd" /></td>
    <td width="11%"><div align="right">卡片单价:</div></td>
    <td width="14%"><asp:TextBox ID="txtPrice" CssClass="input" runat="server" Text="1"></asp:TextBox></td>
  </tr>
  <tr>
    <td width="11%">&nbsp;</td>
    <td width="14%"></td>
    <td width="11%">&nbsp;</td>
    <td width="14%"></td>
    <td width="11%">&nbsp;</td>
    <td width="14%"></td>
    <td width="11%">&nbsp;</td>
    <td width="14%" align="center"><asp:Button ID="btnStock" CssClass="button1" runat="server" Text="卡套入库" OnClick="btnStock_Click" /></td>
  </tr>
</table>

 </div>

     </div>
     </ContentTemplate>          
        </asp:UpdatePanel>
    </form>
</body>
</html>
