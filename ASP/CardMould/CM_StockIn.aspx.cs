﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;

public partial class ASP_CardMould_CM_StockIn : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //卡套入库判断
    private Boolean badCardbookinValidation()
    {
        //对卡套数量进行非空、数字校验

        if (txtStockInNum.Text.Trim() == "")
        {
            context.AddError("A094780120：卡套数量不能为空", txtStockInNum);
        }
        else
        {
            if (!Validation.isNum(txtStockInNum.Text.Trim()))
            {
                context.AddError("A094780121：卡套数量必须为整数", txtStockInNum);
            }
        }

        //对到货时间进行校验
        if (txtStockInDate.Text.Trim() == "")
        {
            context.AddError("A094780122：到货时间不能为空", txtStockInDate);
        }
        else
        {
            if (!Validation.isDate(txtStockInDate.Text.Trim(), "yyyyMMdd"))
            {
                context.AddError("A094780123：到货时间必须为日期格式", txtStockInDate);
            }
            else if (DateTime.ParseExact(txtStockInDate.Text.Trim(), "yyyyMMdd", null).CompareTo(DateTime.ParseExact(DateTime.Now.ToString("yyyyMMdd"), "yyyyMMdd", null)) < 0)
            {
                context.AddError("A094780124：到货时间不能早于今天", txtStockInDate);
            }
        }

        //对单价进行校验
        if (txtPrice.Text.Trim() == "")
        {
            context.AddError("A094780125：卡片单价不能为空", txtPrice);
        }
        else
        {
            if (!Validation.isNum(txtPrice.Text.Trim()))
            {
                context.AddError("A094780126：卡片单价必须为数字", txtPrice);
            }
        }

        return !(context.hasError());
    }
    protected void btnStock_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        context.SPOpen();
        context.AddField("p_StockType").Value = selStockInType.SelectedValue;
        context.AddField("p_StockNum").Value = txtStockInNum.Text.Trim();
        context.AddField("p_StockDate").Value = txtStockInDate.Text.Trim();
        context.AddField("p_Price").Value = txtPrice.Text.Trim();
        bool ok = context.ExecuteSP("SP_CM_StockIn");

        if (ok)
        {
            AddMessage("M094780999：卡套入库成功");

            txtStockInNum.Text = "";
            txtStockInDate.Text = "";
        }
    }
}
