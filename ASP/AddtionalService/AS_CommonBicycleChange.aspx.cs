﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using TDO.BusinessCode;
using PDO.AdditionalService;
using Master;
using PDO.PersonalBusiness;
using System.Collections.Generic;
using DataExchange;

// 园林年卡补换卡

public partial class ASP_AddtionalService_AS_CommonBicycleChange : Master.FrontMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置可读属性
        if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

        setReadOnly(txtCardBalance, txtStartDate);

        // 初始化证件类型        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);
        
        // 初始化费用列表        decimal total = initFeeList(gvResult, "36");
        hidAccRecv.Value = total.ToString("n");
        
        // 初始化旧卡信息查询结果gridview
        UserCardHelper.resetData(gvOldCardInfo, null);
    }
    
    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnPrintPZ.Enabled = false;

        // 检查帐户信息        checkAccountInfo(txtCardNo.Text);

        // 检查是否开通自行车功能        checkGardenFeature();

        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

				hidReadCardOK.Value = !context.hasError() && txtCardNo.Text.Length > 0 ? "ok" : "fail";

        btnSubmit.Enabled = hidReadCardOK.Value == "ok"
            && gvOldCardInfo.SelectedIndex >= 0;

    }

    // 对话框确认处理
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        // 输入项判断处理（证件号码、旧卡号码、客户姓名）
        // ASHelper.changeCardQueryValidate(context, txtId, txtOldCardNo, txtName);
        // if (context.hasError()) return;

        // 从园林卡资料表中查询
        DataTable data = ASHelper.callQuery(context, "QueryBikeOldCards", selQueryType.SelectedValue,
            txtCondition.Text.Trim());


        if (data == null || data.Rows.Count == 0)
        {
            UserCardHelper.resetData(gvOldCardInfo, data);
            AddMessage("N005030001: 查询结果为空");
        }
        else
        {
            UserCardHelper.resetData(gvOldCardInfo, data);

            gvOldCardInfo.SelectedIndex = 0;
            gvOldCardInfo_SelectedIndexChanged(sender, e);
        }
    }

    // 检查自行车特征值
    void checkGardenFeature()
    {
        // 从园林卡资料表中检查USETAG值

        DataTable data = ASHelper.callQuery(context, "QureyBikeInfo", txtCardNo.Text);
        if (data.Rows.Count > 0)
        {
            context.AddError("A0BK000002: 当前卡已开通自行车功能");
        }
    }

    // 旧卡信息查询结果gridview行创建事件
    protected void gvOldCardInfo_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvOldCardInfo','Select$" + e.Row.RowIndex + "')");
        }
    }

    // 旧卡信息查询结果行选择事件
    public void gvOldCardInfo_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行
        GridViewRow selectRow = gvOldCardInfo.SelectedRow;

        // 根据选择行卡号读取用户信息
        readCustInfo(selectRow.Cells[0].Text, txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        hidEndDate.Value = selectRow.Cells[1].Text;
        hidUsabelTimes.Value = selectRow.Cells[3].Text;

        btnSubmit.Enabled = !context.hasError() && hidReadCardOK.Value == "ok";
    }

    // 提交前输入项校验
    private void submitValidate()
    {
        // 用户信息校验
        custInfoValidate(txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);
    }

    /// <summary>
    /// 提交按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (context.hasError()) return;


        SP_AS_BIKEChangePDO pdo = new SP_AS_BIKEChangePDO();
        pdo.newcardNo = txtCardNo.Text.Trim();
        pdo.oldcardNo = gvOldCardInfo.SelectedRow.Cells[0].Text;
        pdo.changetype ="16";//挂失补卡
        pdo.tradetypecode = "0C";

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);


        btnSubmit.Enabled = false;

        // 执行成功，显示成功消息

        if (ok)
        {
            if (!context.hasError()) context.AddMessage("龙人自行车功能换卡成功");

            string p_BatchId = context.GetFieldValue("p_TRADEID").ToString();

            //龙人自行车同步
            if (true)
            {
                string statuscode = "";
                //调用同步接口
                //查询需要同步的数据
                string batchID = p_BatchId;
                string[] parm = new string[1];
                parm[0] = batchID;
                DataTable syncDT = ASHelper.callQuery(context, "QureyBikeSync", parm);

                //批量后台调用接口
                List<SyncBikeRequest> syncRequest;
                DataBikeExchangeHelp.ParseFormDataTable(syncDT, batchID, out syncRequest);
                List<SyncBikeRequest> syncResponse;
                if (syncRequest.Count != 0)
                {
                    bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
                    if (!succ)
                    {
                        if (syncResponse[0].SyncErrInfo.Contains("Exception"))//超时
                        {
                            context.AddError("调用接口超时,请手工同步!超时错误:" + syncResponse[0].SyncErrInfo);
                        }
                        else
                        {
                            context.AddError("调用接口失败!" + syncResponse[0].SyncErrInfo);
                        }
                    }
                    else
                    {
                        context.AddMessage("调用接口成功!");
                    }
                }

                if (context.hasError())
                {
                    statuscode = "0";
                }
                else
                {
                    statuscode = "1";
                }

                //更新同步台帐子表
                context.DBOpen("StorePro");
                context.AddField("P_TRADEID").Value = p_BatchId;
                context.AddField("P_CSN").Value = txtCardNo.Text.Trim();
                context.AddField("P_STATE").Value = statuscode;
                context.AddField("P_ERROR").Value = GetErrorMsg();

                bool okInter = context.ExecuteSP("SP_PB_UPDATEBIKESYNCSTATE");
                if (okInter)
                {
                    context.AddMessage("更新库同步状态成功");
                }
            }

            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, pdo.oldcardNo, txtCustName.Text, "龙人自行车换卡", "0.00"
                     , "0.00", pdo.newcardNo, txtPaperNo.Text, "0.00", "0.00", hidAccRecv.Value, context.s_UserName,
                     context.s_DepartName,
                     selPaperType.SelectedValue == "" ? "" : selPaperType.SelectedItem.Text, "0.00", hidAccRecv.Value);

            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }
        }
    }

    private string GetErrorMsg()
    {
        string msg = "";
        ArrayList errorMessages = context.ErrorMessage;
        for (int index = 0; index < errorMessages.Count; index++)
        {
            if (index > 0)
                msg += "|";

            String error = errorMessages[index].ToString();
            int start = error.IndexOf(":");
            if (start > 0)
            {
                error = error.Substring(start + 1, error.Length - start - 1);
            }

            msg += error;
        }

        return msg;
    }
}
