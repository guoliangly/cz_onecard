﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ASP_AddtionalService_AS_ParkQuery : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.IsPostBack) return;

            initLoad(sender, e);

            //设置GridView绑定的DataTable
            UserCardHelper.resetData(lvwQuery, null);

            setReadOnly(txtParkPlace, LabCardtype, sDate, eDate, txtArea);
        }

    }
    protected void initLoad(object sender, EventArgs e)
    {
        beginDate.Text = DateTime.Today.AddDays(-30).ToString("yyyyMMdd");
        endDate.Text = DateTime.Today.ToString("yyyyMMdd");
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(lvwQuery, null);
        btnQueryImpl();

    }
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        readAcc();
        ScriptManager.RegisterStartupScript(this, this.GetType(),
            "writeCardScript", "readCardRecord();", true);
    }
    protected void btnDBread_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验

        if (!DBreadValidation())
            return;
        readAcc();
    }
    protected void btnQueryCard_Click(object sender, EventArgs e)
    {
        //对输入证件号码进行检验

        if (!PaperNoValidation())
            return;

        DataTable dt = ASHelper.callQuery(context, "QryCardno", txtPaperNo.Text.Trim(), txtPlaceCode.Text.Trim());

        if (dt != null && dt.Rows.Count > 0)
        {
            GroupCardHelper.fill(selCardNo, dt, true);
        }
        else
        {
            context.AddError("A001003192：未查找到对应卡号");
        }
    }
    protected void selCardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCardno.Text = selCardNo.SelectedValue;
    }

    public void lvwQuery_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwQuery.PageIndex = e.NewPageIndex;
        btnQueryImpl();
    }

    protected void lvwQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ControlDeal.RowDataBound(e);
    }

    private Boolean PaperNoValidation()
    {
        //对查询条件非空校验

        if (txtPaperNo.Text.Trim() == "" && txtPlaceCode.Text.Trim() == "")
            context.AddError("A001001145");

        return !(context.hasError());
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验

        txtCardno.Text = txtCardno.Text.Trim();
        if (txtCardno.Text == "")
            context.AddError("A001004113", txtCardno);
  
        return !(context.hasError());

    }

    protected void readAcc()
    {
        DataTable dt = ASHelper.callQuery(context, "QryCardInfo", txtCardno.Text.Trim());
        if (dt == null || dt.Rows.Count <= 0)
        {
            context.AddError("未查到卡片信息");
            return;
        }
        else
        {
            LabCardtype.Text = dt.Rows[0][0].ToString();
            txtParkPlace.Text = dt.Rows[0][2].ToString();
            sDate.Text = dt.Rows[0][3].ToString();
            eDate.Text = dt.Rows[0][4].ToString();
            txtArea.Text = dt.Rows[0][5].ToString();
            labPlaceCode.Text = dt.Rows[0][6].ToString();
            labPlateNo.Text = dt.Rows[0][7].ToString();
            labSequence.Text = dt.Rows[0][8].ToString();
            CustName.Text = dt.Rows[0][9].ToString();
            CustBirthday.Text = dt.Rows[0][10].ToString();
            Papertype.Text = dt.Rows[0][11].ToString();
            Paperno.Text = dt.Rows[0][12].ToString();
            Custsex.Text = sexCodeToName(dt.Rows[0][13].ToString().Trim());
            Custphone.Text = dt.Rows[0][14].ToString();
            Custpost.Text = dt.Rows[0][15].ToString();
            Custaddr.Text = dt.Rows[0][16].ToString();
            txtEmail.Text = dt.Rows[0][17].ToString();
            Remark.Text = dt.Rows[0][18].ToString();
        }

        UserCardHelper.resetData(lvwQuery, null);
    }
    public string sexCodeToName(string sex)
    {
        if (sex != "")
        {
            if (sex == "0")
            {
                sex = "男";
            }
            else
            {
                sex = "女";
            }
        }
        return sex;
    }

    protected void btnQueryImpl()
    {
        //对起始日期和终止日期做检验

        if (!dateValidation())
            return;

        DataTable data = ASHelper.callQuery(context, "QryCardBusiness", txtCardno.Text, beginDate.Text, endDate.Text);
        UserCardHelper.resetData(lvwQuery, data);

    }
    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        Validation valid = new Validation(context);

        if (valid.beDate(beginDate, "").Value.AddMonths(12) < DateTime.Now)
            context.AddError("A001003101");

        return !(context.hasError());
    }
}
