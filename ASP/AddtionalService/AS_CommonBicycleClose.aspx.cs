﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.CardManager;
using TDO.UserManager;
using TM;
using Common;
using TDO.ResourceManager;
using TDO.BusinessCode;
using PDO.PersonalBusiness;
using Master;
using PDO.AdditionalService;
using DataExchange;
using System.Collections;
using TDO.PersonalTrade;

/// <summary>
/// 附加业务-公共自行车关闭
/// </summary>
public partial class ASP_AddtionalService_AS_CommonBicycleClose : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        InitPageControls();
    }


    private void InitPageControls()
    {
        //清除页面显示
        foreach (Control con in this.Page.Controls)
        {
            ClearControl(con);
        }
        this.chkPingzheng.Checked = true;

        ASHelper.initBikeCompanyList(context, ddlBikeCompany);

        ddlBikeCompany.SelectedValue = "BKLR";

        DataTable dt = ASHelper.callQuery(context, "ReadDepositByCompany", "BKLR");
        if (dt.Rows.Count > 0)
        {
            DepositFee.Text = "-"+dt.Rows[0][0].ToString();
            Total.Text = "-" + dt.Rows[0][0].ToString();
            ReturnSupply.Text = "-" + dt.Rows[0][0].ToString();
        }
        else
        {
            DepositFee.Text = "0.00";
            Total.Text = "0.00";
            ReturnSupply.Text = "0.00";
        }
    }

    protected void ddlBikeCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = ASHelper.callQuery(context, "ReadDepositByCompany", ddlBikeCompany.SelectedValue);

        if (dt.Rows.Count > 0)
        {
            DepositFee.Text = "-" + dt.Rows[0][0].ToString();
            Total.Text = "-" + dt.Rows[0][0].ToString();
            ReturnSupply.Text = "-" + dt.Rows[0][0].ToString();
        }
        else
        {
            DepositFee.Text = "0.00";
            Total.Text = "0.00";
            ReturnSupply.Text = "0.00";
        }
    }


    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "submit")
        {
            btnDBRead_Click(sender, e);
        }
    }



    #region 读卡/读数据库按钮事件
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnReadCardProcess();
        if (context.hasError()) return;
        btnSubmit.Enabled = true;
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        if (!DBreadValidation())
            return;

        btnDBReadCardProcess();
        if (context.hasError()) return;
        btnSubmit.Enabled = true;
    }

    protected void btnReadCardProcess()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = hiddenLabCardtype.Value;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001107112");
            return;
        }

        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }

        //从资源状态编码表中读取数据
        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTD_M_RESOURCESTATEOut == null)
            RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //给页面显示项赋值
        LabAsn.Text = hiddenAsn.Value.Substring(4, 16);
        LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
        sDate.Text = ASHelper.toDateWithHyphen(hiddensDate.Value);
        eDate.Text = ASHelper.toDateWithHyphen(hiddeneDate.Value);
        cMoney.Text = ((Convert.ToDecimal(hiddencMoney.Value)) / (Convert.ToDecimal(100))).ToString("0.00");
        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

        //检验卡片是否已经启用
        if (String.Compare(hiddensDate.Value, DateTime.Today.ToString("yyyyMMdd")) > 0)
        {
            context.AddError("卡片尚未启用");
            return;
        }

        //性别显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else CustBirthday.Text = Bdate;
        }
        else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

        //证件类型显示
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

        hiddentxtCardno.Value = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        sellTime.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");

        //判断是否记名卡
        CommonHelper.readCardJiMingState(context, txtCardno.Text, hidIsJiMing);

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardno.Text);
        //判断是否开通龙人自行车
        if (!openFunc.List.Contains("龙人公共自行车"))
        {
            context.AddError("A0BK000002:此卡未开通龙人自行车功能");
        }
        btnPrintPZ.Enabled = false;


        //调用接口判断是否允许关闭
        //重新调用接口
        DataTable data = ASHelper.callQuery(context, "BicycleVerify", txtCardno.Text.Trim(), ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE, Paperno.Text, "2");

        //批量后台调用接口
        List<SyncBikeRequest> syncRequest;
        DataBikeExchangeHelp.BikeVerifyForDataTable(data, out syncRequest);
        List<SyncBikeRequest> syncResponse;
        if (syncRequest.Count != 0)
        {
            bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
            if (!succ)
            {
                context.AddError("调用接口失败!" + syncResponse[0].SyncErrInfo);

            }
            else
            {
                context.AddMessage("调用接口验证成功!");
            }
        }

    }

    protected void btnDBReadCardProcess()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001107112");
            return;
        }



        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }
        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTL_R_ICUSEROut.CARDTYPECODE;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);


        //从资源状态编码表中读取数据
        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTD_M_RESOURCESTATEOut == null)
            RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);



        //性别显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else CustBirthday.Text = Bdate;
        }
        else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

        //证件类型显示
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

        hiddentxtCardno.Value = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        sellTime.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");


        //给页面显示项赋值
        LabAsn.Text = ddoTL_R_ICUSEROut.ASN;
        LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
        sDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");
        eDate.Text = "";//ASHelper.toDateWithHyphen(hiddeneDate.Value);
        cMoney.Text = "0.00";
        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
        //判断是否记名卡
        CommonHelper.readCardJiMingState(context, txtCardno.Text, hidIsJiMing);

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardno.Text);
        //判断是否开通龙人自行车
        if (!openFunc.List.Contains("龙人公共自行车"))
        {
            context.AddError("A0BK000002:此卡未开通龙人自行车功能");
        }
        btnPrintPZ.Enabled = false;


        //调用接口判断是否允许关闭
        //重新调用接口
        DataTable data = ASHelper.callQuery(context, "BicycleVerify", txtCardno.Text.Trim(), ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE, Paperno.Text, "2");

        //批量后台调用接口
        List<SyncBikeRequest> syncRequest;
        DataBikeExchangeHelp.BikeVerifyForDataTable(data, out syncRequest);
        List<SyncBikeRequest> syncResponse;
        if (syncRequest.Count != 0)
        {
            bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
            if (!succ)
            {
                context.AddError("调用接口失败!" + syncResponse[0].SyncErrInfo);

            }
            else
            {
                context.AddMessage("调用接口验证成功!");
            }
        }
    }



    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001008100", txtCardno);
        else
        {
            if ((txtCardno.Text.Trim()).Length != 16)
                context.AddError("A001008101", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001008102", txtCardno);
        }

        return !(context.hasError());
    }
    #endregion



    private string GetSeq()
    {
        context.SPOpen();
        context.AddField("step").Value = 1;
        context.AddField("seq", "string", "Output", "16");
        context.ExecuteReader("SP_GetSeq");
        context.DBCommit();

        return "" + context.GetFieldValue("seq");
    }

    /// <summary>
    /// 提交按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        string tradeid = GetSeq();

        TMTableModule tmTMTableModule = new TMTableModule();

        //查询最近一次开通，补换卡的业务流水号
        TF_B_TRADETDO ddoTF_B_TRADEIn = new TF_B_TRADETDO();
        string strTradeid = "Select TRADEID From TF_B_BIKETRADE WHERE CARDNO = '" + txtCardno.Text + "' " +
                            " And TRADETYPECODE in ( '0D','0C') AND CANCELTAG = '0' order by operatetime desc";

        DataTable dataTradeid = tmTMTableModule.selByPKDataTable(context, ddoTF_B_TRADEIn, null, strTradeid, 0);


        DataTable syncDT = ASHelper.callQuery(context, "QureyBikeCloseSync", dataTradeid.Rows[0][0].ToString(),tradeid);

        if (syncDT == null || syncDT.Rows.Count == 0)
        {
            context.AddError("查询同步数据失败!");
        }
        //先调关闭接口，返回成功后再处理
        List<SyncBikeRequest> syncRequest;
        DataBikeExchangeHelp.ParseFormDataTable(syncDT, dataTradeid.Rows[0][0].ToString(), out syncRequest);
        List<SyncBikeRequest> syncResponse;
        if (syncRequest.Count != 0)
        {
            bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
            if (!succ)
            {
                if (syncResponse[0].SyncErrInfo.Contains("Exception"))//超时
                {
                    context.AddError("调用关闭接口超时,请手工同步!超时错误:" + syncResponse[0].SyncErrInfo);
                }
                else
                {
                    context.AddError("调用关闭接口失败!" + syncResponse[0].SyncErrInfo);
                }
            }
            else
            {
                context.AddMessage("调用关闭接口成功!");
            }
        }
        else
        {
            context.AddError("调用接口时，未查询出数据!");
        }

        if (context.hasError()) return;

        SP_AS_BIKEClosePDO pdo = new SP_AS_BIKEClosePDO();
        pdo.cardNo = txtCardno.Text.Trim();
        pdo.companyno = ddlBikeCompany.SelectedValue;
        pdo.deposit = Convert.ToInt32((Convert.ToDecimal(Total.Text.Trim()) * 100));
        pdo.tradetypecode = "D0";
        pdo.cancelTradeid = dataTradeid.Rows[0][0].ToString();
        pdo.tradeid = tradeid;
        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);


        btnSubmit.Enabled = false;

        // 执行成功，显示成功消息

        if (ok)
        {
            if (!context.hasError()) context.AddMessage("龙人自行车功能关闭成功");

            string p_BatchId = tradeid;

            //龙人自行车同步
            if (true)
            {
                string statuscode = "";
                //调用同步接口
                //查询需要同步的数据
                //string batchID = p_BatchId;
                //string[] parm = new string[1];
                //parm[0] = batchID;
                //DataTable syncDT1 = ASHelper.callQuery(context, "QureyBikeSync", parm);

                ////批量后台调用接口
                //List<SyncBikeRequest> syncRequest;
                //DataBikeExchangeHelp.ParseFormDataTable(syncDT, batchID, out syncRequest);
                //List<SyncBikeRequest> syncResponse;
                //if (syncRequest.Count != 0)
                //{
                //    bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
                //    if (!succ)
                //    {
                //        if (syncResponse[0].SyncCode == "2")//超时
                //        {
                //            context.AddError("调用接口超时,请手工同步!超时错误:" + syncResponse[0].SyncErrInfo);
                //        }
                //        else
                //        {
                //            context.AddError("调用接口失败!" + syncResponse[0].SyncErrInfo);
                //        }
                //    }
                //    else
                //    {
                //        context.AddMessage("调用接口成功!");
                //    }
                //}

                if (context.hasError())
                {
                    statuscode = "0";
                }
                else
                {
                    statuscode = "1";
                }

                //更新同步台帐子表
                context.DBOpen("StorePro");
                context.AddField("P_TRADEID").Value = p_BatchId;
                context.AddField("P_CSN").Value = txtCardno.Text;
                context.AddField("P_STATE").Value = statuscode;
                context.AddField("P_ERROR").Value = GetErrorMsg();

                bool okInter = context.ExecuteSP("SP_PB_UPDATEBIKESYNCSTATE");
                if (okInter)
                {
                    context.AddMessage("更新库同步状态成功");
                }
            }

            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, CustName.Text, "龙人自行车关闭", Convert.ToInt32((Convert.ToDecimal(Total.Text.Trim()))).ToString()+"(押金)"
                ,"", "", Paperno.Text, "0.00", "0.00", "", context.s_UserName,
                context.s_DepartName, Papertype.Text, "0.00", "");

            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }
        }
    }

    private string GetErrorMsg()
    {
        string msg = "";
        ArrayList errorMessages = context.ErrorMessage;
        for (int index = 0; index < errorMessages.Count; index++)
        {
            if (index > 0)
                msg += "|";

            String error = errorMessages[index].ToString();
            int start = error.IndexOf(":");
            if (start > 0)
            {
                error = error.Substring(start + 1, error.Length - start - 1);
            }

            msg += error;
        }

        return msg;
    }
}
