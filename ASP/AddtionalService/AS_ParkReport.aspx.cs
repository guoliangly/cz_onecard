﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.UserManager;
using TM;

public partial class ASP_AddtionalService_AS_ParkReport : Master.ExportMaster
{
    private int operanum = 0;         //操作次数
    private int operamoney = 0;         //操作金额

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            # region 初始化部门
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
            TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
            ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
            InitStaffList(context.s_DepartID);
            # endregion

            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");
            ASHelper.initParkPlaceList(context, selParkPlace);
            ASHelper.initAreaList(context, selArea);
            ASHelper.initCardTypeList(context, selCardType);

            gvResult.DataSource = null;
            gvResult.DataBind();
            gvAccount.DataSource = null;
            gvAccount.DataBind();
        }

    }
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError())
        {
            return;
        }
        List<string> vars = new List<string>();
        vars.Add(selParkPlace.SelectedValue);
        vars.Add(selArea.SelectedValue);
        vars.Add(selCardType.SelectedValue);
        vars.Add(selDept.SelectedValue);
        vars.Add(selStaff.SelectedValue);
        vars.Add(txtCardNo.Text.Trim());
        vars.Add(seltradetype.SelectedValue);
        vars.Add(txtPlaceCode.Text.Trim());
        vars.Add(txtPlateNo.Text.Trim());
        vars.Add(txtFree.Text.Trim() == "" ? "" : Convert.ToInt32((Convert.ToDecimal(txtFree.Text)) * 100).ToString());
        vars.Add(txtFromDate.Text.Trim());
        vars.Add(txtToDate.Text.Trim());

        if (chkFlag.Checked == false)
        {
            printarea.Visible = true;
            queryans1.Visible = true;
            printarea2.Visible = false;
            queryans2.Visible = false;

            DataTable data = new DataTable();
            data = ASHelper.callQuery(context, "QueryParkDetail", vars.ToArray());
            if (data.Rows.Count <= 0)
            {
                gvResult.DataSource = null;
                gvResult.DataBind();
                context.AddError("查询结果为空");
                return;
            }
            gvResult.DataSource = data;
            gvResult.DataBind();
        }
        else
        {
            printarea.Visible = false;
            queryans1.Visible = false;
            printarea2.Visible = true;
            queryans2.Visible = true;

            DataTable data = new DataTable();
            data = ASHelper.callQuery(context, "QueryParkReport", vars.ToArray());
            if (data.Rows.Count <= 0)
            {
                gvAccount.DataSource = null;
                gvAccount.DataBind();
                context.AddError("查询结果为空");
                return;
            }
            gvAccount.DataSource = data;
            gvAccount.DataBind();
        }
    }
    protected void gvAccount_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && gvAccount.ShowFooter)
        {
            operanum += Convert.ToInt32(GetTableCellValue(e.Row.Cells[5]));
            operamoney += Convert.ToInt32(GetTableCellValue(e.Row.Cells[6]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[0].Text = "合计";
            e.Row.Cells[5].Text = operanum.ToString();
            e.Row.Cells[6].Text = operamoney.ToString();
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    protected void btnExport_Click2(object sender, EventArgs e)
    {
        if (gvAccount.Rows.Count > 0)
        {
            ExportGridView(gvAccount);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    // 查询输入校验处理
    private void validate()
    {
        //校验功能费        if (txtFree.Text.Trim() != "")
        {
            if (!Validation.isPosRealNum(txtFree.Text.Trim()))
                context.AddError("A001001147", txtFree);
        }

        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }

    protected void selDept_Changed(object sender, EventArgs e)
    {
        InitStaffList(selDept.SelectedValue);
    }

    private void InitStaffList(string deptNo)
    {
        if (deptNo == "")
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    /// <summary>
    /// 选择停车场
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlParkPlace_SelectedIndexChanged(object sender, EventArgs e)
    {
        selArea.Items.Clear();
        ASHelper.initAreaList(context, selArea, selParkPlace.SelectedValue);
    }

    /// <summary>
    /// 选择区域
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        //根据区域查找停车场自动选中
        DataTable dt = ASHelper.callQuery(context, "ReadPlaceCodeByArea", selArea.SelectedValue);

        if (dt.Rows.Count > 0)
        {
            selParkPlace.SelectedValue = dt.Rows[0][0].ToString();
        }
    }
}
