﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using TDO.BusinessCode;
using PDO.AdditionalService;
using Master;
using PDO.PersonalBusiness;

// 园林年卡补换卡
public partial class ASP_AddtionalService_AS_GardenCardChange : Master.FrontMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置可读属性
        if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

        setReadOnly(txtCardBalance, txtStartDate);

        // 初始化证件类型        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);
        
        // 初始化费用列表        decimal total = initFeeList(gvResult, "36");
        hidAccRecv.Value = total.ToString("n");
        
        // 初始化旧卡信息查询结果gridview
        UserCardHelper.resetData(gvOldCardInfo, null);
    }
    
    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnPrintPZ.Enabled = false;

        // 检查帐户信息        checkAccountInfo(txtCardNo.Text);

        // 检查园林特征值        checkGardenFeature();

        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

				hidReadCardOK.Value = !context.hasError() && txtCardNo.Text.Length > 0 ? "ok" : "fail";

        btnSubmit.Enabled = hidReadCardOK.Value == "ok"
            && gvOldCardInfo.SelectedIndex >= 0;

    }

    // 对话框确认处理
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "writeSuccess")  // 写卡成功
        {
            clearCustInfo(txtCardNo, txtCustName, txtCustBirth, selPaperType, txtPaperNo,
                selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);
            AddMessage("D005020002: 园林年卡前台写卡成功，补换卡成功");
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("A00502C001: 园林年卡前台写卡失败，补换卡失败");
        }

        hidWarning.Value = "";       // 清除警告信息

        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        // 输入项判断处理（证件号码、旧卡号码、客户姓名）
        // ASHelper.changeCardQueryValidate(context, txtId, txtOldCardNo, txtName);
        // if (context.hasError()) return;

        // 从园林卡资料表中查询
        DataTable data = ASHelper.callQuery(context, "QueryOldCards",  selQueryType.SelectedValue,
            txtCondition.Text.Trim());


        if (data == null || data.Rows.Count == 0)
        {
            UserCardHelper.resetData(gvOldCardInfo, data);
            AddMessage("N005030001: 查询结果为空");
        }
        else
        {
            UserCardHelper.resetData(gvOldCardInfo, data);

            gvOldCardInfo.SelectedIndex = 0;
            gvOldCardInfo_SelectedIndexChanged(sender, e);
        }
    }

    // 检查园林卡特征值
    void checkGardenFeature()
    {
        // 从园林卡资料表中检查USETAG值
        DataTable data = ASHelper.callQuery(context, "ParkCardUseTag", txtCardNo.Text);
        if (data.Rows.Count > 0)
        {
            context.AddError("A005020001: 当前卡已经是园林年卡");
        }
    }

    // 旧卡信息查询结果gridview行创建事件
    protected void gvOldCardInfo_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvOldCardInfo','Select$" + e.Row.RowIndex + "')");
        }
    }

    // 旧卡信息查询结果行选择事件
    public void gvOldCardInfo_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行
        GridViewRow selectRow = gvOldCardInfo.SelectedRow;

        // 根据选择行卡号读取用户信息
        readCustInfo(selectRow.Cells[0].Text, txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        hidEndDate.Value = selectRow.Cells[1].Text;
        hidUsabelTimes.Value = selectRow.Cells[3].Text;

        btnSubmit.Enabled = !context.hasError() && hidReadCardOK.Value == "ok";
    }

    // 提交前输入项校验
    private void submitValidate()
    {
        // 用户信息校验
        custInfoValidate(txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);
    }

    // 园林年卡补换卡提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 提交前输入校验
        submitValidate();
        if (context.hasError()) return;

        // 调用园林年卡补换卡存储过程
        SP_AS_GardenCardChangePDO pdo = new SP_AS_GardenCardChangePDO();
        pdo.ID = DealString.GetRecordID(hidTradeNo.Value, hidAsn.Value);
        pdo.oldCardNo = gvOldCardInfo.SelectedRow.Cells[0].Text;
        pdo.newCardNo = txtCardNo.Text;
        pdo.asn = hidAsn.Value.Substring(4, 16);
        pdo.operCardNo = context.s_CardID; // 操作员卡
        pdo.terminalNo = "112233445566";   // 目前固定写成112233445566

        int usableTimes = int.Parse(hidUsabelTimes.Value);

        // 12位,年月日8位+标志位2位+次数2位
        // 园林年卡的标志位为'01',休闲年卡的标志位为'02'.次数都是16进制.
        pdo.endDateNum = hidEndDate.Value + "01" + usableTimes.ToString("X2");
        hidParkInfo.Value = pdo.endDateNum;

        pdo.custName = txtCustName.Text;
        pdo.custBirth = txtCustBirth.Text;
        pdo.paperType = selPaperType.SelectedValue;
        pdo.paperNo = txtPaperNo.Text;
        pdo.custSex = selCustSex.SelectedValue;
        pdo.custPhone = txtCustPhone.Text;
        pdo.custPost = txtCustPost.Text;
        pdo.custAddr = txtCustAddr.Text;
        pdo.custEmail = txtEmail.Text;
        pdo.remark = txtRemark.Text;

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);

        txtCardNo.Text = "";
				hidReadCardOK.Value = "fail";
        UserCardHelper.resetData(gvOldCardInfo, null);
 				gvOldCardInfo.SelectedIndex = -1;
        btnSubmit.Enabled = false;

        // 调用成功，显示成功信息
        if (ok)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "startPark();", true);

            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, pdo.oldCardNo, txtCustName.Text, "园林年卡补换卡", "0.00"
                , "0.00", pdo.newCardNo, txtPaperNo.Text, "0.00", "0.00", hidAccRecv.Value, context.s_UserName,
                context.s_DepartName,
                selPaperType.SelectedValue == "" ? "" : selPaperType.SelectedItem.Text, "0.00", hidAccRecv.Value);
        }
    }
}
