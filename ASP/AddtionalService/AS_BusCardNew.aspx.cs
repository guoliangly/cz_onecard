﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using PDO.AdditionalService;
using TM;
using System.Data;
using TDO.UserManager;

public partial class ASP_AddtionalService_AS_BusCardNew : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

        // 设置可读属性
        setReadOnly(txtCardBalance, txtStartDate, txtEndDate);

        // 设置焦点以及按键事件
        txtRealRecv.Attributes["onfocus"] = "this.select();";
        txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this);";

        // 初始化月票类型
        //ASHelper.initMonthlyCardTypeList(selMonthlyCardType);
        selMonthlyCardType.Items.Add(new ListItem("---请选择---", ""));
        selMonthlyCardType.Items.Add(new ListItem("学生卡", "01"));
        selMonthlyCardType.Items.Add(new ListItem("老人卡", "03"));
        selMonthlyCardType.Items.Add(new ListItem("高龄卡", "24"));

        // 行政区域
        selMonthlyCardDistrict.Items.Add(new ListItem("---请选择---", ""));

        // 初始化证件类型
        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);

        // 初始化费用列表
        initGridView();

    }

    // 初始化费用项gridview
    private void initGridView()
    {
        // 业务类型编码表：31学生月票开卡，32老人月票开卡 23高龄卡开卡

        string type = selMonthlyCardType.SelectedValue;

        hidTradeTypeCode.Value
                = type == "01" ? "31" // 学生卡售卡

                : type == "24" ? "23" // 高龄卡售卡

                : type == "03" ? "32" // 老人卡售卡

                : "";

        initFeeItem(gvResult, hidTradeTypeCode.Value, hidDeposit,
            hidCardPost, hidOtherFee, hidAccRecv,
            txtRealRecv, false, 0, 0);
    }

    // 刷新行政区域选择
    protected void selMonthlyCardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (Control con in this.Page.Controls)
        {
            clearControls(con);
        }

        // 初始化行政区域

        ASHelper.SelectDistricts(context,
            selMonthlyCardDistrict, selMonthlyCardType.SelectedValue);

        initGridView();
        if (selMonthlyCardType.SelectedValue != "")
        {
            btnReadCard.Enabled = true;
        }
        else
        {
            btnReadCard.Enabled = false;
        }

        btnSubmit.Enabled = false;


    }

    private void clearControls(Control control)
    {
        foreach (Control c in control.Controls)
        {
            if (c is Label)
            {
                ((Label)c).Text = "";
            }
            else if (c is TextBox)
            {
                ((TextBox)c).Text = "";
            }
            else if (c is HiddenField)
            {
                ((HiddenField)c).Value = "";
            }
            else if (c is DropDownList)
            {
                if (c.ID != "selMonthlyCardType" && c.ID != "selMonthlyCardDistrict")
                {
                    ((DropDownList)c).SelectedValue = "";
                }
            }

            if (c.Controls.Count > 0)
            {
                clearControls(c);
            }
        }
    }

    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        // 读取卡片状态
        ASHelper.readCardState(context, txtCardNo.Text, txtCardState);
        checkCardState(txtCardNo.Text);

        // 检查新卡卡内余额
        if (txtCardBalance.Text != "0.00")
        {
            context.AddError("A001001144");
        }

        //读卡时判断此身份证号是否可以优惠，防止输完身份证号后再读卡，则优惠方案会变化。 wdx 20130311
        if (txtPaperNo.Text.Trim() != "")
        {
            if (txtCardNo.Text.Trim().Substring(0, 8) != "91500219")
            {
                DataTable data = SPHelper.callPBQuery(context, "QryDiscount", txtPaperNo.Text.Trim());
                //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠

                if (data == null || data.Rows.Count == 0)
                {
                    QryBusFee(gvResult, txtRealRecv);
                    return;
                }
            }
            else
            {
                DataTable data2 = SPHelper.callPBQuery(context, "QryGLK", txtPaperNo.Text.Trim());
                if (data2 == null || data2.Rows.Count == 0)
                {
                    DataTable data3 = SPHelper.callPBQuery(context, "QryLossGLK", txtPaperNo.Text.Trim());
                    if (data3 == null || data3.Rows.Count == 0)
                    {
                        DataTable datedt1 = new DataTable();
                        datedt1.Columns.Add("FEETYPENAME", typeof(string));
                        datedt1.Columns.Add("BASEFEE", typeof(string));
                        datedt1.Rows.Add("卡费", ((decimal)0).ToString("n"));
                        int i = 0;

                        for (; i < 6; ++i)
                        {
                            datedt1.Rows.Add("", "");
                        }

                        txtRealRecv.Text = 0.ToString("0");
                        hidAccRecv.Value = 0.ToString("n");
                        hidCardPost.Value = 0.ToString("n");
                        datedt1.Rows.Add("合计应收", txtRealRecv.Text);

                        gvResult.DataSource = datedt1;
                        gvResult.DataBind();
                    }
                    else
                    {
                        DataTable datedt2 = new DataTable();
                        datedt2.Columns.Add("FEETYPENAME", typeof(string));
                        datedt2.Columns.Add("BASEFEE", typeof(string));
                        datedt2.Rows.Add("卡费", ((decimal)30).ToString("n"));
                        int i = 0;

                        for (; i < 6; ++i)
                        {
                            datedt2.Rows.Add("", "");
                        }

                        txtRealRecv.Text = 30.ToString("0");
                        hidAccRecv.Value = 0.ToString("n");
                        hidCardPost.Value = 30.ToString("n");
                        datedt2.Rows.Add("合计应收", txtRealRecv.Text);

                        gvResult.DataSource = datedt2;
                        gvResult.DataBind();
                    }
                }
                else
                {
                    context.AddError("该身份证已拥有有效高龄卡，如需重新办理，请先旧卡挂失。");
                }
            }
        }

        btnPrintSJ.Enabled = false;
        btnSubmit.Enabled = !context.hasError();
        btnQuery.Enabled = !context.hasError();
    }

    // 提交判断
    private void submitValidate()
    {
        // 校验客户信息
        custInfoValidate(txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        Validation valid = new Validation(context);
        valid.check(selMonthlyCardType.SelectedValue != "", "A005090001: 公交类型必须选择", selMonthlyCardType);
        //valid.check(selMonthlyCardDistrict.SelectedValue != "", "A005090002: 行政区域必须选择", selMonthlyCardDistrict);
        //valid.check(selCustSex.SelectedValue != "", "A005090003: 性别必须选择", selCustSex);
    }

    // 确认对话框确认处理
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")    // 是否继续
        {
            btnSubmit.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess") // 写卡成功
        {
            clearCustInfo(selMonthlyCardType,
               txtCardNo, txtCustName, txtCustBirth, selPaperType, txtPaperNo,
               selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);

            AddMessage("公交卡售卡成功");
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("前台写卡失败，公交卡售卡失败");
        }

        if (chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printShouJu();", true);
        }

        hidWarning.Value = ""; // 清除警告信息
        hidCardPost.Value = "";
        hidDeposit.Value = "";
        initGridView();
    }

    //常州公交卡号判断
    private Boolean busCardnoValidation()
    {
        //对卡号进行非空、长度、数字检验


        if (txtBusCardno.Text.Trim() == "")
            context.AddError("A001004113", txtBusCardno);
        else
        {
            if (Validation.strLen(txtBusCardno.Text.Trim()) != 8)
                context.AddError("公交卡号不是8位", txtBusCardno);
            else if (!Validation.isNum(txtBusCardno.Text.Trim()))
                context.AddError("A001004115", txtBusCardno);
        }

        return !(context.hasError());
    }

    protected void QryBusFee(GridView gvResult, TextBox txtRealRecv)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从系统参数表读取新卡卡费

        DataTable data = ASHelper.callQuery(context, "ReadBusFee");

        hidCardPost.Value = data.Rows[0][0].ToString();
        Object[] row = null;
        decimal total = 0;

        DataTable datedt = new DataTable();
        datedt.Columns.Add("FEETYPENAME", typeof(string));
        datedt.Columns.Add("BASEFEE", typeof(string));

        int i = 0;
        for (; i < data.Rows.Count; ++i)
        {
            row = data.Rows[i].ItemArray;
            total += (decimal)row[0];

            datedt.Rows.Add((string)row[1], ((decimal)row[0]).ToString("n"));
        }

        for (; i < 6; ++i)
        {
            datedt.Rows.Add("", "");
        }

        //hidCardPost.Value = data.Rows[0].ToString();
        txtRealRecv.Text = total.ToString("0");
        hidAccRecv.Value = total.ToString("n");
        datedt.Rows.Add("合计应收", txtRealRecv.Text);

        gvResult.DataSource = datedt;
        gvResult.DataBind();
    }

    //老人卡、学生卡和普通龙城通卡的优惠规则一样,金坛高龄卡每个证件只能拥有一张有效高龄卡，费用为0,高龄卡挂失补卡费用为30
    protected void Paperno_Changed(object sender, EventArgs e)
    {
        if (txtPaperNo.Text.Trim() != "")
        {
            if (txtCardNo.Text.Trim() != "")
            {
                if (txtCardNo.Text.Trim().Substring(0, 8) == "91500219")
                {
                    DataTable data2 = SPHelper.callPBQuery(context, "QryGLK", txtPaperNo.Text.Trim());
                    if (data2 == null || data2.Rows.Count == 0)
                    {
                        DataTable data3 = SPHelper.callPBQuery(context, "QryLossGLK", txtPaperNo.Text.Trim());
                        if (data3 == null || data3.Rows.Count == 0)
                        {
                            DataTable datedt1 = new DataTable();
                            datedt1.Columns.Add("FEETYPENAME", typeof(string));
                            datedt1.Columns.Add("BASEFEE", typeof(string));
                            datedt1.Rows.Add("卡费", ((decimal)0).ToString("n"));
                            int i = 0;

                            for (; i < 6; ++i)
                            {
                                datedt1.Rows.Add("", "");
                            }

                            txtRealRecv.Text = 0.ToString("0");
                            hidAccRecv.Value = 0.ToString("n");
                            hidCardPost.Value = 0.ToString("n");
                            datedt1.Rows.Add("合计应收", txtRealRecv.Text);

                            gvResult.DataSource = datedt1;
                            gvResult.DataBind();
                        }
                        else
                        {
                            DataTable datedt2 = new DataTable();
                            datedt2.Columns.Add("FEETYPENAME", typeof(string));
                            datedt2.Columns.Add("BASEFEE", typeof(string));
                            datedt2.Rows.Add("卡费", ((decimal)30).ToString("n"));
                            int i = 0;

                            for (; i < 6; ++i)
                            {
                                datedt2.Rows.Add("", "");
                            }

                            txtRealRecv.Text = 30.ToString("0");
                            hidAccRecv.Value = 0.ToString("n");
                            hidCardPost.Value = 30.ToString("n");
                            datedt2.Rows.Add("合计应收", txtRealRecv.Text);

                            gvResult.DataSource = datedt2;
                            gvResult.DataBind();
                        }
                    }
                    else
                    {
                        context.AddError("该身份证已拥有有效高龄卡，如需重新办理，请先旧卡挂失。");
                    }

                    btnSubmit.Enabled = !context.hasError();
                    btnQuery.Enabled = !context.hasError();
                }
                else
                {
                    DataTable data = SPHelper.callPBQuery(context, "QryDiscount", txtPaperNo.Text.Trim());
                    //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠
                    if (data == null || data.Rows.Count == 0)
                    {
                        QryBusFee(gvResult, txtRealRecv);
                        return;
                    }
                    else
                    {
                        if (txtCardNo.Text.Trim() != "")
                        {
                            initGridView();
                            btnReadCard_Click(sender, e);
                        }
                    }
                }
            }
            else
            {
                DataTable data = SPHelper.callPBQuery(context, "QryDiscount", txtPaperNo.Text.Trim());
                //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠
                if (data == null || data.Rows.Count == 0)
                {
                    QryBusFee(gvResult, txtRealRecv);
                    return;
                }
                else
                {
                    if (txtCardNo.Text.Trim() != "")
                    {
                        initGridView();
                        btnReadCard_Click(sender, e);
                    }
                }
            }
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //公交卡号判断
        if (!busCardnoValidation())
        {
            this.txtBusCardno.Text = "";//@add by liuhe 20110106,防止txtBusCardno中的值传入存储过程

            return;
        }

        DataTable dt = SPHelper.callPBQuery(context, "QryBusCardno", txtBusCardno.Text.Trim());

        if (dt != null && dt.Rows.Count > 0)
        {
            QryBusFee(gvResult, txtRealRecv);
        }
        else
        {//@modified by liuhe 20110106 添加else，防止txtBusCardno中的值传入存储过程

            this.context.AddError("未查到卡号为" + this.txtBusCardno.Text.Trim() + "的公交卡换卡信息");
            this.txtBusCardno.Text = "";
        }
    }

    //对售卡用户信息进行检验
    private Boolean SaleInfoValidation()
    {
        //对用户姓名进行非空、长度检验

        if (txtCustName.Text.Trim() == "")
            context.AddError("A001001111", txtCustName);

        //对用户性别进行非空检验

        if (selCustSex.SelectedValue == "")
            context.AddError("A001001116", selCustSex);

        //对证件类型进行非空检验

        if (selPaperType.SelectedValue == "")
            context.AddError("A001001117", selPaperType);

        //对出生日期进行非空、日期格式检验

        String cDate = txtCustBirth.Text.Trim();
        if (cDate == "")
            context.AddError("A001001114", txtCustBirth);

        //对联系电话进行非空、长度、数字检验

        if (txtCustPhone.Text.Trim() == "")
            context.AddError("A001001124", txtCustPhone);

        //对证件号码进行非空、长度、英数字检验

        if (txtPaperNo.Text.Trim() == "")
            context.AddError("A001001121", txtPaperNo);
        else if (!Validation.isCharNum(txtPaperNo.Text.Trim()))
            context.AddError("A001001122", txtPaperNo);
        else if (Validation.strLen(txtPaperNo.Text.Trim()) > 20)
            context.AddError("A001001123", txtPaperNo);
        else if (selPaperType.SelectedValue == "00")//身份证必须为15或18位。
        {
            if (Validation.strLen(txtPaperNo.Text.Trim()) != 18 && Validation.strLen(txtPaperNo.Text.Trim()) != 15)
                context.AddError("A001001130", txtPaperNo);
            else
            {
                //身份证号规则校验
                if (!CommonHelper.CheckIDCard(txtPaperNo.Text.Trim()))
                {
                    context.AddError("A001001131", txtPaperNo);
                }
            }
        }

        //对联系地址进行非空、长度检验

        if (txtCustAddr.Text.Trim() == "")
            context.AddError("A001001127", txtCustAddr);

        return !(context.hasError());
    }

    // 公交卡售卡提交
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //用户信息判断
        if (!SaleInfoValidation())
            return;

        if (selMonthlyCardType.SelectedValue == "01")
        {
            if (hiddenGjTag.Value != "01")
            {
                context.AddError("此卡不是学生卡，不能出售");
                return;
            }
        }
        else if (selMonthlyCardType.SelectedValue == "03")
        {
            if (hiddenGjTag.Value != "03")
            {
                context.AddError("此卡不是老人卡，不能出售");
                return;
            }
        }
        else if (selMonthlyCardType.SelectedValue == "24")
        {
            if (hiddenGjTag.Value != "24")
            {
                context.AddError("此卡不是高龄卡，不能出售");
                return;
            }
        }
        submitValidate();
        checkCardState(txtCardNo.Text);

        if (selMonthlyCardType.SelectedValue == "01")
        {
            string birthday = txtCustBirth.Text.Trim().Trim().Substring(0, 4) + "-" + txtCustBirth.Text.Trim().Trim().Substring(4, 2) + "-" + txtCustBirth.Text.Trim().Trim().Substring(6, 2);
            //mod by liuh20101206，把计算年审日期的方法放到FrontMaster中

            txtVerifyDate.Value = VerifyDateValidate(birthday);
        }

        if (context.hasError()) return;

        // 调用公交卡售卡存储过程

        SP_AS_BusCardNewPDO pdo = new SP_AS_BusCardNewPDO();
        pdo.ID = DealString.GetRecordID(hidTradeNo.Value, hidAsn.Value);
        pdo.cardNo = txtCardNo.Text;

        pdo.deposit = (int)(Double.Parse(hidDeposit.Value) * 100);

        pdo.cardCost = (int)(Double.Parse(hidCardPost.Value) * 100);
        pdo.otherFee = (int)(Double.Parse(hidOtherFee.Value) * 100);

        pdo.cardTradeNo = hidTradeNo.Value;
        pdo.asn = hidAsn.Value.Substring(4, 16);
        pdo.cardMoney = (int)(Double.Parse(txtCardBalance.Text) * 100);
        pdo.sellChannelCode = "01";
        pdo.serTakeTag = "0";
        pdo.tradeTypeCode = hidTradeTypeCode.Value;
        pdo.terminalNo = "112233445566";   // 目前固定写成112233445566

        pdo.custName = txtCustName.Text;
        pdo.custBirth = txtCustBirth.Text;
        pdo.paperType = selPaperType.SelectedValue;
        pdo.paperNo = txtPaperNo.Text;
        pdo.custSex = selCustSex.SelectedValue;
        pdo.custPhone = txtCustPhone.Text;
        pdo.custPost = txtCustPost.Text;
        pdo.custAddr = txtCustAddr.Text;
        pdo.custEmail = txtEmail.Text;
        pdo.remark = txtRemark.Text;

        pdo.custRecTypeCode = "1";
        pdo.appType = selMonthlyCardType.SelectedValue;
        pdo.assignedArea = selMonthlyCardDistrict.SelectedValue;
        pdo.endDate = txtVerifyDate.Value;
        pdo.BUSCARDNO = txtBusCardno.Text.Trim();

        pdo.currCardNo = context.s_CardID;

        hidMonthlyFlag.Value = pdo.assignedArea;

        hidUserName.Value = pdo.custName;
        hidUserSex.Value = pdo.custSex == "0" ? "01" //写卡时转换性别编码
                         : pdo.custSex == "1" ? "02"
                         : "09";
        hidUserPaperno.Value = pdo.paperNo;
        hidUserPhone.Value = pdo.custPhone;

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);
        btnSubmit.Enabled = false;

        // 存储过程执行成功，显示成功消息

        if (ok)
        {
            if (hidTradeTypeCode.Value == "31")
            {
                ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, txtCustName.Text, "学生卡售卡", hidAccRecv.Value
              , "", "", "", "",
              "", "", context.s_UserName, context.s_DepartName, "", "0.00", "");
            }
            else if (hidTradeTypeCode.Value == "32")
            {
                ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, txtCustName.Text, "老人卡售卡", hidAccRecv.Value
                , "", "", "", "",
                "", "", context.s_UserName, context.s_DepartName, "", "0.00", "");
            }
            else
            {
                ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, txtCustName.Text, "高龄卡售卡", hidAccRecv.Value
                , "", "", "", "",
                "", "", context.s_UserName, context.s_DepartName, "", "0.00", "");
            }
            // 准备收据打印数据
            //ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, context.s_UserName, hidAccRecv.Value);

            btnPrintSJ.Enabled = true;

            // 学生卡需要写类型及日期

            if (hidTradeTypeCode.Value == "31")
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "DealCardTypeAndVerifyDate();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "startMonthlyInfo();", true);
            }
            selMonthlyCardType.SelectedValue = "";
            btnReadCard.Enabled = false;
        }
    }
}
