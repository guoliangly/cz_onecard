﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.UserManager;
using TM;


//停车收费-开通
public partial class ASP_AddtionalService_AS_ParkNew : Master.FrontMaster
{
    // 页面加载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置可读属性
        setReadOnly(txtCardBalance, txtStartDate);

        // 设置焦点以及按键事件
        txtRealRecv.Attributes["onfocus"] = "this.select();";
        txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this);";
        //dpExpire.Attributes["onchange"] = "ChangeExpire();";
        // 初始化证件类型
        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);

   
        txtRealRecv.Text = 360.ToString("0");
        hidAccRecv.Value = 360.ToString("n");
        ServiceFee.Text = 360.ToString("0");
        Total.Text = 360.ToString("0");
        txtKaiTongDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        lblKaiTongEndDate.Text = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyy-MM-dd");
    }

    //校验是否是已售出龙城通卡
    protected void readCardTypeAndState(string cardNo, Label labCardType)
    {
        DataTable data = ASHelper.callQuery(context,
            "ReadCardTypeAndSateByCardNo", cardNo);

        if (data == null || data.Rows.Count == 0)
        {
            context.AddError("未在用户卡库存表中查询出卡片信息或者卡片类型没有配置");
            return;
        }

        labCardType.Text = "" + data.Rows[0].ItemArray[1];
    }

    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        hidISDBRead.Value = "";
        GetInfo();
    }

    //获取信息
    protected void GetInfo()
    {
        btnPrintPZ.Enabled = false;
        hidWarning.Value = "";
       
        // 读取卡片类型
        readCardTypeAndState(txtCardNo.Text.Trim(), labCardType);
        //if (labCardType.Text != "龙城通卡")
        //{
        //    context.AddError("只有龙城通卡可以开通停车收费");
        //    return;
        //}

        // 读取帐户相关信息 SP_AccCheck
        checkAccountInfo(txtCardNo.Text.Trim());

        // 读取客户资料
        readCustInfo(txtCardNo.Text.Trim(), txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        string startDate = string.Empty;
        string endDate = string.Empty;
        // 判断卡片内停车功能是否到期，否则提示无需开通
        checkParkFeature(out startDate,out endDate);

        // 读取当前停车结束日期
        labEndDate.Text = endDate;
        txtStartDate.Text = startDate;
        //得到九竹序列号
        DataTable data = ASHelper.callQuery(context,
            "GetLinkNo", txtCardNo.Text.Trim());
        if (data != null && data.Rows.Count > 0)
        {
            labLinkNo.Text = data.Rows[0]["LinkNo"].ToString();
            txtLinkNo.Text = data.Rows[0]["LinkNo"].ToString();
            if (endDate != string.Empty)
            {
                txtKaiTongDate.Text =DateTime.ParseExact( endDate,"yyyy-MM-dd",null).AddDays(1).ToString("yyyy-MM-dd");
            }
           
        }
        //DataTable data = ASHelper.callQuery(context, "CheckParkNew", txtCardNo.Text);
        //if (data != null && data.Rows.Count > 0)
        //{
        //    hidWarning.Value += "当前卡片是由老卡换卡售出，<br>且老卡已经开通停车收费。<br><br>老卡号:<span class='red'>"
        //        + data.Rows[0].ItemArray[0] + "</span>。<br>";
        //}

        if (context.hasError()) return;

        btnSubmit.Enabled = hidWarning.Value.Length == 0;

        // 如果存在警告信息，则提示是否继续开卡

        if (hidWarning.Value.Length > 0)
        {
            hidWarning.Value += Server.HtmlDecode("<br>是否继续开通?");
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "warnScript",
                "warnConfirm();", true);
        }
    }

    //读数据库
    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        hidISDBRead.Value = "True";
        GetInfo();
    }

    // 对话框确认按钮处理

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")               // 是否继续
        {
            hidWarning.Value = "";                       // 清除消息内容
            btnSubmit.Enabled = true;
        }
        else if (hidWarning.Value == "submit")
        {
            hidWarning.Value = "";                       // 清除消息内容
            btnDBRead_Click(sender, e);
        }
        else
        {
            hidWarning.Value = "";                       // 清除消息内容
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }


    // 检查停车收费状态
    // 输出开始时间和结束时间 -> endDate
    void checkParkFeature(out string startDate, out string endDate)
    {
        startDate = string.Empty;
        endDate = string.Empty;
        string[] var = new string[2] { txtCardNo.Text.Trim(), "PARK" };
        // 按照卡号读取结束日期
        DataTable data = ASHelper.callQuery(context, "AddtionalSVCEndDate", var);
        if (data.Rows.Count == 0)
        {
            return;
        }
        Object[] row = data.Rows[0].ItemArray;
        endDate = ((DateTime)data.Rows[0][0]).ToString("yyyy-MM-dd");
        startDate = ((DateTime)data.Rows[0][1]).ToString("yyyy-MM-dd");
        if (((DateTime)row[0]).AddDays(1) < DateTime.Now)
        {
            return; // 到期，允许续费
        }
        var = new string[2] { txtCardNo.Text.Trim(), "P1" };
        DataTable openDayDt = ASHelper.callQuery(context, "CheckAddtionalSVCOpenDay", var);

        if (openDayDt != null && openDayDt.Rows.Count > 0)
        {
            DateTime dt = (DateTime)openDayDt.Rows[0].ItemArray[0];
            if (dt.Year == DateTime.Today.Year
                && dt.Month == DateTime.Today.Month
                && dt.Day == DateTime.Today.Day)
            {
                context.AddError("停车收费当日开通，不允许再次开通!");
            }
        }

        hidWarning.Value += Server.HtmlDecode(
            "卡片已开通停车收费，且未到期;<br>停车到期时间为:<span class='red'>" + ((DateTime)row[0]).ToShortDateString() + "</span><br>");
    }

    // 提交校验
    private void submitValidate()
    {
        //校验九竹管理系统序列号
        if (string.IsNullOrEmpty(txtLinkNo.Text.Trim()))
        {
            context.AddError("A10001A021：请填写九竹管理系统系列号", txtLinkNo);
        }

        if (txtKaiTongDate.Text.Trim() == "")
        {
            context.AddError("开通日期不能为空");
        }
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        submitValidate();

        //读取审核员工信息
        string checkStaff = "";
        string checkDepart = "";
        if (hidISDBRead.Value == "True")
        {
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            ddoTD_M_INSIDESTAFFIn.OPERCARDNO = hiddenCheck.Value;
            TD_M_INSIDESTAFFTDO[] ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A10001B030: 请校验审核员工卡");
                return;
            }
            checkStaff = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            checkDepart = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }
        
        if (context.hasError()) return;

        // 调用附加业务功能开通存储过过程
        context.SPOpen();
        context.AddField("p_cardNo").Value = txtCardNo.Text.Trim();
        context.AddField("p_service").Value = "PARK";
        context.AddField("p_tradeFee").Value = (int)(Double.Parse(hidAccRecv.Value) * 100);
        context.AddField("p_operCardNo").Value = context.s_CardID; // 操作员卡
        context.AddField("p_openDate").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).ToString("yyyyMMdd");
        context.AddField("p_end").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyyMMdd");
        context.AddField("p_linkno").Value = txtLinkNo.Text;
        context.AddField("p_tradetypecode").Value = "P1";
        context.AddField("p_CHECKSTAFFNO").Value = checkStaff;
        context.AddField("p_CHECKDEPARTNO").Value = checkDepart;
       
        // 执行存储过程
        bool ok = context.ExecuteSP("SP_AS_AddtionalSVCOpen");
        btnSubmit.Enabled = false;

        // 执行成功，显示成功消息
        if (ok)
        {
            AddMessage("D10001B010: 停车收费已经开通");

            btnPrintPZ.Enabled = true;
            ASHelper.preparePingZheng(ptnPingZheng, txtCardNo.Text.Trim(), txtCustName.Text, "停车收费开通", "0.00"
                , "0.00", "", txtPaperNo.Text, "0.00", "0.00", hidAccRecv.Value, context.s_UserName,
                context.s_DepartName,
                selPaperType.SelectedValue == "" ? "" : selPaperType.SelectedItem.Text, "0.00", hidAccRecv.Value);
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }
        }
    }



    protected void dpExpire_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtKaiTongDate.Text.Trim() != "")
        {
            ServiceFee.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            Total.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            txtRealRecv.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            hidAccRecv.Value = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("n");
            lblKaiTongEndDate.Text= DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32( dpExpire.SelectedValue)).AddDays(-1).ToString("yyyy-MM-dd");
        }
    }
}