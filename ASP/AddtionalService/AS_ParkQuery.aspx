﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AS_ParkQuery.aspx.cs" Inherits="ASP_AddtionalService_AS_ParkQuery" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>停车收费查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../js/print.js"></script>

</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            停车收费->停车收费查询
        </div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            AsyncPostBackTimeout="600" EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="inline">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="15%">
                                <div class="Condition">
                                    查询卡号</div>
                            </td>
                            <td width="8%">
                                证件号码:
                            </td>
                            <td width="10%">
                                <asp:TextBox ID="txtPaperNo" CssClass="inputmid" runat="server" MaxLength="20" />
                            </td>
                            <td width="8%">
                                &nbsp;车位号:
                            </td>
                            <td width="10%">
                                <asp:TextBox ID="txtPlaceCode" CssClass="input" runat="server" MaxLength="10" />
                            </td>
                            <td width="12%">
                                <div align="left">
                                    &nbsp;&nbsp;<asp:Button ID="Button1" CssClass="button1" runat="server" Text="查询卡号" OnClick="btnQueryCard_Click" /></div>
                            </td>
                            <td width="10%">
                                卡号:
                            </td>
                            <td width="15%">
                                <asp:DropDownList ID="selCardNo" AutoPostBack="true" OnSelectedIndexChanged="selCardNo_SelectedIndexChanged"
                                    runat="server" CssClass="inputmid">
                                </asp:DropDownList>
                            </td>
                            <td  width="20%">
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
                <div class="con">
                    <div class="card">
                        车位信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="11%">
                                    <div align="right">
                                        用户卡号:</div>
                                </td>
                                <td width="15%">
                                    <asp:TextBox ID="txtCardno" CssClass="input" MaxLength="16" runat="server"></asp:TextBox></td>
                                <td width="11%">
                                    <div align="right">
                                        卡片类型:</div>
                                </td>
                                <td width="15%">
                                    <asp:TextBox ID="LabCardtype" CssClass="labeltext" runat="server"></asp:TextBox></td>
                                <td width="11%">
                                    <div align="right">
                                        停车场:</div>
                                </td>
                                <td width="15%">
                                    <asp:TextBox ID="txtParkPlace" CssClass="labeltext" runat="server"></asp:TextBox></td>
                                <td width="15%">
                                    <asp:Button ID="btnReadCard" CssClass="button1" runat="server" Text="读卡" OnClientClick="return ReadCardInfo()"
                                        OnClick="btnReadCard_Click" /></td>
                            </tr>
                            <tr>
                                <td width="11%">
                                    <div align="right">
                                        启用日期:</div>
                                </td>
                                <td width="14%">
                                    <asp:TextBox ID="sDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox></td>
                                <td width="11%">
                                    <div align="right">
                                        结束日期:</div>
                                </td>
                                <td width="14%">
                                    <asp:TextBox ID="eDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox></td>
                                <td width="11%">
                                    <div align="right">
                                        区域:</div>
                                </td>
                                <td width="16%">
                                    <asp:TextBox ID="txtArea" CssClass="labeltext" runat="server" Text=""></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 24px">
                                    <div align="right">
                                        车位号:</div>
                                </td>
                                <td style="height: 24px">
                                    <asp:Label ID="labPlaceCode" runat="server"></asp:Label></td>
                                <td style="height: 24px">
                                    <div align="right">
                                        车牌号:</div>
                                </td>
                                <td style="height: 24px">
                                    <asp:Label ID="labPlateNo" runat="server"></asp:Label></td>
                                <td style="height: 24px">
                                    <div align="right">
                                        九竹系统序列号:</div>
                                </td>
                                <td style="height: 24px">
                                    <asp:Label ID="labSequence" runat="server"></asp:Label></td>
                                <td style="height: 24px">
                                    <asp:Button ID="btnDBread" CssClass="button1" runat="server" Text="读数据库" OnClick="btnDBread_Click" /></td>
                            </tr>
                            <asp:HiddenField ID="hiddenread" runat="server" />
                            <asp:HiddenField ID="hiddentxtCardno" runat="server" />
                            <asp:HiddenField ID="hiddentxtNCardno" runat="server" />
                            <asp:HiddenField ID="hiddenASn" runat="server" />
                            <asp:HiddenField ID="hiddenLabCardtype" runat="server" />
                            <asp:HiddenField ID="hiddensDate" runat="server" />
                            <asp:HiddenField ID="hiddeneDate" runat="server" />
                            <asp:HiddenField ID="hiddencMoney" runat="server" />
                            <asp:HiddenField ID="hiddentradeno" runat="server" />
                            <asp:HiddenField ID="hidTradeno" runat="server" />
                            <asp:HiddenField ID="hidTradeMoney" runat="server" />
                            <asp:HiddenField ID="hidTradeType" runat="server" />
                            <asp:HiddenField ID="hidTradeTerm" runat="server" />
                            <asp:HiddenField ID="hidTradeDate" runat="server" />
                            <asp:HiddenField ID="hidTradeTime" runat="server" />
                            <asp:HiddenField ID="hidread" runat="server" />                            
                        </table>
                    </div>
                    <div class="pip">
                        用户信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="11%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustName" runat="server" Text=""></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td width="8%">
                                    <asp:Label ID="Papertype" runat="server" Text=""></asp:Label></td>
                                <td width="8%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td width="23%" colspan="3">
                                    <asp:Label ID="Paperno" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custsex" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custphone" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custpost" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                    电子邮件:</td>
                                <td>
                                    <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label></td>
                                <td valign="top">
                                    <div align="right">
                                        备注:</div>
                                </td>
                                <td colspan="5">
                                    <asp:Label ID="Remark" runat="server" Text=""></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="con">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="16%">
                                <div class="Condition">
                                    查询条件</div>
                            </td>
                            <td width="24%">
                                起始日期:
                                <asp:TextBox ID="beginDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="24%">
                                终止日期:
                                <asp:TextBox ID="endDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="18%">
                                <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" />
                            </td>
                            <td width="18%">
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
                <div class="con">
                    <div class="jieguo">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="25%">
                                    <div class="Condition">
                                        查询结果</div>
                                </td>
                                <td width="25%">
                                    &nbsp;</td>
                                <td width="25%">
                                    &nbsp;</td>
                                <td width="25%">
                                    
                                   
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="kuang5">
                        <asp:GridView ID="lvwQuery" runat="server" Width="100%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                            AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast"
                            PageSize="200" AllowPaging="True" OnPageIndexChanging="lvwQuery_Page" PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top" AutoGenerateColumns="true" OnRowDataBound="lvwQuery_RowDataBound"
                            EmptyDataText="没有数据记录!">
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
