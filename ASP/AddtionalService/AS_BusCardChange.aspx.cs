﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM;
using PDO.AdditionalService;
using Common;
using System.Data;

public partial class ASP_AddtionalService_AS_BusCardChange : Master.FrontMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置可读属性

        if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

        setReadOnly(txtCardBalance, txtStartDate, txtEndDate, txtCardState);

        // 设置焦点以及按键事件
        txtRealRecv.Attributes["onfocus"] = "this.select();";
        txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this);";

        // 初始化证件类型

        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);

        // 初始化行政区域
        selMonthlyCardDistrict.Items.Add(new ListItem("---请选择---", ""));

        //初始化公交类型
        labMonthlyCardType.Items.Add(new ListItem("---请选择---", ""));
        labMonthlyCardType.Items.Add(new ListItem("人才卡", "02"));
        labMonthlyCardType.Items.Add(new ListItem("高龄卡", "24"));

        // 初始化费用列表

        initGridView(0, 0);
    }

    // 初始化费用项gridview
    private void initGridView(int cardDeposit, int eldCardDeposit)
    {
        string type = labMonthlyCardType.SelectedValue;

        hidTradeTypeCode.Value
                = type == "02" ? "7A" // 普通转人才
                : type == "24" ? "76" // 老人转高龄
                : "";

        initFeeItem(gvResult, hidTradeTypeCode.Value, hidDeposit,
            hidCardPost, hidOtherFee, hidAccRecv,
            txtRealRecv, false, 0, 0);
    }

    // 刷新行政区域选择
    protected void selMonthlyCardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 初始化行政区域
        ASHelper.SelectDistricts(context,
            selMonthlyCardDistrict, labMonthlyCardType.SelectedValue);

        initGridView(0, 0);
    }

    // 读取卡片相关信息
    private void readCardInfo()
    {
        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        // 读取卡片状态

        ASHelper.readCardState(context, txtCardNo.Text, txtCardState);

        if (txtCardType.Value == "00" || txtCardType.Value == "03")
        {

        }
        else
        {
            context.AddError("公交类型错误，无法进行类型变更");
            return;
        }

        labBusCardType.Text = txtCardType.Value == "00" ? "普通卡"
                            : txtCardType.Value == "03" ? "老人卡"
                            : "";

        // 读取客户资料
        readCustInfo(txtCardNo.Text, txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        if (context.hasError()) return;

        // 读取卡片押金
        //decimal cardDeposit = 0;

        //DataTable data = ASHelper.callQuery(context, "CardDeposit", txtCardNo.Text);
        //if (data.Rows.Count > 0)
        //{
        //    cardDeposit = (decimal)data.Rows[0].ItemArray[0];
        //}

        //// 查询高龄卡开卡押金

        //data = ASHelper.callQuery(context, "OldCardDeposit");
        //decimal eldCardDeposit = 0;
        //if (data.Rows.Count > 0)
        //{
        //    eldCardDeposit = (decimal)data.Rows[0].ItemArray[0];
        //}
        //else
        //{
        //    context.AddError("A005110003: 高龄卡押金费用项没有配置，无法初始化高龄卡押金");
        //}

        initGridView(0, 0);
    }

    // 校验月票卡资料

    private void CheckMonthlyCard()
    {
        // 校验月票卡应用类型

        DataTable data = ASHelper.callQuery(context, "CardAppType", txtCardNo.Text);
        if (data.Rows.Count != 1)
        {
            labMonthlyCardType.Text = "非月票卡";
            context.AddError("A005110001: 当前卡片不是月票卡");
            return;
        }
        string type = (string)(data.Rows[0].ItemArray[0]);
        labMonthlyCardType.Text
            = type == "01" ? "学生月票卡"
            : type == "02" ? "老人月票卡"
            : type == "03" ? "高龄卡"
            : "未知月票卡";

        if (type != "02")
        {
            context.AddError("A005110002: 当前卡片不是老人月票卡");
            return;
        }
    }

    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnPrintPZ.Enabled = false;

        // 校验帐户信息
        checkAccountInfo(txtCardNo.Text);

        // 查询卡片是否是月票卡或高龄卡
        //CheckMonthlyCard();

        // 读取卡片相关信息
        readCardInfo();

        txtStartDate.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
        txtEndDate.Text = hiddeneDate.Value.Substring(0, 4) + "-" + hiddeneDate.Value.Substring(4, 2) + "-" + hiddeneDate.Value.Substring(6, 2);
        txtCardBalance.Text = ((Convert.ToDecimal(hiddencMoney.Value)) / 100).ToString("0.00");

        btnSubmit.Enabled = !context.hasError();
    }

    // 确认对话框确认处理

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")    // 是否继续
        {
            btnSubmit.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess") // 写卡成功
        {
            clearCustInfo(labMonthlyCardType, selMonthlyCardDistrict,
                txtCardNo, txtCustName, txtCustBirth, selPaperType, txtPaperNo,
                selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);
            AddMessage("D005110002: 公交卡类型变更前台写卡成功");
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("A00511C001: 公交卡类型变更前台写卡失败");
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }

        hidWarning.Value = ""; // 清除警告信息
    }

    // 提交前校验

    private void submitValidate()
    {
        // 客户信息校验
        custInfoValidate(txtCustName, txtCustBirth,
            selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
            txtCustAddr, txtEmail, txtRemark);

        Validation valid = new Validation(context);
        //valid.check(selMonthlyCardDistrict.SelectedValue != "", "A005090002: 行政区域必须选择", selMonthlyCardDistrict);
        //valid.check(selCustSex.SelectedValue != "", "A005090003: 性别必须选择", selCustSex);
    }

    // 老人卡到高领卡升级处理

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 校验输入
        submitValidate();
        if (context.hasError()) return;

        if ((txtCardType.Value == "00" && selMonthlyCardDistrict.SelectedValue != "02") ||
            (txtCardType.Value == "03" && selMonthlyCardDistrict.SelectedValue != "24"))
        {
            context.AddError("公交类型选择错误，不能变更");
            return;
        }

        // 调用“公交类型变更”存储过程

        SP_AS_BusCardChangePDO pdo = new SP_AS_BusCardChangePDO();
        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, hiddenAsn.Value);
        pdo.cardNo = txtCardNo.Text;

        pdo.deposit = (int)(Double.Parse(hidDeposit.Value) * 100);
        pdo.cardCost = (int)(Double.Parse(hidCardPost.Value) * 100);
        pdo.otherFee = (int)(Double.Parse(hidOtherFee.Value) * 100);

        pdo.cardTradeNo = hiddentradeno.Value;
        pdo.asn = hiddenAsn.Value.Substring(4, 16);

        pdo.operCardNo = context.s_CardID;
        pdo.terminalNo = "112233445566";   // 目前固定写成112233445566

        pdo.custName = txtCustName.Text;
        pdo.custBirth = txtCustBirth.Text;
        pdo.paperType = selPaperType.SelectedValue;
        pdo.paperNo = txtPaperNo.Text;
        pdo.custSex = selCustSex.SelectedValue;
        pdo.custPhone = txtCustPhone.Text;
        pdo.custPost = txtCustPost.Text;
        pdo.custAddr = txtCustAddr.Text;
        pdo.custEmail = txtEmail.Text;
        pdo.remark = txtRemark.Text;

        pdo.assignedArea = selMonthlyCardDistrict.SelectedValue;
        hidMonthlyFlag.Value = pdo.assignedArea;

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);
        btnSubmit.Enabled = false;

        // 存储过程执行成功，返回成功消息

        if (ok)
        {

            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "modifyMonthlyInfo();", true);
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCardNo.Text, txtCustName.Text, "公交类型变更", "0.00"
                , hidAccRecv.Value, "", txtPaperNo.Text, "0.00", "0.00", hidAccRecv.Value, context.s_UserName,
                context.s_DepartName,
                selPaperType.SelectedValue == "" ? "" : selPaperType.SelectedItem.Text, "0.00", "0.00");
        }
    }
}
