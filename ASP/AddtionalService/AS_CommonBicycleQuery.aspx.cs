﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM;
using DataExchange;
using System.Xml;
using Common;
using TDO.CardManager;
using TDO.ResourceManager;
using TDO.BusinessCode;

public partial class ASP_AddtionalService_AS_CommonBicycleQuery : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 初始化证件类型
        txtFromDate.Text = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
        txtToDate.Text = DateTime.Now.ToString("yyyyMMdd");
    }

    //根据证件号码查询卡号
    protected void btnQueryCard_Click(object sender, EventArgs e)
    {
        //对输入证件号码进行检验

        if (!PaperNoValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();

        //TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        //ddoTF_F_CUSTOMERRECIn.PAPERNO = txtPaperNo.Text.Trim();

        //TF_F_CUSTOMERRECTDO[] ddoTF_F_CUSTOMERRECOutArr = (TF_F_CUSTOMERRECTDO[])tmTMTableModule.selByPKArr(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), "A001130001", "TF_F_CUSTOMERREC_PAPERNO", null);

        //ControlDeal.SelectBoxFill(selCardNo.Items, ddoTF_F_CUSTOMERRECOutArr, "PAPERNO", "", true);


        DataTable dt = SPHelper.callPBQuery(context, "QryCardno", txtPaperNo.Text.Trim());

        if (dt != null && dt.Rows.Count > 0)
        {
            GroupCardHelper.fill(selCardNo, dt, true);
        }
        else
        {
            context.AddError("A001003192：未查找到对应卡号");
        }

    }
    private Boolean PaperNoValidation()
    {
        //对证件号码进行非空、长度、英数字检验

        if (txtPaperNo.Text.Trim() == "")
            context.AddError("A001001121", txtPaperNo);
        else if (!Validation.isCharNum(txtPaperNo.Text.Trim()))
            context.AddError("A001001122", txtPaperNo);
        else if (Validation.strLen(txtPaperNo.Text.Trim()) > 20)
            context.AddError("A001001123", txtPaperNo);

        return !(context.hasError());
    }

    //选择卡号后，将卡号放入查询部分中
    protected void selCardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCardno.Text = selCardNo.SelectedValue;
    }

    //读卡
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        //校验卡号是否正确
        if (txtCardno.Text.Trim() == "")
        {
            context.AddError("A001004113", txtCardno);
        }
        else
        {
            if (!Validation.isNum(txtCardno.Text))
                context.AddError("A001004115", txtCardno);
        }

        if (context.hasError()) return;

        btnDBReadCardProcess();


        if (context.hasError()) return;
        btnSubmit.Enabled = true;

    }

    protected void btnDBread_Click(object sender, EventArgs e)
    {
        
        //校验卡号是否正确
        if (txtCardno.Text.Trim() == "")
        {
            context.AddError("A001004113", txtCardno);
        }
        else
        {
            if (!Validation.isNum(txtCardno.Text))
                context.AddError("A001004115", txtCardno);
        }

        if (context.hasError()) return;

        btnDBReadCardProcess();
    }


    protected void btnDBReadCardProcess()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001107112");
            return;
        }



        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }
        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTL_R_ICUSEROut.CARDTYPECODE;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);


        //从资源状态编码表中读取数据
        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTD_M_RESOURCESTATEOut == null)
            RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);



        //性别显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else CustBirthday.Text = Bdate;
        }
        else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

        //证件类型显示
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

        hiddentxtCardno.Value = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);


       
        


        //给页面显示项赋值
        LabAsn.Text = ddoTL_R_ICUSEROut.ASN;
        LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
 
        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardno.Text);
        //判断是否开通龙人自行车
        if (!openFunc.List.Contains("龙人公共自行车"))
        {
            context.AddError("A0BK000002:此卡未开通龙人自行车功能");
        }

        DataTable data = ASHelper.callQuery(context, "QureyBikeInfo", txtCardno.Text);
        if (data.Rows.Count > 0)
        {
            txtOpenDate.Text = Convert.ToDateTime(data.Rows[0][1].ToString()).ToString("yyyy-MM-dd");
        }


        //调用接口获取自行车状态
        //重新调用接口
        DataTable dataBike = ASHelper.callQuery(context, "BicycleStateQuery", txtCardno.Text.Trim(), ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE, Paperno.Text);

        //批量后台调用接口
        List<SyncBikeRequest> syncRequest;
        DataBikeExchangeHelp.BikeStateQueryForDataTable(dataBike, out syncRequest);
        List<SyncBikeRequest> syncResponse;
        if (syncRequest.Count != 0)
        {
            bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
            if (!succ)
            {
                context.AddError("调用状态查询接口接口失败!" + syncResponse[0].SyncErrInfo);

            }
            else
            {
                context.AddMessage("调用状态查询接口成功!");
                txtBikeState.Text = ((BikeStateQuerySync)syncResponse[0]).STATE;

            }
        }
        else
        { 
            
        }
    }


    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001008100", txtCardno);
        else
        {
            if ((txtCardno.Text.Trim()).Length != 16)
                context.AddError("A001008101", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001008102", txtCardno);
        }

        return !(context.hasError());
    }

    protected void QueryValid()
    {
        Validation valid = new Validation(context);



        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1)
        {
            context.AddError("开始时间不能为空", txtFromDate);
        }

        if (b2)
        {
            context.AddError("结束时间不能为空", txtToDate);
        }

        if (!b1)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }

        if (!b2)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
            valid.check(toDate.Value.AddDays( - 30) <= fromDate.Value, "开始日期和结束日期范围在一个月");

        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        QueryValid();

        if (context.hasError()) return;

        DataTable dt = new DataTable();
        string strFromDate = txtFromDate.Text.Trim();
        string strToDate = txtToDate.Text.Trim();

        dt = CallCloseBicycleAsync(txtCardno.Text.Trim(), strFromDate, strToDate,selQueryType.SelectedValue);

        if (context.hasError())
        {
            //context.AddMessage("M013001004:调用公共自行车借还车查询接口失败！");
            return;
        }

        if (dt == null || dt.Rows.Count == 0)
        {
            AddMessage("查询结果为空");
            return;
        }

        gvResult.DataSource = dt;
        gvResult.DataBind();
    }


    // 公共自行车借还车记录绑定

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Cells[1].Text = DateTime.ParseExact(e.Row.Cells[1].Text, "yyyyMMddHHmmss", null).ToString();
        //    e.Row.Cells[4].Text = DateTime.ParseExact(e.Row.Cells[4].Text, "yyyyMMddHHmmss", null).ToString();
        //}
    }

    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        gvResult.DataSource = CallCloseBicycleAsync(txtCardno.Text.Trim(), txtFromDate.Text.Trim(),txtToDate.Text.Trim(),selQueryType.SelectedValue);
        gvResult.DataBind();
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    private DataTable CallCloseBicycleAsync(string cardno, string fromdate, string todate,string querytype)
    {
        DataTable dt = new DataTable();
        DataColumn dc = new DataColumn("LEASETIME", typeof(string));
        dt.Columns.Add(dc);
        dc = new DataColumn("LEASESHED", typeof(string));
        dt.Columns.Add(dc);
        dc = new DataColumn("LOCATIVEID", typeof(string));
        dt.Columns.Add(dc);
        dc = new DataColumn("RTTIME", typeof(string));
        dt.Columns.Add(dc);
        dc = new DataColumn("RTSHED", typeof(string));
        dt.Columns.Add(dc);
        dc = new DataColumn("RTLOCATIVEID", typeof(string));
        dt.Columns.Add(dc);
        //重新调用接口
        DataTable data = ASHelper.callQuery(context, "QueryBicycleForTradeList", cardno, fromdate, todate,querytype);

        //批量后台调用接口
        List<SyncBikeRequest> syncRequest;
        DataBikeExchangeHelp.ParseQueryForTradeList(data, out syncRequest);
        List<SyncBikeRequest> syncResponse;
        if (syncRequest.Count != 0)
        {
            bool succ = DataBikeExchangeHelp.Sync(syncRequest, out syncResponse);
            if (!succ)
            {
                context.AddError("调用接口失败!" + syncResponse[0].SyncErrInfo);
            }
            else
            {
                


                if (syncResponse.Count > 0)
                {
                    foreach (AppServiceRecordDetail asrd in ((QueryForTradeListSync)syncResponse[0]).AppServiceRecordList)
                    {
                        DataRow dr = dt.NewRow();
                        dr["LEASETIME"] = asrd.LeaseTime;
                        dr["LEASESHED"] = asrd.LeaseShed;
                        dr["LOCATIVEID"] = asrd.LocativeID;
                        dr["RTTIME"] = asrd.RtTime;
                        dr["RTSHED"] = asrd.RtShed;
                        dr["RTLOCATIVEID"] = asrd.RtLocativeID;
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    context.AddMessage("调用接口成功，未查询出数据!");
                }
                
            }
        }
        return dt;
    }

    private string GetErrorMsg()
    {
        string msg = "";
        ArrayList errorMessages = context.ErrorMessage;
        for (int index = 0; index < errorMessages.Count; index++)
        {
            if (index > 0)
                msg += "|";

            String error = errorMessages[index].ToString();
            int start = error.IndexOf(":");
            if (start > 0)
            {
                error = error.Substring(start + 1, error.Length - start - 1);
            }

            msg += error;
        }

        return msg;
    }
}