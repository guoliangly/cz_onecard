﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AS_ParkFiApproval.aspx.cs"
    Inherits="ASP_AddtionalService_AS_ParkFiApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>停车收费财审</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../js/myext.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
        停车收费->停车收费财审
    </div>
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" runat="server" />

    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:BulletedList ID="bulMsgShow" runat="server" />

            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

            <div class="con">
                <div class="card">
                    查询</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="12%">
                            </td>
                            <td width="12%">
                                <asp:DropDownList ID="ddlExamState" CssClass="input" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlExamState_SelectedIndexChanged">
                                    <asp:ListItem Text="0: 待审核" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1: 审核通过" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2: 审核作废" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="36%">
                            </td>
                            <td width="12%">
                            </td>
                            <td width="12%" align="right">
                            </td>
                            <td width="12%">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="jieguo">
                    查询结果</div>
                <div class="kuang5">
                    <div style="height: 370px; overflow: auto;">
                        <asp:GridView ID="gvResult" runat="server" Width="100%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                            FooterStyle-CssClass="tabcon" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="false" PageSize="50" AllowPaging="true" OnPageIndexChanging="gvResult_Page">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ItemCheckBox" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TRADEID" HeaderText="流水号" />
                                <asp:BoundField DataField="PNAME" HeaderText="停车场" />
                                <asp:BoundField DataField="AREANAME" HeaderText="区域" />
                                <asp:BoundField DataField="PLACECODE" HeaderText="车位号" />
                                <asp:BoundField DataField="PLATENO" HeaderText="车牌号" />
                                <asp:BoundField DataField="CARDNO" HeaderText="卡号" />
                                <asp:BoundField DataField="CARDTYPE" HeaderText="卡片类别" />
                                <asp:BoundField DataField="CUSTNAME" HeaderText="姓名" />
                                <asp:BoundField DataField="OPENDATE" HeaderText="开通日期" DataFormatString="{0:yyyy-MM-dd}" />
                                <asp:BoundField DataField="ENDDATE" HeaderText="结束日期" DataFormatString="{0:yyyy-MM-dd}" />
                                <asp:BoundField DataField="EXAMTIME" HeaderText="财审时间" DataFormatString="{0:yyyy-MM-dd hh:mm:ss}" />
                            </Columns>
                            <EmptyDataTemplate>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                    <tr class="tabbt">
                                        <td>
                                            <input type="checkbox" />
                                        </td>
                                        <td>
                                            流水号
                                        </td>
                                        <td>
                                            卡号
                                        </td>
                                        <td>
                                            卡片类别
                                        </td>
                                        <td>
                                            姓名
                                        </td>
                                        <td>
                                            证件号码
                                        </td>
                                        <td>
                                            开通日期
                                        </td>
                                        <td>
                                            结束日期
                                        </td>
                                        <td>
                                            停车场
                                        </td>
                                        <td>
                                            区域
                                        </td>
                                        <td>
                                            车位号
                                        </td>
                                        <td>
                                            车牌号
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="footall">
            </div>
            <div class="btns">
                <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnPass" runat="server" Text="通过" CssClass="button1" OnClick="btnPass_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="作废" CssClass="button1" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
