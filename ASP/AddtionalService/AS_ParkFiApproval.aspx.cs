﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Common;

/// <summary>
/// 附加业务-停车收费查询
/// </summary>
public partial class ASP_AddtionalService_AS_ParkFiApproval : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        gvResult.DataBind();

        CreateGridViewData();
    }

    protected void gvResult_Page(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        CreateGridViewData();
    }

    /// <summary>
    /// 创建gridview数据
    /// </summary>
    private void CreateGridViewData()
    {
        string strSql = "";
        switch (ddlExamState.SelectedValue)
        {
            case "0": strSql = "GetParklotsFiApproval"; break;
            case "1": strSql = "GetParklotsFiApprovalPass"; break;
            case "2": strSql = "GetParklotsFiApprovalCancel"; break;
        }
        // 从充值总量台帐表中查询信息
        DataTable data = ASHelper.callQuery(context, strSql);
        UserCardHelper.resetData(gvResult, data);

        bool enabled = (data.Rows.Count > 0);
    }

    /// <summary>
    /// 通过按钮点击事件
    /// </summary>
    protected void btnPass_Click(object sender, EventArgs e)
    {
        //选择回收的记录入临时表
        if (!RecordIntoTmp()) return;

        context.SPOpen();
        context.AddField("p_sessionId").Value = Session.SessionID;
        context.AddField("p_stateCode").Value = "1";
        bool ok = context.ExecuteSP("SP_AS_PakingLotsApproval");

        if (ok)
        {
            AddMessage("审核通过成功");
            //更新列表
            CreateGridViewData();
        }
    }
    /// <summary>
    /// 作废按钮点击事件
    /// </summary>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //选择回收的记录入临时表
        if (!RecordIntoTmp()) return;

        //调用审核作废存储过程
        context.SPOpen();
        context.AddField("p_sessionId").Value = Session.SessionID;
        context.AddField("p_stateCode").Value = "2";
        bool ok = context.ExecuteSP("SP_AS_PakingLotsApproval");

        if (ok)
        {
            AddMessage("审核作废成功");
            //更新列表
            CreateGridViewData();
        }
    }

    /// <summary>
    /// 选中的记录入临时表
    /// </summary>
    /// <returns>入临时表成功返回true，否则返回false</returns>
    private bool RecordIntoTmp()
    {
        //清空临时表
        clearTempTable();

        //记录入临时表
        context.DBOpen("Insert");

        int count = 0;
        //循环将选中行入临时表
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (cb != null && cb.Checked)
            {
                ++count;
                context.ExecuteNonQuery("insert into TMP_AS_PARKINGLOTS (SessionId,tradeid) values('" +
                        Session.SessionID + "', '" + gvr.Cells[1].Text + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误
        if (count <= 0)
        {
            context.AddError("没有选中任何行");
            return false;
        }

        return true;
    }
    /// <summary>
    /// 清空临时表
    /// </summary>
    private void clearTempTable()
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_AS_PARKINGLOTS where SessionId='"
            + Session.SessionID + "'");
        context.DBCommit();
    }

    #region 全选列表
    /// <summary>
    /// 选择或取消所有复选框
    /// </summary>
    protected void CheckAll(object sender, EventArgs e)
    {
        //全选信息记录

        CheckBox cbx = (CheckBox)sender;

        foreach (GridViewRow gvr in gvResult.Rows)
        {
            if (!gvr.Cells[1].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }
    #endregion

    /// <summary>
    /// 开通月份选择
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlExamState_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlExamState.SelectedValue != "0")
        {
            btnPass.Enabled = false;
            btnCancel.Enabled = false;

            foreach (GridViewRow gvr in gvResult.Rows)
            {
                if (!gvr.Cells[1].Enabled) continue;
                CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
                ch.Visible = false;
            }
        }
        else
        {
            btnPass.Enabled = true;
            btnCancel.Enabled = true;
        }
        CreateGridViewData();
    }
}
