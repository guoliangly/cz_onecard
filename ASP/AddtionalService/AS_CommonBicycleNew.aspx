﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AS_CommonBicycleNew.aspx.cs"
    Inherits="ASP_AddtionalService_AS_CommonBicycleNew" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>附加业务-公共自行车开通</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
    <div class="tb">
        附加业务->公共自行车开通
    </div>
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
            <asp:BulletedList ID="bulMsgShow" runat="server">
            </asp:BulletedList>
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <div class="con">
                <div class="card">
                    卡片信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    用户卡号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtCardno"  CssClass="input"  MaxLength="16" runat="server"></asp:TextBox>
                            </td>
                            <td width="10%">
                                <div align="right">
                                    卡序列号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="LabAsn" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddenAsn" runat="server" />
                            <td width="10%">
                                <div align="right">
                                    卡片类型:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="LabCardtype" CssClass="labeltext" Width="100" runat="server"></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddenLabCardtype" runat="server" />
                            <td width="10%">
                                <div align="right">
                                    卡片状态:</div>
                            </td>
                            <td width="10%">
                                <asp:TextBox ID="RESSTATE" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddentxtCardno" runat="server" />
                            <asp:HiddenField ID="hiddentradeno" runat="server" />
                            <asp:HiddenField ID="hidWarning" runat="server" />
                            <asp:HiddenField runat="server" ID="hidSupplyMoney" />
                            <asp:HiddenField runat="server" ID="hiddenSupply" />
                            <asp:HiddenField ID="hidLockBlackCardFlag" runat="server" />
                            <asp:HiddenField ID="hidFaPiaoFlag" runat="server" />
                            <asp:HiddenField ID="hidTradeDate" runat="server" />
                            <td width="12%" align="right">
                                <asp:Button ID="btnReadCard" CssClass="button1" runat="server" Text="读卡" OnClientClick="return readCardRecord()"
                                    OnClick="btnReadCard_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    启用日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="sDate" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddensDate" runat="server" />
                            <td>
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="eDate" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddeneDate" runat="server" />
                            <td>
                                <div align="right">
                                    卡内余额:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="cMoney" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox>
                            </td>
                            <asp:HiddenField ID="hiddencMoney" runat="server" />
                            <asp:HiddenField ID="hidSupplyFee" runat="server" />
                            
                            <asp:HiddenField runat="server" ID="hidoutTradeid" />
                            <asp:HiddenField ID="hidIsJiMing" runat="server" />

                            <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
                            <td>
                                <div align="right">
                                    售卡时间:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="sellTime" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    开通功能:</div>
                            </td>
                            <td colspan="7">
                                <aspControls:OpenFunc ID="openFunc" runat="server" />
                            </td>
                            <td width="12%" align="right">
                               <asp:Button ID="btnDBRead" CssClass="button1" runat="server" Text="读数据库"
                                        OnClientClick="return warnCheck()" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="pip">
                    用户信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    用户姓名:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="CustName" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    出生日期:</div>
                            </td>
                            <td width="13%">
                            <asp:TextBox ID="txtCustbirth" CssClass="input" runat="server" maxlength="10"/>
                             <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtCustbirth" Format="yyyy-MM-dd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    证件类型:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="Papertype" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    证件号码:</div>
                            </td>
                            <td width="25%" colspan="3">
                                <asp:Label ID="Paperno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    用户性别:</div>
                            </td>
                            <td>
                         
                                <asp:Label ID="Custsex" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    联系电话:</div>
                            </td>
                            <td>
                               <asp:TextBox ID="txtCustphone" CssClass="input" maxlength="20" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    邮政编码:</div>
                            </td>
                            <td>
                               <asp:TextBox ID="txtCustpost" CssClass="input" maxlength="6" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    联系地址:</div>
                            </td>
                            <td colspan="3">
                               <asp:TextBox ID="txtCustaddr" CssClass="input" maxlength="50" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                电子邮件:
                            </td>
                            <td>
                               <asp:TextBox ID="txtEmail" CssClass="input" maxlength="30" runat="server"></asp:TextBox>
                            </td>
                            <td valign="top">
                                <div align="right">
                                    备注 :</div>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="Remark" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card">
                    开通信息</div>
                <div class="kuang5">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    自行车厂商:</div>
                            </td>
                            <td width="15%" style="white-space: nowrap">
                                <asp:DropDownList ID="ddlBikeCompany" CssClass="inputmid" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlBikeCompany_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td width="10%" align="right">
                            </td>
                            <td width="15%" style="white-space: nowrap">
                            </td>
                            <td width="9%">
                                <div align="right">
                                </div>
                            </td>
                            <td width="15%">
                            </td>
                            <td width="10%" align="right">
                            </td>
                            <td width="15%">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="basicinfo">
                <div class="money">
                    费用信息</div>
                <div class="kuang5">
                    <table cellspacing="0" border="1" style="width: 100%; border-collapse: collapse;"
                        id="gvResult" rules="all" class="tab1">
                        <tbody>
                            <tr class="tabbt">
                                <th scope="col">
                                    费用项目
                                </th>
                                <th scope="col">
                                    费用金额
                                </th>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    押金
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="DepositFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td style="width: 50%;">
                                    手续费
                                </td>
                                <td style="width: 50%;">
                                    0.00
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    服务费
                                </td>
                                <td style="width: 50%;">
                                    0.00
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td style="width: 50%;">
                                    合计应收
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="Total" runat="server" Text="0.00"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pipinfo">
                <div class="info">
                    收款信息</div>
                <div class="kuang5">
                    <div class="bigkuang" style="height: 104px">
                        <div class="left">
                            <img src="../../Images/show-sale.JPG" width="280px" height="96" /></div>
                        <div class="big2">
                            <table width="200" border="0" cellpadding="0" cellspacing="0" class="text25">
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" align="right">
                                        <label>
                                            本次实收:&nbsp;</label>
                                    </td>
                                    <td width="50%">
                                        <asp:TextBox ID="txtRealRecv" CssClass="inputshort" runat="server" MaxLength="9" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        本次应找:&nbsp;
                                    </td>
                                    <td>
                                        <div id="txtChanges">
                                            0.00</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footall">
            </div>
            <div class="btns">
                <table width="200" align="right" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false"
                                OnClientClick="printdiv('ptnPingZheng1')" />
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
                <asp:CheckBox ID="chkPingzheng" runat="server" Text="自动打印凭证" Checked="true" />
            </div>
            <asp:HiddenField ID="hidLockFlag" runat="server" />
                            <asp:HiddenField ID="HiddenField1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
