﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AS_CommonBicycleQuery.aspx.cs" 
    Inherits="ASP_AddtionalService_AS_CommonBicycleQuery" ValidateRequest="false"%>
    
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>公共自行车借还车查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
    
        <div class="tb">
		    附加业务->公共自行车借还车查询
	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->


             <div class="con">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="15%">
                                <div class="Condition">
                                    查询卡号</div>
                            </td>
                            <td width="10%">
                                证件号码:
                            </td>
                            <td width="10%">
                                <asp:TextBox ID="txtPaperNo" CssClass="inputmid" runat="server" MaxLength="20" />
                            </td>
                            <td width="20%">
                                <div align="left">
                                    &nbsp;&nbsp;<asp:Button ID="btnQueryCard" CssClass="button1" runat="server" Text="查询卡号" OnClick="btnQueryCard_Click" /></div>
                            </td>
                            <td width="10%">
                                卡号:
                            </td>
                            <td width="15%">
                                <asp:DropDownList ID="selCardNo" AutoPostBack="true" OnSelectedIndexChanged="selCardNo_SelectedIndexChanged"
                                    runat="server" CssClass="inputmid">
                                </asp:DropDownList>
                            </td>
                            <td  width="20%">
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
	        <div class="con">
                    <div class="card">
                        卡片信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="10%">
                                    <div align="right">
                                        用户卡号:</div>
                                </td>
                                <td width="13%">
                                    <asp:TextBox ID="txtCardno" CssClass="input" MaxLength="16" runat="server"></asp:TextBox></td>
                                <td width="10%">
                                    <div align="right">
                                        卡序列号:</div>
                                </td>
                                <td width="13%">
                                    <asp:TextBox ID="LabAsn" CssClass="labeltext" runat="server"></asp:TextBox></td>
                                <asp:HiddenField ID="hiddenAsn" runat="server" />
                                <td width="10%">
                                    <div align="right">
                                        卡片类型:</div>
                                </td>
                                <td width="13%">
                                    <asp:TextBox ID="LabCardtype" CssClass="labeltext" Width="100" runat="server"></asp:TextBox></td>
                                <asp:HiddenField ID="hiddenLabCardtype" runat="server" />
                                <td width="10%">
                                    <div align="right">
                                        卡片状态:</div>
                                </td>
                                <td width="10%">
                                    <asp:TextBox ID="RESSTATE" CssClass="labeltext" runat="server"></asp:TextBox></td>
                                <asp:HiddenField ID="hiddentxtCardno" runat="server" />
                                <asp:HiddenField ID="hiddentradeno" runat="server" />
                                <asp:HiddenField ID="hidWarning" runat="server" />
                                <asp:HiddenField runat="server" ID="hidSupplyMoney" />
                                <asp:HiddenField runat="server" ID="hiddenSupply" />
                                <asp:HiddenField ID="hidLockBlackCardFlag" runat="server" />
                                <asp:HiddenField ID="hidFaPiaoFlag" runat="server" />
                                <!--市民卡读取交易日期-->
                                 <asp:HiddenField ID="hidTradeDate" runat="server" />
                                <td width="12%" align="right">
                                     <asp:Button ID="btnReadCard" Text="读卡" CssClass="button1" runat="server" OnClientClick="return readCardNo();"
                                    OnClick="btnReadCard_Click" /></td>
                            </tr>
                           
                            <tr>
                                <td>
                                    <div align="right">
                                        开通功能:</div>
                                </td>
                                <td>
                                    <aspControls:OpenFunc ID="openFunc" runat="server" />
                                </td>

                                 <td>
                                    <div align="right">
                                        开通时间:</div>
                                </td>
                                 
                                <td>
                                    <asp:TextBox ID="txtOpenDate" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox></td>
                            
                            <td>
                                    <div align="right">
                                        自行车方状态:</div>
                                </td>

                                <td>
                                    <asp:TextBox ID="txtBikeState" CssClass="labeltext" Width="100" runat="server" Text=""></asp:TextBox></td>
                            
                                <td width="12%" align="right" colspan="3">
                                    <asp:Button ID="btnDBread" CssClass="button1" runat="server" Text="读数据库" OnClick="btnDBread_Click" />  </td>
                            </tr>
                        </table>
                    </div>
                    <div class="pip">
                        用户信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="9%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="13%">
                                    <asp:Label ID="CustName" runat="server" Text=""></asp:Label></td>
                                <td width="9%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td width="13%">
                                    <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label></td>
                                <td width="9%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td width="13%">
                                    <asp:Label ID="Papertype" runat="server" Text=""></asp:Label></td>
                                <td width="9%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td width="25%" colspan="3">
                                    <asp:Label ID="Paperno" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custsex" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custphone" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custpost" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                    电子邮件:</td>
                                <td>
                                    <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label></td>
                                <td valign="top">
                                    <div align="right">
                                        备注 :</div>
                                </td>
                                <td colspan="5">
                                    <asp:Label ID="Remark" runat="server" Text=""></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
				
				
				
				<div class="con">

	           <div class="card">查询</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
               
                   <tr>
                        <td><div align="right">开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">结束日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">借还车时间类型:</div></td>
                        <td>
                             <asp:DropDownList ID="selQueryType" runat="server" >
                            <asp:ListItem Value="0">还车时间</asp:ListItem>
                            <asp:ListItem Value="1">借车时间</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                   </tr>
                       <tr>
                        <td style="width:100px;"><div align="right">&nbsp;</div></td>
                        <td>
                            &nbsp;
                        </td>
                        <td><div align="right">&nbsp;</div></td>
                        <td>
                             &nbsp;
                        </td>
                        <td><div align="right">&nbsp;</div></td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="right">
                            <asp:Button ID="btnSubmit" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
               </table>
               
             </div>

            <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                    &nbsp;
					</td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                  <div id="gdtbfix" style="height:350px;overflow:auto;">
                    <asp:GridView ID="gvResult" runat="server"
                            Width = "95%"
                            CssClass="tab2"
                            HeaderStyle-CssClass="tabbt" 
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="false"
                            AllowPaging="True"
                            PageSize="200"
                            OnPageIndexChanging="gvResult_Page"
                            OnRowDataBound="gvResult_RowDataBound">
                            <Columns>
                                <asp:BoundField HeaderText="借车时间" DataField="LEASETIME" />
                                <asp:BoundField HeaderText="借车站点名称" DataField="LEASESHED" />
                                <asp:BoundField HeaderText="借车车桩号" DataField="LOCATIVEID" />
                                <asp:BoundField HeaderText="还车时间" DataField="RTTIME" />
                                <asp:BoundField HeaderText="还车站点名称" DataField="RTSHED" />
                                <asp:BoundField HeaderText="还车车桩号" DataField="RTLOCATIVEID" />
                            </Columns>
                    </asp:GridView>
                    
                </div>
              </div>
            </div>
            

            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
