﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.CardManager;
using TDO.UserManager;
using TM;
using Common;

/// <summary>
/// 附件业务-停车收费开通
/// </summary>
public partial class ASP_AddtionalService_AS_ParkOpen : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        InitPageControls();
        ASHelper.initCardTypeList(context, ddlCardType);
        ASHelper.initSexList(selCustsex);
        ddlCardType.SelectedValue = "0001";
    }

    private void InitPageControls()
    {
        //清除页面显示
        foreach (Control con in this.Page.Controls)
        {
            ClearControl(con);
        }
        this.chkPingzheng.Checked = true;

        txtRealRecv.Text = 360.ToString("0");
        hidAccRecv.Value = 360.ToString("n");
        ServiceFee.Text = 360.ToString("0");
        Total.Text = 360.ToString("0");

        ASHelper.initPaperTypeList(context, selPapertype);
        ASHelper.initParkPlaceList(context, ddlParkPlace);
        ASHelper.initAreaList(context, ddlArea, ddlParkPlace.SelectedValue);

        txtKaiTongDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        lblKaiTongEndDate.Text = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyy-MM-dd");

        txtCusname.Enabled = false;
        txtCustbirth.Enabled = false;
        selPapertype.Enabled = false;
        txtCustpaperno.Enabled = false;
        selCustsex.Enabled = false;
        txtCustphone.Enabled = false;
        txtCustpost.Enabled = false;
        txtCustaddr.Enabled = false;
        txtEmail.Enabled = false;
        txtRemark.Enabled = false;
        txtReadPaper.Enabled = false;
    }

    #region 读卡/读数据库按钮事件
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        GetInfo();
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        GetInfo();
    }
    #endregion

    #region GetInfo获取卡片、用户、停车信息
    /// <summary>
    /// 获取卡片、用户、停车信息
    /// </summary>
    protected void GetInfo()
    {
        btnPrintPZ.Enabled = false;
        btnSubmit.Enabled = false;
        hidWarning.Value = "";

        if (ddlCardType.SelectedValue == "")
        {
            context.AddError("A10001A971：请选择卡片类别", ddlCardType);
            return;
        }
        //校验
        if (this.ddlCardType.SelectedValue == "0001")
        {
            checkAccountInfo(txtCardNo.Text.Trim());
            if (context.hasError())
                return;
        }

        TMTableModule tmTMTableModule = new TMTableModule();
        TF_F_PCARDRECTDO ddoTF_F_PCARDRECIn = new TF_F_PCARDRECTDO();
        ddoTF_F_PCARDRECIn.CARDNO = txtCardNo.Text;

        TF_F_PCARDRECTDO ddoTF_F_PCARDRECOut = (TF_F_PCARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_PCARDRECIn, typeof(TF_F_PCARDRECTDO), null, "TF_F_PCARDREC", null);

        //如果没有停车收费信息
        if (ddoTF_F_PCARDRECOut == null)
        {
            if (this.ddlCardType.SelectedValue == "0001")
            {
                //如果未开通过停车卡则读取客户资料表资料
                readCustInfo(txtCardNo.Text.Trim(), txtCusname, txtCustbirth,
                    selPapertype, txtCustpaperno, selCustsex, txtCustphone, txtCustpost,
                    txtCustaddr, txtEmail, txtRemark);

                if (txtCustbirth.Text.Trim().Length == 8)
                {
                    string strBirth = txtCustbirth.Text.Trim();
                    txtCustbirth.Text = strBirth.Substring(0, 4) + "-" + strBirth.Substring(4, 2) + "-" + strBirth.Substring(6, 2);
                }
            }

            lblStartDate.Text = "";
            labEndDate.Text = "";
            labLinkNo.Text = "";
            ddlParkPlace.SelectedValue = "";
            ddlArea.SelectedValue = "";
            txtPlaceCode.Text = "";
            txtPlateNo.Text = "";
        }
        else
        {
            txtCusname.Text = ddoTF_F_PCARDRECOut.CUSTNAME;
            txtCustbirth.Text = ddoTF_F_PCARDRECOut.CUSTBIRTH;
            if (txtCustbirth.Text.Trim().Length == 8)
            {
                string strBirth = txtCustbirth.Text.Trim();
                txtCustbirth.Text = strBirth.Substring(0, 4) + "-" + strBirth.Substring(4, 2) + "-" + strBirth.Substring(6, 2);
            }
            selPapertype.SelectedValue = ddoTF_F_PCARDRECOut.PAPERTYPECODE;
            txtCustpaperno.Text = ddoTF_F_PCARDRECOut.PAPERNO;
            selCustsex.SelectedValue = ddoTF_F_PCARDRECOut.CUSTSEX;
            txtCustphone.Text = ddoTF_F_PCARDRECOut.CUSTPHONE;
            txtCustpost.Text = ddoTF_F_PCARDRECOut.CUSTPOST;
            txtCustaddr.Text = ddoTF_F_PCARDRECOut.CUSTADDR;
            txtEmail.Text = ddoTF_F_PCARDRECOut.CUSTEMAIL;
            txtRemark.Text = ddoTF_F_PCARDRECOut.REMARK;

            lblStartDate.Text = ddoTF_F_PCARDRECOut.OPENDATE.ToShortDateString();
            labEndDate.Text = ddoTF_F_PCARDRECOut.ENDDATE.ToShortDateString();
            labLinkNo.Text = ddoTF_F_PCARDRECOut.LINKNO;
            ddlParkPlace.SelectedValue = ddoTF_F_PCARDRECOut.PCODE;
            ddlArea.SelectedValue = ddoTF_F_PCARDRECOut.AREACODE;
            txtPlaceCode.Text = ddoTF_F_PCARDRECOut.PLACECODE;
            txtPlateNo.Text = ddoTF_F_PCARDRECOut.PLATENO;
        }

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardNo.Text);

        btnSubmit.Enabled = hidWarning.Value.Length == 0;
    }
    #endregion

    /// <summary>
    /// 提交按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitValidate();
        if (context.hasError())
            return;

        txtPlaceCode_Changed(sender, e);
        if (context.hasError())
            return;

        #region 读取审核员工信息
        string checkStaff = "";
        string checkDepart = "";
        TMTableModule tmTMTableModule = new TMTableModule();
        if (hidISDBRead.Value == "True")
        {
            TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            ddoTD_M_INSIDESTAFFIn.OPERCARDNO = hiddenCheck.Value;
            TD_M_INSIDESTAFFTDO[] ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A10001B030: 请校验审核员工卡");
                return;
            }
            checkStaff = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            checkDepart = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }
        #endregion

        #region 存储过程参数构建
        //调用附加业务功能开通存储过过程
        context.SPOpen();
        context.AddField("p_CARDNO").Value = txtCardNo.Text.Trim();
        context.AddField("p_PCARDTYPE").Value = ddlCardType.SelectedValue;
        context.AddField("p_CUSTNAME").Value = txtCusname.Text.Trim();
        context.AddField("p_CUSTSEX").Value = selCustsex.SelectedValue;
        context.AddField("p_CUSTBIRTH").Value = txtCustbirth.Text.Trim().Replace("-", "");
        context.AddField("p_PAPERTYPECODE").Value = selPapertype.SelectedValue;
        context.AddField("p_PAPERNO").Value = txtCustpaperno.Text.Trim();
        context.AddField("p_CUSTADDR").Value = txtCustaddr.Text.Trim();
        context.AddField("p_CUSTPOST").Value = txtCustpost.Text.Trim();
        context.AddField("p_CUSTPHONE").Value = txtCustphone.Text.Trim();
        context.AddField("p_CUSTEMAIL").Value = txtEmail.Text.Trim();
        context.AddField("p_REMARK").Value = txtRemark.Text.Trim();

        context.AddField("p_PCODE").Value = ddlParkPlace.SelectedValue;
        context.AddField("p_AREACODE").Value = ddlArea.SelectedValue;
        context.AddField("p_PLACECODE").Value = txtPlaceCode.Text.Trim();
        context.AddField("p_PLATENO").Value = txtPlateNo.Text.Trim();

        context.AddField("p_LINKNO").Value = txtLinkNo.Text.Trim();
        context.AddField("p_TRADETYPECODE").Value = "4R";
        context.AddField("p_TRADEFEE").Value = (int)(Double.Parse(hidAccRecv.Value) * 100);
        context.AddField("p_OPENDATE").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).ToString("yyyyMMdd"); ;
        context.AddField("p_ENDDATE").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyyMMdd"); ;
        context.AddField("p_OPERCARDNO").Value = context.s_CardID;
        context.AddField("p_CHECKSTAFFNO").Value = checkStaff;
        context.AddField("p_CHECKDEPARTNO").Value = checkDepart;
        #endregion

        // 执行存储过程
        bool ok = context.ExecuteSP("SP_AS_PakingLotsOpen");
        btnSubmit.Enabled = false;

        // 执行成功，显示成功消息
        if (ok)
        {
            if (chkIsFree.Checked)
            {
                AddMessage("D10001B010: 停车收费已提交财审");
            }
            else
            {
                AddMessage("D10001B010: 停车收费已经开通");
            }

            btnPrintPZ.Enabled = true;
            ASHelper.preparePingZheng(ptnPingZheng, txtCardNo.Text.Trim(), txtCusname.Text, "停车收费开通", hidAccRecv.Value
                , "0.00", "", txtCustpaperno.Text, "0.00", "0.00", "0.00", context.s_UserName,
                context.s_DepartName,
                selPapertype.SelectedValue == "" ? "" : selPapertype.SelectedItem.Text, hidAccRecv.Value, "0.00");
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }

            InitPageControls();
            ASHelper.initCardTypeList(context, ddlCardType);
            ddlCardType.SelectedValue = "0001";
        }
    }

    /// <summary>
    /// 选择区域
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        //根据区域查找停车场自动选中
        DataTable dt = ASHelper.callQuery(context, "ReadPlaceCodeByArea", ddlArea.SelectedValue);

        if (dt.Rows.Count > 0)
        {
            ddlParkPlace.SelectedValue = dt.Rows[0][0].ToString();
        }
    }

    /// <summary>
    /// 卡片类型
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitPageControls();

        if (this.ddlCardType.SelectedValue != "0001")
        {
            this.btnReadCard.Enabled = false;
            txtCusname.Enabled = true;
            txtCustbirth.Enabled = true;
            selPapertype.Enabled = true;
            txtCustpaperno.Enabled = true;
            selCustsex.Enabled = true;
            txtCustphone.Enabled = true;
            txtCustpost.Enabled = true;
            txtCustaddr.Enabled = true;
            txtEmail.Enabled = true;
            txtRemark.Enabled = true;
            txtReadPaper.Enabled = true;
        }
        else
        {
            this.btnReadCard.Enabled = true;
        }
    }

    /// <summary>
    /// 开通月份选择
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dpExpire_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtKaiTongDate.Text.Trim() != "" && chkIsFree.Checked == false)
        {
            ServiceFee.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            Total.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            txtRealRecv.Text = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("0");
            hidAccRecv.Value = (Convert.ToInt32(dpExpire.SelectedValue) * 60).ToString("n");
        }
        lblKaiTongEndDate.Text = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyy-MM-dd");
    }

    /// <summary>
    /// 选择停车场
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlParkPlace_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlArea.Items.Clear();
        ASHelper.initAreaList(context, ddlArea, ddlParkPlace.SelectedValue);
    }

    #region txtPlaceCode_Changed输入停车位后校验
    /// <summary>
    /// 输入停车位后校验
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtPlaceCode_Changed(object sender, EventArgs e)
    {
        string strKaitongdate = txtKaiTongDate.Text.Trim();
        if (string.IsNullOrEmpty(strKaitongdate))
        {
            context.AddError("A10001A022：请填写开通日期", txtKaiTongDate);
        }
        else
        {
            if (!Validation.isDate(strKaitongdate))
            {
                context.AddError("A10001A028:开通日期不符合日期格式", txtKaiTongDate);
            }
            else
            {
                //if (DateTime.Compare(Convert.ToDateTime(strKaitongdate), DateTime.Now.AddDays(-1)) < 0)
                //{
                //    context.AddError("A10001A029:开通日期不能小于当前日期", txtKaiTongDate);
                //}
            }
        }

        if (string.IsNullOrEmpty(ddlParkPlace.SelectedValue))
        {
            context.AddError("A10001A023：请选择停车场", ddlParkPlace);
        }
        if (string.IsNullOrEmpty(ddlArea.SelectedValue))
        {
            context.AddError("A10001A024：请选择区域", ddlArea);
        }
        if (string.IsNullOrEmpty(txtPlaceCode.Text.Trim()))
        {
            context.AddError("A10001A025：请填写车位号", txtPlaceCode);
        }

        if (context.hasError())
            return;

        context.SPOpen();
        context.AddField("p_PCODE").Value = ddlParkPlace.SelectedValue;
        context.AddField("p_AREACODE").Value = ddlArea.SelectedValue;
        context.AddField("p_PLACECODE").Value = txtPlaceCode.Text.Trim();
        context.AddField("p_OPENDATE").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).ToString("yyyyMMdd"); ;
        context.AddField("p_ENDDATE").Value = DateTime.Parse(txtKaiTongDate.Text.Trim()).AddMonths(Convert.ToInt32(dpExpire.SelectedValue)).AddDays(-1).ToString("yyyyMMdd");
        context.ExecuteSP("SP_AS_ParkingplaceCheck");

    }
    #endregion

    /// <summary>
    /// 是否免费
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkIsFree_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIsFree.Checked)
        {
            txtRealRecv.Text = 0.ToString("0");
            hidAccRecv.Value = 0.ToString("n");
            ServiceFee.Text = 0.ToString("0");
            Total.Text = 0.ToString("0");
        }
        else
        {
            dpExpire_SelectedIndexChanged(sender, e);
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")               // 是否继续
        {
            hidWarning.Value = "";                       // 清除消息内容
            btnSubmit.Enabled = true;
        }
        else if (hidWarning.Value == "submit")
        {
            hidWarning.Value = "";                       // 清除消息内容
            btnDBRead_Click(sender, e);
        }
        else
        {
            hidWarning.Value = "";                       // 清除消息内容
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }

    #region SubmitValidate提交前验证
    /// <summary>
    /// 提交前验证
    /// </summary>
    private void SubmitValidate()
    {
        if (ddlCardType.SelectedValue == "")
        {
            context.AddError("A10001A971：请选择卡片类别", ddlCardType);
        }

        if (this.ddlCardType.SelectedValue != "0001")
        {
            //对用户姓名进行非空、长度检验
            if (txtCusname.Text.Trim() == "")
                context.AddError("A001001111", txtCusname);
            else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
                context.AddError("A001001113", txtCusname);

            //对用户性别进行非空检验
            if (selCustsex.SelectedValue == "")
                context.AddError("A001001116", selCustsex);

            //对证件类型进行非空检验
            if (selPapertype.SelectedValue == "")
                context.AddError("A001001117", selPapertype);

            //对出生日期进行非空、日期格式检验
            String cDate = txtCustbirth.Text.Trim();
            if (cDate == "")
                context.AddError("A001001114", txtCustbirth);
            else if (!Validation.isDate(txtCustbirth.Text.Trim()))
                context.AddError("A001001115", txtCustbirth);

            //对联系电话进行非空、长度、数字检验
            if (txtCustphone.Text.Trim() == "")
                context.AddError("A001001124", txtCustphone);
            else if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
                context.AddError("A001001126", txtCustphone);
            else if (!Validation.isNum(txtCustphone.Text.Trim()))
                context.AddError("A001001125", txtCustphone);

            //对证件号码进行非空、长度、英数字检验
            if (txtCustpaperno.Text.Trim() == "")
                context.AddError("A001001121", txtCustpaperno);
            else if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
                context.AddError("A001001122", txtCustpaperno);
            else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
                context.AddError("A001001123", txtCustpaperno);
            else if (selPapertype.SelectedValue == "00")//身份证必须为15或18位。
            {
                if (Validation.strLen(txtCustpaperno.Text.Trim()) != 18 && Validation.strLen(txtCustpaperno.Text.Trim()) != 15)
                    context.AddError("A001001130", txtCustpaperno);
                else
                {
                    //身份证号规则校验
                    if (!CommonHelper.CheckIDCard(txtCustpaperno.Text.Trim()))
                    {
                        context.AddError("A001001131", txtCustpaperno);
                    }
                }
            }

            //对邮政编码进行非空、长度、数字检验
            if (txtCustpost.Text.Trim() != "")
            {
                //context.AddError("A001001118", txtCustpost);
                if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                    context.AddError("A001001120", txtCustpost);
                else if (!Validation.isNum(txtCustpost.Text.Trim()))
                    context.AddError("A001001119", txtCustpost);
            }

            //对联系地址进行非空、长度检验
            if (txtCustaddr.Text.Trim() == "")
                context.AddError("A001001127", txtCustaddr);
            else if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);

            //对备注进行长度检验
            if (txtRemark.Text.Trim() != "")
                if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                    context.AddError("A001001129", txtRemark);
        }
        else
        {
            if (txtCustbirth.Text.Trim() != "")
                if (!Validation.isDate(txtCustbirth.Text.Trim()))
                    context.AddError("A001001115", txtCustbirth);
        }
 
        //对电子邮件进行格式检验
        if (txtEmail.Text.Trim() != "")
            new Validation(context).isEMail(txtEmail);

        if (string.IsNullOrEmpty(txtLinkNo.Text.Trim()))
        {
            context.AddError("A10001A021：请填写九竹序列号", txtLinkNo);
        }

        if (string.IsNullOrEmpty(txtPlateNo.Text.Trim()))
        {
            context.AddError("A10001A026：请填写车牌号", txtPlateNo);
        }
        if (this.ddlCardType.SelectedValue == "0001")
        {
            if (this.txtCardNo.Text.Length != 16)
            {
                context.AddError("A10001A027：龙城通卡号位数不正确", txtCardNo);
            }
        }
    }
    #endregion
}
