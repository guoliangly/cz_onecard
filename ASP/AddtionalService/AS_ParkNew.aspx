﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AS_ParkNew.aspx.cs" Inherits="ASP_AddtionalService_AS_ParkNew"
    ValidateRequest="false" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>停车收费-开通</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function AddDays(date1, value1) {
            date1.setDate(date1.getDate() + value1);
            return date1;
        }


        function AddMonths(date1, value1) {
            date1.setMonth(date1.getMonth() + value1);
            return date1;
        }  


        function changeEndDate() {
            var DDL = document.getElementById("dpExpire");
            var index = DDL.selectedIndex;
            var Text = DDL.options[index].text;
            var Value =parseInt( DDL.options[index].value);
            var dtArr = document.getElementById("txtKaiTongDate").value.split("-");
            var stateDate = new Date(dtArr[0], dtArr[1], dtArr[2]);
            document.getElementById("lblKaiTongEndDate").innerText = AddDays(AddMonths(stateDate, Value-1), -1).format("yyyy-MM-dd");
                }

    </script>
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
    <div class="tb">
        附加业务->停车收费-开通
    </div>
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
            <asp:BulletedList ID="bulMsgShow" runat="server">
            </asp:BulletedList>
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <div class="con">
                <div class="card">
                    卡片信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    用户卡号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtCardNo" CssClass="input" runat="server" MaxLength="16" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    卡片类型:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="labCardType" CssClass="labeltext" runat="server" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    卡内余额:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtCardBalance" CssClass="labeltext" runat="server" />
                            </td>
                            <td width="9%">
                                &nbsp;
                            </td>
                            <td width="12%">
                                <asp:Button ID="btnReadCard" Text="读卡" CssClass="button1" runat="server" OnClientClick="return readParkInfo()"
                                    OnClick="btnReadCard_Click" />
                            </td>
                            <td width="12%">
                                <asp:Button ID="btnDBRead" CssClass="button1" runat="server" Text="读数据库"  OnClientClick="return warnCheck();" />
                            </td>
                            <asp:HiddenField runat="server" ID="hidCardReaderToken" />
                            <asp:HiddenField runat="server" ID="hidWarning" />
                            <asp:HiddenField runat="server" ID="hidAsn" />
                            <asp:HiddenField runat="server" ID="hidTradeNo" />
                            <asp:HiddenField runat="server" ID="hidAccRecv" />
                            <asp:HiddenField runat="server" ID="hidParkInfo" />
                            <asp:HiddenField runat="server" ID="hiddenCheck" />
                            <asp:HiddenField runat="server" ID="hidISDBRead" />
                            <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    启用日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" CssClass="labeltext" runat="server" />
                            </td>
                            <td>
                                <div align="right">
                                    停车到期:</div>
                            </td>
                            <td>
                                <asp:Label ID="labEndDate" CssClass="labeltext" runat="server" />
                            </td>
                            <td>
                            <div align="right">
                                    九竹序列号:</div>
                            
                            </td>
                           
                            <td>
                              <asp:Label ID="labLinkNo" CssClass="labeltext" runat="server" />
                            </td>
                             <td>
                               &nbsp;
                            </td>
                            <td>
                               &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="pip">
                    用户信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    用户姓名:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtCustName" CssClass="labeltext" runat="server" MaxLength="50" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    出生日期:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtCustBirth" CssClass="labeltext" runat="server" MaxLength="8" />
                            </td>
                            <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtCustBirth"
                                Format="yyyyMMdd" />
                            <td width="9%">
                                <div align="right">
                                    证件类型:</div>
                            </td>
                            <td width="13%">
                                <asp:DropDownList ID="selPaperType" CssClass="labeltext" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    证件号码:</div>
                            </td>
                            <td width="24%">
                                <asp:TextBox ID="txtPaperNo" CssClass="labeltextmid" runat="server" MaxLength="20" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    用户性别:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="selCustSex" CssClass="labeltext" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <div align="right">
                                    联系电话:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustPhone" CssClass="labeltext" runat="server" MaxLength="20" />
                            </td>
                            <td>
                                <div align="right">
                                    邮政编码:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustPost" CssClass="labeltext" runat="server" MaxLength="6" />
                            </td>
                            <td>
                                <div align="right">
                                    联系地址:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustAddr" CssClass="labeltextmid" runat="server" MaxLength="50" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    电子邮件:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" CssClass="labeltext" runat="server" MaxLength="30" />
                            </td>
                            <td>
                                <div align="right">
                                    备注:</div>
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="txtRemark" CssClass="labeltext" runat="server" MaxLength="100" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="pip">
                    停车信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="20%">
                                <div align="right">
                                    九竹管理系统序列号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtLinkNo" CssClass="input" runat="server" MaxLength="50" />
                            </td>
                             <td width="9%">
                             开通日期：
                            </td>
                            <td width="13%">
                            <asp:TextBox ID="txtKaiTongDate" CssClass="inputshort" runat="server" maxlength="10" onpropertychange="changeEndDate();"/>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtKaiTongDate" Format="yyyy-MM-dd" />
                            </td>
                            <td width="10%">
                                <div align="right">
                                    开通有效期:</div>
                            </td>
                            <td width="13%">
                                <asp:DropDownList ID="dpExpire" CssClass="input" runat="server" 
                                    AutoPostBack="true" onselectedindexchanged="dpExpire_SelectedIndexChanged">
                                    <asp:ListItem Value="1">一个月</asp:ListItem>
                                    <asp:ListItem Value="2">二个月</asp:ListItem>
                                    <asp:ListItem Value="3">三个月</asp:ListItem>
                                    <asp:ListItem Value="4">四个月</asp:ListItem>
                                    <asp:ListItem Value="5">五个月</asp:ListItem>
                                    <asp:ListItem Value="6" Selected="True">六个月</asp:ListItem>
                                    <asp:ListItem Value="7">七个月</asp:ListItem>
                                    <asp:ListItem Value="8">八个月</asp:ListItem>
                                    <asp:ListItem Value="9">九个月</asp:ListItem>
                                    <asp:ListItem Value="10">十个月</asp:ListItem>
                                    <asp:ListItem Value="11">十一个月</asp:ListItem>
                                    <asp:ListItem Value="12">十二个月</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                           
                            <td width="9%">
                            结束日期：
                            </td>
                            <td width="13%">
                            <asp:Label ID="lblKaiTongEndDate" CssClass="labeltext" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="basicinfo">
                <div class="money">
                    费用信息</div>
                <div class="kuang5">
                    <table cellspacing="0" border="1" style="width: 100%; border-collapse: collapse;"
                        id="gvResult" rules="all" class="tab1">
                        <tbody>
                            <tr class="tabbt">
                                <th scope="col">
                                    费用项目
                                </th>
                                <th scope="col">
                                    费用金额
                                </th>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    押金
                                </td>
                                <td style="width: 50%;">
                                    0.00
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td style="width: 50%;">
                                    手续费
                                </td>
                                <td style="width: 50%;">
                                    0.00
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    服务费
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="ServiceFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td style="width: 50%;">
                                    &nbsp;
                                </td>
                                <td style="width: 50%;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    &nbsp;
                                </td>
                                <td style="width: 50%;">
                                    &nbsp;
                                </td>
                            </tr>
                          
                            <tr class="tabjg">
                                <td style="width: 50%;">
                                    合计应收
                                </td>
                                <td style="width: 50%;">
                                <asp:Label ID="Total" runat="server" Text="0.00"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pipinfo">
                <div class="info">
                    收款信息</div>
                <div class="kuang5">
                    <div class="smallkuang">
                        <div class="left">
                            <img src="../../Images/show-sale.JPG" width="280px" height="150px" /></div>
                        <div class="big">
                            <table width="150" border="0" cellpadding="0" cellspacing="0" class="text25">
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" align="right">
                                        <label>
                                            本次实收:&nbsp;</label>
                                    </td>
                                    <td width="50%">
                                        <asp:TextBox ID="txtRealRecv" CssClass="inputshort" runat="server" MaxLength="9" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        本次应找:&nbsp;
                                    </td>
                                    <td>
                                        <div id="txtChanges">
                                            0.00</div>
                                    </td>
                                </tr>
                             
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footall">
            </div>
            <div class="btns">
                <table width="200" align="right" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false"
                                OnClientClick="printdiv('ptnPingZheng1')" />
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
                <asp:CheckBox ID="chkPingzheng" runat="server" Text="自动打印凭证" Checked="true" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
