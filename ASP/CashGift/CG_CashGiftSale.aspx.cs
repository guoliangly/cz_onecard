﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using System.Text.RegularExpressions;

public partial class ASP_CashGift_CashGiftSale : Master.FrontMaster {
	protected void Page_Load(object sender, EventArgs e) {
		if (Page.IsPostBack)
			return;

		// 测试模式下卡号可以输入

		if (!context.s_Debugging)
			txtCardNo.Attributes["readonly"] = "true";

		// 设置可读属性

		//setReadOnly(txtCardBalance, txtStartDate, txtEndDate, txtCardState, txtWallet2);

		// 初始化证件类型

		ASHelper.initPaperTypeList(context, selPaperType);

		// 初始化性别
		ASHelper.initSexList(selCustSex);
	}

	private void prepareSP() {
		context.SPOpen();

		context.AddField("P_SUBMIT", "string", "input");
		context.AddField("P_CARDNO", "string", "input");
		context.AddField("p_CardBalance", "Int32", "input");	//add by jiangbb 2012/3/20

        //context.AddField("P_WALLET1", "Int32", "input");
        //context.AddField("P_WALLET2", "Int32", "input");
        //context.AddField("P_STARTDATE", "string", "input");
        //context.AddField("P_ENDDATE", "string", "input");

		context.AddField("P_EXPIREDDATE", "string", "input");
		context.AddField("P_SALEMONEY", "Int32", "input");
		context.AddField("P_ID", "string", "input");
		context.AddField("P_CARDTRADENO", "string", "input");

		//context.AddField("P_ASN", "string", "input");
		//context.AddField("P_SELLCHANNELCODE", "string", "input");
		//context.AddField("P_CARDPRICE", "Int32", "output", "");

		context.AddField("P_TERMINALNO", "string", "input");
		context.AddField("P_CUSTNAME", "String", "input");
		context.AddField("P_CUSTSEX", "String", "input");
		context.AddField("P_CUSTBIRTH", "String", "input");
		context.AddField("P_PAPERTYPE", "String", "input");
		context.AddField("P_PAPERNO", "String", "input");
		context.AddField("P_CUSTADDR", "String", "input");
		context.AddField("P_CUSTPOST", "String", "input");
		context.AddField("P_CUSTPHONE", "String", "input");
		context.AddField("P_CUSTEMAIL", "String", "input");
		context.AddField("P_REMARK", "String", "input");
		context.AddField("P_SEQNO", "string", "output", "16");
		context.AddField("P_CURRCARDNO", "string", "input");
		context.AddField("P_WRITECARDSCRIPT", "string", "output", "1024");

	}

	// 读卡处理
	protected void btnReadCard_Click(object sender, EventArgs e) {
		
        // 读取卡片类型
		readCardType(txtCardNo.Text, labCardType);

		// 读取卡片状态

		ASHelper.readCardState(context, txtCardNo.Text, txtCardState);

		prepareSP();
		context.GetField("p_CardBalance").Value = (int)(Convert.ToDecimal(txtCardBalance.Text) * 100);		//卡内余额 add by jiangbb 2012/3/19
		context.GetField("P_SUBMIT").Value = "0";	//不提交售卡
		context.GetField("P_CARDNO").Value = txtCardNo.Text;

		//
		#region delete by jiangbb 2012/3/19
		//context.GetField("P_WALLET1").Value = (int)(Convert.ToDecimal(txtCardBalance.Text) * 100);
		//context.GetField("P_WALLET2").Value = (int)(Convert.ToDecimal(txtWallet2.Text == "NaN" ? "0.00" : txtWallet2.Text) * 100);

		//context.GetField("P_STARTDATE").Value = txtStartDate.Text;
		//context.GetField("P_ENDDATE").Value = txtEndDate.Text;
		#endregion

		context.ExecuteSP("SP_CG_SaleCard");

		btnPrintSJ.Enabled = false;
		btnSubmit.Enabled = !context.hasError();
	}


    // 读卡校验
    protected void btnCheckRead_Click(object sender, EventArgs e)
    {

        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        // 读取卡片状态

        ASHelper.readCardState(context, txtCardNo.Text, txtCardState);
    }


	// 确认对话框确认处理

	protected void btnConfirm_Click(object sender, EventArgs e) {
		if (hidWarning.Value == "yes")    // 是否继续
        {
			btnSubmit.Enabled = true;
		}
		else if (hidWarning.Value == "writeSuccess") // 写卡成功
        {
			clearCustInfo(txtCardNo, txtCustName, txtCustBirth, selPaperType, txtPaperNo,
			   selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);

			context.SPOpen();
			context.AddField("p_TRADEID").Value = hidSeqNo.Value;
			context.AddField("p_CARDTRADENO").Value = hidTradeNo.Value;
			bool ok = context.ExecuteSP("SP_PB_updateCardTrade");

			if (ok)
				AddMessage("金福卡售卡成功");
            AddMessage("金福卡前台写卡成功");
		}
		else if (hidWarning.Value == "writeFail") // 写卡失败
        {
			context.AddError("前台写卡失败");
		}

        if (chkPingzheng.Checked && btnPrintSJ.Enabled)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript","printShouJu();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "printPaPiaoScript1", "printInvoice();", true);
		}

		hidWarning.Value = ""; // 清除警告信息
	}

	// 提交判断
	private void submitValidate() {
		// 校验客户信息
		custInfoValidate(txtCustName, txtCustBirth,
			selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost,
			txtCustAddr, txtEmail, txtRemark);

		Validation val = new Validation(context);
		val.beNumber(txtSaleMoney, "售卡金额必须是有效整数金额");

	}

	// 售卡提交
	protected void btnSubmit_Click(object sender, EventArgs e) {
		submitValidate();
		if (context.hasError())
			return;

		prepareSP();
		context.GetField("p_CardBalance").Value = (int)(Convert.ToDecimal(txtCardBalance.Text) * 100);		//卡内余额 add by jiangbb 2012-03-23
		context.GetField("P_SUBMIT").Value = "1"; // 提交售卡
		context.GetField("P_CARDNO").Value = txtCardNo.Text;
		
		#region delete by jiangbb 2012-03-23
		//context.GetField("P_WALLET1").Value = (int)(Convert.ToDecimal(txtCardBalance.Text) * 100);
		//context.GetField("P_WALLET2").Value = (int)(Convert.ToDecimal(txtWallet2.Text) * 100);
		//context.GetField("P_STARTDATE").Value = txtStartDate.Text;
		//context.GetField("P_ENDDATE").Value = txtEndDate.Text;

		//int addMonths = Convert.ToInt32(selValidMonths.SelectedValue);
		//// 截止日期=【售卡时间+有效期限】所得月份的最后一天

		//DateTime temp = DateTime.Now.AddMonths(addMonths);
		//string expiredDate =  temp
		//    .AddDays(1 - temp.Day)
		//    .AddMonths(1)
		//    .AddDays(-1).ToString("yyyyMMdd");
		#endregion
		
		context.GetField("P_EXPIREDDATE").Value = "20501231";

		int saleMoney = Convert.ToInt32(txtSaleMoney.Text) * 100;
		context.GetField("P_SALEMONEY").Value = saleMoney;

		context.GetField("P_ID").Value = DealString.GetRecordID(hidTradeNo.Value, hidAsn.Value);
		context.GetField("P_CARDTRADENO").Value = hidTradeNo.Value;
		//context.GetField("P_ASN").Value = hidAsn.Value.Substring(4, 16);

		//context.GetField("P_SELLCHANNELCODE").Value = "01";
		context.GetField("P_TERMINALNO").Value = "112233445566";   // 目前固定写成112233445566

		context.GetField("P_CUSTNAME").Value = txtCustName.Text;
		context.GetField("P_CUSTSEX").Value = selCustSex.SelectedValue;
		context.GetField("P_CUSTBIRTH").Value = txtCustBirth.Text;
		context.GetField("P_PAPERTYPE").Value = selPaperType.SelectedValue;
		context.GetField("P_PAPERNO").Value = txtPaperNo.Text;
		context.GetField("P_CUSTADDR").Value = txtCustAddr.Text;
		context.GetField("P_CUSTPOST").Value = txtCustPost.Text;
		context.GetField("P_CUSTPHONE").Value = txtCustPhone.Text;
		context.GetField("P_CUSTEMAIL").Value = txtEmail.Text;
		context.GetField("P_REMARK").Value = txtRemark.Text;

		context.GetField("P_CURRCARDNO").Value = context.s_CardID;

		bool ok = context.ExecuteSP("SP_CG_SaleCard");

		btnSubmit.Enabled = false;

		// 存储过程执行成功，显示成功消息

		if (ok) {
			hidSeqNo.Value = "" + context.GetField("P_SEQNO").Value;

			// 准备收据打印数据
			// ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, context.s_UserName, txtSaleMoney.Text);
            //ASHelper.prepareShouJu(ptnShouJu, txtCardNo.Text, context.GetField("P_CUSTNAME").Value.ToString(), "金福卡售卡", Convert.ToInt32(txtSaleMoney.Text).ToString("0.00")
            //   , "", "", "", "",
            //   "", "", context.s_UserName, context.s_DepartName, "", "0.00", context.GetField("P_ID").Value.ToString());

            ASHelper.preparePingZheng(ptnPingZheng, txtCardNo.Text, txtCustName.Text, "金福卡售卡",
                Convert.ToInt32(txtSaleMoney.Text).ToString("0.00"), "", "", "",
                (Convert.ToInt32(txtSaleMoney.Text)-20).ToString("0.00"), "", Convert.ToInt32(txtSaleMoney.Text).ToString("0.00"), context.s_UserID,
                context.s_DepartName, "", "0.00", context.GetField("P_ID").Value.ToString());

			btnPrintSJ.Enabled = true;

			string writeCardScripts = "" + context.GetField("P_WRITECARDSCRIPT").Value;

			ScriptManager.RegisterStartupScript(
				this, this.GetType(), "writeCardScript", writeCardScripts, true);
		}
	}

	//add by jiangbb 2012-03-20
	//售卡金额输入框，实际充值显示框，押金输入框联动
	protected void txtSaleMoney_TextChanged(object sender, EventArgs e) {
		if (!Regex.IsMatch(txtSaleMoney.Text.ToString(), "^[0-9]*$")) {
			context.AddError("LJK1000001", txtSaleMoney);
			return;
		}
		if (Convert.ToInt32(txtSaleMoney.Text) > 20) {
			txtPledge.Text = "20";	//押金为20元
			txtRealSale.Text = (Convert.ToInt32(txtSaleMoney.Text) - 20).ToString();	//实际充值金额为售卡金额减20

            //bool isMatch = false;	//是否有相同的
            //for (int i = 0; i < dropDownSale.Items.Count; i++) {
            //    if (txtSaleMoney.Text.Trim().ToString() == dropDownSale.Items[i].Value) {
            //        dropDownSale.Items[i].Selected = true;
            //        isMatch = true;
            //        break;
            //    }
            //}
            //if (!isMatch) {
            //    dropDownSale.SelectedIndex = 0;
            //}
		}
		else {
			txtPledge.Text = string.Empty;
			txtRealSale.Text = string.Empty;
			context.AddError("LJK1000002", txtSaleMoney);
		}
	}

	//add by jiangbb 2012-03-20
	//选定售卡金额下拉框时
	protected void dropDownSale_SelectedIndexChanged(object sender, EventArgs e) {
		if (this.dropDownSale.SelectedValue != "") {		//售卡金额不为‘---请选择---’时，禁用售卡金额，实际充值金额，押金输入框
			txtSaleMoney.Text = this.dropDownSale.SelectedValue;	//售卡金额
			txtPledge.Text = "20";		//押金
			txtRealSale.Text = (Convert.ToInt32(this.dropDownSale.SelectedValue) - 20).ToString();	//实际充值金额
		}
        else
        {
            txtSaleMoney.Text = "0";	//售卡金额
            txtPledge.Text = "0";		//押金
            txtRealSale.Text = "0";	//实际充值金额

        }
	}
}
