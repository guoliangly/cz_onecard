﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CG_CashGiftLostReg.aspx.cs" Inherits="ASP_CashGift_CG_CashGiftLostReg" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>金福卡-丢失登记</title>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
    EnableScriptLocalization="true" ID="ScriptManager2" />
       <script type="text/javascript" language="javascript">
           var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
           swpmIntance.add_initializeRequest(BeginRequestHandler);
           swpmIntance.add_pageLoading(EndRequestHandler);
           function BeginRequestHandler(sender, args) {
               try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
           }
           function EndRequestHandler(sender, args) {
               try { MyExtHide(); } catch (ex) { }
           }
          </script>  
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="tb">金福卡-&gt;丢失登记</div>
<!-- #include file="../../ErrorMsg.inc" --> 
 <div class="con">
 <div class="pip">丢失信息维护</div>
 <div class="kuang5">
   <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
     <tr>
       <td width="12%"><div align="right">卡号:</div></td>
       <td width="30%"><asp:TextBox ID="txtQueryCardno" runat="server" CssClass="inputmid" MaxLength="16" Width="196px" ></asp:TextBox>
       </td>
       <td><asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" OnClick="btnQuery_Click" /></td>
       <td width="27%">&nbsp;</td>
     </tr>
   </table>
 </div>
  <div class="kuang5">
  <div class="gdtb" style="height:300px">
  <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1" >
      <asp:GridView ID="lvwStaff" runat="server" OnPageIndexChanging="lvwStaff_PageIndexChanging"
        Width = "100%"
        CssClass="tab1"
         AllowPaging=true
         PageSize=10
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        >
           <Columns>
                <asp:BoundField HeaderText="卡号" DataField="cardno"/>
                <asp:BoundField HeaderText="丢失原因" DataField="lostreason"/>
                <asp:BoundField HeaderText="丢失时间" DataField="losttime"/>
                <asp:BoundField HeaderText="丢失人" DataField="lostor"/>
                <asp:BoundField HeaderText="操作部门" DataField="departname"/>
                <asp:BoundField HeaderText="操作员工" DataField="staffname"/>
                <asp:BoundField HeaderText="操作时间" DataField="operatetime"/>
            </Columns>           
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>卡号</td>
                    <td>丢失原因</td>
                    <td>丢失时间</td>
                    <td>丢失人</td>
                    <td>操作部门</td>
                    <td>操作员工</td>
                    <td>操作时间</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
    </table>
  </div>
  </div>
  <div class="kuang5">
   <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
     <tr>
       <td width="5%"><div align="right">卡号:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtCardno" runat="server" CssClass="inputmid" MaxLength="16" Width="120px" ></asp:TextBox><span class="red">*</span></td>
       <td width="9%"><div align="right">丢失原因:</div></td>
       <td style="height: 37px">
       <asp:DropDownList ID="selLostReason" runat="server" >
       <asp:ListItem Value="1">商户丢失</asp:ListItem>
       <asp:ListItem Value="2">内部回收前丢失</asp:ListItem>
       </asp:DropDownList>
       <span class="red">*</span></td>
       <td width="9%"><div align="right">丢失人:</div></td>
       <td style="height: 37px"><asp:TextBox ID="txtLostor" runat="server" CssClass="inputmid" Width="100px" ></asp:TextBox><span class="red">(商户丢失的，则可以不填写)</span></td>
     </tr>
     <tr>
       <td width="9%"><div align="right">丢失时间:</div></td>
       <td><asp:TextBox runat="server" ID="txtLostTime" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtLostTime" Format="yyyyMMdd" /><span class="red">*</span></td>
       <td colspan="4"><table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
         <tr>
           
           <td><asp:Button ID="btnStaffAdd" runat="server" Text="丢失登记" CssClass="button1" OnClick="btnStaffAdd_Click" /></td>
           <td></td>
           <td></td>
         </tr>
            </table></td>
      </tr>
   </table>
 </div>
   </div>
   </ContentTemplate>
</asp:UpdatePanel>
    </form>
</body>
</html>
