﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.BalanceChannel;
using TM;
using TDO.UserManager;
using Common;
using TM.UserManager;
using Master;

public partial class ASP_CashGift_CG_CashGiftLostReg : Master.Master
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }

    private Boolean CheckContext()
    {
        //对context的error检测 
        if (context.hasError())
            return false;
        else
            return true;
    }
    public ICollection CreateStaffQueryDataSource()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从员工编码表(TD_M_INSIDESTAFF)中读取数据



        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();

        String strSql = "select a.tradeid,a.cardno,decode(a.lostreason,'1','商户丢失','2','内部回收前丢失',a.lostreason) lostreason,a.losttime,a.lostor, b.staffname,c.departname,a.operatetime ";
        strSql+="from TF_B_TRADE_CASHGIFT_REG a,TD_M_INSIDESTAFF b,TD_M_INSIDEDEPART c ";

        ArrayList list = new ArrayList();
        list.Add(" a.operatestaffno=b.staffno and b.departno=c.departno and a.tradetypecode='58' ");

        if (txtQueryCardno.Text.Trim()!= "")
            list.Add("a.cardno = '" + txtQueryCardno.Text.Trim() + "'");
        strSql += DealString.ListToWhereStr(list);
        strSql += "  order by a.operatetime desc";
        DataTable data = tmTMTableModule.selByPKDataTable(context, tdoTD_M_INSIDESTAFFIn, null, strSql, 1000);
        DataView dataView = new DataView(data);
        return dataView;

    }

    public void lvwStaff_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwStaff.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }


    protected void lvwStaff_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件


            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwStaff','Select$" + e.Row.RowIndex + "')");
        }
    }


    protected void lvwStaff_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtCardno.Text = getDataKeys2("CARDNO");
        //selLostReason.SelectedValue = getDataKeys2("LOSTREASON");
        //txtLostTime.Text = getDataKeys2("LOSTTIME");
        //txtLostor.Text = getDataKeys2("LOSTOR");

    }

    /// <summary>
    /// GridView分页事件，修改人：陈文涛,修改时期：2014/7/7
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvwStaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        lvwStaff.PageIndex = e.NewPageIndex;
        lvwStaff.DataSource = CreateStaffQueryDataSource();
        lvwStaff.DataBind();
    }

    public String getDataKeys2(String keysname)
    {
        return lvwStaff.DataKeys[lvwStaff.SelectedIndex][keysname].ToString();
    }


    //员工编码的按纽事件


    protected void btnStaffAdd_Click(object sender, EventArgs e)
    {
        if (!Common.Validation.isDate(txtLostTime.Text.Trim(), "yyyyMMdd"))
        {
            context.AddError("丢失日期格式有误", txtLostTime);
        }
        if (txtCardno.Text.Trim()=="")
        {
            context.AddError("卡号不能为空", txtCardno);
        }
        if (context.hasError())
        {
            return;
        }
        context.DBOpen("StorePro");

        context.AddField("p_cardNo").Value = txtCardno.Text;
        context.AddField("p_lostReason").Value =selLostReason.SelectedValue;
        context.AddField("p_lostTime","DateTime").Value = DateTime.ParseExact(txtLostTime.Text.Trim(), "yyyyMMdd", null);
        context.AddField("p_lostor").Value = txtLostor.Text.Trim();

        bool ok = context.ExecuteSP("SP_CG_LostReg");

        if (ok)
        {
            AddMessage("金福卡丢失登记成功");
            btnQuery_Click(sender, e);
        }


        //清除输入的员工信息


        ClearStaff();
    }

    protected void btnStaffModify_Click(object sender, EventArgs e)
    {
      
    }

    protected void btnStaffDelete_Click(object sender, EventArgs e)
    {

    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {

        //查询员工信息
        lvwStaff.DataSource = CreateStaffQueryDataSource();
        lvwStaff.DataBind();


    }
    private void ClearStaff()
    {
        //清除输入的员工信息

    }
}
