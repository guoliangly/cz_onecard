﻿<%@ Import namespace="System.Data" %> 
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CG_CashGiftDistribution.aspx.cs" Inherits="ASP_CashGift_CG_CashGiftDistribution" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>金福卡分配</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="../../js/mootools.js"></script>

    <script type="text/javascript">
        
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
<div class="tb">
金福卡管理->卡（取消）分配
</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

<div class="con">
  <div class="base">卡（取消）分配</div>
  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td width="10%"><div align="right">操作类型:</div></td>
    <td>
    <asp:DropDownList ID="selCardState" CssClass="input" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCardState_SelectedIndexChanged"></asp:DropDownList>
    </td>
    <td width="10%"><div align="right">卡号:</div></td>
    <td><asp:TextBox ID="txtCardNo" CssClass="inputmid" runat="server" MaxLength="16"></asp:TextBox></td>
    <td><div align="right">分配日期:</div></td>
    <td style="width: 260px"><asp:TextBox runat="server" ID="txtDistDate" CssClass="inputmid"  MaxLength="8"/>
    <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtDistDate"
        Format="yyyyMMdd" /></td>
  </tr>
  <tr>
    <td><div align="right">回收开始时间:</div>
    </td>
    <td><asp:TextBox ID="txtRecyBeginDate" CssClass="inputmid" runat="server" MaxLength="8"></asp:TextBox>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtRecyBeginDate"
        Format="yyyyMMdd" /></td>
    <td><div align="right">回收结束时间:</div>
    </td>
    <td><asp:TextBox ID="txtRecyEndDate" CssClass="inputmid" runat="server" MaxLength="8"></asp:TextBox>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtRecyEndDate"
        Format="yyyyMMdd" /></td>
    <td><div align="right">数量:</div></td>
    <td>
    <asp:TextBox runat="server" ID="txtSum" CssClass="inputmid"/>
    </td>
  </tr>
  <tr>
    <td><div align="right">部门:</div>
    </td>
    <td><asp:DropDownList ID="seldisDepart" CssClass="input" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selDisDept_Changed"></asp:DropDownList></td>
    <td><div align="right">员工:</div>
    </td>
    <td><asp:DropDownList ID="seldisStaff" CssClass="input" runat="server"></asp:DropDownList></td>
    <td>&nbsp;</td>
    <td><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/></td>
  </tr>
</table>

 </div>
  <div class="jieguo">查询结果</div>
  <div class="kuang5">
<div class="gdtb" style="height:270px;overflow:auto;">
         <asp:GridView ID="gvResult" runat="server"
        Width = "98%"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode="NumericFirstLast"
        PagerStyle-HorizontalAlign="left"
        PagerStyle-VerticalAlign="Top"
        AutoGenerateColumns="False">
           <Columns>
             <asp:TemplateField>
                <HeaderTemplate>
                  <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="true" OnCheckedChanged="CheckAll" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:CheckBox ID="ItemCheckBox" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
               <asp:BoundField HeaderText="卡号" DataField="CARDNO"/>
                <asp:BoundField HeaderText="卡状态" DataField="RESSTATE"/>
                <asp:BoundField HeaderText="卡类型" DataField="CARDTYPENAME"/>
                <asp:BoundField HeaderText="卡面类型" DataField="CARDSURFACENAME"/>
                <asp:BoundField HeaderText="回收时间" DataField="RECLAIMTIME"/>
                <asp:BoundField HeaderText="所属部门" DataField="DEPARTNAME"/>
                <asp:BoundField HeaderText="所属员工" DataField="STAFFNAME"/>
            </Columns>           
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td><input type="checkbox" /></td>
                    <td>卡号</td>
                    <td>卡状态</td>
                    <td>卡类型</td>
                    <td>卡面类型</td>
                    <td>回收时间</td>
                    <td>所属部门</td>
                    <td>所属员工</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
  </div>
  </div>
 <div id="divDist">
  <div class="base">分配到员工</div>
  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td width="10%"><div align="right">领用部门:</div></td>
    <td width="10%">
    <asp:DropDownList ID="selInDepart" CssClass="input" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selInDepart_Changed"></asp:DropDownList>
    </td width="10%">
    <td width="10%"><div align="right">领用员工:</div></td>
    <td><asp:DropDownList ID="selInStaff" CssClass="input" runat="server"></asp:DropDownList></td>
    <td align="right">
    
    </td>
  </tr>
</table>
</div>
 </div>
</div>
<div class="btns">
     <table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click"/></td>
  </tr>
</table>

</div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>

