﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ASP_CashGift_CG_CashGiftQuery : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置可读属性
        setReadOnly(txtCardBalance, txtStartDate, txtEndDate, txtCardState, txtWallet2);

    }
 
    // 读卡处理
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        // 读取卡片状态
        ASHelper.readCardState(context, txtCardNo.Text, txtCardState);

        // 读取客户信息
        readCustInfo(txtCardNo.Text,
            txtCustName, txtCustBirth,
            selPaperType, txtPaperNo,
            selCustSex, txtCustPhone,
            txtCustPost, txtCustAddr, txtEmail, txtRemark, true);

        // 读取其他信息
        DataTable data = SPHelper.callQuery("SP_CG_Query", context, "QryGashInfo", txtCardNo.Text);
        if (data != null && data.Rows.Count > 0)
        {
            labDbStartDAte.Text = "" + data.Rows[0].ItemArray[0];
            labDbEndDate.Text = "" + data.Rows[0].ItemArray[1];
            labDbMoney.Text = ((Decimal)data.Rows[0].ItemArray[2]).ToString("n");
            labDbSaleMoney.Text = (((Decimal)data.Rows[0].ItemArray[3])+20).ToString("n");//实际售卡金额要加20元
        }
    }



    // 读数据库处理
    protected void btnDBread_Click(object sender, EventArgs e)
    {
        // 读取卡片类型
        readCardType(txtCardNo.Text, labCardType);

        // 读取卡片状态

        ASHelper.readCardState(context, txtCardNo.Text, txtCardState);

        // 读取客户信息
        readCustInfo(txtCardNo.Text,
            txtCustName, txtCustBirth,
            selPaperType, txtPaperNo,
            selCustSex, txtCustPhone,
            txtCustPost, txtCustAddr, txtEmail, txtRemark, true);

        // 读取其他信息
        DataTable data = SPHelper.callQuery("SP_CG_Query", context, "QryGashInfo", txtCardNo.Text);
        if (data != null && data.Rows.Count > 0)
        {
            labDbStartDAte.Text = "" + data.Rows[0].ItemArray[0];
            labDbEndDate.Text = "" + data.Rows[0].ItemArray[1];
            labDbMoney.Text = ((Decimal)data.Rows[0].ItemArray[2]).ToString("n");
            labDbSaleMoney.Text = (((Decimal)data.Rows[0].ItemArray[3]) + 20).ToString("n");//实际售卡金额要加20元
        }
    }
}
