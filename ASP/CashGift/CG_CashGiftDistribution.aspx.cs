﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using TDO.UserManager;
using System.Text;
using PDO.UserCard;
using Master;
using PDO.Financial;

//金福卡分配处理

public partial class ASP_CashGift_CG_CashGiftDistribution : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 建立临时表

        UserCardHelper.createTempTable(context);

        // 初始化操作类型

        selCardState.Items.Add(new ListItem("04:分配", "04"));
        selCardState.Items.Add(new ListItem("05:取消分配", "05"));

        // 初始化分配员工
        UserCardHelper.selectDistStaff(context, selInStaff, false);

        UserCardHelper.resetData(gvResult, null);

        if (HasOperPower("201008"))//全网点主管
        {
            //查询区域部门、员工
            InitDepartList(seldisDepart);
            InitStaffList(seldisStaff, seldisDepart.SelectedValue);
            seldisStaff.SelectedValue = context.s_UserID;

            //操作区域所属部门、所属员工
            InitDepartList(selInDepart);
            InitStaffList(selInStaff, selInDepart.SelectedValue);
            selInStaff.SelectedValue = context.s_UserID;
        }

        else if (HasOperPower("201007"))
        {
            //查询区域部门、员工
            seldisDepart.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
            seldisDepart.SelectedValue = context.s_DepartID;
            seldisDepart.Enabled = false;
            InitStaffList(seldisStaff, seldisDepart.SelectedValue);
            seldisStaff.SelectedValue = context.s_UserID;

            //操作区域所属部门、所属员工
            selInDepart.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
            selInDepart.SelectedValue = context.s_DepartID;
            selInDepart.Enabled = false;
            InitStaffList(selInStaff, selInDepart.SelectedValue);
            selInStaff.SelectedValue = context.s_UserID;
        }
    }

    private bool HasOperPower(string powerCode)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tmTMTableModule.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    //初始化部门
    private void InitDepartList(DropDownList selDepart)
    {
        selDepart.Items.Add(new ListItem("---请选择---", ""));

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TD_M_INSIDEDEPARTITEM";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count == 0)
            return;

        for (int i = 0; i < data.Rows.Count; i++)
        {
            selDepart.Items.Add(new ListItem(data.Rows[i]["DEPARTNAME"].ToString(), data.Rows[i]["DEPARTNO"].ToString()));
        }
        selDepart.SelectedValue = context.s_DepartID;
    }

    private void InitStaffList(DropDownList selStaff, string deptNo)
    {
        if (deptNo == "")
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
            selStaff.SelectedValue = context.s_UserID;
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }

    protected void selDisDept_Changed(object sender, EventArgs e)
    {
        InitStaffList(seldisStaff, seldisDepart.SelectedValue);
    }

    protected void selInDepart_Changed(object sender, EventArgs e)
    {
        InitStaffList(selInStaff, selInDepart.SelectedValue);
    }

    // 输入项判断处理

    private void InputValidate()
    {
        Validation valid = new Validation(context);
        // 对卡号进行校验
        if (!string.IsNullOrEmpty(txtCardNo.Text.Trim()))
        {
            bool bl = valid.fixedLength(txtCardNo, 16, "A094781100:卡号不为16位");
            if (bl) valid.beNumber(txtCardNo, "A094781101:卡号必须为数字");
        }

        //校验分配日期
        if (!string.IsNullOrEmpty(txtDistDate.Text.Trim()))
        {
            bool bl = Validation.isDate(txtDistDate.Text.Trim());
            if (!bl)
            {
                context.AddError("A094781102: 分配日期格式不正确",txtDistDate);
            }
        }

        //校验数量
        if (!string.IsNullOrEmpty(txtSum.Text.Trim()))
        {
            valid.beNumber(txtSum, "A094781103:数量必须为数字");
        }

        //校验日期
        string strBeginDate = txtRecyBeginDate.Text.Trim();
        string strEndDate = txtRecyEndDate.Text.Trim();
        DateTime? beginDate = null;
        DateTime? endDate = null;
        bool bl1 = false;
        bool bl2 = false;
        if (strBeginDate != "")
        {
            bl1 = Validation.isDate(strBeginDate,"yyyyMMdd");

            if (bl1)
            {
                beginDate = DateTime.ParseExact(strBeginDate, "yyyyMMdd", null);
            }
            else
            {
                context.AddError("A094781104:起始日期格式不正确", txtRecyBeginDate);
            }
        }

        if (strEndDate != "")
        {
            bl2 = Validation.isDate(strEndDate,"yyyyMMdd");
            if (bl2)
            {
                endDate = DateTime.ParseExact(strEndDate, "yyyyMMdd", null);
            }
            else
            {
                context.AddError("A094781105:终止日期格式不正确", txtRecyEndDate);
            }
        }

        if (bl1 == true && bl2 == true)
        {
            if (beginDate.Value.CompareTo(endDate.Value) > 0)
            {
                context.AddError("A094781106:起始日期不能大于终止日期");
            }
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        InputValidate();
        if (context.hasError()) return;

        // 查询用户卡资料表
        DataTable data = SPHelper.callQuery("SP_CG_Query", context, "QryDistrition", txtCardNo.Text, txtDistDate.Text, txtSum.Text == "" ? "99999999" : txtSum.Text,
            txtRecyBeginDate.Text, txtRecyEndDate.Text, seldisDepart.SelectedValue, seldisStaff.SelectedValue, selCardState.SelectedValue);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        btnSubmit.Enabled = data.Rows.Count > 0;

        UserCardHelper.resetData(gvResult, data);

        //校验查询出来的数量是否小于需要的数量
        if (txtSum.Text != "")
        {
            if (data.Rows.Count != 0 && data.Rows.Count < int.Parse(txtSum.Text.Trim()))
            {
                AddMessage("N005030002：库中已有" + (selCardState.SelectedValue == "04" ? "待分配" : "待取消分配") + "的金福卡数量为" + data.Rows.Count);
            }
        }
    }

    // 选中当前页所有用户卡
    protected void CheckAll(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }

    // gridview分页处理
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    // 清空卡号列表临时表

    private void clearTempTable()
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_UC_CardNoList where SessionId='"
            + Session.SessionID + "'");
        context.DBCommit();
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (selInDepart.SelectedValue == "")
        {
            context.AddError("A094781107：请选择领用部门", selInDepart);
        }

        if (selInStaff.SelectedValue == "")
        {
            context.AddError("A094781108：请选择领用员工", selInStaff);
        }

        if (context.hasError()) return;

        // 首先清空临时表

        clearTempTable();

        context.DBOpen("Insert");

        // 根据页面卡号选中生成临时表数据

        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (cb != null && cb.Checked)
            {
                ++count;
                context.ExecuteNonQuery("insert into TMP_UC_CardNoList values('"
                    + Session.SessionID + "','" + gvr.Cells[1].Text + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A002P03R01: 没有选中任何行");
            return;
        }

        if (selCardState.SelectedValue == "04") // 分配
        {
            // 调用分配存储过程
            context.SPOpen();
            context.AddField("P_sessionId").Value = Session.SessionID;
            context.AddField("p_assignedDepart").Value = selInDepart.SelectedValue;
            context.AddField("p_assignedStaff").Value = selInStaff.SelectedValue;
            bool ok = context.ExecuteSP("SP_CG_Distribution");

            if (ok) AddMessage("D002P03001: 分配成功");
        }
        else // 取消分配
        {
            // 调用取消分配存储过程
            context.SPOpen();
            context.AddField("P_sessionId").Value = Session.SessionID;
            bool ok = context.ExecuteSP("SP_CG_UnDistribution");

            if (ok) AddMessage("D002P03002: 取消分配成功");
        }

        clearTempTable();
        UserCardHelper.resetData(gvResult, null);
    }


    // 选择“操作类型”

    protected void selCardState_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);

        bool dist = selCardState.SelectedValue == "01";

        selInDepart.Enabled = dist;
        selInStaff.Enabled = dist;
    }
}
