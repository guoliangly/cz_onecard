﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_ChangeUserInfo.aspx.cs" Inherits="ASP_ResidentCard_ChangeUserInfo" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>信息变更</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <link href="../../css/photo.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function isManager() 
        {
        
            if(confirm('确定要提交审批吗?'))
            { 
                $get("btnChangeUserInfo").click();
                
            }
            return false;
//            if ($get("hidIsManager").value == "1") {
               
//            }
//            else 
//            {
//                changeUserInfoWarnCheck();
//            }
           //return false;
        }
    </script>
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            市民卡->信息变更</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ScriptManager1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" RenderMode="inline">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%"><div align="right">证件类型:</div></td>
                                <td width="13%"><asp:DropDownList ID="selOldPapertype" CssClass="input" runat="server"></asp:DropDownList></td>
                                <td style="width: 5%" align="right">证件号码:</td>
                                <td style="width: 13%"><asp:TextBox runat="server" ID="txtIDCardNo" MaxLength="20" CssClass="input" Style="width: 140px" /></td>
                                <%--<td style="width: 13%"><asp:Button ID="txtReadPaper" Text="读二代证" CssClass="button1" runat="server"  OnClientClick="readSingleID('txtIDCardNo')"  /> </td>--%>
                                <%--<td style="width: 10%" align="right">处理状态:</td>
                                <td style="width: 13%"><asp:DropDownList runat="server" ID="selDealState" CssClass="input" /></td>--%>
                                <td style="width: 13%"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" /></td>
                            </tr>
                            <tr>
                                <!-- 
                                <td style="width: 5%" align="right">单位名称:</td>
                                <td colspan="3">
                                    <asp:TextBox runat="server" ID="txtCorpName" CssClass="input" MaxLength="50" Style="width: 140px" />&nbsp;
                                    <asp:Button runat="server" Text="-- >" ID="btnCorpQry"  />
                                    <asp:DropDownList ID="selCorp" runat="server" /></td>
                                    -->
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="con">
                    <div class="pip">
                        修改信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="140">
                                    <asp:TextBox ID="txtCusname" CssClass="input" MaxLength="25" runat="server"></asp:TextBox></td>
                                <td width="9%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustbirth" CssClass="input" runat="server" MaxLength="10" />
                                    <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtCustbirth"
                                        Format="yyyy-MM-dd" />
                                </td>
                                <td width="9%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selPapertype" CssClass="input" runat="server" Enabled= "false">
                                    </asp:DropDownList></td>
                                <td width="9%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustpaperno" CssClass="inputmid" MaxLength="20" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selCustsex" CssClass="input" runat="server">
                                    </asp:DropDownList></td>
                                <td>
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustphone" CssClass="input" MaxLength="20" runat="server"></asp:TextBox></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustpost" CssClass="input" MaxLength="6" runat="server"></asp:TextBox></td>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustaddr" CssClass="input" MaxLength="50" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <%--<td>
                                    <div align="right">
                                    电子邮件:</td>
                                <td>--%>
                                   <%-- <asp:TextBox ID="txtEmail" CssClass="input" MaxLength="30" runat="server"></asp:TextBox></td>--%>
                                <td valign="top">
                                    <div align="right">
                                        备注 :</div>
                                </td>
                                <td colspan="4">
                                    <asp:TextBox ID="txtRemark" CssClass="inputlong" MaxLength="50" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:Button ID="txtReadPaper" Text="读二代证" CssClass="button1" runat="server"  OnClientClick="readIDCardEx('txtCusname', 'selCustsex', 'txtCustbirth', 'selPapertype', 'txtCustpaperno', 'txtCustaddr')"  />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <%--<td><asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClick="btnPrint" /></td>--%>
                            <td>
                                <asp:HiddenField runat="server" ID="hidWarning" />
                                <asp:HiddenField runat="server" ID="hiddenCheck"/>
                                <asp:HiddenField runat="server" ID="hidIsManager"/>
                                <asp:HiddenField runat="server" ID="hidOldCardNo"/>
                                <asp:HiddenField runat="server" ID="hidOldName"/>
                                <%--<asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click"/>--%>
                                <asp:LinkButton runat="server" ID="btnChangeUserInfo"  OnClick="btnChangeUserInfo_Click" />
                                <asp:Button ID="btnIsManager" CssClass="button1" runat="server" Text="提交审批" OnClientClick="return isManager();" Enabled="false" /></td>
                                <%--<asp:Button ID="btnIsManager" CssClass="button1" runat="server" Text="修改" OnClick="btnIsManager_Click" /></td>--%>
                                <%--<asp:Button ID="btnChangeUserInfo" CssClass="button1" runat="server" Text="修改" OnClick="btnChangeUserInfo_Click" /></td>--%>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
