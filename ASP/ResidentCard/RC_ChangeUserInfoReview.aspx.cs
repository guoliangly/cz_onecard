﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.GroupCard;
using TM;

// 企福通财务审核处理

public partial class ASP_ResidentCard_RC_ChangeUserInfoReview : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        //初始化审批状态
        InitStatus();


        gvResult.DataKeyNames = new string[] { "tradeid", "opapertypecode", "oidcardno", "nidcardno","npapertypecode", "name", "oname", 
            "birthdate", "telephone", "homeaddress", "postalcode", "sexcode", "rpapercode","sex","remark" };

        

        // 创建资料审核数据列表

        createGridViewData();
    }


    private void InitStatus()
    {
        selApprovalStatus.Items.Add(new ListItem("0:未审批", "0"));
        selApprovalStatus.Items.Add(new ListItem("1:审批通过", "1"));
        selApprovalStatus.Items.Add(new ListItem("2:审批作废", "2"));
        selApprovalStatus.SelectedValue = "0";
    }

    // 创建资料审核数据列表

    private void createGridViewData()
    {
        // 从资料变更审核表查询需要审核的数据
        DataTable data = SPHelper.callResidentCardQuery(context, "CustomerRecReview", "0");
        //if (data == null || data.Rows.Count < 1)
        //{
        //    context.AddError("没有未审批的记录");
        //}
        UserCardHelper.resetData(gvResult, data);

        bool enabled = (data.Rows.Count > 0);

        chkApprove.Enabled = enabled;
        chkReject.Enabled = enabled;

        chkApprove.Checked = false;
        chkReject.Checked = false;
        btnSubmit.Enabled = false;
    }

    // 选中gridview当前页所有数据

    //protected void CheckAll(object sender, EventArgs e)
    //{
    //    CheckBox cbx = (CheckBox)sender;
    //    foreach (GridViewRow gvr in gvResult.Rows)
    //    {
    //        CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
    //        ch.Checked = cbx.Checked;
    //    }
    //}

    // gridview换页事件
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //判断是否选中了行
        //if (gvResult.SelectedIndex < 0)
        //{
        //    context.AddError("请先选中某一行");
        //    return;
        //}
        if (chkApprove.Checked)
        {
            submitApproval("1", "D004P03001: 审核通过成功");   // 通过
        }
        else
        {
            submitApproval("2", "D004P03002: 审核作废成功");   // 作废
        }
    }

    // 调用信息更改审核存储过程

    private void submitApproval(string stateCode, string okMsgCode)
    {

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);

        //插入临时表
        context.DBOpen("Insert");
        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("chkCust");
            if (cb != null && cb.Checked)
            {
                ++count;
                String tradeid = getDataKeys2("tradeid", gvr.RowIndex);
                String OPAPERTYPECODE = getDataKeys2("opapertypecode", gvr.RowIndex);
                String OIDCARDNO = getDataKeys2("oidcardno", gvr.RowIndex);
                String NPAPERTYPECODE = getDataKeys2("npapertypecode", gvr.RowIndex);
                String NIDCARDNO = getDataKeys2("nidcardno", gvr.RowIndex);
                String ONAME = getDataKeys2("oname", gvr.RowIndex);
                String name = getDataKeys2("name", gvr.RowIndex);
                String SEX = getDataKeys2("sex", gvr.RowIndex);
                String BIRTHDATE = getDataKeys2("birthdate", gvr.RowIndex);
                String TELEPHONE = getDataKeys2("telephone", gvr.RowIndex);
                String HOMEADDRESS = getDataKeys2("homeaddress", gvr.RowIndex);
                String POSTALCODE = getDataKeys2("postalcode", gvr.RowIndex);
                String RESULT = stateCode;
                String REMARK = getDataKeys2("remark", gvr.RowIndex);
                context.ExecuteNonQuery("insert into TMP_COMMON_NEW (F0,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,F13,SESSIONID) "
               + " values('" + tradeid + "','" + OPAPERTYPECODE + "','" + OIDCARDNO + "','" + NPAPERTYPECODE + "','" + NIDCARDNO + "','"
               + ONAME + "','" + name + "','" + SEX + "','" + BIRTHDATE + "','" + TELEPHONE + "','" + HOMEADDRESS + "','" + POSTALCODE + "','"
               + RESULT + "','" + REMARK + "','" + Session.SessionID + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A001002212");
            return;
        }

        // 调用信息更改审核存储过程
        context.SPOpen();
        context.AddField("P_SESSIONID").Value = Session.SessionID;
        //context.AddField("P_OIDCARDNO").Value = getDataKeys2("oidcardno");

        //context.AddField("P_RESULT").Value = stateCode;


        bool ok = context.ExecuteSP("SP_RC_CHANGERESIDENTRECREVIEW");

        if (ok) AddMessage(okMsgCode);

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);

        createGridViewData();
    }

    // 通过 复选框 改变事件
    protected void chkApprove_CheckedChanged(object sender, EventArgs e)
    {
        if (chkApprove.Checked)
        {
            chkReject.Checked = false;
        }
        btnSubmit.Enabled = (chkApprove.Checked || chkReject.Checked);
    }

    // 作废 复选框 改变事件
    protected void chkReject_CheckedChanged(object sender, EventArgs e)
    {
        if (chkReject.Checked)
        {
            chkApprove.Checked = false;
        }
        btnSubmit.Enabled = (chkApprove.Checked || chkReject.Checked);
    }

    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    //注册行单击事件

        //    e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvResult','Select$" + e.Row.RowIndex + "')");
        //}
    }

    protected void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
    }


    public String getDataKeys2(String keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    public String getDataKeys2(String keysname, int rowIndex)
    {
        return gvResult.DataKeys[rowIndex][keysname].ToString();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        DataTable data = SPHelper.callResidentCardQuery(context, "CustomerRecReview", selApprovalStatus.SelectedValue);
        if (data == null || data.Rows.Count < 1)
        {
            if (selApprovalStatus.SelectedValue == "0")
            {
                context.AddError("没有未审批的记录");
            }
            if (selApprovalStatus.SelectedValue == "1")
            {
                context.AddError("没有审批通过的记录");
            }
            else if (selApprovalStatus.SelectedValue == "2")
            {
                context.AddError("没有审批作废的记录");
            }
        }
        UserCardHelper.resetData(gvResult, data);
        if (selApprovalStatus.SelectedValue != "0")
        {
            chkApprove.Enabled = false;
            chkReject.Enabled = false;
            btnSubmit.Enabled = false;
        }
        else
        {
            chkApprove.Enabled = true;
            chkReject.Enabled = true;
            chkApprove.Checked = false;
            chkReject.Checked = false;
            btnSubmit.Enabled = false;
        }
    }

}
