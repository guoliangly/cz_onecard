﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.CardManager;
using TM;
using Common;
using System.IO;
using System.Text;
using Master;

// 企福通批量开卡

public partial class ASP_ResidentCard_RC_CustomerRecImport : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 首先清空临时表

        GroupCardHelper.clearTempTable(context, Session.SessionID);
        
        // 清除gridview数据
        clearGridViewData();
    }

    // 输入校验
    private void SubmitValidate()
    {
        Validation valid = new Validation(context);
    }

    private void createCustInfoGrid()
    {
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryUploadCustomerInfo", Session.SessionID);
        UserCardHelper.resetData(gvResult, data);
        btnSubmit.Enabled = true;
    }
    // 清除gridview数据
    private void clearGridViewData()
    {
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
        btnSubmit.Enabled = false;
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 输入校验
        SubmitValidate();
        if(context.hasError())return;

        // 调用个人资料更新存储过程

        context.DBOpen("StorePro");

        context.AddField("p_sessionID").Value = Session.SessionID;

        bool ok = context.ExecuteSP("SP_RC_UPDATERESIDENTCUSTPHONE");
        if (ok)
        {
            AddMessage("个人信息资料更新成功");
        }

        clearGridViewData();
        GroupCardHelper.clearTempTable(context, Session.SessionID);
    }

    // 批量开户文件上传处理

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            clearGridViewData();

            ResidentCardHelper.UploadCustomerInfoFile(context, FileUpload1, Session.SessionID);

            if (context.hasError())
            {
                return;
            }
            createCustInfoGrid();
        }
        catch
        {

        }
        finally
        {
            try
            {
                if (FileUpload1.HasFile && File.Exists(context.ResourcePath + FileUpload1.FileName))
                {
                    File.Delete(context.ResourcePath + FileUpload1.FileName);
                }
            }
            catch
            { }
        }
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
    }

    // gridview换页处理
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        createCustInfoGrid();
    }
}
