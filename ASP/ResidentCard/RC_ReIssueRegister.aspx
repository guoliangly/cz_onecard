﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_ReIssueRegister.aspx.cs" Inherits="ASP_ResidentCard_ReIssueRegister" %>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>补卡登记</title>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/photo.css" rel="stylesheet" type="text/css" />    
	<script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
       <div class="tb">市民卡->补卡登记</div>
       
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script> 
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
  
         <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
         
         <div class="con">
           <div class="kuang5">
           <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
              <tr>
                 <td width="10%"><div align="right">证件类型:</div></td>
                 <td width="13%"><asp:DropDownList ID="selPapertype" CssClass="input" runat="server"></asp:DropDownList></td>
                 <td width="10%"><div align="right">证件证号:</div></td>
                 <td width="15%"><asp:Textbox ID="txtIDCardNo" CssClass="inputmid" runat="server" Text=""></asp:Textbox></td>
                 <td><span class="red">*</span></td>
                 <td width="13%"><input type="button" class="button1" value="读二代证"   onclick="readSingleID('txtIDCardNo')"/></td>
                 <td width="10%"><div align="right">&nbsp;</td>
                 <td width="23%" align="right"><asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" OnClick="btnQuery_Click"/></td>
               </tr>
           </table>
         </div>
         </div>
         
         <div class="con">
           <div class="jieguo">用户信息</div>
          <div class="kuang5">
            <div class="gdtb" style="height:170px">
                     <asp:GridView ID="gvResult" runat="server"
                    Width = "98%"
                    CssClass="tab1"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="false"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="true"
                    OnRowDataBound="gvResult_RowDataBound"
                    EmptyDataText="没有数据记录!">
                        <Columns> 
                        <asp:TemplateField>
                             <ItemTemplate>
                             <asp:LinkButton ID="LinkButton1" OnCommand ="LinkButton1_Click" CommandArgument ='<%# Eval("证件号码") +":" + Container.DataItemIndex + ":"+Eval("证件类型")%>'  runat="server">选择</asp:LinkButton>
                             </ItemTemplate>
                        </asp:TemplateField>     

                        </Columns>           

                    </asp:GridView>
            </div>

          </div>
          </div>
          
          
          <!--
           <div class="basicinfo"   style=" height:195px;">
          <div class="info">卡信息</div>
             <table  border="0" cellpadding="0" cellspacing="0" class="text25">
             <tr><td  width="10">
             </td><td>
                   <div class="photo" >
                   <asp:Panel ID="div0" Visible="false" runat="server">
                   <table width="243" border="0" align="center" cellpadding="0" cellspacing="0" >
                     <tr>
                       <td width="64" height="25">&nbsp;</td>
                       <td width="20">&nbsp;</td>
                       <td width="69">&nbsp;</td>
                       <td width="90" rowspan="4"><asp:Image runat="server" ID="image0" Width="91" Height="112" /></td>
                     </tr>
                     <tr>
                       <td><asp:Label runat="server" ID="name0" /></td>
                       <td><asp:Label runat="server" ID="sex0" /></td>
                       <td align="center"><asp:Label runat="server" ID="size0" /></td>
                      </tr>
                     <tr>
                       <td height="27" colspan="3"><asp:Label runat="server" ID="idcardno0" /></td>
                     </tr>
                     <tr>
                       <td colspan="3"><asp:Label runat="server" ID="addr0" /></td>
                     </tr>
                     </table> 
                     </asp:Panel>
                     </div>
             </td></tr></table>
             
           </div>

      
          -->
        
                 
          <div class="kuang5"  >
            <div class="money">费用信息</div>
            <div class="kuang5" >
            <table width="100%"><tr><td>
                <table width="180" border="0" cellpadding="0" cellspacing="0" class="tab1">
                    <tr class="tabbt">
                        <td width="66">
                            费用项目</td>
                        <td width="94">
                            费用金额(元)</td>
                    </tr>
                    <tr>
                        <td>
                            工本费</td>
                        <td>
                            <asp:Label ID="DepositFee" runat="server" Text="">30.00</asp:Label></td>
                    </tr>
                    <tr class="tabjg">
                        <td>
                           &nbsp; </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                  
                    <tr class="tabjg">
                        <td>
                            合计应收</td>
                        <td>
                            <asp:Label ID="Total" runat="server" Text=""></asp:Label>
                            <span class="red"><asp:Label runat="server" ID="labFee" Text="30.00" /></span> &nbsp; 元</td>
                    </tr>
                </table>
            </div>
            </td>
            
            <td> <div class="left">
                        <img src="../../Images/show-change.JPG" alt="补卡登记"  height="100"/></div></td>
            
            <td align="right">
           
             </td></tr></table>
            
     </div>
     
</div>
        
        
        <div class="btns">
             <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
          <td><asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false" /></td>
            <td><asp:Button ID="btnSubmit" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click" /></td>
          </tr>
        </table>
        <td><asp:CheckBox ID="chkPingzheng" runat="server" Checked="true" />自动打印凭证</td>
        </div>
         
       </ContentTemplate>
     </asp:UpdatePanel>
         
     
    </form>
</body>
</html>
