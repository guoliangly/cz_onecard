﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_StockOut.aspx.cs" Inherits="ASP_UserCard_RC_StockOut" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>市民卡出库</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../js/mootools.js"></script>
        <script type="text/javascript" src="../../js/myext.js"></script>
    <script type="text/javascript">

        /* 质朴长存法  by lifesinger */
        function pad(num, n) {
            var len = num.toString().length;
            while (len < n) {
                num = "0" + num;
                len++;
            }
            return num;
        }

        function Change() {
            var beginNo = $get('txtFromCardNo').value;

            if (beginNo.test("^\\s*\\d{16}\\s*$") == false) {
                $get('txtFromCardNo').value = "";
                return false ;
            }

            if ($get('txtCardSum').value != "") {
                return txtCountControlChange();
            }
            else {
                return false;
            }
        }
        function txtCountControlChange() {
            var count = $get('txtCardSum').value;
            var beginNo = $get('txtFromCardNo').value;

            if (count.test("^\\s*\\d+\\s*$") == false) {
                $get('txtCardSum').value = "";
                return false;
            }
            if (Number(count) > 10000) {
                alert("最多只能出库10000张");
                return false;
            }
            var num1 = Number(count) + Number(beginNo.substr(8, 8)) - 1;
            if(num1.toString().length>8)
            {
                alert("起始卡号后8位加数量的长度不能超过8位");
                return false;
            }
            var endNo = beginNo.substring(0, 8) + pad(num1, 8);
            if (Number(count) == 1) {
                $get('txtToCardNo').value = beginNo;
            }
            else
                $get('txtToCardNo').value = endNo;
            return true;
        }


        function submitConfirm() {
            if(!Change())
                return false;
            var selStaff = "";
            var obj = document.getElementById("selAssignedStaff");
            if (obj.value != "") {
                for (i = 0; i < obj.length; i++) {
                    if (obj.options[i].selected) {
                        selStaff = obj.options[i].text + "";
                        break;
                    }
                }
            }
            if ($get('txtFromCardNo').value != ""
                && $get('txtCardSum').value != ""
                && selStaff != "") {
                MyExtConfirm('确认',
		        '起始卡号为： ' + $get('txtFromCardNo').value + '<br>' +
		        '终止卡号为： ' + $get('txtToCardNo').value + '<br>' +
		        '出库数量： ' + $get('txtCardSum').value + '<br>' +
		        '领用人为：' + selStaff + '<br>' +
		        '是否确认？'
		        , submitConfirmCallback);
                return false;
            }
            return true;
        }
        function submitConfirmCallback(btn) {
            if (btn == 'yes') {
                $get('btnConfirm').click();
            }
        }
    
        
    </script>
</head>
<body>
    <form id="form1" runat="server">

<div class="tb">
市民卡管理->出库
</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

<div class="con">
  <div class="card">市民卡批量出库</div>

  <div class="kuang5">
                        <table class="text25" cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tbody>
                                <tr>
                           
                                    <td style="width: 10%; height: 25px" align="right">
                                        起讫卡号:
                                    </td>
                                    <td style="width: 40%; height: 25px" valign="middle">
                                        <asp:TextBox ID="txtFromCardNoQ" CssClass="input" runat="server" MaxLength="16"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtToCardNoQ" CssClass="input" runat="server" MaxLength="16"></asp:TextBox>
                                    </td>
                                    <td style="width: 5%; height: 25px" align="right">
                                        &nbsp;</td>
                                    <td style=" height: 25px">
                                        <asp:Button ID="btnQuery" OnClick="btnQuery_Click" runat="server" CssClass="button1"
                                            Text="查询"></asp:Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 300px">
                            <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="False" Width="98%"
                                CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" EmptyDataText="没有数据记录!" AllowPaging="True" PageSize="20"
                                OnPageIndexChanging="gvResult_PageIndexChanging">
                                <PagerSettings Mode="NumericFirstLast" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                            </asp:GridView>
                        </div>
                    </div>


  <div class="kuang5">
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
  <tr>
    <td><div align="right">起始卡号:</div></td>
    <td colspan="1">
    <asp:TextBox ID="txtFromCardNo" CssClass="inputmid" runat="server" MaxLength="16"></asp:TextBox>
      <input type="hidden" runat="server" id="txtToCardNo" value="" />
       <span class="red">*</span></td>
      <td><div align="right">出库数量:</div></td>
    <td><asp:TextBox ID="txtCardSum" CssClass="input" runat="server"></asp:TextBox></td>
   <td><div align="right">服务费:</div></td>
    <td><asp:TextBox ID="txtServiceFee" CssClass="input" runat="server" MaxLength="13"></asp:TextBox>
      <span class="red">*</span> 100分</td>
    
  </tr>
  <tr>
   <td><div align="right">领用部门:</div></td>
       <td> <asp:DropDownList CssClass="input" ID="selDept" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="selDept_Changed">
                                    </asp:DropDownList></td>
    <td><div align="right">领用员工:</div></td>
    <td>
    <asp:DropDownList ID="selAssignedStaff" CssClass="input" runat="server"></asp:DropDownList>
    </td>
      <td><div align="right">服务周期:</div></td>
     <td>
        <asp:DropDownList ID="selServiceCycle" CssClass="input" runat="server">
        </asp:DropDownList>
    </td>
   
  </tr>
  <tr>
    <td><div align="right">退值:</div></td>
    <td colspan="5">
        <asp:DropDownList ID="selRetValMode" CssClass="input" runat="server">
        </asp:DropDownList>
    
    </td>
<%--    <td><div align="right">条件　　:</div></td>
    <td>      
        <asp:DropDownList ID="selRetValCond" CssClass="input" runat="server">
        </asp:DropDownList>
      </td>
    <td><div align="right">数值　　:</div></td>
    <td><asp:TextBox ID="txtRetVal" CssClass="input" runat="server" MaxLength="13"></asp:TextBox>
      <span class="red">*</span> 100分</td>
--%>  </tr>
</table>

 </div>
  </div>
<div class="btns">
     <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr>
    <td><asp:Button ID="btnSubmit" CssClass="button1" runat="server" Text="提交"  OnClientClick="return submitConfirm()"/>
    <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnSubmit_Click" />
    </td>
  </tr>
</table>

</div>    
            </ContentTemplate>
        </asp:UpdatePanel>    
    </form>
</body>
</html>
