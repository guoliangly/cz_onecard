﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_CustomerRecImport.aspx.cs" Inherits="ASP_ResidentCard_RC_CustomerRecImport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>资料信息文件导入</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="tb">
            市民卡->资料信息导入
        </div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ScriptManager1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                        信息文件导入</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td style="width:8%" align="right">
                                        导入文件:</div>
                                </td>
                                <td style="width:72%">
                                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="inputlong" Width="70%" />
                                    <asp:Button ID="btnUpload" CssClass="button1" runat="server" Text="导入" OnClick="btnUpload_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width:8%">
                                    <div align="right">
                                        格式说明:</div>
                                </td>
                                <td align="left">
                                    导入.xls格式文件,文件一共四列 依次是身份证号、姓名、联系电话、手机号码<%--,请下载模板文件填写
                                    <asp:Button ID="btnTemplateFileDownLoad" runat="server" Text="下载模板" />--%>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                    <div class="jieguo">
                        资料信息</div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 360px">
                            <asp:GridView ID="gvResult" runat="server" Width="98%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                                AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" AllowPaging="true" PageSize="15"
                                PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="true" OnRowDataBound="gvResult_RowDataBound" OnPageIndexChanging="gvResult_Page" >
                                <Columns>
                                    
                                    <%--<asp:BoundField HeaderText="身份证号码" DataField="CardNo" />
                                    <asp:BoundField HeaderText="姓名" DataField="CustName" />
                                    <asp:BoundField HeaderText="联系电话" DataField="CustSex" />
                                    <asp:BoundField HeaderText="手机号码" DataField="CustBirth" />--%>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                        <tr class="tabbt">
                                            
                                            <td>
                                                身份证号码</td>
                                            <td>
                                                姓名</td>
                                            <td>
                                                联系电话</td>
                                            <td>
                                                手机号码</td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="btns">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="70%">
                                &nbsp;</td>
                            <td align="right">
                                <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                    OnClick="btnSubmit_Click" /></td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
