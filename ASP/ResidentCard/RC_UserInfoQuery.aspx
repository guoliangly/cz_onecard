﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_UserInfoQuery.aspx.cs"
    Inherits="ASP_ResidentCard_RC_UserInfoQuery" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>个人资料查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function isManager() 
        {
            if ($get("hidIsManager").value == "1") {
                $get("btnChangeUserInfo").click();
            }
            else 
            {
                changeUserInfoWarnCheck();
            }
            return false;
        }
    </script>
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            市民卡->个人资料查询
        </div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="card">
                        卡片信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                            <td width="10%"><div align="right">证件类型:</div></td>
                                <td width="13%"><asp:DropDownList ID="selPapertype" CssClass="input" runat="server"></asp:DropDownList></td>
                                <td width="3%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td width="12%" align="left">
                                    <asp:TextBox ID="txtIDCardno" CssClass="inputmid" MaxLength="20" runat="server"></asp:TextBox></td>
                                <td width="6%">
                                    <asp:Button ID="btnReadCard" CssClass="button1" runat="server" Text="查询" 
                                        OnClick="btnReadCard_Click" /></td>
                            </tr>
                            
                        </table>
                    </div>
                    <div class="pip">
                        个人信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="11%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustName" runat="server" Text=""></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td width="8%">
                                    <asp:Label ID="Papertype" runat="server" Text=""></asp:Label></td>
                                <td width="8%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td width="23%" colspan="3">
                                    <asp:Label ID="Paperno" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custsex" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custphone" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custpost" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                
                                <td valign="top">
                                    <div align="right">
                                        备注:</div>
                                </td>
                                <td colspan="5">
                                    <asp:Label ID="Remark" runat="server" Text=""></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <div class="footall">
                    </div>
                </div>
                
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
