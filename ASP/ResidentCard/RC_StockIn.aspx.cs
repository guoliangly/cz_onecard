﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using Common;
using TM;
using TDO.ResourceManager;
using TDO.CardManager;
using PDO.UserCard;
using TDO.UserManager;
// 市民卡入库处理


public partial class ASP_ResidentCard_RC_StockIn : Master.Master
{
    // 输入项校验


    private void SubmitValidate()
    {
        Validation valid = new Validation(context);

        // 对起始卡号和结束卡号进行校验
        //UserCardHelper.validateCardNoRangeForUserCard(context, txtFromCardNo, txtToCardNo, true, true);

        ResidentCardHelper.validateCardNoRangeForsmkCard(context, txtFromCardNo, txtToCardNo, true, true);

        //对卡片单价进行非空、数字检验


        UserCardHelper.validatePrice(context, txtUnitPrice, "A002P01009: 卡片单价不能为空", "A002P01010: 卡片单价必须是10.2的格式");

        //对应用版本进行非空、英数字检验


        UserCardHelper.validateAlpha(context, txtAppVersion, "A002P01011: 应用版本不能为空", "A002P01012: 应用版本必须为英文或者数字");

        // 对有效日期范围进行非空、格式校验


        UserCardHelper.validateDateRange(context, txtEffDate, txtExpDate);
    }

    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        txtFromCardNo.Attributes["OnBlur"] = "javascript:return Change();";
        txtToCardNo.Attributes["OnBlur"] = "javascript:return Change();";
        txtUnitPrice.Attributes["OnChange"] = "javascript:return Change('price');";

        setReadOnly(txtCardSum, txtTotal, txtCardType, txtFaceType, txtUnitPrice, txtAppVersion);

        //从COS类型编码表(TD_M_COSTYPE)中读取数据，放入下拉列表中


        UserCardHelper.selectCosType(context, selCosType, false);

        selCosType.SelectedValue = "01";

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据，放入下拉列表中

        //从厂商编码表(TD_M_MANU)中读取数据，放入下拉列表中
        ResidentCardHelper.selectManuFacturer(context, selProducer, false);

        //从IC卡芯片类型编码表(TD_M_CARDCHIPTYPE)中读取数据，放入下拉列表中
        UserCardHelper.selectChipType(context, selChipType, false);

        selProducer_SelectedIndexChanged(sender, e);

        // 服务周期
        selServiceCycle.Items.Add(new ListItem("00:年度", "00"));
        selServiceCycle.Items.Add(new ListItem("01:季度", "01"));
        selServiceCycle.Items.Add(new ListItem("02:月份", "02"));
        selServiceCycle.Items.Add(new ListItem("03:天数", "03"));
        selServiceCycle.Items[2].Selected = true;

        // 退值（模式）

        selRetValMode.Items.Add(new ListItem("0:不退值", "0"));
        //selRetValMode.Items.Add(new ListItem("有条件退值", "1"));
        selRetValMode.Items.Add(new ListItem("2:无条件退值", "2"));
        selRetValMode.Items[1].Selected = true;

        //领用部门
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTTDOIn = new TD_M_INSIDEDEPARTTDO();
        TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTTDOOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTTDOIn, typeof(TD_M_INSIDEDEPARTTDO), null, null, null);
        ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTTDOOutArr, "DEPARTNAME", "DEPARTNO", true);

        //领用部门 领用员工初始化选择当前操作员部门和员工号
        selDept.SelectedValue = context.s_DepartID;

        selDept_Changed(sender, e);

        selAssignedStaff.SelectedValue = context.s_UserID;

        //初始化有效日期
        txtEffDate.Text = DateTime.Now.ToString("yyyyMMdd");
        txtExpDate.Text = DateTime.Now.AddYears(90).ToString("yyyyMMdd");
    }

    /// <summary>
    /// 初始化领用人
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void selDept_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        tdoTD_M_INSIDESTAFFIn.DEPARTNO = selDept.SelectedValue;
        TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFFROLE1010", null);
        ControlDeal.SelectBoxFill(selAssignedStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        if (selAssignedStaff.Items.Count > 1)
        {
            selAssignedStaff.Items[1].Selected = true;
        }
    }

    protected void selProducer_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = SPHelper.callResidentCardQuery(context, "QueryChipType", selProducer.SelectedItem.Text.Split(':')[1].ToString());

        if (dt.Rows.Count != 0)
        {
            selChipType.SelectedValue = dt.Rows[0][0].ToString();
        }
    }

    protected void txtToCardNo_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        ResidentCardHelper.validateCardNoRangeForsmkCard(context, txtFromCardNo, txtToCardNo, true, true);
        if (context.hasError()) return;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = txtToCardNo.Text.Substring(4, 2);
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);
        if (ddoTD_M_CARDTYPEOut == null)
        {
            context.AddError("S002P01I02");
            return;
        }
        txtCardType.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
        hidCardID.Value = ddoTD_M_CARDTYPEOut.CARDTYPECODE;

        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACEIn = new TD_M_CARDSURFACETDO();
        ddoTD_M_CARDSURFACEIn.CARDSURFACECODE = txtToCardNo.Text.Substring(4, 4);
        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACEOut = (TD_M_CARDSURFACETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDSURFACEIn, typeof(TD_M_CARDSURFACETDO), null, "TD_M_CARDSURFACE_CODE", null);
        if (ddoTD_M_CARDSURFACEOut == null)
        {
            context.AddError("S002P01I04");
            return;
        }
        txtFaceType.Text = ddoTD_M_CARDSURFACEOut.CARDSURFACENAME;
        hidFaceID.Value = ddoTD_M_CARDSURFACEOut.CARDSURFACECODE;

    }
    // 入库处理
    protected void btnStockIn_Click(object sender, EventArgs e)
    {
        // 输入校验
        SubmitValidate();
        if (context.hasError()) return;

        //// 调用入库存储过程
        context.SPOpen();
        context.AddField("p_fromCardNo").Value = txtFromCardNo.Text.Trim();
        context.AddField("p_toCardNo").Value = txtToCardNo.Text.Trim();
        context.AddField("p_cosType").Value = selCosType.SelectedValue;
        context.AddField("p_unitPrice").Value = Validation.getPrice(txtUnitPrice);
        context.AddField("p_faceType").Value = txtToCardNo.Text.Substring(4, 4);
        context.AddField("p_cardType").Value = txtToCardNo.Text.Substring(4, 2);
        context.AddField("p_chipType").Value = selChipType.SelectedValue;
        context.AddField("p_producer").Value = selProducer.SelectedValue;
        context.AddField("p_appVersion").Value = txtAppVersion.Text;
        context.AddField("p_effDate").Value = txtEffDate.Text;
        context.AddField("p_expDate").Value = txtExpDate.Text;

        //add for sp_rc_stockout
        context.AddField("p_assignedStaff").Value = selAssignedStaff.SelectedValue;
        context.AddField("p_serviceCycle").Value = selServiceCycle.SelectedValue;
        context.AddField("p_serviceFee").Value = Validation.getPrice(txtServiceFee);
        context.AddField("p_retValMode").Value = selRetValMode.SelectedValue;

        bool ok = context.ExecuteSP("SP_RC_StockIn");
        if (ok) AddMessage("D002P01002: 市民卡出入库成功");
    }
}
