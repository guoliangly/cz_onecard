﻿/***************************************************************
 * RC_Pickup.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 领卡登记 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 ***************************************************************
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using System.IO;


public partial class ASP_ResidentCard_Pickup : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ResidentCardHelper.showPicture(context, Request, Response)) return;
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(gvResult, null);
        
        // 初始化证件类型
        //ASHelper.initPaperTypeList(context, selPaperType);
        btnSubmit.Enabled = false;

        // 初始化单位选择框
        //DataTable dt = SPHelper.callCitizenCardQuery(context, "ReadCorpInfo");
        //GroupCardHelper.fill(selCorp, dt, true);
        selCorp.Items.Add(new ListItem("---请选择---", ""));

        DataTable dt = SPHelper.callResidentCardQuery(context, "ReadDealState");
        GroupCardHelper.fill(selDealState, dt, true);

        CommonHelper.clearTempTable(context);
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        ResidentCardHelper.checkIDCardNo(context, txtIDCardNo, true);

        if (context.hasError()) return;

        btnSubmit.Enabled = false;

        DataTable data;
        if (txtIDCardNo.Text.Length > 0)
        {
            context.DBOpen("Insert");
            context.AddField(":f0").Value = txtIDCardNo.Text;
            int rows = context.ExecuteNonQuery("insert into tmp_common(f0) values(:f0)");
            context.DBCommit();

            data = SPHelper.callResidentCardQuery(context, "QueryResidentCardByTemp");
        }
        else
        {
            data = SPHelper.callResidentCardQuery(context, "QueryResidentCard",
             txtIDCardNo.Text, selCorp.SelectedValue, selDealState.SelectedValue);
        }

        UserCardHelper.resetData(gvResult, data);
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
        }

        resetPickerInfo();
    }

    // 选中gridview中当前页所有数据项
    protected void CheckAll(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (ch.Enabled)
            {
                ch.Checked = cbx.Checked;
            }
        }
    }

    protected void LinkButton1_Click(object sender, CommandEventArgs e)
    {
        char[] splitter = { ';' };
        string arg = e.CommandArgument.ToString();
        string[] args = arg.Split(splitter);

        string idcardno = args[0];

        DataTable dt = ResidentCardHelper.showNameCardInfo(context, image0, "CC_Pickup.aspx",
        idcardno,"", div0, name0, sex0, idcardno0, addr0, size0);

        //chkPickup.Enabled = false;
        //chkPickup.Checked = false;

        if (dt != null && dt.Rows.Count > 0)
        {
            gvResult.SelectedIndex = int.Parse(args[1]);
            CheckBox cb = (CheckBox)gvResult.SelectedRow.FindControl("ItemCheckBox");
            if (cb != null && cb.Visible)
            {
                //chkPickup.Enabled = true;
                cb.Checked = true;
            }
        }
        //div0.Visible = false;

    }

    protected void chk_CheckedChanged(object sender, EventArgs e)
    {
        
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow ||
            e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Visible = false;
        }

        if ( e.Row.RowType == DataControlRowType.DataRow)
        {
            // 已制卡或换卡已制卡状态时才允许领卡
            if (e.Row.Cells[2].Text == "1" || e.Row.Cells[2].Text == "4")
            {
                // 如果是换卡已制卡，查询是否需要提示转值
                if (!btnSubmit.Enabled)
                {
                    btnSubmit.Enabled = true;
                }
            }
            else
            {
                CheckBox cb = (CheckBox)e.Row.FindControl("ItemCheckBox");
                if(cb != null) cb.Visible = false;
            }
        }
    }

    private void resetPickerInfo()
    {
        gvResult.SelectedIndex = -1;
        //div0.Visible = false;
        //txtPickerName.Text = "";
        //selPaperType.SelectedValue = "";
        //txtPickerPhone.Text = "";
        //txtPickerPaperNo.Text = "";
        //txtRemark.Text = "";
        //chkPickup.Checked = false;
        //chkPickup.Enabled = false;
    }

  

    protected void btnCorpQry_Click(object sender, EventArgs e)
    {
        DataTable dt = SPHelper.callResidentCardQuery(context, "ReadCorpInfo", txtCorpName.Text.Trim());
        GroupCardHelper.fill(selCorp, dt, dt.Rows.Count != 1);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
        if (context.hasError()) return;
        
        ArrayList arrIDCards = new ArrayList();

        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (ch != null && ch.Enabled && ch.Checked)
            {
                arrIDCards.Add(gvr.Cells[4].Text);
            }
        }

        if (arrIDCards.Count == 0)
        {
            context.AddError("没有选择卡片");
            return;
        }

        context.SPOpen();
        context.AddField("p_pickupCards").Value = string.Join(",", (string[])arrIDCards.ToArray(typeof(string)));

        context.AddField("p_pickerName").Value = "";
        context.AddField("p_pickerPaperType").Value = "";
        context.AddField("p_pickerPaperNo").Value = "";
        context.AddField("p_pickerPhone").Value = "";
        context.AddField("p_pickerRemark").Value = "";

        context.AddField("p_seq", "string", "Output", "16");

        bool ok = context.ExecuteSP("SP_RC_Pickup");

        if (ok)
        {
            if (!context.hasError()) context.AddMessage("领卡成功");
            btnSubmit.Enabled = false;
            UserCardHelper.resetData(gvResult, null);
            resetPickerInfo();
            CommonHelper.clearTempTable(context);
        }
    }

}
