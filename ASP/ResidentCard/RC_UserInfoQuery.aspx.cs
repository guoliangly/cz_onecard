﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using TM;
using TDO.BusinessCode;
using Common;
using PDO.PersonalBusiness;
using TDO.CardManager;
using TDO.ResourceManager;

public partial class ASP_ResidentCard_RC_UserInfoQuery : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //从社保证件类型编码表中读取数据，放入下拉列表中

            DataTable dt1 = SPHelper.callResidentCardQuery(context, "QuerySociePaper");
            selPapertype.Items.Add(new ListItem("---请选择---", ""));

            Object[] itemArray;
            ListItem li;
            for (int i = 0; i < dt1.Rows.Count; ++i)
            {
                itemArray = dt1.Rows[i].ItemArray;
                li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
                selPapertype.Items.Add(li);
            }
            selPapertype.SelectedValue = "00";
        }
    }

   
    private Boolean ChangeUserInfoDBreadValidation()
    {
        if (selPapertype.SelectedValue == "")
        {
            context.AddError("请选择证件类型", selPapertype);
        }

        //对卡号进行非空、长度、数字检验

        if (txtIDCardno.Text.Trim() == "")
            context.AddError("A001001121", txtIDCardno);
        else
        {
            if (Validation.strLen(txtIDCardno.Text.Trim()) > 20)
                context.AddError("A001004114", txtIDCardno);
            else if (!Validation.isCharNum(txtIDCardno.Text.Trim()))
                context.AddError("A001001122", txtIDCardno);
        }

        return !(context.hasError());

    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        if (ChangeUserInfoDBreadValidation())
        {
            context.DBOpen("Select");
            context.AddField("idcardno").Value = txtIDCardno.Text.Trim();
            context.AddField("papertypecode").Value = selPapertype.SelectedValue;
            string sql =
                    @"select r.name,r.birthdate,p.rpapername,r.idcardno,decode(r.sex,'0','男','1','女',r.sex) sex,
                r.telephone,r.postalcode,r.homeaddress,r.remark
                from tf_residentcard r
                left join  tf_resident_paper p 
                on r.papertypecode = p.papertypecode
                where r.idcardno = :idcardno and r.papertypecode = :papertypecode";
            DataTable dt = context.ExecuteReader(sql);
            context.DBCommit();
            if (dt != null && dt.Rows.Count > 0)
            {
                CustName.Text = dt.Rows[0]["NAME"].ToString();
                CustBirthday.Text = Convert.ToDateTime(dt.Rows[0]["BIRTHDATE"].ToString()).ToString("yyyy-MM-dd");
                Papertype.Text = dt.Rows[0]["rpapername"].ToString();
                Paperno.Text = dt.Rows[0]["idcardno"].ToString();
                Custsex.Text = dt.Rows[0]["sex"].ToString();
                Custphone.Text = dt.Rows[0]["telephone"].ToString();
                Custpost.Text = dt.Rows[0]["postalcode"].ToString();
                Custaddr.Text = dt.Rows[0]["homeaddress"].ToString();
                Remark.Text = dt.Rows[0]["remark"].ToString();
            }
            else
            {
                ClearData();
                context.AddMessage("N007P00001");
            }
        }

    }

    /// <summary>
    /// 清空数据
    /// </summary>
    private void ClearData()
    {
        CustName.Text = "";
        CustBirthday.Text = "";
        Papertype.Text = "";
        Paperno.Text = "";
        Custsex.Text = "";
        Custphone.Text = "";
        Custpost.Text = "";
        Custaddr.Text = "";
        Remark.Text = "";
    }

}
