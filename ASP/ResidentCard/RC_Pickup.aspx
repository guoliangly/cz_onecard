﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_Pickup.aspx.cs" Inherits="ASP_ResidentCard_Pickup"%>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>中心领卡</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />    
    <link href="../../css/photo.css" rel="stylesheet" type="text/css" />    
</head>
<body>
 	<cr:CardReader id="cardReader" Runat="server"/>    

    <form id="form1" runat="server">
        <div class="tb"> 市民卡->中心领卡  </div>
        
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" RenderMode="inline">
            <ContentTemplate>
            <asp:BulletedList ID="bulMsgShow" runat="server"/>
            <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
            
            
            
        
         
 
 
            
          <div class="basicinfoRC"  >
          <div class="info">卡信息</div>
             <table  border="0" cellpadding="0" cellspacing="0" class="text25">
             <tr><td  width="10">
             </td><td>
                   <div class="photo" >
                   <asp:Panel ID="div0" Visible="false" runat="server">
                   <table width="243" border="0" align="center" cellpadding="0" cellspacing="0" >
                     <tr>
                       <td width="64" height="25">&nbsp;</td>
                       <td width="20">&nbsp;</td>
                       <td width="69">&nbsp;</td>
                       <td width="90" rowspan="4"><asp:Image runat="server" ID="image0" Width="91" Height="112" /></td>
                     </tr>
                     <tr>
                       <td><asp:Label runat="server" ID="name0" /></td>
                       <td><asp:Label runat="server" ID="sex0" /></td>
                       <td align="center"><asp:Label runat="server" ID="size0" /></td>
                      </tr>
                     <tr>
                       <td height="27" colspan="3"><asp:Label runat="server" ID="idcardno0" /></td>
                     </tr>
                     <tr>
                       <td colspan="3"><asp:Label runat="server" ID="addr0" /></td>
                     </tr>
                     </table> 
                     </asp:Panel>
                     </div>
             </td></tr></table>
             
           </div>
           
           
           
           
            <div class="pipinfoRC">
                <div class="base">查询条件</div>
                <div class="kuang5">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                    <tr>
                        <td align="right">身份证号:</td>
                        <td> <asp:TextBox runat="server" ID="txtIDCardNo" MaxLength="18" CssClass="input" style="width:140px" />&nbsp;&nbsp;
                             <asp:Button ID="txtReadPaper" Text="读二代证" CssClass="button1" runat="server" OnClientClick="readIDCard(null, null, null, null, 'txtIDCardNo',null)"/>
                        </td>
                        <td align="right">&nbsp;</td>
                    </tr>
                    <tr>
                         <td align="right">处理状态:</td>
                         <td ><asp:DropDownList runat="server" ID="selDealState" CssClass="input" /></td>
                         <td align="right">&nbsp;</td>
                    </tr>
                    <tr >
                        <td  align="right">单位名称:</td>
                        <td > <asp:TextBox runat="server" ID="txtCorpName" CssClass="input"  MaxLength="50" style="width:140px" />&nbsp;
                              <asp:Button runat="server" Text="-->" ID="btnCorpQry" OnClick="btnCorpQry_Click" /> 
                              <asp:DropDownList ID="selCorp" runat="server" /></td>
                        <td>
                        <td>&nbsp;</td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/></td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                     </tr>
                    </table>

                </div>
         </div>
           

 
          <div class="con">
          <div class="base">查询结果</div>
              <div class="kuang5">
                    <div class="gdtb" style="height:240px">
                             <asp:GridView ID="gvResult" runat="server"
                            Width = "98%"
                            CssClass="tab1"
                            HeaderStyle-CssClass="tabbt"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            AllowPaging="false"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="true"
                            OnRowDataBound="gvResult_RowDataBound"
                            EmptyDataText="没有数据记录!">
                                <Columns>
                                <asp:TemplateField>
                                     <ItemTemplate>
                                     <asp:LinkButton ID="LinkButton1" OnCommand ="LinkButton1_Click" CommandArgument ='<%# Eval("身份证号") + ";" + Container.DataItemIndex %>'  runat="server">选择</asp:LinkButton>
                                     </ItemTemplate>
                                </asp:TemplateField>     
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                      <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                      <asp:CheckBox ID="ItemCheckBox" runat="server"/>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                </Columns>           

                            </asp:GridView>
                    </div>
              </div>
        </div>

<div class="btns">
     <table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:Button runat="server" CssClass="button1"  Text="提交" ID="btnSubmit" OnClick="btnSubmit_Click"/></td>
  </tr>
</table>

</div>
            </ContentTemplate>           
        </asp:UpdatePanel>

    </form>
</body>
</html>
