﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.UserCard;
using Common;
using TM;
using TDO.UserManager;
using System.Collections.Generic;
using Master;

// 市民卡作废处理
public partial class ASP_ResidentCard_RC_CardCancel : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

    }

    /// <summary>
    /// 查询前校验
    /// </summary>
    /// <returns></returns>
    private bool QueryValidate()
    {
        //对起始卡号进行非空、长度、数字检验

        if (this.txtFromCardNoQ.Text.Length > 0 && Validation.strLen(this.txtFromCardNoQ.Text) != 16)
        {
            context.AddError("A004112101:起始卡号不是16位", txtFromCardNoQ);
        }
        if (this.txtToCardNoQ.Text.Length > 0 && Validation.strLen(this.txtToCardNoQ.Text) != 16)
        {
            context.AddError("A004112104:终止卡号不是16位", txtToCardNoQ);
        }

        return !context.hasError();
    }

    /// <summary>
    /// 提交前验证
    /// </summary>
    /// <returns></returns>
    private bool SubmitValidate()
    {
        // 对起始卡号和结束卡号进行校验
        Validation valid = new Validation(context);
        long fromCard = 0, toCard = 0;

        //对起始卡号进行非空、长度、数字检验


        bool b = valid.notEmpty(txtFromCardNo, "A004112100");
        if (b) b = valid.fixedLength(txtFromCardNo, 16, "A004112101");
        if (b) fromCard = valid.beNumber(txtFromCardNo, "A004112102");

        //对终止卡号进行非空、长度、数字检验


        b = valid.notEmpty(txtToCardNo, "A004112103");
        if (b) b = valid.fixedLength(txtToCardNo, 16, "A004112104");
        if (b) toCard = valid.beNumber(txtToCardNo, "A004112105");


        //对作废描述进行校验
        if (string.IsNullOrEmpty(selCancelEx.SelectedValue))
        {
            context.AddError("A009478001",selCancelEx);
        }
        return !context.hasError();

    }

    /// <summary>
    /// 查询按钮事件
    /// </summary>
    protected void btnQuery_Click(object sender, EventArgs e)
    {

        if (!QueryValidate())

            return;


        DataTable data = SPHelper.callResidentCardQuery(context, "CARDCANCELQUERY", txtFromCardNoQ.Text.Trim(), txtToCardNoQ.Text.Trim());

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        gvResult.DataSource = data;
        gvResult.DataBind();
        gvResult.SelectedIndex = -1;
    }

    /// <summary>
    /// 提交按钮事件
    /// </summary>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!SubmitValidate())
            return;

        context.SPOpen();
        context.AddField("p_fromCardNo").Value = txtFromCardNo.Text.Trim();
        context.AddField("p_toCardNo").Value = txtToCardNo.Text.Trim();
        context.AddField("p_cancelEx").Value = selCancelEx.SelectedValue;
        bool ok = context.ExecuteSP("SP_RC_CardCancel");
        if (ok) AddMessage("D002P01004:市民卡作废成功");
        btnQuery_Click(sender, e);
    }

    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件

            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvResult','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = gvResult.SelectedIndex;
        txtFromCardNo.Text = gvResult.Rows[index].Cells[0].Text.Trim();
        txtToCardNo.Text = gvResult.Rows[index].Cells[1].Text.Trim();
    }

    /// <summary>
    /// 分页事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void gvResult_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        //分页事件
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    private void ClearContols()
    {
        txtFromCardNo.Text = "";
        txtToCardNo.Text = "";
        selCancelEx.SelectedIndex = -1;
    }
}
