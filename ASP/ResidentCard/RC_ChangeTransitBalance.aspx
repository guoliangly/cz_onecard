<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_ChangeTransitBalance.aspx.cs" Inherits="ASP_ResidentCard_TransitBalance" %>
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>换卡转值</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<cr:CardReader id="cardReader" Runat="server"/>    

    <form id="form1" runat="server">
<div class="tb">
市民卡->换卡转值

</div>
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
          <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
<asp:HiddenField runat="server" ID="hiddentradeno" />
<asp:HiddenField runat="server" ID="hiddencMoney" />
<asp:HiddenField runat="server" ID="hidSupplyMoney" />

  <asp:HiddenField runat="server" ID="hidWarning" />
  <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click"/>


            <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
    <asp:BulletedList ID="bulMsgShow" runat="server">
    </asp:BulletedList>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>       


<div class="con">
    <div class="card">新卡信息</div>
        
    </tr>
  </table>
  <div class="kuang5">
  
 <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
      <tr>
        <td width="9%"><div align="right">新卡卡号:</div></td>
        <td width="13%"><asp:TextBox ID="txtCardno" CssClass="labeltext" maxlength="16" runat="server"/></td>
        <td  width="9%"><div align="right">启用日期:</div></td>
        <td  width="13%"><asp:TextBox ID="sDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox></td>
        <td  width="9%"><div align="right">卡内余额:</div></td>
        <td  width="13%"><asp:Label runat="server" ID="labBalance" /></td>
        <td width="9%"><div align="right">换卡类型:</div></td>
        <td width="13%"><asp:Label runat="server" ID="labReason" /></td>
        <td width="13%" align="right"><asp:Button ID="btnReadCard" CssClass="button1" OnClick="btnReadCard_Click" OnClientClick="return ReadCardInfo()"  runat="server" Text="读卡" /></td>
        
        
    </tr>
    <tr>
        <td  width="9%"><div align="right">新卡类型:</div></td>
        <td  width="13%"><asp:Label runat="server" ID="labCardType" /></td>
        <td  width="9%">&nbsp;</td>
        <td  width="13%">&nbsp;</td>
        <td  width="9%">&nbsp;</td>
        <td  width="13%">&nbsp;</td>
        <td  width="9%">&nbsp;</td>
        <td  width="13%">&nbsp;</td>
        <td  width="13%">&nbsp;</td>
        
    </tr>
  
</table>

 </div>
 
 <div class="pip">用户信息</div>
        <div class="kuang5">
        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
        <tr>
            <td width="9%"><div align="right">用户姓名:</div></td>
            <td width="13%"><asp:Label ID="labName" CssClass="labeltext" runat="server"/></td>
            <td width="9%"><div align="right">出生日期:</div></td>
            <td width="13%"><asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label></td>
            <td width="9%"><div align="right">证件类型:</div></td>
            <td width="13%"><asp:Label ID="Papertype" runat="server" Text=""></asp:Label></td>
            <td width="9%"><div align="right">证件号码:</div></td>
            <td width="25%" colspan="3"><asp:Label ID="labIDNo" CssClass="labeltext" runat="server"/></td>
        </tr>
        <tr>
            <td><div align="right">用户性别:</div></td>
            <td><asp:Label ID="labSex" CssClass="labeltext" runat="server"/></td>
            <td><div align="right">联系电话:</div></td>
            <td><asp:Label ID="Custphone" runat="server" Text=""></asp:Label></td>
            <td><div align="right">邮政编码:</div></td>
            <td><asp:Label ID="Custpost" runat="server" Text=""></asp:Label></td>
            <td><div align="right">联系地址:</div></td>
            <td colspan="3"><asp:Label ID="Custaddr" runat="server" Text=""></asp:Label></td>
        </tr>
        </table>
  </div>
                    
                    

 
 <div class="card">旧卡信息</div>
    <div class="kuang5">
        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
            <tr><td width="10%"><div align="right">旧卡卡号:</div></td>
                <td width="13%"><asp:Label runat="server" ID="labOldCardNo" /></td>
                <td width="9%"><div align="right">旧卡押金:</div></td>
                <td width="12%"><asp:TextBox ID="ODeposit" CssClass="labeltext" runat="server"></asp:TextBox></td>
                <td width="9%"><div align="right">换卡时间:</div></td>
                 <td width="22%"><asp:Label runat="server" ID="labChangeTime" /></td>
                <td width="11%"></td>
                <td width="14%"></td>
            </tr>
            <tr>
                <td><div align="right">服务开始日:</div></td>
                <td><asp:TextBox ID="OsDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox></td>
                <td><div align="right">旧卡余额:</div></td>
                <td><asp:Label runat="server" ID="labTransitMoney" /></td>
                <!-- 
                <td><div align="right">卡片状态:</div></td>
                <td><asp:TextBox ID="RESSTATE" CssClass="labeltext" runat="server"></asp:TextBox></td>
                -->
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    
    
    
  
 <div class="pip">未转值信息</div>
  <div class="kuang5">
  <div style="height:80px;overflow-y:scroll;" >
  <asp:GridView ID="lvwTransitList" runat="server"
            Width = "95%"
            CssClass="tab1"
            HeaderStyle-CssClass="tabbt"
            AlternatingRowStyle-CssClass="tabjg"
            SelectedRowStyle-CssClass="tabsel"
            AllowPaging="false"
            
            AutoGenerateColumns="false"
            OnRowDataBound = "lvwTransitList_RowDataBound"
            >
            <Columns>
              <asp:BoundField DataField="旧卡卡号"       HeaderText="旧卡卡号"/>
              <asp:BoundField DataField="新卡卡号"       HeaderText="新卡卡号"/>  
              <asp:BoundField DataField="业务类型"       HeaderText="业务类型" />
              
              <asp:BoundField  DataFormatString="{0:yyyy-MM-dd}" DataField="时间" HeaderText="时间" />
              <asp:BoundField DataField="未转值余额"      HeaderText="未转值余额" NullDisplayText="0" DataFormatString="{0:n}" HtmlEncode="false" />
              <asp:BoundField DataField="原因"       HeaderText="原因" />
            </Columns>     
       
           <EmptyDataTemplate>
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
             <tr class="tabbt">
                <td>旧卡卡号</td>
                <td>新卡卡号</td>
                <td>业务类型</td>
                
                <td>换卡时间</td>
                <td>未转值余额</td>
                <td>原因</td>
                
                
            </tr>
           </table>
          </EmptyDataTemplate>
    </asp:GridView>
  </div>
</div>


 <div class="info">转值信息</div>
 <div class="kuang5">
 <div style="height:83px">
 <div class="left">
 <img src="../../Images/show.JPG" width="170" height="83" alt="" /></div>
  <div class="big">
  <table width="200" border="0" cellspacing="0" cellpadding="0">    
     <tr>
       <td width="320" colspan="2" class="red"><div align="center">当前转入新卡的余额为</div></td>
      </tr>
     <tr>
       <td colspan="2"><div align="center"><asp:Label ID="TransitBalance" runat="server" Text="0.00"></asp:Label></div></td>
      </tr>
   </table>
  </div>
  </div>
 </div>
 
 
 


 </div>
 

 <div class="footall"></div>
  <div class="btns">
     <table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr>
    <td><asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false"  
                OnClientClick="printdiv('ptnPingZheng1')" /></td>
    <td><asp:Button CssClass="button1" ID="btnSubmit" Text="转值" runat="server" Enabled="false" OnClick="btnSubmit_Click" /></td>
  </tr>
</table>

<asp:CheckBox ID="chkPingzheng" runat="server" Checked="true" />自动打印凭证
</div>
            </ContentTemplate>          
        </asp:UpdatePanel>
    </form>
</body>
</html>
