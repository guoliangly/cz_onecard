﻿/***************************************************************
 * RC_CardFileInput.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 制卡文件导入 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 * 2010/12/02    粱锦           
 * 2011/05/12    粱锦           修改制卡文件领卡调用的sp
 * 2011/11/22   wdx             增加证件类型
 ***************************************************************
 */

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Common;
using TM;

public partial class ASP_ResidentCard_CardFileInput : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(gvResult, null);
    }


    //创建查询结果
    private void createGridViewData()
    {
        DataTable data = SPHelper.callBatchSlaeQuery(context, "QueryCardFile", Session.SessionID);
        //btnSubmit.Enabled = data.Rows.Count > 0;//

        UserCardHelper.resetData(gvResult, data);

        
    }

    // 获取序列号

    private string GetSeq()
    {
        context.SPOpen();
        context.AddField("step").Value = 1;
        context.AddField("seq", "string", "Output", "16");
        context.ExecuteReader("SP_GetSeq");
        context.DBCommit();

        return "" + context.GetFieldValue("seq");
    }

    // 记录数据导入台帐
    private int recordUploadHistory()
    {
        string seqNo = GetSeq();

        int intLen = FileUpload1.PostedFile.ContentLength;
        byte[] file = new byte[intLen];
        FileUpload1.PostedFile.InputStream.Read(file, 0, intLen);

        context.DBOpen("Insert");
        context.AddField(":TRADEID").Value = seqNo;
        context.AddField(":CARDFILENAME").Value = FileUpload1.FileName;
        context.AddField(":CARDFILE", "Blob").Value = file;

        int rows = -1;
        try
        {
            rows = context.ExecuteNonQuery(
                    @"insert into TF_B_CARDFILETRADE(TRADEID, CARDFILENAME, CARDFILE)
                    values(:TRADEID, :CARDFILENAME, :CARDFILE)");
            context.DBCommit();
        }
        catch (Exception ex)
        {
            context.AddError(ex.Message);
        }
        return rows;
    }

    //检查文件
    protected void btnCheckFile_Click(object sender, EventArgs e)
    {


        UserCardHelper.resetData(gvResult, null);
        //验证文件大小
        GroupCardHelper.uploadFileValidate(context, FileUpload1);
        if (context.hasError()) return;

        // 首先清空临时表

        CommonHelper.clearTempTable(context, Session.SessionID);

        context.DBOpen("Insert");
        Stream stream = FileUpload1.FileContent;
        StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("gb2312"));
        string strLine = "";
        int lineCount = 0; int goodLines = 0;
        String[] fields = null;
        Hashtable ht = new Hashtable();

        while (true)
        {
            strLine = reader.ReadLine();
            if (strLine == null)
            {
                break;
            }
            ++lineCount;

            strLine = strLine.Trim();
            if (strLine.Length <= 0)
            {
                continue;
            }
           
            //制卡文件行最大长度1049
            if (strLine.Length > 1049)
            {
                context.AddError("第" + lineCount + "行长度为" + strLine.Length + ", 根据制卡格式定义不能超过1049");
                continue;
            }

            fields = strLine.Split(new char[2] { ',', '\t' });
            // 字段数目为11时合法

            if (fields.Length != 11)
            {
                context.AddError("第" + lineCount + "行字段数目为" + fields.Length + ", 根据制卡格式定义必须为11");
                continue;
            }
            //文件内容处理，并验证每行各字段的合法性
            dealCardFileContent(ht, fields, lineCount);
            ++goodLines;
        }

        if (goodLines <= 0)
        {
            context.AddError("A004P01F01: 上传文件为空");
        }

        //stream.Position = 0;
        if (context.hasError())
        {
            context.RollBack();
            return;
        }
        btnCheckFile.Enabled = false;
        btnUpload.Enabled = true;

        AddMessage(FileUpload1.FileName + "文件检查成功！请继续上传文件");

        //UserCardHelper.resetData(gvResult, null);
        
    }

   //上传文件
    protected void btnUpload_Click(object sender, EventArgs e)
    {
         //判断文件内容验证是否合法。
        if (!context.hasError())
        {
            context.DBCommit();
            AddMessage(FileUpload1.FileName + "文件上传成功！");
            //recordUploadHistory();
            //storeFileToTmpTable(FileUpload1.FileName, FileUpload1.FileBytes);
            createGridViewData();

            btnCheckFile.Enabled = false;
            btnUpload.Enabled = false;
            
        }
        else
        {
            context.RollBack();
            createGridViewData();
        }

        
    }


   

//    //sporadic 记录导入照片文件
//    private void storeFileToTmpTable(string fileName, byte[] pic)
//    {
//        context.DBOpen("Delete");
//        context.AddField(":SESSID").Value = Session.SessionID;
//        context.ExecuteNonQuery("delete from TF_B_PICTURE where SESSID=:SESSID");
//        context.DBCommit();

//        context.DBOpen("Insert");
//        context.AddField(":SESSID").Value = Session.SessionID;
//        context.AddField(":CREATER").Value = context.s_UserID;
//        context.AddField(":NN").Value = fileName.Substring(0, fileName.Length < 30 ? fileName.Length : 30);
//        context.AddField(":PP", "Blob").Value = pic;
//        int rows = context.ExecuteNonQuery(
//                @"insert into TF_B_PICTURE(SESSID, OLDIDCARDNO,CREATER, CREATETIME, NAME, PICTURE)
//                  values(:SESSID, '0000000000000000', :CREATER, sysdate, :NN, :PP)");
//        context.DBCommit();

//    }


   
    private void dealCardFileContent(Hashtable ht, String[] fields, int lineCount)
    {



        ResidentCardFileRecord rcFileRecord = new ResidentCardFileRecord();
        rcFileRecord.setCmnContext(context);
        rcFileRecord.setLineCount(lineCount);

        rcFileRecord.RealName = fields[10].Trim();              //是否记名
        rcFileRecord.FldID = fields[0].Trim();                 //序号
        rcFileRecord.FldPapersType = fields[2].Trim();         //证件类型
        //判断社保证件类型是否符合要求wdx20111122
        rcFileRecord.FldPaperID = fields[3].Trim();           //证件号码
        rcFileRecord.FldUserName = fields[4].Trim();         //姓名
        rcFileRecord.FldUserSex = fields[5].Trim();          //性别
        rcFileRecord.FldBrith = fields[6].Trim();           //生日
        rcFileRecord.FldAddress = fields[8].Trim();         //地址
        rcFileRecord.FldZip = fields[9].Trim();             //邮政编码
        rcFileRecord.FldPhone = fields[7].Trim();           //联系电话
        rcFileRecord.FldLctNo = fields[1].Trim();           //龙城通卡号
        
        insertToTmp(rcFileRecord);
    }
    /// <summary>
    /// 
    //f0,姓名           old
    //f1,性别           old
    //f2,身份号码       old
    //f3,IC卡号         old
    //f4,社保号         old 
    //f5,银行卡号       old
    //f6,制卡设备号     old  //制卡批次号 new
    //f7,制卡时间           new
    //f8 证件类型           new
    //f9 出生日期           new
    //f10 通讯地址          new
    //f11 邮政编码          new
    //f12 联系电话          new
    //f13 制卡是否成功      new
    /// </summary>
    /// <param name="rcFileRecord"></param>
    private void insertToTmp(ResidentCardFileRecord rcFileRecord)
    {
        if (!context.hasError())
        {
            string insertToTmp = "insert into tmp_residentcard_input(f0, f1, f2, f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,SESSIONID) values('"
                                    + rcFileRecord.FldUserName + "', '"
                                    + rcFileRecord.FldUserSex + "', '"
                                    + rcFileRecord.FldPaperID + "', '"
                                    + rcFileRecord.FldLctNo + "', '"
                                    + rcFileRecord.FldPapersType + "', '"//证件类型wdx20111122
                                    + rcFileRecord.FldBrith + "', '"
                                    + rcFileRecord.FldAddress + "', '"
                                    + rcFileRecord.FldZip + "', '"
                                    + rcFileRecord.FldPhone + "', '"
                                    + rcFileRecord.FldID + "', '"
                                    + rcFileRecord.FldPapersType + "', '"//证件类型wdx20111122
                                    + rcFileRecord.FldPapersType + "', '"//证件名称wdx20111122
                                    + context.s_UserID + "', '"
                                    + "1" + "','"
                                    +rcFileRecord.RealName+"', '"       //记名/不记名
                                    + Session.SessionID + "')";
            context.ExecuteNonQuery(insertToTmp);
        }
    }

    //制卡
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        context.SPOpen();
        context.AddField("p_sessid").Value = Session.SessionID;
        context.AddField("p_operateCard").Value = context.s_CardID;
        bool ok = context.ExecuteSP("SP_PS_BatchSaleCard");

        if (ok)
        {
            AddMessage("售卡成功");
            btnSubmit.Enabled = false;
        }
    }

    protected void gvResult_DataBound(object sender, EventArgs e)
    {
        int count = 0;

        if (gvResult.Rows.Count > 0)
        {
            for (int i = 0; i < gvResult.Rows.Count; i++)
            {
                string txtCell = gvResult.Rows[i].Cells[1].Text;
                if (txtCell == "OK")
                {

                }
                else
                {
                    count++;
                    gvResult.Rows[i].Cells[1].CssClass = "error";
                }
            }

            btnSubmit.Enabled = false;

            if (count == 0)
            {
                btnSubmit.Enabled = true;
            }
        }
    }

    // gridview换页处理
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        createGridViewData();
    }
}
