﻿/***************************************************************
 * RC_ChangeUserInfo.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 资料更新 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/12/27    殷华荣           初次开发
 ***************************************************************
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using Common;

public partial class ASP_ResidentCard_ChangeUserInfo : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ResidentCardHelper.showPicture(context, Request, Response)) return;
        if (Page.IsPostBack) return;

        //从社保证件类型编码表中读取数据，放入下拉列表中

        DataTable dt1 = SPHelper.callResidentCardQuery(context, "QuerySociePaper");
        selPapertype.Items.Add(new ListItem("---请选择---", ""));

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt1.Rows.Count; ++i)
        {
            itemArray = dt1.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
            selPapertype.Items.Add(li);
        }
        selPapertype.SelectedValue = "00";

        //初始化旧证件类型
        selOldPapertype.Items.Add(new ListItem("---请选择---", ""));



        for (int i = 0; i < dt1.Rows.Count; ++i)
        {
            itemArray = dt1.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
            selOldPapertype.Items.Add(li);
        }
        selOldPapertype.SelectedValue = "00";

        //初始化性别
        ASHelper.initSexList(selCustsex);


    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        hidOldCardNo.Value = "";
        //判断证件类型是否选了
        if (selOldPapertype.SelectedValue == "")
        {
            context.AddError("请选择证件类型");
        }

        //判断卡号是否合法
        if (txtIDCardNo.Text.Trim().Length < 1)
        {
            context.AddError("A001001121");
        }
        else if (txtIDCardNo.Text.Trim().Length > 20)
        {
            context.AddError("A001001123");
        }
        else if (!Validation.isCharNum(txtIDCardNo.Text.Trim()))
        {
            context.AddError("A001001122");
        }
        if (context.hasError())
        {
            btnIsManager.Enabled = false;
            return;
        }

        //查询是否有该卡信息
        context.DBOpen("Select");
        context.AddField("idcardno").Value = txtIDCardNo.Text.Trim();
        context.AddField("papertypecode").Value = selOldPapertype.SelectedValue;
        DataTable dt = context.ExecuteReader("select * from TF_RESIDENTCARD where IDCARDNO = :idcardno and PAPERTYPECODE = :papertypecode");
        context.DBCommit();
        if (dt == null || dt.Rows.Count < 1)
        {
            context.AddError("未找到该卡的客户信息!");
            btnIsManager.Enabled = false;
            return;
        }

        //查询是否做过换卡登记
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryResidentCardByCardNo", txtIDCardNo.Text.Trim(), selOldPapertype.SelectedValue, "3");
        if (data == null || data.Rows.Count <= 0)
        {
            context.AddError("该卡未做过换卡登记 不能修改信息!");
            btnIsManager.Enabled = false;
            return;
        }

        DataRow row = data.Rows[0];
        txtCusname.Text = row["姓名"].ToString();
        txtCustbirth.Text = Convert.ToDateTime(row["出生日期"]).ToString("yyyy-MM-dd");
        selPapertype.SelectedValue = row["证件类型编码"].ToString();
        txtCustpaperno.Text = row["证件号码"].ToString();
        selCustsex.SelectedValue = row["性别"].ToString();
        txtCustphone.Text = row["电话"].ToString();
        txtCustpost.Text = row["邮编"].ToString();
        txtCustaddr.Text = row["联系地址"].ToString();
        txtRemark.Text = row["备注"].ToString();
        //hidOldCardNo.Value = txtOldCardNo.Text.Trim();
        hidOldName.Value = txtCusname.Text;
        btnIsManager.Enabled = true;
    }

    //对用户信息进行检验
    private Boolean SaleInfoValidation()
    {

        //对用户姓名进行非空、长度检验

        if (txtCusname.Text.Trim() == "")
            context.AddError("A001001111", txtCusname);
        else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
            context.AddError("A001001113", txtCusname);

        //对用户性别进行非空检验

        if (selCustsex.SelectedValue == "")
            context.AddError("A001001116", selCustsex);

        //对证件类型进行非空检验

        if (selPapertype.SelectedValue == "")
            context.AddError("A001001117", selPapertype);

        //对出生日期进行非空、日期格式检验

        String cDate = txtCustbirth.Text.Trim();
        if (cDate == "")
            context.AddError("A001001114", txtCustbirth);
        else if (!Validation.isDate(txtCustbirth.Text.Trim()))
            context.AddError("A001001115", txtCustbirth);

        //对联系电话进行非空、长度、数字检验

        if (txtCustphone.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
                context.AddError("A001001126", txtCustphone);
            else if (!Validation.isNum(txtCustphone.Text.Trim()))
                context.AddError("A001001125", txtCustphone);
        }

        //对证件号码进行非空、长度、英数字检验

        if (txtCustpaperno.Text.Trim() == "")
            context.AddError("A001001121", txtCustpaperno);
        else if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
            context.AddError("A001001122", txtCustpaperno);
        else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
            context.AddError("A001001123", txtCustpaperno);

        //对邮政编码进行非空、长度、数字检验

        if (txtCustpost.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                context.AddError("A001001120", txtCustpost);
            else if (!Validation.isNum(txtCustpost.Text.Trim()))
                context.AddError("A001001119", txtCustpost);
        }

        //对联系地址进行非空、长度检验

        if (txtCustaddr.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);
        }

        //对备注进行长度检验

        if (txtRemark.Text.Trim() != "")
            if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                context.AddError("A001001129", txtRemark);

        return !(context.hasError());
    }
    protected void btnChangeUserInfo_Click(object sender, EventArgs e)
    {
        //验证是否有提交过信息尚未审批
        context.DBOpen("Select");
        context.AddField("OPAPERTYPECODE").Value = selOldPapertype.SelectedValue;
        context.AddField("OIDCARDNO").Value = txtIDCardNo.Text.Trim();
        string sql = @"select * from TF_RESIDENT_REVIEW where OPAPERTYPECODE = :OPAPERTYPECODE and OIDCARDNO = :OIDCARDNO and RESULT = '0'";
        DataTable dt = context.ExecuteReader(sql);
        if (dt != null && dt.Rows.Count > 0)
        {
            context.AddError("存在已提交审批的信息,尚未审批,请审批后再修改提交.");
            return;
        }

        //对输入项进行检验
        if (!SaleInfoValidation())
            return;
        context.SPOpen();
        context.AddField("P_OPAPERTYPECODE").Value = selOldPapertype.SelectedValue;
        context.AddField("P_OIDCARDNO").Value = txtIDCardNo.Text.Trim();
        context.AddField("P_NPAPERTYPECODE").Value = selPapertype.SelectedValue;
        context.AddField("P_NIDCARDNO").Value = txtCustpaperno.Text.Trim();
        context.AddField("P_ONAME").Value = hidOldName.Value;
        context.AddField("P_NAME").Value = txtCusname.Text.Trim();
        context.AddField("P_SEX").Value = selCustsex.SelectedValue;
        context.AddField("P_BIRTHDATE", "DateTime").Value = Convert.ToDateTime(txtCustbirth.Text.Trim());
        context.AddField("P_TELEPHONE").Value = txtCustphone.Text.Trim();
        context.AddField("P_HOMEADDRESS").Value = txtCustaddr.Text.Trim();
        context.AddField("P_POSTALCODE").Value = txtCustpost.Text.Trim();
        context.AddField("P_REMARK").Value = txtRemark.Text.Trim();

        bool ok = context.ExecuteSP("SP_RC_CHANGERESIDENTREC");
        if (ok)
        {
            context.AddMessage("提交成功 请审核");
            btnIsManager.Enabled = false;
        }

    }

}
