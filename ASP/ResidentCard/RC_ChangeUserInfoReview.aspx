﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_ChangeUserInfoReview.aspx.cs" Inherits="ASP_ResidentCard_RC_ChangeUserInfoReview" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <title>市民卡信息变更审核</title>

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
           function SelectAll(tempControl)
           {
               //将除头模板中的其它所有的CheckBox取反 

                var theBox=tempControl;
                 xState=theBox.checked;    

                elem=theBox.form.elements;
                for(i=0;i<elem.length;i++)
                if(elem[i].type=="checkbox" && elem[i].id!=theBox.id)
                 {
                      if(elem[i].checked!=xState)
                            elem[i].click();
                }
            }  
    </script>
</head>
<body>
    <form id="form1" runat="server">
<div class="tb">
市民卡->信息变更审核

</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
<div class="con">
                    <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%"><div align="right">审批状态:</div></td>
                                <td width="13%"><asp:DropDownList ID="selApprovalStatus" CssClass="input" runat="server"></asp:DropDownList></td>
                                <td width="9%">&nbsp;</td>
			                    <td width="40%">&nbsp;</td>
                                <td style="width: 13%"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" /></td>
                            </tr>
                            <tr>
                                <!-- 
                                <td style="width: 5%" align="right">单位名称:</td>
                                <td colspan="3">
                                    <asp:TextBox runat="server" ID="txtCorpName" CssClass="input" MaxLength="50" Style="width: 140px" />&nbsp;
                                    <asp:Button runat="server" Text="-- >" ID="btnCorpQry"  />
                                    <asp:DropDownList ID="selCorp" runat="server" /></td>
                                    -->
                                
                            </tr>
                        </table>
                    </div>
                </div>
<div class="con">
  <div class="jieguo">资料变更信息</div>
  <div class="kuang5">
  <div class="gdtb" style="height:310px">
   <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1" >
         <asp:GridView ID="gvResult" runat="server"
        Width = "98%"
        CssClass="tab1"
         AllowPaging=true
         PageSize=10
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        OnRowCreated="gvResult_RowCreated" 
        OnSelectedIndexChanged="gvResult_SelectedIndexChanged"
        OnPageIndexChanging="gvResult_Page">
           <Columns>
             <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAllCust" runat="server" onclick="javascript:SelectAll(this);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkCust" runat="server"  />
                    </ItemTemplate>
                 </asp:TemplateField>
                <asp:BoundField HeaderText="新姓名" DataField="NAME"/>
                <asp:BoundField HeaderText="旧姓名" DataField="ONAME"/>
                <asp:BoundField HeaderText="性别" DataField="SEX"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="出生日期" DataField="birthdate"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="旧证件类型" DataField="opapername"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="旧证件号码" DataField="oidcardno"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="新证件类型" DataField="rpapername"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="新证件号码" DataField="nidcardno"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="联系电话" DataField="telephone"/>
                <asp:BoundField HeaderText="联系地址" DataField="homeaddress"/>
                <asp:BoundField HeaderText="邮编" DataField="postalcode"/>
            </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />          
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>姓名</td>
                    <td>性别</td>
                    <td>出生日期</td>
                    <td>旧证件类型</td>
                    <td>旧证件号码</td>
                    <td>新证件类型</td>
                    <td>新证件号码</td>
                    <td>联系电话</td>
                    <td>联系地址</td>
                    <td>邮编</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
        </table>
  </div>
  </div>


<div class="kuang5">
<table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:CheckBox ID="chkApprove" AutoPostBack="true" Text="通过" runat="server" OnCheckedChanged="chkApprove_CheckedChanged" /></td>
    <td align="right"><asp:CheckBox ID="chkReject" AutoPostBack="true" Text="作废" runat="server" OnCheckedChanged="chkReject_CheckedChanged"  /></td>
    <td align="right"><asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click" OnClientClick="{if(confirm('确定要提交吗?')){return true;}return false;}"/></td>
  </tr>
</table>

</div>

</div>
            </ContentTemplate>         
        </asp:UpdatePanel>

    </form>
</body>
</html>

