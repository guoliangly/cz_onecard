﻿/***************************************************************
 * RC_ReIssueRegister.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 补卡登记 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 ***************************************************************
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;

public partial class ASP_ResidentCard_ReIssueRegister : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ResidentCardHelper.showPicture(context, Request, Response)) return;

        if (Page.IsPostBack) return;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据，放入下拉列表中
        //ASHelper.initPaperTypeList(context, selPapertype);
        //从社保证件类型编码表中读取数据，放入下拉列表中

        DataTable dt1 = SPHelper.callResidentCardQuery(context, "QuerySociePaper");
        selPapertype.Items.Add(new ListItem("---请选择---", ""));

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt1.Rows.Count; ++i)
        {
            itemArray = dt1.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
            selPapertype.Items.Add(li);
        }
        selPapertype.SelectedValue = "00";
        UserCardHelper.resetData(gvResult, null);
    }


    protected void LinkButton1_Click(object sender, CommandEventArgs e)
    {
        char[] splitter = { ':' };
        string arg = e.CommandArgument.ToString();
        string[] args = arg.Split(splitter);
        showDetailInfo(args[0], int.Parse(args[1]),args[2]);

    }

    private void showDetailInfo(string idcardno, int gvResultINdex,string papertypecode)
    {
        gvResult.SelectedIndex = gvResultINdex;

        DataTable dt = ResidentCardHelper.showNameCardInfo(context, image0, "CC_ReIssueRegister.aspx",
            idcardno, papertypecode, div0, name0, sex0, idcardno0, addr0, size0);

        string dealCode = ResidentCardHelper.getFeeInfo(dt, context, labFee); // "6"); // "6" 已挂失

        if (dealCode == "6")
        {
            object[] row = dt.Rows[0].ItemArray;
            dt = SPHelper.callResidentCardQuery(context, "QryApprovedMoney", "" + row[6]);
            labFee.Text = "30";
            if (dt != null && dt.Rows.Count > 0)
            {
                labFee.Text = "" + dt.Rows[0].ItemArray[0];
            }
        }
        else
        {
            labFee.Text = "0";
        }



        btnSubmit.Enabled = dealCode == "6";
        //div0.Visible = false;
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow ||
            e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        ResidentCardHelper.checkIDCardNo(context, txtIDCardNo, false);

        if (context.hasError()) return;

        btnSubmit.Enabled = false;
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryResidentCard_ChangeRegister", txtIDCardNo.Text,selPapertype.SelectedValue);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
        }

        resetPage(data);
    }

    private void resetPage(DataTable data)
    {
        btnSubmit.Enabled = false;
        labFee.Text = "0";
        div0.Visible = false;
        UserCardHelper.resetData(gvResult, data);
        if (data != null && data.Rows.Count == 1)
        {
            object[] row = data.Rows[0].ItemArray;
            //showDetailInfo("" + row[2], 0,"");
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        context.SPOpen();
        context.AddField("p_idCardNo").Value = idcardno0.Text;
        context.AddField("p_recvFee").Value = (int)(Double.Parse(labFee.Text) * 100);
        context.AddField("p_seq", "string", "Output", "16");

        bool ok = context.ExecuteSP("SP_RC_ReissueRegister");

        if (ok)
        {
            resetPage(null);
            if (!context.hasError()) context.AddMessage("补卡登记成功");
        }

        btnQuery_Click(sender, e);

    }

   


}
