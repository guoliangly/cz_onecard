﻿/***************************************************************
 * RC_ChangeRegister.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 换卡登记 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 * 2010/12/02    粱锦           
 * 2011/03/29    粱锦           修改bug， bug编号CZSMK-CS-20110323
 * 2011/04/01    粱锦           修改bug， bug编号CZSMK-CS-20110323
 ***************************************************************
 */

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using Master;

public partial class ASP_ResidentCard_ChangeRegister : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ResidentCardHelper.showPicture(context, Request, Response)) return;

        if (Page.IsPostBack) return;

        if (!context.s_Debugging) setReadOnly(txtCardno);
        btnQuery.Enabled = false;
        //if (!context.s_Debugging) btnQuery.Attributes["readonly"] = "true";
        //从社保证件类型编码表中读取数据，放入下拉列表中

        DataTable dt1 = SPHelper.callResidentCardQuery(context, "QuerySociePaper");
        selPapertype.Items.Add(new ListItem("---请选择---", ""));

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt1.Rows.Count; ++i)
        {
            itemArray = dt1.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
            selPapertype.Items.Add(li);
        }

        UserCardHelper.resetData(gvResult, null);
        ASHelper.setChangeReason(selReasonType, false);
    }


    public void selReasonType_Changed(object sender, EventArgs e)
    {
        resetPage(null);
        txtIDCardNo.Text = "";
        //应从参数表中获得换卡收费，暂时写死在页面
        string changeFee = "" + "30.00";

        // 可读卡 
        //12 可读人为损 13 可读自然损 
        if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "13")
        {
            btnReadCard.Enabled = true;
        }
        // 不可读卡 
        //14 不可读人为损 15 不可读自然损
        else if (selReasonType.SelectedValue == "14" || selReasonType.SelectedValue == "15")
        {
            selPapertype.SelectedValue = "00";//默认身份证号wdx20111123
            btnReadCard.Enabled = false;
        }
        //查询和身份证号
        btnQuery.Enabled = !btnReadCard.Enabled;
        txtIDCardNo.Enabled = btnQuery.Enabled;


        //人为损 
        //12 可读人为损 14 不可读人为损  
        if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
        {
            
            DepositFee.Text = changeFee;
        }
        //自然损  
        //13 可读自然损  15 不可读自然损
        else if (selReasonType.SelectedValue == "13" || selReasonType.SelectedValue == "15")
        {
            DepositFee.Text = "0.00";
        }
        //费用        
        labFee.Text = DepositFee.Text;
    }

    protected void LinkButton1_Click(object sender, CommandEventArgs e)
    {
        char[] splitter = { ':' };
        string arg = e.CommandArgument.ToString();
        string[] args = arg.Split(splitter);
        showDetailInfo(args[0], int.Parse(args[1]),args[2]);

    }





    public  DataTable showNameCardInfo(CmnContext context, Image image0, string aspx,
     string idcardno, string papertypecode, Panel div0, Label name0, Label sex0, Label idcardno0, Label addr0, Label size0)
    {
        //image0.ImageUrl = aspx + "?Random=" + (new Random(imageRadom)).Next()
        //   + "&IDCardNo=" + idcardno;

        image0.ImageUrl = "photoBlank.jpg";

        DataTable dt = SPHelper.callResidentCardQuery(context, "QryNameCardInfo", idcardno, papertypecode);
        if (dt != null && dt.Rows.Count > 0)
        {
            div0.Visible = true;
            object[] row = dt.Rows[0].ItemArray;
            name0.Text = "" + row[0];
            sex0.Text = "" + row[1];
            idcardno0.Text = "" + row[2];
            addr0.Text = "" + row[3];
            size0.Text = "" + row[4];
        }
        else
        {
            div0.Visible = false;
        }

        return dt;
    }









    private void showDetailInfo(string idcardno, int gvResultINdex,string papertypecode)
    {
        hidPaperType.Value = papertypecode;
        hidIDCardNo.Value = idcardno;
        gvResult.SelectedIndex = gvResultINdex;
        hiddenName.Value = gvResult.SelectedRow.Cells[3].Text;//姓名
        txtCardnodisplay.Value = gvResult.SelectedRow.Cells[7].Text;//卡号
        DataTable dt = showNameCardInfo(context, image0, "CC_ChangeRegister.aspx",
            idcardno, papertypecode, div0, name0, sex0, idcardno0, addr0, size0);

        hiddendealcode.Value = ResidentCardHelper.getFeeInfo(dt, context, labFee);

        div0.Visible = false;
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow ||
            e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
        }
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        resetPage(null);

        DataTable data = SPHelper.callResidentCardQuery(context, "QueryResidentCard_ChangeRegister", null, txtCardno.Text);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
        }
        resetPage(data);
        // //如果为已领卡"2" 已领卡，则提交按钮可用，否则提交按钮不可用

        // 2011/03/29    粱锦           修改bug，编号CZSMK-CS-20110323
        if (hiddendealcode.Value == "2")
        {
            btnSubmit.Enabled = true;
        }
        else
        {
            //添加错误提示信息
            context.AddError("换卡必须是‘已领卡’状态的卡");
            btnSubmit.Enabled = false;
        }
        btnQuery.Enabled = !btnReadCard.Enabled;
        txtIDCardNo.Enabled = btnQuery.Enabled;
    }


    protected void btnQuery_Click(object sender, EventArgs e)
    {

        hiddencMoney.Value = "0";
        txtCardno.Text = "";
        resetPage(null);
        if (selPapertype.SelectedValue == "")
        {
            context.AddError("证件类型不能为空");
        }
        if (selPapertype.SelectedValue == "00")//身份证
        {
            ResidentCardHelper.checkIDCardNo(context, txtIDCardNo, false);
        }
        if (context.hasError()) return;

        btnSubmit.Enabled = false;
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryResidentCard_ChangeRegister", txtIDCardNo.Text,null,selPapertype.SelectedValue);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
            return;
        }
        resetPage(data);
        // //如果为已领卡"2" 已领卡，则提交按钮可用，否则提交按钮不可用

        // 2011/03/29    粱锦           修改bug，编号CZSMK-CS-20110323
        if (hiddendealcode.Value == "2")
        {
            btnSubmit.Enabled = true;
        }
        else
        {
            //添加错误提示信息
            context.AddError("换卡必须是‘已领卡’状态的卡");
            btnSubmit.Enabled = false;
        }
       
    }

    private void resetPage(DataTable data)
    {
        btnSubmit.Enabled = false;
        labFee.Text = DepositFee.Text;
        div0.Visible = false;
        UserCardHelper.resetData(gvResult, data);
        if (data != null && data.Rows.Count == 1)
        {
            object[] row = data.Rows[0].ItemArray;
            //showDetailInfo("" + row[2], 0);
            showDetailInfo(row[5].ToString(), 0, row[4].ToString().Substring(0,2));
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (selChangeDes.SelectedValue == "")
        {
            context.AddError("A009478002:请选择换卡描述类型", selChangeDes);
            return;
        }

        //换卡登记并收费
        context.SPOpen();
        context.AddField("p_idCardNo").Value = idcardno0.Text;
        context.AddField("p_paperTypeCode").Value = hidPaperType.Value;
        context.AddField("p_reasonCode").Value = selReasonType.SelectedValue;
        context.AddField("p_cardMoney").Value = (int)(Double.Parse(hiddencMoney.Value));//这个余额是直接从卡里面读出来的
        // add by jiangbb  for change explain
        context.AddField("p_changeEx").Value = selChangeDes.SelectedValue;
        
        context.AddField("p_recvFee").Value = (int)(Double.Parse(labFee.Text) * 100);
        context.AddField("p_seq", "string", "Output", "16");

        bool ok = context.ExecuteSP("SP_RC_ChangeRegister");

        if (ok)
        {
            resetPage(null);
            
            if (!context.hasError()) context.AddMessage("换卡登记成功");

            //打印回单
            ASHelper.preparePingZheng(ptnPingZheng, txtCardnodisplay.Value, hiddenName.Value, "换卡登记",
              labFee.Text, "", "", idcardno0.Text, "", "", "0.00", context.s_UserName, context.s_DepartName,
              "", "0.00", "");
            btnPrintPZ.Enabled = true;

            //可读自然损，可读人为损 必须 锁旧卡

            if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "13")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "lockCard();", true);
              
            }
            else
            {
                if (chkPingzheng.Checked && btnPrintPZ.Enabled)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "printPaPiaoScript1", "printInvoice();", true);
                }
            }
           
        }

       
        //可读自然损，可读人为损 读卡查询，否则查库
        resetPage(null);
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryResidentCard_ChangeRegister",
          hidIDCardNo.Value, null, hidPaperType.Value);
        resetPage(data);
        if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "13")
        {
            btnQuery.Enabled = !btnReadCard.Enabled;
            txtIDCardNo.Enabled = btnQuery.Enabled;
        }
        else
        {
            hiddencMoney.Value = "0";
            txtCardno.Text = "";
            btnSubmit.Enabled = false;
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "writeSuccess")
        {
            context.AddMessage("前台锁卡成功");
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台锁卡失败");
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "printPaPiaoScript1", "printInvoice();", true);
        }

    }

}