﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_Query.aspx.cs" Inherits="ASP_ResidentCard_Query" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <link href="../../css/photo.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            市民卡->查询</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ScriptManager1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" RenderMode="inline">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%"><div align="right">证件类型:</div></td>
                                <td width="13%"><asp:DropDownList ID="selPapertype" CssClass="input" runat="server"></asp:DropDownList></td>
                                <td style="width: 10%" align="right">证件号码:</td>
                                <td style="width: 13%"><asp:TextBox runat="server" ID="txtIDCardNo" MaxLength="18" CssClass="input" Style="width: 140px" /></td>
                                <td style="width: 13%"><asp:Button ID="txtReadPaper" Text="读二代证" CssClass="button1" runat="server"  OnClientClick="readSingleID('txtIDCardNo')"  /> </td>
                                <td style="width: 10%" align="right">处理状态:</td>
                                <td style="width: 13%"><asp:DropDownList runat="server" ID="selDealState" CssClass="input" /></td>
                                <td style="width: 13%"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" /></td>
                            </tr>
                            <tr>
                                <!-- 
                                <td style="width: 5%" align="right">单位名称:</td>
                                <td colspan="3">
                                    <asp:TextBox runat="server" ID="txtCorpName" CssClass="input" MaxLength="50" Style="width: 140px" />&nbsp;
                                    <asp:Button runat="server" Text="-- >" ID="btnCorpQry" OnClick="btnCorpQry_Click" />
                                    <asp:DropDownList ID="selCorp" runat="server" /></td>
                                    -->
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="con">
                    <div class="jieguo">
                        查询结果</div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 180px">
                            <asp:GridView ID="gvResult" runat="server" Width="98%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                                AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" AllowPaging="false"
                                PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="true" OnRowDataBound="gvResult_RowDataBound" EmptyDataText="没有数据记录!">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" OnCommand="LinkButton1_Click" CommandArgument='<%# Eval("证件号码") +":" + Container.DataItemIndex + ":"+Eval("证件类型")%>'
                                                runat="server">选择</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="con">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 20%;" align="left">
                                <div class="card">详细信息</div>
                            </td>
                            <td style="width: 40%;" align="right">&nbsp;</td>
                            <td style="width: 10%;" align="right">查询类型:</td>
                            <td style="width: 10%;" align="left">
                                <asp:DropDownList runat="server" ID="selQryType">
                                    <asp:ListItem Text="客户业务" Value="QueryCustBiz" />
                                </asp:DropDownList>
                                 <!--<asp:ListItem Text="银行代理业务" Value="QueryBanktBiz" />-->
                            </td>
                            <td style="width: 20%;" align="left">
                                <asp:Button runat="server" ID="btnQryDetail" Text="查询" Enabled="false" CssClass="button1"  OnClick="btnQryDetail_Click" />
                            </td>
                        </tr>
                    </table>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                            <td> <div class="gdtb" style="height: 150px; width: 100%; top: 1px;">
                                        <asp:GridView ID="gvDetail" runat="server" Width="98%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                                            AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" AllowPaging="false"
                                            PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                            AutoGenerateColumns="true" EmptyDataText="没有数据记录!">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        #</HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    </td>
                                
                                <asp:HiddenField runat="server" ID="hidPaperType" />
                                 <asp:HiddenField runat="server" ID="hidIDCardNo" />
                                <!--
                                <td width="256">
                                    <div class="photo">
                                        <asp:Panel ID="div0" Visible="false" runat="server">
                                            <table width="243" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="64" height="25">
                                                        &nbsp;</td>
                                                    <td width="20">
                                                        &nbsp;</td>
                                                    <td width="69">
                                                        &nbsp;</td>
                                                    <td width="90" rowspan="4">
                                                        <asp:Image runat="server" ID="image0" Width="91" Height="112" /></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="name0" /></td>
                                                    <td>
                                                        <asp:Label runat="server" ID="sex0" /></td>
                                                    <td align="center">
                                                        <asp:Label runat="server" ID="size0" /></td>
                                                </tr>
                                                <tr>
                                                    <td height="27" colspan="3">
                                                        <asp:Label runat="server" ID="idcardno0" /></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label runat="server" ID="addr0" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                    </td>
                                    -->
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
