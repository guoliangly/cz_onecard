﻿/***************************************************************
 * RC_Query.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 查询 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 ***************************************************************
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ASP_ResidentCard_Query : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ResidentCardHelper.showPicture(context, Request, Response)) return;
        if (Page.IsPostBack) return;

        //从社保证件类型编码表中读取数据，放入下拉列表中
        DataTable dt1 = SPHelper.callResidentCardQuery(context, "QuerySociePaper");
        selPapertype.Items.Add(new ListItem("---请选择---", ""));

        Object[] itemArray;
        ListItem li;
        for (int i = 0; i < dt1.Rows.Count; ++i)
        {
            itemArray = dt1.Rows[i].ItemArray;
            li = new ListItem("" + itemArray[2] + ":" + itemArray[1], (String)itemArray[2]);
            selPapertype.Items.Add(li);
        }
        selPapertype.SelectedValue = "00";
        UserCardHelper.resetData(gvResult, null);
        UserCardHelper.resetData(gvDetail, null);
        selCorp.Items.Add(new ListItem("---请选择---", ""));

        DataTable dt = SPHelper.callResidentCardQuery(context, "ReadDealState");
        GroupCardHelper.fill(selDealState, dt, true);

        CommonHelper.clearTempTable(context);
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (selPapertype.SelectedValue == "")
        {
            context.AddError("证件类型不能为空");
        }
        if (selPapertype.SelectedValue == "00")//身份证
        {
            ResidentCardHelper.checkIDCardNo(context, txtIDCardNo, true);
        }
        

        if (context.hasError()) return;

        btnQryDetail.Enabled = false;
        div0.Visible = false;
        UserCardHelper.resetData(gvDetail, null);

        DataTable data;
        data = SPHelper.callResidentCardQuery(context, "QueryResidentCard",
         txtIDCardNo.Text, selPapertype.SelectedValue, selDealState.SelectedValue);

        UserCardHelper.resetData(gvResult, data);
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N007P00001: 查询结果为空");
        }
        else
        {
            object[] row = data.Rows[0].ItemArray;
            showDetailInfo("" + row[5], 0, selPapertype.SelectedValue);
            btnQryDetail.Enabled = true;
        }
    }


    protected void btnQryDetail_Click(object sender, EventArgs e)
    {
        DataTable dt = SPHelper.callResidentCardQuery(context, selQryType.SelectedValue, hidIDCardNo.Value,hidPaperType.Value);
        UserCardHelper.resetData(gvDetail, dt);
    }

    protected void LinkButton1_Click(object sender, CommandEventArgs e)
    {
        char[] splitter = { ':' };
        string arg = e.CommandArgument.ToString();
        string[] args = arg.Split(splitter);

        string idcardno = args[0];
        string papertypecode = args[2];
        
        showDetailInfo(idcardno, int.Parse(args[1]),papertypecode);
    }

    protected void showDetailInfo(string idcardno, int selectedInd,string papertypecode)
    {
        hidIDCardNo.Value = idcardno;
        hidPaperType.Value = papertypecode;
        DataTable dt = ResidentCardHelper.showNameCardInfo(context, image0, "CC_Query.aspx",
            idcardno,papertypecode, div0, name0, sex0, idcardno0, addr0, size0);
        gvResult.SelectedIndex = selectedInd;

        //div0.Visible = false;
        btnQryDetail.Enabled = div0.Visible;
    }

    protected void btnCorpQry_Click(object sender, EventArgs e)
    {
        DataTable dt = SPHelper.callResidentCardQuery(context, "ReadCorpInfo", txtCorpName.Text.Trim());
        GroupCardHelper.fill(selCorp, dt, dt.Rows.Count != 1);
    }


    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow ||
            e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
        }

    }

}
