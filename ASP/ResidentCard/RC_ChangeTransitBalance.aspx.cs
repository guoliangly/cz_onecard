﻿/***************************************************************
 * RC_ChangeTransitBalance.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 市民卡子系统 - 换卡转值 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 * 2010/12/02    粱锦           
 * 2011/11/24   wdx         市民卡加证件类型
 * 2011/12/15 yhr 修改了临时表
 ***************************************************************
 */
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TM;
using TDO.ResourceManager;
using TDO.UserManager;
using TDO.BusinessCode;
using TDO.CardManager;
using PDO.PersonalBusiness;
using Common;
using TDO.BalanceChannel;


public partial class ASP_ResidentCard_TransitBalance : Master.Master
{

    private static int par_ChangeSpan = 7;       //换卡转值时长限制


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";

        TMTableModule tmTMTableModule = new TMTableModule();


        //从系统参数表中读取换卡转值时长限制
        TD_M_TAGTDO ddoTD_M_TAGTDOIn = new TD_M_TAGTDO();
        ddoTD_M_TAGTDOIn.TAGCODE = "RC_CHANGELOSS_SPAN";//取得是市民卡换卡补卡的转值时长限制

        TD_M_TAGTDO[] ddoTD_M_TAGTDOOutArr = (TD_M_TAGTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TAGTDOIn, "S001002125");

        if (ddoTD_M_TAGTDOOutArr[0].TAGVALUE != null)
        {
            par_ChangeSpan = Convert.ToInt16(ddoTD_M_TAGTDOOutArr[0].TAGVALUE);
        }
    }

    // 确认对话框确认处理
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "writeSuccess")   // 写卡成功
        {
            AddMessage("D00503FFF2: 换卡转值前台写卡成功");
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("换卡转值前台写卡失败");
        }

        hidWarning.Value = ""; // 清除警告信息

        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }

    //读卡
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        try
        {
            labBalance.Text = "" + int.Parse(hiddencMoney.Value) / 100.0;//新卡卡内余额

            //查询换卡登记信息
            bool ok = getChangeRegisterInfo();
            if (ok)
            {
                //查询用户信息
                getResidentInfo();
                

                //查询未转值信息
                createGridViewData();

                //计算应转值金额
                calMoeny();
            }
            else
            {
                //clearCustInfo();
            }
            //验证第1张旧卡的不可读时间是否超过7天
            if (labReason.Text.Trim() == "14:不可读人为损坏卡" || labReason.Text.Trim() == "15:不可读自然损坏卡")
            {
                if (!validateChangeDate(labChangeTime.Text.Trim()))
                {
                    btnSubmit.Enabled = false;
                    return;
                }
            }
            //验证后面的第2张开始后面的卡的不可读时间是否超过7天
            foreach (GridViewRow var in lvwTransitList.Rows)
            {
                if (var.Cells[5].Text.Trim() == "不可读人为损" || var.Cells[5].Text.Trim() == "不可读自然损")
                {
                    if (!validateChangeDate(var.Cells[3].Text))
                    {
                        btnSubmit.Enabled = false;
                        return;
                    }
                }
            }
            if (context.hasError())
                return;
            btnSubmit.Enabled = true;
        }
        catch (Exception ex)
        {
            Common.Log.Info(ex.Message, "AppLog");
            context.AddError(ex.Message);
        }
        
    }

    //查询换卡登记信息
    private bool getChangeRegisterInfo()
    {
        callSP_RC_ChangeTransitBalance("0");
        bool ok = context.ExecuteSP("SP_RC_ChangeTransitBalance");
        //btnSubmit.Enabled = ok;
        if (ok)
        {

            //新卡信息
            labCardType.Text = "" + context.GetFieldValue("p_cardType");
            sDate.Text = "" + context.GetFieldValue("p_sDate");
            labReason.Text = "" + context.GetFieldValue("p_REASON");

            //旧卡信息
            labOldCardNo.Text = "" + context.GetFieldValue("p_OLDTJTCARDNO");
            hidSupplyMoney.Value = "" + context.GetFieldValue("p_OLDCARDMONEY");
            labChangeTime.Text = "" + context.GetFieldValue("p_OPERATETIME");
            //RESSTATE.Text = "" + context.GetFieldValue("p_OLDTJTCARDSTATE");
        }
        return ok;
    }

    //查询用户信息
    private void getResidentInfo()
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从IC卡资料表中读取数据

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

        //从IC卡电子钱包账户表中读取数据

        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;

        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

        ////从用户卡库存表(TL_R_ICUSER)中读取数据
        //TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        //ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        //TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        //if (ddoTL_R_ICUSEROut == null)
        //{
        //    context.AddError("A001001101");
        //    return;
        //}

        ////从资源状态编码表中读取数据
        //TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        //ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        //TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        //if (ddoTD_M_RESOURCESTATEOut == null)
        //    RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        //else
        //    RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        #region 获取用户信息

            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);
            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }


            labName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            //性别显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                labSex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                labSex.Text = "女";
            else labSex.Text = "";

            //出生日期显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);


            //证件类型显示
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            labIDNo.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;

        #endregion


        ODeposit.Text = (Convert.ToDecimal(ddoTF_F_CARDRECOut.DEPOSIT) / 100).ToString("0.00");
        OsDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");

    }

    //计算应转值金额
    private void calMoeny(){

        //计算换卡旧卡余额
        try
        {
            labTransitMoney.Text = "" + int.Parse(hidSupplyMoney.Value) / 100.0;
        }
        catch (Exception)
        {
            labTransitMoney.Text = "0";
        }
        TransitBalance.Text = labTransitMoney.Text;
        //累加未转值金额
        decimal ye = 0;
        foreach (GridViewRow var in lvwTransitList.Rows)
        {
            try
            {
                ye = ye + decimal.Parse(var.Cells[4].Text) * 100;
            }
            catch (Exception)
            {
                ye = 0;
            }
        }

        try
        {
            ye = (ye + decimal.Parse(TransitBalance.Text) * 100) / 100;
            TransitBalance.Text = ye.ToString("0.00");
        }
        catch (Exception)
        {
            TransitBalance.Text = labTransitMoney.Text;
        }
        try {
            if (decimal.Parse(TransitBalance.Text).ToString("0.00") != "0.00")
            {
                btnSubmit.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            context.AddError("转值金额为0，无需转值");
        }
        
       
    }

    ///递归查询当前卡的未转值（补卡，换卡）记录 返回结果
    private void createGridViewData()
    {
        DataTable data = SPHelper.callResidentCardQuery(context, "QueryRecursionChangeLoss", labOldCardNo.Text.Trim(), Session.SessionID);

        UserCardHelper.resetData(lvwTransitList, data);
    }

   

    //lvwTransitList控件的 OnRowDataBound事件
    protected void lvwTransitList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            //隐藏状态标志
            //e.Row.Cells[5].Visible = false;
            //e.Row.Cells[6].Visible = false;
        }

    }



    //执行存储过程 SP_RC_ChangeTransitBalance
    private void callSP_RC_ChangeTransitBalance(string option)
    {
        context.SPOpen();
        context.AddField("p_newCardNo").Value = txtCardno.Text;
        context.AddField("p_cardTradeNo").Value = hiddentradeno.Value;
        context.AddField("p_newCardMoney").Value = hiddencMoney.Value;
        context.AddField("p_operateCard").Value = context.s_CardID;
        context.AddField("p_option").Value = option;
        context.AddField("p_sessionID").Value = Session.SessionID;
        
        //context.AddField("p_idNo", "string", "Output", "18");
        //context.AddField("p_name", "string", "Output", "20");
        //context.AddField("p_sex", "string", "Output", "2");
        context.AddField("p_OLDTJTCARDNO", "string", "Output", "16");
        context.AddField("p_REASON", "string", "Output", "16");
        context.AddField("p_OLDCARDMONEY", "Int32", "Output");
        context.AddField("p_OPERATETIME", "string", "Output", "16");
        context.AddField("p_cardType", "string", "Output", "30");
        context.AddField("p_sDate", "string", "Output", "16");
        context.AddField("p_CurrentMoney").Value = Convert.ToInt32(Convert.ToDecimal(TransitBalance.Text) * 100).ToString();
        //context.AddField("p_OLDTJTCARDSTATE", "string", "Output", "16");
        
        
    }


    private bool validateChangeDate(string changetime)
    {
        //可读无时间限制
        //string reason = labReason.Text.Trim().Substring(0, 2);
        //if (reason == "12" || reason == "13") return true;

         //换卡时间和当前时间差小于7天
        //Common.Log.Info("换卡时间和当前时间差小于7天");
        try
        {
            string s = changetime;
            DateTime d;
            if (s.Contains("-"))
            {
                d = DateTime.ParseExact(s, "yyyy-MM-dd", null);
            }
            else
            {
                d = DateTime.ParseExact(s, "yyyyMMdd", null);
            }
            //DateTime d = Convert.ToDateTime(s);
            if (d.AddDays(par_ChangeSpan) >= DateTime.Now)
            {
                context.AddError("A001005122");
                return false;
            }

        }
        catch (Exception exception)
        {
            context.AddError("转换换卡时间错误");
            return false;
        }
        return true;
    }

    //转值操作
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //七天限制
        //if (!validateChangeDate()) return;

        //插入临时表
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from tmp_common_NEW where SESSIONID = '" + Session.SessionID + "'");
        context.DBCommit();


        foreach (GridViewRow var in lvwTransitList.Rows)
        {
            context.DBOpen("Insert");
            context.ExecuteNonQuery("insert into tmp_common_new(f0,f1,f2,f3,f4,f5,SESSIONID) values('" +
                   var.Cells[0].Text + "','" + //旧卡卡号
                   var.Cells[1].Text + "','" + //新卡卡号
                   var.Cells[2].Text + "','" + //业务类型
                   var.Cells[3].Text + "','" + //时间
                   var.Cells[4].Text + "','" + //未转值余额

                   var.Cells[5].Text + "','" + //原因
                   Session.SessionID + "')");  //会话ID
            context.DBCommit();
            
        }
        callSP_RC_ChangeTransitBalance("3");
        bool ok = context.ExecuteSP("SP_RC_ChangeTransitBalance");

        if (ok)
        {
            context.AddMessage("换卡转值成功");
            btnSubmit.Enabled = false;
            hidSupplyMoney.Value = "" + Convert.ToInt32(Convert.ToDecimal(TransitBalance.Text) * 100);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                "chargeCard();", true);

            //打印回单
            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, labName.Text, "换卡转值",
             Convert.ToDecimal(TransitBalance.Text).ToString("0.00"), "", "", "", (Convert.ToDecimal(TransitBalance.Text) + Convert.ToDecimal(hiddencMoney.Value) / (Convert.ToDecimal(100))).ToString("0.00"), "", "0.00", context.s_UserName, context.s_DepartName,
              "", "0.00", "");
            btnPrintPZ.Enabled = true;
        }

    }
   
}
