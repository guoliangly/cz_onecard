<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RC_CardFileInput.aspx.cs" Inherits="ASP_ResidentCard_CardFileInput"  %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>制卡文件导入</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
<div class="tb">
市民卡->制卡文件导入

</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>

        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server" RenderMode="inline" >
            <ContentTemplate>
        <asp:BulletedList ID="bulMsgShow" runat="server"/>
        <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>


<div class="con">
  <div class="jieguo">选择制卡文件</div>
  <div class="kuang5">
 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
  
  <tr>
    <td style="width:8%" align="right">导入文件:</td>
    <td style="width:72%" colspan="5"> <asp:FileUpload ID="FileUpload1" runat="server" CssClass="inputlong"  Width="70%"/>
        <asp:Button ID="btnCheckFile" CssClass="button1" runat="server" Text="检查文件" OnClick="btnCheckFile_Click"/>
        <asp:Button ID="btnUpload" CssClass="button1" runat="server" Text="上传文件" OnClick="btnUpload_Click" Enabled="false"/>
    </td>

  </tr>

  <tr>
    <td align="right" >格式说明:</td>
    <td style="width:50%" >制卡文件每行为11列，数据采用“TAB”分割的的文本格式</td>
    
  </tr>
</table>

 </div>
         
        
  <div class="jieguo">
   <table width="95%" border="0"cellpadding="0" cellspacing="0">
      <tr >
        <td align="LEFT">售卡文件信息</td>
        <td width="*"></td>
        <td width="60"><asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="售卡" OnClick="btnSubmit_Click"/></td>
      </tr>
    </table>
  </div>
  <div class="kuang5">
<div class="gdtb" style="height:380px;overflow:scroll;width:100%; top:100PX;">
         <asp:GridView ID="gvResult" runat="server"
        Width = "1800px"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        AllowPaging="true"
        PageSize="100"
        PagerSettings-Mode="NumericFirstLast"
        PagerStyle-HorizontalAlign="left"
        PagerStyle-VerticalAlign="Top"
        AutoGenerateColumns="true"
         OnDataBound="gvResult_DataBound"
        EmptyDataText="没有数据记录!"
        OnPageIndexChanging="gvResult_Page"
         > 
        </asp:GridView>
</div>
  </div>
</div>


 </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="btnCheckFile" />
    </Triggers>    
        </asp:UpdatePanel>        

    </form>
</body>
</html>
