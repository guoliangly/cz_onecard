using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.PersonalBusiness;
using TDO.CardManager;
using TDO.PersonalTrade;
using TDO.BalanceChannel;
using TDO.UserManager;
using PDO.SpecialDeal;
using Common;
using TDO.ResourceManager;
using Master;

public partial class ASP_SpecialDeal_SD_SpeAdjustAcc : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //卡内余额,调账总额不可更改
            txtCurrMoney.Attributes.Add("readonly", "true");
            txtAdjustMoney.Attributes.Add("readonly", "true");

            //显示调账明细信息列表
            showAdjustAcc();

            //初始化打印凭证不可用
            btnPrintPZ.Enabled = false;

            //初始化调帐按钮不可用
            //btnAdjustAcc.Enabled = false;

            //测试模式下卡号可输入
            if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";
        }

    }

    private void showAdjustAcc()
    {
        //显示调账明细信息列表
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
    }

    protected void btnCashReadCard_Click(object sender, EventArgs e)
    {
        if (txtCardno.Text.Substring(0, 6) != "915008")
        {
            context.AddError("卡片不是金福卡", txtCardno);
            return;
        }

        hidShCheck.Value = "0";

        TMTableModule tmTMTableModule = new TMTableModule();

        //卡有效时
        //读取特殊调帐可充值账户表(TF_SPEADJUSTOFFERACC)中卡的调账信息

        TF_SPEADJUSTOFFERACCTDO tdoAdjAccIn = new TF_SPEADJUSTOFFERACCTDO();
        tdoAdjAccIn.CARDNO = txtCardno.Text;

        TF_SPEADJUSTOFFERACCTDO tdoAdjAccOut = (TF_SPEADJUSTOFFERACCTDO)tmTMTableModule.selByPK(context, tdoAdjAccIn, typeof(TF_SPEADJUSTOFFERACCTDO), null, "TF_SPEADJUSTOFFERACC_BYCARDNO", null);

        //没有调帐信息时

        if (tdoAdjAccOut == null || tdoAdjAccOut.OFFERMONEY <= 0)
        {
            ClearAdjustInfo();
            context.AddError("A009113102");
            return;
        }

        //显示卡内余额
        txtCurrMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / 100).ToString("0.00");

        //显示调账总额
        hidAdjustMoney.Value = tdoAdjAccOut.OFFERMONEY.ToString();

        txtAdjustMoney.Text = (Convert.ToDecimal(tdoAdjAccOut.OFFERMONEY) / 100).ToString("0.00");

        //查询调账明细并显示信息

        gvResult.DataSource = QueryResultColl();
        gvResult.DataBind();

        //启用调帐处理按钮
        btnAdjustAcc.Enabled = true;
    }

    //校验调帐时读龙城通卡
    protected void btnCheckCard_Click(object sender, EventArgs e)
    {

        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;

        bool ok = TMStorePModule.Excute(context, pdo);

        if (!ok) return;

        btnAdjustAcc_Click(sender, e);
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        hidShCheck.Value = "0";

        TMTableModule tmTMTableModule = new TMTableModule();
        //卡账户有效性检验


        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //卡有效时
            //读取特殊调帐可充值账户表(TF_SPEADJUSTOFFERACC)中卡的调账信息

            TF_SPEADJUSTOFFERACCTDO tdoAdjAccIn = new TF_SPEADJUSTOFFERACCTDO();
            tdoAdjAccIn.CARDNO = txtCardno.Text;

            TF_SPEADJUSTOFFERACCTDO tdoAdjAccOut = (TF_SPEADJUSTOFFERACCTDO)tmTMTableModule.selByPK(context, tdoAdjAccIn, typeof(TF_SPEADJUSTOFFERACCTDO), null, "TF_SPEADJUSTOFFERACC_BYCARDNO", null);

            //没有调帐信息时

            if (tdoAdjAccOut == null || tdoAdjAccOut.OFFERMONEY <= 0)
            {
                ClearAdjustInfo();
                context.AddError("A009113102");
                return;
            }

            //显示卡内余额
            txtCurrMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / 100).ToString("0.00");

            //显示调账总额
            hidAdjustMoney.Value = tdoAdjAccOut.OFFERMONEY.ToString();

            txtAdjustMoney.Text = (Convert.ToDecimal(tdoAdjAccOut.OFFERMONEY) / 100).ToString("0.00");

            //查询调账明细并显示信息

            gvResult.DataSource = QueryResultColl();
            gvResult.DataBind();

            //启用调帐处理按钮
            btnAdjustAcc.Enabled = true;

        }

        else
        {
            //清空界面信息
            ClearAdjustInfo();
        }

    }


    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        //分页处理 
        gvResult.PageIndex = e.NewPageIndex;

        btnReadCard_Click(sender, e);

    }



    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ControlDeal.RowDataBound(e);
    }


    public ICollection QueryResultColl()
    {
        //从特殊调帐台帐表(TF_B_SPEADJUSTACC)中读取数据

        DataTable data = SPHelper.callSDQuery(context, "SpeAdjustAcc", txtCardno.Text);
        return new DataView(data);
    }

    private bool AdjustAccValidation()
    {
        //对卡号非空的校验
        txtCardno.Text = txtCardno.Text.Trim();
        if (txtCardno.Text == "")
            context.AddError("A009113001", txtCardno);

        //对调帐总额非空的校验

        else if (txtAdjustMoney.Text.Trim() == "")
            context.AddError("A009113002", txtAdjustMoney);

        return context.hasError();

    }

    private void ClearAdjustInfo()
    {
        txtCardno.Text = "";
        //卡内余额
        txtCurrMoney.Text = "";

        //清空调帐总额
        txtAdjustMoney.Text = "";

        //显示调账明细信息列表
        showAdjustAcc();
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnAdjustAcc.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            AddMessage("M009113201");
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("A009113202");
        }
        else if (hidWarning.Value == "submit")
        {
            btnCheckCard_Click(sender, e);
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        hidWarning.Value = "";
    }

    private void GetCustInfo()
    {
        //取得IC卡用户的姓名,证件类型,证件号码
        TF_F_CUSTOMERRECTDO ddoIn = new TF_F_CUSTOMERRECTDO();
        ddoIn.CARDNO = txtCardno.Text;
        TMTableModule tmTMTableModule = new TMTableModule();

        TF_F_CUSTOMERRECTDO ddoOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoIn,
            typeof(TF_F_CUSTOMERRECTDO), null, "TF_F_CUSTOMERREC_QUERY_BYCARDNO", null);

        hidPaperNo.Value = ddoOut.PAPERNO;
        hidCustName.Value = ddoOut.CUSTNAME;

        //查询证件类型代码对应的证件类型名称

        string paperTypeSql = "select PAPERTYPENAME from TD_M_PAPERTYPE where PAPERTYPECODE ='" + ddoOut.PAPERNO + "'";

        DataTable data = tm.selByPKDataTable(context, paperTypeSql, 0);
        if (data != null && data.Rows.Count != 0)
        {
            Object[] row = data.Rows[0].ItemArray;
            hidPaperType.Value = getCellValue(row[0]);
        }
    }

    private string getCellValue(Object obj)
    {
        return (obj == DBNull.Value ? "" : ((string)obj).Trim());
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!DBreadValidation())
            return;

        if (txtCashGiftCardno.Text.Trim().Substring(0, 6) != "915008")
        {
            context.AddError("卡片不是金福卡", txtCashGiftCardno);
        }

        TMTableModule tmTMTableModule = new TMTableModule();
        //读取特殊调帐可充值账户表(TF_SPEADJUSTOFFERACC)中卡的调账信息

        TF_SPEADJUSTOFFERACCTDO tdoAdjAccIn = new TF_SPEADJUSTOFFERACCTDO();
        tdoAdjAccIn.CARDNO = txtCashGiftCardno.Text;

        TF_SPEADJUSTOFFERACCTDO tdoAdjAccOut = (TF_SPEADJUSTOFFERACCTDO)tmTMTableModule.selByPK(context, tdoAdjAccIn, typeof(TF_SPEADJUSTOFFERACCTDO), null, "TF_SPEADJUSTOFFERACC_BYCARDNO", null);

        //没有调帐信息时

        if (tdoAdjAccOut == null || tdoAdjAccOut.OFFERMONEY <= 0)
        {
            ClearAdjustInfo();
            context.AddError("A009113102");
            return;
        }

        //显示调账总额
        hidAdjustMoney.Value = tdoAdjAccOut.OFFERMONEY.ToString();

        txtAdjustMoney.Text = (Convert.ToDecimal(tdoAdjAccOut.OFFERMONEY) / 100).ToString("0.00");

        //查询调账明细并显示信息

        gvResult.DataSource = Query();
        gvResult.DataBind();

        hidShCheck.Value = "0";
        //如果客户调帐金额小于20则只能调帐至龙城通卡
        if (Convert.ToDecimal(tdoAdjAccOut.OFFERMONEY) < 2000)
        {
            hidShCheck.Value = "1";
            btnAdjustAcc.Enabled = true;
        }
        else
        {
            hidShCheck.Value = "2";
            btnAdjustAcc.Enabled = true;
            btnAdjustSale.Enabled = true;
        }
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCashGiftCardno.Text.Trim() == "")
            context.AddError("A001004113", txtCashGiftCardno);
        else
        {
            if ((txtCashGiftCardno.Text.Trim()).Length != 16)
                context.AddError("A001004114", txtCashGiftCardno);
            else if (!Validation.isNum(txtCashGiftCardno.Text.Trim()))
                context.AddError("A001004115", txtCashGiftCardno);
        }

        return !(context.hasError());
    }

    public ICollection Query()
    {
        //从特殊调帐台帐表(TF_B_SPEADJUSTACC)中读取数据

        DataTable data = SPHelper.callSDQuery(context, "SpeAdjustAcc", txtCashGiftCardno.Text);
        return new DataView(data);
    }

    protected void btnAdjustAcc_Click(object sender, EventArgs e)
    {

        if (hidShCheck.Value == "0")
        {
            //调用调账的判断处理

            if (AdjustAccValidation()) return;

            TMTableModule tmTMTableModule = new TMTableModule();
            //从IC卡电子钱包账户表中读取数据

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null, "TF_F_CARDEWALLETACC", null);

            //调用调账的判断处理

            SP_SD_SpeAdjustAccPDO pdo = new SP_SD_SpeAdjustAccPDO();

            //生成记录流水号
            string p_ID = hiddentradeno.Value + DateTime.Now.ToString("MMddhhmmss") + hiddenAsn.Value.Substring(12, 4);
            pdo.ID = p_ID;
            pdo.CARDNO = hidShCheck.Value == "0" ? txtCardno.Text : txtCashGiftCardno.Text;
            pdo.CARDTRADENO = hiddentradeno.Value;

            pdo.CARDMONEY = Convert.ToInt32(hiddencMoney.Value);

            pdo.CARDACCMONEY = ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY;

            pdo.ASN = hiddenAsn.Value.Substring(4, 16);
            pdo.CARDTYPECODE = hiddenLabCardtype.Value;
            pdo.TRADETYPECODE = "98";    //特殊调帐充值

            pdo.TERMNO = "112233445566"; //终端号固定为112233445566

            pdo.ADJUSTMONEY = Convert.ToInt32(hidAdjustMoney.Value); //调帐总额

            string strAdjustMoney = (Convert.ToDecimal(pdo.ADJUSTMONEY) / 100).ToString("0.00");

            pdo.OPERCARDNO = context.s_CardID; //操作员卡号

            bool ok = TMStorePModule.Excute(context, pdo);

            if (ok)
            {
                hidSupplyMoney.Value = "" + pdo.ADJUSTMONEY;

                string strIcBalance = (Convert.ToDecimal(pdo.CARDMONEY + pdo.ADJUSTMONEY) / 100).ToString("0.00");
                //充值写卡片信息
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                     "writeCardScript", "chargeCard();", true);

                btnPrintPZ.Enabled = true;

                //获取IC用户的姓名, 证件类型, 证件号码
                GetCustInfo();

                ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, hidCustName.Value, "充值", strAdjustMoney
                    , "", "", hidPaperNo.Value, strIcBalance, "",
                    strAdjustMoney, context.s_UserName, context.s_DepartName, hidPaperType.Value, "0.00", p_ID);

                //打印按钮启用
                //btnPrintPZ.Enabled = true;

                //调帐按钮不可用

                btnAdjustAcc.Enabled = false;
            }
        }
        else
        {
            hidShCheck.Value = "0";
            //调用调账的判断处理

            if (AdjustAccValidation()) return;

            TMTableModule tmTMTableModule = new TMTableModule();
            //从IC卡电子钱包账户表中读取数据

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null, "TF_F_CARDEWALLETACC", null);

            //调用调账的判断处理
            string strAdjustMoney = (Convert.ToDecimal(Convert.ToInt32(hidAdjustMoney.Value)) / 100).ToString("0.00");
            //记录流水号
            string p_ID = hiddentradeno.Value + DateTime.Now.ToString("MMddhhmmss") + hiddenAsn.Value.Substring(12, 4);
            context.SPOpen();
            context.AddField("p_ID").Value = p_ID;
            context.AddField("p_CARDNO").Value = txtCardno.Text;
            context.AddField("p_OLDCARDNO").Value = txtCashGiftCardno.Text;
            context.AddField("p_CARDTRADENO").Value = hiddentradeno.Value;
            context.AddField("p_CARDMONEY").Value = Convert.ToInt32(hiddencMoney.Value);
            context.AddField("p_CARDACCMONEY").Value = ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY;
            context.AddField("p_ASN").Value = hiddenAsn.Value.Substring(4, 16);
            context.AddField("p_CARDTYPECODE").Value = hiddenLabCardtype.Value;
            context.AddField("p_TRADETYPECODE").Value = "98";    //特殊调帐充值
            context.AddField("p_TERMNO").Value = "112233445566"; //终端号固定为112233445566
            context.AddField("p_ADJUSTMONEY").Value = Convert.ToInt32(hidAdjustMoney.Value); //调帐总额
            context.AddField("p_OPERCARDNO").Value = context.s_CardID; //操作员卡号
            bool ok = context.ExecuteSP("SP_SD_SpeAdjustAccCashToLCT");
            if (ok)
            {
                hidSupplyMoney.Value = "" + Convert.ToInt32(hidAdjustMoney.Value);

                string strIcBalance = (Convert.ToDecimal(Convert.ToInt32(hiddencMoney.Value) + Convert.ToInt32(hidAdjustMoney.Value)) / 100).ToString("0.00");
                //充值写卡片信息
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                     "writeCardScript", "chargeCard();", true);

                btnPrintPZ.Enabled = true;

                //获取IC用户的姓名, 证件类型, 证件号码
                GetCustInfo();

                ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, hidCustName.Value, "充值", strAdjustMoney
                    , "", "", hidPaperNo.Value, strIcBalance, "",
                    strAdjustMoney, context.s_UserName, context.s_DepartName, hidPaperType.Value, "0.00", p_ID);

                //打印按钮启用
                //btnPrintPZ.Enabled = true;

                //调帐按钮不可用

                btnAdjustAcc.Enabled = false;
            }
        }

        //清空信息
        //ClearAdjustInfo();
    }

    protected void btnAdjustSale_Click(object sender, EventArgs e)
    {
        hidShCheck.Value = "0";
        TMTableModule tmTMTableModule = new TMTableModule();

        //从IC卡资料表中读取老卡数据
        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCashGiftCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCashGiftCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            hidCustName.Value = "";
        }
        else
        {
            hidCustName.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
        }

        //从用户卡库存表(TL_R_ICUSER)中读取新卡数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = hiddenCardno.Value;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001004128");
            return;
        }

        if (ddoTL_R_ICUSEROut.RESSTATECODE != "01" && ddoTL_R_ICUSEROut.RESSTATECODE != "05" && ddoTL_R_ICUSEROut.RESSTATECODE != "04")
        {
            context.AddError("A094780101：卡库存状态不是分配或出库或新卡回收状态");
            return;
        }

        //if (Convert.ToDecimal(hiddenWallet2.Value) * 100 != ddoTL_R_ICUSEROut.CARDPRICE)
        //{
        //    context.AddError("电子钱包2不为20元,不能转值");
        //    return;
        //}

        //if ((Convert.ToDecimal(txtAdjustMoney.Text) * 100) < ddoTL_R_ICUSEROut.CARDPRICE)
        //{
        //    context.AddError("余额过小，不能进行调账售卡");
        //    return;
        //}

        SP_SD_AdjustSalePDO pdo = new SP_SD_AdjustSalePDO();
        string id = DealString.GetRecordID(hiddentradeno.Value, hiddenAsn.Value);
        pdo.ID = id;
        pdo.OLDCARDNO = txtCashGiftCardno.Text;
        //pdo.VALIDENDDATE = DateTime.Now.AddMonths(4).AddDays(1 - DateTime.Now.AddMonths(4).Day).AddDays(-1).ToString("yyyyMMdd");
        pdo.VALIDENDDATE = "20501231";
        pdo.CARDNO = hiddenCardno.Value;
        pdo.DEPOSIT = ddoTL_R_ICUSEROut.CARDPRICE;
        pdo.CARDMONEY = Convert.ToInt32(hiddencMoney.Value);
        pdo.ADJUSTMONEY = Convert.ToInt32(Convert.ToDecimal(txtAdjustMoney.Text) * 100);
        pdo.OTHERFEE = 0;
        pdo.CARDTRADENO = hiddentradeno.Value;
        pdo.CARDTYPECODE = txtCashGiftCardno.Text.Substring(4, 2);
        pdo.SELLCHANNELCODE = "01";
        pdo.SERSTAKETAG = "5";
        pdo.TRADETYPECODE = "56";
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;
        pdo.currDept = context.s_DepartID;
        pdo.currOper = context.s_UserID;
        string strAdjustMoney = (Convert.ToDecimal(pdo.ADJUSTMONEY) / 100).ToString("0.00");
        PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            string strIcBalance = ((Convert.ToDecimal(txtAdjustMoney.Text)) - 20).ToString("0.00");

            hidCardReaderToken.Value = cardReader.createToken(context);
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "startCashGiftCard(" + ((Convert.ToDecimal(txtAdjustMoney.Text) * 100) - 2000) + ");", true);

            ASHelper.preparePingZheng(ptnPingZheng, hiddenCardno.Value, "", "金福卡调帐", strAdjustMoney
                    , "", "", hidPaperNo.Value, strIcBalance, "",
                    strAdjustMoney, context.s_UserName, context.s_DepartName, hidPaperType.Value, "0.00", id);

            btnPrintPZ.Enabled = true;

            btnAdjustSale.Enabled = false;

        }
    }
}
