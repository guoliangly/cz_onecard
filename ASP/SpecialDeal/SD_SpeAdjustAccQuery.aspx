﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SD_SpeAdjustAccQuery.aspx.cs" Inherits="ASP_SpecialDeal_SD_SpeAdjustAccQuery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>特殊调帐查询</title>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
   
   
</head>
<body>
    <form id="form1" runat="server">
     <div class="tb">异常处理->特殊调帐查询</div>
         <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <ContentTemplate>
         
         <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
         
        <div class="con">
         <div class="card">调帐查询</div>
           <div class="kuang5">
              <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
             <tr>
               <td> <div align="right">审核状态:</div></td>
               <td>
	              <div >
	               <asp:DropDownList ID="selChkState" runat="server"  CssClass="input"></asp:DropDownList>
                  </div>
               </td>
               <td> <div align="right">部门:</div></td>  
               <td>
                  <div align="left">
                    <asp:DropDownList ID="selInSideDept" runat="Server"  CssClass="inputmidder"
                     OnSelectedIndexChanged="selDept_Changed" AutoPostBack="true"></asp:DropDownList>
                     
                  </div>
               </td>
                
               <td><div align="right">录入员工:</div></td>
               <td>
                  <div align="left">
                    <asp:DropDownList ID="selInStaff" runat="Server" CssClass="input"></asp:DropDownList>
                  </div>
               </td>
             
               <td><div align="right"></div></td>
               </tr>
             <tr>
               <td><div align="right">录入日期:</div></td>
               <td><div >
                 <asp:TextBox ID="txtInDate" runat="server" CssClass="input" MaxLength="8" ></asp:TextBox>
                 <ajaxToolkit:CalendarExtender ID="BeginCalendar" 
                          runat="server" TargetControlID="txtInDate" Format="yyyyMMdd"  PopupPosition="BottomLeft" />
               </div></td>
               <td><div align="right">IC卡号:</div></td>
               <td colspan="2">
                 <asp:TextBox ID="txtCardNo" runat="server" CssClass="inputmid" MaxLength="16" ></asp:TextBox>
                 </td>
               <td width="9%"><asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" OnClick="btnQuery_Click" /></td>
               <td>&nbsp;</td>
               </tr>
           </table>

         </div>

           <div class="jieguo">调帐信息</div>
          <div class="kuang5"> 
          <div id="gdtb" style="height:450px">
               <asp:GridView ID="gvResult" runat="server"
                    CssClass="tab1"
                    Width ="1500"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="200"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="Left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="true"
                    OnPageIndexChanging="gvResult_Page"
                    OnRowDataBound="gvResult_RowDataBound"
                    EmptyDataText="没有数据记录!" /> 
                    </div>                  
            </div>

        </div>
       </ContentTemplate>
     </asp:UpdatePanel>
    
    </form>
</body>
</html>
