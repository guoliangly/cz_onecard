﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="MS_SMSSend.aspx.cs"
    Inherits="ASP_SMSService_MS_SMSSend" %>

<%@ Register Src="~/Control/NewPager.ascx" TagName="NewPager" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>短信发送</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" AsyncPostBackTimeout="600"
        EnableScriptLocalization="true" ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
        function queryShow() {
            var activityNo = document.getElementById('txtActivityNO').value;
            if (activityNo == "") {
                MyExtConfirm('', '若要保证信息准确，请先选择活动<br>' +
                                '，是否无活动继续查询？', submitQuery);
            }
        }
        function submitQuery(btn) {
            if (btn == 'yes') {
                $get('btnConfirm').click();
            }
         
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="tb">
                短信业务->短信发送
            </div>
            <asp:BulletedList ID="bulMsgShow" runat="server" />
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <div class="con">
                <div class="base">
                    活动查询</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    开始日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="beginDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="endDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td>
                                <asp:Button ID="btnQueryA" runat="server" Text="查询" CssClass="button1" OnClick="btnQueryA_Click"
                                    Width="78px" />
                            </td>
                            <td width="27%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="jieguo">
                    查询结果
                </div>
                <div class="kuang5">
                    <asp:GridView ID="lvwActivity" runat="server" Width="100%" CssClass="tab1" AllowPaging="true"
                        PageSize="5" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                        SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                        PagerStyle-VerticalAlign="Top" AutoGenerateColumns="False" OnPageIndexChanging="lvwActivity_Page"
                        OnRowCreated="lvwActivity_RowCreated" OnSelectedIndexChanged="lvwActivity_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="活动编号" DataField="ACTIVITYNO" />
                            <asp:BoundField HeaderText="活动主题" DataField="ACTIVITYTHEME" />
                            <asp:BoundField HeaderText="创建日期" DataField="CREATETIME" />
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="tabsel" />
                        <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tabbt" />
                        <AlternatingRowStyle CssClass="tabjg" />
                        <EmptyDataTemplate>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                <tr class="tabbt">
                                    <td>
                                        活动编号
                                    </td>
                                    <td>
                                        活动主题
                                    </td>
                                    <td>
                                        创建日期
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动编号:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityNO" runat="server" CssClass="inputmid" MaxLength="12"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    活动主题:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityTheme" runat="server" CssClass="inputmid" MaxLength="10"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动短信内容:</div>
                            </td>
                            <td style="height: 50px">
                                <asp:TextBox ID="txtNoteMsg" runat="server" CssClass="inputmid" MaxLength="250" Width="500px"
                                    ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="base">
                    查询条件</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <asp:Panel ID="panElec" runat="server" Visible="false">
                            <tr>
                                <td>
                                    <div align="right">
                                        库内账户余额:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMinElecMoney" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMaxElecMoney" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                </td>
                                <td>
                                    <div align="right">
                                        钱包消费总金额:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMinTotalConsume" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMaxTotalConsume" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                </td>
                                <td>
                                    <div align="right">
                                        最后消费日期:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="lastConsumeDate" CssClass="inputmid" runat="server" MaxLength="8" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="lastConsumeDate"
                                        Format="yyyyMMdd" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                    </div>
                                </td>
                                <td>
                                    <%--    <asp:DropDownList ID="selConsumeBalunit" CssClass="inputmid" runat="server">
                                    </asp:DropDownList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        消费商户:</div>
                                </td>
                                <td colspan="7">
                                    <asp:TextBox ID="txtBalunit" CssClass="inputmid" runat="server" AutoPostBack="true"
                                        OnTextChanged="txtBalunit_Changed" Width="215px"></asp:TextBox>
                                    <asp:DropDownList ID="selBalunit" CssClass="inputmidder" Width="206" runat="server"
                                        AutoPostBack="true" OnSelectedIndexChanged="selBalunit_Changed">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                    </div>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="panAcc" runat="server" Visible="false">
                            <tr>
                                <td>
                                    <div align="right">
                                        账户宝开通:</div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selOpenState" runat="server" CssClass="inputmid">
                                        <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                        <asp:ListItem Value="0">0:已开通</asp:ListItem>
                                        <asp:ListItem Value="1">1:未开通</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                        账户宝类型:</div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selAccType" runat="server" CssClass="inputmid">
                                        <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                        <asp:ListItem Value="0001">账户宝</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                        账户宝余额:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMinAccMoney" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMaxAccMoney" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    <div align="right">
                                    </div>
                                </td>
                                <td>
                                    <div align="right">
                                        账户宝消费总额:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMinTotalAccCon" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMaxTotalAccCon" CssClass="inputmid" runat="server" Width="70px"
                                        MaxLength="6"></asp:TextBox>
                                    <div align="right">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="panOther" runat="server" Visible="false">
                            <tr>
                                <td>
                                    <div align="right">
                                        运营商类型:</div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selOperatorType" runat="server" CssClass="inputmid">
                                        <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                        <asp:ListItem Value="1">移动</asp:ListItem>
                                        <asp:ListItem Value="2">电信</asp:ListItem>
                                        <asp:ListItem Value="3">联通</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                        卡类型:</div>
                                </td>
                                <td colspan="5">
                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" EnableTheming="True" RepeatColumns="10">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td>
                                <div align="right">
                                    出生日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="birthDate" CssClass="inputmid" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="birthDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td>
                                <div align="right">
                                    联系地址:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="inputmid" MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    性别:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="selSex" runat="server" CssClass="inputmid">
                                    <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                    <asp:ListItem Value="0">0:男</asp:ListItem>
                                    <asp:ListItem Value="1">1:女</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <div align="right">
                                    年龄:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFromAge" CssClass="inputmid" runat="server" Width="70px" MaxLength="3"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtToAge" CssClass="inputmid" runat="server" Width="70px" MaxLength="3"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnQueryC" CssClass="button1" runat="server" Width="50px" Text="查询"
                                    OnClick="btnQueryC_Click" />
                                <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />&nbsp;
                                <asp:Button ID="btnShowElec" CssClass="button1" runat="server" Text="钱包信息>>" OnClick="btnShowElec_Click"
                                    Width="100px" />&nbsp;
                                <asp:Button ID="btnShowAcc" CssClass="button1" runat="server" Width="100px" Text="账户宝信息>>"
                                    OnClick="btnShowAcc_Click" />&nbsp;
                                <asp:Button ID="btnShowOther" CssClass="button1" runat="server" Width="100px" Text="其它信息>>"
                                    OnClick="btnShowOther_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="jieguo">
                    查询结果
                </div>
                <div class="kuang5">
                    <asp:Label ID="lblCommonMsg" runat="server" ForeColor="Red" Font-Size="Medium"></asp:Label><br />
                    <asp:Label ID="lblPhoneNum" runat="server" Font-Size="Medium"></asp:Label><br />
                    <asp:Label ID="lblCardNum" runat="server" Font-Size="Medium"></asp:Label>
                    <asp:TextBox ID="txtSendMaxNum" CssClass="input" runat="server" Text="0" Visible="false" />
                </div>
                <div class="base">
                    活动提交</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    发送方式:</div>
                            </td>
                            <td width="20%">
                                <asp:DropDownList ID="selSendType" runat="server" CssClass="inputmid" AutoPostBack="true" OnSelectedIndexChanged="selSendType_Changed">
                                    <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                    <asp:ListItem Value="0">0:随机</asp:ListItem>
                                    <asp:ListItem Value="1">1:顺序</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    发送数目:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="txtSendNum" CssClass="input" runat="server" MaxLength="5" ReadOnly="true" />
                            </td>
                            <%--    <td width="9%">
                                <div align="right">
                                    发送时间:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="txtNoteSendTime" CssClass="input" runat="server" MaxLength="8"/>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtNoteSendTime"
                                    Format="yyyyMMdd" />
                            </td>--%>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="button1" OnClick="btnSubmit_Click"
                                    Width="78px" />
                            </td>
                            <td width="27%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
