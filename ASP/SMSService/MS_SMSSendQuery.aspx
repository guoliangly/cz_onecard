﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MS_SMSSendQuery.aspx.cs"
    Inherits="ASP_SMSService_MS_SMSSendQuery" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>短信业务-短信发送查询统计</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="tb">
                短信业务-&gt;短信发送查询统计</div>
            <!-- #include file="../../ErrorMsg.inc" -->
            <div class="con">
                <div class="pip">
                    查询</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td>
                                <div align="right">
                                    开始日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="beginDate" CssClass="inputmid" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td>
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="endDate" CssClass="inputmid" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td>
                                <div align="right">
                                    类型:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="selProjectType" runat="server" CssClass="inputmid" AutoPostBack="true"
                                    OnSelectedIndexChanged="selProjectType_Changed">
                                    <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                    <asp:ListItem Value="0">商户活动</asp:ListItem>
                                    <asp:ListItem Value="1">企福通到账提醒</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <div align="right">
                                    项目名称:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="selProject" runat="server" CssClass="inputmid">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    <asp:CheckBox ID="chkFlag" runat="server" Text="统计" /></div>
                            </td>
                            <td colspan="2">
                                <div align="right">
                                    统计方式:</div>
                            </td>
                            <td colspan="4">
                                <asp:DropDownList ID="selCountType" runat="server" CssClass="inputmid">
                                    <asp:ListItem Value="1">1:日</asp:ListItem>
                                    <asp:ListItem Value="2">2:月</asp:ListItem>
                                    <asp:ListItem Value="3">3:年</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" Width="78px"
                                    OnClick="btnQuery_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <table border="0" id="queryans1" runat="server" width="95%">
                    <tr>
                        <td align="left">
                            <div class="jieguo">
                                查询结果</div>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnExport" CssClass="button1" runat="server" Width="78px" Text="导出Excel" Visible="false" />
                            <asp:Button ID="btnPrint" CssClass="button1" runat="server" Width="78px" Text="打印" Visible="false"
                                OnClientClick="return printGridView('printarea');" />
                        </td>
                    </tr>
                </table>
                <div class="kuang5">
                    <asp:GridView ID="lvwActivity" runat="server" Width="100%" CssClass="tab1" AllowPaging="true"
                        PageSize="20" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                        SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                        PagerStyle-VerticalAlign="Top" AutoGenerateColumns="true" OnPageIndexChanging="lvwActivity_Page">
                        <%--   <Columns>
                            <asp:BoundField HeaderText="日期" DataField="日期" />
                            <asp:BoundField HeaderText="项目名称" DataField="项目名称" />
                            <asp:BoundField HeaderText="短信数目" DataField="短信数目" />
                            <asp:BoundField HeaderText="成功数目" DataField="成功数目" />
                            <asp:BoundField HeaderText="失败数目" DataField="失败数目" />
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="tabsel" />
                        <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tabbt" />
                        <AlternatingRowStyle CssClass="tabjg" />
                        <EmptyDataTemplate>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                <tr class="tabbt">
                                    <td>
                                        日期
                                    </td>
                                    <td>
                                        项目名称
                                    </td>
                                    <td>
                                        短信数目
                                    </td>
                                    <td>
                                        成功数目
                                    </td>
                                    <td>
                                        失败数目
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>--%>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
