﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="MS_SMSBatchSend.aspx.cs"
    Inherits="ASP_SMSService_MS_SMSBatchSend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>短信业务-短信批量发送</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="tb">
                短信业务-&gt;短信批量发送</div>
            <asp:BulletedList ID="bulMsgShow" runat="server" />
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <div class="con">
                <div class="pip">
                    文件导入</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    导入文件:</div>
                            </td>
                            <td width="20%">
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="inputlong" Height="20px" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                </div>
                            </td>
                            <td width="20%">
                                <asp:Button ID="btnUpload" runat="server" Text="导入" CssClass="button1" Width="78px"
                                    OnClick="btnUpload_Click" />
                            </td>
                            <td width="10%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="kuang5">
                    <asp:GridView ID="lvwStaff" runat="server" Width="100%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                        AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast"
                        PagerStyle-HorizontalAlign="left" DataPagerStyle-VerticalAlign="Top" AutoGenerateColumns="false"
                        OnRowDataBound="lvwStaff_RowDataBound">
                        <PagerStyle CssClass="page" />
                        <Columns>
                            <asp:BoundField DataField="F0" HeaderText="客户姓名" />
                            <asp:BoundField DataField="F1" HeaderText="身份证号码" />
                            <asp:BoundField DataField="F2" HeaderText="电话号码" />
                        </Columns>
                        <EmptyDataTemplate>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                <tr class="tabbt">
                                    <td>
                                        客户姓名
                                    </td>
                                    <td>
                                        身份证号码
                                    </td>
                                    <td>
                                        电话号码
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div class="pip">
                    查询</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    开始日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="beginDate" CssClass="input" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="endDate" CssClass="input" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="10%">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                                <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" Width="78px"
                                    OnClick="btnQuery_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="base">
                    查询结果</div>
                <div class="kuang5">
                    <asp:GridView ID="lvwActivity" runat="server" Width="100%" CssClass="tab1" AllowPaging="true"
                        PageSize="5" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                        SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                        PagerStyle-VerticalAlign="Top" AutoGenerateColumns="False" OnPageIndexChanging="lvwActivity_Page"
                        OnRowCreated="lvwActivity_RowCreated" OnSelectedIndexChanged="lvwActivity_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="活动编号" DataField="ACTIVITYNO" />
                            <asp:BoundField HeaderText="活动主题" DataField="ACTIVITYTHEME" />
                            <asp:BoundField HeaderText="创建日期" DataField="CREATETIME" />
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                        <SelectedRowStyle CssClass="tabsel" />
                        <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tabbt" />
                        <AlternatingRowStyle CssClass="tabjg" />
                        <EmptyDataTemplate>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                <tr class="tabbt">
                                    <td>
                                        活动编号
                                    </td>
                                    <td>
                                        活动主题
                                    </td>
                                    <td>
                                        创建日期
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动编号:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityNO" runat="server" CssClass="inputmid" MaxLength="12"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    活动主题:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityTheme" runat="server" CssClass="inputmid" MaxLength="20"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动短信内容:</div>
                            </td>
                            <td style="height: 50px">
                                <asp:TextBox ID="txtNoteMsg" runat="server" CssClass="inputmid" MaxLength="250" Width="500px"
                                    ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="base">
                    活动提交</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right" style="display: none">
                                    发送数目:</div>
                            </td>
                            <td width="20%" style="display: none">
                                <asp:TextBox ID="txtSendNum" CssClass="input" runat="server" MaxLength="5" />
                            </td>
                            <td width="40%">
                            </td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="提交" CssClass="button1" OnClick="btnSave_Click"
                                    Width="78px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
