﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using PDO.SMSService;
using Master;

public partial class ASP_SMSService_MS_ActivityConfig : Master.Master
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lvwActivity.DataKeyNames = new string[] { "ACTIVITYNO", "ACTIVITYTHEME", "CREATETIME", "NOTECOUNT", "ACTIVITYEXP", "NOTEMSG", "USETAG" };
            initLoad(sender, e);
            UserCardHelper.resetData(lvwActivity, null);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        beginDate.Text = DateTime.Today.AddDays(-7).ToString("yyyyMMdd");
        endDate.Text = DateTime.Today.ToString("yyyyMMdd");
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!dateValidation())
            return;

        lvwActivity.DataSource = CreateStaffQueryDataSource();
        lvwActivity.DataBind();
    }

    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        return !(context.hasError());
    }

    public ICollection CreateStaffQueryDataSource()
    {
        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "QueryActivityInfo";
        pdo.var1 = beginDate.Text.Trim().ToString();
        pdo.var2 = endDate.Text.Trim().ToString();
        pdo.var3 = selUseTag.SelectedValue;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count < 1)
        {
            lvwActivity.DataSource = new DataTable();
            lvwActivity.DataBind();
            context.AddMessage("未查出活动信息记录");
        }
        return new DataView(data);
    }

    public void lvwActivity_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwActivity.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void lvwActivity_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwActivity','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void lvwActivity_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtActivityNO.Text = getDataKeys2("ACTIVITYNO");
        txtActivityTheme.Text = getDataKeys2("ACTIVITYTHEME");
        txtActivityExp.Value = getDataKeys2("ACTIVITYEXP");
        txtNoteMsg.Value = getDataKeys2("NOTEMSG");
    }

    public String getDataKeys2(String keysname)
    {
        return lvwActivity.DataKeys[lvwActivity.SelectedIndex][keysname].ToString();
    }

    protected void btnActivityAdd_Click(object sender, EventArgs e)
    {
        if (!ValidInput())
            return;
        context.SPOpen();
        context.AddField("P_ACTIVITYNO").Value = txtActivityNO.Text.Trim();
        context.AddField("P_ACTIVITYTHEME").Value = txtActivityTheme.Text.Trim();
        context.AddField("P_ACTIVITYEXP").Value = txtActivityExp.Value.Trim();
        context.AddField("P_NOTEMSG").Value = txtNoteMsg.Value.Trim();
        bool ok = context.ExecuteSP("SP_MS_ACTIVITYADD");
        if (ok)
        {
            AddMessage("新增成功");
            btnQuery_Click(sender, e);
        }

        ClearActivity();
    }

    protected void btnActivityModify_Click(object sender, EventArgs e)
    {
        if (!MofifyValidation())
            return;
     
        context.SPOpen();
        context.AddField("P_ACTIVITYNO").Value = txtActivityNO.Text.Trim();
        context.AddField("P_ACTIVITYTHEME").Value = txtActivityTheme.Text.Trim();
        context.AddField("P_ACTIVITYEXP").Value = txtActivityExp.Value.Trim();
        context.AddField("P_NOTEMSG").Value = txtNoteMsg.Value.Trim();
        bool ok = context.ExecuteSP("SP_MS_ACTIVITYEDIT");
        if (ok)
        {
            AddMessage("修改成功");
            btnQuery_Click(sender, e);
        }

        ClearActivity();
    }

    protected void btnActivityDelete_Click(object sender, EventArgs e)
    {
        if (!DeleteValidation())
            return;

        context.SPOpen();
        context.AddField("P_ACTIVITYNO").Value = txtActivityNO.Text.Trim();
        context.AddField("P_ACTIVITYTHEME").Value = txtActivityTheme.Text.Trim();
        context.AddField("P_ACTIVITYEXP").Value = txtActivityExp.Value.Trim();
        context.AddField("P_NOTEMSG").Value = txtNoteMsg.Value.Trim();
        bool ok = context.ExecuteSP("SP_MS_ACTIVITYDELETE");
        if (ok)
        {
            AddMessage("删除成功");
            btnQuery_Click(sender, e);
        }
        ClearActivity();
    }

    private Boolean ValidInput()
    {
        //对活动编号进行非空、字母数字检验
        String strActivityNo = txtActivityNO.Text.Trim();
        if (strActivityNo == "")
        { 
            context.AddError("活动编号不能为空", txtActivityNO); 
            return false; 
        }
        else
        {
            if (!Validation.isCharNum(txtActivityNO.Text.Trim()))
            {
                context.AddError("活动编号须为数字字母组合", txtActivityNO);
                return false;
            }
            if (Validation.strLen(txtActivityNO.Text.Trim()) > 12)
            {
                context.AddError("活动编号不能超过12字节", txtActivityNO);
                return false;
            }
        }

        //对活动主题进行非空、长度检验
        String strActivityTheme = txtActivityTheme.Text.Trim();
        if (strActivityTheme == "")
        {
            context.AddError("活动主题不能为空", txtActivityTheme);
            return false;    
        }
        else
        {
            if (Validation.strLen(strActivityTheme) > 20)
            {
                context.AddError("活动主题不能超过20字节", txtActivityTheme);
                return false;
            }
        }

        //对活动说明进行非空、长度检验
        String strActivityExp = txtActivityExp.Value.Trim();
        if (strActivityExp == "")
        {
            context.AddError("活动说明不能为空");
            return false;
        }
        else
        {
            if (Validation.strLen(strActivityExp) > 400)
            {
                context.AddError("活动说明不能超过400字节");
                return false;
            }
        }

        //对活动短信内容进行非空、长度检验
        String strNoteMsg = txtNoteMsg.Value.Trim();
        if (strNoteMsg == "")
        {
            context.AddError("活动短信内容不能为空");
            return false;
        }
        else
        {
            if (Validation.strLen(strNoteMsg) > 400)
            {
                context.AddError("活动短信内容不能超过400字节");
                return false;
            }
        }

        if (context.hasError())
            return false;
        else
            return true;
    }

    private Boolean MofifyValidation()
    {
        //判断活动编码是否为空白
        if (lvwActivity.SelectedIndex < 0)
        {
            context.AddError("没有选择可处理的记录", txtActivityNO);
            return false;    
        }

        //判断该活动编码是否是选中的活动编码
        if (txtActivityNO.Text.Trim() != getDataKeys2("ACTIVITYNO"))
        {
            context.AddError("活动编号不能修改", txtActivityNO);
            return false;    
        }

        //判断该活动主题是否是选中的活动主题
        if (txtActivityTheme.Text.Trim() != getDataKeys2("ACTIVITYTHEME"))
        {
            context.AddError("活动主题不能修改", txtActivityTheme);
            return false;    
        }

        if (getDataKeys2("USETAG") == "无效")
        {
            context.AddError("无效活动不能做修改删除操作");
            return false;    
        }

        //对修改（删除）的活动信息检验
        ValidInput();
        return CheckContext();
    }

    private Boolean DeleteValidation()
    {
        //判断活动编码是否为空白
        if (lvwActivity.SelectedIndex < 0)
        {
            context.AddError("没有选择可处理的记录", txtActivityNO);
            return false;    
        }

        //判断该活动编码是否是选中的活动编码
        if (txtActivityNO.Text.Trim() != getDataKeys2("ACTIVITYNO"))
        {
            context.AddError("活动编号与选择的记录编号不一致,无法删除", txtActivityNO);
            return false;    
        }

        //判断该活动主题是否是选中的活动主题
        if (txtActivityTheme.Text.Trim() != getDataKeys2("ACTIVITYTHEME"))
        {
            context.AddError("活动主题与选择的记录主题不一致,无法删除", txtActivityTheme);
            return false;    
        }

        if (getDataKeys2("USETAG") == "无效")
        {
            context.AddError("无效活动不能做修改删除操作");
            return false;    
        }

        //对修改（删除）的活动信息检验
        ValidInput();
        return CheckContext();
    }

    private Boolean CheckContext()
    {
        if (context.hasError())
            return false;
        else
            return true;
    }

    private void ClearActivity()
    {
        txtActivityNO.Text = "";
        txtActivityTheme.Text = "";
        txtActivityExp.Value = "";
        txtNoteMsg.Value = "";
    }
}
