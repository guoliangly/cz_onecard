﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using Common;
using TM;
using System.IO;
using System.Text;
using PDO.SMSService;
using Master;

public partial class ASP_SMSService_MS_SMSBatchSend : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lvwActivity.DataKeyNames = new string[] { "ACTIVITYNO", "ACTIVITYTHEME", "NOTEMSG" };
            lvwStaff.DataSource = new DataTable();
            lvwStaff.DataBind();
            UserCardHelper.resetData(lvwActivity, null);
            beginDate.Text = DateTime.Today.AddDays(-7).ToString("yyyyMMdd");
            endDate.Text = DateTime.Today.ToString("yyyyMMdd");
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //导入文件
        if (!FileUpload1.HasFile)
        {
            context.AddError("请导入一个有效文件");
            return;
        }

        int len = FileUpload1.FileBytes.Length;

        if (len <= 0)
        {
            context.AddError("导入文件小于0");
            return;
        }

        if (len > 5 * 1024 * 1024) // 5M
        {
            context.AddError("导入文件大于5M");
            return;
        }

        // 首先清空临时表
        clearTempTable();

        context.DBOpen("Insert");

        //读取文件
        Stream stream = FileUpload1.FileContent;
        StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("gb2312"));
        string strLine = "";
        int lineCount = 0;
        String[] fields = null;

        //客户姓名列表
        ArrayList groupMobliePhone = new ArrayList();

        //循环读取文件所有记录行
        while ((strLine = reader.ReadLine()) != null)
        {
            ++lineCount;

            strLine = strLine.Trim();

            if (strLine.IndexOf("'") != -1)
            {
                context.AddError("第" + lineCount + "行包含非法字符'");
                continue;
            }

            if (strLine.Length > 316)
            {
                //写文件
                context.AddError("第" + lineCount + "行长度为" + strLine.Length + ", 根据格式定义不能超过347位");
                continue;
            }

            fields = strLine.Split(new char[1] { ',' });

            // 字段数目为2到8时都合法
            if (fields.Length < 2 || fields.Length > 8)
            {
                context.AddError("第" + lineCount + "行字段数目为" + fields.Length + ", 根据格式定义必须为2个,最多8个");
                continue;
            }

            //一行记录中的姓名
            string custname = fields[0].Trim();
            if (custname == "")
            {
                context.AddError("第" + lineCount + "行姓名为空");
            }
            else if (Validation.strLen(custname) > 25)
            {
                context.AddError("第" + lineCount + "行客户姓名长度过大");
            }

            //身份证号码校验
            string papercode = fields[1].Trim();
            if (papercode == "")
            {
                context.AddError("第" + lineCount + "行身份证号码为空");
            }
            else if (Validation.strLen(papercode) > 20)
            {
                context.AddError("第" + lineCount + "行身份证号码长度过大");
            }

            //手机号码校验
            string mobilephone = fields[2].Trim();
            if (mobilephone == "")
            {
                context.AddError("第" + lineCount + "行手机号码为空");
            }
            else if (Validation.strLen(mobilephone) != 11)
            {
                context.AddError("第" + lineCount + "行手机号码不为11位");
            }

            groupMobliePhone.Add(mobilephone);
            checkPhone(mobilephone, groupMobliePhone, lineCount, "手机号码");

            if (!context.hasError())
            {
                //记录插入数据库
                context.ExecuteNonQuery("insert into TMP_COMMON(F0,F1,F2,F3) values('" + custname + "','" + papercode + "', '" + mobilephone + "','" + Session.SessionID + "')");
            }
            txtSendNum.Text = lineCount.ToString();
        }

        if (!context.hasError())
        {
            context.DBCommit();
            createGridViewData();
            AddMessage("客户信息批量导入文件成功");
        }

        else
        {
            context.RollBack();
            lvwStaff.DataSource = new DataTable();
            lvwStaff.DataBind();
        }
    }

    private void createGridViewData()
    {
        //查询临时表TMP_COMMON中数据
        string sql = "SELECT F0, F1, F2 FROM TMP_COMMON where F3 = '" + Session.SessionID + "'";

        TMTableModule tm = new TMTableModule();
        DataTable data = tm.selByPKDataTable(context, sql, 10000);

        lvwStaff.DataSource = data;
        lvwStaff.DataBind();
    }

    protected void lvwStaff_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Cells[2].Text = DecryptString.DecodeString(e.Row.Cells[2].Text);
        //}
    }

    private void checkPhone(string mobilephone, ArrayList groupcust, int lineCount, string groupInfo)
    {
        int count = 0;
        foreach (string i in groupcust)
        {
            if (mobilephone == i)
            {
                ++count;
            }
        }

        if (count >= 2)
        {
            context.AddError("第" + lineCount + "行手机号码" + groupInfo + "在本文件中重复。");
            return;
        }
    }

    private void clearTempTable()
    {
        //删除临时表数据
        context.DBOpen("Delete");
        context.ExecuteNonQuery(" delete from TMP_COMMON " +
                              " where F3 = '" + Session.SessionID + "'");
        context.DBCommit();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!SaveValidation())
            return;

        if (lvwStaff.Rows.Count > 0)
        {
            context.SPOpen();
            context.AddField("p_sessionID").Value = Session.SessionID;
            context.AddField("p_ActivityNO").Value = txtActivityNO.Text.Trim();
            context.AddField("p_ActivityTheme").Value = txtActivityTheme.Text.Trim();
            context.AddField("p_NoteMsg").Value = txtNoteMsg.Text.Trim();
            context.AddField("p_SendNum").Value = txtSendNum.Text.Trim();
            context.AddField("p_SendTime").Value = "";

            bool ok = context.ExecuteSP("SP_MS_NOTESENDBATCHADD");
            if (ok)
            {
                AddMessage("客户信息批量保存成功");
                clearTempTable();
            }
        }
        else
        {
            context.AddError("导入结果为空，无法保存");
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!dateValidation())
            return;

        //查询员工信息
        lvwActivity.DataSource = CreateStaffQueryDataSource();
        lvwActivity.DataBind();
    }

    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        return !(context.hasError());
    }

    public ICollection CreateStaffQueryDataSource()
    {
        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "QueryActivityInfo";
        pdo.var1 = beginDate.Text.Trim().ToString();
        pdo.var2 = endDate.Text.Trim().ToString();
        pdo.var3 = "1";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count < 1)
        {
            lvwActivity.DataSource = new DataTable();
            lvwActivity.DataBind();
            context.AddMessage("未查出活动信息记录");
        }
        return new DataView(data);
    }

    public void lvwActivity_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwActivity.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void lvwActivity_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwActivity','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void lvwActivity_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtActivityNO.Text = getDataKeys2("ACTIVITYNO");
        txtActivityTheme.Text = getDataKeys2("ACTIVITYTHEME");
        txtNoteMsg.Text = getDataKeys2("NOTEMSG");
    }

    public String getDataKeys2(String keysname)
    {
        return lvwActivity.DataKeys[lvwActivity.SelectedIndex][keysname].ToString();
    }

    private Boolean SaveValidation()
    {
        //判断活动编码是否为空白
        if (lvwActivity.SelectedIndex < 0)
        {
            context.AddError("没有选择可处理的记录", txtActivityNO);
            return false;         
        }
        //判断该活动编码是否是选中的活动编码
        if (txtActivityNO.Text.Trim() != getDataKeys2("ACTIVITYNO"))
        {
            context.AddError("活动编号与选择的记录编号不一致", txtActivityNO);
            return false;
        }

        //判断该活动主题是否是选中的活动主题
        if (txtActivityTheme.Text.Trim() != getDataKeys2("ACTIVITYTHEME"))
        {
            context.AddError("活动主题与选择的记录主题不一致", txtActivityTheme);
            return false;
        }

        return CheckContext();
    }

    private Boolean CheckContext()
    {
        if (context.hasError())
            return false;
        else
            return true;
    }
}