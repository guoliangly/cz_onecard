﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using PDO.SMSService;
using Master;

public partial class ASP_SMSService_MS_SMSSendApproval : Master.Master
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //指定员工GridView DataKeyNames
            lvwActivity.DataKeyNames = new string[] { "ID", "ACTIVITYNO", "ACTIVITYTHEME", "NOTEMSG", "ACTIVITYTYPE" };
            initLoad(sender, e);
            UserCardHelper.resetData(lvwActivity, null);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        beginDate.Text = DateTime.Today.AddDays(-7).ToString("yyyyMMdd");
        endDate.Text = DateTime.Today.ToString("yyyyMMdd");
       
        DataTable dt1 = new DataTable();
        context.DBOpen("Select");
        string sql1;
        sql1 = @"SELECT TAGVALUE  FROM TD_M_TAG WHERE USETAG = '1' AND TAGCODE='SMSPUSHTIME'";
        dt1 = context.ExecuteReader(sql1);
        lblSendTime.Text = dt1.Rows[0][0].ToString();

        DataTable dt2 = new DataTable();
        context.DBOpen("Select");
        string sql2;
        sql2 = @"select count(*) from TF_F_TASKDETAILS_SMS t where t.smsstatus is null and t.STATUSDES is null";
        dt2 = context.ExecuteReader(sql2);
        lblWaitSMSNum.Text = dt2.Rows[0][0].ToString();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!dateValidation())
            return;

        //查询员工信息
        lvwActivity.DataSource = CreateStaffQueryDataSource();
        lvwActivity.DataBind();
    }

    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        return !(context.hasError());
    }

    public ICollection CreateStaffQueryDataSource()
    {
        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "QueryNoteSendCheck";
        pdo.var1 = beginDate.Text.Trim().ToString();
        pdo.var2 = endDate.Text.Trim().ToString();
        pdo.var3 = selCheckState.SelectedValue;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count < 1)
        {
            lvwActivity.DataSource = new DataTable();
            lvwActivity.DataBind();
            context.AddMessage("未查出短信发送任务记录");
        }
        return new DataView(data);
    }

    public void lvwActivity_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwActivity.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void lvwActivity_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }

    protected void lvwActivity_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwActivity','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void lvwActivity_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtActivityNO.Text = getDataKeys2("ACTIVITYNO");
        txtActivityTheme.Text = getDataKeys2("ACTIVITYTHEME");
        txtNoteMsg.Text = getDataKeys2("NOTEMSG");
    }

    public String getDataKeys2(String keysname)
    {
        return lvwActivity.DataKeys[lvwActivity.SelectedIndex][keysname].ToString();
    }

    protected void CheckAll(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in lvwActivity.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }

    protected void btnPass_Click(object sender, EventArgs e)
    {    
        if (!RecordIntoTmp()) return;
        context.SPOpen();
        context.AddField("p_sessionID").Value = Session.SessionID;
        context.AddField("P_CHECKSTATE").Value = "2";
        if (chkyx.Checked)
        {
            context.AddField("P_PRIORITY").Value = "2";
        }
        else
        {
            context.AddField("P_PRIORITY").Value = "1";
        }
      
        bool ok = context.ExecuteSP("SP_MS_NOTESENDCHECK");
        if (ok)
        {
            AddMessage("审核成功");
            btnQuery_Click(sender, e);
        }

        ClearActivity();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (!RecordIntoTmp()) return;
        context.SPOpen();
        context.AddField("p_sessionID").Value = Session.SessionID;
        context.AddField("P_CHECKSTATE").Value = "3";
        context.AddField("P_PRIORITY").Value = "1";
        bool ok = context.ExecuteSP("SP_MS_NOTESENDCHECK");
        if (ok)
        {
            AddMessage("作废成功");
            btnQuery_Click(sender, e);
        }
        ClearActivity();
    }

    private bool RecordIntoTmp()
    {
        //清空临时表
        clearTempTable();

        //记录入临时表
        context.DBOpen("Insert");

        int count = 0;
        //循环将选中行入临时表
        foreach (GridViewRow gvr in lvwActivity.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (cb != null && cb.Checked)
            {
                ++count;
                context.ExecuteNonQuery("insert into TMP_COMMON (F0,F1,F2,F3) values('" +
                        Session.SessionID + "', '" + gvr.Cells[1].Text + "', '" + gvr.Cells[2].Text + "', '" + gvr.Cells[9].Text + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误
        if (count <= 0)
        {
            context.AddError("没有选中任何行");
            return false;
        }

        return true;
    }

    private void clearTempTable()
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_COMMON where F0='"
            + Session.SessionID + "'");
        context.DBCommit();
    }

    private void ClearActivity()
    {
        //清除输入的活动信息
        txtActivityNO.Text = "";
        txtActivityTheme.Text = "";
        txtNoteMsg.Text = "";
    }
}
