﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MS_ActivityConfig.aspx.cs"
    Inherits="ASP_SMSService_MS_ActivityConfig" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>短信业务-活动创建</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #txtNoteMsg
        {
            width: 400px;
            height: 50px;
        }
        #txtActivityExp
        {
            width: 400px;
            height: 50px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
        function countChar(textarea, spanName) {
            var txt = document.getElementById(textarea).value;
            var num = txt.length;
            document.getElementById(spanName).innerHTML = num + "/" + (num % 70 == 0 ? parseInt(num / 70) : (parseInt(num / 70) + 1));
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="tb">
                短信业务-&gt;活动创建</div>
            <!-- #include file="../../ErrorMsg.inc" -->
            <div class="con">
                <div class="pip">
                    活动信息维护</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    开始日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="beginDate" CssClass="input" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="endDate" CssClass="input" runat="server" MaxLength="8" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    有效标志:</div>
                            </td>
                            <td width="15%">
                                <asp:DropDownList ID="selUseTag" runat="server">
                                    <asp:ListItem Value="" Selected="True">---请选择---</asp:ListItem>
                                    <asp:ListItem Value="0">0:无效</asp:ListItem>
                                    <asp:ListItem Value="1">1:有效</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" Width="78px"
                                    OnClick="btnQuery_Click" />
                            </td>
                            <td width="27%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="kuang5">
                    <div class="gdtb" style="height: 300px">
                        <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1">
                            <asp:GridView ID="lvwActivity" runat="server" Width="100%" CssClass="tab1" AllowPaging="true"
                                PageSize="10" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" AutoGenerateColumns="False" OnPageIndexChanging="lvwActivity_Page"
                                OnRowCreated="lvwActivity_RowCreated" OnSelectedIndexChanged="lvwActivity_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField HeaderText="活动编号" DataField="ACTIVITYNO" />
                                    <asp:BoundField HeaderText="活动主题" DataField="ACTIVITYTHEME" />
                                    <asp:BoundField HeaderText="创建日期" DataField="CREATETIME" />
                                    <asp:BoundField HeaderText="有效标识" DataField="USETAG" />
                                    <asp:BoundField HeaderText="已发短信数" DataField="NOTECOUNT" />
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                        <tr class="tabbt">
                                            <td>
                                                活动编号
                                            </td>
                                            <td>
                                                活动主题
                                            </td>
                                            <td>
                                                创建日期
                                            </td>
                                            <td>
                                                有效标识
                                            </td>
                                            <td>
                                                已发短信数
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </table>
                    </div>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    活动编号:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityNO" runat="server" CssClass="inputmid" MaxLength="12"
                                    Width="300px"></asp:TextBox><span class="red">*</span>
                            </td>
                            <td width="10%">
                                <div align="right">
                                    活动主题:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityTheme" runat="server" CssClass="inputmid" MaxLength="10"
                                    Width="300px"></asp:TextBox><span class="red">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    短信内容:</div>
                            </td>
                            <td style="height: 50px">
                                <textarea id="txtNoteMsg" runat="server" cols="40" rows="5" onkeydown="countChar('txtNoteMsg','counter');"
                                    onkeyup="countChar('txtNoteMsg','counter');"></textarea>
                                <span class="red">*</span> <span id="counter"></span>
                                <br />
                            </td>
                            <td width="10%">
                                <div align="right">
                                    活动说明:</div>
                            </td>
                            <td style="height: 50px">
                                <textarea id="txtActivityExp" runat="server" cols="40" rows="5"></textarea><span
                                    class="red">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 30px">
                                <div align="right">
                                </div>
                            </td>
                            <td colspan="3">
                                <table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnActivityAdd" runat="server" Text="增加" Width="78px" CssClass="button1"
                                                OnClick="btnActivityAdd_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnActivityModify" runat="server" Text="修改" Width="78px" CssClass="button1"
                                                OnClick="btnActivityModify_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnActivityDelete" runat="server" Text="删除" Width="78px" CssClass="button1"
                                                OnClick="btnActivityDelete_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
