﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using PDO.SMSService;
using Common;

/***********************************************
 * update: chenwentao 2014-10-09
 * content: 修改手机号验证规则
***********************************************/
public partial class ASP_SMSService_MS_SMSSend : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "GetCardType";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data.Rows.Count > 0)
        {
            CheckBoxList1.DataSource = data;
            CheckBoxList1.DataTextField = "cardtypename";
            CheckBoxList1.DataValueField = "cardtypecode";
            CheckBoxList1.DataBind();
        }

        lvwActivity.DataKeyNames = new string[] { "ACTIVITYNO", "ACTIVITYTHEME", "NOTEMSG" };
        initLoad(sender, e);
        UserCardHelper.resetData(lvwActivity, null);
    }

    protected void initLoad(object sender, EventArgs e)
    {
        beginDate.Text = DateTime.Today.AddDays(-7).ToString("yyyyMMdd");
        endDate.Text = DateTime.Today.ToString("yyyyMMdd");
    }

    protected void btnShowElec_Click(object sender, EventArgs e)
    {
        panElec.Visible = btnShowElec.Text == "钱包信息>>";
        btnShowElec.Text = panElec.Visible ? "钱包信息<<" : "钱包信息>>";
    }

    protected void btnShowAcc_Click(object sender, EventArgs e)
    {
        panAcc.Visible = btnShowAcc.Text == "账户宝信息>>";
        btnShowAcc.Text = panAcc.Visible ? "账户宝信息<<" : "账户宝信息>>";
    }

    protected void btnShowOther_Click(object sender, EventArgs e)
    {
        panOther.Visible = btnShowOther.Text == "其它信息>>";
        btnShowOther.Text = panOther.Visible ? "其它信息<<" : "其它信息>>";
    }

    protected void btnQueryC_Click(object sender, EventArgs e)
    {
        if (txtActivityNO.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "queryShow();", true);
        }
        else
        {
            btnConfirm_Click(sender, e);
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        QueryValidInput();

        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "QueryNoteSendInfo";
        //用户个人信息
        pdo.var1 = txtFromAge.Text.Trim();
        pdo.var2 = txtToAge.Text.Trim();
        pdo.var3 = selSex.SelectedValue;
        pdo.var4 = birthDate.Text.Trim();
        if (txtAddress.Text.Trim() != "")
        {
            pdo.var5 = "%" + txtAddress.Text.Trim() + "%";
        }
        else
        {
            pdo.var5 = "";
        }
        //电子钱包账户信息
        pdo.var6 = txtMinElecMoney.Text.Trim();
        pdo.var7 = txtMaxElecMoney.Text.Trim();
        pdo.var8 = lastConsumeDate.Text.Trim();
        pdo.var9 = txtMinTotalConsume.Text.Trim();
        pdo.var10 = txtMaxTotalConsume.Text.Trim();
        if (txtBalunit.Text == "")
        {
            pdo.var11 = "";
        }
        else
        {
            pdo.var11 = selBalunit.SelectedItem.ToString().Substring(0, 8).Trim();
        }

        //账户宝未开通
        if (selOpenState.SelectedValue == "1")
        {
            pdo.var13 = "";
            pdo.var14 = "";
            pdo.var15 = "";
            pdo.var16 = "";
            pdo.var17 = "";
        }
        else
        {
            pdo.var13 = txtMinAccMoney.Text.Trim();
            pdo.var14 = txtMaxAccMoney.Text.Trim();
            pdo.var15 = txtMinTotalAccCon.Text.Trim();
            pdo.var16 = txtMaxTotalAccCon.Text.Trim();
            pdo.var17 = selAccType.SelectedValue;
        }

        string str = string.Empty;
        pdo.var12 = selOpenState.SelectedValue;
        str = selOpenState.SelectedValue;
        if (txtMinAccMoney.Text.Trim() != "" || txtMaxAccMoney.Text.Trim() != "" || txtMinTotalAccCon.Text.Trim() != "" || txtMaxTotalAccCon.Text.Trim() != "" || selAccType.SelectedValue != "")
        {
            pdo.var12 = "0";
            str = "0";
        }



        //其它信息
        if (selOperatorType.SelectedValue == "1")
        {

            pdo.var18 = "134,135,136,137,138,139,147,150,151,152,157,158,159,182,187,188,178,183,184";
        }
        else if (selOperatorType.SelectedValue == "2")
        {
            pdo.var18 = "189,133,153,180,181,177";
        }
        else if (selOperatorType.SelectedValue == "3")
        {
            pdo.var18 = "130,131,132,155,156,185,186,145,176";
        }
        else
        {
            pdo.var18 = "134,135,136,137,138,139,147,150,151,152,157,158,159,182,187,188,189,133,153,180,130,131,132,155,156,185,186,178,183,184,181,177,145,176";
        }
        string strCardType = string.Empty;
        for (int i = 0; i < CheckBoxList1.Items.Count; i++)
        {
            if (CheckBoxList1.Items[i].Selected == true)
            {
                strCardType += CheckBoxList1.Items[i].Value + ",";
            }
        }
        if (strCardType != "")
        {
            strCardType = strCardType.Remove(strCardType.LastIndexOf(","), 1);
            pdo.var19 = strCardType;
        }
        else
        {
            pdo.var19 = "";
        }

        pdo.var20 = txtActivityNO.Text.Trim();
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data.Rows.Count > 0)
        {
            if (str == "")
            {
                lblPhoneNum.Text = "1.满足条件的电话记录有 " + (Convert.ToInt32(data.Rows[0][0].ToString()) + Convert.ToInt32(data.Rows[1][0].ToString())).ToString() + " 条。";
                lblCardNum.Text = "2.满足条件的卡片记录有 " + (Convert.ToInt32(data.Rows[0][1].ToString()) + Convert.ToInt32(data.Rows[1][1].ToString())).ToString() + " 条。";
                txtSendMaxNum.Text = (Convert.ToInt32(data.Rows[0][0].ToString()) + Convert.ToInt32(data.Rows[1][0].ToString())).ToString();
                txtSendNum.Text = (Convert.ToInt32(data.Rows[0][0].ToString()) + Convert.ToInt32(data.Rows[1][0].ToString())).ToString();
            }
            else
            {
                lblPhoneNum.Text = "1.满足条件的电话记录有 " + data.Rows[0][0].ToString() + " 条。";
                lblCardNum.Text = "2.满足条件的卡片记录有 " + data.Rows[0][1].ToString() + " 条。";
                txtSendMaxNum.Text = data.Rows[0][0].ToString();
                txtSendNum.Text = data.Rows[0][0].ToString();
            }
            lblCommonMsg.Text = "因为数据会动态变化，查询结果仅供参考。";
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!SubmitValidInput())
            return;

        context.SPOpen();
        context.AddField("P_FROMAGE").Value = txtFromAge.Text.Trim();
        context.AddField("P_TOAGE").Value = txtToAge.Text.Trim();
        context.AddField("P_SEX").Value = selSex.SelectedValue;
        context.AddField("P_BIRTHDATE").Value = birthDate.Text.Trim();
        if (txtAddress.Text.Trim() != "")
        {
            context.AddField("P_ADDRESS").Value = "%" + txtAddress.Text.Trim() + "%";
        }
        else
        {
            context.AddField("P_ADDRESS").Value = "";
        }

        if (txtMinElecMoney.Text.Trim() != "")
        {
            context.AddField("P_MINELECMONEY").Value = Convert.ToInt32(Convert.ToDecimal(txtMinElecMoney.Text.Trim()) * 100);
        }
        else
        {
            context.AddField("P_MINELECMONEY").Value = "";
        }

        if (txtMaxElecMoney.Text.Trim() != "")
        {
            context.AddField("P_MAXELECMONEY").Value = Convert.ToInt32(Convert.ToDecimal(txtMaxElecMoney.Text.Trim()) * 100);
        }
        else
        {
            context.AddField("P_MAXELECMONEY").Value = "";
        }

        if (txtMinTotalConsume.Text.Trim() != "")
        {
            context.AddField("P_MINTOTALCONSUME").Value = Convert.ToInt32(Convert.ToDecimal(txtMinTotalConsume.Text.Trim()) * 100);
        }
        else
        {
            context.AddField("P_MINTOTALCONSUME").Value = "";
        }

        if (txtMaxTotalConsume.Text.Trim() != "")
        {
            context.AddField("P_MAXTOTALCONSUME").Value = Convert.ToInt32(Convert.ToDecimal(txtMaxTotalConsume.Text.Trim()) * 100);
        }
        else
        {
            context.AddField("P_MAXTOTALCONSUME").Value = "";
        }

        context.AddField("P_LASTCONSUMETIME").Value = lastConsumeDate.Text.Trim();
        if (txtBalunit.Text == "")
        {
            context.AddField("P_CONSUMEBALUNIT").Value = "";
        }
        else
        {
            context.AddField("P_CONSUMEBALUNIT").Value = selBalunit.SelectedItem.ToString().Substring(0, 8).Trim();
        }

        context.AddField("P_OPENSTATE").Value = selOpenState.SelectedValue;
        if (selOpenState.SelectedValue == "1")
        {
            context.AddField("P_ACCTYPE").Value = "";
            context.AddField("P_MINACCMONEY").Value = "";
            context.AddField("P_MAXACCMONEY").Value = "";
            context.AddField("P_MINTOTALACCCON").Value = "";
            context.AddField("P_MAXTOTALACCCON").Value = "";
        }
        else
        {
            context.AddField("P_ACCTYPE").Value = selAccType.SelectedValue;
            if (txtMinAccMoney.Text.Trim() != "")
            {
                context.AddField("P_MINACCMONEY").Value = Convert.ToInt32(Convert.ToDecimal(txtMinAccMoney.Text.Trim()) * 100);
            }
            else
            {
                context.AddField("P_MINACCMONEY").Value = "";
            }

            if (txtMaxAccMoney.Text.Trim() != "")
            {
                context.AddField("P_MAXACCMONEY").Value = Convert.ToInt32(Convert.ToDecimal(txtMaxAccMoney.Text.Trim()) * 100);
            }
            else
            {
                context.AddField("P_MAXACCMONEY").Value = "";
            }

            if (txtMinTotalAccCon.Text.Trim() != "")
            {
                context.AddField("P_MINTOTALACCCON").Value = Convert.ToInt32(Convert.ToDecimal(txtMinTotalAccCon.Text.Trim()) * 100);
            }
            else
            {
                context.AddField("P_MINTOTALACCCON").Value = "";
            }

            if (txtMaxTotalAccCon.Text.Trim() != "")
            {
                context.AddField("P_MAXTOTALACCCON").Value = Convert.ToInt32(Convert.ToDecimal(txtMaxTotalAccCon.Text.Trim()) * 100);
            }
            else
            {
                context.AddField("P_MAXTOTALACCCON").Value = "";
            }
        }
        context.AddField("P_OPERATORTYPE").Value = selOperatorType.SelectedValue;
        if (selOperatorType.SelectedValue == "1")//     135、136、137、138、139、150、151、152、158、159、182、183、184
        {
            context.AddField("P_OPERATORPHONE").Value = "134,135,136,137,138,139,147,150,151,152,157,158,159,182,187,188,183,184,178";
        }
        else if (selOperatorType.SelectedValue == "2") //133、153、180、181、189
        {
            context.AddField("P_OPERATORPHONE").Value = "189,133,153,180,133,177,181";
        }
        else if (selOperatorType.SelectedValue == "3") //130、131、132、155、156
        {
            context.AddField("P_OPERATORPHONE").Value = "130,131,132,155,156,185,186,145,176";
        }
        else
        {
            context.AddField("P_OPERATORPHONE").Value = "";
        }

        string strCardType = string.Empty;
        for (int i = 0; i < CheckBoxList1.Items.Count; i++)
        {
            if (CheckBoxList1.Items[i].Selected == true)
            {
                strCardType += CheckBoxList1.Items[i].Value + ",";
            }
        }
        if (strCardType != "")
        {
            strCardType = strCardType.Remove(strCardType.LastIndexOf(","), 1);
            context.AddField("P_CARDTYPE").Value = strCardType;
        }
        else
        {
            context.AddField("P_CARDTYPE").Value = "";
        }

        context.AddField("P_ACTIVITYNO").Value = txtActivityNO.Text.Trim();
        context.AddField("P_ACTIVITYTHEME").Value = txtActivityTheme.Text.Trim();
        context.AddField("P_NOTETYPE").Value = "A";
        context.AddField("P_NOTEMSG").Value = txtNoteMsg.Text.Trim();
        if (selSendType.SelectedValue != "")
        {
            context.AddField("P_SENDTYPE").Value = selSendType.SelectedValue;
        }
        else
        {
            context.AddField("P_SENDTYPE").Value = "0";
        }
       
        context.AddField("P_SENDNUM").Value = txtSendNum.Text.Trim();
        context.AddField("P_SENDTIME").Value = "";

        bool ok = context.ExecuteSP("SP_MS_NOTESENDADD");
        if (ok)
        {
            AddMessage("提交成功");
        }
    }

    protected void btnQueryA_Click(object sender, EventArgs e)
    {
        if (!dateValidation())
            return;
        //查询员工信息
        lvwActivity.DataSource = CreateStaffQueryDataSource();
        lvwActivity.DataBind();
    }

    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        return !(context.hasError());
    }

    public ICollection CreateStaffQueryDataSource()
    {
        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        pdo.funcCode = "QueryActivityInfo";
        pdo.var1 = beginDate.Text.Trim().ToString();
        pdo.var2 = endDate.Text.Trim().ToString();
        pdo.var3 = "1";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count < 1)
        {
            lvwActivity.DataSource = new DataTable();
            lvwActivity.DataBind();
            context.AddMessage("未查出活动信息记录");
        }
        return new DataView(data);
    }

    public void lvwActivity_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwActivity.PageIndex = e.NewPageIndex;
        btnQueryA_Click(sender, e);
    }

    protected void lvwActivity_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwActivity','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void lvwActivity_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtActivityNO.Text = getDataKeys2("ACTIVITYNO");
        txtActivityTheme.Text = getDataKeys2("ACTIVITYTHEME");
        txtNoteMsg.Text = getDataKeys2("NOTEMSG");
    }

    public String getDataKeys2(String keysname)
    {
        return lvwActivity.DataKeys[lvwActivity.SelectedIndex][keysname].ToString();
    }

    protected void txtBalunit_Changed(object sender, EventArgs e)
    {
        //模糊结算单元名称，并在列表中赋值
        string name = txtBalunit.Text.Trim();
        DataTable table = new DataTable();
        if (name == "")
        {
            selBalunit.Items.Clear();
        }
        else
        {
            context.DBOpen("Select");
            string sql;

            sql = @"SELECT DISTINCT BALUNITNO,BALUNITNO||':'||BALUNIT BALUNIT  FROM TF_TRADE_BALUNIT WHERE USETAG = '1' AND (BALUNIT LIKE '%" + name + "%' OR BALUNITNO LIKE '%" + name + "%') AND ROWNUM<=100 ORDER BY BALUNITNO";

            table = context.ExecuteReader(sql);
        }
        if (table.Rows.Count > 0)
        {
            selBalunit.DataSource = table;
            selBalunit.DataTextField = "BALUNIT";
            selBalunit.DataBind();
            selBalunit.Items.Insert(0, new ListItem("---请选择---", ""));
        }
    }

    protected void selBalunit_Changed(object sender, EventArgs e)
    {
        string balunit = selBalunit.SelectedItem.ToString();
        txtBalunit.Text = balunit.Substring(9, balunit.Length - 9);
    }

    protected void selSendType_Changed(object sender, EventArgs e)
    {
        if (selSendType.SelectedValue != "")
        {
            txtSendNum.ReadOnly = false;
        }
        else
        {
            txtSendNum.ReadOnly = true;
            txtSendNum.Text = txtSendMaxNum.Text;
        }
    }

    private Boolean SubmitValidInput()
    {
        //查询结果：满足条件的电话记录为0条，不能提交任务
        if (Convert.ToInt32(txtSendMaxNum.Text.Trim()) <= 0)
        {
            context.AddError("满足条件的电话记录为0条，不能提交任务");
            return false;
        }

        //短信发送方式
        if (selSendType.SelectedValue != "")
        {
            //短信发送数目
            String strSendNum = txtSendNum.Text.Trim();
            if (strSendNum == "")
            {
                context.AddError("请输入发送短信数目", txtSendNum);
                return false;
            }
            else
            {
                if (!Validation.isNum(txtSendNum.Text.Trim()))
                {
                    context.AddError("发送短信数目必须为数字", txtSendNum);
                    return false;
                }
                else if (Convert.ToInt32(txtSendNum.Text.Trim()) > Convert.ToInt32(txtSendMaxNum.Text.Trim()))
                {
                    context.AddError("发送短信数不能大于查询的电话记录数", txtSendNum);
                    return false;
                }
            }
        }
        else
        {
            if (Convert.ToInt32(txtSendNum.Text.Trim()) != Convert.ToInt32(txtSendMaxNum.Text.Trim()))
            {
                context.AddError("未选择发送方式，发送短信数须等于查询的电话记录数", txtSendNum);
                return false;
            }
        }

        //判断活动编码是否为空白
        if (lvwActivity.SelectedIndex < 0)
        {
            context.AddError("没有选择可处理的记录", txtActivityNO);
            return false;
        }
        //判断该活动编码是否是选中的活动编码
        if (txtActivityNO.Text.Trim() != getDataKeys2("ACTIVITYNO"))
        {
            context.AddError("活动编号与选择的记录编号不一致", txtActivityNO);
            return false;
        }
        //判断该活动主题是否是选中的活动主题
        if (txtActivityTheme.Text.Trim() != getDataKeys2("ACTIVITYTHEME"))
        {
            context.AddError("活动主题与选择的记录主题不一致", txtActivityTheme);
            return false;
        }
     
        if (context.hasError())
            return false;
        else
            return true;
    }

    private Boolean QueryValidInput()
    {
        if (txtMinElecMoney.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMinElecMoney.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtActivityNO);
                return false; 
            }
        }

        if (txtMaxElecMoney.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMaxElecMoney.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMaxElecMoney);
                return false; 
            }
        }

        if (txtMinElecMoney.Text.Trim() != "" && txtMaxElecMoney.Text.Trim() != "")
        {
            if (Convert.ToInt32(txtMinElecMoney.Text.Trim()) > Convert.ToInt32(txtMaxElecMoney.Text.Trim()))
            {
                context.AddError("最小库内账户余额不能大于最大库内账户余额", txtMinElecMoney);
                return false; 
            }
        }

        if (txtMinTotalConsume.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMinTotalConsume.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMinTotalConsume);
                return false; 
            }
        }

        if (txtMaxTotalConsume.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMaxTotalConsume.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMaxTotalConsume);
                return false; 
            }
        }

        if (txtMinTotalConsume.Text.Trim() != "" && txtMaxTotalConsume.Text.Trim() != "")
        {
            if (Convert.ToInt32(txtMinTotalConsume.Text.Trim()) > Convert.ToInt32(txtMaxTotalConsume.Text.Trim()))
            {
                context.AddError("最小钱包消费总金额不能大于最大钱包消费总金额", txtMinTotalConsume);
                return false; 
            }
        }

        if (txtMinAccMoney.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMinAccMoney.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMinAccMoney);
                return false; 
            }
        }

        if (txtMaxAccMoney.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMaxAccMoney.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMaxAccMoney);
                return false; 
            }
        }

        if (txtMinAccMoney.Text.Trim() != "" && txtMaxAccMoney.Text.Trim() != "")
        {
            if (Convert.ToInt32(txtMinAccMoney.Text.Trim()) > Convert.ToInt32(txtMaxAccMoney.Text.Trim()))
            {
                context.AddError("最小账户宝余额不能大于最大账户宝余额", txtMinAccMoney);
                return false; 
            }
        }

        if (txtMinTotalAccCon.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMinTotalAccCon.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMinTotalAccCon);
                return false; 
            }
        }

        if (txtMaxTotalAccCon.Text.Trim() != "")
        {
            if (!Validation.isNum(txtMaxTotalAccCon.Text.Trim()))
            {
                context.AddError("金额必须是数字", txtMaxTotalAccCon);
                return false; 
            }
        }

        if (txtMinTotalAccCon.Text.Trim() != "" && txtMaxTotalAccCon.Text.Trim() != "")
        {
            if (Convert.ToInt32(txtMinTotalAccCon.Text.Trim()) > Convert.ToInt32(txtMaxTotalAccCon.Text.Trim()))
            {
                context.AddError("最小账户宝消费总额不能大于最大账户宝消费总额", txtMinTotalAccCon);
                return false; 
            }
        }

        if (lastConsumeDate.Text.Trim() != "")
        {
            if (!Validation.isNum(lastConsumeDate.Text.Trim()))
            {
                context.AddError("消费日期必须是数字", lastConsumeDate);
                return false; 
            }
            if (Validation.strLen(lastConsumeDate.Text.Trim()) != 8)
            {
                context.AddError("消费日期必须为8位日期格式", lastConsumeDate);
                return false; 
            }
        }

        if (birthDate.Text.Trim() != "")
        {
            if (!Validation.isNum(birthDate.Text.Trim()))
            {
                context.AddError("出生日期必须是数字", birthDate);
                return false; 
            }
            if (Validation.strLen(birthDate.Text.Trim()) != 8)
            {
                context.AddError("出生日期必须为8位日期格式", birthDate);
                return false; 
            }
        }

        if (txtFromAge.Text.Trim() != "")
        {
            if (!Validation.isNum(txtFromAge.Text.Trim()))
            {
                context.AddError("年龄必须是数字", txtFromAge);
                return false; 
            }
        }

        if (txtToAge.Text.Trim() != "")
        {
            if (!Validation.isNum(txtToAge.Text.Trim()))
            {
                context.AddError("年龄必须是数字", txtToAge);
                return false; 
            }
        }

        if (context.hasError())
            return false;
        else
            return true;
    }
}
