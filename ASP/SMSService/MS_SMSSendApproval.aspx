﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MS_SMSSendApproval.aspx.cs"
    Inherits="ASP_SMSService_MS_SMSSendApproval" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>短信业务-短信发送审核</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="tb">
                短信业务-&gt;短信发送审核</div>
            <!-- #include file="../../ErrorMsg.inc" -->
            <div class="con">
                <div class="pip">
                    查询</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    开始日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="beginDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="beginDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    结束日期:</div>
                            </td>
                            <td width="20%">
                                <asp:TextBox ID="endDate" CssClass="input" runat="server" MaxLength="10" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="endDate"
                                    Format="yyyyMMdd" />
                            </td>
                            <td width="9%">
                                <div align="right">
                                    状态:</div>
                            </td>
                            <td width="15%">
                                <asp:DropDownList ID="selCheckState" runat="server" CssClass="inputmid">
                                    <asp:ListItem Value="1" Selected="True">1:待审核</asp:ListItem>
                                    <asp:ListItem Value="2">2:审核通过</asp:ListItem>
                                    <asp:ListItem Value="3">3:审核作废</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="10%">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                                <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" Width="78px"
                                    OnClick="btnQuery_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="kuang5">
                    <div class="gdtb" style="height: 300px">
                        <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1">
                            <asp:GridView ID="lvwActivity" runat="server" Width="100%" CssClass="tab1" AllowPaging="true"
                                PageSize="20" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                OnRowDataBound="lvwActivity_RowDataBound" SelectedRowStyle-CssClass="tabsel"
                                PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="False" OnPageIndexChanging="lvwActivity_Page" OnRowCreated="lvwActivity_RowCreated"
                                OnSelectedIndexChanged="lvwActivity_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ItemCheckBox" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="编号" DataField="ID" />
                                    <asp:BoundField HeaderText="主题" DataField="ACTIVITYTHEME" />
                                    <asp:BoundField HeaderText="发送人员数" DataField="sendPersonNum" />
                                    <asp:BoundField HeaderText="短信总条数" DataField="SENDNUM" />
                                    <asp:BoundField HeaderText="登记时间" DataField="OPERATETIME" />
                                    <asp:BoundField HeaderText="登记人" DataField="OPERATESTAFFNO" />
                                    <asp:BoundField HeaderText="审核时间" DataField="CHECKTIME" />
                                    <asp:BoundField HeaderText="审核人" DataField="CHECKSTAFFNO" />
                                    <asp:BoundField HeaderText="类型" DataField="ACTIVITYTYPE" />
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                        <tr class="tabbt">
                                            <td>
                                                <input type="checkbox" />
                                            </td>
                                            <td>
                                                编号
                                            </td>
                                            <td>
                                                主题
                                            </td>
                                            <td>
                                                发送人员数
                                            </td>
                                            <td>
                                                短信总条数
                                            </td>
                                            <td>
                                                登记时间
                                            </td>
                                            <td>
                                                登记人
                                            </td>
                                            <td>
                                                审核时间
                                            </td>
                                            <td>
                                                审核人
                                            </td>
                                            <td>
                                                类型
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </table>
                    </div>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    发送时间:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:Label ID="lblSendTime" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    排队短信:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:Label ID="lblWaitSMSNum" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动编号:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityNO" runat="server" CssClass="inputmid" MaxLength="12"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    活动主题:</div>
                            </td>
                            <td style="height: 30px">
                                <asp:TextBox ID="txtActivityTheme" runat="server" CssClass="inputmid" MaxLength="20"
                                    ReadOnly="true" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    活动短信内容:</div>
                            </td>
                            <td style="height: 50px">
                                <asp:TextBox ID="txtNoteMsg" runat="server" CssClass="inputmid" MaxLength="250" Width="500px"
                                    ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                </div>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkyx" runat="server" Text="优先发送" />
                            </td>
                            <td colspan="4">
                                <table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnPass" runat="server" Text="通过" CssClass="button1" OnClick="btnPass_Click"
                                                Width="78px" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="作废" CssClass="button1" OnClick="btnCancel_Click"
                                                Width="78px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
