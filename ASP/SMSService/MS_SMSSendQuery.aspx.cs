﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using PDO.SMSService;
using Master;

public partial class ASP_SMSService_MS_SMSSendQuery : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            initLoad(sender, e);
            UserCardHelper.resetData(lvwActivity, null);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        beginDate.Text = DateTime.Today.AddDays(-7).ToString("yyyyMMdd");
        endDate.Text = DateTime.Today.ToString("yyyyMMdd");
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!dateValidation())
            return;

        if (selProjectType.SelectedValue == "")
        {
            context.AddError("请选择查询类型");
        }
        if (context.hasError())
            return ;
    
        //查询员工信息
        lvwActivity.DataSource = CreateQueryDataSource();
        lvwActivity.DataBind();
    }

    private Boolean dateValidation()
    {
        beginDate.Text = beginDate.Text.Trim();
        endDate.Text = endDate.Text.Trim();
        UserCardHelper.validateDateRange(context, beginDate, endDate, true);

        return !(context.hasError());
    }

    public ICollection CreateQueryDataSource()
    {
        SP_MS_QueryPDO pdo = new SP_MS_QueryPDO();
        if (chkFlag.Checked == false)
        {
            pdo.funcCode = "QueryNoteSendStatistics";
            pdo.var1 = beginDate.Text.Trim().ToString();
            pdo.var2 = endDate.Text.Trim().ToString();
            pdo.var3 = selProject.SelectedValue;
            StoreProScene storePro = new StoreProScene();
            DataTable data = storePro.Execute(context, pdo);
            if (data == null || data.Rows.Count < 1)
            {
                lvwActivity.DataSource = new DataTable();
                lvwActivity.DataBind();
                context.AddError("查询结果为空");
            }
            return new DataView(data);
        }
        else
        {
            if (selProject.SelectedValue == "企福通到账提醒")
            {
                pdo.funcCode = "QueryNoteSendStatistics3";
            }
            else
            {
                pdo.funcCode = "QueryNoteSendStatistics2";
            }
            pdo.var1 = beginDate.Text.Trim().ToString();
            pdo.var2 = endDate.Text.Trim().ToString();
            pdo.var3 = selCountType.SelectedValue;
            StoreProScene storePro = new StoreProScene();
            DataTable data = storePro.Execute(context, pdo);
            if (data == null || data.Rows.Count < 1)
            {
                lvwActivity.DataSource = new DataTable();
                lvwActivity.DataBind();
                context.AddError("查询结果为空");
            }
            return new DataView(data);
        }
    }

    public void lvwActivity_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwActivity.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void selProjectType_Changed(object sender, EventArgs e)
    {
        if (selProjectType.SelectedValue == "0")
        {
            DataTable table = new DataTable();
            context.DBOpen("Select");
            string sql = @"SELECT DISTINCT ACTIVITYNO, ACTIVITYTHEME  FROM TF_F_ACTIVITYINFO WHERE USETAG = '1' ORDER BY ACTIVITYNO desc";
            table = context.ExecuteReader(sql);
            if (table.Rows.Count > 0)
            {
                selProject.DataSource = table;
                selProject.DataTextField = "ACTIVITYTHEME";
                selProject.DataValueField = "ACTIVITYTHEME";
                selProject.DataBind();
                selProject.Items.Insert(0, new ListItem("---请选择---", ""));
            }
        }
        else
        {
            selProject.Items.Clear();
            selProject.Items.Insert(0, new ListItem("企福通到账提醒", "企福通到账提醒"));
        }
    }
}
