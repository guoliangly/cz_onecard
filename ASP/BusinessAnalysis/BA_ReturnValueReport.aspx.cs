﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using PDO.Financial;
using Master;
using TM;

public partial class ASP_BusinessAnalysis_BA_ReturnValueReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            labTitle.Text = "退值统计";

            DateTime tf = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1);
            DateTime tt = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month));

            txtFromDate.Text = tf.ToString("yyyyMMdd");
            txtToDate.Text = tt.ToString("yyyyMMdd");
        }
    }
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);
        validate();
        if (context.hasError()) return;

        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = "ReturnValueReport";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = "";
        pdo.var4 = "";
        pdo.var5 = selDateType.SelectedValue;

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);


        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
            btnPrint.Enabled = false;
        }
        else
        {
            
            if (selCardType.SelectedValue != "")
            {
                DataTable datatemp = data.Clone();
                DataRow[] drs = data.Select("卡类型='" + selCardType.Items[selCardType.SelectedIndex].Text + "'");
                foreach (DataRow dr in drs)
                {
                    datatemp.ImportRow(dr);
                }
                if (datatemp == null || datatemp.Rows.Count == 0)
                {
                    AddMessage("N005030001: 查询结果为空");
                    btnPrint.Enabled = false;
                }
                else
                {

                    //增加合计行
                    ArrayList al2 = new ArrayList();
                    al2.Add("合计");
                    al2.Add("");
                    for (int j = 2; j < datatemp.Columns.Count; j++)
                    {
                        decimal hejitotalmoney = 0;
                        for (int i = 1; i < datatemp.Rows.Count; i++)
                        {
                            hejitotalmoney += Convert.ToDecimal(datatemp.Rows[i][j].ToString());
                        }
                        al2.Add(hejitotalmoney.ToString("0.00"));
                    }
                    datatemp.Rows.Add(al2.ToArray());
                    UserCardHelper.resetData(gvResult, datatemp);
                    btnPrint.Enabled = true;
                }
            }
            else
            {
                UserCardHelper.resetData(gvResult, data);
                btnPrint.Enabled = true;
            }
           
            
        }
           
        
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    private Array intArray = null;
    private Array doubleArray = null;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      
    }
    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 1);
    }
    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
    }
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }
   
}
