﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BA_ReturnValueReport.aspx.cs" Inherits="ASP_BusinessAnalysis_BA_ReturnValueReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>退值统计</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <style>
    th{ text-align:center;font-weight:bold;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
        <div class="tb">
		    经营分析->退值统计
	    </div>
<ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" AsyncPostBackTimeout="600"
            EnableScriptLocalization="true" ID="ScriptManager2" />
          <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script> 
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->  
	        <div class="con">

           <div class="card">查询</div>
           <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td><div align="right">卡类型:</div></td>
                        <td>
                          <asp:DropDownList ID="selCardType"  CssClass="inputmid" runat="server">
                            <asp:ListItem Value="">--请选择--</asp:ListItem>
                            <asp:ListItem Value="0">龙城通普卡</asp:ListItem>
                            <asp:ListItem Value="1">学生卡</asp:ListItem>
                            <asp:ListItem Value="2">老人卡</asp:ListItem>
                            <asp:ListItem Value="3">异型卡</asp:ListItem>
                            <asp:ListItem Value="4">市民卡</asp:ListItem>
                            <asp:ListItem Value="5">移动龙城通卡</asp:ListItem>
                            <asp:ListItem Value="6">天翼龙城通卡</asp:ListItem>
                          </asp:DropDownList>
                        </td>
                        <td><div align="right">日期类型:</div></td>
                        <td colspan="3">
                         <asp:DropDownList ID="selDateType"  CssClass="inputmid" runat="server">
                            <asp:ListItem Value="0">按日</asp:ListItem>
                            <asp:ListItem Value="1">按月</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                   </tr>
                    <tr>
                        <td><div align="right">开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">结束日期:</div></td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                        </td>
                        <td align="right">
                            <asp:Button ID="Button2" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
               </table>
               
             </div>
	
	        <asp:HiddenField ID="hidNo" runat="server" Value="" />
	
            <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                        <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click" />
                        <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="return printGridView('printarea');"/>
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:380px;">
                    <table id="printReport" width ="95%">
                        <tr align="center">
                            <td style ="font-size:16px;font-weight:bold"><asp:Label ID="labTitle" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <table width="300px" align="left">
                                    <tr align="left">
                                        <td></td>
                                    </tr>
                                </table>
                            
                                <table width="300px" align="right">
                                    <tr align="right">
                                        <td>开始日期：<%=txtFromDate.Text%></td>
                                        <td>结束日期：<%=txtToDate.Text%></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                  <asp:GridView ID="gvResult" runat="server"
                        Width = "98%"
                        CssClass="tab2"
                        HeaderStyle-CssClass="tabbt"
                        AlternatingRowStyle-CssClass="tabjg"
                        SelectedRowStyle-CssClass="tabsel"

                        PagerSettings-Mode="NumericFirstLast"
                        PagerStyle-HorizontalAlign="center"
                        PagerStyle-VerticalAlign="Top"
                        AutoGenerateColumns="false"
                        OnRowCreated="gvResult_RowCreated"
                         OnPreRender="gvResult_PreRender"
                        >       

                          <Columns>
                <asp:BoundField HeaderText="日期" DataField="日期"/>
                <asp:BoundField HeaderText="卡类型" DataField="卡类型"/>
                <asp:BoundField HeaderText="退卡" DataField="退卡"/>
                <asp:BoundField HeaderText="销户" DataField="销户"/>
                <asp:BoundField HeaderText="挂失卡销户" DataField="挂失卡销户"/>
                <asp:BoundField HeaderText="合计" DataField="合计"/>
           </Columns>           
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>日期</td>
                    <td>卡类型</td>
                    <td>退卡</td>
                    <td>销户</td>
                    <td>挂失卡销户</td>
                    <td>合计</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
                    </asp:GridView>
                </div>
              </div>
            </div>
      
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
              </Triggers>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
