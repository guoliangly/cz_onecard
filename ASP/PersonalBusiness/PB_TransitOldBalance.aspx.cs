﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using TDO.BusinessCode;
using TDO.CardManager;
using PDO.PersonalBusiness;
using TDO.PersonalTrade;
using TDO.UserManager;
using Master;

public partial class ASP_PersonalBusiness_PB_TransitOldBalance : Master.FrontMaster
{
    private static int par_ChangeSpan = 7;       //换卡转值时长限制
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LabAsn.Attributes["readonly"] = "true";
            LabCardtype.Attributes["readonly"] = "true";
            sDate.Attributes["readonly"] = "true";
            eDate.Attributes["readonly"] = "true";
            cMoney.Attributes["readonly"] = "true";

            if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";

            //设置GridView绑定的DataTable
            lvwTransitQuery.DataSource = new DataTable();
            lvwTransitQuery.DataBind();
            lvwTransitQuery.SelectedIndex = -1;

            //指定GridView DataKeyNames
            lvwTransitQuery.DataKeyNames = new string[] { "OLDCARDNO", "CARDNO", "OPERATETIME", "CARDACCMONEY","TFLAG","TRADEID" };

            //创建转值的临时表
            //createTempTable();

            initLoad(sender, e);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();


        //从系统参数表中读取换卡转值时长限制
        TD_M_TAGTDO ddoTD_M_TAGTDOIn = new TD_M_TAGTDO();
        ddoTD_M_TAGTDOIn.TAGCODE = "PB_TBLOSS_SPAN";//取得是挂失卡的时长限制

        TD_M_TAGTDO[] ddoTD_M_TAGTDOOutArr = (TD_M_TAGTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TAGTDOIn, "S001002125");

        if (ddoTD_M_TAGTDOOutArr[0].TAGVALUE != null)
        {
            par_ChangeSpan = Convert.ToInt16(ddoTD_M_TAGTDOOutArr[0].TAGVALUE);
        }



        //从前台业务交易费用表中读取数据
        TD_M_TRADEFEETDO ddoTD_M_TRADEFEETDOIn = new TD_M_TRADEFEETDO();
        ddoTD_M_TRADEFEETDOIn.TRADETYPECODE = "04";

        TD_M_TRADEFEETDO[] ddoTD_M_TRADEFEEOutArr = (TD_M_TRADEFEETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TRADEFEETDOIn, typeof(TD_M_TRADEFEETDO), "S001002125", "TD_M_TRADEFEE", null);

        for (int i = 0; i < ddoTD_M_TRADEFEEOutArr.Length; i++)
        {
            if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "03")
                ProcedureFee.Text = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "99")
                OtherFee.Text = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
        }
        Total.Text = (Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");

        TransitBalance.Text = "0.00";
        Transit.Enabled = false;
    }

    public void lvwTransitQuery_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwTransitQuery.PageIndex = e.NewPageIndex;
        lvwTransitQuery.DataSource = TransithistoryOld();
        lvwTransitQuery.DataBind();
    }


    protected void lvwTransitQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            //隐藏记录流水和标志位
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[5].Visible = false;
        }

    }


    //查询老公交卡换旧卡未转值记录
    public DataTable TransithistoryOld()
    {
        Decimal TransitMoney = 0;
        TMTableModule tmTMTableModule = new TMTableModule();
        TF_B_TRADETDO ddoTF_B_TRADEIn = new TF_B_TRADETDO();
        DataRow relation;
        int i = 1;

        //查询以往换卡记录
        string strChangeOld = "SELECT a.TRADEID, a.CARDNO,a.OLDCARDNO,a.OPERATETIME,b.CARDACCMONEY/100.0 CARDACCMONEY, '1' TFLAG ";
        strChangeOld += " FROM TF_B_TRADE a,TF_F_CARDEWALLETACC b WHERE a.CARDNO = '" + txtCardno.Text + "' AND " +
                        " a.TRADETYPECODE IN ('3F') AND b.CARDNO = a.OLDCARDNO AND a.CANCELTAG = '0' AND NOT EXISTS( SELECT 1 FROM TF_B_TRADE WHERE OLDCARDNO = a.OLDCARDNO AND TRADETYPECODE  IN ('04','4L') AND CANCELTAG = '0') ";
        DataTable dataChangeOld = tmTMTableModule.selByPKDataTable(context, ddoTF_B_TRADEIn, null, strChangeOld, 0);

        if (dataChangeOld.Rows.Count != 0)
        {
            //查询转值记录
            foreach (DataRow dr in dataChangeOld.Rows)
            {
                //换卡时间超过7天
                if (Convert.ToDateTime(dr[3].ToString()).AddDays(par_ChangeSpan) <= DateTime.Now)
                {
                    //更新卡号
                    newCardNo.Value = dr[2].ToString();
                    //累加金额
                    TransitMoney = TransitMoney + Convert.ToDecimal(dr[4].ToString());

                }
                //换卡时间不足7天
                else if (Convert.ToDateTime(dr[3].ToString()).AddDays(par_ChangeSpan) > DateTime.Now)
                {
                    context.AddError("A001005122");
                    TransitMoney = 0;
                    dataChangeOld.Rows.Clear();
                    break;
                }
            }
        }
        else
        {
            context.AddError("A001005120");
        }
        TransitBalance.Text = TransitMoney.ToString("0.00");
        return dataChangeOld;
    }


    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        Transit.Enabled = false;
        TMTableModule tmTMTableModule = new TMTableModule();

        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {

            //查询开通功能

            PBHelper.openFunc(context, openFunc, txtCardno.Text);
            //如果旧卡不存在，则查询是否有老公交卡换旧卡的记录
            lvwTransitQuery.DataSource = TransithistoryOld();
            lvwTransitQuery.DataBind();



            if (lvwTransitQuery.Rows.Count <= 0) return;

            if (context.hasError()) return;

            Transit.Enabled = true;
            btnPrintPZ.Enabled = false;
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            Transit.Enabled = false;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            SP_PB_updateCardTradePDO pdo = new SP_PB_updateCardTradePDO();
            pdo.CARDTRADENO = hiddentradeno.Value;
            pdo.TRADEID = hidoutTradeid.Value;

            bool ok = TMStorePModule.Excute(context, pdo);

            if (ok)
            {
                AddMessage("前台写卡成功");
                clearCustInfo(txtCardno);
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        hidWarning.Value = "";
    }

    private void RecordIntoTmp()
    {
        //记录入临时表
        context.DBOpen("Insert");
        int seq = 0;
        foreach (GridViewRow gvr in lvwTransitQuery.Rows)
        {
            if (gvr.Cells[4].Text == "1")
            {
                context.ExecuteNonQuery("insert into TMP_PB_TransitBalance values('" + Session.SessionID + "'," + (seq++) + ",'" + gvr.Cells[5].Text + "')");
            }
        }
        context.DBCommit();

    }

    private void clearTempTable()
    {
        //清空临时表
        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_PB_TransitBalance where SESSIONID='" + Session.SessionID + "'");
        context.DBCommit();
    }

    protected void Transit_Click(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //库内余额为负时,不能进行转值
        if (Convert.ToDecimal(TransitBalance.Text) < 0)
        {
            context.AddError("A001005123");
            return;
        }
        //清空临时表数据
        clearTempTable();

        //向临时表中插入数据
        RecordIntoTmp();

        //存储过程赋值

        SP_PB_TransitOldBalancePDO pdo = new SP_PB_TransitOldBalancePDO();
        pdo.SESSIONID = Session.SessionID;
        pdo.NEWCARDNO = txtCardno.Text;
        pdo.TRADETYPECODE = "04";
        pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(TransitBalance.Text) * 100);
        pdo.PREMONEY = Convert.ToInt32(hiddencMoney.Value);
        pdo.ASN = LabAsn.Text;
        pdo.CARDTRADENO = hiddentradeno.Value;
        pdo.CARDTYPECODE = hiddenLabCardtype.Value;
        pdo.CHANGERECORD = ChangeRecord.Value;
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;

       


        hidSupplyMoney.Value = "" + pdo.CURRENTMONEY;
        PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            hidoutTradeid.Value = "" + ((SP_PB_TransitOldBalancePDO)pdoOut).TRADEID;
            AddMessage("M001005001");
            //写卡
            hidCardReaderToken.Value = cardReader.createToken(context); 
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                "chargeCard();", true);
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, hidCustname.Value, "转值", "0.00"
                , "", "", hidPaperno.Value, (Convert.ToDecimal(pdo.CURRENTMONEY+pdo.PREMONEY) / (Convert.ToDecimal(100))).ToString("0.00"),
                "", (Convert.ToDecimal(pdo.CURRENTMONEY) / (Convert.ToDecimal(100))).ToString("0.00"), context.s_UserName, context.s_DepartName,
                hidPapertype.Value, OtherFee.Text, "0.00");
            ChangeRecord.Value = "0";
            //重新初始化
            initLoad(sender, e);
        }
    }
}
