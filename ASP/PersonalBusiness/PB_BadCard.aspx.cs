﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.PersonalBusiness;

public partial class ASP_PersonalBusiness_PB_BadCard : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //坏卡登记判断
    private Boolean badCardbookinValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001004113", txtCardno);
        else
        {
            if (Validation.strLen(txtCardno.Text.Trim()) != 16 && Validation.strLen(txtCardno.Text.Trim()) != 8)
                context.AddError("A001004114", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001004115", txtCardno);
        }

        return !(context.hasError());
    }
    protected void btnBadCard_Click(object sender, EventArgs e)
    {
        //因卡片制作过程中出错，导致卡面显示卡号异常,增加提示
        string expstr = CommonHelper.IsExpCardno(txtCardno.Text.Trim());
        if (expstr != "")
        {
            context.AddError(expstr);
            return;
        }
        if (txtCardno.Text.Trim().Substring(0, 8) == "91501103")
        {
            context.AddError("此卡为江南农商行龙城通卡，不能坏卡登记");
            return;
        }

        //对输入卡号进行检验
        if (!badCardbookinValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();

        SP_PB_BadCardPDO pdo = new SP_PB_BadCardPDO();

        //存储过程赋值
        pdo.CARDNO = txtCardno.Text;
        pdo.BadType = selBadType.SelectedValue;
        pdo.Remark = txtRemark.Text.Trim();
        pdo.TRADETYPECODE = "93";

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("M001001003");
            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
        }
    }
}
