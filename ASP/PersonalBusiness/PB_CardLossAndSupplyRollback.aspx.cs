﻿using System;
using System.Data;
using System.Web.UI;
using Common;
using PDO.PersonalBusiness;

using TDO.BusinessCode;
using TDO.CardManager;
using TDO.CustomerAcc;
using TDO.PersonalTrade;
using TM;

public partial class ASP_PersonalBusiness_PB_CardLossAndSupplyRollback : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            setReadOnly(OsDate, ODeposit, OldcMoney, ReasonType, LabCardtype, NsDate, NewcMoney, NDeposit);
            if (!context.s_Debugging) txtNCardno.Attributes["readonly"] = "true";

            InitLoad(sender, e);

            gvAccount.DataSource = null;
            gvAccount.DataBind();
        }
    }

    protected void InitLoad(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        //从前台业务交易费用表中读取售卡费用数据

        TD_M_TRADEFEETDO ddoTdMTradefeetdoIn = new TD_M_TRADEFEETDO();
        ddoTdMTradefeetdoIn.TRADETYPECODE = "A3";

        TD_M_TRADEFEETDO[] ddoTdMTradefeeOutArr = (TD_M_TRADEFEETDO[])tmTmTableModule.selByPKArr(context, ddoTdMTradefeetdoIn, typeof(TD_M_TRADEFEETDO), "S001004139", "TD_M_TRADEFEE", null);

        for (int i = 0; i < ddoTdMTradefeeOutArr.Length; i++)
        {
            //费用类型为押金

            if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "00")
            {
                hiddenDepositFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
                hiddenDepositBalanceFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
            }

            //费用类型为卡费

            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "10")
                hiddenCardcostFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");

            //费用类型为手续费
            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "03")
                hidProcedureFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");

            //费用类型为其他费用

            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "99")
                hidOtherFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
        }

        DepositFee.Text = hiddenDepositFee.Value;
        DepositBalanceFee.Text = hiddenDepositBalanceFee.Value;
        CardcostFee.Text = hiddenCardcostFee.Value;
        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(DepositBalanceFee.Text) + Convert.ToDecimal(CardcostFee.Text)
            + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        ReturnSupply.Text = Total.Text;
    }

    protected void ReadChangeInfo(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        //从IC卡资料表中读取数据

        TF_F_CARDRECTDO ddoTfFCardrecIn = new TF_F_CARDRECTDO();
        ddoTfFCardrecIn.CARDNO = txtOCardno.Text;
        TF_F_CARDRECTDO ddoTfFCardrecOut = (TF_F_CARDRECTDO)tmTmTableModule.selByPK(context, ddoTfFCardrecIn, typeof(TF_F_CARDRECTDO), null);

        if (ddoTfFCardrecOut == null)
        {
            context.AddError("A001022103");
            return;
        }

        if (txtOCardno.Text.Trim().StartsWith("715009049"))
        {
            ODepositBalance.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / 100).ToString("0.00");
        }
        else
        {
            ODeposit.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / 100).ToString("0.00");
        }

        OsDate.Text = ddoTfFCardrecOut.SERSTARTTIME.ToString("yyyy-MM-dd");

        //从业务台帐主表中读取数据
        TF_B_TRADETDO ddoTfBTradeIn = new TF_B_TRADETDO();
        ddoTfBTradeIn.OLDCARDNO = txtOCardno.Text;
        ddoTfBTradeIn.TRADETYPECODE = "4A";
        TF_B_TRADETDO ddoTfBTradeOut = (TF_B_TRADETDO)tmTmTableModule.selByPK(context, ddoTfBTradeIn, typeof(TF_B_TRADETDO), null, "TF_B_TRADE_ROLLBACK_TIME", null);

        if (ddoTfBTradeOut == null)
        {
            context.AddError("未查出补卡记录");
            return;
        }
        hiddenChangeCardno.Value = ddoTfBTradeOut.CARDNO;
        hiddenCardstate.Value = ddoTfBTradeOut.CARDSTATE;
        hiddenSerstaketag.Value = ddoTfBTradeOut.SERSTAKETAG;
        hiddenTradeid.Value = ddoTfBTradeOut.TRADEID;
        hiddenREASONCODE.Value = ddoTfBTradeOut.REASONCODE;

        //退卡原因
        ReasonType.Text = "挂失补卡";
    }

    private Boolean ReadValidation()
    {
        //对卡号进行非空、长度、数字检验

        if (txtOCardno.Text.Trim() == "")
            context.AddError("A001004113", txtOCardno);
        else
        {
            if ((txtOCardno.Text.Trim()).Length != 16)
                context.AddError("A001004114", txtOCardno);
            else if (!Validation.isNum(txtOCardno.Text.Trim()))
                context.AddError("A001004115", txtOCardno);
        }

        return !(context.hasError());
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        //if (!txtOCardno.Text.Trim().StartsWith("715009"))
        //{
        //    context.AddError("非市民卡B卡不可售卡");
        //    return;
        //}

        if (!ReadValidation())
            return;

        TMTableModule tmTmTableModule = new TMTableModule();

        ReadChangeInfo(sender, e);
        if (context.hasError())
            return;

        //从IC卡电子钱包账户表中读取数据

        TF_F_CARDEWALLETACCTDO ddoTfFCardewalletaccIn = new TF_F_CARDEWALLETACCTDO();
        ddoTfFCardewalletaccIn.CARDNO = txtOCardno.Text;
        TF_F_CARDEWALLETACCTDO ddoTfFCardewalletaccOut = (TF_F_CARDEWALLETACCTDO)tmTmTableModule.selByPK(context, ddoTfFCardewalletaccIn, typeof(TF_F_CARDEWALLETACCTDO), null, "TF_F_CARDEWALLETACC", null);

        if (ddoTfFCardewalletaccOut == null)
        {
            context.AddError("A001022103");
            return;
        }
        OldcMoney.Text = (Convert.ToDecimal(ddoTfFCardewalletaccOut.CARDACCMONEY) / (Convert.ToDecimal(100))).ToString("0.00");

        btnReadNCard.Enabled = true;
        btnPrintPZ.Enabled = false;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnRollback.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            AddMessage("前台写卡成功");
            clearCustInfo(txtOCardno, txtNCardno);
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        hidWarning.Value = "";
    }

    protected void btnReadNCard_Click(object sender, EventArgs e)
    {
        //if (!txtNCardno.Text.Trim().StartsWith("715009"))
        //{
        //    context.AddError("非市民卡B卡不可售卡");
        //    return;
        //}

        TMTableModule tmTmTableModule = new TMTableModule();
        if (txtNCardno.Text != hiddenChangeCardno.Value)
        {
            context.AddError("A001022101");
            return;
        }

        //卡账户有效性检验
        //SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        //pdo.CARDNO = txtNCardno.Text;
        //bool ok = TMStorePModule.Excute(context, pdo);

        //context.SPOpen();
        //context.AddField("p_CARDNO").Value = txtNCardno.Text;
        //bool ok = context.ExecuteSP("SP_RB_Credit_Check");

        //if (ok)
        //{
        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTdMCardtypeIn = new TD_M_CARDTYPETDO();
        ddoTdMCardtypeIn.CARDTYPECODE = hiddenLabCardtype.Value;
        TD_M_CARDTYPETDO ddoTdMCardtypeOut = (TD_M_CARDTYPETDO)tmTmTableModule.selByPK(context, ddoTdMCardtypeIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

        LabCardtype.Text = ddoTdMCardtypeOut.CARDTYPENAME;
        NewcMoney.Text = ((Convert.ToDecimal(hiddencMoney.Value)) / (Convert.ToDecimal(100))).ToString("0.00");

        #region 账户信息
        DataTable dt = SPHelper.callQuery("SP_CA_Query", context, "QueryAcctList", txtNCardno.Text.Trim());

        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddError("A006023009:未查出客户账户信息");
            return;
        }
        else
        {
            foreach (DataRow item in dt.Rows)
            {
                if (item["状态"].ToString() != "有效")
                {
                    context.AddError("A001090103:客户账户状态无效");
                    return;
                }

                //余额账本
                TF_F_ACCT_BALANCETDO ddoTfFAcctBalanceIn = new TF_F_ACCT_BALANCETDO();
                ddoTfFAcctBalanceIn.ICCARD_NO = txtNCardno.Text.Trim();
                ddoTfFAcctBalanceIn.ACCT_ID = item["ACCT_ID"].ToString();

                TF_F_ACCT_BALANCETDO ddoTfFAcctBalanceOut = (TF_F_ACCT_BALANCETDO)tmTmTableModule.selByPK(context, ddoTfFAcctBalanceIn, typeof(TF_F_ACCT_BALANCETDO), null, "TF_F_ACCT_BALANCE", null);

                if (ddoTfFAcctBalanceOut == null)
                {
                    context.AddError("A001090104:未查出余额账本信息");
                    return;
                }
                else
                {
                    if (!ddoTfFAcctBalanceOut.STATE.Equals("A"))
                    {
                        context.AddError("A001090105:余额账本状态无效");
                        return;
                    }
                }
            }
        }

        gvAccount.DataSource = dt;
        gvAccount.DataBind();
        gvAccount.SelectedIndex = -1;
        #endregion

        //从IC卡资料表中读取数据
        TF_F_CARDRECTDO ddoTfFCardrecIn = new TF_F_CARDRECTDO();
        ddoTfFCardrecIn.CARDNO = txtNCardno.Text;
        TF_F_CARDRECTDO ddoTfFCardrecOut = (TF_F_CARDRECTDO)tmTmTableModule.selByPK(context, ddoTfFCardrecIn, typeof(TF_F_CARDRECTDO), null);

        if (txtNCardno.Text.Trim().StartsWith("715009049"))
        {
            NDepositBalance.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / (Convert.ToDecimal(100))).ToString("0.00");
        }
        else
        {
            NDeposit.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / (Convert.ToDecimal(100))).ToString("0.00");
        }

        NsDate.Text = ddoTfFCardrecOut.SERSTARTTIME.ToString("yyyy-MM-dd");

        //从现金台帐主表中读取换卡费用
        TF_B_TRADEFEETDO ddoTfBTradefeeIn = new TF_B_TRADEFEETDO();
        ddoTfBTradefeeIn.TRADEID = hiddenTradeid.Value;
        TF_B_TRADEFEETDO ddoTfBTradefeeOut = (TF_B_TRADEFEETDO)tmTmTableModule.selByPK(context, ddoTfBTradefeeIn, typeof(TF_B_TRADEFEETDO), null, "TF_B_TRADEFEE_ROLLBACK", null);

        if (ddoTfBTradefeeOut == null)
        {
            context.AddError("A001022104");
            return;
        }

        CardcostFee.Text = (Convert.ToDecimal(hiddenCardcostFee.Value) - Convert.ToDecimal(ddoTfBTradefeeOut.CARDSERVFEE) / (Convert.ToDecimal(100))).ToString("0.00");
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(DepositBalanceFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        ReturnSupply.Text = Total.Text;

        btnRollback.Enabled = true;
        btnPrintPZ.Enabled = false;
        //}

    }
    
    protected void btnRollback_Click(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        //查询是否当天当操作员进行的换卡
        TF_B_TRADETDO ddoTfBTradeIn = new TF_B_TRADETDO();

        string str = " Select CARDNO From TF_B_TRADE WHERE TRADEID = '" + hiddenTradeid.Value + "' " +
                     " And OPERATETIME BETWEEN Trunc(sysdate,'dd') AND sysdate" +
                     " And OPERATESTAFFNO = '" + context.s_UserID + "' ";
        DataTable data = tmTmTableModule.selByPKDataTable(context, ddoTfBTradeIn, null, str, 0);
        
        if (data.Rows.Count == 0)
        {
            context.AddError("A001021100");
            return;
        }

        //查询补卡操作是否最新的一次业务操作
        string strSale = " Select TRADETYPECODE From TF_B_TRADE WHERE CARDNO = '" + txtNCardno.Text + "' " +
                         " And OPERATETIME > (SELECT OPERATETIME FROM TF_B_TRADE WHERE CARDNO = '" + txtNCardno.Text + "' " +
                         " AND TRADETYPECODE = '4A' AND CANCELTAG = '0') " +
                         " And CANCELTAG = '0' AND ASCII(TRADETYPECODE) < 65 ";
        DataTable dataSale = tmTmTableModule.selByPKDataTable(context, ddoTfBTradeIn, null, strSale, 0);

        if (dataSale.Rows.Count > 0)
        {
            context.AddError("补卡不是最新一次业务操作");
            return;
        }

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecIn = new TF_F_CUSTOMERRECTDO();
        ddoTfFCustomerrecIn.CARDNO = txtNCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecOut = (TF_F_CUSTOMERRECTDO)tmTmTableModule.selByPK(context, ddoTfFCustomerrecIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTfFCustomerrecOut == null)
        {
            context.AddError("A001107112");
            return;
        }

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTdMPapertypeIn = new TD_M_PAPERTYPETDO();
        ddoTdMPapertypeIn.PAPERTYPECODE = ddoTfFCustomerrecOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTdMPapertypeOut = (TD_M_PAPERTYPETDO)tmTmTableModule.selByPK(context, ddoTdMPapertypeIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //证件类型赋值

        if (ddoTfFCustomerrecOut.PAPERTYPECODE != "")
        {
            hidPapertype.Value = ddoTdMPapertypeOut.PAPERTYPENAME;
        }
        else hidPapertype.Value = ddoTfFCustomerrecOut.PAPERTYPECODE;

        hidCustname.Value = ddoTfFCustomerrecOut.CUSTNAME;
        hidPaperno.Value = ddoTfFCustomerrecOut.PAPERNO;

        ////社保卡9位和32位

        //DataTable data1 = ResidentCardHelper.GetSocialInfo(context, ddoTfFCustomerrecOut.PAPERNO);

        //if (data1 != null && data1.Rows.Count > 0)
        //{
        //    hiddenSocialNo32.Value = Convert.ToString(data1.Rows[0]["SOCIALNO"]);
        //    hiddenSocialNo9.Value = Convert.ToString(data1.Rows[0]["SOCLSECNO"]).PadLeft(10, '0');
        //}
        //else
        //{
        hiddenSocialNo9.Value = "";
        hiddenSocialNo32.Value = "";
        //}

        string strTradeid = @"SELECT *
                              FROM (SELECT *
                                      FROM TF_B_TRADE
                                     WHERE CARDNO = '" + txtNCardno.Text + @"'
                                       AND TRADETYPECODE = '8P'
                                       AND CANCELTAG = '0'
                                     ORDER BY OPERATETIME DESC)
                             WHERE ROWNUM <= 1";

        TF_B_TRADETDO trade = (TF_B_TRADETDO)tmTmTableModule.selByPK(context, ddoTfBTradeIn, typeof(TF_B_TRADETDO), null, strTradeid);

        //存储过程赋值
        SP_PB_ChangeGSCardRollback_COMPDO pdo = new SP_PB_ChangeGSCardRollback_COMPDO();
        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, hiddenAsn.Value);
        pdo.AccID = DealString.GetRecordID(txtOCardno.Text.Substring(12, 4), txtOCardno.Text);
        pdo.AccTradeID = trade.TRADEID;
        pdo.OLDCARDNO = txtOCardno.Text;
        pdo.NEWCARDNO = txtNCardno.Text;
        pdo.TRADETYPECODE = "A4";
        pdo.CARDTRADENO = hiddentradeno.Value;
        pdo.CANCELTRADEID = hiddenTradeid.Value;
        pdo.REASONCODE = hiddenREASONCODE.Value;
        pdo.TRADEPROCFEE = Convert.ToInt32(Convert.ToDecimal(ProcedureFee.Text) * 100);
        pdo.OTHERFEE = Convert.ToInt32(Convert.ToDecimal(OtherFee.Text) * 100);
        if (txtNCardno.Text.Trim().StartsWith("715009049"))
        {
            pdo.NDEPOSIT = Convert.ToInt32(Convert.ToDecimal(NDepositBalance.Text) * 100);
        }
        else
        {
            pdo.NDEPOSIT = Convert.ToInt32(Convert.ToDecimal(NDeposit.Text) * 100);
        }
        pdo.CARDSTATE = hiddenCardstate.Value;
        pdo.SERSTAKETAG = hiddenSerstaketag.Value;
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;
        hidSupplyMoney.Value = hiddencMoney.Value;

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //换卡返销成功时提示
            AddMessage("M001022100");
            //写卡
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                    "changeCardRollback();", true);
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtOCardno.Text, hidCustname.Value, "换卡返销", ReturnSupply.Text
            , ReturnSupply.Text, txtNCardno.Text, hidPaperno.Value, "",
             "0.00", ReturnSupply.Text, context.s_UserID, context.s_DepartName, hidPapertype.Value, ProcedureFee.Text, "0.00");

            //换卡返销按钮不可用
            btnRollback.Enabled = false;
            //重新初始化
            InitLoad(sender, e);
        }
    }
}