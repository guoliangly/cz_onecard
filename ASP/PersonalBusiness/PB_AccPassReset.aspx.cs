﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using PDO.GroupCard;
using PDO.PersonalBusiness;
using TDO.CardManager;

/// <summary>
/// 密码重置页面
/// </summary>
public partial class ASP_PersonalBusiness_PB_AccPassReset : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";
    }

    /// <summary>
    /// 查询按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        foreach (Control con in this.Page.Controls)
        {
            ClearLabelControl(con);
        }

        TMTableModule tmTMTableModule = new TMTableModule();

        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            // （1）查询卡资料
            String sql = "SELECT CUSTNAME,CUSTSEX,CUSTBIRTH,PAPERTYPECODE,PAPERNO," +
                         " CUSTADDR,CUSTPOST, CUSTPHONE,CUSTEMAIL  " +
                         " FROM  TF_F_CUSTOMERREC " +
                         " WHERE CARDNO = '" + txtCardno.Text + "'";
            TMTableModule tm = new TMTableModule();
            DataTable data = null;
            data = tm.selByPKDataTable(context, sql, 0);
            if (data == null || data.Rows.Count == 0) // 无记录
            {
                context.AddError("A001107112");//无法查询卡片资料
            }
            else
            {
                Object[] row = data.Rows[0].ItemArray;
                labOldName.Text = getCellValue(row[0]);
                labOldSex.Text = getSex(getCellValue(row[1]));
                labOldBirth.Text = getCellValue(row[2]);
                labOldPaperType.Text = getCellValue(row[3]);
                labOldPaperNo.Text = getCellValue(row[4]);
                labOldAddr.Text = getCellValue(row[5]);
                labOldPost.Text = getCellValue(row[6]);
                labOldPhone.Text = getCellValue(row[7]);
                txtEmail.Text = getCellValue(row[8]);
            }            

            // （4） 查询卡片状态

            sql = " SELECT b.RESSTATE                       " +
                " FROM TL_R_ICUSER        a,              " +
                "      TD_M_RESOURCESTATE b               " +
                " WHERE a.RESSTATECODE    = b.RESSTATECODE" +
                " AND   a.CARDNO = '" + txtCardno.Text + "'";
            data = tm.selByPKDataTable(context, sql, 0);
            if (data == null || data.Rows.Count == 0) // 无记录
            {
                context.AddError("A001001101");//无法查询卡片状态
            }
            else
            {
                Object[] row = data.Rows[0].ItemArray;
                labOldState.Text = (string)row[0];
            }

            if (context.hasError())
            {
                return;
            }

            btnSubmit.Enabled = true;
        }
    }

    /// <summary>
    /// 重置按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtCardno.Text == "")
        {
            context.AddError("A005001100:未输入卡号");
            return;
        }
        bool okCard = true;
        //重置卡上密码
        context.SPOpen();
        context.AddField("p_cardNo").Value = txtCardno.Text;

        okCard = context.ExecuteSP("SP_PB_accPasswordReset");

        if (okCard)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
            "resetPassword2();", true);
        }
        if (okCard)
        {
            AddMessage("M004111100");

            btnSubmit.Enabled = false;

            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
        }
    }

    private string getCellValue(Object obj)
    {
        return (obj == DBNull.Value ? "" : (string)obj);
    }

    private string getSex(string sex) { 
        if(sex=="0"){
            return "男";
        }else{
            return "女";
        }
    }

}
