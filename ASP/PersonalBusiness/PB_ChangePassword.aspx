﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_ChangePassword.aspx.cs" Inherits="ASP_PersonalBusiness_PB_ChangePassword" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>修改用户密码</title>

    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

   <script type="text/javascript">
       var flagPW = '-1';

       function $(_sId) {
           return window.document.getElementById(_sId);
       }

       function ReadNewKey() {
           try {

               var com = "4,9600,n,8,1";
               reta = ICcard.Read_Key(com, 1, 25000);
               return reta;
           }
           catch (e) {
               return "";
           }
       }
       function ReadNew2Key() {
           try {
               var com = "4,9600,n,8,1";
               reta = ICcard.Read_Key(com, 2, 25000);
               return reta;
           }
           catch (e) {
               return "";
           }
       }

       function StartPort() {
           strInput = "";
           if (document.getElementById('cbxIsFirst').checked) {
               flagPW = '1';
               $('txtOPwd').value = '000000';
               $('txtOPwd').style.readOnly = true;
               $('txtNPwd').value = '';
               $('txtNPwd').focus();
           }
           else {
               flagPW = '0';
               $('txtOPwd').value = '';
               $('txtOPwd').focus();
           }

           MSComm1_OnComm();
       }
       function MSComm1_OnComm() {
           if (flagPW == '0') {
               strInput = ReadNewKey();
           } else if (flagPW == '1') {
               strInput = ReadNewKey();
           } else if (flagPW == '2') {
               strInput = ReadNew2Key();
           }
           // var re = /^[-\+]?\d+(\.\d+)?$/;

           if (strInput == null || strInput == "") {
               MyExtAlert('密码键盘有误：', '请检查密码键盘是否插好' + ', 错误信息:' + strInput);
               return;
           } else {
               if (strInput.indexOf('串口') > -1) {
                   MyExtAlert('密码键盘有误：', '请检查密码键盘和COM端口号' + ', 错误信息:' + strInput);
                   return;
               }
           }




           if (strInput != "") {
               if (flagPW == '0') {
                   flagPW = '1';
                   $("txtOPwd").value = strInput;
                   strInput = "";
                   $("txtNPwd").value = '';
                   $("txtNPwd").focus();
                   MSComm1_OnComm();
               } else if (flagPW == '1') {
                   flagPW = '2';
                   $("txtNPwd").value = strInput;
                   strInput = "";
                   $("txtANPwd").value = '';
                   $("txtANPwd").focus();
                   MSComm1_OnComm();
               } else if (flagPW == '2') {
                   flagPW = '-1';
                   $("txtANPwd").value = strInput;
                   strInput = "";
                   if (!$("btnChangeUserPass").disabled) {
                       $("btnChangeUserPass").focus();
                   }

               }
           }
       }

       function IsFirstChange() {
           if ($('cbxIsFirst').checked) {
               $('txtOPwd').value = '000000';
               $('txtOPwd').readOnly = true;
               $('txtNPwd').value = '';
               $('txtNPwd').focus();
           }
           else {
               $('txtOPwd').value = '';
               $('txtOPwd').readOnly = false;
               $('txtOPwd').focus();
           }
       }
   </script>

</head>
<body>
<OBJECT classid="clsid:016E79EE-0B07-46E1-8866-2B679B73898A"id="ICcard"
            codebase="ICcard.ocx" width="0" height="0">
</object>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
             个人业务->修改用户密码
        </div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidPwdNum" Value="0" runat="server" />
                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <asp:HiddenField runat="server" ID="hidWarning" />
                <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
                <asp:HiddenField ID="hiddenCardRent" runat="server" />
                <asp:HiddenField ID="hiddenOldPassword" runat="server" />
                 <asp:HiddenField ID="hiddenNewPassword" runat="server" />
                <div class="con">
                    <div class="card">
                        卡片信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="9%">
                                    <div align="right">
                                        用户卡号:</div>
                                </td>
                                <td width="14%">
                                    <asp:TextBox ID="txtCardno" CssClass="labeltext" runat="server"></asp:TextBox></td>
                                <asp:HiddenField ID="hiddenread" runat="server" />
                                <td width="10%">
                                    <asp:Button ID="btnChangeUserPassCardread" CssClass="button1" runat="server" Text="读卡"
                                        OnClientClick="return readCardNo();" OnClick="btnChangeUserPassCardread_Click" /></td>
                                <td width="11%">
                                    &nbsp;</td>
                                <td width="18%">
                                    &nbsp;</td>
                                <td width="12%">
                                    &nbsp;</td>
                                <td width="11%">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div class="pip">
                        用户信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="11%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustName" runat="server"></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td width="14%">
                                    <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label></td>
                                <td width="11%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td width="8%">
                                    <asp:Label ID="Papertype" runat="server" Text=""></asp:Label></td>
                                <td width="8%">
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td width="23%" colspan="3">
                                    <asp:Label ID="Paperno" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custsex" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custphone" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="Custpost" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                    电子邮件:</td>
                                <td>
                                    <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        </div>
                                </td>
                                <td>
                                    </td>
                                <td>
                                    <div align="right">
                                    </div>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div class="card">
                        修改密码</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                            <td>
                                    <div align="center" valign="middle">
                                    <asp:CheckBox ID="cbxIsFirst" runat="server" AutoPostBack="false"  onpropertychange="javascript:IsFirstChange();" />首次修改
                                     </div>
                                </td>
                                <td width="15%">
                                <input id="btnPassInput" CssClass="button1" type="button" value="小键盘输入密码" onclick="StartPort()" />
                                    </td>
                                <td>
                                    <div align="right">
                                        原密码:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOPwd" CssClass="inputmid" TextMode="Password" MaxLength="6" runat="server"></asp:TextBox></td>
                                <td>
                                    <div align="right">
                                        新密码:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNPwd" CssClass="inputmid" TextMode="Password" MaxLength="6" runat="server"></asp:TextBox></td>
                                <td>
                                    <div align="right">
                                        新密码确认:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtANPwd" CssClass="inputmid" TextMode="Password" MaxLength="6"
                                        runat="server"></asp:TextBox></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="330px" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                            </td>
                            <td align="right">
                                &nbsp;
                               

                            </td>
                            <td style="width: 30px">
                            </td>
                            <td style="width: 120px">
                                <asp:Button ID="btnChangeUserPass" CssClass="button1" runat="server" Text="修改" Enabled="false"
                                    OnClick="btnChangeUserPass_Click" /></td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
