﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TDO.BalanceChannel;
using TM;
using TDO.PersonalTrade;
using TDO.SupplyBalance;
using PDO.PersonalBusiness;

public partial class ASP_PersonalBusiness_PB_Refund : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BackSlope.Items.Add(new ListItem("100%", "0"));
            BackSlope.Items.Add(new ListItem("99.3%", "1"));
            initLoad(sender, e);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //初始化银行
        TD_M_BANKTDO ddoTD_M_BANKIn = new TD_M_BANKTDO();
        TD_M_BANKTDO[] ddoTD_M_BANKOutArr = (TD_M_BANKTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_BANKIn, typeof(TD_M_BANKTDO), "S008107211", "", null);

        ControlDeal.SelectBoxFill(selBank.Items, ddoTD_M_BANKOutArr, "BANK", "BANKCODE", true);

        btnRefund.Enabled = false;
    }

    private Boolean QueryValidation()
    {
        //对充值ID进行非空,长度,数字检验
        if (SupplyId.Text == "")
            context.AddError("A001016102", SupplyId);
        else if (SupplyId.Text.Length != 18)
            context.AddError("A001016103", SupplyId);
        else if (!Validation.isCharNum(SupplyId.Text))
            context.AddError("A001016104", SupplyId);

        return !(context.hasError());
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //对充值ID进行非空,长度,数字检验
        if (!QueryValidation())
            return;

        //查询是否已做过退款
        TF_B_REFUNDTDO ddoTF_B_REFUNDIn = new TF_B_REFUNDTDO();
        string strID = " Select ID From TF_B_REFUND Where ID = '" + SupplyId.Text + "'";
        DataTable dataID = tmTMTableModule.selByPKDataTable(context, ddoTF_B_REFUNDIn, null, strID, 0);

        if (dataID.Rows.Count != 0)
        {
            context.AddError("A001016101");
            return;
        }

        //查询卡号,交易金额
        TQ_SUPPLY_RIGHTTDO ddoTQ_SUPPLY_RIGHTIn = new TQ_SUPPLY_RIGHTTDO();
        ddoTQ_SUPPLY_RIGHTIn.ID = SupplyId.Text;
        TQ_SUPPLY_RIGHTTDO ddoTQ_SUPPLY_RIGHTOut = (TQ_SUPPLY_RIGHTTDO)tmTMTableModule.selByPK(context, ddoTQ_SUPPLY_RIGHTIn, typeof(TQ_SUPPLY_RIGHTTDO), null, "TQ_SUPPLY_RIGHT", null);

        if (ddoTQ_SUPPLY_RIGHTOut == null)
        {
            context.AddError("A001016100");
            return;
        }

        //页面赋值
        txtCardno.Text = ddoTQ_SUPPLY_RIGHTOut.CARDNO;
        BackMoney.Text = ((Convert.ToDouble(ddoTQ_SUPPLY_RIGHTOut.TRADEMONEY)) / 100).ToString("0.00");

        if (BackSlope.SelectedValue == "0")
            FactMoney.Text = BackMoney.Text;
        else if (BackSlope.SelectedValue == "1")
            FactMoney.Text = (Convert.ToDouble(BackMoney.Text) * 0.993).ToString("0.00");

        btnRefund.Enabled = true;
    }
    
    private Boolean RefundValidation()
    {
        //对银行做非空检验
        if (selBank.SelectedValue == "")
            context.AddError("A001016108", selBank);

        //对用户姓名进行非空、长度检验
        if (txtCusname.Text.Trim() == "")
            context.AddError("A001001111", txtCusname);
        else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
            context.AddError("A001001113", txtCusname);

        //对银行账户做非空,长度,数字检验
        if (BankAccNo.Text == "")
        {
            context.AddError("A001016105", BankAccNo);
        }
        else if (Validation.strLen(BankAccNo.Text) > 30)
        {
            context.AddError("A001016106", BankAccNo);
        }
        else if (!Validation.isNum(BankAccNo.Text))
        {
            context.AddError("A001016107", BankAccNo);
        }

        return !(context.hasError());
    }

    protected void btnRefund_Click(object sender, EventArgs e)
    {
        //对银行,银行账户和用户姓名进行检验
        if (!RefundValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();

        SP_PB_RefundPDO pdo = new SP_PB_RefundPDO();
        pdo.ID = SupplyId.Text.Trim();
        pdo.TRADETYPECODE = "91";
        pdo.CARDNO = txtCardno.Text;
        pdo.BACKMONEY = Convert.ToInt32(Convert.ToDouble(BackMoney.Text) * 100);
        pdo.BANKCODE = selBank.SelectedValue;
        pdo.BANKACCNO = BankAccNo.Text.Trim();
        if (BackSlope.SelectedValue == "0")
            pdo.BACKSLOPE = Convert.ToDecimal(1.00);
        else if (BackSlope.SelectedValue == "1")
            pdo.BACKSLOPE = Convert.ToDecimal(0.993);
        pdo.CUSTNAME = txtCusname.Text.Trim();
        pdo.FACTMONEY = Convert.ToInt32(Convert.ToDouble(FactMoney.Text) * 100);

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("M001016100");
            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
            initLoad(sender, e);
        }
    }

    protected void BackSlope_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (BackSlope.SelectedValue == "0" && BackMoney.Text != "")
            FactMoney.Text = BackMoney.Text;
        else if (BackSlope.SelectedValue == "1" && BackMoney.Text != "")
            FactMoney.Text = (Convert.ToDouble(BackMoney.Text) * 0.993).ToString("0.00");
    }
}
