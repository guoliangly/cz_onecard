﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using TDO.ResourceManager;
using TDO.UserManager;
using TDO.BusinessCode;
using TDO.CardManager;
using PDO.PersonalBusiness;
using Common;
using TDO.BalanceChannel;

public partial class ASP_PersonalBusiness_PB_ChangeOldCard : Master.FrontMaster
{

    //Update 2014-07-21 chwentao  记名卡换卡时除了证件类型、号码、姓名外别的可以修改

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // btnDBRead.Attributes["onclick"] = "warnCheck()";

            setReadOnly(OsDate, ODeposit, OldcMoney, LabCardtype, NsDate, NewcMoney, NDeposit);
            setReadOnly(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                            txtCustpost, txtCustaddr, txtCustEmail, txtRemark, txtReadPaper);
            txtRealRecv.Attributes["onfocus"] = "this.select();";
            txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this,'test', 'hidAccRecv');";
            if (!context.s_Debugging) setReadOnly(txtOCardno);
            if (!context.s_Debugging) txtNCardno.Attributes["readonly"] = "true";

            //初始化换卡类型

            setChangeReason(selReasonType, false);

            //初始化证件类型
            ASHelper.initPaperTypeList(context, selPapertype);

            //初始化性别
            ASHelper.initSexList(selCustsex);

            initLoad(sender, e);
            hidAccRecv.Value = Total.Text;
        }

    }

    // 设置损坏类型
    public void setChangeReason(DropDownList selReasonType, bool allowEmpty)
    {
        if (allowEmpty) selReasonType.Items.Add(new ListItem("---请选择---", ""));
        selReasonType.Items.Add(new ListItem("12:可读人为损坏卡", "12"));
        selReasonType.Items.Add(new ListItem("14:不可读人为损坏卡", "14"));

    }

    protected void initLoad(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从前台业务交易费用表中读取售卡费用数据

        TD_M_TRADEFEETDO ddoTD_M_TRADEFEETDOIn = new TD_M_TRADEFEETDO();
        ddoTD_M_TRADEFEETDOIn.TRADETYPECODE = "03";

        TD_M_TRADEFEETDO[] ddoTD_M_TRADEFEEOutArr = (TD_M_TRADEFEETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TRADEFEETDOIn, typeof(TD_M_TRADEFEETDO), "S001004139", "TD_M_TRADEFEE", null);

        for (int i = 0; i < ddoTD_M_TRADEFEEOutArr.Length; i++)
        {
            //费用类型为押金

            if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "00")
                hiddenDepositFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为卡费

            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "10")
                hiddenCardcostFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为手续费
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "03")
                hidProcedureFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为其他费用

            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "99")
                hidOtherFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
        }
        //费用赋值

        DepositFee.Text = hiddenDepositFee.Value;
        CardcostFee.Text = hiddenCardcostFee.Value;
        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        //卡费选项不可见,押金选中,读新卡按钮不可用,换卡按钮不可用


        Deposit.Visible = false;
        Cardcost.Checked = true;
        btnReadNCard.Enabled = false;
        Change.Enabled = false;
    }

    //换卡类型改变时,读卡和读数据库按钮是否可用改变

    protected void selReasonType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //foreach (Control con in this.Page.Controls)
        //{
        //    ClearControl(con);
        //}

        initLoad(sender, e);
        //换卡类型为可读卡时,读数据库按钮不可用,读卡按钮可用
        if (selReasonType.SelectedValue == "13" || selReasonType.SelectedValue == "12")
        {
            if (!context.s_Debugging) setReadOnly(txtOCardno);
            btnReadCard.Enabled = true;
            btnDBRead.Enabled = false;
            txtOCardno.CssClass = "labeltext";
        }
        //换卡类型为不可读卡时,读卡不可用,读数据库可用
        else if (selReasonType.SelectedValue == "14" || selReasonType.SelectedValue == "15")
        {
            txtOCardno.Attributes.Remove("readonly");
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = true;
            txtOCardno.CssClass = "input";
        }
    }

    //判断是否企福通,是企福通时显示卡费卡押金选项
    protected void GroupJudge(object sender, EventArgs e)
    {
    //    TMTableModule tmTMTableModule = new TMTableModule();

    //    //从企福通可充值账户表中读取数据

    //    TF_F_CARDOFFERACCTDO ddoTF_F_CARDOFFERACCIn = new TF_F_CARDOFFERACCTDO();
    //    ddoTF_F_CARDOFFERACCIn.CARDNO = txtOCardno.Text;
    //    ddoTF_F_CARDOFFERACCIn.USETAG = "1";

    //    TF_F_CARDOFFERACCTDO ddoTF_F_CARDOFFERACCOut = (TF_F_CARDOFFERACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDOFFERACCIn, typeof(TF_F_CARDOFFERACCTDO), null);

    //    if (ddoTF_F_CARDOFFERACCOut != null)
    //        Cardcost.Visible = true;
        DataTable dt = CAHelper.callQuery(context, "QueryAccountFlag", txtOCardno.Text);

        if (dt != null && dt.Rows.Count > 0)
        {
            Cardcost.Visible = true;
        }
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        //清空换卡方式
        hidChangeMode.Value = "";
        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验

        // txtOCardno.Text = hiddentxtCardno.Value;
        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtOCardno.Text;

        Master.PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            //从IC卡资料表中读取数据

            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtOCardno.Text;

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //从用户卡库存表(TL_R_ICUSER)中读取数据

            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtOCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //chenwentao 20140725  验证卡租金
            TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOIn = new TP_BUS_CARDRENTTDO();
            ddoTP_BUS_CARDRENTTDOIn.CARDNO = txtOCardno.Text;

            TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOOut = (TP_BUS_CARDRENTTDO)tmTMTableModule.selByPK(context, ddoTP_BUS_CARDRENTTDOIn, typeof(TP_BUS_CARDRENTTDO), null, "TP_BUS_CARDRENTTDO", null);
            if (ddoTP_BUS_CARDRENTTDOOut != null)
            {
                //ODeposit.Text = (Convert.ToDecimal(ddoTP_BUS_CARDRENTTDOOut.CARDRENT) / (Convert.ToDecimal(100))).ToString("0.00");
                context.AddError("A001001809: 当前卡有未转卡租金，请先转卡租金再退卡");
                return;
            }
            else
            {
                ODeposit.Text = "0.00";
            }

            //从资源状态编码表中读取数据

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;


            #region 获取用户信息
            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据


            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut!= null)
            {
                CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
                //性别显示
                if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                    Custsex.Text = "男";
                else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                    Custsex.Text = "女";
                else Custsex.Text = "";

                //出生日期显示
                if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
                {
                    String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                    if (Bdate.Length == 8)
                    {
                        CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                    }
                    else CustBirthday.Text = Bdate;
                }
                else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

                //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


                TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
                ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

                TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);


                //证件类型显示
                hidpapert.Value = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
                if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
                {
                    Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
                }
                else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

                Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
                Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
                Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
                Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
                txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
                Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

                hidUserName.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
                hidUserSex.Value = ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0" ? "01" //写卡时转换性别编码
                                 : ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1" ? "02"
                                 : "";
                hidUserPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;
                hidUserPhone.Value = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            }


            
            #endregion

            //给页面显示项赋值

            hidOldCardcost.Value = ddoTF_F_CARDRECOut.CARDCOST.ToString();
            SERTAKETAG.Value = ddoTF_F_CARDRECOut.SERSTAKETAG;
            OSERVICEMOENY.Value = ddoTF_F_CARDRECOut.SERVICEMONEY.ToString();
            SERSTARTIME.Value = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString();
            CUSTRECTYPECODE.Value = ddoTF_F_CARDRECOut.CUSTRECTYPECODE;
            //txtOCardno.Text = hiddentxtCardno.Value;
            
            OsDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");
            try {
                OldcMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / (Convert.ToDecimal(100))).ToString("0.00");
            }
           
            catch(Exception ex)
            {
                OldcMoney.Text = "0.00";
            }

            //判断是否记名卡
            CommonHelper.readCardJiMingState(context, txtOCardno.Text, hidIsJiMing);
            IsJiMing.Text = hidIsJiMing.Value == "1" ? "记名卡" : "不记名卡/无车日卡";
            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtOCardno.Text);
            GroupJudge(sender, e);
            hidCardReaderToken.Value = cardReader.createToken(context);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                "lockOldCard();", true);

            //btnReadNCard.Enabled = (hidWarning.Value.Length == 0);
            btnReadNCard.Enabled = true;
            Change.Enabled = false;
            btnPrintPZ.Enabled = false;
            btnPrintSJ.Enabled = false;

            hidLockFlag.Value = "true";
        }
        else if (pdoOut.retCode == "A001107199")
        {//验证如果是黑名单卡，锁卡
            this.LockBlackCard(txtOCardno.Text);
            this.hidLockBlackCardFlag.Value = "yes";
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnReadNCard.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            #region 如果是前台黑名单锁卡
            if (this.hidLockBlackCardFlag.Value == "yes")
            {
                AddMessage("黑名单卡已锁");
                clearCustInfo(txtOCardno, txtNCardno, txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                 txtCustpost, txtCustaddr, txtCustEmail, txtRemark);
                setReadOnly(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                            txtCustpost, txtCustaddr, txtCustEmail, txtRemark, txtReadPaper);
                hidChangeMode.Value = "";
                this.hidLockBlackCardFlag.Value = "";
                return;
            }
            #endregion

            if (hidLockFlag.Value == "true")
            {
                AddMessage("旧卡锁定成功");
            }
            else
            {
                if (selReasonType.SelectedValue == "14" || selReasonType.SelectedValue == "15")
                {
                    AddMessage("换卡成功,请于" + DateTime.Today.AddDays(7).ToString("yyyy-MM-dd") + "后来办理转值业务!");
                }
                else
                {
                    AddMessage("前台写卡成功");
                }
                clearCustInfo(txtOCardno, txtNCardno,txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                 txtCustpost, txtCustaddr, txtCustEmail, txtRemark);
                setReadOnly(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                            txtCustpost, txtCustaddr, txtCustEmail, txtRemark, txtReadPaper);
                hidChangeMode.Value = "";
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnDBRead_Click(sender, e);
        }

        if (chkPingzheng.Checked && btnPrintPZ.Enabled && chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printAll();", true);
        }
        else if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        else if (chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printShouJu();", true);
        }
        hidLockFlag.Value = "";
        hidWarning.Value = "";
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验

        if (txtOCardno.Text.Trim() == "")
            context.AddError("A001004113", txtOCardno);
        else
        {
            if ((txtOCardno.Text.Trim()).Length != 8)
                context.AddError("A009478016", txtOCardno);
            else if (!Validation.isNum(txtOCardno.Text.Trim()))
                context.AddError("A001004115", txtOCardno);
        }

        return !(context.hasError());
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        //清空换卡方式
        hidChangeMode.Value = "";
        //因卡片制作过程中出错，导致卡面显示卡号异常,增加提示
        string expstr=CommonHelper.IsExpCardno(txtOCardno.Text.Trim()) ;
        if (expstr != "")
        {
            context.AddError(expstr);
            return;
        }



        //对输入卡号进行检验

        if (!DBreadValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();
        //卡账户有效性检验

        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtOCardno.Text;

        Master.PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            //从IC卡资料表中读取数据

            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtOCardno.Text;

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //从IC卡电子钱包账户表中读取数据

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtOCardno.Text;

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

            //从用户卡库存表(TL_R_ICUSER)中读取数据

            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtOCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //chenwentao 20140725  验证卡租金
            TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOIn = new TP_BUS_CARDRENTTDO();
            ddoTP_BUS_CARDRENTTDOIn.CARDNO = txtOCardno.Text;

            TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOOut = (TP_BUS_CARDRENTTDO)tmTMTableModule.selByPK(context, ddoTP_BUS_CARDRENTTDOIn, typeof(TP_BUS_CARDRENTTDO), null, "TP_BUS_CARDRENTTDO", null);
            if (ddoTP_BUS_CARDRENTTDOOut != null)
            {
                //ODeposit.Text = (Convert.ToDecimal(ddoTP_BUS_CARDRENTTDOOut.CARDRENT) / (Convert.ToDecimal(100))).ToString("0.00");
                context.AddError("A001001809: 当前卡有未转卡租金，请先转卡租金再退卡");
                return;
            }
            else
            {
                ODeposit.Text = "0.00";
            }

            //从资源状态编码表中读取数据

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            #region 获取用户信息
            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据


            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut != null)
            {
                CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
                //性别显示
                if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                    Custsex.Text = "男";
                else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                    Custsex.Text = "女";
                else Custsex.Text = "";

                //出生日期显示
                if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
                {
                    String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                    if (Bdate.Length == 8)
                    {
                        CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                    }
                    else CustBirthday.Text = Bdate;
                }
                else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

                //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


                TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
                ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

                TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);


                //证件类型显示
                hidpapert.Value = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
                if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
                {
                    Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
                }
                else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

                Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
                Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
                Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
                Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
                txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
                Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;


                hidUserName.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
                hidUserSex.Value = ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0" ? "01" //写卡时转换性别编码
                                 : ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1" ? "02"
                                 : "";
                hidUserPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;
                hidUserPhone.Value = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;

                
            }


            
            #endregion

            //给页面显示项赋值

            hidOldCardcost.Value = ddoTF_F_CARDRECOut.CARDCOST.ToString();
            //hiddenAsn.Value = ddoTF_F_CARDRECOut.ASN;
            SERTAKETAG.Value = ddoTF_F_CARDRECOut.SERSTAKETAG;
            OSERVICEMOENY.Value = ddoTF_F_CARDRECOut.SERVICEMONEY.ToString();
            SERSTARTIME.Value = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString();
            CUSTRECTYPECODE.Value = ddoTF_F_CARDRECOut.CUSTRECTYPECODE;
            ODeposit.Text = (Convert.ToDecimal(ddoTF_F_CARDRECOut.DEPOSIT) / 100).ToString("0.00");
            OsDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");
            OldcMoney.Text = (Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY) / 100).ToString("0.00");


            //判断是否记名卡
            CommonHelper.readCardJiMingState(context, txtOCardno.Text, hidIsJiMing);
            IsJiMing.Text = hidIsJiMing.Value == "1" ? "记名卡" : "不记名卡/无车日卡";
            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtOCardno.Text);

            hiddentxtCardno.Value = txtOCardno.Text;
            GroupJudge(sender, e);

            //读新卡按钮可用

            btnReadNCard.Enabled = true;
            txtNCardno.Text = "";
            Change.Enabled = false;

            btnPrintPZ.Enabled = false;
            btnPrintSJ.Enabled = false;
        }
        else if (pdoOut.retCode == "A001107199")
        {//验证如果是黑名单卡，锁卡
            this.LockBlackCard(txtOCardno.Text);
            this.hidLockBlackCardFlag.Value = "yes";
        }
    }

    protected void btnReadNCard_Click(object sender, EventArgs e)
    {
        //清空换卡方式
        hidChangeMode.Value = "";
        TMTableModule tmTMTableModule = new TMTableModule();

        //从用户卡库存表(TL_R_ICUSER)中读取数据

        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtNCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
           
        }
        else
        {
            //从资源状态编码表中读取数据

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                NewResstateCode.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                NewResstateCode.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;



            if (ddoTL_R_ICUSEROut.RESSTATECODE == "01" || ddoTL_R_ICUSEROut.RESSTATECODE == "05")
            {
                hidChangeMode.Value = "1";//换新卡

                //旧卡为不记名卡且换新卡时需要填写
                if (hidIsJiMing.Value == "0")
                {
                  
                    clearReadOnly(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                        txtCustpost, txtCustaddr, txtCustEmail, txtRemark, txtReadPaper);
                    //clearCustInfo(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                    //    txtCustpost, txtCustaddr, txtCustEmail, txtRemark);
                }
                else
                {
                  
                    setReadOnly(txtCusname, selPapertype, txtCustpaperno, selCustsex,txtReadPaper);
                    clearReadOnly(txtCustbirth, txtCustphone, txtCustpost, txtCustaddr, txtCustpost, txtCustEmail, txtRemark, selCustsex);

                    txtCusname.Text = CustName.Text;
                    txtCustaddr.Text = Custaddr.Text;
                    txtCustbirth.Text = CustBirthday.Text;
                    txtCustEmail.Text =txtEmail.Text;
                    txtCustpaperno.Text = Paperno.Text;
                    txtCustphone.Text = Custphone.Text;
                    txtCustpost.Text = Custpost.Text;
                    txtRemark.Text = Remark.Text;
                    if (hidUserSex.Value == "01")
                    {
                        selCustsex.SelectedValue = "0";
                    }
                    if (hidUserSex.Value == "02")
                    {
                        selCustsex.SelectedValue = "1";
                    }
                    selPapertype.SelectedValue = hidpapert.Value;

                    //clearCustInfo(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                    //    txtCustpost, txtCustaddr, txtCustEmail, txtRemark);
                }
            }
            else
            {
                setReadOnly(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                            txtCustpost, txtCustaddr, txtCustEmail, txtRemark, txtReadPaper);

                clearCustInfo(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                    txtCustpost, txtCustaddr, txtCustEmail, txtRemark);
                //如旧卡是记名卡，则不能换旧龙城卡。
                if (hidIsJiMing.Value == "1")
                {
                    context.AddError("旧卡为记名卡只能换出库或分配状态的龙城通卡");
                    return;
                }
                //如果是换旧卡，则旧卡必须为有效卡
                SP_AccCheckPDO pdo = new SP_AccCheckPDO();
                pdo.CARDNO = txtNCardno.Text;

                Master.PDOBase pdoOut;
                bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);
                if (context.hasError()) return;
                hidChangeMode.Value = "2";//换旧卡
            }
        }
        
       

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据

        // txtNCardno.Text = hiddentxtCardno.Value;
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = hiddenLabCardtype.Value;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

        if (hidChangeMode.Value == "1")//换新卡判断，新卡必须是普通龙城通卡
        {
            if (hiddenGjTag.Value != "00")
            {
                context.AddError("此卡不是普通卡，不能换卡");
                return;
            }
        }
        
        //给页面显示项赋值

        hidCardprice.Value = ddoTL_R_ICUSEROut.CARDPRICE.ToString();
        txtNCardno.Text = txtNCardno.Text;
        NDeposit.Text = (Convert.ToDecimal(ddoTL_R_ICUSEROut.CARDPRICE) / 100).ToString("0.00");
        LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
        NsDate.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
        NewcMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / 100).ToString("0.00");

        FeeCount(sender, e);

        hidAccRecv.Value = Total.Text;
        txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();
        if (hidChangeMode.Value == "1")//换新卡如果新卡是出库或分配状态，则必须判断新卡卡内余额不能为0.
        {
            if (Convert.ToDecimal(hiddencMoney.Value) != 0)
            {
                context.AddError("A001001144");
                return;
            }
        }

        //换卡按钮可用
        Change.Enabled = true;

        btnPrintPZ.Enabled = false;
        btnPrintSJ.Enabled = false;
    }
    

    //根据换卡类型和选择押金类型确定费用
    protected void FeeCount(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        TD_M_CALLINGSTAFFTDO ddoTD_M_CALLINGSTAFFIn = new TD_M_CALLINGSTAFFTDO();
        ddoTD_M_CALLINGSTAFFIn.OPERCARDNO = txtOCardno.Text;
        TD_M_CALLINGSTAFFTDO[] ddoTD_M_CALLINGSTAFFOut = (TD_M_CALLINGSTAFFTDO[])tmTMTableModule.selByPKArr(context,
            ddoTD_M_CALLINGSTAFFIn, typeof(TD_M_CALLINGSTAFFTDO), null, "TD_M_CALLINGSTAFF_BY_CARDNO", null);

        //老卡不是司机卡

        if (ddoTD_M_CALLINGSTAFFOut == null || ddoTD_M_CALLINGSTAFFOut.Length == 0)
        {
            //换卡类型为自然损坏卡时

            if (selReasonType.SelectedValue == "13" || selReasonType.SelectedValue == "15")
            {
                DepositFee.Text = hiddenDepositFee.Value;
                CardcostFee.Text = hiddenCardcostFee.Value;
            }
            //换卡类型为人为损坏卡时

            else if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
            {
                //选择卡押金

                if (Deposit.Checked == true)
                {
                    DepositFee.Text = (Convert.ToDecimal(hiddenDepositFee.Value) + Convert.ToDecimal(hidCardprice.Value) / 100).ToString("0.00");
                    CardcostFee.Text = hiddenCardcostFee.Value;
                }
                //选择卡费
                else if (Cardcost.Checked == true)
                {
                    DepositFee.Text = hiddenDepositFee.Value;
                    CardcostFee.Text = (Convert.ToDecimal(hiddenCardcostFee.Value) + Convert.ToDecimal(hidCardprice.Value) / 100).ToString("0.00");
                }
            }
        }
        //老卡是司机卡
        else if (ddoTD_M_CALLINGSTAFFOut.Length > 0)
        {
            //换卡类型为自然损坏卡
            if (selReasonType.SelectedValue == "13" || selReasonType.SelectedValue == "15")
            {
                DepositFee.Text = hiddenDepositFee.Value;
                CardcostFee.Text = hiddenCardcostFee.Value;
            }
            //换卡类型为人为损坏卡
            else if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
            {
                DepositFee.Text = hiddenDepositFee.Value;
                CardcostFee.Text = (Convert.ToDecimal(hidOldCardcost.Value) / 100).ToString("0.00");
            }
        }
        
        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        //旧卡如果是记名卡，则判断折扣，wdx20140708
        if (hidIsJiMing.Value == "1")
        {
            if (Paperno.Text.Trim() != "")
            {
                DataTable data = SPHelper.callPBQuery(context, "QryDiscount", Paperno.Text.Trim());
                //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠

                if (data == null || data.Rows.Count == 0)
                {
                    QryDiscount();
                    return;
                }
                //else
                //{
                //    if (txtNCardno.Text.Trim() != "")
                //    {
                //        btnReadNCard_Click(sender, e);
                //    }
                //}

            }
            else
            {
                QryDiscount();
            }
        }
        //如果是换旧卡，则无费用
        if (hidChangeMode.Value == "2")
        {
            ProcedureFee.Text = "0.00";
            OtherFee.Text = "0.00";
            Total.Text = "0.00";
       
        }
    }

    protected void QryDiscount()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从系统参数表读取新卡费用
        TD_M_TAGTDO ddoTD_M_TAGIn = new TD_M_TAGTDO();
        ddoTD_M_TAGIn.TAGCODE = "BUSCARD_MONEY";
        TD_M_TAGTDO ddoTD_M_TAGOut = (TD_M_TAGTDO)tmTMTableModule.selByPK(context, ddoTD_M_TAGIn, typeof(TD_M_TAGTDO), null, "TD_M_TAG", null);

        DepositFee.Text = "0.00";
        CardcostFee.Text = ((Convert.ToDecimal(ddoTD_M_TAGOut.TAGVALUE)) / 100).ToString("0.00");

        Total.Text = ((Convert.ToDecimal(CardcostFee.Text)) + (Convert.ToDecimal(DepositFee.Text)) + (Convert.ToDecimal(OtherFee.Text))).ToString("0.00");
        txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();
    }

    protected void ReadPaper_Click(object sender, EventArgs e)
    {
        Paperno_Changed(sender, e);
    }

    protected void Paperno_Changed(object sender, EventArgs e)
    {
        //如果是异形卡。则不按照龙城通的规则
        if (txtNCardno.Text.Trim() != "" && txtNCardno.Text.Trim().Substring(0, 6) == "915003")
        {
            return;
        }
        if (txtCustpaperno.Text.Trim() != "")
        {
            DataTable data = SPHelper.callPBQuery(context, "QryDiscount", txtCustpaperno.Text.Trim());
            //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠

            if (data == null || data.Rows.Count == 0)
            {
                QryDiscount();
                return;
            }
            else
            {
                if (txtNCardno.Text.Trim() != "")
                {
                    btnReadNCard_Click(sender, e);
                }
            }

        }
    }

    //更改押金类型时更新费用信息

    protected void Deposit_Changed(object sender, EventArgs e)
    {
        string change;
        if (Deposit.Checked == true)
        {
            change = CardcostFee.Text;
            CardcostFee.Text = DepositFee.Text;
            DepositFee.Text = change;
        }
    }

    //更改卡费用类型时,改变费用
    protected void Cardcost_Changed(object sender, EventArgs e)
    {
        string change;
        if (Cardcost.Checked == true)
        {
            change = DepositFee.Text;
            DepositFee.Text = CardcostFee.Text;
            CardcostFee.Text = change;
        }
    }

    //对售卡用户信息进行检验

    private Boolean SaleInfoValidation()
    {
        //当选择非记名卡时

       

        //当选择记名卡时
        if (true)
        {
            //对用户姓名进行非空、长度检验

            if (txtCusname.Text.Trim() == "")
                context.AddError("A001001111", txtCusname);
            else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
                context.AddError("A001001113", txtCusname);

            //对用户性别进行非空检验

            if (selCustsex.SelectedValue == "")
                context.AddError("A001001116", selCustsex);

            //对证件类型进行非空检验

            if (selPapertype.SelectedValue == "")
                context.AddError("A001001117", selPapertype);

            //对出生日期进行非空、日期格式检验

            String cDate = txtCustbirth.Text.Trim();
            if (cDate == "")
                context.AddError("A001001114", txtCustbirth);
            else if (!Validation.isDate(txtCustbirth.Text.Trim()))
                context.AddError("A001001115", txtCustbirth);

            //对联系电话进行非空、长度、数字检验

            if (txtCustphone.Text.Trim() == "")
                context.AddError("A001001124", txtCustphone);
            else if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
                context.AddError("A001001126", txtCustphone);
            else if (!Validation.isNum(txtCustphone.Text.Trim()))
                context.AddError("A001001125", txtCustphone);

            //对证件号码进行非空、长度、英数字检验

            if (txtCustpaperno.Text.Trim() == "")
                context.AddError("A001001121", txtCustpaperno);
            else if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
                context.AddError("A001001122", txtCustpaperno);
            else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
                context.AddError("A001001123", txtCustpaperno);
            else if (selPapertype.SelectedValue == "00")//身份证必须为15或18位。
            {
                if (Validation.strLen(txtCustpaperno.Text.Trim()) != 18 && Validation.strLen(txtCustpaperno.Text.Trim()) != 15)
                    context.AddError("A001001130", txtCustpaperno);
                else
                {
                    //身份证号规则校验
                    if (!CommonHelper.CheckIDCard(txtCustpaperno.Text.Trim()))
                    {
                        context.AddError("A001001131", txtCustpaperno);
                    }
                }
            }

            //对邮政编码进行非空、长度、数字检验

            if (txtCustpost.Text.Trim() != "")
            {
                //context.AddError("A001001118", txtCustpost);
                if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                    context.AddError("A001001120", txtCustpost);
                else if (!Validation.isNum(txtCustpost.Text.Trim()))
                    context.AddError("A001001119", txtCustpost);
            }

            //对联系地址进行非空、长度检验

            if (txtCustaddr.Text.Trim() == "")
                context.AddError("A001001127", txtCustaddr);
            else if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);

            //对备注进行长度检验

            if (txtRemark.Text.Trim() != "")
                if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                    context.AddError("A001001129", txtRemark);

            //对电子邮件进行格式检验

            if (txtCustEmail.Text.Trim() != "")
                new Validation(context).isEMail(txtCustEmail.Text.Trim());
        }
        return !(context.hasError());
    }

    protected void Change_Click(object sender, EventArgs e)
    {

        //换新卡还是换旧卡判断
        TMTableModule tmTMTableModule = new TMTableModule();
        //从用户卡库存表(TL_R_ICUSER)中读取数据

        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtNCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;

        }
        else
        {
            if (ddoTL_R_ICUSEROut.RESSTATECODE == "01" || ddoTL_R_ICUSEROut.RESSTATECODE == "05")
            {
                hidChangeMode.Value = "1";//换新卡
            }
            else
            {
                //如果是换旧卡，则旧卡必须为有效卡
                SP_AccCheckPDO pdoAcc = new SP_AccCheckPDO();
                pdoAcc.CARDNO = txtNCardno.Text;
                Master.PDOBase pdoOut;
                TMStorePModule.Excute(context, pdoAcc, out pdoOut);
                if (context.hasError()) return;
                hidChangeMode.Value = "2";//换旧卡
            }
        }

        if (hidChangeMode.Value == "2")//换旧卡
        {
            //如旧卡是记名卡，则不能换旧龙城卡。
            if (hidIsJiMing.Value == "1")
            {
                context.AddError("旧卡为记名卡只能换出库或分配状态的龙城通卡");
                return;
            }
            ChangeSaleCard_Click(sender, e);
            return;
        }

        //旧卡如果是不记名卡，则判断新卡信息
        if (hidIsJiMing.Value == "0")
        {
            //新卡用户信息判断
            if (!SaleInfoValidation())
                return;
        }

       
        //如果是移动龙城通卡，则不能换卡
        if (txtNCardno.Text.Trim().StartsWith("91501101") || txtOCardno.Text.Trim().StartsWith("91501101"))
        {
            context.AddError("移动龙城通卡不能换卡");
            return;
        }

        if (txtNCardno.Text.Trim().StartsWith("91501103") || txtOCardno.Text.Trim().StartsWith("91501103"))
        {
            context.AddError("江南农商行龙城通卡不能换卡");
            return;
        }
        //如果是天翼龙成通卡，则不判断卡所属员工是否是售卡员工wdx 20111202
        if (txtNCardno.Text.Trim().Substring(0, 8) == "91501102")
        {
            //卡库存状态不是出库或分配状态
            if (ddoTL_R_ICUSEROut.RESSTATECODE != "01" && ddoTL_R_ICUSEROut.RESSTATECODE != "05")
            {
                context.AddError("A001004132");
                return;
            }
        }
        //判断售卡权限
        else
        {
            checkCardState(txtNCardno.Text);
        }
        if (context.hasError()) return;

        if (txtRealRecv.Text == null)
        {
            context.AddError("A001001143");
            return;
        }

        //获取旧卡的公交类型


        //TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
        //ddoTF_F_CARDCOUNTACCIn.CARDNO = txtOCardno.Text;

        //TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC_CARDNO", null);

        //if (ddoTF_F_CARDCOUNTACCOut != null)
        //{
        //    context.AddError("此旧卡非普通卡,不能换卡");
        //    return;
        //}


        //读取审核员工信息
        TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        ddoTD_M_INSIDESTAFFIn.OPERCARDNO = hiddenCheck.Value;

        TD_M_INSIDESTAFFTDO[] ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);

        SP_PB_ChangeOldCardPDO pdo = new SP_PB_ChangeOldCardPDO();
        //存储过程赋值

        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, txtNCardno.Text);
        //pdo.ID = hiddentradeno.Value + DateTime.Now.ToString("MMddhhmmss") + hiddenAsn.Value.Substring(16, 4);
        pdo.CUSTRECTYPECODE = CUSTRECTYPECODE.Value;
        pdo.CARDCOST = Convert.ToInt32(Convert.ToDecimal(CardcostFee.Text) * 100);
        pdo.NEWCARDNO = txtNCardno.Text;
        pdo.OLDCARDNO = txtOCardno.Text;
        pdo.ONLINECARDTRADENO = hiddentradeno.Value;
        pdo.CHANGECODE = selReasonType.SelectedValue;
        pdo.ASN = hiddenAsn.Value.Substring(4, 16);
        //pdo.ASN = hiddenASn.Value;
        pdo.CARDTYPECODE = hiddenLabCardtype.Value;
        pdo.SELLCHANNELCODE = "01";
        pdo.TRADETYPECODE = "03";
        pdo.PREMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;
        //最近卡充值实际余额为本次交易前卡内余额

        pdo.SUPPLYREALMONEY = 0;


        pdo.CUSTNAME = txtCusname.Text.Trim();
        pdo.CUSTSEX = selCustsex.SelectedValue;
        if (txtCustbirth.Text.Trim() != "")
        {
            String[] arr = (txtCustbirth.Text.Trim()).Split('-');

            String cDate = arr[0] + arr[1] + arr[2];

            pdo.CUSTBIRTH = cDate;
        }

        else
        {
            pdo.CUSTBIRTH = txtCustbirth.Text.Trim();
        }

        pdo.PAPERTYPECODE = selPapertype.SelectedValue;
        pdo.PAPERNO = txtCustpaperno.Text.Trim();
        pdo.CUSTADDR = txtCustaddr.Text.Trim();
        pdo.CUSTPOST = txtCustpost.Text.Trim();
        pdo.CUSTPHONE = txtCustphone.Text.Trim();
        pdo.CUSTEMAIL = txtCustEmail.Text.Trim();
        pdo.REMARK = txtRemark.Text.Trim();

        //换卡类型为可读自然损坏卡
        if (selReasonType.SelectedValue == "13")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100);
            pdo.SERSTARTTIME = Convert.ToDateTime(SERSTARTIME.Value);
            pdo.SERVICEMONE = Convert.ToInt32(OSERVICEMOENY.Value);
            pdo.CARDACCMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.NEWSERSTAKETAG = SERTAKETAG.Value;
            //pdo.SUPPLYREALMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.TOTALSUPPLYMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.OLDDEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100 - Convert.ToDecimal(OSERVICEMOENY.Value));
            pdo.SERSTAKETAG = "3";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100 + Convert.ToDecimal(NewcMoney.Text) * 100);
            pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.CHECKSTAFFNO = context.s_UserID;
            pdo.CHECKDEPARTNO = context.s_DepartID;
        }
        //换卡类型为可读人为损坏卡
        else if (selReasonType.SelectedValue == "12")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.NEWSERSTAKETAG = "0";
            //pdo.SUPPLYREALMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.TOTALSUPPLYMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100 + Convert.ToDecimal(NewcMoney.Text) * 100);
            pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.CHECKSTAFFNO = context.s_UserID;
            pdo.CHECKDEPARTNO = context.s_DepartID;
        }
        //换卡类型为不可读人为损坏卡

        else if (selReasonType.SelectedValue == "14")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            //pdo.SUPPLYREALMONEY = 0;
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A001010108");
                return;
            }
            pdo.CHECKSTAFFNO = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            pdo.CHECKDEPARTNO = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }
        //换卡类型为不可读自然损坏卡

        else if (selReasonType.SelectedValue == "15")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100);
            pdo.SERSTARTTIME = Convert.ToDateTime(SERSTARTIME.Value);
            pdo.SERVICEMONE = Convert.ToInt32(OSERVICEMOENY.Value);
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = SERTAKETAG.Value;
            //pdo.SUPPLYREALMONEY = 0;
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100 - Convert.ToDecimal(OSERVICEMOENY.Value));
            pdo.SERSTAKETAG = "3";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A001010108");
                return;
            }
            pdo.CHECKSTAFFNO = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            pdo.CHECKDEPARTNO = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据


        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut != null)
        {
            hidCustname.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            hidPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            //证件类型赋值


            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                hidPapertype.Value = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else hidPapertype.Value = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
        }
        


        hidSupplyMoney.Value = "" + pdo.CURRENTMONEY;

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //AddMessage("M001004001");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                    "changeCard();", true);
            btnPrintPZ.Enabled = true;


            ASHelper.preparePingZheng(ptnPingZheng, txtOCardno.Text, hidCustname.Value, "换卡", "0.00"
                , DepositFee.Text, txtNCardno.Text, hidPaperno.Value, (Convert.ToDecimal(pdo.NEXTMONEY) / (Convert.ToDecimal(100))).ToString("0.00"),
                "", Total.Text, context.s_UserName, context.s_DepartName, hidPapertype.Value, ProcedureFee.Text, "0.00");

            if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
            {
                btnPrintSJ.Enabled = true;
                ASHelper.prepareShouJu(ptnShouJu, txtNCardno.Text, hidCustname.Value, "换卡收费", Total.Text
             , "", "", "", "",
             "", Total.Text, context.s_UserName, context.s_DepartName, "", "0.00", "");
            }

            //foreach (Control con in this.Page.Controls)
            //{
            //    ClearControl(con);
            //}
            initLoad(sender, e);
            Change.Enabled = false;
        }
    }


    protected void ChangeSaleCard_Click(object sender, EventArgs e)
    {

        TMTableModule tmTMTableModule = new TMTableModule();
        //读取审核员工信息
        TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        ddoTD_M_INSIDESTAFFIn.OPERCARDNO = hiddenCheck.Value;

        TD_M_INSIDESTAFFTDO[] ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);

        SP_PB_ChangeOldSaleCardPDO pdo = new SP_PB_ChangeOldSaleCardPDO();
        //存储过程赋值

        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, txtNCardno.Text);
        //pdo.ID = hiddentradeno.Value + DateTime.Now.ToString("MMddhhmmss") + hiddenAsn.Value.Substring(16, 4);
        pdo.CUSTRECTYPECODE = CUSTRECTYPECODE.Value;
        pdo.CARDCOST = Convert.ToInt32(Convert.ToDecimal(CardcostFee.Text) * 100);
        pdo.NEWCARDNO = txtNCardno.Text;
        pdo.OLDCARDNO = txtOCardno.Text;
        pdo.ONLINECARDTRADENO = hiddentradeno.Value;
        pdo.CHANGECODE = selReasonType.SelectedValue;
        pdo.ASN = hiddenAsn.Value.Substring(4, 16);
        //pdo.ASN = hiddenASn.Value;
        pdo.CARDTYPECODE = hiddenLabCardtype.Value;
        pdo.SELLCHANNELCODE = "01";
        pdo.TRADETYPECODE = "3F";
        pdo.PREMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;
        //最近卡充值实际余额为本次交易前卡内余额
        if (selReasonType.SelectedValue == "12")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.NEWSERSTAKETAG = "0";
            pdo.SUPPLYREALMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.TOTALSUPPLYMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100 + Convert.ToDecimal(NewcMoney.Text) * 100);
            pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(OldcMoney.Text) * 100);
            pdo.CHECKSTAFFNO = context.s_UserID;
            pdo.CHECKDEPARTNO = context.s_DepartID;
        }
        //换卡类型为不可读人为损坏卡

        else if (selReasonType.SelectedValue == "14")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            pdo.SUPPLYREALMONEY = 0;
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            //如果换旧卡，则发生金额为0元。
            pdo.CURRENTMONEY = 0;//Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A001010108");
                return;
            }
            pdo.CHECKSTAFFNO = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            pdo.CHECKDEPARTNO = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }
        

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据


        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut != null)
        {
            hidCustname.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            hidPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            //证件类型赋值


            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                hidPapertype.Value = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else hidPapertype.Value = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
        }

        hidSupplyMoney.Value = "" + pdo.CURRENTMONEY;

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //AddMessage("M001004001");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                    "chargeCard();", true);
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtOCardno.Text, hidCustname.Value, "换旧卡", "0.00"
                , DepositFee.Text, txtNCardno.Text, hidPaperno.Value, (Convert.ToDecimal(pdo.NEXTMONEY) / (Convert.ToDecimal(100))).ToString("0.00"),
                "", Total.Text, context.s_UserName, context.s_DepartName, hidPapertype.Value, ProcedureFee.Text, "0.00");

            initLoad(sender, e);
           
            Change.Enabled = false;
        }
    }

}
