﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_ExceptionSync.aspx.cs" Inherits="ASP_PersonalBusiness_PB_ExceptionSync" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>异常同步</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.5.min.js" type="text/javascript"></script>
    <link href="../../css/Cust_AS.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/myext.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 8%;
            height: 28px;
        }

        .auto-style2 {
            width: auto;
            height: 28px;
        }
    </style>
        <script type="text/javascript">            
            //checkbox 全选
    </script>
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            个人业务->异常同步
        </div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ScriptManager1" runat="server" />
        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
                <div class="con">
                    <div class="card">数据同步</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>                                
                                <td width="9%">
                                    <div align="right">
                                        业务类型:
                                    </div>
                                </td>
                                <td width="13%">
                                    <asp:DropDownList ID="ddlTradeType" runat="server" Width="115px">
                                        <asp:ListItem Value="00">00:全部业务</asp:ListItem>
                                        <asp:ListItem Value="0E">0E:口挂挂失</asp:ListItem>
                                        <asp:ListItem Value="05">05:退卡</asp:ListItem>
                                        <asp:ListItem Value="6L">6L:挂失卡销户</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td width="10%">
                                    <div align="right">
                                        异常状态:
                                    </div>
                                </td>
                                <td width="10%">
                                    <asp:DropDownList ID="radioExType" runat="server">
                                        <asp:ListItem Text="同步未成功" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="已作废" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td width="10%">
                                    <div align="right">操作部门:</div>
                                </td>
                                <td width="10%">
                                    <asp:DropDownList ID="seldisDepart" CssClass="input" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selDisDept_Changed">
                                    </asp:DropDownList>
                                </td>
                                <td width="10%">
                                    <div align="right">操作员工:</div>
                                </td>
                                <td width="10%" colspan="2">
                                    <asp:DropDownList ID="seldisStaff" CssClass="input" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" OnClick="btnQuery_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="jieguo">
                        <span>
                            <asp:Label ID="LabExText" runat="server"></asp:Label></span>
                    </div>
                    <div class="kuang5">
                        <div id="gdtb" style="height: 360px">
                            <asp:GridView ID="gvResult" runat="server" Width="100%" CssClass="tab1"
                                HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="spec"
                                AllowPaging="True" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" AutoGenerateColumns="true" EmptyDataText="" PageSize="15"
                                OnPageIndexChanging="gvResult_Page" OnRowDataBound="gvResult_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="checkAll" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="toSelect" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                        <tr class="tabbt">
                                            <td>业务类型
                                            </td>
                                            <td>同步失败原因
                                            </td>
                                            <td>银行卡号
                                            </td>
                                            <td>操作员工
                                            </td>
                                            <td>操作员工部门
                                            </td>
                                            <td>姓名
                                            </td>
                                            <td>证件类型
                                            </td>
                                            <td>证件号码
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <PagerStyle CssClass="page" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="cust_tabbox cust_tabbox_spec ">
                    <div style="float: right; margin: 5px">
                        <asp:RadioButtonList ID="radioType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Enabled="false">作废同步</asp:ListItem>
                            <asp:ListItem Value="2" Enabled="false">取消作废</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">重新同步</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="button1" OnClick="btnSubmit_Click" OnClientClick="return confirm('确认提交?');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>

