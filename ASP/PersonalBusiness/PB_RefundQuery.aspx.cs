﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using TM;
using TDO.ResourceManager;
using TDO.BalanceChannel;
using Common;

public partial class ASP_PersonalBusiness_PB_RefundQuery : Master.Master
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            //POS库存查询表头
            initPosList();
        }
    }

    //表头
    private void initPosList()
    {
        DataTable posData = new DataTable();
        DataView posDataView = new DataView(posData);

        gvResult.DataSource = posDataView;
        gvResult.DataBind();
    }

    public void lvwPos_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //输入判断
        if (!ValidateForPosQuery())
            return;

        DataTable data = SPHelper.callPBQuery(context, "RefundQuery", txtID.Text.Trim(), txtCardNo.Text.Trim(), SupplyBeginDate.Text.Trim(),
                SupplyEndDate.Text.Trim(), RefundBeginDate.Text.Trim(), RefundEndDate.Text.Trim());

        gvResult.DataSource = data;
        gvResult.DataBind();
    }


    //输入验证
    private bool ValidateForPosQuery()
    {

        string strCardNo = txtCardNo.Text.Trim();
        if (strCardNo != "")
        {
            if (!Validation.isNum(strCardNo))
                context.AddError("卡号应为16位数字", txtCardNo);

            if (Validation.strLen(strCardNo) > 16)
                context.AddError("卡号应为16位数字", txtCardNo);
        }


        string strID = txtID.Text.Trim();
        if (strID != "")
        {
            if (!Validation.isNum(strID))
                context.AddError("充值ID应为18位数字", txtID);

            if (Validation.strLen(strID) > 18)
                context.AddError("充值ID应为18位数字", txtID);
        }

        Validation valid = new Validation(context);
        DateTime? dateEff = null, dateExp = null;
        //对充值起始日期进行非空和日期格式检验

        if (!string.IsNullOrEmpty(SupplyBeginDate.Text.Trim()))
            dateEff = valid.beDate(SupplyBeginDate, "A094780050：充值开始日期格式不是yyyyMMdd");

        //对充值终止日期进行非空和日期格式检验
        if (!string.IsNullOrEmpty(SupplyEndDate.Text.Trim()))
            dateExp = valid.beDate(SupplyEndDate, "A094780051：充值结束日期格式不是yyyyMMdd");

        //起始日期不能大于终止日期
        if (dateEff != null && dateExp != null)
        {
            valid.check(dateEff.Value.CompareTo(dateExp.Value) <= 0, "A094780052：充值开始日期不能大于充值结束日期");
        }

        dateEff = null; dateExp = null;

        //对退款起始日期进行非空和日期格式检验

        if (!string.IsNullOrEmpty(RefundBeginDate.Text.Trim()))
            dateEff = valid.beDate(RefundBeginDate, "A094780053：退款开始日期格式不是yyyyMMdd");

        //对退款终止日期进行非空和日期格式检验
        if (!string.IsNullOrEmpty(RefundEndDate.Text.Trim()))
            dateExp = valid.beDate(RefundEndDate, "A094780051：退款结束日期格式不是yyyyMMdd");

        //起始日期不能大于终止日期
        if (dateEff != null && dateExp != null)
        {
            valid.check(dateEff.Value.CompareTo(dateExp.Value) <= 0, "A094780052：退款开始日期不能大于退款结束日期");
        }


        if (context.hasError())
            return false;
        else
            return true;
    }

}
