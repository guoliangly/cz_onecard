﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataExchange;
using TDO.UserManager;
using PDO.Financial;
using Master;
using TM;

public partial class ASP_PersonalBusiness_PB_ExceptionSync : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        //GridView绑定为空
        clearGridView();

        //标题Lable
        LabExText.Text = radioExType.SelectedItem.Text + "信息";
        btnSubmit.Enabled = false;

        if (HasOperPower("201008"))//全部网点主管
        {
            //初始化部门
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
            TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
            ControlDeal.SelectBoxFill(seldisDepart.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
            seldisDepart.SelectedValue = context.s_DepartID;
            InitStaffList(seldisStaff, context.s_DepartID);
            seldisStaff.SelectedValue = context.s_UserID;
        }
        else if (HasOperPower("201007"))//网点主管
        {
            seldisDepart.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
            seldisDepart.SelectedValue = context.s_DepartID;
            seldisDepart.Enabled = false;

            InitStaffList(seldisStaff, context.s_DepartID);
            seldisStaff.SelectedValue = context.s_UserID;
        }
        else//网点营业员
        {
            seldisDepart.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
            seldisDepart.SelectedValue = context.s_DepartID;
            seldisDepart.Enabled = false;

            seldisStaff.Items.Add(new ListItem(context.s_UserID + ":" + context.s_UserName, context.s_UserID));
            seldisStaff.SelectedValue = context.s_UserID;
            seldisStaff.Enabled = false;
        }
    }

    private bool HasOperPower(string powerCode)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tmTMTableModule.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    //初始化部门
    private void InitDepartList(DropDownList selDepart)
    {
        selDepart.Items.Add(new ListItem("---请选择---", ""));

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TD_M_INSIDEDEPARTITEM";
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count == 0)
            return;

        for (int i = 0; i < data.Rows.Count; i++)
        {
            selDepart.Items.Add(new ListItem(data.Rows[i]["DEPARTNAME"].ToString(), data.Rows[i]["DEPARTNO"].ToString()));
        }
        selDepart.SelectedValue = context.s_DepartID;
    }

    private void InitStaffList(DropDownList selStaff, string deptNo)
    {
        if (deptNo == "")
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
            selStaff.SelectedValue = context.s_UserID;
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }

    protected void selDisDept_Changed(object sender, EventArgs e)
    {
        InitStaffList(seldisStaff, seldisDepart.SelectedValue);
    }

    // 分页    
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        gvResult.DataSource = CreateQueryDataSource();
        gvResult.DataBind();
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[12].Visible = false;    //TRADEID
            e.Row.Cells[13].Visible = false;    //交易类型
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[7].Text == "港澳通行证")
            {
                e.Row.Cells[7].Text = "港澳台通行证";
            }
            e.Row.Cells[11].Text = Convert.ToDateTime(e.Row.Cells[11].Text).ToString("yyyy-MM-dd HH:mm:ss");    //同步时间
        }
    }

    //全选信息记录

    protected void CheckAll(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        GridView gv = new GridView();
        gv = gvResult;
        foreach (GridViewRow gvr in gv.Rows)
        {
            if (!gvr.Cells[1].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("toSelect");
            ch.Checked = cbx.Checked;
        }
    }

    //查询单位编号
    //protected void queryDept(object sender, EventArgs e)
    //{
    //    //模糊查询单位名称，并在列表中赋值
    //    string name = txtDeptName.Text.Trim();
    //    if (name == "")
    //    {
    //        selDeptName.Items.Clear();
    //        return;
    //    }
    //    DataTable data = SPHelper.callQuery("SP_RC_Query", context, "QureyDept", name);
    //    selDeptName.Items.Clear();
    //    if (data.Rows.Count > 0)
    //    {
    //        selDeptName.Items.Add(new ListItem("---------请选择---------", ""));
    //    }
    //    foreach (DataRow dr in data.Rows)
    //    {
    //        selDeptName.Items.Add(new ListItem(dr["CORPNO"].ToString() + ":" + dr["CORPNAME"].ToString(), dr["CORPNO"].ToString()));
    //    }
    //}

    //单位下拉框文本改变事件
    //protected void selDept_Change(object sender, EventArgs e)
    //{
    //    //单位名称反向赋值
    //    if (selDeptName.SelectedItem.Value != "")
    //        txtDeptName.Text = selDeptName.SelectedItem.Text.Split(':')[1];
    //}

    public void clearGridView()
    {
        //清空列表
        gvResult.DataSource = new DataView();
        gvResult.DataBind();
    }
    //查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //清空列表
        clearGridView();

        //判断查询条件        
        if (!QueryValidation()) return;

        LabExText.Text = radioExType.SelectedItem.Text + "信息";

        if (radioExType.SelectedValue == "0")       //同步未成功
        {
            radioType.SelectedIndex = 2;
            radioType.Items[0].Enabled = true; //作废同步按钮启用
            radioType.Items[1].Enabled = false;//取消作废按钮禁用
            radioType.Items[2].Enabled = true;//重新同步按钮启用            
        }
        else if (radioExType.SelectedValue == "3")   //已作废
        {
            radioType.SelectedIndex = 1;
            radioType.Items[0].Enabled = false; //作废同步按钮禁用
            radioType.Items[1].Enabled = true;//取消作废按钮启用
            radioType.Items[2].Enabled = false;//重新同步按钮禁用            
        }
        //绑定GridView
        DataView dv = (DataView)CreateQueryDataSource();
        if (dv.Count == 0)
        {
            context.AddError("查询结果为空");
            btnSubmit.Enabled = false;
        }
        else
        {
            gvResult.DataSource = dv;
            gvResult.DataBind();
            btnSubmit.Enabled = true;
        }

    }

    // 查询存储过程    
    public ICollection CreateQueryDataSource()
    {
        DataTable data = new DataTable();
        DataView dataView = new DataView();


        data = SPHelper.callQuery("SP_PB_QUERY", context, "QureyExceptionSync", ddlTradeType.SelectedValue, radioExType.SelectedValue, seldisDepart.SelectedValue, seldisStaff.SelectedValue);
        dataView = new DataView(data);
        return dataView;
    }

    private bool QueryValidation()
    {
        ////姓名
        //if (txtName.Text.Trim() != "")
        //{
        //    if (Validation.strLen(txtName.Text.Trim()) > 60)
        //    {
        //        context.AddError("姓名字符长度不能超过30位", txtName);
        //    }
        //}
        ////证件号码
        //if (txtPaperNo.Text.Trim() != "")
        //{
        //    if (Validation.strLen(txtPaperNo.Text.Trim()) > 20)
        //    {
        //        context.AddError("证件号码长度不能超过20位", txtPaperNo);
        //    }
        //    else if (!Validation.isCharNum(txtPaperNo.Text.Trim()))
        //    {
        //        context.AddError("证件号码不为英数", txtPaperNo);
        //    }

        //    if (ddlPaperType.SelectedValue == "01" && Validation.strLen(txtPaperNo.Text.Trim()) < 15)
        //    {
        //        context.AddError("身份证件号码长度不能少于15位", txtPaperNo);
        //    }
        //}

        //单位名称
        //if (txtDeptName.Text.Trim() != "")
        //{
        //    if (Validation.strLen(txtDeptName.Text.Trim()) > 100)
        //    {
        //        context.AddError("单位字符长度不能超过50位", txtDeptName);
        //    }
        //}

        ////查询条件个数
        //if (txtName.Text.Trim() == "" && txtPaperNo.Text.Trim() == "" && ddlTradeType.SelectedValue == "")
        //{
        //    context.AddError("查询条件不能全部为空");
        //}
        return !(context.hasError());
    }
    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (radioType.SelectedValue == "0") //重新同步
        {
            ReSync();
        }
        else if (radioType.SelectedValue == "1") //作废同步
        {
            RollbackSync();
        }
        else if (radioType.SelectedValue == "2") //取消作废同步
        {
            CancelRollbackSync();
        }
        btnSubmit.Enabled = false;
        gvResult.DataSource = CreateQueryDataSource();
        gvResult.DataBind();
    }

    // 重新同步
    protected void ReSync()
    {
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gvResult.Rows[i].FindControl("toSelect");
            if (cb != null && cb.Checked)
            {
                string tradeid = gvResult.Rows[i].Cells[12].Text.Trim();
                string cardno = gvResult.Rows[i].Cells[10].Text.Trim();
                string bankno = gvResult.Rows[i].Cells[9].Text.Trim();
                string statuscode = "";
                //重新调用接口
                string batchID = tradeid;
                string[] parm = new string[1];
                parm[0] = batchID;
                DataTable data = SPHelper.callQuery("SP_PB_QUERY", context, "QureyDataCollectSync", parm);

                List<SyncRequest> syncRequest;
                DataExchangeHelp.ParseFormDataTable(data, out syncRequest);
                List<SyncRequest> syncResponse;
                bool succ = false;
                if (syncRequest.Count != 0)
                {
                    succ = DataExchangeHelp.Sync(syncRequest, out syncResponse);
                    if (!succ)
                    {
                        context.AddError(syncResponse[0].SyncErrInfo);
                    }
                }
                if (context.hasError())
                {
                    statuscode = "0";
                }
                else
                {
                    statuscode = "1";
                }

                // 更新同步台帐子表
                context.DBOpen("StorePro");
                context.AddField("P_TRADEID").Value = tradeid;
                context.AddField("P_CSN").Value = cardno;
                context.AddField("P_STATE").Value = statuscode;
                context.AddField("P_ERROR").Value = GetErrorMsg();

                bool ok = context.ExecuteSP("SP_PB_UPDATESYNCSTATE");
                if (ok)
                {
                    context.AddMessage("更新库同步状态成功");
                    //记录异常同步台账子表
                    RecordReSync(tradeid, cardno);
                }
            }
        }
    }

    //记录异常同步台账子表
    private void RecordReSync(string tradeid, string cardno)
    {
        context.DBOpen("StorePro");
        context.AddField("P_TRADEID").Value = tradeid;
        context.AddField("P_CARDNO").Value = cardno;
        bool ok = false;
        ok = context.ExecuteSP("SP_PB_INSERTRESYNC");
        if (ok)
        {
            context.AddMessage(string.Format("记录异常同步台账子表成功"));
        }
    }

    private string GetErrorMsg()
    {
        string msg = "";
        ArrayList errorMessages = context.ErrorMessage;
        for (int index = 0; index < errorMessages.Count; index++)
        {
            if (index > 0)
                msg += "|";

            String error = errorMessages[index].ToString();
            int start = error.IndexOf(":");
            if (start > 0)
            {
                error = error.Substring(start + 1, error.Length - start - 1);
            }

            msg += error;
        }

        return msg;
    }

    // 取消作废同步
    protected void CancelRollbackSync()
    {
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gvResult.Rows[i].FindControl("toSelect");
            if (cb != null && cb.Checked)
            {
                string tradeid = gvResult.Rows[i].Cells[12].Text.Trim();
                string cardno = gvResult.Rows[i].Cells[10].Text.Trim();
                string bankno = gvResult.Rows[i].Cells[9].Text.Trim();


                bool ok = false;
                // 更新同步台帐子表
                context.DBOpen("StorePro");
                context.AddField("P_TRADEID").Value = tradeid;
                context.AddField("P_CSN").Value = cardno;
                context.AddField("P_STATE").Value = "0";
                ok = context.ExecuteSP("SP_PB_UPDATESYNCSTATE");
                if (ok)
                {
                    context.AddMessage(string.Format("异常同步信息取消作废成功"));
                }
            }
        }
    }

    // 作废同步
    protected void RollbackSync()
    {
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gvResult.Rows[i].FindControl("toSelect");
            if (cb != null && cb.Checked)
            {
                string tradeid = gvResult.Rows[i].Cells[12].Text.Trim();
                string cardno = gvResult.Rows[i].Cells[10].Text.Trim();
                string bankno = gvResult.Rows[i].Cells[9].Text.Trim();

                bool ok = false;
                // 更新同步台帐子表
                context.DBOpen("StorePro");
                context.AddField("P_TRADEID").Value = tradeid;
                context.AddField("P_CSN").Value = cardno;
                context.AddField("P_STATE").Value = "3";
                ok = context.ExecuteSP("SP_PB_UPDATESYNCSTATE");

                if (ok)
                {
                    context.AddMessage(string.Format("异常同步信息作废成功"));
                }
            }
        }
    }

    //记录异常同步台账子表
    private void RecordReSync(string tradeid, string cardno, string syncSysCode)
    {
        context.DBOpen("StorePro");
        context.AddField("P_TRADEID").Value = tradeid;
        context.AddField("P_CARDNO").Value = cardno;
        context.ExecuteSP("SP_RC_INSERTRESYNC");
    }

    //证件类型编码转换
    public string paperTypeCodeTrad(string paperTypeCode)
    {
        if (paperTypeCode == "01")
        {
            return "00";
        }
        else if (paperTypeCode == "02")
        {
            return "05";
        }
        else if (paperTypeCode == "03")
        {
            return "08";
        }
        else if (paperTypeCode == "99")
        {
            return "99";   //其他
        }
        else
        {
            return "";
        }
    }

    //protected void txtReadPaper_Click(object sender, EventArgs e)
    //{
    //    ScriptManager1.SetFocus(btnQuery);
    //}
}