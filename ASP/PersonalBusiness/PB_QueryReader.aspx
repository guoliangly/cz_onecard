﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_QueryReader.aspx.cs" Inherits="ASP_PersonalBusiness_PB_QueryReader" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>充付器查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
        充付器模块->充付器查询</div>
    <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" runat="server" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
            <asp:BulletedList ID="bulMsgShow" runat="server">
            </asp:BulletedList>
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <asp:HiddenField ID="hidChangeType" runat="server" />
            <div class="con">
                <div class="card">
                 查询
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td width="12%" align="right">
                                充付器序列号:
                            </td>
                            <td width="12%">
                                <asp:TextBox ID="txtReaderNo" CssClass="inputmid" runat="server" MaxLength="16" />
                            </td>
                            <td width="12%" align="right">
                                状态:
                            </td>
                            <td width="12%" align="left">
                                <asp:DropDownList runat="server" ID="ddlReaderState" CssClass="inputmid">
                                    <asp:ListItem Text="---请选择---" Value=""></asp:ListItem>
                                    <asp:ListItem Text="生成" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="入库" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="出库" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="售出" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="更换回收" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="退货回收" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="12%" align="right">
                                类型:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlReaderType" CssClass="inputmid"></asp:DropDownList>
                            </td>
                             <td width="12%" align="right">
                                厂商:
                            </td>
                            <td width="12%" align="left">
                                <asp:DropDownList runat="server" ID="ddlReaderManu" CssClass="inputmid"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                           
                            <td width="12%">
                                <div align="right">
                                    起止出售日期:</div>
                            </td>
                            <td width="36%" colspan="3">
                                <ajaxToolkit:CalendarExtender ID="BeginCalendar" runat="server" TargetControlID="txtStartDate"
                                    Format="yyyyMMdd" PopupPosition="BottomLeft" />
                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="inputmid" MaxLength="8"></asp:TextBox>
                                -
                                 <asp:TextBox ID="txtEndDate" runat="server" CssClass="inputmid" MaxLength="8"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEndDate"
                                    Format="yyyyMMdd" PopupPosition="BottomLeft" />
                            </td>
                            <td align="right" width="12%"></td>
                            <td align="right" width="12%"></td>
                            <td align="right" width="12%"></td>
                            <td width="12%" align="left">
                               <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="jieguo">
                    查询结果
                </div>
                <div class="kuang5">
                    <div id="gvUseCard" style="height: 300px; overflow: auto; display: block">
                        <asp:GridView ID="gvResult" runat="server" Width="98%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                            FooterStyle-CssClass="tabcon" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="false" PageSize="20" AllowPaging="true"
                            OnPageIndexChanging="gvResult_Page"
                            DataKeyNames="serialnumber" OnRowCommand="gvResult_RowCommand">
                            <Columns>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>序号</HeaderTemplate>
                                    <ItemTemplate>
                                    <%#Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="serialnumber" HeaderText="序列号" />
                                <asp:BoundField DataField="state" HeaderText="状态" />
                                <asp:BoundField DataField="manuname" HeaderText="厂商" />
                                <asp:BoundField DataField="typename" HeaderText="类型" />
                                <asp:BoundField DataField="year" HeaderText="年份" />
                                <asp:BoundField DataField="platformname" HeaderText="应用平台" />
                                <asp:TemplateField>
                                    <HeaderTemplate>操作</HeaderTemplate>
                                    <ItemTemplate>
                                     <asp:LinkButton Text="查看用户信息" CommandName="Detail" runat="server" CommandArgument='<%#Eval("serialnumber") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                <div style="padding:5px 0 0 0; font-weight:bold;">
                <span style="margin-left:50px">生成:</span><label runat="server" id="labCreate"></label>
                <span style="margin-left:30px">入库:</span><label runat="server" id="labStockIn"></label>
                <span style="margin-left:30px">出库:</span><label runat="server" id="labStockOut"></label>
                <span style="margin-left:30px">售出:</span><label runat="server" id="labSale"></label>
                <span style="margin-left:30px">更换回收:</span><label runat="server" id="labChangeRecovery"></label>
                <span style="margin-left:30px">退货回收:</span><label runat="server" id="labReturnRecovery"></label>
                <span style="margin-left:30px">总计:</span><label runat="server" id="labSum"></label>
                </div>
                </div>
                <div class="pip">            
                <span>用户信息</span>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="12%">
                                <div align="right">
                                    用户姓名:</div>
                            </td>
                            <td width="12%">
                                <asp:Label ID="CustName" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    出生日期:</div>
                            </td>
                            <td width="12%">
                                <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    证件类型:</div>
                            </td>
                            <td width="12%">
                                <asp:DropDownList runat="server" ID="ddlPaperType" Enabled="false"></asp:DropDownList>
                                <%--<asp:Label ID="Papertype" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td width="12%">
                                <div align="right">
                                    证件号码:</div>
                            </td>
                            <td width="12%" colspan="3">
                                <asp:Label ID="Paperno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    用户性别:</div>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlSex" Enabled="false"></asp:DropDownList>
                                <%--<asp:Label ID="Custsex" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td>
                                <div align="right">
                                    联系电话:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custphone" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    邮政编码:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custpost" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    联系地址:</div>
                            </td>
                            <td colspan="3">
                                <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                电子邮件:
                            </td>
                            <td>
                                <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label>
                            </td>
                            <td valign="top">
                                <div align="right">
                                    备注 :</div>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="Remark" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
