﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using TM;
using TDO.BusinessCode;
using Common;
using PDO.PersonalBusiness;
using TDO.CardManager;
using TDO.ResourceManager;

public partial class ASP_PersonalBusiness_PB_ChangeUserInfo : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            btnReadCard.Attributes["onclick"] = "javascript:readCard()";
            LabAsn.Attributes["readonly"] = "true";
            LabCardtype.Attributes["readonly"] = "true";
            sDate.Attributes["readonly"] = "true";
            eDate.Attributes["readonly"] = "true";
            cMoney.Attributes["readonly"] = "true";

            ASHelper.initSexList(selCustsex);
            initLoad(sender, e);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据，放入下拉列表中
        ASHelper.initPaperTypeList(context, selPapertype);

        //验证当前负责员工是否是网点负责人
        string sql = "select count(*) from TD_M_ROLEPOWER rp where rp.powercode='201007' and rp.powertype='2' " +
             "and rp.roleno in( select ROLENO from TD_M_INSIDESTAFFROLE ti where ti.Staffno='" + context.s_UserID + "')";

        if (isManager(sql))
        {
            hidIsManager.Value = "1";
        }
        else
        {
            hidIsManager.Value = "0";
        }
    }

    private Boolean ChangeUserInfoDBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001004113", txtCardno);
        else
        {
            if (Validation.strLen(txtCardno.Text.Trim()) != 16)
                context.AddError("A001004114", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001004115", txtCardno);
        }

        return !(context.hasError());

    }

    //对售卡用户信息进行检验
    private Boolean SaleInfoValidation()
    {
        //当选择非记名卡时
        if (Signtype.Checked == false)
        {
            //对用户姓名进行长度检验
            if (txtCusname.Text.Trim() != "")
                if (Validation.strLen(txtCusname.Text.Trim()) > 50)
                    context.AddError("A001001113", txtCusname);

            //对出生日期进行日期格式检验
            String cDate = txtCustbirth.Text.Trim();
            if (cDate != "")
                if (!Validation.isDate(txtCustbirth.Text.Trim()))
                    context.AddError("A001001115", txtCustbirth);

            //对联系电话进行长度检验
            if (txtCustphone.Text.Trim() != "")
                if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
                    context.AddError("A001001126", txtCustphone);
                else if (!Validation.isNum(txtCustphone.Text.Trim()))
                    context.AddError("A001001125", txtCustphone);

            //对证件号码进行长度、英数字检验
            if (txtCustpaperno.Text.Trim() != "")
                if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
                    context.AddError("A001001122", txtCustpaperno);
                else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
                    context.AddError("A001001123", txtCustpaperno);

            //对邮政编码进行长度、数字检验
            if (txtCustpost.Text.Trim() != "")
                if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                    context.AddError("A001001120", txtCustpost);
                else if (!Validation.isNum(txtCustpost.Text.Trim()))
                    context.AddError("A001001119", txtCustpost);

            //对联系地址进行长度检验
            if (txtCustaddr.Text.Trim() != "")
                if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                    context.AddError("A001001128", txtCustaddr);

            //对电子邮件进行格式检验
            if (txtEmail.Text.Trim() != "")
                new Validation(context).isEMail(txtEmail);

            //对备注进行长度检验
            if (txtRemark.Text.Trim() != "")
                if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                    context.AddError("A001001129", txtRemark);

        }

        //当选择记名卡时
        if (Signtype.Checked == true)
        {
            //对用户姓名进行非空、长度检验
            if (txtCusname.Text.Trim() == "")
                context.AddError("A001001111", txtCusname);
            else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
                context.AddError("A001001113", txtCusname);

            //对用户性别进行非空检验
            if (selCustsex.SelectedValue == "")
                context.AddError("A001001116", selCustsex);

            //对证件类型进行非空检验
            if (selPapertype.SelectedValue == "")
                context.AddError("A001001117", selPapertype);

            //对出生日期进行非空、日期格式检验
            String cDate = txtCustbirth.Text.Trim();
            if (cDate == "")
                context.AddError("A001001114", txtCustbirth);
            else if (!Validation.isDate(txtCustbirth.Text.Trim()))
                context.AddError("A001001115", txtCustbirth);

            //对联系电话进行非空、长度、数字检验
            if (txtCustphone.Text.Trim() == "")
                context.AddError("A001001124", txtCustphone);
            else if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
                context.AddError("A001001126", txtCustphone);
            else if (!Validation.isNum(txtCustphone.Text.Trim()))
                context.AddError("A001001125", txtCustphone);

            //对证件号码进行非空、长度、英数字检验
            if (txtCustpaperno.Text.Trim() == "")
                context.AddError("A001001121", txtCustpaperno);
            else if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
                context.AddError("A001001122", txtCustpaperno);
            else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
                context.AddError("A001001123", txtCustpaperno);

            //对邮政编码进行非空、长度、数字检验
            if (txtCustpost.Text.Trim() != "")
            {
                //context.AddError("A001001118", txtCustpost);
                if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                    context.AddError("A001001120", txtCustpost);
                else if (!Validation.isNum(txtCustpost.Text.Trim()))
                    context.AddError("A001001119", txtCustpost);
            }

            //对联系地址进行非空、长度检验
            if (txtCustaddr.Text.Trim() == "")
                context.AddError("A001001127", txtCustaddr);
            else if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);

            //对备注进行长度检验
            if (txtRemark.Text.Trim() != "")
                if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                    context.AddError("A001001129", txtRemark);

            //对电子邮件进行格式检验
            if (txtEmail.Text.Trim() != "")
                new Validation(context).isEMail(txtEmail);
        }
        return !(context.hasError());
    }

    private Boolean ChangeUserInfoValidation()
    {
        //对用户姓名进行非空、长度检验
        if (txtCusname.Text.Trim() == "")
            context.AddError("A001001111", txtCusname);
        else if (Validation.strLen(txtCusname.Text.Trim()) > 50)
            context.AddError("A001001113", txtCusname);

        //对出生日期进行日期格式检验
        String cDate = txtCustbirth.Text.Trim();
        if (cDate != "")
            if (!Validation.isDate(txtCustbirth.Text.Trim()))
                context.AddError("A001001115", txtCustbirth);

        //对联系电话进行非空、长度、数字检验
        if (txtCustphone.Text.Trim() == "")
            context.AddError("A001001124", txtCustphone);
        else if (Validation.strLen(txtCustphone.Text.Trim()) > 20)
            context.AddError("A001001126", txtCustphone);
        else if (!Validation.isNum(txtCustphone.Text.Trim()))
            context.AddError("A001001125", txtCustphone);

        //对证件号码进行非空、长度、英数字检验
        if (txtCustpaperno.Text.Trim() == "")
            context.AddError("A001001121", txtCustpaperno);
        else if (!Validation.isCharNum(txtCustpaperno.Text.Trim()))
            context.AddError("A001001122", txtCustpaperno);
        else if (Validation.strLen(txtCustpaperno.Text.Trim()) > 20)
            context.AddError("A001001123", txtCustpaperno);

        //对邮政编码进行长度、数字检验
        if (txtCustpost.Text.Trim() != "")
            if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                context.AddError("A001001120", txtCustpost);
            else if (!Validation.isNum(txtCustpost.Text.Trim()))
                context.AddError("A001001119", txtCustpost);

        //对联系地址进行长度检验
        if (txtCustaddr.Text.Trim() != "")
            if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);

        //对电子邮件进行格式检验
        if (txtEmail.Text.Trim() != "")
            new Validation(context).isEMail(txtEmail);

        //对备注进行长度检验
        if (txtRemark.Text.Trim() != "")
            if (Validation.strLen(txtRemark.Text.Trim()) > 50)
                context.AddError("A001001129", txtRemark);

        return !(context.hasError());

    }

  

    protected void btnChangeUserInfoDBread_Click(object sender, EventArgs e)
    {
        //因卡片制作过程中出错，导致卡面显示卡号异常,增加提示
        string expstr = CommonHelper.IsExpCardno(txtCardno.Text.Trim());
        if (expstr != "")
        {
            context.AddError(expstr);
            return;
        }
        //对输入卡号进行检验
        if (!ChangeUserInfoDBreadValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验
        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }

            //从卡资料表(TF_F_CARDREC)中读取数据

            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //移动龙城通卡不允许做卡资料修改wdx20120702
            if (txtCardno.Text.StartsWith("91501101"))
            {
                context.AddError("移动龙城通卡不允许修改资料");
                return;
            }
            //判断是否是市民卡
            if (txtCardno.Text.StartsWith("915001"))
            {
                context.AddError("不能修改市民卡资料");
                return;
            }

            //if (ddoTF_F_CARDRECOut != null && ddoTF_F_CARDRECOut.CARDTYPECODE == "01")
            //{
            //    context.AddError("不能修改市民卡资料");
            //    return;
            //}

            //从IC卡电子钱包帐户表(TF_F_CARDEWALLETACC)中读取数据

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

            //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据

            TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
            ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTF_F_CARDRECOut.CARDTYPECODE;

            TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "");

            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            //给页面各显示项附值

            Signtype.Checked = ddoTF_F_CARDRECOut.CUSTRECTYPECODE == "1" ? true : false;
            LabAsn.Text = ddoTF_F_CARDRECOut.ASN;
            LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
            sDate.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");

            String Vdate = ddoTF_F_CARDRECOut.VALIDENDDATE;
            eDate.Text = Vdate.Substring(0, 4) + "-" + Vdate.Substring(4, 2) + "-" + Vdate.Substring(6, 2);

            Double cardMoney = Convert.ToDouble(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY);
            cMoney.Text = (cardMoney / 100).ToString("0.00");
            txtCusname.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            selCustsex.SelectedValue = ddoTF_F_CUSTOMERRECOut.CUSTSEX;
            //出生日期赋值
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    txtCustbirth.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else txtCustbirth.Text = Bdate;
            }
            else txtCustbirth.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            //证件类型赋值
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                selPapertype.SelectedValue = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
            }
            else selPapertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;


            txtCustpaperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            txtCustaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            txtCustpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            txtCustphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            txtRemark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtCardno.Text.Trim());

            btnIsManager.Enabled = true;
        }
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {

        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验
        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //移动龙城通卡不允许做卡资料修改wdx20120702
            if (txtCardno.Text.StartsWith("91501101"))
            {
                context.AddError("移动龙城通卡不允许修改资料");
                return;
            }
            //检查是否是市民卡


            if (txtCardno.Text.StartsWith("915001"))
            {
                context.AddError("不能修改市民卡资料");
                return;
            }
            //if (hiddenLabCardtype.Value == "01")
            //{
            //    context.AddError("不能修改市民卡资料");
            //    return;
            //}

            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }

            //从卡资料表(TF_F_CARDREC)中读取数据


            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);


            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "");

            //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据

            TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
            ddoTD_M_CARDTYPEIn.CARDTYPECODE = hiddenLabCardtype.Value;

            TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

            

            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            //给页面各显示项附值
            Signtype.Checked = ddoTF_F_CARDRECOut.CUSTRECTYPECODE == "1" ? true : false;
            LabAsn.Text = hiddenAsn.Value.Substring(4, 16); 
            LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
            sDate.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
            eDate.Text = hiddeneDate.Value.Substring(0, 4) + "-" + hiddeneDate.Value.Substring(4, 2) + "-" + hiddeneDate.Value.Substring(6, 2);
            cMoney.Text = ((Convert.ToDouble(hiddencMoney.Value)) / 100).ToString("0.00");

            txtCusname.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            selCustsex.SelectedValue = ddoTF_F_CUSTOMERRECOut.CUSTSEX;
            //出生日期赋值
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    txtCustbirth.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else txtCustbirth.Text = Bdate;
            }
            else txtCustbirth.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            //证件类型赋值
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                selPapertype.SelectedValue = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
            }
            else selPapertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            txtCustpaperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            txtCustaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            txtCustpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            txtCustphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            txtRemark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtCardno.Text.Trim());
            btnIsManager.Enabled = true;
        }
    }

    private bool isManager(string sql)
    {
        //验证是否是网点负责人
        TMTableModule tmTMTableModule = new TMTableModule();
        DataTable dt = tmTMTableModule.selByPKDataTable(context, sql, 0);
        Object obj = dt.Rows[0].ItemArray[0];
        if (obj == DBNull.Value)
            return false;
        return Convert.ToInt32(obj) > 0;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "submit")
        {
            //验证网点负责人
            string sql = "select count(*) from TD_M_ROLEPOWER rp where rp.powercode='201007' and rp.powertype='2' "+
                         "and rp.roleno in( select ROLENO from TD_M_INSIDESTAFFROLE ti,TD_M_INSIDESTAFF tf "+
                         "where ti.Staffno=tf.staffno and opercardno='" + hiddenCheck.Value + "')";
            if (!isManager(sql))
            {
                context.AddError("A001010109:审核员工不是网点负责人");
                return;
            }
            btnChangeUserInfo_Click(sender, e);
        }

        hidWarning.Value = "";
    }

    //protected void btnIsManager_Click(object sender, EventArgs e)
    //{
    //    string sql = "select count(*) from TD_M_ROLEPOWER rp where rp.powercode='201007' and rp.powertype='2' "+
    //                 "and rp.roleno in( select ROLENO from TD_M_INSIDESTAFFROLE ti where ti.Staffno='" + context.s_UserID +"')";
    //    //验证当前负责员工是否是网点负责人
    //    if (isManager(sql))
    //    {
    //        btnChangeUserInfo_Click(sender, e);
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ChangeUserInfoScript", "changeUserInfoWarnCheck();", true);
    //    }
    //}

    protected void btnChangeUserInfo_Click(object sender, EventArgs e)
    {

        if (txtCardno.Text.Trim().Substring(0, 8) == "91501103")
        {
            context.AddError("此卡为江南农商行龙城通卡，不能修改资料");
            return;
        }

        //对输入项进行检验

        if (!SaleInfoValidation())
            return;

        //移动龙城通卡不允许修改资料wdx20120702
        if (txtCardno.Text.StartsWith("91501101"))
        {
            context.AddError("移动龙城通卡不允许修改资料");
            return;
        }

        SP_PB_ChangeUserInfoPDO pdo = new SP_PB_ChangeUserInfoPDO();
        //存储过程赋值
        pdo.CARDNO = txtCardno.Text.Trim();
        pdo.CUSTNAME = txtCusname.Text.Trim();
        pdo.CUSTSEX = selCustsex.SelectedValue;

        if (txtCustbirth.Text.Trim() != "")
        {
            String[] arr = (txtCustbirth.Text.Trim()).Split('-');

            String cDate = arr[0] + arr[1] + arr[2];

            pdo.CUSTBIRTH = cDate;
        }

        else
        {
            pdo.CUSTBIRTH = txtCustbirth.Text.Trim();
        }


        pdo.PAPERTYPECODE = selPapertype.SelectedValue;
        pdo.PAPERNO = txtCustpaperno.Text.Trim();
        pdo.CUSTADDR = txtCustaddr.Text.Trim();
        pdo.CUSTPOST = txtCustpost.Text.Trim();
        pdo.CUSTPHONE = txtCustphone.Text.Trim();
        pdo.CUSTEMAIL = txtEmail.Text.Trim();
        pdo.REMARK = txtRemark.Text.Trim();
        pdo.TRADETYPECODE = "29";

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("M001011001");
            clearCustInfo(txtCusname, txtCustbirth, selPapertype, txtCustpaperno, selCustsex, txtCustphone,
                txtCustpost, txtCustaddr, txtEmail, txtRemark);
            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
            //initLoad(sender, e);
        }
    }


}
