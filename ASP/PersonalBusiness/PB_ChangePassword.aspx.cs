﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using TDO.CardManager;
using TM;
using Common;
using TDO.BusinessCode;
using PDO.GroupCard;
using PDO.PersonalBusiness;

/***************************************************************
 * 功能名: 个人业务_修改密码
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/4    liuh			初次开发 * 2011/04/01   liuhe           修改密码存储过程传参由txt控件取值改为hidden传值
 ****************************************************************/
public partial class ASP_PersonalBusiness_PB_ChangePassword : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;


        if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";

    }

    /// <summary>
    /// 读卡
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnChangeUserPassCardread_Click(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            //从企福通可充值账户表中读取数据



            //TF_F_CARDOFFERACCTDO ddoTF_F_CARDOFFERACCIn = new TF_F_CARDOFFERACCTDO();
            //ddoTF_F_CARDOFFERACCIn.CARDNO = txtCardno.Text;

            //TF_F_CARDOFFERACCTDO ddoTF_F_CARDOFFERACCInOut = (TF_F_CARDOFFERACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDOFFERACCIn, typeof(TF_F_CARDOFFERACCTDO), null, "TF_F_CARDOFFERACC", null);

            //if (ddoTF_F_CARDOFFERACCInOut != null)
            //{
            //    if (ddoTF_F_CARDOFFERACCInOut.USETAG == "1")
            //    {

            //        //从集团客户-企福通对应关系表中读取数据



            //        TD_GROUP_CARDTDO ddoTD_GROUP_CARDIn = new TD_GROUP_CARDTDO();
            //        ddoTD_GROUP_CARDIn.CARDNO = txtCardno.Text;

            //        TD_GROUP_CARDTDO ddoTD_GROUP_CARDOut = (TD_GROUP_CARDTDO)tmTMTableModule.selByPK(context, ddoTD_GROUP_CARDIn, typeof(TD_GROUP_CARDTDO), null, "TD_GROUP_CARD", null);

            //        if (ddoTD_GROUP_CARDOut != null)
            //        {
            //            //从集团客户资料表中读取数据




            //            TD_GROUP_CUSTOMERTDO ddoTD_GROUP_CUSTOMERIn = new TD_GROUP_CUSTOMERTDO();
            //            ddoTD_GROUP_CUSTOMERIn.CORPCODE = ddoTD_GROUP_CARDOut.CORPNO;
            //            ddoTD_GROUP_CUSTOMERIn.USETAG = "1";

            //            TD_GROUP_CUSTOMERTDO ddoTD_GROUP_CUSTOMEROut = (TD_GROUP_CUSTOMERTDO)tmTMTableModule.selByPK(context, ddoTD_GROUP_CUSTOMERIn, typeof(TD_GROUP_CUSTOMERTDO), null, "TD_GROUP_CUSTOMER_CHANGE", null);

            //            if (ddoTD_GROUP_CUSTOMEROut != null)
            //            {
            //                Corpname.Text = ddoTD_GROUP_CUSTOMEROut.CORPNAME;
            //            }
            //        }
            //    }
            //}

            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据



            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null, "TF_F_CUSTOMERREC", null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据



            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                Papertype.Text = "";
            }

            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                Custsex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                Custsex.Text = "女";
            else Custsex.Text = "";

            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = "";

            Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;


            btnChangeUserPass.Enabled = true;
        }
    }

    /// <summary>
    /// 修改密码前的验证
    /// </summary>
    /// <returns></returns>
    private Boolean ChangePasswordValidation()
    {
        //对原密码进行非空、长度、数字检验



        String strOPwd = txtOPwd.Text.Trim();

        if (strOPwd == "")
            context.AddError("A004111001", txtOPwd);
        else
        {
            int len = Validation.strLen(strOPwd);

            if (Validation.strLen(strOPwd) != 6)
                context.AddError("A004111002", txtOPwd);
            else if (!Validation.isNum(strOPwd))
                context.AddError("A004111003", txtOPwd);

        }

        //对新密码进行非空、长度、英数检验



        String strNPwd = txtNPwd.Text.Trim();
        if (strNPwd == "")
            context.AddError("A004111004", txtNPwd);
        else
        {
            int len = Validation.strLen(strNPwd);

            if (Validation.strLen(strNPwd) != 6)
                context.AddError("A004111005", txtNPwd);
            else if (!Validation.isNum(strNPwd))
                context.AddError("A004111006", txtNPwd);

        }

        //对新密码确认进行非空检验



        String strANPwd = txtANPwd.Text.Trim();

        if (strANPwd == "")
            context.AddError("A004111007", txtANPwd);

        //对原密码与新密码是否一样进行检验



        if (!context.hasError())
        {
            if (strOPwd == strNPwd)
                context.AddError("A004111008", txtANPwd);
        }

        //对新密码与新密码确认是否一样进行检验



        if (!context.hasError())
        {
            if (strNPwd != strANPwd)
                context.AddError("A004111009", txtANPwd);
        }

        if (context.hasError())
            return false;
        else
            return true;
    }

    /// <summary>
    /// 修改密码按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnChangeUserPass_Click(object sender, EventArgs e)
    {
        if (!ChangePasswordValidation())
            return;

        if (txtCardno.Text == "")
        {
            context.AddError("A005001100:未输入卡号");
            return;
        }
        this.hiddenOldPassword.Value = this.txtOPwd.Text;
        this.hiddenNewPassword.Value = this.txtNPwd.Text;

            if (cbxIsFirst.Checked)
            {
                this.hiddenOldPassword.Value = "642159";
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                " changePassword2();", true);
    }

    /// <summary>
    /// 写卡成功后执行


    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "writeSuccess")
        {
            context.ErrorMessage.Clear();

            context.SPOpen();
            context.AddField("p_cardNo").Value = txtCardno.Text;
            context.AddField("p_NEWPASSWD").Value = DealString.encrypPass(hiddenNewPassword.Value.Trim());

            bool ok = context.ExecuteSP("SP_PB_ChangePassword");

            if (ok)
            {


                AddMessage("M001111001");

                foreach (Control con in this.Page.Controls)
                {
                    ClearControl(con);
                }
                btnChangeUserPass.Enabled = false;
                hiddenOldPassword.Value = "";
                hiddenNewPassword.Value = "";
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("修改密码失败");
        }
    }

}
