﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_CardLossAndSupply.aspx.cs"
    Inherits="ASP_PersonalBusiness_PB_CardLossAndSupply" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>挂失卡补卡</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
    <div class="tb">
        个人业务->挂失卡补卡
    </div>
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <aspControls:PrintShouJu ID="ptnShouJu" runat="server" PrintArea="ptnShouJu1" />
            <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
            <asp:BulletedList ID="bulMsgShow" runat="server">
            </asp:BulletedList>
            <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
            <div class="con">
                <div class="card">
                    旧卡信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="10%">
                                <div align="right">
                                    旧卡卡号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtOCardno" CssClass="labeltext" MaxLength="16" runat="server"></asp:TextBox>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    旧卡质保金:</div>
                            </td>
                            <td width="12%">
                                <asp:TextBox ID="ODeposit" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    换卡类型:</div>
                            </td>
                            <td width="22%">
                                <asp:DropDownList ID="selReasonType" runat="server">
                                    <asp:ListItem Text="16:挂失补卡" Value="16" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="11%">
                                <%--  <asp:Button ID="btnReadCard" CssClass="button1" runat="server" Text="读卡" OnClientClick="return ReadCardInfo('txtOCardno')"
                                    OnClick="btnReadCard_Click" />--%>
                            </td>
                            <td width="14%">
                                <asp:Button ID="btnDBRead" CssClass="button1" runat="server" Text="读数据库"   OnClientClick="return warnCheck()" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    服务开始日:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="OsDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    旧卡余额:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="OldcMoney" CssClass="labeltext" runat="server" Text=""></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    卡片状态:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="RESSTATE" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    旧卡抵用金:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="ODepositBalance" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    旧卡卡费:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="OCardcost" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    开通功能:</div>
                            </td>
                            <td colspan="6">
                                <aspControls:OpenFunc ID="openFunc" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="base">
                    账户信息
                </div>
                <div class="kuang5">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div style="height: 60px" class="gdtb">
                                    <asp:GridView ID="gvAccount" runat="server" Width="100%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                                        AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast"
                                        PageSize="200" AllowPaging="True" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                        AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="账户类型" HeaderText="账户类型" />
                                            <asp:BoundField DataField="状态" HeaderText="状态" />
                                            <asp:BoundField DataField="余额" HeaderText="余额" />
                                            <asp:BoundField DataField="创建日期" HeaderText="创建日期" />
                                            <asp:BoundField DataField="当日消费限额" HeaderText="当日消费限额" />
                                            <asp:BoundField DataField="每笔消费限额" HeaderText="每笔消费限额" />
                                            <asp:BoundField DataField="总消费次数" HeaderText="总消费次数" />
                                            <asp:BoundField DataField="总充值次数" HeaderText="总充值次数" />
                                            <asp:BoundField DataField="总消费金额" HeaderText="总消费金额" />
                                            <asp:BoundField DataField="总充值金额" HeaderText="总充值金额" />
                                            <asp:BoundField DataField="最后消费时间" HeaderText="最后消费时间" />
                                            <asp:BoundField DataField="最后充值时间" HeaderText="最后充值时间" />
                                            <asp:BoundField DataField="所属集团" HeaderText="所属集团" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                                <tr class="tabbt">
                                                    <td>
                                                        账户类型
                                                    </td>
                                                    <td>
                                                        账户类别
                                                    </td>
                                                    <td>
                                                        状态
                                                    </td>
                                                    <td>
                                                        余额
                                                    </td>
                                                    <td>
                                                        创建日期
                                                    </td>
                                                    <td>
                                                        当日消费限额
                                                    </td>
                                                    <td>
                                                        每笔消费限额
                                                    </td>
                                                    <td>
                                                        总消费次数
                                                    </td>
                                                    <td>
                                                        总充值次数
                                                    </td>
                                                    <td>
                                                        总消费金额
                                                    </td>
                                                    <td>
                                                        总充值金额
                                                    </td>
                                                    <td>
                                                        最后消费时间
                                                    </td>
                                                    <td>
                                                        最后充值时间
                                                    </td>
                                                    <td>
                                                        所属集团
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="pip">
                    用户信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    用户姓名:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="CustName" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    出生日期:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="CustBirthday" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    证件类型:</div>
                            </td>
                            <td width="13%">
                                <asp:Label ID="Papertype" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    证件号码:</div>
                            </td>
                            <td width="25%">
                                <asp:Label ID="Paperno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    用户性别:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custsex" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    联系电话:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custphone" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    邮政编码:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custpost" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <div align="right">
                                    联系地址:</div>
                            </td>
                            <td>
                                <asp:Label ID="Custaddr" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                电子邮件:
                            </td>
                            <td>
                                <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label>
                            </td>
                            <td valign="top">
                                <div align="right">
                                    备注 :</div>
                            </td>
                            <asp:HiddenField ID="hidUserName" runat="server" />
                            <asp:HiddenField ID="hidUserSex" runat="server" />
                            <asp:HiddenField ID="hidUserPaperno" runat="server" />
                            <asp:HiddenField ID="hidUserPhone" runat="server" />
                            <td>
                                <asp:Label ID="Remark" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card">
                    新卡信息</div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                        <tr>
                            <td width="9%">
                                <div align="right">
                                    新卡卡号:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="txtNCardno" CssClass="labeltext" MaxLength="16" runat="server"></asp:TextBox>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    新卡费用:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="NDeposit" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td width="9%">
                                <div align="right">
                                    新卡类型:</div>
                            </td>
                            <td width="13%">
                                <asp:TextBox ID="LabCardtype" CssClass="labeltext" runat="server"></asp:TextBox>
                            </td>
                            <td width="9%">
                                &nbsp;
                            </td>
                            <td width="25%">
                                <asp:Button ID="btnReadNCard" CssClass="button1" Enabled="false" runat="server" Text="读新卡"
                                    OnClientClick="return ReadCardInfoAndTag('txtNCardno')" OnClick="btnReadNCard_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    启用日期:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="NsDate" CssClass="labeltext" runat="server" Text=""></asp:TextBox>
                            </td>
                            <td>
                                <div align="right">
                                    新卡余额:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="NewcMoney" CssClass="labeltext" runat="server" Text=""></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <asp:HiddenField ID="hiddentxtCardno" runat="server" />
                        <asp:HiddenField ID="hiddentxtNCardno" runat="server" />
                        <asp:HiddenField ID="hiddenAsn" runat="server" />
                        <asp:HiddenField ID="hiddenLabCardtype" runat="server" />
                        <asp:HiddenField ID="hiddensDate" runat="server" />
                        <asp:HiddenField ID="hiddeneDate" runat="server" />
                        <asp:HiddenField ID="hiddencMoney" runat="server" />
                        <asp:HiddenField ID="hiddentradeno" runat="server" />
                        <asp:HiddenField ID="hiddenDepositFee" runat="server" />
                        <asp:HiddenField ID="hiddenDepositBalanceFee" runat="server" />
                        <asp:HiddenField ID="hiddenCardcostFee" runat="server" />
                        <asp:HiddenField ID="hidProcedureFee" runat="server" />
                        <asp:HiddenField ID="hidOtherFee" runat="server" />
                        <asp:HiddenField ID="hidCardprice" runat="server" />
                        <asp:HiddenField ID="hidAccRecv" runat="server" />
                        <asp:HiddenField ID="CUSTRECTYPECODE" runat="server" />
                        <asp:HiddenField ID="SERSTARTIME" runat="server" />
                        <asp:HiddenField ID="OSERVICEMOENY" runat="server" />
                        <asp:HiddenField ID="SERTAKETAG" runat="server" />
                        <asp:HiddenField ID="hiddenCheck" runat="server" />
                        <asp:HiddenField ID="hidWarning" runat="server" />
                        <asp:HiddenField ID="hiddenverify" runat="server" />
                        <asp:HiddenField ID="hidSupplyMoney" runat="server" />
                        <asp:HiddenField ID="hidPapertype" runat="server" />
                        <asp:HiddenField ID="hidCustname" runat="server" />
                        <asp:HiddenField ID="hidPaperno" runat="server" />
                        <asp:HiddenField ID="hidOldCardcost" runat="server" />
                        <asp:HiddenField runat="server" ID="hidCardReaderToken" />
                        <asp:HiddenField ID="hidLockFlag" runat="server" />
                        <asp:HiddenField ID="hidLockBlackCardFlag" runat="server" />
                        <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
                        <asp:HiddenField runat="server" ID="hidCardnoForCheck" />
                        <asp:HiddenField runat="server" ID="hiddenSocialNo9" />
                        <asp:HiddenField runat="server" ID="hiddenSocialNo32" />
                    </table>
                </div>
            </div>
            <div class="basicinfo">
                <div class="money">
                    费用信息</div>
                <div class="kuang5">
                    <table width="180" border="0" cellpadding="0" cellspacing="0" class="tab1">
                        <tr class="tabbt">
                            <td width="66">
                                费用项目
                            </td>
                            <td width="94">
                                费用金额(元)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                质保金/押金
                            </td>
                            <td>
                                <asp:Label ID="DepositFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="tabjg">
                            <td>
                                抵用金
                            </td>
                            <td>
                                <asp:Label ID="DepositBalanceFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                卡费
                            </td>
                            <td>
                                <asp:Label ID="CardcostFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="tabjg">
                            <td>
                                手续费
                            </td>
                            <td>
                                <asp:Label ID="ProcedureFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                其他费用
                            </td>
                            <td>
                                <asp:Label ID="OtherFee" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="tabjg">
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="tabjg">
                            <td>
                                合计应收
                            </td>
                            <td>
                                <asp:Label ID="Total" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="pipinfo">
                <div class="info">
                    换卡信息</div>
                <div class="kuang5">
                    <div class="bigkuang">
                        <div class="left">
                            <img src="../../Images/show-change.JPG" /></div>
                        <div class="big">
                            <table width="200" border="0" cellpadding="0" cellspacing="0" class="text25">
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox runat="server" Text="&nbsp;转入账户宝" ID="isAccCharge" Visible="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="80">
                                        <label>
                                            本次实收</label>
                                    </td>
                                    <td width="80">
                                        <asp:TextBox ID="txtRealRecv" CssClass="inputshort" MaxLength="7" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        本次应找
                                    </td>
                                    <td>
                                        <div id="test">
                                            0.00</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footall">
            </div>
            <div class="btns">
                <table width="300" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false"
                                OnClientClick="printdiv('ptnPingZheng1')" />
                        </td>
                        <td>
                            <asp:Button ID="btnPrintSJ" runat="server" Text="打印收据" CssClass="button1" Enabled="false"
                                OnClientClick="printdiv('ptnShouJu1')" Visible="false" />
                        </td>
                        <td>
                            <asp:Button ID="Change" CssClass="button1" runat="server" Text="挂失卡补卡" OnClick="Change_Click" />
                        </td>
                    </tr>
                </table>
                <asp:CheckBox ID="chkPingzheng" runat="server" Checked="true" />自动打印凭证
                <asp:CheckBox ID="chkShouju" runat="server" Visible="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
