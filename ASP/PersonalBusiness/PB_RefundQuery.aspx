﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_RefundQuery.aspx.cs" Inherits="ASP_PersonalBusiness_PB_RefundQuery" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <title>退款查询</title>
</head>
<body>
    <form id="form1" runat="server">
    
	    <div class="tb">
		    个人业务->退款查询
	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->  
	        <div class="con">
	
		        <div class="card">
                    退款查询
		        </div>
          
		        <div class="kuang5">
          
			        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
				        <tr>
				        <td><div align="right">充值ID:</div></td>
					        <td>
                              <asp:TextBox ID="txtID" CssClass="inputmid" MaxLength="18" runat="server"></asp:TextBox>
					      
						        </td>
					        <td><div align="right">卡号:</div></td>
					        <td>
                                <asp:TextBox ID="txtCardNo" CssClass="inputmid" MaxLength="16" runat="server"></asp:TextBox>
					        </td>
					        <td>&nbsp;</td>
					        </tr>
					        <tr>
					            <td>
					                <div align="right">充值开始时间:</div>
					            </td>
					            <td>
					                <asp:TextBox ID="SupplyBeginDate" CssClass="inputmid" runat="server"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="SupplyBeginDate" Format="yyyyMMdd" />
					            </td>
					            <td>
					                <div align="right">充值结束时间:</div>
					            </td>
					            <td>
					                <asp:TextBox ID="SupplyEndDate" CssClass="inputmid" runat="server"></asp:TextBox>
					                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="SupplyEndDate" Format="yyyyMMdd" />
					            </td>
					            <td></td>
					        </tr>
					        <tr>
					            <td>
					                <div align="right">退款开始时间:</div>
					            </td>
					            <td>
					                <asp:TextBox ID="RefundBeginDate" CssClass="inputmid" runat="server"></asp:TextBox>
					                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="RefundBeginDate" Format="yyyyMMdd" />
					            </td>
					            <td>
					                <div align="right">退款结束时间:</div>
					            </td>
					            <td>
					                <asp:TextBox ID="RefundEndDate" CssClass="inputmid" runat="server"></asp:TextBox>
					                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="RefundEndDate" Format="yyyyMMdd" />
					            </td>
					            <td><div align="center"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/></div></td>
					        </tr>
			        </table>
        				
			        <div class="kuang5">
				        <div class="gdtb" style="height:395px ; overflow:auto">
				            <asp:GridView ID="gvResult" runat="server"
        Width = "98%"
        CssClass="tab1"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AllowPaging="true"
        PageSize="50"
        AutoGenerateColumns="False">
           <Columns>
                <asp:BoundField HeaderText="充值ID" DataField="ID"/>   
                <asp:BoundField HeaderText="卡号" DataField="CARDNO"/>
                <asp:BoundField HeaderText="充值日期" DataField="TRADEDATE"/>
                <asp:BoundField HeaderText="充值金额" DataField="TRADEMONEY"/>
                <asp:BoundField HeaderText="退款日期" DataField="OPERATETIME"/>
                <asp:BoundField HeaderText="退款账户的开户行" DataField="BANK"/>
                <asp:BoundField HeaderText="退款账户的银行账户" DataField="BANKACCNO"/>
                <asp:BoundField HeaderText="退款账户的开户名" DataField="CUSTNAME"/>
                <asp:BoundField HeaderText="退款比例" DataField="BACKSLOPE" DataFormatString="{0:P}"/>
            </Columns>
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>充值ID</td>
                    <td>卡号</td>
                    <td>充值日期</td>
                    <td>充值金额</td>
                    <td>退款日期</td>
                    <td>退款账户的开户行</td>
                    <td>退款账户的银行账户</td>
                    <td>退款账户的开户名</td>
                   
                    <td>退款比例</td>
                     <td>备注</td>
                      <td>状态</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
				        </div>
			        </div>
        			
		        </div>
        		
           </div>
	        
	        </ContentTemplate>  
        </asp:UpdatePanel>  
     
    </form>
             
</body>
</html>