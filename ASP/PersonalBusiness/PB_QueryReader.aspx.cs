﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using Master;
using Common;
using System.Text;
using System.Collections.Generic;
/***************************************************************
 * 功能名: 充付器查询

 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2013/01/28    shil            初次开发

 ****************************************************************/

public partial class ASP_PersonalBusiness_PB_QueryReader : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDL();
        }
    }

    private void BindDDL()
    {
        //充付器类型
        ReaderHelper.ReaderTypeBindDDL(context,ddlReaderType);

        //充付器厂商
        ReaderHelper.ManuBindDDL(context,ddlReaderManu);

        //证件类型
        ASHelper.initPaperTypeList(context,ddlPaperType);

        //性别
        ASHelper.initSexList(ddlSex);
    }

    /// <summary>
    /// 查询按钮点击事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        ClearCustInfo();
        //校验输入有效性
        if (!queryValidation()) 
            return;

        labCreate.InnerText = "";
        labStockIn.InnerText = "";
        labStockOut.InnerText = "";
        labSale.InnerText = "";
        labChangeRecovery.InnerText = "";
        labReturnRecovery.InnerText = "";
        labSum.InnerText = "";

        string readerno = txtReaderNo.Text.Trim();
        string startdate = txtStartDate.Text.Trim();
        string enddate = txtEndDate.Text.Trim();
        string readerstate = ddlReaderState.SelectedValue;
        string readertype = ddlReaderType.SelectedValue;
        string readermanu = ddlReaderManu.SelectedValue;

        DataTable ReaderData = SPHelper.callQuery("SP_EM_ReaderQuery", context, "QueryReader",readerno,readerstate,readertype,readermanu,startdate,enddate);
        gvResult.DataSource = ReaderData;
        gvResult.DataBind();

        SetReaderCount(ReaderData);
    }

    /// <summary>
    /// gridview单击事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string readerno = e.CommandArgument.ToString();
        if ("Detail".Equals(e.CommandName))
        {
            ClearCustInfo();
            if (!IsExistUser(readerno))
            {
                context.AddMessage("当前充付器不存在用户信息");
                return;
            }
            //姓名，出生如期，证件类型，证件号码，用户性别，联系电话，邮政编码，联系地址，电子邮件，备注
            DataTable ReaderUserData = SPHelper.callQuery("SP_EM_ReaderQuery", context, "QueryReaderUserInfo", readerno);
            CustName.Text = ReaderUserData.Rows[0]["CUSTNAME"].ToString();
            CustBirthday.Text = ReaderUserData.Rows[0]["CUSTBIRTH"].ToString();
            ddlPaperType.SelectedValue = ReaderUserData.Rows[0]["PAPERTYPECODE"].ToString();
            Paperno.Text = ReaderUserData.Rows[0]["PAPERNO"].ToString();
            ddlSex.SelectedValue = ReaderUserData.Rows[0]["CUSTSEX"].ToString();
            Custphone.Text = ReaderUserData.Rows[0]["CUSTPHONE"].ToString();
            Custpost.Text = ReaderUserData.Rows[0]["CUSTPOST"].ToString();
            Custaddr.Text = ReaderUserData.Rows[0]["CUSTADDR"].ToString();
            txtEmail.Text = ReaderUserData.Rows[0]["CUSTEMAIL"].ToString();
            Remark.Text = ReaderUserData.Rows[0]["REMARK"].ToString();
        }
    }

    /// <summary>
    /// 分页
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_Page(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    /// <summary>
    /// 输入校验
    /// </summary>
    /// <returns>没有错误时返回true，有错误时返回false</returns>
    private Boolean queryValidation()
    {
        //充付器序列号
        if (!string.IsNullOrEmpty(txtReaderNo.Text.Trim()))
        {
            //if (Validation.strLen(txtReaderNo.Text.Trim()) != 16)
            //    context.AddError("A094570211：充付器序列号长度必须为16位", txtReaderNo);
            if (!Validation.isNum(txtReaderNo.Text.Trim()))
                context.AddError("A094570212:充付器序列号必须为数字", txtReaderNo);
        }

        //校验日期
        ResourceManageHelper.checkDate(context, txtStartDate, txtEndDate, "A095470102", "A095470103", "A095470104");

        return !context.hasError();
    }
    
    /// <summary>
    /// 获取关键字的值
    /// </summary>
    /// <param name="keysname"></param>
    /// <returns></returns>
    public String getDataKeys(string keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    /// <summary>
    /// 设置各种状态的充付器数量
    /// </summary>
    /// <param name="ReaderData"></param>
    private void SetReaderCount(DataTable ReaderData)
    {
        if (ReaderData == null || ReaderData.Rows.Count == 0)
            return;
        //各种状态充付器数量
        labCreate.InnerText = ReaderData.Rows[0]["generation"].ToString();
        labStockIn.InnerText = ReaderData.Rows[0]["storage"].ToString();
        labStockOut.InnerText = ReaderData.Rows[0]["stockout"].ToString();
        labSale.InnerText = ReaderData.Rows[0]["sold"].ToString();
        labChangeRecovery.InnerText = ReaderData.Rows[0]["changerecovery"].ToString();
        labReturnRecovery.InnerText = ReaderData.Rows[0]["returnrecovery"].ToString();
        labSum.InnerText = (Convert.ToInt32(labCreate.InnerText) + Convert.ToInt32(labStockIn.InnerText) + Convert.ToInt32(labStockOut.InnerText)
            + Convert.ToInt32(labSale.InnerText) + Convert.ToInt32(labChangeRecovery.InnerText) + Convert.ToInt32(labReturnRecovery.InnerText)).ToString();
    }

    /// <summary>
    /// 判断当前充付器是否有用户信息
    /// </summary>
    /// <param name="readerno"></param>
    /// <returns></returns>
    private bool IsExistUser(string readerno)
    {
        string ReaderUserSql = string.Format("select * from tf_f_readercustrec where serialnumber='{0}'", readerno);
        context.DBOpen("Select");
        DataTable ReaderUserData=context.ExecuteReader(ReaderUserSql);
        context.DBCommit();
        if (ReaderUserData == null || ReaderUserData.Rows.Count == 0)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// 清除用户信息
    /// </summary>
    private void ClearCustInfo()
    {
        CustName.Text = "";
        CustBirthday.Text = "";
        ddlPaperType.SelectedValue = "";
        Paperno.Text = "";
        ddlSex.SelectedValue = "";
        Custphone.Text = "";
        Custpost.Text = "";
        Custaddr.Text = "";
        txtEmail.Text = "";
        Remark.Text = "";
    }
}