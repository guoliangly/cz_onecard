﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PB_AccPassReset.aspx.cs"
    Inherits="ASP_PersonalBusiness_PB_AccPassReset" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>密码重置</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <div class="tb">
            个人业务->密码重置
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        
        <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                        卡片查询</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%">
                                    <div align="right">
                                        卡号:</div>
                                </td>
                                <td width="30%">
                                    <asp:TextBox ID="txtCardno" CssClass="labeltext" runat="server" MaxLength="16"></asp:TextBox>
                                </td>
                                <asp:HiddenField ID="hiddenread" runat="server" />
                                <td align="right">
                                    <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="读卡" OnClientClick="return readCardNo()"
                                        OnClick="btnQuery_Click" /></td>
                            </tr>
                        </table>
                    </div>
                    <div class="card">
                        卡片信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="25%">
                                    <asp:Label ID="labOldName" runat="server"></asp:Label></td>
                                    <td width="9%">
                                        <div align="right">
                                            卡片状态:</div>
                                    </td>
                                    <td width="17%">
                                        <asp:Label ID="labOldState" runat="server"></asp:Label></td>
                                    <td width="13%">
                                        <div align="right">
                                            联系电话:</div>
                                    </td>
                                    <td width="24%">
                                        <asp:Label ID="labOldPhone" runat="server"></asp:Label>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                         电子邮件:</div>
                                </td>
                                <td>
                                    <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label></td>
                                <td>
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td>
                                    <asp:Label ID="labOldSex" runat="server"></asp:Label></td>
                                <td>
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td>
                                    <asp:Label ID="labOldPaperType" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        证件号码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="labOldPaperNo" runat="server"></asp:Label></td>
                                <td>
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td>
                                    <asp:Label ID="labOldBirth" runat="server"></asp:Label></td>
                                <td>
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td>
                                    <asp:Label ID="labOldPost" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="5">
                                    <asp:Label ID="labOldAddr" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <div class="footall">
                    </div>
                </div>
                <div class="btns">
                    <table width="330px" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                        <td></td>
                            <td>
                                &nbsp;
                             

                            </td>
                            <td style="width: 30px">
                            </td>
                            <td style="width:120px">
                                <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                    OnClick="btnSubmit_Click" /></td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
