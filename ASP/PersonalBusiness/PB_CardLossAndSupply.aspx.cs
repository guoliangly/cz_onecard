﻿using System;
using System.Data;
using System.Web.UI;
using Common;
using PDO.PersonalBusiness;
using TDO.BusinessCode;
using TDO.CardManager;
using TDO.CustomerAcc;
using TDO.ResourceManager;
using TDO.UserManager;
using TM;

public partial class ASP_PersonalBusiness_PB_CardLossAndSupply : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            setReadOnly(OsDate, ODeposit, OldcMoney, LabCardtype, NsDate, NewcMoney, NDeposit);

            txtRealRecv.Attributes["onfocus"] = "this.select();";
            txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this,'test', 'hidAccRecv');";
            if (!context.s_Debugging) setReadOnly(txtOCardno);
            if (!context.s_Debugging) txtNCardno.Attributes["readonly"] = "true";

            //初始化换卡类型
            InitLoad(sender, e);
            hidAccRecv.Value = Total.Text;

            gvAccount.DataSource = null;
            gvAccount.DataBind();

            txtOCardno.Attributes.Remove("readonly");
            btnDBRead.Enabled = true;
            txtOCardno.CssClass = "input";
        }
    }

    protected void InitLoad(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        //从前台业务交易费用表中读取售卡费用数据
        TD_M_TRADEFEETDO ddoTdMTradefeetdoIn = new TD_M_TRADEFEETDO();
        ddoTdMTradefeetdoIn.TRADETYPECODE = "4A";

        TD_M_TRADEFEETDO[] ddoTdMTradefeeOutArr = (TD_M_TRADEFEETDO[])tmTmTableModule.selByPKArr(context, ddoTdMTradefeetdoIn, typeof(TD_M_TRADEFEETDO), "S001004139", "TD_M_TRADEFEE", null);

        for (int i = 0; i < ddoTdMTradefeeOutArr.Length; i++)
        {
            //费用类型为押金
            if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "00")
            {
                //质保金
                hiddenDepositFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
                //抵用金
                hiddenDepositBalanceFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
            }
            //费用类型为卡费
            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "10")
                hiddenCardcostFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");

            //费用类型为手续费
            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "03")
                hidProcedureFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");

            //费用类型为其他费用
            else if (ddoTdMTradefeeOutArr[i].FEETYPECODE == "99")
                hidOtherFee.Value = ((Convert.ToDecimal(ddoTdMTradefeeOutArr[i].BASEFEE)) / 100).ToString("0.00");
        }

        //费用赋值
        DepositFee.Text = hiddenDepositFee.Value;
        DepositBalanceFee.Text = hiddenDepositBalanceFee.Value;
        CardcostFee.Text = hiddenCardcostFee.Value;
        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(DepositBalanceFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");

        //读新卡按钮不可用,换卡按钮不可用
        btnReadNCard.Enabled = false;
        Change.Enabled = false;
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtOCardno.Text.Trim() == "")
            context.AddError("A001004113", txtOCardno);
        else
        {
            if ((txtOCardno.Text.Trim()).Length != 16)
                context.AddError("A001004114", txtOCardno);
            else if (!Validation.isNum(txtOCardno.Text.Trim()))
                context.AddError("A001004115", txtOCardno);
        }

        //校验卡状态
        string strSql = string.Format("select * from tf_f_cardrec t where t.cardstate='03' and t.cardno='{0}'",txtOCardno.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();
        if (dt == null || dt.Rows.Count== 0)
            context.AddError("卡状态不是书面挂失状态不能做挂失补卡！");

        if (!IsCanRepair())
            context.AddError("该类型卡不能做挂失补卡业务！");

        if (HasGsCard())
            context.AddError("该卡此次挂失之后已经做过挂失补卡业务！");

        return !(context.hasError());
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!DBreadValidation())
            return;

        TMTableModule tmTmTableModule = new TMTableModule();


        //context.SPOpen();
        //context.AddField("p_CARDNO").Value = txtOCardno.Text;
        //bool ok = context.ExecuteSP("SP_RB_Credit_Check");

        //if (ok)
        //{
        //从IC卡资料表中读取数据
        TF_F_CARDRECTDO ddoTfFCardrecIn = new TF_F_CARDRECTDO();
        ddoTfFCardrecIn.CARDNO = txtOCardno.Text;

        TF_F_CARDRECTDO ddoTfFCardrecOut = (TF_F_CARDRECTDO)tmTmTableModule.selByPK(context, ddoTfFCardrecIn, typeof(TF_F_CARDRECTDO), null);

        //从IC卡电子钱包账户表中读取数据
        TF_F_CARDEWALLETACCTDO ddoTfFCardewalletaccIn = new TF_F_CARDEWALLETACCTDO();
        ddoTfFCardewalletaccIn.CARDNO = txtOCardno.Text;

        TF_F_CARDEWALLETACCTDO ddoTfFCardewalletaccOut = (TF_F_CARDEWALLETACCTDO)tmTmTableModule.selByPK(context, ddoTfFCardewalletaccIn, typeof(TF_F_CARDEWALLETACCTDO), null);

        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTlRIcuserIn = new TL_R_ICUSERTDO();
        ddoTlRIcuserIn.CARDNO = txtOCardno.Text;

        TL_R_ICUSERTDO ddoTlRIcuserOut = (TL_R_ICUSERTDO)tmTmTableModule.selByPK(context, ddoTlRIcuserIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTlRIcuserOut == null)
        {
            context.AddError("A001001101");
            return;
        }

        //从资源状态编码表中读取数据
        TD_M_RESOURCESTATETDO ddoTdMResourcestateIn = new TD_M_RESOURCESTATETDO();
        ddoTdMResourcestateIn.RESSTATECODE = ddoTlRIcuserOut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTdMResourcestateOut = (TD_M_RESOURCESTATETDO)tmTmTableModule.selByPK(context, ddoTdMResourcestateIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTdMResourcestateOut == null)
            RESSTATE.Text = ddoTlRIcuserOut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTdMResourcestateOut.RESSTATE;

        #region 获取用户信息
        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecIn = new TF_F_CUSTOMERRECTDO();
        ddoTfFCustomerrecIn.CARDNO = txtOCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecOut = (TF_F_CUSTOMERRECTDO)tmTmTableModule.selByPK(context, ddoTfFCustomerrecIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTfFCustomerrecOut == null)
        {
            context.AddError("A001107112");
            return;
        }

        CustName.Text = ddoTfFCustomerrecOut.CUSTNAME;
        //性别显示
        if (ddoTfFCustomerrecOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTfFCustomerrecOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期显示
        if (ddoTfFCustomerrecOut.CUSTBIRTH != "")
        {
            String bdate = ddoTfFCustomerrecOut.CUSTBIRTH;
            if (bdate.Length == 8)
            {
                CustBirthday.Text = bdate.Substring(0, 4) + "-" + bdate.Substring(4, 2) + "-" + bdate.Substring(6, 2);
            }
            else CustBirthday.Text = bdate;
        }
        else CustBirthday.Text = ddoTfFCustomerrecOut.CUSTBIRTH;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTdMPapertypeIn = new TD_M_PAPERTYPETDO();
        ddoTdMPapertypeIn.PAPERTYPECODE = ddoTfFCustomerrecOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTdMPapertypeOut = (TD_M_PAPERTYPETDO)tmTmTableModule.selByPK(context, ddoTdMPapertypeIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //证件类型显示
        if (ddoTfFCustomerrecOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTdMPapertypeOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTfFCustomerrecOut.PAPERTYPECODE;

        Paperno.Text = ddoTfFCustomerrecOut.PAPERNO;
        Custaddr.Text = ddoTfFCustomerrecOut.CUSTADDR;
        Custpost.Text = ddoTfFCustomerrecOut.CUSTPOST;
        Custphone.Text = ddoTfFCustomerrecOut.CUSTPHONE;
        txtEmail.Text = ddoTfFCustomerrecOut.CUSTEMAIL;
        Remark.Text = ddoTfFCustomerrecOut.REMARK;

        hidUserName.Value = ddoTfFCustomerrecOut.CUSTNAME;
        hidUserSex.Value = ddoTfFCustomerrecOut.CUSTSEX == "0" ? "01" //写卡时转换性别编码
                         : ddoTfFCustomerrecOut.CUSTSEX == "1" ? "02"
                         : "";

        if (ddoTfFCustomerrecOut.PAPERTYPECODE != "00" && ddoTfFCustomerrecOut.CUSTBIRTH != "")
        {
            hidUserPaperno.Value = "111111" + ddoTfFCustomerrecOut.CUSTBIRTH + "1111";
        }
        else
        {
            hidUserPaperno.Value = ddoTfFCustomerrecOut.PAPERNO;
        }

        hidUserPhone.Value = ddoTfFCustomerrecOut.CUSTPHONE;
        #endregion

        #region 账户信息
        ShowCustomerAcc();
        #endregion

        //给页面显示项赋值
        hidOldCardcost.Value = ddoTfFCardrecOut.CARDCOST.ToString();
        SERTAKETAG.Value = ddoTfFCardrecOut.SERSTAKETAG;
        OSERVICEMOENY.Value = ddoTfFCardrecOut.SERVICEMONEY.ToString();
        SERSTARTIME.Value = ddoTfFCardrecOut.SERSTARTTIME.ToString();
        CUSTRECTYPECODE.Value = ddoTfFCardrecOut.CUSTRECTYPECODE;

        if (txtOCardno.Text.Trim().StartsWith("715009049"))
        {
            ODepositBalance.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / (Convert.ToDecimal(100))).ToString("0.00");
        }
        else
        {
            ODeposit.Text = (Convert.ToDecimal(ddoTfFCardrecOut.DEPOSIT) / (Convert.ToDecimal(100))).ToString("0.00");
        }

        OCardcost.Text = (Convert.ToDecimal(ddoTfFCardrecOut.CARDCOST) / (Convert.ToDecimal(100))).ToString("0.00");
        OsDate.Text = ddoTfFCardrecOut.SERSTARTTIME.ToString("yyyy-MM-dd");
        OldcMoney.Text = (Convert.ToDecimal(ddoTfFCardewalletaccOut.CARDACCMONEY) / 100).ToString("0.00");

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtOCardno.Text);

        hiddentxtCardno.Value = txtOCardno.Text;

        //读新卡按钮可用
        btnReadNCard.Enabled = true;

        btnPrintPZ.Enabled = false;
        //}
    }

    protected void btnReadNCard_Click(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        //if (txtOCardno.Text.Trim().Substring(4, 2) != txtNCardno.Text.Trim().Substring(4, 2))
        //{
        //    context.AddError("旧卡和新卡卡片类型应一致");
        //    return;
        //}

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTdMCardtypeIn = new TD_M_CARDTYPETDO();
        ddoTdMCardtypeIn.CARDTYPECODE = hiddenLabCardtype.Value;

        TD_M_CARDTYPETDO ddoTdMCardtypeOut = (TD_M_CARDTYPETDO)tmTmTableModule.selByPK(context, ddoTdMCardtypeIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);

        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTlRIcuserIn = new TL_R_ICUSERTDO();
        ddoTlRIcuserIn.CARDNO = txtNCardno.Text;

        TL_R_ICUSERTDO ddoTlRIcuserOut = (TL_R_ICUSERTDO)tmTmTableModule.selByPK(context, ddoTlRIcuserIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTlRIcuserOut == null)
        {
            context.AddError("A001004128");
            return;
        }

        if (ddoTlRIcuserOut.RESSTATECODE == "04")  //回收
        {
            context.AddError("回收的卡应该不能做为新卡换卡");
            return;
        }

        if (!IsCanRepaired())
        {
            context.AddError("该类型卡不能换卡售出！");
            return;
        }

        //给页面显示项赋值
        hidCardprice.Value = ddoTlRIcuserOut.CARDPRICE.ToString();
        NDeposit.Text = (Convert.ToDecimal(ddoTlRIcuserOut.CARDPRICE) / 100).ToString("0.00");
        LabCardtype.Text = ddoTdMCardtypeOut.CARDTYPENAME;
        NsDate.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
        NewcMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / 100).ToString("0.00");

        FeeCount(sender, e);

        hidAccRecv.Value = Total.Text;
        txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();

        if (Convert.ToDecimal(hiddencMoney.Value) != 0)
        {
            context.AddError("A001001144");
            return;
        }

        DisCount();

        //换卡按钮可用
        Change.Enabled = true;
        btnPrintPZ.Enabled = false;
    }

    //根据换卡类型和卡类型确定费用
    private void FeeCount(object sender, EventArgs e)
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        DepositFee.Text = hiddenDepositFee.Value;
        DepositBalanceFee.Text = hiddenDepositBalanceFee.Value;
        CardcostFee.Text = (Convert.ToDecimal(hiddenCardcostFee.Value) + Convert.ToDecimal(hidCardprice.Value) / 100).ToString("0.00");

        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(DepositBalanceFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnReadNCard.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            if (hidLockFlag.Value == "true")
            {
                AddMessage("旧卡锁定成功");
            }
            else
            {
                if (selReasonType.SelectedValue == "16")
                {
                    DateTime LossDate = GetLossDate().Date;
                    DateTime DtNow = DateTime.Today;
                    if (LossDate < DtNow.AddDays(-7))
                    {
                        AddMessage("换卡成功，距挂失日期已超过7天，请至挂失卡转值页面办理转值业务!");
                    }
                    else
                    {
                        AddMessage("换卡成功,请于" + LossDate.AddDays(7).ToString("yyyy-MM-dd") + "后来办理转值业务!");
                    }
                }
                else
                {
                    AddMessage("前台写卡成功");
                }
                clearCustInfo(txtOCardno, txtNCardno);
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnDBRead_Click(sender, e);
        }

        if (chkPingzheng.Checked && btnPrintPZ.Enabled && chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, GetType(), "writeCardScript",
                "printAll();", true);
        }
        else if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, GetType(), "writeCardScript",
                "printInvoice();", true);
        }

        hidLockFlag.Value = "";
        hidWarning.Value = "";
        hidCardnoForCheck.Value = "";
    }

    protected void Change_Click(object sender, EventArgs e)
    {
        //判断售卡权限
        checkCardState(txtNCardno.Text);
        if (context.hasError()) return;
        TMTableModule tmTMTableModule = new TMTableModule();
        //如果是移动龙城通卡，则不能换卡
        if (txtNCardno.Text.Trim().StartsWith("91501101") || txtOCardno.Text.Trim().StartsWith("91501101"))
        {
            context.AddError("移动龙城通卡不能补卡");
            return;
        }

        if (txtNCardno.Text.Trim().StartsWith("91501103") || txtOCardno.Text.Trim().StartsWith("91501103"))
        {
            context.AddError("江南农商行龙城通卡不能补卡");
            return;
        }
        //如果是天翼龙成通卡，则不判断卡所属员工是否是售卡员工wdx 20111202
        if (txtNCardno.Text.Trim().Substring(0, 8) == "91501102")
        {
            //从用户卡库存表中读取数据
            TL_R_ICUSERTDO ddoTlRIcuserIn = new TL_R_ICUSERTDO();
            ddoTlRIcuserIn.CARDNO = txtNCardno.Text;

            TL_R_ICUSERTDO ddoTlRIcuserOut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTlRIcuserIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);
            if (ddoTlRIcuserOut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //卡库存状态不是出库或分配状态

            if (ddoTlRIcuserOut.RESSTATECODE != "01" && ddoTlRIcuserOut.RESSTATECODE != "05")
            {
                context.AddError("A001004132");
                return;
            }
        }
        //判断售卡权限
        else
        {
            checkCardState(txtNCardno.Text);
        }
        if (context.hasError()) return;

        if (txtRealRecv.Text == null)
        {
            context.AddError("A001001143");
            return;
        }

        //获取旧卡的公交类型


        TF_F_CARDCOUNTACCTDO ddoTfFCardcountaccIn = new TF_F_CARDCOUNTACCTDO();
        ddoTfFCardcountaccIn.CARDNO = txtOCardno.Text;

        TF_F_CARDCOUNTACCTDO ddoTfFCardcountaccOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTfFCardcountaccIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC_CARDNO", null);

        if (ddoTfFCardcountaccOut != null)
        {
            context.AddError("此旧卡非普通卡,不能补卡");
            return;
        }

        if (txtRealRecv.Text == null)
        {
            context.AddError("A001001143");
            return;
        }
        TMTableModule tmTmTableModule = new TMTableModule();

        //读取审核员工信息
        TD_M_INSIDESTAFFTDO ddoTdMInsidestaffIn = new TD_M_INSIDESTAFFTDO();
        ddoTdMInsidestaffIn.OPERCARDNO = hiddenCheck.Value;

        TD_M_INSIDESTAFFTDO[] ddoTdMInsidestaffOut = (TD_M_INSIDESTAFFTDO[])tmTmTableModule.selByPKArr(context, ddoTdMInsidestaffIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);


        //挂失补卡登记7天后自动余额转入账户宝
        if (isAccCharge.Checked == true && isAccCharge.Visible == true)
        {
            context.DBOpen("StorePro");
            context.AddField("P_TRADETYPECODE").Value = "4A";
            context.AddField("P_NEWCARDNO").Value = txtNCardno.Text.Trim();
            context.AddField("P_OLDCARDNO").Value = txtOCardno.Text.Trim();
            if (!context.ExecuteSP("SP_PB_CHANGEREGIEST")) return;
        }

        SP_PB_ChangeGSCard_COMMITPDO pdo = new SP_PB_ChangeGSCard_COMMITPDO();

        //存储过程赋值
        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, txtNCardno.Text);
        pdo.CUSTRECTYPECODE = CUSTRECTYPECODE.Value;
        pdo.CARDCOST = Convert.ToInt32(Convert.ToDecimal(CardcostFee.Text) * 100);
        pdo.NEWCARDNO = txtNCardno.Text;
        pdo.OLDCARDNO = txtOCardno.Text;
        pdo.ONLINECARDTRADENO = hiddentradeno.Value;
        pdo.CHANGECODE = selReasonType.SelectedValue;
        pdo.ASN = hiddenAsn.Value.Substring(4, 16);
        pdo.CARDTYPECODE = hiddenLabCardtype.Value;
        pdo.SELLCHANNELCODE = "01";
        pdo.TRADETYPECODE = "4A";
        pdo.PREMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;

        //最近卡充值实际余额为本次交易前卡内余额
        pdo.SUPPLYREALMONEY = 0;
        //if (txtOCardno.Text.Trim().StartsWith("715009049"))
        //{
        //    pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODepositBalance.Text) * 100);
        //}
        //else
        //{
        pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100);
        //}

        //换卡类型为不可读人为损坏卡,挂失卡相当于不可读人为损坏卡
        pdo.SERSTARTTIME = DateTime.Now;
        pdo.SERVICEMONE = 0;
        pdo.CARDACCMONEY = 0;
        pdo.NEWSERSTAKETAG = "0";
        pdo.TOTALSUPPLYMONEY = 0;
        pdo.OLDDEPOSIT = 0;
        pdo.SERSTAKETAG = "2";
        pdo.NEXTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
        pdo.CURRENTMONEY = Convert.ToInt32(Convert.ToDecimal(NewcMoney.Text) * 100);
        if (ddoTdMInsidestaffOut == null || ddoTdMInsidestaffOut.Length == 0)
        {
            context.AddError("A001010108");
            return;
        }
        pdo.CHECKSTAFFNO = ddoTdMInsidestaffOut[0].STAFFNO;
        pdo.CHECKDEPARTNO = ddoTdMInsidestaffOut[0].DEPARTNO;

        if (txtOCardno.Text.Trim().StartsWith("715009049"))
        {
            pdo.SERSTAKETAG = SERTAKETAG.Value;
            pdo.NEWSERSTAKETAG = SERTAKETAG.Value;
        }

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecIn = new TF_F_CUSTOMERRECTDO();
        ddoTfFCustomerrecIn.CARDNO = txtOCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTfFCustomerrecOut = (TF_F_CUSTOMERRECTDO)tmTmTableModule.selByPK(context, ddoTfFCustomerrecIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTfFCustomerrecOut == null)
        {
            context.AddError("A001107112");
            return;
        }
        hidCustname.Value = ddoTfFCustomerrecOut.CUSTNAME;
        hidPaperno.Value = ddoTfFCustomerrecOut.PAPERNO;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTdMPapertypeIn = new TD_M_PAPERTYPETDO();
        ddoTdMPapertypeIn.PAPERTYPECODE = ddoTfFCustomerrecOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTdMPapertypeOut = (TD_M_PAPERTYPETDO)tmTmTableModule.selByPK(context, ddoTdMPapertypeIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //证件类型赋值
        if (ddoTfFCustomerrecOut.PAPERTYPECODE != "")
        {
            hidPapertype.Value = ddoTdMPapertypeOut.PAPERTYPENAME;
        }
        else
            hidPapertype.Value = ddoTfFCustomerrecOut.PAPERTYPECODE;

        hidSupplyMoney.Value = "" + pdo.CURRENTMONEY;

        ////社保卡9位和32位
        //DataTable data = ResidentCardHelper.GetSocialInfo(context, ddoTfFCustomerrecOut.PAPERNO);

        //if (data != null && data.Rows.Count > 0)
        //{
        //    hiddenSocialNo32.Value = Convert.ToString(data.Rows[0]["SOCIALNO"]);
        //    hiddenSocialNo9.Value = Convert.ToString(data.Rows[0]["SOCLSECNO"]).PadLeft(10, '0');
        //}
        //else
        //{
            hiddenSocialNo9.Value = "";
            hiddenSocialNo32.Value = "";
        //}

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            hidCardnoForCheck.Value = txtNCardno.Text;

            ScriptManager.RegisterStartupScript(this, GetType(), "writeCardScript",
                    "changeCard();", true);
            btnPrintPZ.Enabled = true;

            //string salecardType = "";
            //if (txtNCardno.Text.Trim().StartsWith("71500901"))
            //{
            //    salecardType = "(卡费)";
            //}
            //else if (txtNCardno.Text.Trim().StartsWith("71500902"))
            //{
            //    salecardType = "(质保金)";
            //}
            //else if (txtNCardno.Text.Trim().StartsWith("71500903"))
            //{
            //    salecardType = "(押金)";
            //}
            //else if (txtNCardno.Text.Trim().StartsWith("71500904"))
            //{
            //    salecardType = "(抵用金)";
            //}
            //else if (txtNCardno.Text.Trim().StartsWith("71500905"))
            //{
            //    salecardType = "(质保金)";
            //}
            //if (selReasonType.SelectedValue == "16")//挂失卡,人为损
            //{
            //    salecardType = "(卡费)";
            //}

            ASHelper.preparePingZheng(ptnPingZheng, txtOCardno.Text, hidCustname.Value, "挂失卡补卡", Total.Text
                , Total.Text, txtNCardno.Text, hidPaperno.Value, (Convert.ToDecimal(pdo.NEXTMONEY) / (Convert.ToDecimal(100))).ToString("0.00"),
                "", Total.Text, context.s_UserID, context.s_DepartName, hidPapertype.Value, ProcedureFee.Text, "0.00");

            InitLoad(sender, e);
            Change.Enabled = false;
        }
    }

    private void ShowCustomerAcc()
    {
        TMTableModule tmTmTableModule = new TMTableModule();

        DataTable dt = SPHelper.callQuery("SP_CA_Query", context, "QueryAcctList", txtOCardno.Text.Trim());

        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddError("A006023009:未查出客户账户信息");
            return;
        }
        else
        {
            foreach (DataRow item in dt.Rows)
            {
                if (item["状态"].ToString() != "挂失")
                {
                    context.AddError("A001090103:客户账户状态不是挂失状态");
                    return;
                }

                //余额账本
                TF_F_ACCT_BALANCETDO ddoTfFAcctBalanceIn = new TF_F_ACCT_BALANCETDO();
                ddoTfFAcctBalanceIn.ICCARD_NO = txtOCardno.Text.Trim();
                ddoTfFAcctBalanceIn.ACCT_ID = item["ACCT_ID"].ToString();

                TF_F_ACCT_BALANCETDO ddoTfFAcctBalanceOut = (TF_F_ACCT_BALANCETDO)tmTmTableModule.selByPK(context, ddoTfFAcctBalanceIn, typeof(TF_F_ACCT_BALANCETDO), null, "TF_F_ACCT_BALANCE", null);

                if (ddoTfFAcctBalanceOut == null)
                {
                    context.AddError("A001090104:未查出余额账本信息");
                    return;
                }
                else
                {
                    if (!ddoTfFAcctBalanceOut.STATE.Equals("F"))
                    {
                        context.AddError("A001090105:余额账本状态不是挂失状态");
                        return;
                    }
                }
            }
        }

        gvAccount.DataSource = dt;
        gvAccount.DataBind();
        gvAccount.SelectedIndex = -1;
    }

    private DateTime GetLossDate()
    {
        string strSql = string.Format("select t.operatetime from tf_b_trade t where t.cardno='{0}' and t.tradetypecode='08' order by t.operatetime desc",txtOCardno.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();

        DateTime date = DateTime.Parse(dt.Rows[0]["operatetime"].ToString());
        return date;
    }

    /// <summary>
    /// 是否已经做过补卡
    /// </summary>
    /// <returns></returns>
    private bool HasGsCard()
    {
        string strSql = string.Format("select t1.rsrv1 from tf_F_cardrec t1 where t1.cardno='{0}'",txtOCardno.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();
        if (null == dt || dt.Rows.Count == 0)
            return false;
        if (string.IsNullOrEmpty(dt.Rows[0]["rsrv1"].ToString()))
            return false;
        return true;
    }

    /// <summary>
    /// 判断卡是否可以做补卡业务
    /// </summary>
    /// <returns></returns>
    private bool IsCanRepair()
    {
        bool isCan = true;
        string CardType = DealString.GetResourceValue(context.ResourcePath, "CardLossRepair", "UnableRepair");
        string[] TypeCode = CardType.Split(',');
        string OldCardType = txtOCardno.Text.Trim().Substring(4,4);
        for (int i = 0; i < TypeCode.Length; i++)
        {
            if (TypeCode[i] == OldCardType)
            {
                isCan = false;
                break;
            }
        }
            return isCan;
    }

    /// <summary>
    /// 判断新卡是否可以补卡售出
    /// </summary>
    /// <returns></returns>
    private bool IsCanRepaired()
    {
        bool isCan = true;
        string CardType = DealString.GetResourceValue(context.ResourcePath, "CardLossRepair", "UnableRepaired");
        string[] TypeCode = CardType.Split(',');
        string NewCardType = txtNCardno.Text.Trim().Substring(4, 4);
        for (int i = 0; i < TypeCode.Length; i++)
        {
            if (TypeCode[i] == NewCardType)
            {
                isCan = false;
                break;
            }
        }
        return isCan;
    }

    /// <summary>
    ///
    /// </summary>
    private void DisCount()
    {
        if (txtNCardno.Text.Trim() != "" && txtNCardno.Text.Trim().Substring(0, 6) == "915003")
        {
            return;
        }
        if (Paperno.Text.Trim() != "")
        {
            DataTable data = SPHelper.callPBQuery(context, "QryDiscount", Paperno.Text.Trim());

            //只要购买过普通龙城通卡，就不给予优惠，没有就给予优惠
            if (data == null || data.Rows.Count == 0)
            {
                QryDiscount();
                return;
            }
        }
    }

    private void QryDiscount()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从系统参数表读取新卡费用
        TD_M_TAGTDO ddoTD_M_TAGIn = new TD_M_TAGTDO();
        ddoTD_M_TAGIn.TAGCODE = "BUSCARD_MONEY";
        TD_M_TAGTDO ddoTD_M_TAGOut = (TD_M_TAGTDO)tmTMTableModule.selByPK(context, ddoTD_M_TAGIn, typeof(TD_M_TAGTDO), null, "TD_M_TAG", null);

        DepositFee.Text = "0.00";
        CardcostFee.Text = ((Convert.ToDecimal(ddoTD_M_TAGOut.TAGVALUE)) / 100).ToString("0.00");

        Total.Text = ((Convert.ToDecimal(CardcostFee.Text)) + (Convert.ToDecimal(DepositFee.Text)) + (Convert.ToDecimal(OtherFee.Text))).ToString("0.00");
        txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();
    }

}