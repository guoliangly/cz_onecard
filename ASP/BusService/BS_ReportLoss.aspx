﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BS_ReportLoss.aspx.cs" Inherits="ASP_BusService_BS_ReportLoss" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>挂失解挂</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <link href="../../css/photo.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
</head>
<body>
    <form id="form2" runat="server">
        <div class="tb">
            公交卡->挂失解挂
        </div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true"
            ID="ToolkitScriptManager1" runat="server" />

        <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance(); 
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>

        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server" RenderMode="inline">
            <ContentTemplate>
                <aspControls:PrintPingZheng ID="ptnPingZheng" runat="server" PrintArea="ptnPingZheng1" />
                <asp:BulletedList ID="bulMsgShow" runat="server" />

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="base">
                    </div>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td style="width: 10%; height: 25px;" align="right">
                                    证件号:
                                </td>
                                <td style="width: 20%; height: 25px;" valign="middle">
                                    <asp:TextBox runat="server" ID="txtPaperno" MaxLength="18" CssClass="input"  Width="135px" />&nbsp;&nbsp;
                                </td>
                                <td style="width: 10%; height: 25px;" align="right">
                                    卡号:
                                </td>
                                <td style="width: 20%; height: 25px;" valign="middle">
                                    <asp:TextBox runat="server" ID="txtCardNo" MaxLength="16" CssClass="input"  Width="135px" />&nbsp;&nbsp;
                                </td>
                                <td style="width: 5%; height: 25px;" align="right">
                                    &nbsp;</td>
                                <td style="width: 15%; height: 25px;">
                                    <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" />
                                </td>
                                <td style="width: 20%; height: 25px;">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div class="base">
                    </div>
                    <div class="kuang5">
                        <div class="gdtb" style="height: 130px">
                            <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="True" OnSelectedIndexChanged="gv_SelectedIndexChanged"
                                Width="98%" CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                                SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                                PagerStyle-VerticalAlign="Top" EmptyDataText="没有数据记录!" AllowPaging="False">
                                <PagerSettings Mode="NumericFirstLast" />
                                <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <SelectedRowStyle CssClass="tabsel" />
                                <HeaderStyle CssClass="tabbt" />
                                <AlternatingRowStyle CssClass="tabjg" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="basicinfo">
                    <div class="money">
                        费用信息</div>
                    <div class="kuang5">
                        <div style="height: 170px">
                            <table width="98%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                <tr class="tabbt">
                                    <td width="66">
                                        费用项目</td>
                                    <td width="94">
                                        费用金额(元)</td>
                                </tr>
                                <tr>
                                    <td>
                                        挂失</td>
                                    <td>
                                        0.00</td>
                                </tr>
                                <tr class="tabjg">
                                    <td>
                                        解挂</td>
                                    <td>
                                        0.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        其他费用</td>
                                    <td>
                                        0.00</td>
                                </tr>
                                <tr>
                                    <tr class="tabjg">
                                        <td>
                                            合计应收</td>
                                        <td>
                                            0.00</td>
                                    </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="pipinfo">
                    <div class="info">
                        卡片信息</div>
                    <div class="kuang5"  style="height:65px">
                        <table width="98%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        用户卡号:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCardno" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        卡序列号:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblAsn" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        卡片类型:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblCardtype" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        卡片状态:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCardstate" runat="server" Text="" Width="100%"></asp:Label>
                                    <input type="hidden" id="hidCardstate" runat="server" />
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        启用日期:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblSDate" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        结束日期:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblEDate" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        账户余额:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCardaccmoney" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td colspan="4">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="pipinfo">
                    <div class="pip">
                        用户信息</div>
                    <div class="kuang5"  style="height:65px">
                        <table width="98%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        用户姓名:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCustName" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        出生日期:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblCustBirthday" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        证件类型:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblPapertype" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        用户性别:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCustsex" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        联系电话:</div>
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lblCustphone" runat="server" Text="" Width="100%"></asp:Label>
                                    <td width="15%">
                                        <div align="right">
                                            证件号码:</div>
                                    </td>
                                    <td width="20%">
                                        <asp:Label ID="lblPaperno" runat="server" Text="" Width="100%"></asp:Label>
                                    </td>
                            </tr>
                            <tr>
                                <td width="12%">
                                    <div align="right">
                                        邮政编码:</div>
                                </td>
                                <td width="18%">
                                    <asp:Label ID="lblCustpost" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                                <td width="15%">
                                    <div align="right">
                                        联系地址:</div>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblCustaddr" runat="server" Text="" Width="100%"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="btns">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="55%">
                                <asp:CheckBox ID="chkPingzheng" runat="server" Checked="true" Text="自动打印凭证" />
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="btnPrintPZ" runat="server" Text="打印凭证" CssClass="button1" Enabled="false"
                                    OnClientClick="printdiv('ptnPingZheng1')" />
                            </td>
                            <td align="right">
                                <asp:RadioButton ID="rdoReportLoss" Enabled="false" AutoPostBack="true" Text="口头挂失"
                                    runat="server" GroupName="ReportLoss" OnCheckedChanged="rdo_CheckedChanged" />
                            </td>
                            <td align="right">
                                <asp:RadioButton ID="rdoRealReportLoss" Enabled="false" AutoPostBack="true" Text="书面挂失"
                                    runat="server" GroupName="ReportLoss" OnCheckedChanged="rdo_CheckedChanged" />
                            </td>
                            <td align="right">
                                <asp:RadioButton ID="rdoCancelLoss" Enabled="false" AutoPostBack="true" Text="解挂"
                                    runat="server" GroupName="ReportLoss" OnCheckedChanged="rdo_CheckedChanged" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                    OnClick="btnSubmit_Click" /></td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
