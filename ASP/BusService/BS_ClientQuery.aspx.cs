﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TM;
using TDO.UserManager;
using TDO.Financial;

public partial class ASP_BusService_BS_ClientQuery : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataTable dt = new DataTable();
            gvResult.DataSource = dt;
            gvResult.DataBind();
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);
        if (string.IsNullOrEmpty(txtCardNo.Text.Trim()) && string.IsNullOrEmpty(txtPaperNo.Text.Trim()) && string.IsNullOrEmpty(txtCustName.Text.Trim()))
        {
            context.AddError("卡号、证件号码、姓名至少输入一项");
        }

        if (!string.IsNullOrEmpty(txtCardNo.Text.Trim()))
        {
            if (!Validation.isNum(txtCardNo.Text.Trim()))
            {
                context.AddError("A094780140:卡号只能为数字", txtCardNo);
            }

            if (txtCardNo.Text.Trim().Length > 8)
            {
                context.AddError("A094780141:卡号长度不能大于8位",txtCardNo);
            }
        }

        if (!string.IsNullOrEmpty(txtPaperNo.Text.Trim()))
        {
            if (!Validation.isCharNum(txtPaperNo.Text.Trim()))
            {
                context.AddError("A094780142:证件号码只能为英数", txtPaperNo);
            }
        }

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        DataTable data;
        data = SPHelper.callPBQuery(context, "ClientQuery", txtCardNo.Text, txtPaperNo.Text, txtCustName.Text);


        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }

    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

}
