<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BS_ClientQuery.aspx.cs" Inherits="ASP_BusService_BS_ClientQuery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>公交卡-客户信息查询</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
        <div class="tb">
		    公交卡-客户信息查询
	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->
	        <div class="con">

	           <div class="card">查询</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td style="width:100px;"><div align="right">卡号:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCardNo" CssClass="inputmid" MaxLength="8"></asp:TextBox>
                        </td>
                        
                        <td><div align="right">证件号码:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPaperNo" CssClass="inputmid"></asp:TextBox>
                        </td>
                        <td><div align="right">姓名:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCustName" CssClass="inputmid"></asp:TextBox>
                        </td>
                        <td align="right">
                            <asp:Button ID="Button1" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
               </table>
               
             </div>
	
	        <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div style="height:380px;overflow:auto;">
                    <asp:GridView ID="gvResult" runat="server"
                            Width = "100%"
                            CssClass="tab2"
                            AllowPaging="True"
                            PageSize="50"
                            HeaderStyle-CssClass="tabbt" 
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="False"
                            OnPageIndexChanging="gvResult_Page">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="20%" HeaderText="卡号" DataField="CARDNO" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderText="姓名" DataField="CUSTNAME" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderText="性别" DataField="CUSTSEX" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderText="证件号码" DataField="PAPERNO" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderText="联系电话" DataField="CUSTPHONE" />
                            </Columns>
                           <EmptyDataTemplate>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                              <tr class="tabbt">
                                <td width="20%">卡号</td>
                                <td width="20%">姓名</td>
                                <td width="20%">性别</td>
                                <td width="20%">身份证号</td>
                                <td width="20%">联系电话</td>
                              </tr>
                              </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    
                </div>
              </div>
            </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
