﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using TDO.CardManager;
using TDO.BusinessCode;
using Common;
using PDO.BusService;
using TDO.UserManager;
using TDO.ResourceManager;
using PDO.PersonalBusiness;

public partial class ASP_BusService_BS_ReturnSpecialCardMoney : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!context.s_Debugging)
            setReadOnly(LabAsn, sDate, eDate, cMoney);

            initLoad(sender, e);
        }
    }

    protected void initLoad(object sender, EventArgs e)
    {
        hiddenCardState.Value = "";
        hidCancelDepositFee.Value = "0.00";
        hidCancelSupplyFee.Value = "0.00";
        ProcedureFee.Text = "0.00";
        OtherFee.Text = "0.00";

        CancelDepositFee.Text = hidCancelDepositFee.Value;
        CancelSupplyFee.Text = hidCancelSupplyFee.Value;
        Total.Text = (Convert.ToDecimal(CancelDepositFee.Text) + Convert.ToDecimal(CancelSupplyFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        ReturnSupply.Text = Total.Text;
    }

    protected void readInfo(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001002111");
            return;
        }

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //给页面显示项赋值
        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期赋值
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else CustBirthday.Text = Bdate;
        }
        else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

        //证件类型赋值
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;
    }

    protected void selReturnType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selReturnType.SelectedValue == "01" || selReturnType.SelectedValue == "02")
        {
            txtCardno.Attributes.Remove("readonly");
            txtCardno.CssClass = "input";
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = true;
            selStateType.Enabled = false;
            selStateType.SelectedValue = "";
        }
        else if (selReturnType.SelectedValue == "04")
        {
            if (!context.s_Debugging) setReadOnly(txtCardno);
            txtCardno.CssClass = "labeltext";
            btnReadCard.Enabled = true;
            btnDBRead.Enabled = false;
            selStateType.Enabled = false;
            selStateType.SelectedValue = "11";
        }
        else if (selReturnType.SelectedValue == "03")
        {
            selStateType.Enabled = true;
            selStateType.SelectedValue = "11";
            btnReadCard.Enabled = true;
            btnDBRead.Enabled = false;
        }
        else
        {
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = false;
            selStateType.Enabled = false;
            selStateType.SelectedValue = "";
        }
     
        btnReturnMoney.Enabled = false;
        initLoad(sender, e);

    }

    protected void selStateType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selStateType.SelectedValue == "11")
        {
            if (!context.s_Debugging) setReadOnly(txtCardno);
            txtCardno.CssClass = "labeltext";
            btnReadCard.Enabled = true; 
            btnDBRead.Enabled = false;
        }
        //退卡类型为不可读卡时,读卡按钮不可用,读数据库按钮可用
        else if (selStateType.SelectedValue == "15")
        {
            txtCardno.Attributes.Remove("readonly");
            txtCardno.CssClass = "input";
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = true; 
        }
        else
        {
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = false; 
        }
    }
    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!DBreadValidation())
            return;

        //高龄老人卡能退款
        DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardSurface", txtCardno.Text.Trim());
        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddError("未找到该特种卡卡面类型");
            return;
        }
        else
        {
            if (dt.Rows[0].ItemArray[0].ToString() != "9901")
            {
                context.AddError("非高龄老年卡不允许退款");
                return;
            }
        }

        //验证是否已经退过款
        DataTable data = BusCardHelper.callQuery(context, "QryIsReturnMoney", txtCardno.Text.Trim());
        if (data.Rows.Count > 0)
        {
            string returnDate = data.Rows[0].ItemArray[0].ToString();
            context.AddError("已经退过款，无法再退款，退款时间 " + returnDate);
            return;
        }

        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验
        //SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        //pdo.CARDNO = txtCardno.Text;

        //Master.PDOBase pdoOut;
        //bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        //if (ok)
        //{
            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            //从卡资料表(TF_F_CARDREC)中读取数据
            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text.Trim();

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
            sellTime.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");
            hiddenCardState.Value = ddoTF_F_CARDRECOut.CARDSTATE;//退款时判断是否已换卡或挂失

            hidDEPOSIT.Value = (-Convert.ToDecimal(ddoTF_F_CARDRECOut.DEPOSIT) / 100).ToString("0.00");
            hidService.Value = (Convert.ToDecimal(ddoTF_F_CARDRECOut.SERVICEMONEY) / 100).ToString("0.00");

            //从IC卡电子钱包帐户表(TF_F_CARDEWALLETACC)中读取数据
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text.Trim();
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

            //给页面显示项赋值
            hidSERSTAKETAG.Value = ddoTF_F_CARDRECOut.SERSTAKETAG;
            hiddenLabCardtype.Value = ddoTF_F_CARDRECOut.CARDTYPECODE;
            LabAsn.Text = ddoTF_F_CARDRECOut.ASN;
            sDate.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");

            String Vdate = ddoTF_F_CARDRECOut.VALIDENDDATE;
            eDate.Text = Vdate.Substring(0, 4) + "-" + Vdate.Substring(4, 2) + "-" + Vdate.Substring(6, 2);

            Decimal cardMoney = Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY);

            //计算退款金额
            if (cardMoney > 0)
            {
                //系统内存在2条及以上充值记录，且2条及以上为非0记录
                if (BusCardHelper.GetSupplyRightCount(context, this.txtCardno.Text.Trim()) >= 2)
                {
                    cMoney.Text = (cardMoney / 100).ToString("0.00");
                    //CancelSupplyFee.Text = (cardMoney / 100).ToString("0.00");
                    CancelSupplyFee.Text = (- Convert.ToDecimal(cardMoney / 100)).ToString("0.00");
                }
                else
                {
                    cMoney.Text = "0.00";
                    CancelSupplyFee.Text = "0.00";
                }
            }
            else
            {
                cMoney.Text = "0.00";
                CancelSupplyFee.Text = "0.00";
            }
            if (cardMoney <= 0)
            {
                context.AddError("当前余额为0,不允许退款.");
                return;
            }

            Total.Text = (Convert.ToDecimal(CancelDepositFee.Text) + Convert.ToDecimal(CancelSupplyFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
            ReturnSupply.Text = Total.Text;

            PBHelper.openFunc(context, openFunc, txtCardno.Text);
            readInfo(sender, e);

            btnReturnMoney.Enabled = !context.hasError();
            btnPrintPZ.Enabled = false;
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        //对卡号进行检验
        if (!DBreadValidation())
            return;

        //高龄老人卡能退款
        DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardSurface", txtCardno.Text.Trim());
        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddError("未找到该特种卡卡面类型");
            return;
        }
        else
        {
            if (dt.Rows[0].ItemArray[0].ToString() != "9901")
            {
                context.AddError("非高龄老年卡不允许退款");
                return;
            }
        }

        //验证是否已经退过款
        DataTable data = BusCardHelper.callQuery(context, "QryIsReturnMoney", txtCardno.Text.Trim());
        if (data.Rows.Count > 0)
        {
            string returnDate = data.Rows[0].ItemArray[0].ToString();
            context.AddError("已经退过款，无法再退款，退款时间 " + returnDate);
            return;
        }

        TMTableModule tmTMTableModule = new TMTableModule();
        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        //从资源状态编码表中读取数据
        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTD_M_RESOURCESTATEOut == null)
            RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        //页面显示项赋值
        LabAsn.Text = ddoTL_R_ICUSEROut.ASN;
        sDate.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
        eDate.Text = hiddeneDate.Value.Substring(0, 4) + "-" + hiddeneDate.Value.Substring(4, 2) + "-" + hiddeneDate.Value.Substring(6, 2);
        cMoney.Text = ((Convert.ToDecimal(hiddencMoney.Value)) / 100).ToString("0.00");

        //从卡资料表(TF_F_CARDREC)中读取数据
        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;
        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        hiddenCardState.Value = ddoTF_F_CARDRECOut.CARDSTATE;//退款时判断是否已换卡或挂失
        sellTime.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");
        //当天出售的卡不能退卡
        if (ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyyMMdd") == DateTime.Now.ToString("yyyyMMdd"))
        {
            context.AddError("A001007129");
            return;
        }

        Decimal cardMoney = Convert.ToDecimal(cMoney.Text);
        //计算退款金额
        if (cardMoney > 0)
        {
            //系统内存在2条及以上充值记录，且2条及以上为非0记录
            if (BusCardHelper.GetSupplyRightCount(context, this.txtCardno.Text.Trim()) >= 2)
            {
                cMoney.Text = (cardMoney / 100).ToString("0.00");
                CancelSupplyFee.Text = (-Convert.ToDecimal(cardMoney / 100)).ToString("0.00");
            }
            else
            {
                cMoney.Text = "0.00";
                CancelSupplyFee.Text = "0.00";
            }
        }
        else
        {
            cMoney.Text = "0.00";
            CancelSupplyFee.Text = "0.00";
        }
        if (cardMoney <= 0)
        {
            context.AddError("当前余额为0,不允许退款.");
            return;
        }

        Total.Text = (Convert.ToDecimal(CancelDepositFee.Text) + Convert.ToDecimal(CancelSupplyFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        ReturnSupply.Text = Total.Text;

        PBHelper.openFunc(context, openFunc, txtCardno.Text);
        readInfo(sender, e);

        btnReturnMoney.Enabled = !context.hasError();
        btnPrintPZ.Enabled = false;

    }
    protected void btnReturnMoney_Click(object sender, EventArgs e)
    {

        //判断卡号是否为空
        if (txtCardno.Text.Trim() == "")
        {
            context.AddError("A001008100", txtCardno);
            return;
        }
        if (selReturnType.SelectedValue == "")
        {
            context.AddError("退款类型不能为空");
            return;
        }
        //检查挂失记录
        if (selReturnType.SelectedValue == "01")
        {
            if (hiddenCardState.Value != "03" && hiddenCardState.Value != "04")
            {
                context.AddError("卡片不是挂失状态");
                return;
            }
        }
        //检查换卡记录
        if (selReturnType.SelectedValue == "02" && hiddenCardState.Value != "22")
        {
            context.AddError("卡片未做过换卡，卡状态不符");
            return;
        }

        SP_BS_SpecialReturnMoneyPDO pdo = new SP_BS_SpecialReturnMoneyPDO();
        //存储过程赋值
        pdo.CARDNO = txtCardno.Text;
        pdo.ASN = LabAsn.Text;
        pdo.CARDTYPECODE = hiddenLabCardtype.Value;
        pdo.REASONCODE = selStateType.SelectedValue;
        pdo.CARDMONEY = Convert.ToInt32(Convert.ToDecimal(cMoney.Text) * 100);
        pdo.REFUNDMONEY = Convert.ToInt32(Convert.ToDecimal(CancelSupplyFee.Text) * 100);
        pdo.REFUNDDEPOSIT = Convert.ToInt32(Convert.ToDecimal(CancelDepositFee.Text) * 100);
        pdo.TRADEPROCFEE = Convert.ToInt32(Convert.ToDecimal(ProcedureFee.Text) * 100);
        pdo.OTHERFEE = Convert.ToInt32(Convert.ToDecimal(OtherFee.Text) * 100);
        pdo.OPERCARDNO = context.s_CardID;
        pdo.TERMNO = "112233445566";
        pdo.CARDTRADENO = LabAsn.Text.Substring(0, 4);
        pdo.ID = DealString.GetRecordID(hiddentradeno.Value, LabAsn.Text);
        pdo.SERSTAKETAG = "3";
        pdo.CHECKSTAFFNO = context.s_UserID;
        pdo.CHECKDEPARTNO = context.s_DepartID;

        Master.PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            if (selReturnType.SelectedValue == "03" && selStateType.SelectedValue == "11")
            {
                //可读卡退卡需要锁卡
                ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                    "lockCard();", true);
            }
            AddMessage("退款成功");
            btnPrintPZ.Enabled = true;
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }
            string tradeTypeName = "退款";
            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, CustName.Text, tradeTypeName, Total.Text
              , hidDEPOSIT.Value, "", Paperno.Text, "0.00", hidService.Value,
              Total.Text, context.s_UserName, context.s_DepartName, Papertype.Text, ProcedureFee.Text, "0.00");
            initLoad(sender, e);
            btnReturnMoney.Enabled = false;
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnReturnMoney.Enabled = true;
            return;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            AddMessage("退卡成功");

            clearCustInfo(txtCardno);
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnDBRead_Click(sender, e);
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        hidWarning.Value = "";
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001008100", txtCardno);
        else
        {
            if ((txtCardno.Text.Trim()).Length != 8)
                context.AddError("A900010B94: 卡号位数必须为8位", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001008102", txtCardno);
        }

        return !(context.hasError());
    }
}
