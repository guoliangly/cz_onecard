﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.BusinessCode;
using TM;
using TDO.CardManager;
using PDO.PersonalBusiness;
using TDO.UserManager;
using TDO.ResourceManager;

/***************************************************************
 * 功能名: 公交业务_挂失卡退租金
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/16    liuh			初次开发
 ****************************************************************/
public partial class ASP_BusService_BS_ReturnLossCard : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            if (!context.s_Debugging) setReadOnly(txtCardno);
            setReadOnly(txtAsn);

            this.CancelDepositFee.Text = "0.00";
            this.ProcedureFee.Text = "0.00";
            this.OtherFee.Text = "0.00";
            this.Total.Text = "0.00";
            this.ReturnSupply.Text = "0.00";
        }
    }

    /// <summary>
    /// 读卡
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        //验证卡号、售卡日期、卡上押金

        if (this.txtCardno.Text.Trim().Length == 0
            || this.hiddensDate.Value.Trim().Length == 0
            || this.hiddenCardRent.Value.Trim().Length == 0)
        {
            context.AddError("卡内信息不完整");
            return;
        }

        TMTableModule tmTMTableModule = new TMTableModule();
        //从用户卡库存表(TL_R_ICUSER)中读取数据

        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut != null)
        {
            //从卡资料表(TF_F_CARDREC)中读取数据

            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;
            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //挂失的卡才可以退押金，03挂失、23挂失转值、24挂失卡销户

            if (ddoTF_F_CARDRECOut.CARDSTATE != "03"
                && ddoTF_F_CARDRECOut.CARDSTATE != "23"
                 && ddoTF_F_CARDRECOut.CARDSTATE != "24")
            {
                context.AddError("A001007192:当前卡片不是挂失卡，不能退押金");
                return;
            }

            //从月票表中取公交卡类型

            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
            ddoTF_F_CARDCOUNTACCIn.CARDNO = ddoTF_F_CARDRECOut.CARDNO;
            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC", null);
            if (ddoTF_F_CARDCOUNTACCOut == null)
            {
                context.AddError("A001002113");
                return;
            }
            //公交类型编码
            TD_M_APPAREATDO ddoTD_M_APPAREATDOIn = new TD_M_APPAREATDO();
            ddoTD_M_APPAREATDOIn.AREACODE = ddoTF_F_CARDCOUNTACCOut.ASSIGNEDAREA;
            TD_M_APPAREATDO ddoTD_M_APPAREATDOOut = (TD_M_APPAREATDO)tmTMTableModule.selByPK(context, ddoTD_M_APPAREATDOIn, typeof(TD_M_APPAREATDO), null, "TD_M_APPAREA", null);

            #region 卡片信息
            this.txtAsn.Text = ddoTL_R_ICUSEROut.ASN;
            RESSTATE.Text = this.GetCardstateNameByCode(ddoTF_F_CARDRECOut.CARDSTATE);

            this.txtCardtype.Text = ddoTD_M_APPAREATDOOut.AREANAME;
            this.txtIsHave.Text = "是";

            PBHelper.openFunc(context, openFunc, txtCardno.Text);
            #endregion

            #region 用户信息
            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001002111");
                return;
            }

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            //给页面显示项赋值

            CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                Custsex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                Custsex.Text = "女";
            else Custsex.Text = "";
            //出生日期赋值

            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            //证件类型赋值

            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;
            #endregion

        }
        else
        {
            if (this.txtCardno.Text.Trim().Length != 8)
            {
                context.AddError("A001007194:非老公交卡");
                return;
            }

            #region 库中无此卡则清空客户信息等显示

            clearCustInfo(txtAsn);
            clearCustInfo(RESSTATE);
            clearCustInfo(txtCardtype);
            clearCustInfo(CustName);
            clearCustInfo(Custsex);
            clearCustInfo(CustBirthday);
            clearCustInfo(Papertype);
            clearCustInfo(Paperno);
            clearCustInfo(Custaddr);
            clearCustInfo(Custpost);
            clearCustInfo(Custphone);
            clearCustInfo(txtEmail);
            clearCustInfo(Remark);
            clearCustInfo(Custphone);
            clearCustInfo(Custphone);
            #endregion

            this.txtIsHave.Text = "否";
        }

        if (hiddenCardPermit.Value.Trim().Length != 0)//"00":正常，"04":锁卡
        {
            this.txtIsLocked.Text = this.hiddenCardPermit.Value == "04" ? "是" : "否";
        }
        if (hiddensDate.Value.Trim().Length != 0)//从卡里取的售卡时间
        {
            this.txtSellTime.Text = hiddensDate.Value.Substring(0, 4) + "-" + hiddensDate.Value.Substring(4, 2) + "-" + hiddensDate.Value.Substring(6, 2);
        }
        if (hiddenCardRent.Value.Trim().Length != 0)//从卡里取的卡租金
        {
            #region 计算退卡余额

            this.txtDeposit.Text = (-(Convert.ToDecimal(hiddenCardRent.Value)) / (Convert.ToDecimal(100))).ToString("0.00");

            int deposit = Convert.ToInt32(hiddenCardRent.Value);//卡上租金
            int returnDeposit;//退租金

            DateTime sellTime = DateTime.Parse(this.txtSellTime.Text);
            DateTime referTime = DateTime.Parse("2010-5-1");

            if (sellTime.CompareTo(referTime) < 0)//2010-5-1之前购卡的
            {
                int formerMonth = (referTime.Year * 12 + referTime.Month) - (sellTime.Year * 12 + sellTime.Month);//（2010-5-1 - selltime）

                int backMonth = (DateTime.Now.Year * 12 + DateTime.Now.Month) - (referTime.Year * 12 + referTime.Month);//(now - 2010-5-1)

                returnDeposit = deposit - formerMonth * 100 - backMonth * 50;

            }
            else
            {//2010-5-1和之后购卡的
                int backMonth = (DateTime.Now.Year * 12 + DateTime.Now.Month) - (sellTime.Year * 12 + sellTime.Month);//(now - selltime)

                returnDeposit = deposit - backMonth * 50;
            }
            #endregion

            if (returnDeposit <= 0)
            {
                context.AddError("租金已扣完，无租金余额可退");
                return;
            }
            else
            {
                this.CancelDepositFee.Text = ((Convert.ToDecimal(-returnDeposit)) / (Convert.ToDecimal(100))).ToString("0.00");
                this.Total.Text = this.CancelDepositFee.Text;
                this.ReturnSupply.Text = this.CancelDepositFee.Text;
            }
        }

        btnReturn.Enabled = !context.hasError();

        btnPrintPZ.Enabled = false;
    }

    /// <summary>
    /// 写卡成功后执行

    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnReturn.Enabled = true;
            return;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            AddMessage("退卡租金成功");

            clearCustInfo(txtCardno);
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnReadCard_Click(sender, e);
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
    }

    /// <summary>
    /// 退租金按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        context.SPOpen();
        context.AddField("p_cardNo").Value = this.txtCardno.Text;
        context.AddField("p_carddeposit").Value = Convert.ToInt32(Convert.ToDecimal(this.txtDeposit.Text) * 100);
        context.AddField("p_selltime").Value = this.txtSellTime.Text.Trim();
        context.AddField("p_currentmoney").Value = Convert.ToInt32(Convert.ToDecimal(CancelDepositFee.Text) * 100);
        context.AddField("p_ishave").Value = this.txtIsHave.Text.Trim() == "是" ? "1" : "0"; ;
        context.AddField("p_ID").Value = DealString.GetRecordID(hiddentradeno.Value, this.txtCardno.Text);
        context.AddField("p_CARDTRADENO").Value = this.hiddentradeno.Value;

        bool ok = context.ExecuteSP("sp_bs_returnLossCard");

        if (ok)
        {
            btnPrintPZ.Enabled = true;

            //打印凭证
            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, CustName.Text, "挂失卡退租金", CancelDepositFee.Text
                , "0.00", "", Paperno.Text, "0.00", "0.00",
                Total.Text, context.s_UserName, context.s_DepartName, Papertype.Text, ProcedureFee.Text, "0.00");


            if (this.hiddenCardPermit.Value == "04")//已锁卡
            {
                AddMessage("退卡租金成功");
                if (chkPingzheng.Checked && btnPrintPZ.Enabled)
                {
                    ScriptManager.RegisterStartupScript(
                        this, this.GetType(), "writeCardScript",
                        "printdiv('ptnPingZheng1');", true);
                }
                clearCustInfo(txtCardno);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript",
                      "lockCard();", true);

            }
            this.btnReturn.Enabled = false;
        }
    }
}
