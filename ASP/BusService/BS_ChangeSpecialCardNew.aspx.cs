﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using TDO.ResourceManager;
using TDO.UserManager;
using TDO.BusinessCode;
using TDO.CardManager;
using PDO.PersonalBusiness;
using Common;
using TDO.BalanceChannel;
using PDO.BusService;

public partial class ASP_BusService_BS_ChangeSpecialCardNew : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //btnDBRead.Attributes["onclick"] = "warnCheck()";
            setReadOnly(OsDate, ODeposit, OldcMoney);
            setReadOnly(OsDate, ODeposit, OldcMoney);
            txtRealRecv.Attributes["onfocus"] = "this.select();";
            txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this,'test', 'hidAccRecv');";
            if (!context.s_Debugging) setReadOnly(txtOCardno);

            //初始化换卡类型
            ASHelper.setChangeReason(selReasonType, false);

            initLoad();
            hidAccRecv.Value = Total.Text;
        }
    }

    protected void initLoad()
    {
        hidReadSpecialCardType.Value = "";
        hiddenDepositFee.Value = "0.00";
        if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
        {
            hiddenCardcostFee.Value = "10.00";
        }
        else
        {
            hiddenCardcostFee.Value = "0.00";
        }
        hidProcedureFee.Value = "0.00";
        hidOtherFee.Value = "0.00";
        //费用赋值
        DepositFee.Text = hiddenDepositFee.Value;
        CardcostFee.Text = hiddenCardcostFee.Value;
        ProcedureFee.Text = hidProcedureFee.Value;
        OtherFee.Text = hidOtherFee.Value;
        Total.Text = (Convert.ToDecimal(DepositFee.Text) + Convert.ToDecimal(CardcostFee.Text) + Convert.ToDecimal(ProcedureFee.Text) + Convert.ToDecimal(OtherFee.Text)).ToString("0.00");
        txtRealRecv.Text = Total.Text;
        Cardcost.Checked = true;
        Change.Enabled = false;
    }

    //换卡类型改变时,读卡和读数据库按钮是否可用改变
    protected void selReasonType_SelectedIndexChanged(object sender, EventArgs e)
    {
        initLoad();
        //换卡类型为可读卡时,读数据库按钮不可用,读卡按钮可用
        if (selReasonType.SelectedValue == "13" || selReasonType.SelectedValue == "12")
        {
            if (!context.s_Debugging) setReadOnly(txtOCardno);
            btnReadCard.Enabled = true;
            btnDBRead.Enabled = false;
            txtOCardno.CssClass = "labeltext";
        }
        //换卡类型为不可读卡时,读卡不可用,读数据库可用
        else if (selReasonType.SelectedValue == "14" || selReasonType.SelectedValue == "15")
        {
            txtOCardno.Attributes.Remove("readonly");
            btnReadCard.Enabled = false;
            btnDBRead.Enabled = true;
            txtOCardno.CssClass = "input";
        }
        if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
        {
            hiddenCardcostFee.Value = "10.00";
        }
        else 
        { 
            hiddenCardcostFee.Value = "0.00"; 
        }
    }

    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验
        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtOCardno.Text;

        Master.PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            //从IC卡资料表中读取数据
            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtOCardno.Text;

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtOCardno.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);
            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            #region 获取用户信息
            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }

            CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            //性别显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                Custsex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                Custsex.Text = "女";
            else Custsex.Text = "";

            //出生日期显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            //证件类型显示
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

            hidUserName.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            hidUserSex.Value = ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0" ? "01" //写卡时转换性别编码
                             : ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1" ? "02"
                             : "";
            hidUserPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            hidUserPhone.Value = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            #endregion

            //给页面显示项赋值
            hidOldCardcost.Value = ddoTF_F_CARDRECOut.CARDCOST.ToString();
            SERTAKETAG.Value = ddoTF_F_CARDRECOut.SERSTAKETAG;
            OSERVICEMOENY.Value = ddoTF_F_CARDRECOut.SERVICEMONEY.ToString();
            SERSTARTIME.Value = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString();
            CUSTRECTYPECODE.Value = ddoTF_F_CARDRECOut.CUSTRECTYPECODE;
            //txtOCardno.Text = hiddentxtCardno.Value;
            ODeposit.Text = (Convert.ToDecimal(ddoTF_F_CARDRECOut.DEPOSIT) / (Convert.ToDecimal(100))).ToString("0.00");
            OsDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");
            OldcMoney.Text = (Convert.ToDecimal(hiddencMoney.Value) / (Convert.ToDecimal(100))).ToString("0.00");

            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtOCardno.Text);

            //获取旧卡的公交类型
            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
            ddoTF_F_CARDCOUNTACCIn.CARDNO = txtOCardno.Text;

            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC_CARDNO", null);
            if (ddoTF_F_CARDCOUNTACCOut == null)
            {
                context.AddError("此旧卡非特种卡");
                return;
            }
            //读取特种卡卡面类型
            DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardSurface", txtOCardno.Text.Trim());
            if (dt == null || dt.Rows.Count == 0)
            {
                context.AddError("未找到该特种卡卡面类型");
                return;
            }
            else
            {
                lblOSpecialCardName.Text = dt.Rows[0].ItemArray[1].ToString().Trim();
                hiddenOSpecialCardType.Value = dt.Rows[0].ItemArray[0].ToString();
            }

            hidCardReaderToken.Value = cardReader.createToken(context);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "lockCard();", true);

            btnPrintPZ.Enabled = false;
            btnPrintSJ.Enabled = false;

            hidLockFlag.Value = "true";
        }
        else if (pdoOut.retCode == "A001107199")
        {
            //验证如果是黑名单卡，锁卡
            this.LockBlackCard(txtOCardno.Text);
            this.hidLockBlackCardFlag.Value = "yes";
        }
        //换卡按钮可用
        Change.Enabled = true;
    }

    private Boolean DBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtOCardno.Text.Trim() == "")
            context.AddError("A001004113", txtOCardno);
        else
        {
            if ((txtOCardno.Text.Trim()).Length != 16 && (txtOCardno.Text.Trim()).Length != 8)
                context.AddError("A001004114", txtOCardno);
            else if (!Validation.isNum(txtOCardno.Text.Trim()))
                context.AddError("A001004115", txtOCardno);
        }

        return !(context.hasError());
    }

    protected void btnDBRead_Click(object sender, EventArgs e)
    {
        //对输入卡号进行检验
        if (!DBreadValidation())
            return;

        TMTableModule tmTMTableModule = new TMTableModule();
        //卡账户有效性检验
        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtOCardno.Text;

        Master.PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            //从IC卡资料表中读取数据
            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtOCardno.Text;

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //从IC卡电子钱包账户表中读取数据
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtOCardno.Text;

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtOCardno.Text;
            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);
            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            #region 获取用户信息
            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据


            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001107112");
                return;
            }


            CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            //性别显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                Custsex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                Custsex.Text = "女";
            else Custsex.Text = "";

            //出生日期显示
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据


            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);


            //证件类型显示
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;


            hidUserName.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
            hidUserSex.Value = ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0" ? "01" //写卡时转换性别编码
                             : ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1" ? "02"
                             : "";
            hidUserPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            hidUserPhone.Value = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            #endregion

            //给页面显示项赋值
            hidOldCardcost.Value = ddoTF_F_CARDRECOut.CARDCOST.ToString();
            //hiddenAsn.Value = ddoTF_F_CARDRECOut.ASN;
            SERTAKETAG.Value = ddoTF_F_CARDRECOut.SERSTAKETAG;
            OSERVICEMOENY.Value = ddoTF_F_CARDRECOut.SERVICEMONEY.ToString();
            SERSTARTIME.Value = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString();
            CUSTRECTYPECODE.Value = ddoTF_F_CARDRECOut.CUSTRECTYPECODE;
            ODeposit.Text = (Convert.ToDecimal(ddoTF_F_CARDRECOut.DEPOSIT) / 100).ToString("0.00");
            OsDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyy-MM-dd");
            OldcMoney.Text = (Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY) / 100).ToString("0.00");

            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtOCardno.Text);

            //获取旧卡的公交类型
            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
            ddoTF_F_CARDCOUNTACCIn.CARDNO = txtOCardno.Text;

            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC_CARDNO", null);
            if (ddoTF_F_CARDCOUNTACCOut == null)
            {
                context.AddError("此旧卡非学生卡或老人卡或高龄卡");
                return;
            }
            //读取特种卡卡面类型
            DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardSurface", txtOCardno.Text.Trim());
            if (dt == null || dt.Rows.Count == 0)
            {
                context.AddError("未找到该特种卡卡面类型");
                return;
            }
            else
            {
                lblOSpecialCardName.Text = dt.Rows[0].ItemArray[1].ToString().Trim();
                hiddenOSpecialCardType.Value = dt.Rows[0].ItemArray[0].ToString();

                string typehidden = hiddenOSpecialCardType.Value;
                hidReadSpecialCardType.Value
                        = typehidden == "9901" ? "J"
                        : typehidden == "9902" ? "I"
                        : typehidden == "9903" ? "P"
                        : "";
            }
            //hiddentxtCardno.Value = txtOCardno.Text;

            //换卡按钮可用
            Change.Enabled = true;

            btnPrintPZ.Enabled = false;
            btnPrintSJ.Enabled = false;
        }
        else if (pdoOut.retCode == "A001107199")
        {//验证如果是黑名单卡，锁卡
            this.LockBlackCard(txtOCardno.Text);
            this.hidLockBlackCardFlag.Value = "yes";
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {

            Change_Click(sender, e);
            hidWarning.Value = "";
            return;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            #region 如果是前台黑名单锁卡
            if (this.hidLockBlackCardFlag.Value == "yes")
            {
                AddMessage("黑名单卡已锁");
                clearCustInfo(txtOCardno);
                //clearCustInfo(txtOCardno, txtNCardno);
                this.hidLockBlackCardFlag.Value = "";
                return;
            }
            #endregion

            if (hidLockFlag.Value == "true")
            {
                AddMessage("旧卡锁定成功");
            }
            else
            {
                changeSpecialCard();
              
                clearCustInfo(txtOCardno);
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnDBRead_Click(sender, e);
        }

        if (chkPingzheng.Checked && btnPrintPZ.Enabled && chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printAll();", true);
        }
        else if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }
        else if (chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printShouJu();", true);
        }
        hidLockFlag.Value = "";
        hidWarning.Value = "";
    }

    protected void Change_Click(object sender, EventArgs e)
    {
        //判断售卡权限
        //checkCardState(txtNCardno.Text);

        if (txtRealRecv.Text == null)
        {
            context.AddError("A001001143");
            return;
        }
        //公交类型判断
        if (hiddenOSpecialCardType.Value == "")
        {
            context.AddError("旧卡公交类型不能为空");
        }

        TMTableModule tmTMTableModule = new TMTableModule();
        //获取旧卡的公交类型
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
        ddoTF_F_CARDCOUNTACCIn.CARDNO = txtOCardno.Text;

        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC_CARDNO", null);
        if (ddoTF_F_CARDCOUNTACCOut == null)
        {
            context.AddError("此旧卡非特种卡");
            return;
        }

        //读取审核员工信息
        TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        ddoTD_M_INSIDESTAFFIn.OPERCARDNO = hiddenCheck.Value;
        TD_M_INSIDESTAFFTDO[] ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_CHECK", null);
        if (selReasonType.SelectedValue == "14" || selReasonType.SelectedValue == "15")
        {
            if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.Length == 0)
            {
                context.AddError("A001010108");
                return;
            }
            hidCheckStaffNo.Value = ddoTD_M_INSIDESTAFFOut[0].STAFFNO;
            hidCheckDepartNo.Value = ddoTD_M_INSIDESTAFFOut[0].DEPARTNO;
        }

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtOCardno.Text;
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);
        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001107112");
            return;
        }
        hidCustname.Value = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
        hidPaperno.Value = ddoTF_F_CUSTOMERRECOut.PAPERNO;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //证件类型赋值
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            hidPapertype.Value = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else hidPapertype.Value = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        //读旧卡后直接换卡写卡
        if (hidReadSpecialCardType.Value == "I")
        {
            //计算卡内年审时间
            string year = "20001231";
            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            if (currentMonth == 12)
            {
                year = DateTime.Now.AddYears(1).Year.ToString() + "1231";
            }
            else
            {
                year = DateTime.Now.Year.ToString() + "1231";
            }
            this.txtVerifyDate.Value = year;//给年审函数参数控件赋值
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "startSpecialInfo2();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "startSpecialInfo1();", true);
        }
    }

    private void changeSpecialCard()
    {
        if (hidCardNo.Value == "" || hidCardNo.Value == null)
        {
            context.AddError("写卡成功后未获取到新卡卡号，写库失败");
            return;
        }
        if (hidCardNo.Value == txtOCardno.Text)
        {
            context.AddError("新旧卡卡号相同，请确认是否放置的新卡，写库失败，卡片已锁");
            return;
        }

        if (hidTradeNo.Value == "" || hidAsn.Value == "" || hidSpecialCardType.Value == "" || hidSpecialCardName.Value == "")
        {
            context.AddError("写卡成功后未获取到新卡信息，写库失败，卡片已锁");
            return;
        }

        if (hidSpecialCardType.Value != hidReadSpecialCardType.Value)
        {
            context.AddError("旧卡类型和新卡类型不一致，写库失败，卡片已锁");
            return;
        }

        // 校验新卡是否存在库中
        DataTable data = ASHelper.callQuery(context, "ReadCardTypeByCardNo", hidCardNo.Value);
        if (data == null || data.Rows.Count == 0)
        {
            context.AddError("未在用户卡库存表中查询出卡片信息或者卡片类型没有配置，卡片已锁");
            return;
        }

        SP_BS_SpecialChangeCardPDO pdo = new SP_BS_SpecialChangeCardPDO();
        //存储过程赋值
        pdo.ID = DealString.GetRecordID(hidTradeNo.Value, hidCardNo.Value);
        pdo.CUSTRECTYPECODE = CUSTRECTYPECODE.Value;
        pdo.CARDCOST = Convert.ToInt32(Convert.ToDecimal(CardcostFee.Text) * 100);
        pdo.NEWCARDNO = hidCardNo.Value;
        pdo.OLDCARDNO = txtOCardno.Text;
        pdo.ONLINECARDTRADENO = hidTradeNo.Value;
        pdo.CHANGECODE = selReasonType.SelectedValue;
        pdo.ASN = hidAsn.Value;
        pdo.CARDTYPECODE = "99";
        pdo.SELLCHANNELCODE = "01";
        pdo.TRADETYPECODE = hidReadSpecialCardType.Value == "J" ? "N1"
         : hidReadSpecialCardType.Value == "I" ? "N2"
         : hidReadSpecialCardType.Value == "P" ? "N3"
         : "";
        pdo.PREMONEY = 0;
        pdo.TERMNO = "112233445566";
        pdo.OPERCARDNO = context.s_CardID;
        pdo.SUPPLYREALMONEY = 0;
        //换卡类型为可读自然损坏卡
        if (selReasonType.SelectedValue == "13")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100);
            pdo.SERSTARTTIME = Convert.ToDateTime(SERSTARTIME.Value);
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "3";
            pdo.NEXTMONEY = 0;
            pdo.CURRENTMONEY = 0;
            pdo.CHECKSTAFFNO = context.s_UserID;
            pdo.CHECKDEPARTNO = context.s_DepartID;
        }
        //换卡类型为可读人为损坏卡
        else if (selReasonType.SelectedValue == "12")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = 0;
            pdo.CURRENTMONEY = 0;
            pdo.CHECKSTAFFNO = context.s_UserID;
            pdo.CHECKDEPARTNO = context.s_DepartID;
        }
        //换卡类型为不可读人为损坏卡
        else if (selReasonType.SelectedValue == "14")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(DepositFee.Text) * 100);
            pdo.SERSTARTTIME = DateTime.Now;
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "2";
            pdo.NEXTMONEY = 0;
            pdo.CURRENTMONEY = 0;
            pdo.CHECKSTAFFNO = hidCheckStaffNo.Value;
            pdo.CHECKDEPARTNO = hidCheckDepartNo.Value;
        }
        //换卡类型为不可读自然损坏卡
        else if (selReasonType.SelectedValue == "15")
        {
            pdo.DEPOSIT = Convert.ToInt32(Convert.ToDecimal(ODeposit.Text) * 100);
            pdo.SERSTARTTIME = Convert.ToDateTime(SERSTARTIME.Value);
            pdo.SERVICEMONE = 0;
            pdo.CARDACCMONEY = 0;
            pdo.NEWSERSTAKETAG = "0";
            pdo.TOTALSUPPLYMONEY = 0;
            pdo.OLDDEPOSIT = 0;
            pdo.SERSTAKETAG = "3";
            pdo.NEXTMONEY = 0;
            pdo.CURRENTMONEY = 0;
            pdo.CHECKSTAFFNO = hidCheckStaffNo.Value;
            pdo.CHECKDEPARTNO = hidCheckDepartNo.Value;
        }

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("换卡成功");
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtOCardno.Text, hidCustname.Value, "特种卡换卡", "0.00"
                , "0.00", hidCardNo.Value, hidPaperno.Value, "0.00",
                "", "0.00", context.s_UserName, context.s_DepartName, hidPapertype.Value, "0.00", "0.00");

            if (selReasonType.SelectedValue == "12" || selReasonType.SelectedValue == "14")
            {
                btnPrintSJ.Enabled = true;
                ASHelper.prepareShouJu(ptnShouJu, hidCardNo.Value, hidCustname.Value, "特种卡换卡", Total.Text
             , "", "", "", "", "", Total.Text, context.s_UserName, context.s_DepartName, "", "0.00", "");
            }

            initLoad();
            Change.Enabled = false;
        }
    }
}
