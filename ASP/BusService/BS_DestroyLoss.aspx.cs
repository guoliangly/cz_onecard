﻿/***************************************************************
 * BS_DestroyLoss.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 公交服务 - 挂失卡销户 页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 * 2010/12/02    粱锦           修改挂失卡销户业务编码L6为6L
 ***************************************************************
  */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.CardManager;
using TDO.BusinessCode;
using TDO.PersonalTrade;
using TDO.UserManager;
using TDO.Warn;
using Common;
using Master;
using TM;
using TDO.BalanceChannel;
using PDO.PersonalBusiness;

public partial class ASP_PersonalBusiness_BS_DestroyLoss : Master.FrontMaster
{
    private static int par_LossSpan = 7;       //挂失卡销户时长限制
    private static decimal par_LossDestoryQuota = 0;   //挂失卡销户金额限制




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            initLoad(sender, e);
        }

    }

    protected void initLoad(object sender, EventArgs e)
    {
        //初始化销户的时间和金额限制
        initRestrict();

        //初始化销户费用信息
        initFeeInfo();
    }


    private void initRestrict()
    {
        TMTableModule tmTMTableModule = new TMTableModule();


        //从系统参数表中读取挂失卡销户时长限制
        TD_M_TAGTDO ddoTD_M_TAGTDOIn = new TD_M_TAGTDO();
        ddoTD_M_TAGTDOIn.TAGCODE = "PB_TBLOSS_SPAN";

        TD_M_TAGTDO[] ddoTD_M_TAGTDOOutArr = (TD_M_TAGTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TAGTDOIn, "S001028125");

        if (ddoTD_M_TAGTDOOutArr[0].TAGVALUE != null)
        {
            par_LossSpan = Convert.ToInt16(ddoTD_M_TAGTDOOutArr[0].TAGVALUE);
        }


        //从系统参数表中读取挂失卡销户金额限制
        ddoTD_M_TAGTDOIn.TAGCODE = "PB_TBLOSS_QUOTA";

        ddoTD_M_TAGTDOOutArr = (TD_M_TAGTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TAGTDOIn, "S001028126");

        if (ddoTD_M_TAGTDOOutArr[0].TAGVALUE != null)
        {
            par_LossDestoryQuota = Convert.ToDecimal(ddoTD_M_TAGTDOOutArr[0].TAGVALUE);
        }

    }

    private void initFeeInfo()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从前台业务交易费用表中读取数据
        TD_M_TRADEFEETDO ddoTD_M_TRADEFEETDOIn = new TD_M_TRADEFEETDO();
        ddoTD_M_TRADEFEETDOIn.TRADETYPECODE = "6L";

        TD_M_TRADEFEETDO[] ddoTD_M_TRADEFEEOutArr = (TD_M_TRADEFEETDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_TRADEFEETDOIn, typeof(TD_M_TRADEFEETDO), "S001001137", "TD_M_TRADEFEE", null);

        for (int i = 0; i < ddoTD_M_TRADEFEEOutArr.Length; i++)
        {
            //费用类型为押金
            if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "00")
                CancelDepositFee.Text = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为充值
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "01")
                hiddenCancelSupplyFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为维护费
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "02")
                hiddenMaintenanceFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为手续费
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "03")
                hiddenProcedureFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
            //费用类型为其他费用
            else if (ddoTD_M_TRADEFEEOutArr[i].FEETYPECODE == "99")
                hiddenOtherFee.Value = ((Convert.ToDecimal(ddoTD_M_TRADEFEEOutArr[i].BASEFEE)) / 100).ToString("0.00");
        }
        CancelSupplyFee.Text = hiddenCancelSupplyFee.Value;
        MaintenanceFee.Text = hiddenMaintenanceFee.Value;
        ProcedureFee.Text = hiddenProcedureFee.Value;
        OtherFee.Text = hiddenOtherFee.Value;
        Total.Text = (Convert.ToDecimal(CancelDepositFee.Text) +
                      Convert.ToDecimal(hiddenCancelSupplyFee.Value) +
                      Convert.ToDecimal(hiddenMaintenanceFee.Value) +
                      Convert.ToDecimal(hiddenProcedureFee.Value) +
                      Convert.ToDecimal(hiddenOtherFee.Value)).ToString("0.00");
        ReturnSupply.Text = Total.Text;
    }

    //对卡号进行非空、长度、数字检验
    private Boolean LossDBreadValidation()
    {
        //对卡号进行非空、长度、数字检验
        if (txtCardno.Text.Trim() == "")
            context.AddError("A001028100", txtCardno);
        else
        {
            if ((txtCardno.Text.Trim()).Length != 8)
                context.AddError("A001008109", txtCardno);
            else if (!Validation.isNum(txtCardno.Text.Trim()))
                context.AddError("A001028102", txtCardno);
        }

        return !(context.hasError());
    }


    //从数据库中读取挂失卡数据，用户数据
    protected void btnLossDBquery_Click(object sender, EventArgs e)
    {
        //对卡号进行检验

        if (!LossDBreadValidation()) return;

        //特殊号段不办理本业务addbyliuh20101130
        string returnMsg = ValidateSpecilCard(this.txtCardno.Text.Trim());
        if (returnMsg.Length > 0)
        {
            context.AddError(returnMsg);
            return;
        }
        
        // 校验是否为特种卡，如果是特种卡，就不给挂失销户
        DataTable data = BusCardHelper.callQuery(context, "QryIsSpecialCard", txtCardno.Text.Trim());
        if (data.Rows.Count > 0)
        {
            context.AddError("该卡为公交特种卡，无法进行挂失卡销户");
            return;
        }
        //检验是否做过销户
        if (!DestroyExistValidation()) return;

        btnDestroy.Enabled = true;
        btnPrintPZ.Enabled = false;

        //查询挂失卡信息
        loadLossCardInfo();

        //查询客户资料信息
        loadCustomerInfo();

    }

    private void loadLossCardInfo()
    {
        TMTableModule tmTMTableModule = new TMTableModule();


        //从卡资料表(TF_F_CARDREC)中读取数据
        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text.Trim();

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        if (ddoTF_F_CARDRECOut == null)
        {
            context.AddError("A001028103");
            return;
        }

        if (ddoTF_F_CARDRECOut.CARDSTATE != "03")//add by liuh根据卡资料状态判断当前是否挂失状态20101208
        {
            context.AddError("A001028903:卡片不是挂失状态");
            return;
        }

        //从IC卡电子钱包帐户表(TF_F_CARDEWALLETACC)中读取数据
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text.Trim();

        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);
        if (ddoTF_F_CARDEWALLETACCOut == null)
        {
            context.AddError("A001028103");
            return;
        }


        //从黑名单表中（TF_B_WARN_BLACK）读取挂失时间，
        //      挂失员工从台账表中读取，如果台账中没有记录，则挂失员工为空
        #region
        TDO.Warn.TF_B_WARN_BLACKTDO ddoTF_B_WARN_BLACKTDOIn = new TF_B_WARN_BLACKTDO();
        System.Text.StringBuilder strsqlblack = new System.Text.StringBuilder();
        strsqlblack.Append(" select * from TF_B_WARN_BLACK  ");
        strsqlblack.Append(" where cardno = '" + txtCardno.Text.Trim() + "'");

        TF_B_WARN_BLACKTDO ddoTF_B_WARN_BLACKTDOOut = (TF_B_WARN_BLACKTDO)tmTMTableModule.selByPK(context, ddoTF_B_WARN_BLACKTDOIn, typeof(TF_B_WARN_BLACKTDO), null, strsqlblack.ToString());
        if (ddoTF_B_WARN_BLACKTDOOut == null)
        {
            context.AddError("A001028104");
            return;
        }
        LossDate.Text = ddoTF_B_WARN_BLACKTDOOut.CREATETIME.ToString("yyyy-MM-dd");
        #endregion

        #region//从业务台帐主表(TF_B_TRADE)中读取挂失时间，挂失员工

        TF_B_TRADETDO ddoTF_B_TRADEIn = new TF_B_TRADETDO();
        System.Text.StringBuilder strsql = new System.Text.StringBuilder();
        strsql.Append(" select * from TF_B_TRADE where tradetypecode = '08'");
        strsql.Append(" and ROWNUM < 2 and cardno = '" + txtCardno.Text.Trim() + "'");
        strsql.Append(" order by tradeid desc");

        TF_B_TRADETDO ddoTF_B_TRADEOut = (TF_B_TRADETDO)tmTMTableModule.selByPK(context, ddoTF_B_TRADEIn, typeof(TF_B_TRADETDO), null, strsql.ToString());
        if (ddoTF_B_TRADEOut == null)
        {
            ReturnStaff.Text = "";
        }
        else
        {
            //从内部员工编码表(TD_M_INSIDESTAFF)中读取数据
            TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            ddoTD_M_INSIDESTAFFIn.STAFFNO = ddoTF_B_TRADEOut.OPERATESTAFFNO;

            TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO)tmTMTableModule.selByPK(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_Destroy", null);
            ReturnStaff.Text = ddoTD_M_INSIDESTAFFOut.STAFFNAME;
        }

        #endregion


        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTF_F_CARDRECOut.CARDTYPECODE;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);



        //给页面各显示项附值
        hiddenLabAsn.Value = ddoTF_F_CARDRECOut.ASN;
        hiddenCardtype.Value = ddoTF_F_CARDRECOut.CARDTYPECODE;
        LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;
        sDate.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");


        Decimal aMoney = Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY);
        //如果余额大于50元，则提示
        if (aMoney > 5000)
        {
            context.AddError("余额超过50元，无法直接挂失销户，需提交营业经理审核");
            return;
        }
        accMoney.Text = (aMoney / (Convert.ToDecimal(100))).ToString("0.00");


        //修改合计费用信息
        Decimal backMoney = 0 - aMoney / 100;
        CancelSupplyFee.Text = backMoney.ToString("0.00");
        Total.Text = (Convert.ToDecimal(CancelDepositFee.Text) + Convert.ToDecimal(hiddenCancelSupplyFee.Value) +
            Convert.ToDecimal(hiddenMaintenanceFee.Value) + Convert.ToDecimal(hiddenProcedureFee.Value) +
            Convert.ToDecimal(hiddenOtherFee.Value) + backMoney).ToString("0.00");
        ReturnSupply.Text = Total.Text;
    }

    private void loadCustomerInfo()
    {

        TMTableModule tmTMTableModule = new TMTableModule();

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text.Trim();

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);
        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001002111");
            return;
        }

        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

        //性别赋值
        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期赋值
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else
                CustBirthday.Text = Bdate;
        }
        else
            CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;


        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);


        //证件类型赋值
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else
            Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;
    }

    //检查是否超过7天
    private Boolean DestroyDateValidation()
    {
        //String[] arr = (LossDate.Text).Split('-');
        DateTime rDate = Convert.ToDateTime(LossDate.Text);

        //检查是否超过7天

        if (rDate.AddDays(par_LossSpan) > DateTime.Now)
            context.AddError("A001028114");

        return !(context.hasError());
    }


    //检查是否已做过销户
    private Boolean DestroyExistValidation()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //检查是否已做过销户
        TF_B_TRADETDO ddoTF_B_TRADEIn = new TF_B_TRADETDO();

        string str = " Select CARDNO From TF_B_TRADE Where CARDNO = '" + txtCardno.Text + "' And TRADETYPECODE = '6L' AND CANCELTAG = '0'";
        DataTable datastr = tmTMTableModule.selByPKDataTable(context, ddoTF_B_TRADEIn, null, str, 0);

        if (datastr.Rows.Count != 0)
            context.AddError("A001028108");

        return !(context.hasError());
    }

    //是否超过 挂失卡销户金额限制
    private Boolean DestroyQuotaValidation()
    {

        decimal par_accMoney = Convert.ToDecimal(accMoney.Text) * 100;

        //检查挂失卡销户金额限制

        if (par_accMoney > par_LossDestoryQuota)
            context.AddError("A001028116");

        return !(context.hasError());
    }


    protected void btnDestroy_Click(object sender, EventArgs e)
    {
        //对输入卡号进行判断
        if (!LossDBreadValidation()) return;

        //判断是否超过7天
        if (!DestroyDateValidation()) return;

        //判断是否超过 挂失卡销户金额限制
        //modified by liuh 去掉金额限制20101201
        //if (!DestroyQuotaValidation()) return;

        //库内余额为负时,不能进行销户
        if (Convert.ToDecimal(ReturnSupply.Text) > 0)
        {
            context.AddError("A001028115");
            return;
        }

        SP_PB_DestroyLossPDO pdo = new SP_PB_DestroyLossPDO();


        //存储过程赋值
        string strCardno = txtCardno.Text.Trim();
        pdo.ID = DealString.GetRecordID(strCardno.Substring(strCardno.Length - 4, 4), txtCardno.Text);
        //pdo.ID = txtCardno.Text.Substring(12, 4) + DateTime.Now.ToString("MMddhhmmss") + txtCardno.Text.Substring(12, 4);
        pdo.CARDNO = txtCardno.Text.Trim();
        pdo.ASN = hiddenLabAsn.Value;
        pdo.CARDTYPECODE = hiddenCardtype.Value;
        pdo.CARDACCMONEY = Convert.ToInt32(Convert.ToDecimal(accMoney.Text) * 100);
        pdo.RDFUNDMONEY = Convert.ToInt32(Convert.ToDecimal(CancelSupplyFee.Text) * 100);

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("M001028001");
            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, CustName.Text, "挂失卡销户", CancelSupplyFee.Text, "0.00"
                , "", Paperno.Text, "", "", Total.Text, context.s_UserName, context.s_DepartName, Papertype.Text,
                ProcedureFee.Text, "0.00");

            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printInvoice();", true);
            }

            //foreach (Control con in this.Page.Controls)
            //{
            //    ClearControl(con);
            //}
            initLoad(sender, e);
            btnDestroy.Enabled = false;
        }
    }
}
