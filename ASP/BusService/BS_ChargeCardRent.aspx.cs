﻿/***************************************************************
 * BS_ChargCardRent.aspx.cs
 * 系统名  : 城市一卡通系统
 * 子系统名: 常州公交子系统 - 转卡租金页面
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2010/11/18    粱锦           初次开发
 ***************************************************************
 */


using System;
using System.Collections;
using System.Web.UI;
using TM;
using TDO.CardManager;
using TDO.BusinessCode;

using Common;
using PDO.PersonalBusiness;
using TDO.ResourceManager;
using Master;
using TDO.UserManager;
using System.Data;

public partial class ASP_BusService_BS_ChargeCardRent : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            setReadOnly(LabAsn, LabCardtype, sDate, eDate, cMoney, RESSTATE);

            if (!context.s_Debugging) setReadOnly(txtCardno);

            //initLoad();

            ScriptManager2.SetFocus(btnReadCard);
        }
    }

    //读卡按钮单击事件
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        btnReadCardProcess();
        if (context.hasError()) return;

        

        if (context.hasError())
        {
            ScriptManager2.SetFocus(btnReadCard);
            return;
        }
        else
        {
            //ScriptManager2.SetFocus(Money);
        }
    }


    //读卡处理过程
    protected void btnReadCardProcess()
    {
        //纪念卡不能充值
        string returnMsg = ValidateSpecilCard(this.txtCardno.Text.Trim()); 
        if (returnMsg.Length > 0)
        {
            context.AddError(returnMsg);
            return;
        }

        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验        SP_AccCheckPDO pdo = new SP_AccCheckPDO();
        pdo.CARDNO = txtCardno.Text;

        PDOBase pdoOut;
        bool ok = TMStorePModule.Excute(context, pdo, out pdoOut);

        if (ok)
        {
            //查询卡用户信息
            getCustomerInfo(tmTMTableModule);

            //查询待转值信息
            getCardRentInfo(tmTMTableModule);

            
            btnPrintPZ.Enabled = false;
        }
        else if (pdoOut.retCode == "A001107199")
        {//验证如果是黑名单卡，锁卡
            this.LockBlackCard(txtCardno.Text);
            this.hidLockBlackCardFlag.Value = "yes";
        }
    }

   //查询卡用户信息
    private void getCustomerInfo(TMTableModule tmTMTableModule)
    {
        //从用户卡库存表(TL_R_ICUSER)中读取数据

        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }

        //从月票表中取公交卡类型

        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
        ddoTF_F_CARDCOUNTACCIn.CARDNO = ddoTL_R_ICUSEROut.CARDNO;
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC", null);
        if (ddoTF_F_CARDCOUNTACCOut == null)
        {
            context.AddError("A001002113");
            return;
        }

        //公交类型编码
        TD_M_APPAREATDO ddoTD_M_APPAREATDOIn = new TD_M_APPAREATDO();
        ddoTD_M_APPAREATDOIn.AREACODE = ddoTF_F_CARDCOUNTACCOut.ASSIGNEDAREA;
        TD_M_APPAREATDO ddoTD_M_APPAREATDOOut = (TD_M_APPAREATDO)tmTMTableModule.selByPK(context, ddoTD_M_APPAREATDOIn, typeof(TD_M_APPAREATDO), null, "TD_M_APPAREA", null);


        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardno.Text;

        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        if (ddoTF_F_CUSTOMERRECOut == null)
        {
            context.AddError("A001107112");
            return;
        }



        //从资源状态编码表中读取数据



        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
        ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

        TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

        if (ddoTD_M_RESOURCESTATEOut == null)
            RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
        else
            RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据



        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

        //给页面显示项赋值



        hiddenLabCardtype.Value = ddoTL_R_ICUSEROut.CARDTYPECODE;
        LabAsn.Text = ddoTL_R_ICUSEROut.ASN;
        LabCardtype.Text = ddoTD_M_APPAREATDOOut.AREANAME;
        sDate.Text = ASHelper.toDateWithHyphen(hiddensDate.Value);
        eDate.Text = ASHelper.toDateWithHyphen(hiddeneDate.Value);
        cMoney.Text = ((Convert.ToDecimal(hiddencMoney.Value)) / (Convert.ToDecimal(100))).ToString("0.00");
        CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

        //检验卡片是否已经启用



        if (String.Compare(hiddensDate.Value, DateTime.Today.ToString("yyyyMMdd")) > 0)
        {
            context.AddError("卡片尚未启用");
            return;
        }

        //性别显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
            Custsex.Text = "男";
        else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
            Custsex.Text = "女";
        else Custsex.Text = "";

        //出生日期显示
        if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
        {
            String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            if (Bdate.Length == 8)
            {
                CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
            }
            else CustBirthday.Text = Bdate;
        }
        else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;

        //证件类型显示
        if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
        {
            Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        }
        else Papertype.Text = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

        Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
        Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
        Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        sellTime.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardno.Text);
    }


    //查询卡租金信息
    private void getCardRentInfo(TMTableModule tmTMTableModule)
    {

        //验证退卡租金
        TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOIn = new TP_BUS_CARDRENTTDO();
        ddoTP_BUS_CARDRENTTDOIn.CARDNO = txtCardno.Text;

        TP_BUS_CARDRENTTDO ddoTP_BUS_CARDRENTTDOOut = 
                (TP_BUS_CARDRENTTDO)tmTMTableModule.selByPK(context, 
                                                             ddoTP_BUS_CARDRENTTDOIn, 
                                                             typeof(TP_BUS_CARDRENTTDO), 
                                                              null, 
                                                              "TP_BUS_CARDRENTTDO", 
                                                               null);

        if (ddoTP_BUS_CARDRENTTDOOut != null)
        {
            hidSupplyFee.Value = Convert.ToString(ddoTP_BUS_CARDRENTTDOOut.CARDRENT);
            SupplyFee.Text = "" + Convert.ToString(Convert.ToDouble(ddoTP_BUS_CARDRENTTDOOut.CARDRENT) / 100.0);
            Total.Text = SupplyFee.Text;
            ReturnSupply.Text = SupplyFee.Text;
            SupplyFee.Text = SupplyFee.Text;

            btnSupply.Enabled = true;
        }
        else
        {
            context.AddError("A001001810: 当前卡没有未转卡租金");
            btnSupply.Enabled = false;
            return;
        }
    }

    //充值确认
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "CashChargeConfirm")
        {
            btnSupply_Click(sender, e);
            hidWarning.Value = "";
            return;
        }

     
        if (hidWarning.Value == "yes")
        {
            btnSupply.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            #region 如果是前台黑名单锁卡
            //前台锁卡没有写写卡台账

            if (this.hidLockBlackCardFlag.Value == "yes")
            {
                AddMessage("黑名单卡已锁");
                clearCustInfo(txtCardno);
                this.hidLockBlackCardFlag.Value = "";
                return;
            }
            #endregion

            SP_PB_updateCardTradePDO pdo = new SP_PB_updateCardTradePDO();
            pdo.CARDTRADENO = hiddentradeno.Value;
            pdo.TRADEID = hidoutTradeid.Value;

            bool ok = TMStorePModule.Excute(context, pdo);

            if (ok)
            {
                AddMessage("前台写卡成功");
                clearCustInfo(txtCardno);
            }
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript",
                "printInvoice();", true);
        }

        //清空页面
        //foreach (Control con in this.Page.Controls)
        //{
        //    ClearControl(con);
        //}
        //initLoad();
        hidWarning.Value = "";
    }

    //充值提交
    protected void btnSupply_Click(object sender, EventArgs e)    {

       

         //调用存储过程
        callSP_BS_ChargCardRent("1");
        bool ok = context.ExecuteSP("SP_BS_ChargCardRent");

        if (ok)
        {
            btnSupply.Enabled = false;

            hidoutTradeid.Value = "" + context.GetFieldValue("p_TRADEID");

            //AddMessage("M001002001");
            ScriptManager.RegisterStartupScript(this, this.GetType(),"writeCardScript", "chargeCard();", true);

            btnPrintPZ.Enabled = true;
            //Money.Text = "";

            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, CustName.Text, "转卡租金",
                                        hiddenSupply.Value, "", "", Paperno.Text,
                                        (hiddencMoney.Value + hidSupplyFee.Value), "", SupplyFee.Text, 
                                        context.s_UserName,
                                        context.s_DepartName, 
                                        Papertype.Text, "0.00", "");

        }
     

    }


    //执行存储过程 callSP_RC_ReIssueTransitBalance
     //   p_ID	          char,
     //   p_CARDNO	      char,
     //   p_CARDTRADENO	  char,
     //   p_CARDMONEY	    int,
     //   p_CARDACCMONEY	int,
     //   p_ASN	          char,
     //   p_CARDTYPECODE	char,
     //   p_SUPPLYMONEY	  int,
     //   p_OTHERFEE			int,
     //   p_TRADETYPECODE	char,
     //   p_TERMNO				char,
     //   p_OPERCARDNO		char,
     //   p_TRADEID	      out char, 		-- Return trade id
     //   p_option          char,				-- 0 do w/o submit；1 do with submi
     //   p_currOper        char,
     //   p_currDept        char,
     //   p_retCode     out char,
     //   p_retMsg      out varchar2
    private void callSP_BS_ChargCardRent(string option)
    {

        TMTableModule tmTMTableModule = new TMTableModule();

        //从IC卡电子钱包账户表中读取数据

        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;

        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null, "TF_F_CARDEWALLETACC", null);


        context.SPOpen();
        context.AddField("p_ID").Value = DealString.GetRecordID(hiddentradeno.Value, LabAsn.Text);
        context.AddField("p_CARDNO").Value = txtCardno.Text;
        context.AddField("p_CARDTRADENO").Value = hiddentradeno.Value;
        context.AddField("p_CARDMONEY").Value = hiddencMoney.Value;
        context.AddField("p_CARDACCMONEY").Value = ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY;
        context.AddField("p_ASN").Value = LabAsn.Text;
        context.AddField("p_CARDTYPECODE").Value = hiddenLabCardtype.Value;
        context.AddField("p_SUPPLYMONEY").Value = hidSupplyFee.Value;
        context.AddField("p_TRADETYPECODE").Value = "9B";
        context.AddField("p_TERMNO").Value = "112233445566";
        context.AddField("p_OPERCARDNO").Value = context.s_CardID;
        context.AddField("p_TRADEID", "string", "Output", "18");
        context.AddField("p_option").Value = option;
        hidSupplyMoney.Value = hidSupplyFee.Value;

    }

}
