﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using TM;
using TDO.PersonalTrade;
using TDO.CardManager;
using TDO.BusinessCode;


/// <summary>
/// 挂失解挂页面 
/// </summary>
public partial class ASP_BusService_BS_ReportLoss : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            return;
        }
    }

    #region btnQuery_Click 查询按钮事件
    /// <summary>
    /// 查询按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //验证查询条件
        if (this.CheckQueryControl() == false)
        {
            return;
        }

        #region 构建查询sql
        System.Text.StringBuilder sql = new System.Text.StringBuilder();
        sql.Append("select  decode(c.cardstate, '10', '10:售出', '11', '11:换卡售出', '03', '03:书面挂失',  '04', '04:口头挂失')  as 状态,");
        sql.Append("c.cardno as 卡号,t.paperno as 证件号,t.custname as 姓名,decode(t.custsex, '0', '男', '女') as 性别, ");
        sql.Append("to_char(c.serstarttime,'yyyy-MM-dd') as 启用日期,w.cardaccmoney/100 as 卡内余额");
        sql.Append(" from  tf_f_cardrec c ,tf_f_customerrec t,tf_f_cardewalletacc w  ");
        sql.Append(" where c.cardno=t.cardno and c.cardno=w.cardno");
        sql.Append(" and  c.cardstate in ('10' , '11','03','04')");//'10:售出', '11:换卡售出', '03:书面挂失','04:口头挂失'
        sql.Append(" and  c.custrectypecode='1'");//限定是记名卡
        sql.Append(" and  length(c.cardno)=8");//限定是公交卡

        if (this.txtCardNo.Text.Trim().Length > 0)
        {
            sql.Append(" and  c.cardno='" + this.txtCardNo.Text + "'");
        }
        if (this.txtPaperno.Text.Trim().Length > 0)
        {
            sql.Append(" and  t.paperno='" + this.txtPaperno.Text + "'"); 
        }
        #endregion

        TMTableModule tmTMTableModule = new TMTableModule();
        TF_B_TRADETDO tdoTF_B_TRADETDO = new TF_B_TRADETDO();
        DataTable data = tmTMTableModule.selByPKDataTable(context, tdoTF_B_TRADETDO, null, sql.ToString(), 0);

        UserCardHelper.resetData(gvResult, data);
        gvResult.SelectedIndex = -1;

        this.btnSubmit.Enabled = false;
        this.rdoCancelLoss.Checked = false;
        this.rdoRealReportLoss.Checked = false;
        this.rdoReportLoss.Checked = false;
        this.rdoCancelLoss.Enabled = false;
        this.rdoRealReportLoss.Enabled = false;
        this.rdoReportLoss.Enabled = false;


        foreach (Control con in this.Page.Controls)
        {
            ClearLabelControl(con);
        }
    }
    #endregion

    #region CheckQueryControl 对查询条件的验证
    /// <summary>
    /// 对查询条件的验证
    /// </summary>
    private bool CheckQueryControl()
    {
        bool isPapernoEmpty = Validation.isEmpty(this.txtPaperno);
        bool isCardnoEmpty = Validation.isEmpty(this.txtCardNo);

        if (isPapernoEmpty && isCardnoEmpty)
        {
            context.AddError("A900010B91: 查询条件不可为空");
            return false;
        }

        if (isPapernoEmpty == false && Validation.isCharNum(txtPaperno.Text.Trim()) == false)
        {
            context.AddError("A900010B92: 证件号必须是半角英文或数字", txtPaperno);
            return false;
        }

        if (isCardnoEmpty == false)
        {
            if (Validation.isNum(txtCardNo.Text.Trim()) == false)
            {
                context.AddError("A900010B93: 卡号必须是数字", txtCardNo);
                return false;
            }
            else if (this.txtCardNo.Text.Trim().Length != 8)
            {//老公交卡卡号8位，新卡卡号16位
                context.AddError("A900010B94: 卡号位数必须为8位", txtCardNo);
                return false;
            }
        }

        return true;
    }
    #endregion

    #region gv_SelectedIndexChanged选择按钮事件
    /// <summary>
    /// 选择按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void gv_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 得到选择行
        GridViewRow selectRow = gvResult.SelectedRow;

        #region 初始化几个用于页面展现的TDO对象
        string cardno = selectRow.Cells[2].Text;
        TMTableModule tmTMTableModule = new TMTableModule();

        //从IC卡资料表中读取数据
        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = cardno;
        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

        //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
        ddoTF_F_CUSTOMERRECIn.CARDNO = cardno;
        TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

        //从电子钱包账户表读取数据 
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = cardno;
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

        //从月票表中取公交卡类型
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
        ddoTF_F_CARDCOUNTACCIn.CARDNO = ddoTF_F_CARDRECOut.CARDNO;
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC", null);
        if (ddoTF_F_CARDCOUNTACCOut == null)
        {
            context.AddError("A001002113");
            return;
        }

        //公交类型编码
        TD_M_APPAREATDO ddoTD_M_APPAREATDOIn = new TD_M_APPAREATDO();
        ddoTD_M_APPAREATDOIn.AREACODE = ddoTF_F_CARDCOUNTACCOut.ASSIGNEDAREA;
        TD_M_APPAREATDO ddoTD_M_APPAREATDOOut = (TD_M_APPAREATDO)tmTMTableModule.selByPK(context, ddoTD_M_APPAREATDOIn, typeof(TD_M_APPAREATDO), null, "TD_M_APPAREA", null);


        //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
        ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;
        TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);
        #endregion

        this.lblCardno.Text = cardno;
        this.lblAsn.Text = ddoTF_F_CARDRECOut.ASN;
        this.lblCardtype.Text = ddoTD_M_APPAREATDOOut.AREANAME;
        this.lblCardstate.Text = GetCardstateNameByCode(ddoTF_F_CARDRECOut.CARDSTATE);

        this.lblSDate.Text = ddoTF_F_CARDRECOut.SERSTARTTIME.ToString("yyyyMMdd");
        this.lblEDate.Text = ddoTF_F_CARDRECOut.VALIDENDDATE;
        this.lblCardaccmoney.Text = ((Convert.ToDecimal(ddoTF_F_CARDEWALLETACOut.CARDACCMONEY)) / 100).ToString("0.00");

        this.lblCustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;
        this.lblCustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
        this.lblPapertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
        this.lblCustsex.Text = ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0" ? "男" : "女";
        this.lblCustphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
        this.lblPaperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
        this.lblCustpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
        this.lblCustaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;

        this.hidCardstate.Value = ddoTF_F_CARDRECOut.CARDSTATE;

        #region 控制RadioButton的显示
        string stateCode = ddoTF_F_CARDRECOut.CARDSTATE;
        if (stateCode.Equals("10") || stateCode.Equals("11")) //  10：在售，11：换卡在售
        {
            this.rdoReportLoss.Enabled = true;
            this.rdoRealReportLoss.Enabled = true;
            this.rdoCancelLoss.Enabled = false;
        }
        else if (stateCode == "04") // 口头挂失
        {
            this.rdoReportLoss.Enabled = false;
            this.rdoRealReportLoss.Enabled = true;
            this.rdoCancelLoss.Enabled = true;
        }
        else if (stateCode == "03") // 书面挂失
        {
            this.rdoReportLoss.Enabled = false;
            this.rdoRealReportLoss.Enabled = false;
            this.rdoCancelLoss.Enabled = true;
        }
        this.btnPrintPZ.Enabled = false;
        #endregion
    }
    #endregion

    #region btnSubmit_Click 提交按钮事件
    /// <summary>
    /// 提交按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        #region 初始化参数值
        string strCardnewstate = "";//03挂失，10售出，11换卡售出，04 口头挂失
        string strTradetypecode = "";//08书面挂失，0E口挂，09解挂
        string strMsg = "";//页面返回的消息文本
        string strTradetypeName = "";//打印凭证中的业务类型


        if (this.rdoReportLoss.Checked)//口挂
        {
            strCardnewstate = "04";
            strTradetypecode = "0E";
            strMsg = "口挂成功";
            strTradetypeName = "口头挂失";
        }
        else if (this.rdoRealReportLoss.Checked)//书挂
        {
            strCardnewstate = "03";
            strTradetypecode = "08";
            strMsg = "书挂成功";
            strTradetypeName = "书面挂失";
        }
        else if (this.rdoCancelLoss.Checked)//解挂
        {
            //取得挂失前卡资料表中的卡状态

            TMTableModule tmTMTableModule = new TMTableModule();
            TF_B_TRADETDO ddoTF_B_TRADETDOIn = new TF_B_TRADETDO();
            ddoTF_B_TRADETDOIn.CARDNO = this.lblCardno.Text;
            TF_B_TRADETDO[] ddoTF_B_TRADETDOOutArr = (TF_B_TRADETDO[])tmTMTableModule.selByPKArr(context, ddoTF_B_TRADETDOIn, typeof(TF_B_TRADETDO), null, "TF_B_TRADETDO_REPORTLOSS", null);

            if (ddoTF_B_TRADETDOOutArr.Length > 0)
            {
                strCardnewstate = ddoTF_B_TRADETDOOutArr[0].CARDSTATE;
            }
            else
            {
                strCardnewstate = "10";
            }

            strTradetypecode = "09";
            strMsg = "解挂成功";
            strTradetypeName = "解挂";

            //如果是解挂需要验证当前客户是否存在另一张同类特种卡
            //读获取取特种卡卡面类型
            string strCardSurface = "";
            DataTable data = BusCardHelper.callQuery(context, "QryLossCardSurface", this.lblCardno.Text.Trim());
            if (data == null || data.Rows.Count == 0)
            {
                context.AddError("未找到该特种卡卡面类型");
                return;
            }
            else
            {
                strCardSurface = data.Rows[0].ItemArray[0].ToString();
            }
            if (strCardSurface == "9901" || strCardSurface == "9902" || strCardSurface == "9903")
            {
                string paperno = lblPaperno.Text.Trim();
                string oldPaperno = paperno;
                DataTable dt = null;
                if (lblPaperno.Text.Trim().Length == 18)
                {
                    oldPaperno = paperno.Remove(6, 2);
                    oldPaperno = oldPaperno.Remove(oldPaperno.Length - 1, 1);
                    dt = BusCardHelper.callQuery(context, "QrySpecialCardByPaperNo", paperno, oldPaperno, strCardSurface);
                }
                else if (lblPaperno.Text.Trim().Length == 15)
                {
                    paperno = lblPaperno.Text.Trim();
                    dt = BusCardHelper.callQuery(context, "QrySpecialCardByOldPaperNo", paperno, lblCustName.Text.Trim(), strCardSurface);
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.AddError("该身份证已开通同类特种卡，该卡已不能解挂");
                    return;
                }
            }  
        }

        ////验证是否已经退过款，已退过款不能再进行挂失解挂
        //DataTable data = BusCardHelper.callQuery(context, "QryIsReturnMoney", this.lblCardno.Text.Trim());
        //if (data.Rows.Count > 0)
        //{
        //    string returnDate = data.Rows[0].ItemArray[0].ToString();
        //    context.AddError("已经退过款，无法再挂失解挂，退款时间 " + returnDate);
        //    return;
        //}
        #endregion

        context.SPOpen();
        context.AddField("p_cardNo").Value = this.lblCardno.Text;
        context.AddField("p_cardnewstate").Value = strCardnewstate;
        context.AddField("p_tradetypecode").Value = strTradetypecode;
        context.AddField("p_createtime").Value = "";//后台同步时需要的参数，前台调用时传空字符

        bool ok = context.ExecuteSP("SP_PB_ReportLoss");

        if (ok)
        {
            if (!context.hasError()) context.AddMessage(strMsg);

            btnPrintPZ.Enabled = true;

            //打印凭证
            ASHelper.preparePingZheng(ptnPingZheng, this.lblCardno.Text, this.lblCustName.Text, strTradetypeName,
                "0.00", "", "", this.lblPaperno.Text, "", "0.00",
                "0.00", context.s_UserName, "", this.lblPapertype.Text, "0.00",
                "");
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printdiv('ptnPingZheng1');", true);
            }

            btnQuery_Click(sender, e);
        }
    }
    #endregion

    #region rdo_CheckedChanged RadioButton选择事件
    /// <summary>
    /// RadioButton选择事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rdo_CheckedChanged(object sender, EventArgs e)
    {
        RadioButton rdo = (RadioButton)sender;
        btnSubmit.Enabled = rdo.Checked;
    }
    #endregion
}
