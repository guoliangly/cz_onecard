﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Master;

public partial class ASP_CustomerAcc_CA_BatchOpenOnLinePay :Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 首先清空临时表
        GroupCardHelper.clearTempCustInfoTable(context);
        // 清除gridview数据
        clearGridViewData();

        // 初始化集团客户
        GroupCardHelper.initGroupCustomer(context, selCorp);
    }

    // 输入校验
    private void SubmitValidate()
    {
        Validation valid = new Validation(context);
        // 集团客户列表必须选择
        valid.notEmpty(selCorp, "A004P01I02: 集团客户必须选择");
    }

    // 选中gridview当前页所有数据
    protected void CheckAll(object sender, EventArgs e)
    {
        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (ch.Enabled)
            {
                ch.Checked = cbx.Checked;
            }
        }
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (!ch.Checked)
            {
                btnSubmit.Enabled = false;
                return;
            }
        }
        btnSubmit.Enabled = true;
    }

    protected void Check(object sender, EventArgs e)
    {
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (!ch.Checked)
            {
                btnSubmit.Enabled = false;
                return;
            }
        }
        btnSubmit.Enabled = true;
    }

    private void createCustInfoGrid()
    {
        string[] parm = new string[1];
        parm[0] = Session.SessionID;
        DataTable data = SPHelper.callQuery("SP_CA_Query", context, "BatchOpenOnlinePayChecks", parm);
        UserCardHelper.resetData(gvResult, data);

        DataTable errordata = SPHelper.callQuery("SP_CA_Query", context, "BadOpenOnlineItems", parm);
        if (errordata != null && errordata.Rows.Count > 0 && ((decimal)errordata.Rows[0].ItemArray[0]) > 0)
        {
            context.AddError("共有" + errordata.Rows[0].ItemArray[0] + "张卡片没有通过检验，请操作员确认");
            btnSubmit.Enabled = false;
            return;
        }
        else
        {
            btnSubmit.Enabled = true;
        }
    }

    // 清除gridview数据
    private void clearGridViewData()
    {
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
        btnSubmit.Enabled = false;
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 输入校验
        SubmitValidate();
        if (context.hasError()) return;

        // 调用批量开通在线支付存储过程

        context.DBOpen("StorePro");
        context.AddField("P_SESSIONID").Value = Session.SessionID;
        context.AddField("p_groupCode").Value = selCorp.SelectedValue; // 集团客户编码
        bool ok = context.ExecuteSP("SP_CA_BATCHOPENONLINEPAY");
        if (ok)
        {
            AddMessage("批量开通在线支付成功!");
            DataTable dtPhone = GetGroupOpenPhone();
            for (int i = 0; i < dtPhone.Rows.Count; i++)
            {
                string strMsg = MsgHelper.GetBatchOpenMsg(dtPhone.Rows[i]["f1"].ToString());
                MsgHelper.SendMsg(dtPhone.Rows[i]["f9"].ToString(), strMsg);
            }
        }

        clearGridViewData();
        GroupCardHelper.clearTempCustInfoTable(context);
    }

    private DataTable GetGroupOpenPhone()
    {
        context.DBOpen("Select");
        string strSql1 = string.Format("select t1.f9,t1.f1 from TMP_COMMON t1,TF_F_CARDUSEAREA t2 where t1.F13='{0}' and t1.f1=t2.cardno and t2.functiontype='15'", Session.SessionID);
        DataTable dtPhone = context.ExecuteReader(strSql1);

        return dtPhone;
    }

    // 批量开户文件上传处理

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //验证导入文件格式
        if (!FileUpload1.FileName.ToUpper().EndsWith(".TXT"))
        {
            context.AddError("A006012016:导入文件格式错误，请使用TXT文件");
            return;
        }
        clearGridViewData();
        CAHelper.UploadCustInfoFile(context, FileUpload1, false);
        if (context.hasError()) return;
        createCustInfoGrid();
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl = (Label)e.Row.Cells[0].FindControl("Label");
            CheckBox ch = (CheckBox)e.Row.FindControl("ItemCheckBox");
            string result = gvResult.DataKeys[e.Row.RowIndex]["RESULT2"].ToString();
            if (lbl.Text == "OK")
            {
                ch.Checked = true;
                ch.Enabled = false;
            }
            else if (result == "0")
            {
                ch.Checked = false;
                ch.Enabled = false;
                btnSubmit.Enabled = false;
                e.Row.Cells[0].CssClass = "error";
            }
            else if (result == "1")
            {
                ch.Checked = false;
                ch.Enabled = true;
                btnSubmit.Enabled = false;
                e.Row.Cells[0].CssClass = "error";
            }
            if (e.Row.Cells[8].Text.Trim() == "&nbsp;")
                e.Row.Cells[8].Text = "";
            if (e.Row.Cells[10].Text.Trim() == "&nbsp;")
                e.Row.Cells[10].Text = "";
            if (e.Row.Cells[11].Text.Trim() == "&nbsp;")
                e.Row.Cells[11].Text = "";
        }
    }

}