﻿using System;
using System.Web.UI;
using Common;
using TDO.CardManager;
using TDO.CustomerAcc;
using TDO.ResourceManager;
using TM;
using Master;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;

public partial class ASP_CustomerAcc_CA_ChangeContactInfo : Master.FrontMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            if (!context.s_Debugging)
            {
            }
            ASHelper.initSexList(selCustsex);
            ASHelper.initPaperTypeList(context, selPapertype);
            gvAccount.DataKeyNames = new string[] { "ACCT_ID", "CUST_ID", "ACCT_TYPE_NO","ICCARD_NO", "ACCT_TYPE_NAME","ACCT_ITEM_TYPE", 
                                                    "LIMIT_DAYAMOUNT", "LIMIT_EACHTIME", "EFF_DATE", "STATE", "STATENAME", 
                                                     "Create_Date", "REL_BALANCE", "Total_Consume_Money", "Total_Consume_Times",
                                                     "LAST_CONSUME_TIME",  "Total_Supply_Money", "Total_Supply_Times", "LAST_SUPPLY_TIME"};

        }
    }

    #endregion

    #region Event

    /// <summary>
    /// 读卡按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        ClearAcctInfo();
        ClearUserInfo();
        //加载卡户、用户、账户信息
        LoadDisplayInfo();
        if (!CAHelper.ValidatePhone(txtCustphone.Text.Trim()))
        {
            context.AddError("手机号有误！");
            return;
        }
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        //linkSend.Text = "发送验证码";
        //linkSend.Enabled = true;
    }

    protected void btnDBReadCard_Click(object sender, EventArgs e)
    {
        ClearAcctInfo();
        ClearUserInfo();

        //对卡号空值长度数字的判断
        UserCardHelper.validateCardNo(context, txtCardno, false);
        if (context.hasError())
        {
            return;
        }
        //加载卡户、用户、账户信息
        LoadDisplayInfo();
        if (!CAHelper.ValidatePhone(txtCustphone.Text.Trim()))
        {
            context.AddError("手机号有误！");
            return;
        }
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        //linkSend.Text = "发送验证码";
        //linkSend.Enabled = true;
    }

    /// <summary>
    /// 发送验证码
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void SendCaptcha(object sender, EventArgs e)
    //{
    //    if (string.IsNullOrEmpty(txtCustphone.Text.Trim()))
    //    {
    //        txtCustphone.ToolTip = "手机号不能为空";
    //        txtCustphone.Style["background-color"] = "#FF8C69";
    //        return;
    //    }
    //    if (!CAHelper.ValidatePhone(txtCustphone.Text.Trim()))
    //    {
    //        txtCustphone.ToolTip = "手机号不合法";
    //        txtCustphone.Style["background-color"] = "#FF8C69";
    //        return;
    //    }
    //    string Captcha = MsgHelper.GetRandomNum(6);
    //    context.SPOpen();
    //    context.AddField("p_custphone").Value = txtCustphone.Text.Trim();
    //    context.AddField("p_captcha").Value = Captcha;
    //    context.AddField("p_tradetypecode").Value = "29";
    //    bool ok = context.ExecuteSP("SP_CA_CAPTCHA");
    //    if (ok)
    //    {
    //        context.DBCommit();
    //        linkSend.Text = "59秒后重新发送";
    //        string Msg = string.Empty;
    //        Msg = MsgHelper.GetCaptchaMsg(Captcha, "02");
    //        string RecMsg = MsgHelper.SendMsg(txtCustphone.Text.Trim(), Msg);
    //        if (RecMsg.Contains("0000"))
    //        {
    //            context.AddMessage("发送成功");
    //        }
    //        if (RecMsg.Contains("0001"))
    //        {
    //            context.AddError("数据库异常，发送失败");
    //            return;
    //        }
    //        if (RecMsg.Contains("9999"))
    //        {
    //            context.AddError("其他错误，发送失败");
    //            return;
    //        }
    //    }
    //    linkSend.Enabled = false;
    //}

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ValidateBeforeSubmit() == false)
        {
            return;
        }
        context.SPOpen();
        context.AddField("P_CARDNO").Value=txtCardno.Text.Trim();
        context.AddField("P_CUSTPHONE").Value = txtCustphone.Text.Trim();
        context.AddField("P_CUSTTELPHONE").Value = txtCustTelphone.Text.Trim();
        context.AddField("P_CUSTPOST").Value = txtCustpost.Text.Trim();
        context.AddField("P_CUSTADDR").Value = txtCustaddr.Text.Trim();
        context.AddField("P_CUSTEMAIL").Value = txtEmail.Text.Trim();
        context.AddField("p_CAPTCHA").Value = "";
        context.AddField("P_PAPERTYPE").Value = selPapertype.SelectedValue;
        context.AddField("p_PAPERNO").Value = txtCustpaperno.Text.Trim();
        context.AddField("p_ISVALIDATE").Value ="0";
        bool ok = context.ExecuteSP("SP_CA_CHANGECONTACTINFO");
        if (ok)
        {
            context.DBCommit();
            btnPrintPZ.Enabled = true;
            //ASHelper.preparePingZheng,待补充
            ASHelper.preparePingZheng(ptnPingZheng, txtCardno.Text, txtCusname.Text, "修改联系信息",
"0.0", "", "", txtCustpaperno.Text, "",
"", "", context.s_UserID, context.s_DepartName,
selPapertype.SelectedValue, "0.00", "");

            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printdiv('ptnPingZheng1');", true);
            }

            //string Msg = MsgHelper.GetScuMsg(txtCardno.Text.Trim(),"02");
            //MsgHelper.SendMsg(txtCustphone.Text.Trim(),Msg);
            btnSubmit.Enabled = false;
            //linkSend.Enabled = false;
            this.txtCardno.Text = "";
            AddMessage("M001090110:修改资料成功");
        }
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        //linkSend.Text = "发送验证码";
    }

    #endregion

    #region Function

    /// <summary>
    /// 提交前验证输入项
    /// </summary>
    /// <returns></returns>
    private bool ValidateBeforeSubmit()
    {

        if (string.IsNullOrEmpty(txtCustphone.Text.Trim()))
        {
            context.AddError("手机号码不能为空", txtCustphone);
        }

        if ((!string.IsNullOrEmpty(txtCustphone.Text.Trim()))&&(!CAHelper.ValidatePhone(txtCustphone.Text.Trim())))
        {
            context.AddError("手机号码不合法", txtCustphone);
        }

        if (txtCustTelphone.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustTelphone.Text.Trim()) > 20)
            context.AddError("A001001127:固定电话长度大于20位", txtCustTelphone);
        }

        //对邮政编码进行非空、长度、数字检验
        if (txtCustpost.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                context.AddError("A001001120", txtCustpost);
            else if (!Validation.isNum(txtCustpost.Text.Trim()))
                context.AddError("A001001119", txtCustpost);
        }
        //对联系地址进行非空、长度检验
        if (txtCustaddr.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);
        }
        //对电子邮件进行格式检验
        if (txtEmail.Text.Trim() != "")
            new Validation(context).isEMail(txtEmail);

        return !(context.hasError());
    }

    /// <summary>
    /// 加载卡户、用户、账户信息
    /// </summary>
    private void LoadDisplayInfo()
    {
        #region 卡户信息
        TMTableModule tmTMTableModule = new TMTableModule();

        #region lblResState库存状态
        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }
        #endregion

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTL_R_ICUSEROut.CARDTYPECODE;
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);
        if (ddoTD_M_CARDTYPEOut != null)
        {
            this.LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;//卡类型
        }

        this.LabAsn.Text = ddoTL_R_ICUSEROut.ASN;//asn

        //从IC卡电子钱包账户表中读取数据
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);
        if (ddoTF_F_CARDEWALLETACCOut != null)
        {
            this.cMoney.Text = ((Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY)) / (Convert.ToDecimal(100))).ToString("0.00");
        }

        //从IC卡资料表中读取数据

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        if (ddoTF_F_CARDRECOut != null)
        {
            this.sDate.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");
            this.eDate.Text = ASHelper.toDateWithHyphen(ddoTF_F_CARDRECOut.VALIDENDDATE);
        }

        #endregion

        #region 账户信息

        DataTable data = CAHelper.callQuery(context, "QRYACCTOUNT", txtCardno.Text);
        UserCardHelper.resetData(gvAccount, data);

        Dictionary<String, DDOBase> listDDO = new Dictionary<String, DDOBase>();
        string cust_id;
        if (data != null && data.Rows.Count > 0)
        {
            cust_id = data.Rows[0]["CUST_ID"].ToString(); //取出客户标识
        }
        else
        {
            context.AddError("A001090102:未查出客户账户信息");
            return;
        }

        #endregion

        #region 用户信息

        listDDO = new Dictionary<String, DDOBase>();
        if (!CAHelper.GetCustInfo(context, txtCardno.Text, cust_id, out listDDO))
        {
            return;
        }
        else
        {
            DDOBase ddoTF_F_CUSTOut;
            listDDO.TryGetValue("TF_F_CUST", out ddoTF_F_CUSTOut);
            DDOBase ddoTD_M_PAPERTYPEOut;
            listDDO.TryGetValue("TD_M_PAPERTYPE", out ddoTD_M_PAPERTYPEOut);

            //出生日期显示
            if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH != "")
            {
                String Bdate = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH;
                if (Bdate.Length == 8)
                {
                    txtCustbirth.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else
                {
                    txtCustbirth.Text = Bdate;
                }
            }

            selPapertype.SelectedIndex = selPapertype.Items.IndexOf(selPapertype.Items.FindByValue(((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_TYPE_CODE));
            selCustsex.SelectedIndex = selCustsex.Items.IndexOf(selCustsex.Items.FindByValue(((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_SEX));
            txtCusname.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_NAME;
            txtCustpaperno.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_NO;
            txtCustaddr.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_ADDR;
            txtCustpost.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_POST;
            txtCustphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_PHONE;
            txtCustTelphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_TELPHONE;
            txtEmail.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_EMAIL;
        }

        #endregion

        this.btnSubmit.Enabled = true;
    }

    private void ClearUserInfo()
    {
        txtCusname.Text = "";
        txtCustbirth.Text = "";
        selPapertype.SelectedIndex = 0;
        txtCustpaperno.Text = "";
        selCustsex.SelectedIndex = 0;
        txtCustphone.Text = "";
        txtCustTelphone.Text = "";
        txtCustpost.Text = "";
        txtCustaddr.Text = "";
        txtEmail.Text = "";
        //txtCaptcha.Text = "";
    }

    private void ClearAcctInfo()
    {
        gvAccount.DataSource = new DataTable();
        gvAccount.DataBind();
        gvAccount.SelectedIndex = -1;
    }

    public String getDataKeys2(String keysname)
    {
        return gvAccount.DataKeys[gvAccount.SelectedIndex][keysname].ToString();
    }

    #endregion

}