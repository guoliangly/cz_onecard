﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Master;
using TM;
using Common;
using PDO.PersonalBusiness;
using TDO.CardManager;
using TDO.ResourceManager;
using TDO.BusinessCode;
using TDO.CustomerAcc;
using System.Text;
using System.Text.RegularExpressions;

public partial class ASP_CustomerAcc_CA_OnLinePay :Master.FrontMaster
{
    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            if (!context.s_Debugging) setReadOnly(txtCardno);

            setReadOnly(LabAsn, LabCardtype, cMoney, sDate, eDate);
            ASHelper.initSexList(selCustsex);
            ASHelper.initPaperTypeList(context, selPapertype);
        }
    }

    #endregion

    #region Event

    /// <summary>
    /// 读卡按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        chIsValidate.Checked = true;
        ClearAcctInfo();
        ClearUserInfo();
        //加载卡户、用户、账户信息
        LoadDisplayInfo();

        #region 卡户信息
        Dictionary<String, DDOBase> listDDO = new Dictionary<String, DDOBase>();
        if (!CAHelper.GetCardInfo(context, txtCardno.Text, out listDDO))
        {
            return;
        }
        else
        {
            DDOBase ddoTL_R_ICUSEROut;
            listDDO.TryGetValue("TL_R_ICUSER", out ddoTL_R_ICUSEROut);
            DDOBase ddoTD_M_RESOURCESTATEOut;
            listDDO.TryGetValue("TD_M_RESOURCESTATE", out ddoTD_M_RESOURCESTATEOut);
            DDOBase ddoTD_M_CARDTYPEOut;
            listDDO.TryGetValue("TD_M_CARDTYPE", out ddoTD_M_CARDTYPEOut);
            DDOBase ddoTF_F_CARDEWALLETACCOut;
            listDDO.TryGetValue("TF_F_CARDEWALLETACC", out ddoTF_F_CARDEWALLETACCOut);
            DDOBase ddoTF_F_CARDRECOut;
            listDDO.TryGetValue("TF_F_CARDREC", out ddoTF_F_CARDRECOut);
            #region lblResState库存状态
            if (ddoTD_M_RESOURCESTATEOut == null)
                lblResState.Text = ((TL_R_ICUSERTDO)ddoTL_R_ICUSEROut).RESSTATECODE;
            else
                lblResState.Text = ((TD_M_RESOURCESTATETDO)ddoTD_M_RESOURCESTATEOut).RESSTATE;
            #endregion
            this.lblCardtype.Text = ((TD_M_CARDTYPETDO)ddoTD_M_CARDTYPEOut).CARDTYPENAME;//卡类型
            this.lblAsn.Text = ((TL_R_ICUSERTDO)ddoTL_R_ICUSEROut).ASN;//asn
            //this.lblCardccMoney.Text = ((Convert.ToDecimal(((TF_F_CARDEWALLETACCTDO)ddoTF_F_CARDEWALLETACCOut).CARDACCMONEY)) / (Convert.ToDecimal(100))).ToString("0.00");
            this.lblSellDate.Text = ((TF_F_CARDRECTDO)ddoTF_F_CARDRECOut).SELLTIME.ToString("yyyy-MM-dd");
            this.lblEndDate.Text = ASHelper.toDateWithHyphen(((TF_F_CARDRECTDO)ddoTF_F_CARDRECOut).VALIDENDDATE);
        }

        //查询卡片开通功能并显示
        PBHelper.openFunc(context, openFunc, txtCardno.Text);
        #endregion

        ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        linkSend.Text = "发送验证码";

        if (!ValidateCard())
        {
            return;
        }

        InitAccInfo();
        InitRadio();
        this.linkSend.Enabled = true;
        this.btnSubmit.Enabled = true;
    }

    /// <summary>
    /// 发送验证码
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SendCaptcha(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCustphone.Text.Trim()))
        {
            txtCustphone.ToolTip = "手机号不能为空";
            txtCustphone.Style["background-color"] = "#FF8C69";
            return;
        }
        if (!CAHelper.ValidatePhone(txtCustphone.Text.Trim()))
        {
            txtCustphone.ToolTip = "手机号不合法";
            txtCustphone.Style["background-color"] = "#FF8C69";
            return;
        }
        string Captcha = MsgHelper.GetRandomNum(6);
        context.SPOpen();
        context.AddField("p_custphone").Value = txtCustphone.Text.Trim();
        context.AddField("p_captcha").Value = Captcha;
        string type=(rdoOpen.Checked == true) ? "9D" : "D9";
        context.AddField("p_tradetypecode").Value = type;
        bool ok = context.ExecuteSP("SP_CA_CAPTCHA");
        if (ok)
        {
            context.DBCommit();
            linkSend.Text = "59秒后重新发送";
            string Msg = string.Empty;
            string TradeType = rdoOpen.Checked == true ? "00" : "01";
            Msg = MsgHelper.GetCaptchaMsg(Captcha, TradeType);
            string RecMsg = MsgHelper.SendMsg(txtCustphone.Text.Trim(), Msg);
            if (RecMsg.Contains("0000"))
            {
                context.AddMessage("发送成功");
            }
            if (RecMsg.Contains("0001"))
            {
                context.AddError("数据库异常，发送失败");
                return;
            }
            if (RecMsg.Contains("9999"))
            {
                context.AddError("其他错误，发送失败");
                return;
            }
        }
        linkSend.Enabled = false;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ValidateBeforeSubmit() == false)
        {
            return;
        }

        context.SPOpen();
        context.AddField("P_CARDNO").Value = this.txtCardno.Text;
        context.AddField("P_CUSTPHONE").Value = txtCustphone.Text.Trim();
        context.AddField("P_CUSTTELPHONE").Value = txtCustTelphone.Text.Trim();
        context.AddField("P_CUSTPOST").Value = txtCustpost.Text.Trim();
        context.AddField("P_CUSTADDR").Value = txtCustaddr.Text.Trim();
        context.AddField("P_CUSTEMAIL").Value = txtEmail.Text.Trim();
        context.AddField("p_CAPTCHA").Value = txtCaptcha.Text.Trim();
        context.AddField("p_FUNCTIONCODE").Value =rdoOpen.Checked==true?"OPEN":"CLOSE";
        context.AddField("p_ISVALIDATE").Value = chIsValidate.Checked ? "1" : "0";
        context.AddField("p_PAPERNO").Value = txtCustpaperno.Text.Trim();
        context.AddField("P_PAPERTYPE").Value = selPapertype.SelectedValue;
        context.AddField("p_TRADETYPE").Value = rdoOpen.Checked == true ? "9D" : "D9";

        bool ok = context.ExecuteSP("SP_CA_ONLINEPAY");
        if (ok)
        {
            context.DBCommit();
            btnPrintPZ.Enabled = true;
            //ASHelper.preparePingZheng,待补充
            string str = string.Empty;
            if (rdoOpen.Checked)
            {
                str = "开通在线支付";
            }
            else
            {
                str = "关闭在线支付";
            }
            ASHelper.preparePingZheng(ptnPingZheng, this.txtCardno.Text, txtCusname.Text, str, "0", "", "", txtCustpaperno.Text,
                "", "", "", context.s_UserName, context.s_DepartName, selPapertype.SelectedValue, "0", "");
            if (chkPingzheng.Checked && btnPrintPZ.Enabled)
            {
                ScriptManager.RegisterStartupScript(
                    this, this.GetType(), "writeCardScript",
                    "printdiv('ptnPingZheng1');", true);
            }
            if (rdoOpen.Checked)
            {
                AddMessage("在线支付开通成功");
                string Msg = MsgHelper.GetScuMsg(txtCardno.Text.Trim(), "00");
                MsgHelper.SendMsg(txtCustphone.Text.Trim(),Msg);
            }
            else
            {
                AddMessage("在线支付关闭成功");
                string Msg = MsgHelper.GetScuMsg(txtCardno.Text.Trim(),"01");
                MsgHelper.SendMsg(txtCustphone.Text.Trim(), Msg);
            }
            btnSubmit.Enabled = false;
            linkSend.Enabled = false;
            this.txtCardno.Text = "";
            InitAccInfo();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        linkSend.Text = "发送验证码";
    }

    #endregion

    #region Function

    /// <summary>
    /// 提交前验证输入项
    /// </summary>
    /// <returns>验证结果</returns>
    private bool ValidateBeforeSubmit()
    {
        //if (string.IsNullOrEmpty(txtCustphone.Text.Trim()))
        //{
        //    context.AddError("手机号码不能为空",txtCustphone);
        //}

        if (!CAHelper.ValidatePhone(txtCustphone.Text.Trim()))
        {
            context.AddError("手机号码不合法",txtCustphone);
        }

        if (txtCustTelphone.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustTelphone.Text.Trim()) > 20)
                context.AddError("A001001127:固定电话长度大于20位", txtCustTelphone);
        }

        //对邮政编码进行非空、长度、数字检验
        if (txtCustpost.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustpost.Text.Trim()) != 6)
                context.AddError("A001001120", txtCustpost);
            else if (!Validation.isNum(txtCustpost.Text.Trim()))
                context.AddError("A001001119", txtCustpost);
        }
        //对联系地址进行非空、长度检验
        if (txtCustaddr.Text.Trim() != "")
        {
            if (Validation.strLen(txtCustaddr.Text.Trim()) > 50)
                context.AddError("A001001128", txtCustaddr);
        }
        //对电子邮件进行格式检验
        if (txtEmail.Text.Trim() != "")
            new Validation(context).isEMail(txtEmail);

        return !(context.hasError()); 
    }

    /// <summary>
    /// 验证是否可以开通在线支付
    /// </summary>
    /// <param name="cardno"></param>
    /// <returns></returns>
    private bool ValidateCard()
    {
        string strSql = string.Format("select * from TF_F_CARDCOUNTACC where cardno='{0}' and apptype='01'",txtCardno.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        if (dt.Rows.Count > 0)
        {
            context.AddError("学生卡不能开通在线支付");
            return false;
        }

        string strSql1 = string.Format("select * from TF_F_CUST_ACCT where iccard_no='{0}' and acct_type_no='0001' and state='A'",txtCardno.Text.Trim());
        DataTable dt1 = context.ExecuteReader(strSql1);
        if (dt1.Rows.Count == 0)
        {
            context.AddError("未开通账户宝或账户状态无效");
            return false;
        }

        string strPwd = DealString.encrypPass("111111").ToString();
        string strSql2 = string.Format("select * from TF_F_CUST_ACCT where iccard_no='{0}' and CUST_PASSWORD='{1}' and acct_type_no='0001'", txtCardno.Text.Trim(), strPwd);
        DataTable dt2 = context.ExecuteReader(strSql2);
        if (dt2.Rows.Count > 0)
        {
            context.AddError("请先修改初始密码");
            return false;
        }
        context.DBCommit();
        return true;
    }

    /// <summary>
    /// 初始化账户信息
    /// </summary>
    /// <returns>账户是否存在</returns>
    private bool InitAccInfo()
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //客户账户
        DataTable data = CAHelper.callQuery(context, "QRYACCTOUNT", txtCardno.Text.Trim());
        UserCardHelper.resetData(gvAccount, data);
        return true;
    }

    /// <summary>
    /// 初始化radio
    /// </summary>
    private void InitRadio()
    {
        string strSql = string.Format("select * from TF_F_CARDUSEAREA where cardno='{0}' and functiontype='15'", txtCardno.Text.Trim());
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();
        if (dt.Rows.Count > 0)
        {
            rdoOpen.Checked = false;
            rdoClose.Disabled = false;
            rdoClose.Checked = true;
            rdoOpen.Disabled = true; ;
        }
        else
        {
            rdoClose.Checked = false;
            rdoOpen.Disabled = false;
            rdoOpen.Checked = true;
            rdoClose.Disabled = true;
        }
    }

    /// <summary>
    /// 加载卡户、用户、账户信息
    /// </summary>
    private void LoadDisplayInfo()
    {
        #region 卡户信息
        TMTableModule tmTMTableModule = new TMTableModule();

        #region lblResState库存状态
        //从用户卡库存表(TL_R_ICUSER)中读取数据
        TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
        ddoTL_R_ICUSERIn.CARDNO = txtCardno.Text;

        TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

        if (ddoTL_R_ICUSEROut == null)
        {
            context.AddError("A001001101");
            return;
        }
        #endregion

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = ddoTL_R_ICUSEROut.CARDTYPECODE;
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);
        if (ddoTD_M_CARDTYPEOut != null)
        {
            this.LabCardtype.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;//卡类型
        }

        this.LabAsn.Text = ddoTL_R_ICUSEROut.ASN;//asn

        //从IC卡电子钱包账户表中读取数据
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
        ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardno.Text;
        TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);
        if (ddoTF_F_CARDEWALLETACCOut != null)
        {
            this.cMoney.Text = ((Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY)) / (Convert.ToDecimal(100))).ToString("0.00");
        }

        //从IC卡资料表中读取数据

        TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
        ddoTF_F_CARDRECIn.CARDNO = txtCardno.Text;

        TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);
        if (ddoTF_F_CARDRECOut != null)
        {
            this.sDate.Text = ddoTF_F_CARDRECOut.SELLTIME.ToString("yyyy-MM-dd");
            this.eDate.Text = ASHelper.toDateWithHyphen(ddoTF_F_CARDRECOut.VALIDENDDATE);
        }

        #endregion

        #region 账户信息

        DataTable data = CAHelper.callQuery(context, "QRYACCTOUNT", txtCardno.Text);
        UserCardHelper.resetData(gvAccount, data);

        Dictionary<String, DDOBase> listDDO = new Dictionary<String, DDOBase>();
        string cust_id;
        if (data != null && data.Rows.Count > 0)
        {
            cust_id = data.Rows[0]["CUST_ID"].ToString(); //取出客户标识
        }
        else
        {
            context.AddError("A001090102:未查出客户账户信息");
            return;
        }

        #endregion

        #region 用户信息

        listDDO = new Dictionary<String, DDOBase>();
        if (!CAHelper.GetCustInfo(context, txtCardno.Text, cust_id, out listDDO))
        {
            return;
        }
        else
        {
            DDOBase ddoTF_F_CUSTOut;
            listDDO.TryGetValue("TF_F_CUST", out ddoTF_F_CUSTOut);
            DDOBase ddoTD_M_PAPERTYPEOut;
            listDDO.TryGetValue("TD_M_PAPERTYPE", out ddoTD_M_PAPERTYPEOut);

            //出生日期显示
            if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH != "")
            {
                String Bdate = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH;
                if (Bdate.Length == 8)
                {
                    txtCustbirth.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else
                {
                    txtCustbirth.Text = Bdate;
                }
            }

            selPapertype.SelectedIndex = selPapertype.Items.IndexOf(selPapertype.Items.FindByValue(((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_TYPE_CODE));
            selCustsex.SelectedIndex = selCustsex.Items.IndexOf(selCustsex.Items.FindByValue(((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_SEX));
            txtCusname.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_NAME;
            txtCustpaperno.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_NO;
            txtCustaddr.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_ADDR;
            txtCustpost.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_POST;
            txtCustphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_PHONE;
            txtCustTelphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_TELPHONE;
            txtEmail.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_EMAIL;
        }
        #endregion
    }

    private void ClearUserInfo()
    {
        txtCusname.Text = "";
        txtCustbirth.Text = "";
        selPapertype.SelectedIndex = 0;
        txtCustpaperno.Text = "";
        selCustsex.SelectedIndex = 0;
        txtCustphone.Text = "";
        txtCustTelphone.Text = "";
        txtCustpost.Text = "";
        txtCustaddr.Text = "";
        txtEmail.Text = "";
        txtCaptcha.Text = "";
    }

    private void ClearAcctInfo()
    {
        gvAccount.DataSource = new DataTable();
        gvAccount.DataBind();
        gvAccount.SelectedIndex = -1;
    }

    #endregion
}