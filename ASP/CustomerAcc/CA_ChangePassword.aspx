﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CA_ChangePassword.aspx.cs"
    Inherits="ASP_CustomerAcc_CA_ChangePassword"  EnableEventValidation="false" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>修改账户密码</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <link  href="../../css/Cust_AS.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
       var flagPW = '-1';

       function $(_sId) {
           return window.document.getElementById(_sId);
       }

       function ReadNewKey() {
           try {

               var com = "4,9600,n,8,1";
               reta = ICcard.Read_Key(com, 1, 25000);
               return reta;
           }
           catch (e) {
               return "";
           }
       }
       function ReadNew2Key() {
           try {
               var com = "4,9600,n,8,1";
               reta = ICcard.Read_Key(com, 2, 25000);
               return reta;
           }
           catch (e) {
               return "";
           }
       }

       function StartPort() {
           strInput = "";
           if (document.getElementById('cbxIsFirst').checked) {
               flagPW = '1';
               $('txtOPwd').value = '111111';
               $('txtOPwd').style.readOnly = true;
               $('txtNPwd').value = '';
               $('txtNPwd').focus();
           }
           else {
               flagPW = '0';
               $('txtOPwd').value = '';
               $('txtOPwd').focus();
           }

           MSComm1_OnComm();
       }
       function MSComm1_OnComm() {
           if (flagPW == '0') {
               strInput = ReadNewKey();
           } else if (flagPW == '1') {
               strInput = ReadNewKey();
           } else if (flagPW == '2') {
               strInput = ReadNew2Key();
           }

           if (strInput == null || strInput == "") {
               MyExtAlert('密码键盘有误：', '请检查密码键盘是否插好' + ', 错误信息:' + strInput);
               return;
           } else {
               if (strInput.indexOf('串口') > -1) {
                   MyExtAlert('密码键盘有误：', '请检查密码键盘和COM端口号' + ', 错误信息:' + strInput);
                   return;
               }
           }

           if (strInput != "") {
               if (flagPW == '0') {
                   flagPW = '1';
                   $("txtOPwd").value = strInput;
                   strInput = "";
                   $("txtNPwd").value = '';
                   $("txtNPwd").focus();
                   MSComm1_OnComm();
               } else if (flagPW == '1') {
                   flagPW = '2';
                   $("txtNPwd").value = strInput;
                   strInput = "";
                   $("txtANPwd").value = '';
                   $("txtANPwd").focus();
                   MSComm1_OnComm();
               } else if (flagPW == '2') {
                   flagPW = '-1';
                   $("txtANPwd").value = strInput;
                   strInput = "";
                   if (!$("btnChangeUserPass").disabled) {
                       $("btnChangeUserPass").focus();
                   }

               }
           }
       }

       function IsFirstChange() {
           if ($('cbxIsFirst').checked) {
               $('chMsg').disabled = false;
               $('txtOPwd').value = '111111';
               //$('txtNPwd').value = '';
               //$('txtANPwd').value = '';
               $('txtOPwd').readOnly = true;
               $('txtNPwd').value = '';
               $('txtNPwd').focus();
               $('chOnline').disabled = false;
               $('chOnline').checked = false;
               $('txtCaptcha').readOnly = false;
           }
           else {
               $('txtOPwd').value = '';
               //$('txtNPwd').value = '';
               //$('txtANPwd').value = '';
               $('txtOPwd').readOnly = false;
               $('txtOPwd').focus();
               $('chOnline').disabled = true;
               $('txtCaptcha').readOnly = true;
               $('chMsg').disabled = true;
               $('chOnline').checked = false;
               $('chMsg').checked = false;
           }
       }

       function StartPort1() {
           strInput = "";
           $('txtCaptcha').value = "";
           $('txtCaptcha').focus();
           MSComm2_OnComm();
       }
       function MSComm2_OnComm() {
           strInput = ReadNewKey();

           if (strInput == null || strInput == "") {
               MyExtAlert('密码键盘有误：', '请检查密码键盘是否插好' + ', 错误信息:' + strInput);
               return;
           } else {
               if (strInput.indexOf('串口') > -1) {
                   MyExtAlert('密码键盘有误：', '请检查密码键盘和COM端口号' + ', 错误信息:' + strInput);
                   return;
               }
           }
           $('txtCaptcha').value = strInput;
           strInput = "";
       }
    </script>
    <script type="text/javascript">
        var totalTime = parseInt(60);
        var mytimeout = "";
        function clickTest() {
            document.getElementById("link").click();
            window.document.getElementById('linkSend').disabled = true;
            countDown();
        }
        function countDown() {
            if (totalTime == 1) {
                window.document.getElementById('linkSend').innerText = "重新发送";
                window.document.getElementById('linkSend').disabled = false;
                totalTime = parseInt(60);
                //return;
            } else {
                totalTime = totalTime - 1;
                window.document.getElementById('linkSend').innerText = totalTime + "秒后重新发送";
                mytimeout=setTimeout("countDown()", 1000);
            }
        }
        function resetbtn() {
            if (mytimeout != "" || mytimeout != null) {
                totalTime = parseInt(60);
                clearTimeout(mytimeout);
                window.document.getElementById('linkSend').innerText = "发送验证码";
            }
        }
</script>

    <%--实现gridview全选与反选,2014/7/8,陈文涛--%>
    <script type="text/javascript">
        function selectAll(ctlName, bool) {
            var ctl = document.getElementById(ctlName); //根据控件的在客户端所呈现的ID获取控件
            var checkbox = ctl.getElementsByTagName('input'); //获取该控件内标签为input的控件
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].type == 'checkbox') {
                    checkbox[i].checked = bool;
                }
            }
        }
    </script>

</head>
<body>
<OBJECT classid="clsid:016E79EE-0B07-46E1-8866-2B679B73898A"id="ICcard"
            codebase="ICcard.ocx" width="0" height="0">
</object>
    <cr:CardReader ID="cardReader" runat="server" />
    <form id="form1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="cust_left"></td>
            <td class="cust_mid">账户宝>修改密码</td>
            <td class="cust_right"></td>
         </tr>
        </table>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />

        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidPwdNum" Value="0" runat="server" />
                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>

                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <asp:HiddenField runat="server" ID="hidWarning" />
                <asp:HiddenField ID="hiddenCardRent" runat="server" />
                <asp:HiddenField ID="hiddenOldPassword" runat="server" />
                <asp:HiddenField ID="hiddenNewPassword" runat="server" />
                <asp:HiddenField ID="hiddenACCTYPENO" runat="server" />
                <div class="cust_tabbox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
    	                <td class="cust_top1_l"><div class="cust_p1"></div></td>
                        <td class="cust_top1_m"><span style="line-height:22px; margin-left:5px; color:#333;">卡内信息</span></td>
                        <td class="cust_top1_r"></td>
                    </tr>
                    </table>
                    <div class="cust_line1"></div>
                    <div class="cust_line2"></div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="cust_form">
                    <tr>
                    <th width="9%">
                            用户卡号：
                    </th>
                    <td width="14%">
                        <asp:TextBox ID="txtCardno" CssClass="labeltext" runat="server"></asp:TextBox></td>
                    <asp:HiddenField ID="hiddenread" runat="server" />
                    <td width="10%">
                    <div class="cust_bton1"><asp:LinkButton ID="btnReadCard"  runat="server" Text="读卡" OnClientClick="return readCardNo();"
                            OnClick="btnReadCard_Click" /></div>
                    </td>
                    <td width="11%">
                        &nbsp;</td>
                    <td width="18%">
                        &nbsp;</td>
                    <td width="11%">
                        &nbsp;</td>
                    <td width="12%">
                        &nbsp;</td>
                    </tr>
                    </table>
                </div>
                <div class="cust_tabbox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
    	                <td class="cust_top1_l"><div class="cust_p3"></div></td>
                        <td class="cust_top1_m"><span style="line-height:22px; margin-left:5px; color:#333;">账户信息</span></td>
                        <td class="cust_top1_r"></td>
                    </tr>
                    </table>
                    <div class="cust_line1"></div>
                    <div class="cust_line2"></div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div>  <%--增加了checkbox模版列 陈文涛，2014/7/8--%>
                                    <asp:GridView ID="gvAccount" runat="server" Width="100%" CssClass="tab1" HeaderStyle-CssClass="tabbt"
                                            AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast"
                                            PageSize="10" AllowPaging="True"  PagerStyle-HorizontalAlign="left"
                                            PagerStyle-VerticalAlign="Top" AutoGenerateColumns="false" 
                                            EmptyDataText="没有数据记录!"
                                            >
                                            <Columns>
                                            <asp:TemplateField>
                                            <HeaderTemplate>
                                            <asp:CheckBox runat=server ID="selAll" onclick="javascript:selectAll('gvAccount',this.checked);"/>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                            <asp:CheckBox runat=server ID="selAcc" ToolTip='<%#Eval("ACCT_TYPE_NO") %>'/>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:BoundField DataField="ACCT_TYPE_NAME" HeaderText="账户类型" />
                                                <asp:BoundField DataField="LIMIT_DAYAMOUNT" HeaderText="每日消费限额" />
                                                <asp:BoundField DataField="LIMIT_EACHTIME" HeaderText="每笔消费限额" />
                                                <asp:BoundField DataField="EFF_DATE" HeaderText="生效日期" />
                                                <asp:BoundField DataField="STATENAME" HeaderText="状态" />
                                                <asp:BoundField DataField="Create_Date" HeaderText="创建日期" />
                                                <asp:BoundField DataField="REL_BALANCE" HeaderText="账户宝余额" />
                                                <asp:BoundField DataField="Total_Consume_Money" HeaderText="总消费金额" />
                                                <asp:BoundField DataField="Total_Consume_Times" HeaderText="总消费次数" />
                                                <asp:BoundField DataField="LAST_CONSUME_TIME" HeaderText="最后消费时间" />
                                                <asp:BoundField DataField="Total_Supply_Money" HeaderText="总充值金额" />
                                                <asp:BoundField DataField="Total_Supply_Times" HeaderText="总充值次数" />
                                                <asp:BoundField DataField="LAST_SUPPLY_TIME" HeaderText="最后充值时间" />
                                            </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                </div>
                <div class="cust_tabbox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
    	            <td class="cust_top1_l"><div class="cust_p10"></div></td>
                    <td class="cust_top1_m"><span style="line-height:22px; margin-left:5px; color:#333;">用户信息</span></td>
                    <td class="cust_top1_r"></td>
                    </tr>
                    </table>
                    <div class="cust_line1"></div>
                    <div class="cust_line2"></div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="cust_form">
                            <tr>
                                <th style="width: 10%">
                                        用户姓名：
                                </th>
                                <td style="width: 17%">
                                    <asp:Label ID="lblCustName" runat="server" CssClass="labeltext"></asp:Label>
                                </td>
                                <th style="width: 10%">
                                        出生日期：
                                </th>
                                <td style="width: 15%">
                                    <asp:Label ID="lblCustBirthday" runat="server" CssClass="labeltext"></asp:Label>
                                </td>
                                <th style="width: 10%">
                                        证件类型：
                                </th>
                                <td style="width: 10%">
                                    <asp:Label ID="lblPapertype" runat="server" CssClass="labeltext"></asp:Label>
                                    <input type="hidden" id="hidPapertype" runat="server" />
                                </td>
                                <th style="width: 10%">
                                        证件号码：
                                </th>
                                <td style="width: 18%">
                                    <asp:Label ID="lblPaperno" runat="server" CssClass="labeltext"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                        用户性别：
                                </th>
                                <td>
                                    <asp:Label ID="lblCustsex" runat="server" CssClass="labeltext"></asp:Label></td>
                                <th>
                                        手机号码：
                                </th>
                                <td>
                                    <asp:Label ID="lblCustphone" runat="server" CssClass="labeltext"></asp:Label></td>
                                <th>
                                        固定电话：
                                </th>
                                <td>
                                    <asp:Label ID="lblCustTelphone" runat="server" CssClass="labeltext"></asp:Label>
                                </td>
                                <th>
                                        邮政编码：
                                </th>
                                <td>
                                    <asp:Label ID="lblCustpost" runat="server" CssClass="labeltext"></asp:Label></td>
                            </tr>
                            <tr>
                                <th>
                                        电子邮件：
                                </th>
                                <td>
                                    <asp:Label ID="lblEmail" runat="server" CssClass="labeltext"></asp:Label>
                                </td>
                                <th valign="top">
                                        联系地址：
                                </th>
                                <td colspan="5">
                                    <asp:Label ID="lblCustaddr" runat="server" CssClass="labeltext"></asp:Label></td>
                            </tr>
                        </table>
                </div>
                <div class="cust_tabbox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
    	            <td class="cust_top1_l"><div class="cust_p7"></div></td>
                    <td class="cust_top1_m"><span style="line-height:22px; margin-left:5px; color:#333;">修改密码</span></td>
                    <td class="cust_top1_r"></td>
                    </tr>
                    </table>
                    <div class="cust_line1"></div>
                    <div class="cust_line2"></div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="cust_form">
                            <tr>
                                <td>
                                    <div align="center" valign="middle">
                                    <label for="cbxIsFirst">首次修改</label>
                                        <input type="checkbox" onclick="IsFirstChange();" id="cbxIsFirst" runat="server"
                                            value="首次修改" />
                                    </div>
                                </td>
                                <td width="15%">
                                    <div class="cust_bton2"><a id="btnPassInput"  style="width: 100px"  
                                        onclick="StartPort();return false;">小键盘输入密码</a></div>
                                </td>
                                <th>
                                    <div align="right">
                                        原密码:</div>
                                </th>
                                <td> <%--ontextchanged="txtOPwd_TextChanged"--%>
                                    <asp:TextBox ID="txtOPwd" CssClass="input" TextMode="Password" MaxLength="6"
                                        runat="server">
                                    </asp:TextBox>
                                </td>
                                <th>
                                    <div align="right">
                                        新密码:</div>
                                </th>
                                <td>
                                    <asp:TextBox ID="txtNPwd" CssClass="input" TextMode="Password" MaxLength="6"
                                        runat="server"></asp:TextBox></td>
                                <th>
                                    <div align="right">
                                        新密码确认:</div>
                                </th>
                                <td>
                           <asp:TextBox ID="txtANPwd" CssClass="input" TextMode="Password" MaxLength="6"
                            runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                            <td>
                            <div align="center" valign="middle">
                            <label for="chOnline">在线支付</label>
                            <input type="checkbox" id="chOnline" runat="server"
                            value="开通在线支付" disabled=disabled/>
                             </div>
                             </td>
                             <td>
                             <div class=cust_bton2><asp:LinkButton runat=server Text="发送验证码" ID="linkSend" OnClientClick="clickTest()" Enabled=false></asp:LinkButton></div>
                             <asp:LinkButton runat=server ID="link" OnClick="SendCaptcha"></asp:LinkButton>
                             </td>
                        <th>
                        <div align="right">
                        验证码:</div>
                        </th>
                        <td>
                         <asp:TextBox ID="txtCaptcha" CssClass="input" MaxLength="6"
                         runat="server"></asp:TextBox>
                        </td>
                        <td><div class="cust_bton2"><a id="A1"  style="width: 100px"
                        onclick="StartPort1();return false;">小键盘输入</a></td>
                        <td colspan="3">
                        <label for="chMsg">短信验证</label>
                        <input type=checkbox runat=server id="chMsg" value="短信验证" disabled=disabled/>
                        </td>
                        </tr>
                    </table>
                </div>
                <div class="cust_tabbox">
                    <div class="cust_bottom_bton"><asp:LinkButton ID="btnChangeUserPass" runat="server" Text="修改" OnClick="btnChangeUserPass_Click"
                              OnClientClick="return confirm('确认修改?');"   Enabled="false" /></div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
