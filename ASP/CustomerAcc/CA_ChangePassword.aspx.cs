﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using TDO.CardManager;
using TM;
using Common;
using TDO.BusinessCode;
using PDO.PersonalBusiness;
using TDO.CustomerAcc;
using TDO.UserManager;
using System.Text;

/***************************************************************
 * 功能名: 账户宝_修改密码
 * 更改日期      姓名           摘要
 * ----------    -----------    --------------------------------
 * 2011/06/21    liuh			初次开发

 ****************************************************************/
public partial class ASP_CustomerAcc_CA_ChangePassword : Master.Master
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
        {
            if (!context.s_Debugging) txtCardno.Attributes["readonly"] = "true";
            gvAccount.DataKeyNames = new string[] { "ACCT_ID", "CUST_ID", "ACCT_TYPE_NO","ICCARD_NO", "ACCT_TYPE_NAME","ACCT_ITEM_TYPE", 
                                                    "LIMIT_DAYAMOUNT", "LIMIT_EACHTIME", "EFF_DATE", "STATE", "STATENAME", 
                                                     "Create_Date", "REL_BALANCE", "Total_Consume_Money", "Total_Consume_Times",
                                                     "LAST_CONSUME_TIME",  "Total_Supply_Money", "Total_Supply_Times", "LAST_SUPPLY_TIME"};
        }
    }

    //清空用户信息
    private void ClearUserInfo()
    {
        lblCustName.Text = "";
        lblCustBirthday.Text = "";
        lblPapertype.Text = "";
        lblPaperno.Text = "";
        lblCustsex.Text = "";
        lblCustphone.Text = "";
        lblCustTelphone.Text = "";
        lblCustpost.Text = "";
        lblEmail.Text = "";
        lblCustaddr.Text = "";
    }

    #endregion

    #region Event

    /// <summary>
    /// 读卡按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        cbxIsFirst.Checked = false;
        ClearUserInfo();
        chOnline.Checked = false;
        ViewState["change"] = "1";
        txtOPwd_TextChanged(sender, e);

        #region 账户信息

        DataTable data = CAHelper.callQuery(context, "QRYACCTOUNT", txtCardno.Text);
        UserCardHelper.resetData(gvAccount, data);

        Dictionary<String, DDOBase> listDDO = new Dictionary<String, DDOBase>();
        string cust_id;
        if (data != null && data.Rows.Count > 0)
        {
            cust_id = data.Rows[0]["CUST_ID"].ToString(); //取出客户标识
        }
        else
        {
            context.AddError("A001090102:未查出客户账户信息");
            return;
        }

        #endregion

        #region 用户信息
        listDDO = new Dictionary<String, DDOBase>();
        if (!CAHelper.GetCustInfo(context, txtCardno.Text, cust_id, out listDDO))
        {
            return;
        }
        else
        {
            DDOBase ddoTF_F_CUSTOut;
            listDDO.TryGetValue("TF_F_CUST", out ddoTF_F_CUSTOut);
            DDOBase ddoTD_M_PAPERTYPEOut;
            listDDO.TryGetValue("TD_M_PAPERTYPE", out ddoTD_M_PAPERTYPEOut);

            //证件类型显示
            if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_TYPE_CODE != "")
            {
                lblPapertype.Text = ((TD_M_PAPERTYPETDO)ddoTD_M_PAPERTYPEOut).PAPERTYPENAME;
            }
            else
            {
                lblPapertype.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_TYPE_CODE;
            }

            //性别显示
            if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_SEX == "0")
                lblCustsex.Text = "男";
            else if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_SEX == "1")
                lblCustsex.Text = "女";
            else lblCustsex.Text = "";

            //出生日期显示
            if (((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH != "")
            {
                String Bdate = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_BIRTH;
                if (Bdate.Length == 8)
                {
                    lblCustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else
                {
                    lblCustBirthday.Text = Bdate;
                }
            }

            lblCustName.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_NAME;
            lblPaperno.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_NO;
            lblCustaddr.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_ADDR;
            lblCustpost.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_POST;
            lblCustphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_PHONE;
            lblCustTelphone.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_TELPHONE;
            lblEmail.Text = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).CUST_EMAIL;

            if (!CommonHelper.HasOperPower(context))
            {
                lblPaperno.Text = CommonHelper.GetPaperNo(lblPaperno.Text);
                lblCustphone.Text = CommonHelper.GetCustPhone(lblCustphone.Text);
                lblCustTelphone.Text = CommonHelper.GetCustPhone(lblCustTelphone.Text);
                lblCustaddr.Text = CommonHelper.GetCustAddress(lblCustaddr.Text);
            }
        }
        #endregion
        ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
        linkSend.Text = "发送验证码";
        this.btnChangeUserPass.Enabled = true;
        linkSend.Enabled = true;
    }

    /// <summary>
    /// 发送验证码
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SendCaptcha(object sender, EventArgs e)
    {
        ViewState["change"] = "0";
        if (string.IsNullOrEmpty(lblCustphone.Text.Trim()))
        {
            lblCustphone.ToolTip = "手机号不能为空";
            lblCustphone.Style["background-color"] = "#FF8C69";
            return;
        }
        if (!CAHelper.ValidatePhone(lblCustphone.Text.Trim()))
        {
            lblCustphone.ToolTip = "手机号不合法";
            lblCustphone.Style["background-color"] = "#FF8C69";
            return;
        }
        string Captcha = MsgHelper.GetRandomNum(6);
        context.SPOpen();
        context.AddField("p_custphone").Value = lblCustphone.Text.Trim();
        context.AddField("p_captcha").Value = Captcha;
        context.AddField("p_tradetypecode").Value = "29";
        bool ok = context.ExecuteSP("SP_CA_CAPTCHA");
        if (ok)
        {
            context.DBCommit();
            linkSend.Text = "59秒后重新发送";
            string Msg = string.Empty;
            Msg = MsgHelper.GetCaptchaMsg(Captcha, "00");
            string RecMsg = MsgHelper.SendMsg(lblCustphone.Text.Trim(), Msg);
            if (RecMsg.Contains("0000"))
            {
                context.AddMessage("发送成功");
            }
            if (RecMsg.Contains("0001"))
            {
                context.AddError("数据库异常，发送失败");
                return;
            }
            if (RecMsg.Contains("9999"))
            {
                context.AddError("其他错误，发送失败");
                return;
            }
        }
        txtOPwd_TextChanged(sender,e);
        chOnline.Disabled = false;
        chMsg.Disabled = false;
        linkSend.Enabled = false;
    }

    /// <summary>
    /// 修改密码按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnChangeUserPass_Click(object sender, EventArgs e)
    {
        linkSend.Text = "发送验证码";
        ViewState["change"] = "0";
        if (!ChangePasswordValidation())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
            linkSend.Enabled = true;
            return;
        }

        if (txtCardno.Text == "")
        {
            context.AddError("A005001100");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
            linkSend.Enabled = true;
            return;
        }

        string oldPwd = DealString.encrypPass(txtOPwd.Text.Trim());//CAHelper.Md5Encrypt(txtOPwd.Text.Trim());
        if (this.cbxIsFirst.Checked)
        {
            //如果是首次修改，对比配置表中的初始密码和账户表密码
            TMTableModule tmTMTableModule = new TMTableModule();
            oldPwd = DealString.encrypPass("111111").ToString();

            context.DBOpen("Select");
            string strSql = string.Format("select * from tf_f_cust_acct where iccard_no='{0}' and cust_password='{1}'",txtCardno.Text.Trim(),oldPwd);
            DataTable dt = context.ExecuteReader(strSql);
            context.DBCommit();
            if (dt.Rows.Count <= 0)
            {
                context.AddError("初始密码不正确！");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
                linkSend.Enabled = true;
                return;
            }
        }

        string sessionid = Session.SessionID;

        //未选中任何行直接返回。
        if (!IsSelRow())
        {
            context.AddError("未选中任行，没有需要修改密码的记录");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
            linkSend.Enabled = true;
            return;
        }

        context.SPOpen();
        DataTable data = CAHelper.callQuery(context, "QRYACCTOUNT", txtCardno.Text);
        Dictionary<String, DDOBase> listDDO = new Dictionary<String, DDOBase>();
        string cust_id="";
        if (data != null && data.Rows.Count > 0)
        {
            cust_id = data.Rows[0]["CUST_ID"].ToString(); //取出客户标识
        }
        listDDO = new Dictionary<String, DDOBase>();
        if (!CAHelper.GetCustInfo(context, txtCardno.Text, cust_id, out listDDO))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
            linkSend.Enabled = true;
            return;
        }
        DDOBase ddoTF_F_CUSTOut;
        listDDO.TryGetValue("TF_F_CUST", out ddoTF_F_CUSTOut);
        string paperType = ((TF_F_CUSTTDO)ddoTF_F_CUSTOut).PAPER_TYPE_CODE;

        context.DBCommit();

        CAHelper.FillAccIDList(context, gvAccount, sessionid);

        context.SPOpen();
        context.AddField("P_SESSIONID").Value = sessionid;
        context.AddField("P_CARDNO").Value = txtCardno.Text.Trim();
        context.AddField("P_OLDPASSWD").Value = oldPwd;
        context.AddField("P_NEWPASSWD").Value = DealString.encrypPass(txtNPwd.Text.Trim());
        context.AddField("P_PHONE").Value = lblCustphone.Text.Trim();
        context.AddField("P_CAPTCHA").Value = txtCaptcha.Text.Trim();
        context.AddField("P_ISOPENONLINE").Value = (chOnline.Checked==true) ? "1" : "0";
        context.AddField("P_ISVALIDATE").Value = (chMsg.Checked == true) ? "1" : "0";
        context.AddField("p_PAPERNO").Value = lblPaperno.Text.Trim();
        context.AddField("P_PAPERTYPE").Value = paperType;

        bool ok = context.ExecuteSP("SP_CA_CHANGEACCPASSCOMMIT");

        if (ok)
        {
            context.DBCommit();
            AddMessage("M001111001");

            if (chOnline.Checked == true)
            {
                string Msg = MsgHelper.GetScuMsg(txtCardno.Text.Trim(),"00");
                MsgHelper.SendMsg(lblCustphone.Text.Trim(),Msg);
            }

            foreach (Control con in this.Page.Controls)
            {
                ClearControl(con);
            }
            btnChangeUserPass.Enabled = false;
            this.cbxIsFirst.Checked = false;
            chOnline.Checked = false;
            linkSend.Enabled = false;
        }
        else {
            chOnline.Checked = false;
            cbxIsFirst.Checked = false;
        }

        //清空临时表
        CAHelper.clearTempCustInfoTable(context);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "AdjustScript", "resetbtn();", true);
    }

    /// <summary>
    /// 修改密码前的验证
    /// </summary>
    /// <returns></returns>
    private Boolean ChangePasswordValidation()
    {
        //对原密码进行非空、长度、数字检验
        String strOPwd = txtOPwd.Text.Trim();

        if (strOPwd == "")
            context.AddError("A004111001", txtOPwd);
        else
        {
            int len = Validation.strLen(strOPwd);

            if (Validation.strLen(strOPwd) != 6)
                context.AddError("A004111002", txtOPwd);
            else if (!Validation.isNum(strOPwd))
                context.AddError("A004111003", txtOPwd);
        }

        //对新密码进行非空、长度、英数检验
        String strNPwd = txtNPwd.Text.Trim();
        if (strNPwd == "")
        {
            context.AddError("A004111004", txtNPwd);
        }
        else if (strNPwd=="111111")
        {
            context.AddError("新密码不能设置成初始密码。");
        }
        else
        {
            int len = Validation.strLen(strNPwd);

            if (Validation.strLen(strNPwd) != 6)
                context.AddError("A004111005", txtNPwd);
            else if (!Validation.isNum(strNPwd))
                context.AddError("A004111006", txtNPwd);
        }

        //对新密码确认进行非空检验
        String strANPwd = txtANPwd.Text.Trim();

        if (strANPwd == "")
            context.AddError("A004111007", txtANPwd);

        //对原密码与新密码是否一样进行检验
        if (!context.hasError())
        {
            if (strOPwd == strNPwd)
                context.AddError("A004111008", txtANPwd);
        }

        //对新密码与新密码确认是否一样进行检验
        if (!context.hasError())
        {
            if (strNPwd != strANPwd)
                context.AddError("A004111009", txtANPwd);
        }

        //验证修改初始密码时候是否可以开通在线支付
        if (!IsSelZHB() && chOnline.Checked&&cbxIsFirst.Checked)
        {
            context.AddError("修改账户宝初始密码时候才能开通在线支付！");
        }
        if (context.hasError())
            return false;
        else
            return true;
    }

    protected void gvAccount_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvAccount','Select$" + e.Row.RowIndex + "')");
        }
    }

    protected void gvAccount_SelectedIndexChanged(object sender, EventArgs e)
    {
        hiddenACCTYPENO.Value = "";
        string acctitemtype = getDataKeys2("ACCT_ITEM_TYPE"); //账目类别 0 现金账户 1 积分账户
        string cardno = getDataKeys2("ICCARD_NO"); //获取卡号
        string acctid = getDataKeys2("ACCT_ID");   //账户ID
        hiddenACCTYPENO.Value = getDataKeys2("ACCT_TYPE_NO");
    }

    #endregion

    #region Function

    public String getDataKeys2(String keysname)
    {
        return gvAccount.DataKeys[gvAccount.SelectedIndex][keysname].ToString();
    }

    /// <summary>
    /// 判断gridview是否有选中的行 chenwentao 2014/7/8
    /// </summary>
    /// <returns></returns>
    private bool IsSelRow()
    {
        for (int i = 0; i < gvAccount.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gvAccount.Rows[i].Cells[0].FindControl("selAcc");
            if (cb.Checked)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 选择的是否有账户宝
    /// </summary>
    /// <returns></returns>
    private bool IsSelZHB()
    {
        for (int i = 0; i < gvAccount.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gvAccount.Rows[i].Cells[0].FindControl("selAcc");
            if (cb.Checked)
            {
                string type = gvAccount.DataKeys[i]["ACCT_TYPE_NO"].ToString();
                if ("0001".Contains(type))
                    return true;
            }
        }
        return false;
    }

    protected void txtOPwd_TextChanged(object sender, EventArgs e)
    {
        if (ViewState["change"].ToString() == "1")
        {
            txtOPwd.Attributes["value"] ="";
            txtNPwd.Attributes["value"] = "";
            txtANPwd.Attributes["value"] = "";
            ViewState["change"] = "0";
            return;
        }
        txtOPwd.Attributes["value"] = this.txtOPwd.Text;
        txtNPwd.Attributes["value"] = this.txtNPwd.Text;
        txtANPwd.Attributes["value"] = this.txtANPwd.Text;
    }

    #endregion
}
