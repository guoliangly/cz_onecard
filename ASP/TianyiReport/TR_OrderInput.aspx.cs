﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using Common;
using TM;
using TDO.ResourceManager;
using TDO.CardManager;
using PDO.UserCard;
using TDO.UserManager;
// 用户卡入库处理


public partial class ASP_TianyiReport_TR_OrderInput : Master.Master
{
    // 输入项校验

    private void SubmitValidate()
    {
        Validation valid = new Validation(context);

        if (selDept.SelectedValue == "")
        {
            context.AddError("领用部门不能为空", selDept);
        }
        
        //验证订单号
        if (txtOrderNo.Text.Trim().Length < 1)
        {
            context.AddError("A002P01020: 订单号不能为空", txtOrderNo);
        }
        else if (txtOrderNo.Text.Trim().Length < 10)
        {
            context.AddError("A002P01021: 订单号不能少于十位", txtOrderNo);
        }

        // 对起始卡号和结束卡号进行校验
        UserCardHelper.validateCardNoRangeForTianYi(context, txtFromCardNo, txtToCardNo, true, true);

        
        //起始卡号和结束卡号前8位必须相同
        if (txtFromCardNo.Text.Trim().Substring(0, 8) != txtToCardNo.Text.Trim().Substring(0, 8))
        {
            context.AddError("起始卡号和结束卡号前8位必须相同");
            return;
        }
        //对卡片单价进行非空、数字检验

        UserCardHelper.validatePrice(context, txtUnitPrice, "A002P01009: 卡片单价不能为空", "A002P01010: 卡片单价必须是10.2的格式");

        //对单卡功能使用费进行非空、数字检验
        UserCardHelper.validatePrice(context, txtCardUseFee, "A002P01018: 卡片功能使用费不能为空", "A002P01019: 卡片功能使用费必须是10.2的格式");

        //对应用版本进行非空、英数字检验

        UserCardHelper.validateAlpha(context, txtAppVersion, "A002P01011: 应用版本不能为空", "A002P01012: 应用版本必须为英文或者数字");

        // 对有效日期范围进行非空、格式校验

        UserCardHelper.validateDateRange(context, txtEffDate, txtExpDate);
    }

    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        
        txtFromCardNo.Attributes["OnBlur"]   = "javascript:return Change();";
        txtToCardNo.Attributes  ["OnBlur"]   = "javascript:return Change();";
        txtUnitPrice.Attributes ["OnChange"] = "javascript:return Change('price');";
        
        setReadOnly(txtCardSum, txtTotal, txtCardType, txtFaceType);

        //从COS类型编码表(TD_M_COSTYPE)中读取数据，放入下拉列表中

        UserCardHelper.selectCosType(context, selCosType, false);

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据，放入下拉列表中

        //UserCardHelper.selectCardType(context, selCardType, false);

        //从厂商编码表(TD_M_MANU)中读取数据，放入下拉列表中

        UserCardHelper.selectManu(context, selProducer, false);

        //从IC卡卡面编码表(TD_M_CARDSURFACE)中读取数据，放入下拉列表中

        //UserCardHelper.selectCardFace(context, selFaceType, false);

        //从IC卡芯片类型编码表(TD_M_CARDCHIPTYPE)中读取数据，放入下拉列表中

        UserCardHelper.selectChipType(context, selChipType, false);


        //初始化部门
        selDept.Items.Add(new ListItem("--请选择--", ""));
        selDept.Items.Add(new ListItem("9999:电信", "9999"));
        selDept.Items.Add(new ListItem("9998:移动", "9998"));
        selDept.Items.Add(new ListItem("9993:江南银行","9993"));

        //功能使用费
        //TMTableModule tmTMTableModule = new TMTableModule();
        //TD_M_TAGTDO ddoTD_M_TagIn = new TD_M_TAGTDO();
        //ddoTD_M_TagIn.TAGCODE = "TIANYI_CARD_PAY";
        //TD_M_TAGTDO ddoTD_M_TagOut = (TD_M_TAGTDO)tmTMTableModule.selByPK(context, ddoTD_M_TagIn, typeof(TD_M_TAGTDO), null);
        //if (ddoTD_M_TagOut != null)
        //{
        //    lblFunFee.Text = "功能使用费: " + Convert.ToInt32(ddoTD_M_TagOut.TAGVALUE) / 100.0 + "元";
        //    lblFunFee.Visible = false;
        //}
    }
    protected void txtToCardNo_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        UserCardHelper.validateCardNoRangeForTianYi(context, txtFromCardNo, txtToCardNo, true, true);
        if (context.hasError()) return;

        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEIn = new TD_M_CARDTYPETDO();
        ddoTD_M_CARDTYPEIn.CARDTYPECODE = txtToCardNo.Text.Substring(4, 2);
        TD_M_CARDTYPETDO ddoTD_M_CARDTYPEOut = (TD_M_CARDTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDTYPEIn, typeof(TD_M_CARDTYPETDO), null, "TD_M_CARDTYPE_CHUSER", null);
        if (ddoTD_M_CARDTYPEOut == null)
        {
            context.AddError("S002P01I02");
            return;
        }
        txtCardType.Text = ddoTD_M_CARDTYPEOut.CARDTYPENAME;

        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACEIn = new TD_M_CARDSURFACETDO();
        ddoTD_M_CARDSURFACEIn.CARDSURFACECODE = txtToCardNo.Text.Substring(4, 4);
        TD_M_CARDSURFACETDO ddoTD_M_CARDSURFACEOut = (TD_M_CARDSURFACETDO)tmTMTableModule.selByPK(context, ddoTD_M_CARDSURFACEIn, typeof(TD_M_CARDSURFACETDO), null, "TD_M_CARDSURFACE_CODE", null);
        if (ddoTD_M_CARDSURFACEOut == null)
        {
            context.AddError("S002P01I04");
            return;
        }
        txtFaceType.Text = ddoTD_M_CARDSURFACEOut.CARDSURFACENAME;

    }
    // 入库处理
    protected void btnStockIn_Click(object sender, EventArgs e)
    {

        // 输入校验
        SubmitValidate();
        if (context.hasError()) return;

        string assignedstaffno = "";
        if (selDept.SelectedValue == "9999")//电信
        {
            assignedstaffno = "990000";
            //判断卡号和部门是否对应
            if (!txtToCardNo.Text.Trim().StartsWith("91501102"))
            {
                context.AddError("卡号与部门不对应，请查验");
                return;
            }
        }
        else if (selDept.SelectedValue == "9998")//移动
        {
            assignedstaffno = "990002";
            if (!txtToCardNo.Text.Trim().StartsWith("91501101"))
            {
                context.AddError("卡号与部门不对应，请查验");
                return;
            }
        }
        else if (selDept.SelectedValue == "9993")//江南银行
        {
            assignedstaffno = "990003";
            if (!txtToCardNo.Text.Trim().StartsWith("91501103"))
            {
                context.AddError("卡号与部门不对应，请查验");
                return;
            }
        }


        // 调用入库存储过程
        context.SPOpen();
        context.AddField("P_ORDERNO").Value = txtOrderNo.Text.Trim();
        context.AddField("P_STARTCARDNO").Value = txtFromCardNo.Text.Trim();
        context.AddField("P_ENDCARDNO").Value = txtToCardNo.Text.Trim();
        context.AddField("P_ORDERCOUNT", "Int32").Value = Convert.ToInt32(txtCardSum.Text.Trim());
        context.AddField("P_CARDPRICE", "Int32").Value = Convert.ToInt32(txtUnitPrice.Text.Trim()) * 100;
        context.AddField("P_CARDUSEFEE", "Int32").Value = Convert.ToInt32(txtCardUseFee.Text.Trim()) * 100;
        context.AddField("P_CARDSURFACECODE").Value = txtToCardNo.Text.Trim().Substring(4, 4);
        context.AddField("P_CARDTYPECODE").Value = txtToCardNo.Text.Trim().Substring(4, 2);
        context.AddField("P_CARDCHIPTYPECODE").Value = selChipType.SelectedValue;
         context.AddField("P_COSTYPECODE").Value = selCosType.SelectedValue;
        context.AddField("P_MANUTYPECODE").Value =selProducer.SelectedValue;
        context.AddField("P_APPVERNO").Value = txtAppVersion.Text.Trim();
        context.AddField("P_VALIDBEGINDATE").Value = txtEffDate.Text.Trim();
        context.AddField("P_VALIDENDDATE").Value = txtExpDate.Text.Trim();
        context.AddField("p_assignedstaffno").Value = assignedstaffno;
        context.AddField("p_assigneddepartno").Value = selDept.SelectedValue;
        bool ok = context.ExecuteSP("SP_TR_ORDERINPUTSUBMIT");
        if (ok)
        {
            AddMessage("D002P01001: 提交审批成功");
        }
    }
}
