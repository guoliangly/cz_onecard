﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TM;
using TDO.UserManager;
using TDO.Financial;

public partial class ASP_TianyiReport_TR_ManageReportDetail : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化业务类型
            TD_TRADETYPE_SHOWTDO tdoTD_TRADETYPE_SHOWIn = new TD_TRADETYPE_SHOWTDO();
            TD_TRADETYPE_SHOWTDO[] tdoTD_TRADETYPE_SHOWOutArr = (TD_TRADETYPE_SHOWTDO[])tm.selByPKArr(context, tdoTD_TRADETYPE_SHOWIn, typeof(TD_TRADETYPE_SHOWTDO), null, "TRADETYPE_SHOW_TIANYI", null);
            selTradeType.Items.Add(new ListItem("0000:全部业务", "0000"));
            foreach (DDOBase ddoDDOBase in tdoTD_TRADETYPE_SHOWOutArr)
            {
                selTradeType.Items.Add(new ListItem(ddoDDOBase.GetString("SHOWNO") + ":" + ddoDDOBase.GetString("SHOWNAME"), ddoDDOBase.GetString("SHOWNO")));
            }

            //初始化日期
            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");

            //初始化部门
            selDept.Items.Add(new ListItem("--请选择--", ""));
            selDept.Items.Add(new ListItem("9999:电信", "9999"));
            selDept.Items.Add(new ListItem("9998:移动", "9998"));
            selDept.Items.Add(new ListItem("9993:江南银行", "9993"));
        }
    }

    

   
  

    private double serviceCharge = 0;
    private double cardDeposit = 0;
    private double cardCharge = 0;
    private double handingFee = 0;
    private double functionCharge = 0;
    private double otherCharge = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            serviceCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[3]));
            //cardDeposit += Convert.ToDouble(GetTableCellValue(e.Row.Cells[5]));
            cardCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
            //handingFee += Convert.ToDouble(GetTableCellValue(e.Row.Cells[7]));
            //functionCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[8]));
            otherCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[5]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[3].Text = serviceCharge.ToString("n");
            //e.Row.Cells[5].Text = cardDeposit.ToString("n");
            e.Row.Cells[4].Text = cardCharge.ToString("n");
            //e.Row.Cells[7].Text = handingFee.ToString("n");
            //e.Row.Cells[8].Text = functionCharge.ToString("n");
            e.Row.Cells[5].Text = otherCharge.ToString("n");
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

        //if (selStaff.SelectedValue == "")
        //    context.AddError("请选择员工");

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        //SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        //pdo.funcCode = "TIANYI_MANAGE_REPORT_DETAIL";
        //pdo.var1 = txtFromDate.Text;
        //pdo.var2 = txtToDate.Text;
        //pdo.var5 = selDept.SelectedValue;
        //pdo.var6 = selTradeType.SelectedValue;
        //pdo.var10 = selDept.SelectedValue;
        //StoreProScene storePro = new StoreProScene();
        //DataTable data = storePro.Execute(context, pdo);

        context.SPOpen();
        context.AddField("p_funcCode").Value = "TIANYI_MANAGE_REPORT_DETAIL";
        context.AddField("p_var1").Value = txtFromDate.Text;
        context.AddField("p_var2").Value = txtToDate.Text;
        context.AddField("p_var3").Value = "";
        context.AddField("p_var4").Value = "";
        context.AddField("p_var5").Value = selDept.SelectedValue;
        context.AddField("p_var6").Value = selTradeType.SelectedValue;
        context.AddField("p_var7").Value = "";
        context.AddField("p_var8").Value = "";
        context.AddField("p_var9").Value = "";
        context.AddField("p_var10").Value = selDept.SelectedValue;
        context.AddField("p_cursor", "Cursor", "Output");
        DataTable data = context.ExecuteReader("SP_TR_Query");

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        serviceCharge = 0;
        cardDeposit = 0;
        cardCharge = 0;
        handingFee = 0;
        functionCharge = 0;
        otherCharge = 0;

        UserCardHelper.resetData(gvResult, data);
        //MergeRows(gvResult, 3);
        
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    public void MergeRows(GridView gv, int columnIndex)
    {
        TableCell preCell = null;

        if (gv.Rows.Count > 0)
            preCell = gv.Rows[0].Cells[columnIndex];

        if (preCell != null)
        {
            for (int r = 1; r < gv.Rows.Count; r++)
            {
                TableCell cell = gv.Rows[r].Cells[columnIndex];
                if (preCell.Text == cell.Text)
                {
                    cell.Visible = false;
                    preCell.RowSpan = (preCell.RowSpan == 0 ? 1 : preCell.RowSpan) + 1;
                }
                else
                {
                    preCell = cell;
                    preCell.BackColor = System.Drawing.Color.White;
                }
            }
        }
    }
}