﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TM;
using TDO.UserManager;
using TDO.Financial;
using TDO.CardManager;
using TDO.BalanceChannel;

public partial class ASP_TianyiReport_TR_CardUseFee : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化业务类型

            //TD_M_CARDTYPETDO tdoTD_M_CARDTYPETDOIn = new TD_M_CARDTYPETDO();
            //TD_M_CARDTYPETDO[] tdoTD_M_CARDTYPETDOOutArr = (TD_M_CARDTYPETDO[])tm.selByPKArr(context, tdoTD_M_CARDTYPETDOIn, typeof(TD_M_CARDTYPETDO), null, "ALLCARDTYPE", null);
            //selCardType.Items.Add(new ListItem("0000:全部类型", "0000"));
            //foreach (DDOBase ddoDDOBase in tdoTD_M_CARDTYPETDOOutArr)
            //{
            //    selCardType.Items.Add(new ListItem(ddoDDOBase.GetString("CARDTYPECODE") + ":" + ddoDDOBase.GetString("CARDTYPENAME"), ddoDDOBase.GetString("CARDTYPECODE")));
            //}

            //初始化日期

            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");

            //初始化部门
            selDept.Items.Add(new ListItem("9999:电信", "9999"));
            selDept.Items.Add(new ListItem("9998:移动", "9998"));
            selDept.Items.Add(new ListItem("9993:江南银行", "9993"));

            //初始化充值点
            //TF_SELSUP_BALUNITTDO tdoTF_SELSUP_BALUNITIn = new TF_SELSUP_BALUNITTDO();
            //TF_SELSUP_BALUNITTDO[] tdoTF_SELSUP_BALUNITOutArr = (TF_SELSUP_BALUNITTDO[])tm.selByPKArr(context, tdoTF_SELSUP_BALUNITIn, typeof(TF_SELSUP_BALUNITTDO), null);

            //ControlDeal.SelectBoxFill(selBalunit.Items, tdoTF_SELSUP_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);
            //selBalunit.Items[0].Value = "00000000";
        }
    }

    private int operCount = 0;
    private double totalCharges = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            operCount += Convert.ToInt32(GetTableCellValue(e.Row.Cells[4]));
            totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[7]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = operCount.ToString();
            e.Row.Cells[7].Text = totalCharges.ToString();
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        string orderno = txtOrderNo.Text.Trim();

        context.SPOpen();
        context.AddField("p_funcCode").Value = "TIANYI_CardUseFee";
        context.AddField("p_var1").Value = txtFromDate.Text;
        context.AddField("p_var2").Value = txtToDate.Text;
        context.AddField("p_var3").Value = orderno;
        context.AddField("p_var4").Value = selDept.SelectedValue;
        context.AddField("p_var5").Value = "";
        context.AddField("p_var6").Value = "";
        context.AddField("p_var7").Value = "";
        context.AddField("p_var8").Value = "";
        context.AddField("p_var9").Value = "";
        context.AddField("p_var10").Value = "";
        context.AddField("p_cursor", "Cursor", "Output");
        DataTable data = context.ExecuteReader("SP_TR_Query");

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        totalCharges = 0;
        UserCardHelper.resetData(gvResult, data);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

}
