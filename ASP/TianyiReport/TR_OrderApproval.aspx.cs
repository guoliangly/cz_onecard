﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.GroupCard;
using TM;

// 企福通财务审核处理

public partial class ASP_TianyiReport_TR_OrderApproval : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        //初始化审批状态
        InitStatus();

        gvResult.DataKeyNames = new string[] { "ORDERNO", "STARTCARDNO", "ENDCARDNO", "ORDERCOUNT","CARDPRICE", "CARDUSEFEE", "CARDSURFACECODE", 
            "cardsurfacename","CARDTYPECODE","cardtypename", "CARDCHIPTYPECODE","cardchiptypename", "COSTYPECODE","costype", 
            "MANUTYPECODE","manuname", "APPVERNO", "VALIDBEGINDATE","VALIDENDDATE","STAFFNAME","DEPARTNAME" };

        // 创建资料审核数据列表

        createGridViewData();

        //初始化部门
        selDept.Items.Add(new ListItem("--请选择--", ""));
        selDept.Items.Add(new ListItem("9999:电信", "9999"));
        selDept.Items.Add(new ListItem("9998:移动", "9998"));
        selDept.Items.Add(new ListItem("9993:江南银行","9993"));
    }


    private void InitStatus()
    {
        selApprovalStatus.Items.Add(new ListItem("0:未审批", "0"));
        selApprovalStatus.Items.Add(new ListItem("1:审批通过", "1"));
        selApprovalStatus.Items.Add(new ListItem("2:审批作废", "2"));
        selApprovalStatus.SelectedValue = "0";
    }

    // 创建资料审核数据列表

    private void createGridViewData()
    {
        // 从资料变更审核表查询需要审核的数据

        context.SPOpen();
        DataTable data = CallTianYiQuery(context, "TIANYI_OrderReview", selApprovalStatus.SelectedValue, txtOrderNo.Text.Trim(), selDept.SelectedValue);

        //if (data == null || data.Rows.Count < 1)
        //{
        //    context.AddError("没有未审批的记录");
        //}
        UserCardHelper.resetData(gvResult, data);

        //计算汇总信息
        Sum(data);

        bool enabled = (data.Rows.Count > 0);

        chkApprove.Enabled = enabled;
        chkReject.Enabled = enabled;

        chkApprove.Checked = false;
        chkReject.Checked = false;
        btnSubmit.Enabled = false;
    }

    // gridview换页事件
    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (chkApprove.Checked)
        {
            submitApproval("1", "D004P03001: 审核通过成功");   // 通过
        }
        else
        {
            submitApproval("2", "D004P03002: 审核作废成功");   // 作废
        }
    }

    // 调用信息更改审核存储过程

    private void submitApproval(string stateCode, string okMsgCode)
    {

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);

        //插入临时表
        context.DBOpen("Insert");
        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("chkCust");
            if (cb != null && cb.Checked)
            {
                ++count;
                String orderno = getDataKeys2("ORDERNO", gvr.RowIndex);
                String STARTCARDNO = getDataKeys2("STARTCARDNO", gvr.RowIndex);
                String ENDCARDNO = getDataKeys2("ENDCARDNO", gvr.RowIndex);
                String ORDERCOUNT = getDataKeys2("ORDERCOUNT", gvr.RowIndex);
                String CARDPRICE = getDataKeys2("CARDPRICE", gvr.RowIndex);
                String CARDUSEFEE = getDataKeys2("CARDUSEFEE", gvr.RowIndex);
                String CARDSURFACECODE = getDataKeys2("CARDSURFACECODE", gvr.RowIndex);
                String CARDTYPECODE = getDataKeys2("CARDTYPECODE", gvr.RowIndex);
                String CARDCHIPTYPECODE = getDataKeys2("CARDCHIPTYPECODE", gvr.RowIndex);
                String COSTYPECODE = getDataKeys2("COSTYPECODE", gvr.RowIndex);
                String MANUTYPECODE = getDataKeys2("MANUTYPECODE", gvr.RowIndex);
                String APPVERNO = getDataKeys2("APPVERNO", gvr.RowIndex);
                String VALIDBEGINDATE = getDataKeys2("VALIDBEGINDATE", gvr.RowIndex);
                String VALIDENDDATE = getDataKeys2("VALIDENDDATE", gvr.RowIndex);
                String RESULT = stateCode;
                context.ExecuteNonQuery("insert into TMP_COMMON_NEW (F0,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,F13,F14,SESSIONID) "
               + " values('" + orderno + "','" + STARTCARDNO + "','" + ENDCARDNO + "','" + ORDERCOUNT + "','" + CARDPRICE + "','"
               + CARDUSEFEE + "','" + CARDSURFACECODE + "','" + CARDTYPECODE + "','" + CARDCHIPTYPECODE + "','" + COSTYPECODE
               + "','" + MANUTYPECODE + "','" + APPVERNO + "','" + VALIDBEGINDATE + "','" + VALIDENDDATE + "','"
               + RESULT + "','"  + Session.SessionID + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A001002213");
            return;
        }

        // 调用信息更改审核存储过程
        context.SPOpen();
        context.AddField("P_SESSIONID").Value = Session.SessionID;
        
        bool ok = context.ExecuteSP("SP_TR_ORDERINPUTREVIEW");

        if (ok) AddMessage(okMsgCode);

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);
        createGridViewData();
    }

    // 通过 复选框 改变事件
    protected void chkApprove_CheckedChanged(object sender, EventArgs e)
    {
        if (chkApprove.Checked)
        {
            chkReject.Checked = false;
        }
        btnSubmit.Enabled = (chkApprove.Checked || chkReject.Checked);
    }

    // 作废 复选框 改变事件
    protected void chkReject_CheckedChanged(object sender, EventArgs e)
    {
        if (chkReject.Checked)
        {
            chkApprove.Checked = false;
        }
        btnSubmit.Enabled = (chkApprove.Checked || chkReject.Checked);
    }

    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    //注册行单击事件

        //    e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvResult','Select$" + e.Row.RowIndex + "')");
        //}
    }

    protected void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {
    }


    public String getDataKeys2(String keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    public String getDataKeys2(String keysname, int rowIndex)
    {
        return gvResult.DataKeys[rowIndex][keysname].ToString();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        context.SPOpen();
        DataTable data;
        if (txtOrderNo.Text.Trim().Length < 1)
        {
            data = CallTianYiQuery(context, "TIANYI_OrderReview", selApprovalStatus.SelectedValue, txtOrderNo.Text.Trim(),selDept.SelectedValue);
        }
        else
        {
           //验证订单号
            if (txtOrderNo.Text.Trim().Length < 1)
            {
                context.AddError("A002P01020: 订单号不能为空", txtOrderNo);
                return;
            }
            else if (txtOrderNo.Text.Trim().Length < 10)
            {
                context.AddError("A002P01021: 订单号不能少于十位", txtOrderNo);
                return;
            }

            data = CallTianYiQuery(context, "TIANYI_OrderReview", selApprovalStatus.SelectedValue, txtOrderNo.Text.Trim());
        }
        Sum(data);
        if (data == null || data.Rows.Count < 1)
        {
            if (selApprovalStatus.SelectedValue == "0")
            {
                context.AddError("没有未审批的记录");
            }
            if (selApprovalStatus.SelectedValue == "1")
            {
                context.AddError("没有审批通过的记录");
            }
            else if (selApprovalStatus.SelectedValue == "2")
            {
                context.AddError("没有审批作废的记录");
            }
        }
        UserCardHelper.resetData(gvResult, data);
        if (selApprovalStatus.SelectedValue != "0")
        {
            chkApprove.Enabled = false;
            chkReject.Enabled = false;
            btnSubmit.Enabled = false;
        }
        else
        {
            chkApprove.Enabled = true;
            chkReject.Enabled = true;
            chkApprove.Checked = false;
            chkReject.Checked = false;
            btnSubmit.Enabled = false;
        }
    }

    /// <summary>
    /// 查询订单记录
    /// </summary>
    /// <param name="context"></param>
    /// <param name="funcCode"></param>
    /// <param name="vars"></param>
    /// <returns></returns>
    private DataTable CallTianYiQuery(Master.CmnContext context, string funcCode, params string[] vars)
    {
        context.AddField("p_funcCode").Value = funcCode;
        if (vars.Length >= 1)
            context.AddField("p_var1").Value = vars[0];
        else
            context.AddField("p_var1").Value = "";
        if (vars.Length >= 2)
            context.AddField("p_var2").Value = vars[1];
        else
            context.AddField("p_var2").Value = "";
        if (vars.Length >= 3)
            context.AddField("p_var3").Value = vars[2];
        else
            context.AddField("p_var3").Value = "";
        if (vars.Length >= 4)
            context.AddField("p_var4").Value = vars[3];
        else
            context.AddField("p_var4").Value = "";
        if (vars.Length >= 5)
            context.AddField("p_var5").Value = vars[4];
        else
            context.AddField("p_var5").Value = "";
        if (vars.Length >= 6)
            context.AddField("p_var6").Value = vars[5];
        else
            context.AddField("p_var6").Value = "";
        if (vars.Length >= 7)
            context.AddField("p_var7").Value = vars[6];
        else
            context.AddField("p_var7").Value = "";
        if (vars.Length >= 8)
            context.AddField("p_var8").Value = vars[7];
        else
            context.AddField("p_var8").Value = "";
        if (vars.Length >= 9)
            context.AddField("p_var9").Value = vars[8];
        else
            context.AddField("p_var9").Value = "";
        if (vars.Length >= 10)
            context.AddField("p_var10").Value = vars[9];
        else
            context.AddField("p_var10").Value = "";
        context.AddField("p_cursor", "Cursor", "Output");
        DataTable data = context.ExecuteReader("SP_TR_Query");
        return data;
    }

    //计算汇总信息
    private void Sum(DataTable data)
    {
        int totalCardCount = 0;
        int totalCardFee = 0;
        int totalCardUseFee = 0;
        int total = 0;
        if (data != null && data.Rows.Count > 0)
        {
            foreach (DataRow row in data.Rows)
            {
                totalCardCount += Convert.ToInt32(row["ordercount"].ToString());
                totalCardFee += Convert.ToInt32(row["ordercount"].ToString()) * Convert.ToInt32(row["cardprice"].ToString());
                totalCardUseFee += Convert.ToInt32(row["ordercount"].ToString()) * Convert.ToInt32(row["cardusefee"].ToString());
                total = totalCardFee + totalCardUseFee;
            }
        }
        txtCardCount.Text = "  " + totalCardCount.ToString();
        txtCardPrice.Text = "  " + totalCardFee.ToString() + "元";
        txtTotalCardUseFee.Text = "  " + totalCardUseFee.ToString() + "元";
        txtTotal.Text = "  " + total.ToString() + "元";
        //客户名称
        if (selDept.SelectedValue == "")
        {
            lblCustomer.Text = "移动电信江南银行";
        }
        else if (selDept.SelectedValue == "9999")
        {
            lblCustomer.Text = "电信";
        }
        else if (selDept.SelectedValue == "9998")
        {
            lblCustomer.Text = "移动";
        }
        else if (selDept.SelectedValue == "9993")
        {
            lblCustomer.Text = "江南银行";
        }
    }
}
