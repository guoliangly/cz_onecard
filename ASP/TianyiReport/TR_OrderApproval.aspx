﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TR_OrderApproval.aspx.cs" Inherits="ASP_TianyiReport_TR_OrderApproval" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <title>市民卡信息变更审核</title>

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script language="javascript">
           function SelectAll(tempControl)
           {
               //将除头模板中的其它所有的CheckBox取反 

                var theBox=tempControl;
                 xState=theBox.checked;    

                elem=theBox.form.elements;
                for(i=0;i<elem.length;i++)
                if(elem[i].type=="checkbox" && elem[i].id!=theBox.id)
                 {
                      if(elem[i].checked!=xState)
                            elem[i].click();
                }
            }  
    </script>
</head>
<body>
    <form id="form1" runat="server">
<div class="tb">
天翼龙城通业务->订单审核(出库)

</div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
<div class="con">
                    <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td width="10%"><div align="right">审批状态:</div></td>
                                <td width="13%"><asp:DropDownList ID="selApprovalStatus" CssClass="input" runat="server"></asp:DropDownList></td>
                                <td width="9%"><div align="right">订单号:</div></td>
			                    <td width="20%"><asp:TextBox ID="txtOrderNo" CssClass="inputmid" runat="server" MaxLength="16"></asp:TextBox></td>
                                
                                <td style="width: 8%"><div align="right">领用部门:</div></td>
                                <td style="width: 12%">
                                    <asp:DropDownList ID="selDept" CssClass="input" runat="server"></asp:DropDownList>
                                </td>
                                    
                                <td style="width: 13%"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" /></td>
                                
                            </tr>
                        </table>
                    </div>
                </div>
<div class="con">
  <div class="jieguo">详细信息</div>
  <div class="kuang5">
  <div class="gdtb" style="height:310px">
  <table id="printReport" width ="95%">
                        <tr>
                            <td>
                               &nbsp;
                            </td>
                        </tr>
                    </table>
   <table width="800" border="0" cellpadding="0" cellspacing="0" class="tab1" >
        
         <asp:GridView ID="gvResult" runat="server"
        Width = "98%"
        CssClass="tab1"
         AllowPaging=true
         PageSize=10
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"
        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        OnRowCreated="gvResult_RowCreated" 
        OnSelectedIndexChanged="gvResult_SelectedIndexChanged"
        OnPageIndexChanging="gvResult_Page">
           <Columns>
             <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAllCust" runat="server" onclick="javascript:SelectAll(this);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkCust" runat="server"  />
                    </ItemTemplate>
                 </asp:TemplateField>
                <asp:BoundField HeaderText="订单号" DataField="ORDERNO"/>
                <asp:BoundField HeaderText="起始卡号" DataField="STARTCARDNO"/>
                <asp:BoundField HeaderText="终止卡号" DataField="ENDCARDNO"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="数量" DataField="ORDERCOUNT"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="卡费" DataField="CARDPRICE"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="功能使用费" DataField="CARDUSEFEE"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="领用部门" DataField="DEPARTNAME"  />
                <asp:BoundField HeaderText="领用员工" DataField="STAFFNAME"   />
            </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />          
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>订单号</td>
                    <td>起始卡号</td>
                    <td>终止卡号</td>
                    <td>数量</td>
                    <td>卡费</td>
                    <td>功能使用费</td>
                     <td>领用部门</td>
                      <td>领用员工</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
        </table>
  </div>
  </div>
<div class="kuang5">
   <table width="95%" border="0"cellpadding="0" cellspacing="0">
      <tr>
        <td width="16%">客户:<asp:Label ID="lblCustomer" runat="server"></asp:Label></td>
        <td align="right">开卡总数:</td>
        <td><asp:TextBox ID="txtCardCount" CssClass="labeltext" runat="server" ReadOnly="true" MaxLength="13"></asp:TextBox></td>
        <td align="right">卡费汇总:</td>
        <td><asp:TextBox ID="txtCardPrice" CssClass="labeltext" runat="server" ReadOnly="true" MaxLength="13"></asp:TextBox></td>
        <td align="right">功能费汇总:</td>
        <td><asp:TextBox ID="txtTotalCardUseFee" CssClass="labeltext" runat="server" ReadOnly="true" MaxLength="13"></asp:TextBox></td>
        <td align="right">总额:</td>
        <td><asp:TextBox ID="txtTotal" CssClass="labeltext" runat="server" ReadOnly="true" MaxLength="13"></asp:TextBox></td>
      </tr>
  </table>
</div>

<div class="kuang5">
<table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:CheckBox ID="chkApprove" AutoPostBack="true" Text="通过" runat="server" OnCheckedChanged="chkApprove_CheckedChanged" /></td>
    <td align="right"><asp:CheckBox ID="chkReject" AutoPostBack="true" Text="作废" runat="server" OnCheckedChanged="chkReject_CheckedChanged"  /></td>
    <td align="right"><asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click" OnClientClick="{if(confirm('确定要提交吗?')){return true;}return false;}"/></td>
  </tr>
</table>
</div>
</div>
            </ContentTemplate>         
        </asp:UpdatePanel>

    </form>
</body>
</html>

