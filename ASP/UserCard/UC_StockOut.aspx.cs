﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.UserCard;
using Common;
using TM;
using TDO.UserManager;
using System.Collections.Generic;
using Master;

// 用户卡出库处理
public partial class ASP_UserCard_UC_StockOut : Master.Master
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        //txtCardSum.Attributes.Add("onkeyup", "javascript:return txtCountControlChange();");
        //txtCardSum.Attributes.Add("onblur", "javascript:return txtCountControlChange();");
        //txtFromCardNo.Attributes["OnBlur"] = "javascript:return Change();";
       // txtToCardNo.Attributes["OnBlur"] = "javascript:return Change();";
        //setReadOnly(txtCardSum);

        // 选取拥有领用权限的员工，生成列表
        //UserCardHelper.selectAssignableStaffs(context, selAssignedStaff, false); 

        // 服务周期
        selServiceCycle.Items.Add(new ListItem("00:年度", "00"));
        selServiceCycle.Items.Add(new ListItem("01:季度", "01"));
        selServiceCycle.Items.Add(new ListItem("02:月份", "02"));
        selServiceCycle.Items.Add(new ListItem("03:天数", "03"));
        selServiceCycle.Items[2].Selected = true;

        // 退值（模式）
        selRetValMode.Items.Add(new ListItem("0:不退值", "0"));
        //selRetValMode.Items.Add(new ListItem("有条件退值", "1"));
        selRetValMode.Items.Add(new ListItem("2:无条件退值", "2"));
        selRetValMode.Items[1].Selected = true;


        //领用部门
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTTDOIn = new TD_M_INSIDEDEPARTTDO();
        TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTTDOOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTTDOIn, typeof(TD_M_INSIDEDEPARTTDO), null, null, null);
        ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTTDOOutArr, "DEPARTNAME", "DEPARTNO", true);
    }

    // 输入校验处理
    private void SubmitValidate()
    {
        TextBox tbtemp = new TextBox();
        tbtemp.Text = txtToCardNo.Value;
        // 对起始卡号和结束卡号进行校验
        UserCardHelper.validateCardNoRange(context, txtFromCardNo, tbtemp);

        //对卡服务费进行非空、数字检验

        UserCardHelper.validatePrice(context, txtServiceFee, "A002P02009: 卡服务费不能为空", "A002P02010: 卡服务费必须是10.2的格式");
    }

    // 提交处理
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitValidate();
        if (context.hasError()) return;

        // 调用出库存储过程
        SP_UC_StockOutPDO pdo = new SP_UC_StockOutPDO();
        pdo.fromCardNo = txtFromCardNo.Text;                  // 起始卡号
        pdo.toCardNo = txtToCardNo.Value;                      // 结束卡号
        pdo.assignedStaff = selAssignedStaff.SelectedValue;   // 领用员工
        pdo.serviceCycle = selServiceCycle.SelectedValue;     // 服务周期
        pdo.serviceFee = Validation.getPrice(txtServiceFee);  // 每期服务费
        pdo.retValMode = selRetValMode.SelectedValue;         // 退值模式

        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok) AddMessage("D002P02000: 出库成功");
        btnQuery_Click(sender, e);
    }

    /// <summary>
    /// 初始化领用人
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void selDept_Changed(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        tdoTD_M_INSIDESTAFFIn.DEPARTNO = selDept.SelectedValue;
        TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFFROLE1010", null);
        ControlDeal.SelectBoxFill(selAssignedStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        if (selAssignedStaff.Items.Count > 1)
        {
            selAssignedStaff.Items[1].Selected = true;
        }
    }

    /// <summary>
    /// 查询按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (!InputValidate())
            return;
        SP_UC_QueryPDO pdo = new SP_UC_QueryPDO();
        pdo.funcCode = "STOCKOUTQUERY";
        pdo.var1 = txtFromCardNoQ.Text.Trim();
        pdo.var2 = txtToCardNoQ.Text.Trim();
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        gvResult.DataSource = data;
        gvResult.DataBind();
        gvResult.SelectedIndex = -1;
    }

    /// <summary>
    /// 分页事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void gvResult_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        //分页事件
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
    }

    /// <summary>
    /// 查询前验证查询条件

    /// </summary>
    /// <returns></returns>
    private bool InputValidate()
    {
        if (this.txtFromCardNoQ.Text.Length > 0 && (Validation.strLen(this.txtFromCardNoQ.Text) != 16&& Validation.strLen(this.txtFromCardNoQ.Text) != 8))
        {
            context.AddError("A200001991:发票代码长度必须是8或16位", txtFromCardNoQ);
            return false;
        }
        if (this.txtToCardNoQ.Text.Length > 0 && (Validation.strLen(this.txtToCardNoQ.Text) != 16 && Validation.strLen(this.txtToCardNoQ.Text) != 8))
        {
            context.AddError("A200001991:发票代码长度必须是8或16位", txtToCardNoQ);
            return false;
        }
        return true;
      
    }
}
