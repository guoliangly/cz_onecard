﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TM;
using PDO.UserCard;
using Master;

public partial class ASP_UserCard_UC_CardStat : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            StateSubmit();


            //从资源状态编码表(TD_M_RESOURCESTATE)中读取数据，放入下拉列表中

            UserCardHelper.selectResState(context, selCardStat, true);


            //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据，放入下拉列表中

            UserCardHelper.selectCardType(context, selCardType, true);


            //从IC卡卡面编码表(TD_M_CARDSURFACE)中读取数据，放入下拉列表中

            UserCardHelper.selectCardFace(context, selFaceType, true);

            //从IC卡芯片类型编码表(TD_M_CARDCHIPTYPE)中读取数据，放入下拉列表中

            UserCardHelper.selectChipType(context, selChipType, true);

            gvState.Visible = false;
        }
    }

    private void StateSubmit()
    {
        SP_UC_QueryPDO pdo = new SP_UC_QueryPDO();
        if (selLatitude.SelectedValue == "STATE")
        {
            pdo.funcCode = "CardStatBySTATE";
        }
        else
        {
            pdo.funcCode = "CardStat";
        }
        
        pdo.var1 = selCardType.SelectedValue;
        pdo.var2 = selFaceType.SelectedValue;
        pdo.var3 = selChipType.SelectedValue;
        pdo.var4 = selCardStat.SelectedValue;
        pdo.var5 = txtToDate.Text;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        if (selLatitude.SelectedValue == "STATE")
        {
            gvState.Visible = true;
            gvResult.Visible = false;
            gvState.DataSource = data;
            gvState.DataBind();
        }
        else
        {
            gvState.Visible = false;
            gvResult.Visible = true;
            gvResult.DataSource = data;
            gvResult.DataBind();
        }
    }

    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 2);
    }

    private int total = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            total += Convert.ToInt32(e.Row.Cells[4].Text);
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].ColumnSpan = 4;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = "" + total;
        }
    }

    protected void gvResult_DataBound(object sender, EventArgs e)
    {
        GridViewMergeHelper.DataBoundWithEmptyRows(gvResult);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportGridView(gvResult);
    }


    protected void btnQuery_Click(object sender, EventArgs e)
    {
        StateSubmit();
    }



    protected void gvState_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvState, 0, 2);
    }

    private int totalState = 0;

    protected void gvState_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            totalState += Convert.ToInt32(e.Row.Cells[4].Text);
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].ColumnSpan = 4;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = "" + totalState;
        }
    }

    protected void gvState_DataBound(object sender, EventArgs e)
    {
        GridViewMergeHelper.DataBoundWithEmptyRows(gvState);
    }
}
