﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UC_CardUseStat.aspx.cs" Inherits="ASP_UserCard_UC_CardUseStat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>用户卡使用统计</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script language="javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <div class="tb">
        用户卡管理->卡使用统计

        </div>
    <asp:BulletedList ID="bulMsgShow" runat="server">
    </asp:BulletedList>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>


        <div class="con">
        <div class="base">
                        查询条件</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                               
                                <tr>
                                    <td>
                                        <div align="right">
                                            统计纬度:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selLatitude" CssClass="inputmid" runat="server">
                                         <asp:ListItem Value="TYPE">按卡类型</asp:ListItem>
                                        <asp:ListItem Value="STATE">按卡状态</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>
                                        <div align="right">
                                            卡片类型:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selCardType" CssClass="inputmid" runat="server">
                                        </asp:DropDownList></td>
                                    <td>
                                        <div align="right">
                                            卡面类型:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selFaceType" CssClass="inputmid" runat="server">
                                        </asp:DropDownList></td>
                                
                                </tr>
                            
                            <tr>
                                <td>
                                        <div align="right">
                                            芯片类型:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selChipType" CssClass="inputmid" runat="server">
                                        </asp:DropDownList></td>
                                 <td>
                                        <div align="right">
                                            卡片状态:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selCardStat" CssClass="inputmid" runat="server">
                                        </asp:DropDownList></td>
                                        <td>
                                        <div align="right">
                                            截止日期:</div>
                                    </td>
                                    <td>
                                       <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" /></td>
                                <td align="right">
                                    <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>






          <div class="jieguo">统计结果
          </div>
  <div id="printarea" class="kuang5">
         <asp:GridView ID="gvResult" runat="server"
        Width = "100%"
        CssClass="tab2"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"

        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        OnDataBound="gvResult_DataBound"
        OnPreRender="gvResult_PreRender"
        ShowFooter="true" OnRowDataBound="gvResult_RowDataBound"
        >
           <Columns>
                <asp:BoundField HeaderText="卡类型" DataField="CARDTYPENAME"/>
                <asp:BoundField HeaderText="卡面" DataField="CARDSURFACENAME"/>
                <asp:BoundField HeaderText="芯片类型" DataField="CARDCHIPTYPENAME"/>
                <asp:BoundField HeaderText="卡状态" DataField="CardState"/>
                <asp:BoundField HeaderText="卡数量" DataField="AMOUNT"/>
           </Columns>           
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>卡类型</td>
                    <td>卡面</td>
                    <td>芯片类型</td>
                    <td>卡状态</td>
                    <td>卡数量</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>




        <asp:GridView ID="gvState" runat="server"
        Width = "100%"
        CssClass="tab2"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"

        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        OnDataBound="gvState_DataBound"
        OnPreRender="gvState_PreRender"
        ShowFooter="true" OnRowDataBound="gvState_RowDataBound"
        >
           <Columns>
                <asp:BoundField HeaderText="卡使用状态" DataField="CARDSTATE"/>
                <asp:BoundField HeaderText="卡类型" DataField="CARDTYPENAME"/>
                <asp:BoundField HeaderText="卡面" DataField="CARDSURFACENAME"/>
                <asp:BoundField HeaderText="芯片类型" DataField="CARDCHIPTYPENAME"/>
                <asp:BoundField HeaderText="卡数量" DataField="AMOUNT"/>
           </Columns>           
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>卡状态</td>
                    <td>卡类型</td>
                    <td>卡面</td>
                    <td>芯片类型</td>
                    
                    <td>卡数量</td>
                    <td>总计</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
  </div>       
  </div>
  </ContentTemplate>
</asp:UpdatePanel>
      <div class="btns">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>   
     <td align="right">
    <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出" OnClick="btnExport_Click"/>
    <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="onprint();"/>
    </td>
    </tr>
    </table>
    </div>


    </form>
	<script type="text/javascript">
	window.onbeforeprint = beforePrint;
	window.onafterprint = afterPrint;
	var bdhtml = null;
	var borderstyle = null;
	function beforePrint(){
		bdhtml= window.document.body.innerHTML; 
		window.document.body.innerHTML = $get('printarea').innerHTML;

        borderstyle = window.document.body.style.border;
		window.document.body.style.border = '';
	}
	function afterPrint(){
		window.document.body.innerHTML  = bdhtml;
		window.document.body.style.border = borderstyle;
	}
	function onprint(){
	    parent.content.focus();
	    parent.content.print();
	}
	</script>
    
</body>
</html>
