﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using Common;
using TM;
using TDO.ResourceManager;
using TDO.CardManager;
using System.Text;
using PDO.UserCard;
using Onecard.Control;

// 用户卡查询处理
public partial class ASP_UserCard_UC_CardQuery : Master.Master
{


    // 提交前的输入校验
    private void SubmitValidate()
    {
        if (txtCardPrice.Text.Trim() != "")
        {
            UserCardHelper.validatePrice(context, txtCardPrice, "A002P01009: 卡片单价不能为空", "A002P01010: 卡片单价必须是10.2的格式");
        }
        // 对起始卡号和结束卡号进行校验
        UserCardHelper.validateCardNoRange(context, txtFromCardNo, txtToCardNo, false, true);
    }

    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        //从资源状态编码表(TD_M_RESOURCESTATE)中读取数据，放入下拉列表中
        UserCardHelper.selectResState(context, selCardStat, true);

        //从COS类型编码表(TD_M_COSTYPE)中读取数据，放入下拉列表中
        UserCardHelper.selectCosType(context, selCosType, true);

        //从IC卡类型编码表(TD_M_CARDTYPE)中读取数据，放入下拉列表中
        UserCardHelper.selectCardType(context, selCardType, true);

        //从厂商编码表(TD_M_MANU)中读取数据，放入下拉列表中
        UserCardHelper.selectManu(context, selProducer, true);

        //从IC卡卡面编码表(TD_M_CARDSURFACE)中读取数据，放入下拉列表中
        UserCardHelper.selectCardFace(context, selFaceType, true);

        //从IC卡芯片类型编码表(TD_M_CARDCHIPTYPE)中读取数据，放入下拉列表中
        UserCardHelper.selectChipType(context, selChipType, true);


        // 部门
        UserCardHelper.selectDepts(context, selDepts, true); 
        // 员工
        UserCardHelper.selectStaffs(context, selStaffs, selDepts, true);

        UserCardHelper.resetData(gvResult, null);

        NewPager1.Visible=false;
    }


    protected void btnShowCond_Click(object sender, EventArgs e)
    {
        panCond.Visible = btnShowCond.Text == "更多条件>>";
        btnShowCond.Text = panCond.Visible ? "更多条件<<" : "更多条件>>";
    }


     //查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        // 输入判断处理
        SubmitValidate();
        if (context.hasError()) return;
        this.gvResult.PageSize = PageSize;
        if (PageIndex < 0) PageIndex = 0;
        // 从用户卡资料表中根据条件进行查询
        SP_UC_QueryPDO pdo = new SP_UC_QueryPDO();
        pdo.funcCode = "CardInfoQuery";
        pdo.var1 = txtFromCardNo.Text.Trim();
        pdo.var2 = txtToCardNo.Text.Trim();
        pdo.var3 = selStaffs.SelectedValue;
        pdo.var4 = selCardStat.SelectedValue;
        pdo.var5 = selCosType.SelectedValue;
        pdo.var6 = selProducer.SelectedValue;
        pdo.var7 = selCardType.SelectedValue;
        pdo.var8 = selFaceType.SelectedValue;
        pdo.var9 = selChipType.SelectedValue;
        pdo.var10 = selDepts.SelectedValue;
        pdo.var11 = txtCardPrice.Text;
        pdo.PageIndex = PageIndex;
        pdo.PageSize = PageSize;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        _recordCount = pdo.RecordCount;
        _pageCount = pdo.PageCount;
        data.Columns.Add(new DataColumn("Num", typeof(int)));
        for (int i = 0; i < data.Rows.Count; i++)
        {
            data.Rows[i]["Num"] = PageIndex * PageSize + i + 1;
        }
        if (data == null || data.Rows.Count == 0)
        {
            NewPager1.Visible = false;
            AddMessage("N005030001: 查询结果为空");
        }
        else
        {
            page.PageSize = PageSize;
            page.PageIndex = PageIndex;
            page.RecordCount = RecordCount;
            page.PageCount = PageCount;
            NewPager1.Visible = true;
            page.SetPagingState();
        }

        UserCardHelper.resetData(gvResult, data);
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SP_UC_QueryPDO pdo = new SP_UC_QueryPDO();
        pdo.funcCode = "CardUseArea";
        StoreProScene storePro = new StoreProScene();
        DataTable data;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[2].Text == "06")
            {
                pdo.var1 = e.Row.Cells[0].Text;
                data = storePro.Execute(context, pdo);
                e.Row.Cells[2].Text = "" + data.Rows[0].ItemArray[0];
            }
            else
            {
                e.Row.Cells[2].Text = "";
            }
        }
    }
    protected void selDepts_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 员工
        UserCardHelper.selectStaffs(context, selStaffs, selDepts, true);
    }



    public int PageIndex
    {
        get { return this.gvResult.PageIndex; }
        set { this.gvResult.PageIndex = value; }
    }



    #region 分页
    protected static int _recordCount;
    protected static int _pageCount;
    private NewPager page = null;

    protected int PageSize
    {
        get { return 100; }
    }
    protected int RecordCount
    {
        get { return _recordCount; }
        set { _recordCount = value; }
    }
    private int PageCount
    {
        get { return _pageCount; }
        set { _pageCount = value; }
    }
    override protected void OnInit(EventArgs e)
    {
        base.OnInit(e);
        InitializeComponent();
    }

    private void InitializeComponent()
    {
        page = this.FindControl("NewPager1") as NewPager;
        page.FirstClick += new NewPageClickEventHandler(this.Pager_FirstClicked);
        page.PreClick += new NewPageClickEventHandler(this.Pager_PreClicked);
        page.NextClick += new NewPageClickEventHandler(this.Pager_NextClicked);
        page.LastClick += new NewPageClickEventHandler(this.Pager_LastClicked);
        page.GoPageClick += new NewPageClickEventHandler(this.Pager_GoPageClicked);
    }
    protected void Pager_PreClicked(object sender, System.EventArgs e)
    {
        this.PageIndex--;
        btnQuery_Click(sender, e);
    }
    protected void Pager_NextClicked(object sender, System.EventArgs e)
    {
        this.PageIndex++;
        btnQuery_Click(sender, e);
    }
    protected void Pager_FirstClicked(object sender, System.EventArgs e)
    {
        this.PageIndex = 0;
        btnQuery_Click(sender, e);
    }
    protected void Pager_LastClicked(object sender, System.EventArgs e)
    {
        this.PageIndex = this.PageCount - 1;
        btnQuery_Click(sender, e);
    }
    protected void Pager_GoPageClicked(object sender, System.EventArgs e)
    {
        this.PageIndex = page.PageIndex;
        btnQuery_Click(sender, e);
    }
    #endregion

}
