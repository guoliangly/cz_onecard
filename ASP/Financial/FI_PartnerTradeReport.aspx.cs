﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TDO.BalanceChannel;
using TM;
using TDO.BalanceParameter;
using System.Text;

public partial class ASP_Financial_FI_PartnerTradeReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期


            tbResult.Visible = false;
            txtFromDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");


            //从行业编码表(TD_M_CALLINGNO)中读取数据，放入查询输入行业名称下拉列表中

            TMTableModule tmTMTableModule = new TMTableModule();

            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211");

            ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);
            //去除公交行业
            selCalling.Items.Remove(selCalling.Items[1]);

            tbResult.InnerHtml = "";
        }
    }

    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的行业名称后,初始化单位名称

        selCorp.Items.Clear();
        InitCorp(selCalling, selCorp, "TD_M_CORPCALLUSAGE");

    }
    protected void InitCorp(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        // 从单位编码表(TD_M_CORP)中读取数据，放入增加,修改区域中单位信息下拉列表中

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = origindwls.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        SelectBoxFillWithCode(extdwls.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }

    protected void selCorp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的单位名称后,初始化部门名称,初始化结算单元名称



        //选定单位后,设置部门下拉列表数据
        if (selCorp.SelectedValue == "")
        {
            InitBalUnit("00", selCalling);
            return;
        }
        //初始化结算单元(属于选择单位)名称下拉列表值


        InitBalUnit("01", selCorp);


    }
    private void InitBalUnit(string balType, DropDownList dwls)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;

        //查询选定行业下的结算单元
        if (balType == "00")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCALLING", null);
        }

        //查询选定单位下的结算单元
        else if (balType == "01")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCORPONLY", null);
        }

        //查询选定部门下的结算单元
        else if (balType == "02")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = selCorp.SelectedValue;
            tdoTF_TRADE_BALUNITIn.DEPARTNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYDEPART", null);
        }

        ControlDeal.SelectBoxFill(selBalUint.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", false);
    }

    public void SelectBoxFillWithCode(ListItemCollection Items, DDOBase[] ddoDDOBaseArr, String textName, String valueName, Boolean emptFill)
    {
        Items.Clear();

        if (emptFill)
            Items.Add(new ListItem("---请选择---", ""));

        foreach (DDOBase ddoDDOBase in ddoDDOBaseArr)
        {
            Items.Add(new ListItem(ddoDDOBase.GetString(textName), ddoDDOBase.GetString(valueName)));
        }
    }

    private bool checkEndDate()
    {
        TP_DEALTIMETDO tdoTP_DEALTIMEIn = new TP_DEALTIMETDO();
        TP_DEALTIMETDO[] tdoTP_DEALTIMEOutArr = (TP_DEALTIMETDO[])tm.selByPKArr(context, tdoTP_DEALTIMEIn, typeof(TP_DEALTIMETDO), null, "DEALTIME", null);
        if (tdoTP_DEALTIMEOutArr.Length == 0)
        {
            context.AddError("没有找到有效的结算处理时间");
            return false;
        }
        else
        {
            DateTime dealDate = tdoTP_DEALTIMEOutArr[0].DEALDATE.Date;
            DateTime endDate = DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null);
            if (endDate.CompareTo(dealDate) >= 0)
            {
                context.AddError("结束日期过大，未结算");
                return false;
            }
        }
        return true;
    }

    private double totalCharges = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[6]));
        //}
        //else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        //{
        //    e.Row.Cells[0].Text = "总计";
        //    e.Row.Cells[6].Text = totalCharges.ToString("n");
        //}
    }
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }
    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }
    public string BeginDate
    {
        get
        {
            return DateTime.ParseExact(txtFromDate.Text.Trim(), "yyyyMMdd", null).ToString("yyyyMMdd");
        }
    }

    public string EndDate
    {
        get
        {
            return DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null).ToString("yyyyMMdd");
        }
    }
    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        tbResult.InnerHtml = "";
        tbResult.Visible = false;
        if (selCorp.SelectedValue == "")
        {
            context.AddError("单位名称不能为空", selCorp);
            return;
        }
        tdHeader.InnerText = selCorp.Items[selCorp.SelectedIndex].Text + "商户日报表";
        //UserCardHelper.resetData(gvResult, null);

        validate();
        checkEndDate();
        if (context.hasError()) return;
        tdHeader.InnerText = selCorp.Items[selCorp.SelectedIndex].Text + "商户日报表";
        string ntext = "<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;width: 98%;\"  align=\"center\" class=\"tab2\">";
        ntext = ntext + "<tr><td colspan='2' style='background-color: #FFFF00;border:1px solid black;'>财务划账金额</td>";
        ntext = ntext + Dispaly3() + "</tr>";
        ntext = ntext + Dispaly1() + "</table>";
        tbResult.InnerHtml = ntext;
        tbResult.Visible = true;
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (!tbResult.Visible)
        {
            context.AddError("未查询，请先查询", selCorp);
            return;
        }
        if (selCorp.SelectedValue == "")
        {
            context.AddError("单位名称不能为空", selCorp);
            return;
        }
        if (selBalUint.Items.Count == 0 || selBalUint.SelectedValue == "")
        {
            context.AddError("结算单元不能为空", selBalUint);
            return;
        }
        validate();
        checkEndDate();
        if (context.hasError()) return;

        string newpath = Server.MapPath("~/") + @"Templates\" + "商户日报" + ".xls";
        System.IO.FileInfo filetemp = new System.IO.FileInfo(newpath);
        if (filetemp.Exists)
        {
            filetemp.Delete();
        }
        ExcelEditHelper excelHelper = new ExcelEditHelper();
        string pathdir = Server.MapPath("~/") + "Templates\\";
        excelHelper.Open(pathdir + "ParnterDailyReport.xls");
        //插入报告期

        excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 2, 1, "报告期：" + DateTime.ParseExact(BeginDate, "yyyyMMdd", null).ToString("yyyy年MM月dd日") + "-" + DateTime.ParseExact(EndDate, "yyyyMMdd", null).ToString("yyyy年MM月dd日"));
        //插入日期
        int i = 0;//行

        int j = 0;//列

        DataTable dt0 = GetDate(BeginDate.Trim(), EndDate.Trim());

        foreach (DataColumn dcc in dt0.Columns)
        {
            excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 3, 3 + j, dt0.Rows[0][j].ToString());
            j++;
        }
        //插入商户划账金额
        //DataTable dt1 = GetCorpPosDaylyTradeMoney(BeginDate.Trim(),EndDate.Trim());

        //i = 0;//行

        //j = 0;//列

        //foreach (DataColumn dcc in dt1.Columns)
        //{
        //    excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 4, 3 + j, Convert.ToDecimal(dt1.Rows[0][j]).ToString("0.00"));
        //    j++;
        //}
        //插入pos每日金额
        DataTable dt2 = GetDepartPosDaylyTradeMoney(BeginDate.Trim(), EndDate.Trim());

        i = 0;
        j = 0;
        for (i = 0; i < dt2.Rows.Count; i++)
        {
            for (j = 0; j < dt2.Columns.Count; j++)
            {
                excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 6 + i, 1 + j, dt2.Rows[i][j].ToString());
            }
        }
        //插入合计
        //DataTable dt3 = GetCorpTotalMoney(BeginDate.Trim(), EndDate.Trim());
        //j = 0;
        //excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 6 + i, 1, "合计");
        //excelHelper.UniteCells(excelHelper.GetSheet("商户日报"), 6 + i, 1, 6 + i, 2);
        //for (j = 0; j < dt3.Columns.Count; j++)
        //{
        //    excelHelper.SetCellValue(excelHelper.GetSheet("商户日报"), 6 + i, 3 + j, dt3.Rows[0][j].ToString());
        //}
        excelHelper.SaveAs(newpath);
        excelHelper.Close();
        System.IO.FileInfo file = new System.IO.FileInfo(newpath);

        //打开保存对话框

        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = false;
        Response.Charset = "UTF-8";
        Response.ContentType = "application/ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(selCorp.Items[selCorp.SelectedIndex].Text + "商户日报" + EndDate.Trim() + ".xls", System.Text.Encoding.UTF8));
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
        Response.AppendHeader("Content-Length", file.Length.ToString());
        Response.Charset = "";
        this.EnableViewState = false;
        Response.WriteFile(newpath);
        //删除创建的Excel文件
        //FileInfo fileinf = new FileInfo(newpath);
        //fileinf.Delete();
        //关闭连接
        Response.Flush();
        Response.End();

    }

    protected void btnExportPayFile_Click(object sender, EventArgs e)
    {
        //ExportToFile(gvResult, "转账_" + hidNo.Value + ".txt");
    }
    protected void ExportToFile(GridView gv, string filename)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "GB2312";
        Response.ContentType = "application/vnd.text"; Response.ContentType = "text/plain";
        Response.AddHeader("Content-disposition", "attachment; filename=" + Server.UrlEncode(filename));
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");

        foreach (GridViewRow gvr in gv.Rows)
        {
            string temp = gvr.Cells[1].Text.Trim();
            Response.Write(temp.PadRight(20)); // 银行账号
            string money = "" + Convert.ToInt32(Convert.ToDecimal(gvr.Cells[3].Text) * 100);
            Response.Write(money.PadLeft(16)); // 金额
            temp = gvr.Cells[2].Text.Trim();
            Response.Write(temp + "".PadRight(30 - Validation.strLen(temp)));// 卡号
            Response.Write("\r\n");
        }

        Response.Flush();
        Response.End();
    }

    //得到账单的日期

    public DataTable GetPeriodDate(string begindate, string enddate)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        string strSql = "select * from ( ";
        strSql += "select  to_char(t.endtime-1,'yyyyMMdd') endtime from tf_trade_outcomefin t where t.balunitno='{2}' and  t.endtime-1>=TO_DATE('{0}','yyyyMMdd') and t.endtime < TO_DATE('{1}','yyyyMMdd')+1 ";
        strSql += "union all ";
        strSql += "select  to_char(t.endtime-1,'yyyyMMdd') endtime from tf_trade_outcomefin_online t where t.corpno in (select corpno from tf_trade_balunit where balunitno='{2}') and  t.endtime-1>=TO_DATE('{0}','yyyyMMdd') and t.endtime < TO_DATE('{1}','yyyyMMdd')+1 ";
        strSql += " ) order by endtime asc";
        strSql = string.Format(strSql, begindate, enddate, selBalUint.SelectedValue);
        return tm.selByPKDataTable(context, strSql, 0);
    }

    //得到账单的账期跨度

    public DataTable GetPeriodDateSpan(string begindate, string enddate)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        string strSql = "select * from ( ";
        strSql += "select to_char(t.begintime,'yyyyMMdd')||'-'||to_char(t.endtime-1,'yyyyMMdd') from tf_trade_outcomefin t where t.balunitno='{2}' and  t.endtime-1>=TO_DATE('{0}','yyyyMMdd') and t.endtime < TO_DATE('{1}','yyyyMMdd')+1";
        strSql += "union all";
        strSql += "select to_char(t.begintime,'yyyyMMdd')||'-'||to_char(t.endtime-1,'yyyyMMdd')  from tf_trade_outcomefin_online t where t.corpno in (select corpno from tf_trade_balunit where balunitno='{2}') and  t.endtime-1>=TO_DATE('{0}','yyyyMMdd') and t.endtime < TO_DATE('{1}','yyyyMMdd')+1";
        strSql += " ) order by 1 asc";
        strSql = string.Format(strSql, begindate, enddate, selBalUint.SelectedValue);
        return tm.selByPKDataTable(context, strSql, 0);
    }

    //日期排列
    public DataTable GetDate(string begindate, string enddate)
    {
        DateTime bd = DateTime.ParseExact(begindate, "yyyyMMdd", null);
        DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);

        DataTable dtData = GetResult(begindate, enddate);
        if (selQueryType.SelectedValue == "1")
        {
            if (dtData == null || dtData.Rows.Count == 0)
            {
                return new DataTable();
            }
            object obj = dtData.Compute("min(DEALTIME)", "");
            bd = DateTime.ParseExact(obj.ToString(), "yyyyMMdd", null);
        }

        DataTable dt = new DataTable();
        ArrayList al = new ArrayList();
        int i = 0;
        while (bd <= ed)
        {
            DataRow[] dr = dtData.Select("DEALTIME='" + bd.ToString("yyyyMMdd") + "'");
            bool isFd = dr.Length > 0 ? true : false;

            if (isFd)
            {
                dt.Columns.Add("date" + (i++).ToString());

                if (begindate.Substring(0, 6) == enddate.Substring(0, 6))//同一个月
                {
                    al.Add(bd.ToString("dd日"));
                }
                else if (begindate.Substring(0, 4) == enddate.Substring(0, 4))//同一年
                {
                    al.Add(bd.ToString("MM月dd日"));
                }
                else
                {
                    al.Add(bd.ToString("yyyy年MM月dd日"));
                }
            }
            bd = bd.AddDays(1);
        }
        //foreach (DataRow dr in dtDate.Rows)
        //{
        //    bd = DateTime.ParseExact(dr[0].ToString(), "yyyyMMdd", null);
        //    dt.Columns.Add("date" + (i++).ToString());
        //    if (begindate.Substring(0, 6) == enddate.Substring(0, 6))//同一个月
        //    {
        //        al.Add(bd.ToString("dd日"));
        //    }
        //    else if (begindate.Substring(0, 4) == enddate.Substring(0, 4))//同一年

        //    {
        //        al.Add(bd.ToString("MM月dd日"));
        //    }
        //    else
        //    {
        //        al.Add(bd.ToString("yyyy年MM月dd日"));
        //    }
        //}

        //DataTable dtDate = GetPeriodDateSpan(begindate, enddate);
        //foreach (DataRow dr in dtDate.Rows)
        //{
        //    dt.Columns.Add("date" + (i++).ToString());
        //    al.Add(dr[0].ToString());
        //}
        dt.Rows.Add(al.ToArray());
        return dt;
    }

    //取得查询结果
    private DataTable GetResult(string begindate, string enddate)
    {
        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        //GetDepartPosDaylyTradeMoney1每个POS每个结算日期的交易总金额;GetDepartPosDaylyTradeMoney2每个POS每个交易日期的交易总金额
        pdo.funcCode = selQueryType.SelectedValue == "0" ? "GetDepartPosDaylyTradeMoney1" : "GetDepartPosDaylyTradeMoney2";
        pdo.var1 = begindate;
        pdo.var2 = enddate;
        pdo.var3 = selCorp.SelectedValue;
        pdo.var4 = selBalUint.SelectedValue;
        StoreProScene storePro = new StoreProScene();
        DataTable dtData = storePro.Execute(context, pdo);
        return dtData;
    }

    //每个门店每个pos每天的交易总金额

    public DataTable GetDepartPosDaylyTradeMoney(string begindate, string enddate)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //先把某单位下所有门店的pos查找出来。

        string strSql = "select b.depart,a.posno from TF_R_PSAMPOSREC a ,td_m_depart b where a.departno=b.departno(+) and a.corpno='{0}' order by b.depart";
        strSql = string.Format(strSql, selCorp.SelectedValue);
        DataTable dtDataPOS = tm.selByPKDataTable(context, strSql, 0);
        //把某pos的交易总金额查找出来

        DataTable dtData = GetResult(begindate, enddate);
        if (dtData == null || dtData.Rows.Count == 0)
        {
            return new DataTable();
        }
        object obj = dtData.Compute("min(DEALTIME)", "");
        DataTable dt = new DataTable();
        DateTime bd = DateTime.ParseExact(obj.ToString(), "yyyyMMdd", null);
        DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
        dt.Columns.Add("departno");
        dt.Columns.Add("posno");
        DateTime bdtemp = bd;
        ArrayList al1 = new ArrayList();
        al1.Add("门店");
        al1.Add("POS");
        int ii = 0;
        while (bdtemp <= ed)
        {
            DataRow[] dr = dtData.Select("DEALTIME='" + bdtemp.ToString("yyyyMMdd") + "'");
            bool isFd = dr.Length > 0 ? true : false;
            if (isFd)
            {
                dt.Columns.Add("date" + (ii++).ToString());
                al1.Add("");
            }
            bdtemp = bdtemp.AddDays(1);
        }
        dt.Rows.Add(al1.ToArray());

        foreach (DataRow drPOS in dtDataPOS.Rows)
        {
            ArrayList al = new ArrayList();
            al.Add(drPOS["depart"].ToString());//门店
            al.Add(drPOS["posno"].ToString());//pos

            bdtemp = bd;
            while (bdtemp <= ed)
            {
                DataRow[] dr = dtData.Select("DEALTIME='" + bdtemp.ToString("yyyyMMdd") + "'");
                bool isFd = dr.Length > 0 ? true : false;
                if (isFd)
                {

                    DataRow[] drs = dtData.Select("posno='" + drPOS["posno"].ToString() + "' and  dealtime='" + bdtemp.ToString("yyyyMMdd") + "'");
                    if (drs.Length > 0)
                    {
                        al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
                    }
                    else
                    {
                        al.Add("0.00");
                    }
                }
                bdtemp = bdtemp.AddDays(1);
            }
            dt.Rows.Add(al.ToArray());
        }

        //所有门店合计

        ArrayList al2 = new ArrayList();
        al2.Add("合计");
        al2.Add("");
        for (int j = 2; j < dt.Columns.Count; j++)
        {
            decimal hejitotalmoney = 0;
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                hejitotalmoney += Convert.ToDecimal(dt.Rows[i][j].ToString());
            }
            al2.Add(hejitotalmoney.ToString("0.00"));
        }
        dt.Rows.Add(al2.ToArray());
        return dt;
    }
    ////所有门店合计总金额

    //public DataTable GetCorpTotalMoney(string begindate, string enddate)
    //{
    //    TMTableModule tmTMTableModule = new TMTableModule();
    //    SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
    //    pdo.funcCode = "GetCorpTotalMoney";
    //    pdo.var1 = begindate;
    //    pdo.var2 = enddate;
    //    pdo.var3 = selCorp.SelectedValue;
    //    pdo.var4 = selBalUint.SelectedValue;

    //    StoreProScene storePro = new StoreProScene();
    //    DataTable dtData = storePro.Execute(context, pdo);
    //    DataTable dt = new DataTable();
    //    DateTime bd = DateTime.ParseExact(begindate, "yyyyMMdd", null);
    //    DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
    //    DateTime bdtemp = bd;
    //    int i = 0;
    //    DataTable dtDate = GetPeriodDate(begindate, enddate);
    //    foreach (DataRow dr in dtDate.Rows)
    //    {

    //        dt.Columns.Add("date" + (i++).ToString());
    //    }
    //    ArrayList al = new ArrayList();
    //    bdtemp = bd;
    //    foreach (DataRow dr in dtDate.Rows)
    //    {

    //        DataRow[] drs = dtData.Select("dealtime='" + dr[0].ToString() + "'");
    //        if (drs.Length > 0)
    //        {
    //            al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
    //        }
    //        else
    //        {
    //            al.Add("0.00");
    //        }
    //    }
    //    dt.Rows.Add(al.ToArray());
    //    return dt;

    //}


    //单位每日转账金额
    public DataTable GetCorpPosDaylyTradeMoney(string begindate, string enddate)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "GetCorpPosDaylyTradeMoney";
        pdo.var1 = begindate;
        pdo.var2 = enddate;
        pdo.var3 = selCorp.SelectedValue;
        pdo.var4 = selBalUint.SelectedValue;
        StoreProScene storePro = new StoreProScene();
        DataTable dtData = storePro.Execute(context, pdo);
        DataTable dt = new DataTable();
        DateTime bd = DateTime.ParseExact(begindate, "yyyyMMdd", null);
        DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
        ArrayList al = new ArrayList();
        int i = 0;
        DataTable dtDate = GetPeriodDate(begindate, enddate);
        foreach (DataRow dr in dtDate.Rows)
        {
            dt.Columns.Add("date" + (i++).ToString());
            DataRow[] drs = dtData.Select("dealtime='" + dr[0].ToString() + "'");
            if (drs.Length > 0)
            {
                al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
            }
            else
            {
                al.Add("0.00");
            }
        }
        dt.Rows.Add(al.ToArray());
        return dt;
    }

    public string Dispaly1()
    {
        StringBuilder text = new StringBuilder();
        DataTable dt = GetDepartPosDaylyTradeMoney(BeginDate.Trim(), EndDate.Trim());
        foreach (DataRow dr in dt.Rows)
        {
            text.Append("<tr>");
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                text.Append("<td style='border:1px solid black;'>" + dr[i].ToString() + "</td>");
            }
            text.Append("</tr>");
        }
        return text.ToString();
    }

    public string Dispaly2()
    {
        StringBuilder text = new StringBuilder();
        DataTable dt = GetCorpPosDaylyTradeMoney(BeginDate.Trim(), EndDate.Trim());
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                text.Append("<td style='background-color: #FFFF00;border:1px solid black;'>" + dr[i].ToString() + "</td>");
            }
        }
        return text.ToString();
    }

    public string Dispaly3()
    {
        StringBuilder text = new StringBuilder();
        DataTable dt = GetDate(BeginDate.Trim(), EndDate.Trim());
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                text.Append("<td style='background-color: #FFFF00;border:1px solid black;'>" + dr[i].ToString() + "</td>");
            }
        }
        return text.ToString();
    }

    //显示合计
    //public string Dispaly4()
    //{
    //    string text = "";
    //    DataTable dt = GetCorpTotalMoney(BeginDate.Trim(), EndDate.Trim());
    //    foreach (DataRow dr in dt.Rows)
    //    {
    //        for (int i = 0; i < dt.Columns.Count; i++)
    //        {
    //            text = text + "<td style='border:1px solid black;'>" + dr[i].ToString() + "</td>";
    //        }
    //    }
    //    return text;
    //}
}
