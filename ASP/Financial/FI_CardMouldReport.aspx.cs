﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Master;
using Common;
using TM;
using TDO.UserManager;
using PDO.Financial;

public partial class ASP_Financial_FI_CardMouldReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            

            if (HasOperPower("201008")||context.s_DepartID=="0007")//全部网点主管或财务部
            {
                //初始化部门

                TMTableModule tmTMTableModule = new TMTableModule();
                TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
                TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
                ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
            }

             else if (HasOperPower("201007"))   //网点主管
            {
                selDept.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
                selDept.SelectedValue = context.s_DepartID;
                selDept.Enabled = false;
            }
        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {

        if (context.hasError()) return;

        DataTable data = SPHelper.callCMQuery(context, "CM_Report", selDept.SelectedValue, selCardMType.SelectedValue);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    private int _rkCount;   //换货入库
    private int _ckCount;   //出库
    private int _scCount;   //触手
    private int _shCount;   //损坏
    private int _zsCount;   //赠送
    private int _xjCount;   //小计
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)    // 数据行
        {
            _rkCount = _rkCount + Convert.ToInt32(e.Row.Cells[1].Text.Trim());
            _ckCount = _ckCount + Convert.ToInt32(e.Row.Cells[2].Text.Trim());
            _scCount = _scCount + Convert.ToInt32(e.Row.Cells[3].Text.Trim());
            _shCount = _shCount + Convert.ToInt32(e.Row.Cells[4].Text.Trim());
            _zsCount = _zsCount + Convert.ToInt32(e.Row.Cells[5].Text.Trim());
            _xjCount = _xjCount + Convert.ToInt32(e.Row.Cells[6].Text.Trim());
        }

        if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "小计";
            e.Row.Cells[1].Text = _rkCount.ToString();
            e.Row.Cells[2].Text = _ckCount.ToString();
            e.Row.Cells[3].Text = _scCount.ToString();
            e.Row.Cells[4].Text = _shCount.ToString();
            e.Row.Cells[5].Text = _zsCount.ToString();
            e.Row.Cells[6].Text = _xjCount.ToString();
        }
    }

    private bool HasOperPower(string powerCode)
    {
        //TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tm.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }
}
