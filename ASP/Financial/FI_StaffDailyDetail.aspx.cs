﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TM;
using TDO.UserManager;
using TDO.Financial;

public partial class ASP_Financial_FI_StaffDailyDetail : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化业务

            TD_TRADETYPE_SHOWTDO tdoTD_TRADETYPE_SHOWIn = new TD_TRADETYPE_SHOWTDO();
            TD_TRADETYPE_SHOWTDO[] tdoTD_TRADETYPE_SHOWOutArr = (TD_TRADETYPE_SHOWTDO[])tm.selByPKArr(context, tdoTD_TRADETYPE_SHOWIn, typeof(TD_TRADETYPE_SHOWTDO), null, "TRADETYPE_SHOW", null);
            selTradeType.Items.Add(new ListItem("0000:全部业务", "0000"));
            foreach (DDOBase ddoDDOBase in tdoTD_TRADETYPE_SHOWOutArr)
            {
                selTradeType.Items.Add(new ListItem(ddoDDOBase.GetString("SHOWNO") + ":" + ddoDDOBase.GetString("SHOWNAME"), ddoDDOBase.GetString("SHOWNO")));
            }
            //增加读卡器销售报表查询, add by youyue 2013-3-11
            selTradeType.Items.Add(new ListItem("0040:充付器", "0040"));
            //初始化日期

            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");

            if (HasOperPower("201008"))//全部网点主管
            {
                //初始化部门

                TMTableModule tmTMTableModule = new TMTableModule();
                TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
                TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
                ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
                selDept.SelectedValue = context.s_DepartID;
                InitStaffList(context.s_DepartID);
                selStaff.SelectedValue = context.s_UserID;
            }
            else if (HasOperPower("201007"))//网点主管
            {
                selDept.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
                selDept.SelectedValue = context.s_DepartID;
                selDept.Enabled = false;

                InitStaffList(context.s_DepartID);
                selStaff.SelectedValue = context.s_UserID;
            }
            else//网点营业员

            {
                selDept.Items.Add(new ListItem(context.s_DepartID + ":" + context.s_DepartName, context.s_DepartID));
                selDept.SelectedValue = context.s_DepartID;
                selDept.Enabled = false;

                selStaff.Items.Add(new ListItem(context.s_UserID + ":" + context.s_UserName, context.s_UserID));
                selStaff.SelectedValue = context.s_UserID;
                selStaff.Enabled = false;
            }

            selTradeType_Changed(sender, e);
        }
    }

    private bool HasOperPower(string powerCode)
    {
        //TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tm.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    private void InitStaffList(string deptNo)
    {
        if (deptNo == "")
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
            selStaff.SelectedValue = context.s_UserID;
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }
    
    protected void selDept_Changed(object sender, EventArgs e)
    {
        InitStaffList(selDept.SelectedValue);
    }

    //主业务类型选择
    protected void selTradeType_Changed(object sender, EventArgs e)
    {
        //清空子业务类型

        selReaTradeType.Items.Clear();
        selReaTradeType.Items.Add(new ListItem("---请选择---", ""));
        //充值

        if (selTradeType.SelectedValue == "0002")
        {
            selReaTradeType.Items.Add(new ListItem("现金", "1"));
            selReaTradeType.Items.Add(new ListItem("银联", "01"));
            selReaTradeType.Items.Add(new ListItem("翼支付", "02"));
        }
        else if (selTradeType.SelectedValue == "0040")
        {
            selReaTradeType.Items.Add(new ListItem("1A:出售", "1A"));
            selReaTradeType.Items.Add(new ListItem("1B:更换", "1B"));
            selReaTradeType.Items.Add(new ListItem("1C:退货", "1C"));
            selReaTradeType.Items.Add(new ListItem("R1:出售返销", "R1"));
            selReaTradeType.Items.Add(new ListItem("R2:更换返销", "R2"));
        }
        else if (selTradeType.SelectedValue == "0030")
        {
            selReaTradeType.Items.Add(new ListItem("龙人", "BKLR"));
        }
        else
        {
            string sql1 = "select b.tradetypecode,b.tradetype from td_m_tradetype b where b.tradetypecode in (select a.tradetypeno from TD_TRADETYPE_SHOW a where showno='{0}')";
            DataTable dt = tm.selByPKDataTable(context, string.Format(sql1,selTradeType.SelectedValue), 0);
            foreach (DataRow dr in dt.Rows)
            {
                selReaTradeType.Items.Add(new ListItem(dr["tradetypecode"].ToString() + ":" + dr["tradetype"].ToString(), dr["tradetypecode"].ToString()));
            }
        }

    }

    private double serviceCharge = 0;
    private double cardDeposit = 0;
    private double cardCharge = 0;
    private double handingFee = 0;
    private double functionCharge = 0;
    private double otherCharge = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            serviceCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
            cardDeposit += Convert.ToDouble(GetTableCellValue(e.Row.Cells[5]));
            cardCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[6]));
            handingFee += Convert.ToDouble(GetTableCellValue(e.Row.Cells[7]));
            functionCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[8]));
            otherCharge += Convert.ToDouble(GetTableCellValue(e.Row.Cells[9]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = serviceCharge.ToString("n");
            e.Row.Cells[5].Text = cardDeposit.ToString("n");
            e.Row.Cells[6].Text = cardCharge.ToString("n");
            e.Row.Cells[7].Text = handingFee.ToString("n");
            e.Row.Cells[8].Text = functionCharge.ToString("n");
            e.Row.Cells[9].Text = otherCharge.ToString("n");
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

        //if (selStaff.SelectedValue == "")
        //    context.AddError("请选择员工");

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TRADE_LOG_LIST";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = selStaff.SelectedValue;
        pdo.var4 = selReaTradeType.SelectedValue;
        pdo.var5 = selDept.SelectedValue;
        pdo.var6 = selTradeType.SelectedValue;
        pdo.var7 = selEvaluate.SelectedValue;
        pdo.var10 = selDept.SelectedValue;

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        //去除data中的"()" chenwentao 2014-07-28
        for (int i = 0; i < data.Rows.Count; i++)
        {
            string str = string.Empty;
            str = data.Rows[i]["交易类型"].ToString();
            data.Rows[i]["交易类型"] = str.Replace("()", "");
        }

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        serviceCharge = 0;
        cardDeposit = 0;
        cardCharge = 0;
        handingFee = 0;
        functionCharge = 0;
        otherCharge = 0;

        UserCardHelper.resetData(gvResult, data);
        //MergeRows(gvResult, 3);
        
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    public void MergeRows(GridView gv, int columnIndex)
    {
        TableCell preCell = null;

        if (gv.Rows.Count > 0)
            preCell = gv.Rows[0].Cells[columnIndex];

        if (preCell != null)
        {
            for (int r = 1; r < gv.Rows.Count; r++)
            {
                TableCell cell = gv.Rows[r].Cells[columnIndex];
                if (preCell.Text == cell.Text)
                {
                    cell.Visible = false;
                    preCell.RowSpan = (preCell.RowSpan == 0 ? 1 : preCell.RowSpan) + 1;
                }
                else
                {
                    preCell = cell;
                    preCell.BackColor = System.Drawing.Color.White;
                }
            }
        }
    }
}