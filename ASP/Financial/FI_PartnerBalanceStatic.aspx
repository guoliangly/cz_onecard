﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_PartnerBalanceStatic.aspx.cs" Inherits="ASP_Financial_FI_PartnerBalanceStatic" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>结算统计表</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
        <div class="tb">
		    财务报表->结算统计表

	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" AsyncPostBackTimeout="12000"  EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->  
	        <div class="con">

	           <div class="card">查询</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td><div align="right">结算开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">结算结束日期:</div></td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                        </td>
                        
                   </tr>

                   <tr>
                  <td align="right">行业名称:</td>
                  <td><asp:DropDownList ID="selCalling" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCalling_SelectedIndexChanged">
                     </asp:DropDownList>  
                  </td>
                  <td align="right">单位名称:</td>
                    <td><asp:DropDownList ID="selCorp" CssClass="inputmidder" runat="server">
                     </asp:DropDownList>    
		            </td>

                </tr>
                <tr>
                <td align="right">统计方式:</td>
                
                <td>
                <asp:DropDownList ID="selStaticType" runat="server" CssClass="input">
                <%--<asp:ListItem Value="1">行业消费按月</asp:ListItem>
                <asp:ListItem Value="2">行业消费按日</asp:ListItem>--%>
                <asp:ListItem Value="1">单位消费按月</asp:ListItem>
                <asp:ListItem Value="2">单位消费按日</asp:ListItem>
                </asp:DropDownList>
                </td>
                <td colspan="2" align="right">
                            <asp:Button ID="Button1" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                        
                </tr>
               </table>
               
             </div>
	
	        <asp:HiddenField ID="hidNo" runat="server" Value="" />
	
            <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                    <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="return printGridView('printarea');"/>
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:380px">
                    <table id="printReport" width ="80%">
                        <tr align="center">
                            <td  id="tdHeader" runat="server" style ="font-size:16px;font-weight:bold">结算统计表</td>
                        </tr>
                        <tr>
                            <td>
                            
                                <table width="400px" align="left">
                                    <tr align="right">
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                  
<asp:GridView ID="gvResult" runat="server"
                            Width = "95%"
                            CssClass="tab2"
                            HeaderStyle-CssClass="tabbt"
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            AutoGenerateColumns="true"
                            OnRowDataBound="gvResult_RowDataBound" 
                             OnLoad="gvResult_PreRender"
                            ShowFooter="false" >
                    </asp:GridView>
           


                    
                </div>
              </div>
            </div>
       
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
              </Triggers>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>