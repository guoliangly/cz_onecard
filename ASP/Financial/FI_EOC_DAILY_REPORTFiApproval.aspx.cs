﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using PDO.Financial;
using Master;
using Common;
using TDO.BalanceParameter;
using TDO.BalanceChannel;
using TM;
using System.Web;

// 商户转账日报
public partial class ASP_FI_EOC_DAILY_REPORTFiApproval : Master.ExportMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {


            TMTableModule tmTMTableModule = new TMTableModule();
            //初始化查询输入的行业名称下拉列表框


            //从行业编码表(TD_M_CALLINGNO)中读取数据，放入查询输入行业名称下拉列表中



            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211");

            ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

            //初始化日期

            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

            UserCardHelper.resetData(gvResult, null);

            return;
        }

    }


    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的行业名称后,初始化单位名称,初始化结算单元名称



        selCorp.Items.Clear();
        selDepart.Items.Clear();
        selBalUint.Items.Clear();

        InitCorp(selCalling, selCorp, "TD_M_CORPCALLUSAGE");

        //初始化结算单元(属于选择行业)名称下拉列表值


        InitBalUnit("00", selCalling);

    }
    protected void selCorp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的单位名称后,初始化部门名称,初始化结算单元名称



        //选定单位后,设置部门下拉列表数据
        if (selCorp.SelectedValue == "")
        {
            selDepart.Items.Clear();
            InitBalUnit("00", selCalling);
            return;
        }

        //初始化单位下的部门信息


        InitDepart(selCorp, selDepart, "TD_M_DEPARTUSAGE");

        //初始化结算单元(属于选择单位)名称下拉列表值


        InitBalUnit("01", selCorp);
        //默认选中单位的结算单元


        selBalUint.SelectedIndex = selBalUint.Items.Count > 1 ? 1 : 0;

    }
    protected void selDepart_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的部门名称后,初始化结算单元名称



        //选定单位后,设置部门下拉列表数据
        if (selDepart.SelectedValue == "")
        {
            InitBalUnit("01", selCorp);
            return;
        }

        //初始化结算单元(属于选择部门)名称下拉列表值


        InitBalUnit("02", selDepart);
        //默认选中单位的结算单元


        selBalUint.SelectedIndex = selBalUint.Items.Count > 1 ? 1 : 0;
    }


    protected void InitCorp(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        // 从单位编码表(TD_M_CORP)中读取数据，放入增加,修改区域中单位信息下拉列表中

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = origindwls.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }

    private void InitBalUnit(string balType, DropDownList dwls)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;

        //查询选定行业下的结算单元
        if (balType == "00")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCALLING", null);
        }

        //查询选定单位下的结算单元
        else if (balType == "01")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCORP", null);
        }

        //查询选定部门下的结算单元
        else if (balType == "02")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = selCorp.SelectedValue;
            tdoTF_TRADE_BALUNITIn.DEPARTNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYDEPART", null);
        }

        ControlDeal.SelectBoxFill(selBalUint.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);
    }

    private void InitDepart(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从部门编码表(TD_M_CDEPART)中读取数据，放入下拉列表中



        TD_M_DEPARTTDO tdoTD_M_DEPARTIn = new TD_M_DEPARTTDO();
        tdoTD_M_DEPARTIn.CORPNO = origindwls.SelectedValue;

        TD_M_DEPARTTDO[] tdoTD_M_DEPARTOutArr = (TD_M_DEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_DEPARTIn, typeof(TD_M_DEPARTTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_DEPARTOutArr, "DEPART", "DEPARTNO", true);
    }



    private double totalCharges = 0;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow ||
           e.Row.RowType == DataControlRowType.Header ||
           e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
        }
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = totalCharges.ToString("n");
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    private bool checkEndDate()
    {
        TP_DEALTIMETDO tdoTP_DEALTIMEIn = new TP_DEALTIMETDO();
        TP_DEALTIMETDO[] tdoTP_DEALTIMEOutArr = (TP_DEALTIMETDO[])tm.selByPKArr(context, tdoTP_DEALTIMEIn, typeof(TP_DEALTIMETDO), null, "DEALTIME", null);
        if (tdoTP_DEALTIMEOutArr.Length == 0)
        {
            context.AddError("没有找到有效的结算处理时间");
            return false;
        }
        else
        {
            DateTime dealDate = tdoTP_DEALTIMEOutArr[0].DEALDATE.Date;
            DateTime endDate = DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null);
            if (endDate.CompareTo(dealDate) >= 0)
            {
                context.AddError("结束日期过大，未结算");
                return false;
            }
        }
        return true;
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        DateTime? toDate = null;
        bool b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        hiddate.Value = "";
        hidBalunit.Value = "";
        hidApproval.Value = "";

        validate();
        if (context.hasError()) return;

        if (!checkEndDate()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TD_EOC_DAILY_REPORTFiApproval";
        pdo.var1 = txtToDate.Text;
        pdo.var2 = selBalUint.SelectedValue;
        pdo.var3 = selApproval.SelectedValue;
        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);

        hidNo.Value = pdo.var9;

        DataTable dt = new DataTable();
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        else
        {
            dt = data.Clone();
            dt.Rows.Clear();
            hiddate.Value = txtToDate.Text;
            hidBalunit.Value = selBalUint.SelectedValue;
            hidApproval.Value = selApproval.SelectedValue;

            List<string> ListID = new List<string>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string key = data.Rows[i]["商户代码"].ToString() + data.Rows[i]["结算周期"].ToString();
                if (!ListID.Contains(key))
                {
                    DataRow dr = data.Rows[i];
                    dt.Rows.Add(dr.ItemArray);
                    ListID.Add(key);
                }
                else
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        string dtkey = dt.Rows[j]["商户代码"].ToString() + dt.Rows[j]["结算周期"].ToString();
                        if (dtkey == key)
                        {
                            dt.Rows[j]["实际调帐金额"] = ((decimal)dt.Rows[j]["实际调帐金额"] + (decimal)data.Rows[i]["实际调帐金额"]).ToString();
                            dt.Rows[j][8] = ((decimal)dt.Rows[j][8] - (decimal)data.Rows[i]["实际调帐金额"]).ToString();
                        }
                    }
                }
            }
        }

        UserCardHelper.resetData(gvResult, dt);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    protected void btnBankExport_Click(object sender, EventArgs e)
    {

        string newpath = Server.MapPath("~/") + @"Templates\" + "网银汇款列表" + ".xls";
        System.IO.FileInfo filetemp = new System.IO.FileInfo(newpath);
        if (filetemp.Exists)
        {
            filetemp.Delete();
        }
        ExcelEditHelper excelHelper = new ExcelEditHelper();
        string pathdir = Server.MapPath("~/") + "Templates\\";
        excelHelper.Open(pathdir + "EOC_DAILY_REPORTFiApproval.xls");

        SP_FI_QueryPDO pd = new SP_FI_QueryPDO();
        pd.funcCode = "TD_EOC_DAILY_REPORTFiApprovalBank";
        pd.var1 = hiddate.Value;
        pd.var2 = hidBalunit.Value;
        pd.var3 = hidApproval.Value;
        StoreProScene store = new StoreProScene();
        DataTable dt = store.Execute(context, pd);

        int i = 0;
        int j = 0;

        //Excel填值
        for (i = 0; i < dt.Rows.Count; i++)
        {
            for (j = 0; j < dt.Columns.Count; j++)
            {
                excelHelper.SetCellValue(excelHelper.GetSheet("网银汇款列表"), 2 + i, 1 + j, dt.Rows[i][j].ToString());
            }
        }

        excelHelper.SaveAs(newpath);
        excelHelper.Close();
        System.IO.FileInfo file = new System.IO.FileInfo(newpath);

        //打开保存对话框

        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = false;
        Response.Charset = "UTF-8";
        Response.ContentType = "application/ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode("网银汇款列表" + DateTime.Now.ToString("yyyyMMdd").ToString() + ".xls", System.Text.Encoding.UTF8));
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
        Response.AppendHeader("Content-Length", file.Length.ToString());
        Response.Charset = "";
        this.EnableViewState = false;
        Response.WriteFile(newpath);
        //删除创建的Excel文件
        //FileInfo fileinf = new FileInfo(newpath);
        //fileinf.Delete();
        //关闭连接
        Response.Flush();
        Response.End();
    }


    protected void btnExportPayFile_Click(object sender, EventArgs e)
    {
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        submitApproval("", "D004P03001: 审核成功");
    }

    // 调用信息更改审核存储过程

    private void submitApproval(string stateCode, string okMsgCode)
    {

        //清空临时表

        CommonHelper.clearNewTempTable(context, Session.SessionID);

        //插入临时表

        context.DBOpen("Insert");
        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("chkCust");
            if (cb != null && cb.Checked)
            {
                ++count;

                String id = gvr.Cells[1].Text;
                String state = selApproval2.SelectedValue;
                string balunitno = gvr.Cells[2].Text;
                string zhangqi = gvr.Cells[11].Text; 
                context.ExecuteNonQuery("insert into TMP_COMMON_NEW (F0,F1,F2,F14,SESSIONID) "
               + " values('" + id + "','" + balunitno + "','" + zhangqi + "','" + state + "','" + Session.SessionID + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误


        if (count <= 0)
        {
            context.AddError("A001002213");
            return;
        }

        // 调用信息更改审核存储过程
        context.SPOpen();
        context.AddField("P_SESSIONID").Value = Session.SessionID;

        bool ok = context.ExecuteSP("SP_FI_DailyReportFiApproval");

        if (ok) AddMessage(okMsgCode);

        //清空临时表

        CommonHelper.clearNewTempTable(context, Session.SessionID);
    }
    public String getDataKeys2(String keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    public String getDataKeys2(String keysname, int rowIndex)
    {
        return gvResult.DataKeys[rowIndex][keysname].ToString();
    }
}
