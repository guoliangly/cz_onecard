﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Master;
using Common;
using TM;
using TDO.UserManager;
using PDO.Financial;

public partial class ASP_Financial_FI_EvaluateReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");


            //初始化部门

            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
            TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
            ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
            selDept.SelectedValue = context.s_DepartID;
            InitStaffList(context.s_DepartID);
            selStaff.SelectedValue = context.s_UserID;

        }
    }

    protected void selDept_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitStaffList(selDept.SelectedValue);
        selStaff.SelectedValue = "";
    }

    private double totalCount = 0;

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "QueryEvaluateReport";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = selDept.SelectedValue;
        pdo.var4 = selStaff.SelectedValue;

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("查询结果为空");
        }
        else
        {
            totalCount = data.Rows.Count;
        }

        UserCardHelper.resetData(gvResult, data);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    private int _vsatis = 0;
    private int _satis = 0;
    private int _nor = 0;
    private int _nosatis = 0;
    private int _noneva = 0;
    private string _satisfic = "";
    private string _satisNum = "";

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (string.IsNullOrEmpty(selDept.SelectedValue) && string.IsNullOrEmpty(selStaff.SelectedValue))
        {
            //计算满意度和统计量
            if (e.Row.RowType == DataControlRowType.DataRow) //数据行
            {
                double satisfic;//满意度
                int vsatis = int.Parse(e.Row.Cells[2].Text, null); //非常满意
                int satis = int.Parse(e.Row.Cells[3].Text, null);  //满意
                int nor = int.Parse(e.Row.Cells[4].Text, null);    //一般
                int nosatis = int.Parse(e.Row.Cells[5].Text, null);//不满意
                int noneva = int.Parse(e.Row.Cells[6].Text, null);  //未评价

                e.Row.Cells[7].Text = GetSatisfic(vsatis, satis, nor, nosatis);
                e.Row.Cells[8].Text = GetSatisNum(vsatis, satis, nor, nosatis, noneva, e.Row.Cells[0].Text, "");

                _vsatis += vsatis;
                _satis += satis;
                _nor += nor;
                _nosatis += nosatis;
                _noneva += noneva;
                _satisfic = GetSatisfic(_vsatis, _satis, _nor, _nosatis);
                _satisNum = GetSatisNum(_vsatis, _satis, _nor, _nosatis, _noneva, "", "");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[1].Text = "合计";
                e.Row.Cells[2].Text = _vsatis.ToString();
                e.Row.Cells[3].Text = _satis.ToString();
                e.Row.Cells[4].Text = _nor.ToString();
                e.Row.Cells[5].Text = _nosatis.ToString();
                e.Row.Cells[6].Text = _noneva.ToString();
                e.Row.Cells[7].Text = _satisfic.ToString();
                e.Row.Cells[8].Text = _satisNum.ToString();
            }
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[6].Visible = false;
        }
        else
        {
            //计算满意度和统计量
            if (e.Row.RowType == DataControlRowType.DataRow) //数据行
            {
                double satisfic;//满意度
                int vsatis = int.Parse(e.Row.Cells[4].Text, null); //非常满意
                int satis = int.Parse(e.Row.Cells[5].Text, null);  //满意
                int nor = int.Parse(e.Row.Cells[6].Text, null);    //一般
                int nosatis = int.Parse(e.Row.Cells[7].Text, null);//不满意
                int noneva = int.Parse(e.Row.Cells[8].Text, null);  //未评价

                e.Row.Cells[9].Text = GetSatisfic(vsatis, satis, nor, nosatis);
                e.Row.Cells[10].Text = GetSatisNum(vsatis, satis, nor, nosatis, noneva, e.Row.Cells[0].Text, e.Row.Cells[1].Text);

                _vsatis += vsatis;
                _satis += satis;
                _nor += nor;
                _nosatis += nosatis;
                _noneva += noneva;
                _satisfic = GetSatisfic(_vsatis, _satis, _nor, _nosatis);
                _satisNum = GetSatisNum(_vsatis, _satis, _nor, _nosatis, _noneva, selDept.SelectedValue, selStaff.SelectedValue);
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[2].Text = "合计";
                e.Row.Cells[2].RowSpan = 2;
                e.Row.Cells[4].Text = _vsatis.ToString();
                e.Row.Cells[5].Text = _satis.ToString();
                e.Row.Cells[6].Text = _nor.ToString();
                e.Row.Cells[7].Text = _nosatis.ToString();
                e.Row.Cells[8].Text = _noneva.ToString();
                e.Row.Cells[9].Text = _satisfic.ToString();
                e.Row.Cells[10].Text = _satisNum.ToString();
            }
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[8].Visible = false;
        }
    }

    /// <summary>
    /// 获取满意度
    /// </summary>
    /// <param name="vsatis">非常满意次数</param>
    /// <param name="satis">满意</param>
    /// <param name="nor">一般</param>
    /// <param name="nosatis">不满意</param>
    /// <returns>满意度(%)</returns>
    protected string GetSatisfic(int vsatis, int satis, int nor, int nosatis)
    {
        double satisfic = 0.00;
        //满意度=(非常满意次数总和×100＋满意次数总和×90＋一般次数总和×70＋不满意次数总和×0)÷(非常满意次数总和＋满意次数总和＋一般次数总和＋不满意次数总和)÷100
        try
        {
            satisfic = Convert.ToDouble(vsatis * 100 + satis * 90 + nor * 70 + nosatis * 0) / (vsatis + satis + nor + nosatis) / 100;
            return satisfic.ToString("P");
        }
        catch
        {
            return "";
        }

    }

    /// <summary>
    /// 获取统计量
    /// </summary>
    /// <param name="vsatis">非常满意</param>
    /// <param name="satis">满意</param>
    /// <param name="nor">一般</param>
    /// <param name="nosatis">不满意</param>
    /// <param name="noneva">未评价</param>
    /// <returns>统计量(%)</returns>
    protected string GetSatisNum(int vsatis, int satis, int nor, int nosatis, int noneva, string departno, string staffno)
    {
        double satisNum = 0.00;
        //统计量=选择时间段内所有评价次数的总和(包括营业员触发评价但是客户未评价)÷选择时间段内操作的所有需要评价业务的次数总和

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "QueryNeedEvanum";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = departno;
        pdo.var4 = staffno;

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        if (data == null || data.Rows.Count == 0)
        {
            return "";
        }
        else
        {
            try
            {
                satisNum = Convert.ToDouble(vsatis + satis + nor + nosatis + noneva) / Convert.ToInt32(data.Rows[0][0].ToString());
                return satisNum.ToString("P");
            }
            catch
            {
                return "";
            }

        }
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }

    private bool HasOperPower(string powerCode)
    {
        //TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tm.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    private void InitStaffList(string deptNo)
    {
        if (deptNo == "")
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "");
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
            selStaff.SelectedValue = context.s_UserID;
        }
        else
        {
            TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
            tdoTD_M_INSIDESTAFFIn.DEPARTNO = deptNo;
            tdoTD_M_INSIDESTAFFIn.DIMISSIONTAG = "1";

            TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tm.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_DEPT", null);
            ControlDeal.SelectBoxFill(selStaff.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        }
    }
}
