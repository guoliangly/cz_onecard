﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_TradeCollectReport.aspx.cs" Inherits="ASP_Financial_FI_TradeCollectReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>业务统计报表</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        var cal1;
        var cal2;

        function pageLoad() {
            try {
                cal1 = $find("calendar1");
                cal2 = $find("calendar2");

                modifyCalDelegates(cal1);
                modifyCalDelegates(cal2);
            }
            catch (e)
            { }
            
        }

        function modifyCalDelegates(cal) {
            //we need to modify the original delegate of the month cell. 
            cal._cell$delegates = {
                mouseover: Function.createDelegate(cal, cal._cell_onmouseover),
                mouseout: Function.createDelegate(cal, cal._cell_onmouseout),

                click: Function.createDelegate(cal, function (e) {
                    /// <summary>  
                    /// Handles the click event of a cell 
                    /// </summary> 
                    /// <param name="e" type="Sys.UI.DomEvent">The arguments for the event</param> 

                    e.stopPropagation();
                    e.preventDefault();

                    if (!cal._enabled) return;

                    var target = e.target;
                    var visibleDate = cal._getEffectiveVisibleDate();
                    Sys.UI.DomElement.removeCssClass(target.parentNode, "ajax__calendar_hover");
                    switch (target.mode) {
                        case "prev":
                        case "next":
                            cal._switchMonth(target.date);
                            break;
                        case "title":
                            switch (cal._mode) {
                                case "days": cal._switchMode("months"); break;
                                case "months": cal._switchMode("years"); break;
                            }
                            break;
                        case "month":
                            //if the mode is month, then stop switching to day mode. 
                            if (target.month == visibleDate.getMonth()) {
                                //this._switchMode("days"); 
                            } else {
                                cal._visibleDate = target.date;
                                //this._switchMode("days"); 
                            }
                            cal.set_selectedDate(target.date);
                            cal._switchMonth(target.date);
                            cal._blur.post(true);
                            cal.raiseDateSelectionChanged();
                            break;
                        case "year":
                            if (target.date.getFullYear() == visibleDate.getFullYear()) {
                                cal._switchMode("months");
                            } else {
                                cal._visibleDate = target.date;
                                cal._switchMode("months");
                            }
                            break;

                        //                case "day":                              
                        //                    this.set_selectedDate(target.date);                              
                        //                    this._switchMonth(target.date);                              
                        //                    this._blur.post(true);                              
                        //                    this.raiseDateSelectionChanged();                              
                        //                    break;                              
                        case "today":
                            cal.set_selectedDate(target.date);
                            cal._switchMonth(target.date);
                            cal._blur.post(true);
                            cal.raiseDateSelectionChanged();
                            break;
                    }

                })
            }

        }

        function onCalendarShown(sender, args) {
            //set the default mode to month 
            sender._switchMode("months", true);
            changeCellHandlers(cal1);
        }


        function changeCellHandlers(cal) {

            if (cal._monthsBody) {

                //remove the old handler of each month body. 
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        $common.removeHandlers(row.cells[j].firstChild, cal._cell$delegates);
                    }
                }
                //add the new handler of each month body. 
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        $addHandlers(row.cells[j].firstChild, cal._cell$delegates);
                    }
                }

            }
        }

        function onCalendarHidden(sender, args) {

            if (sender.get_selectedDate()) {
                if (cal1.get_selectedDate() && cal2.get_selectedDate() && cal1.get_selectedDate() > cal2.get_selectedDate()) {
                    alert('The "From" Date should smaller than the "To" Date, please reselect!');
                    sender.show();
                    return;
                }
                //get the final date 
                var finalDate = new Date(sender.get_selectedDate());
                var selectedMonth = finalDate.getMonth();
                finalDate.setDate(1);
                if (sender == cal2) {
                    // set the calender2's default date as the last day 
                    finalDate.setMonth(selectedMonth + 1);
                    finalDate = new Date(finalDate - 1);
                }
                //set the date to the TextBox 
                sender.get_element().value = finalDate.format(sender._format);
            }
        } 
 
    </script> 

</head>
<body>
    <form id="form1" runat="server">
    
        <div class="tb">
		    财务管理->业务统计报表
	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>  
               
            <!-- #include file="../../ErrorMsg.inc" -->  
	        <div class="con">

	           <div class="card">查询</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td style="width:100px;"><div align="right">业务类型:</div></td>
                        <td>
                            <asp:DropDownList ID="selTradeType" CssClass="inputmid" runat="server"></asp:DropDownList>
                        </td>
                        <td><div align="right">按日/按月:</div></td>
                        <td>
                            <asp:DropDownList ID="selDayAndMonth" CssClass="input" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selDayAndMonth_Changed">
                            <asp:ListItem Value="DAY">按日统计</asp:ListItem>
                            <asp:ListItem Value="MONTH">按月统计</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td><div align="right"></div></td>
                        <td>
                            
                        </td>
                        <td align="right">
                            <asp:Button ID="Button1" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
                   <tr id="trDay" runat="server">
                        <td><div align="right">开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">结束日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                        </td>
                        <td colspan="3">&nbsp;</td>
                   </tr>
                      <tr id="trMonth" runat="server" visible=false>
                        <td><div align="right">开始月份:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromMonth" MaxLength="6" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender BehaviorID="calendar1"  OnClientHidden="onCalendarHidden"  OnClientShown="onCalendarShown" 
 ID="CalendarExtender3" runat="server" TargetControlID="txtFromMonth" Format="yyyyMM" />
                        </td>
                        <td><div align="right">结束月份:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtToMonth" MaxLength="6" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender  BehaviorID="calendar2"  OnClientHidden="onCalendarHidden"  OnClientShown="onCalendarShown" 
 ID="CalendarExtender4" runat="server" TargetControlID="txtToMonth" Format="yyyyMM" />
                        </td>
                        <td colspan="3">&nbsp;</td>
                   </tr>
               </table>
               
             </div>

            <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                        <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click" />
                        <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="return printGridView('printarea');" />
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:360px;">
                
                    <table id="printReport" width ="95%">
                        <tr align="center">
                            <td style ="font-size:16px;font-weight:bold" id="tdTitle" runat="server"></td>
                        </tr>
                        <tr>
                            <td>
                                <table width="300px" align="right">
                                    <tr align="right">
                                        <td  id="tdFromDate" runat="server"></td>
                                        <td id="tdToDate" runat="server"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                
                    <asp:GridView ID="gvResult" runat="server"
        Width = "95%"
        CssClass="tab2"
        HeaderStyle-CssClass="tabbt"
        AlternatingRowStyle-CssClass="tabjg"
        SelectedRowStyle-CssClass="tabsel"

        PagerSettings-Mode=NumericFirstLast
        PagerStyle-HorizontalAlign=left
        PagerStyle-VerticalAlign=Top
        AutoGenerateColumns="False"
        OnDataBound="gvResult_DataBound"
        OnPreRender="gvResult_PreRender"
        ShowFooter="true" OnRowDataBound="gvResult_RowDataBound"
        >
           <Columns>
                <asp:BoundField HeaderText="时间" DataField="时间"/>
                <asp:BoundField HeaderText="业务类型" DataField="业务类型"/>
                <asp:BoundField HeaderText="部门" DataField="部门"/>
                <asp:BoundField HeaderText="笔数" DataField="笔数"/>
                <asp:BoundField HeaderText="金额" DataField="金额"/>
           </Columns>           
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>时间</td>
                    <td>业务类型</td>
                    <td>部门</td>
                    <td>笔数</td>
                    <td>金额</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>
                    
                </div>
              </div>
            </div>
  
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>