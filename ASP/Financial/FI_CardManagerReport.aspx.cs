﻿using System;
using System.Data;
using Common;
using PDO.Financial;
using Master;
using System.Web.UI.WebControls;

/***************************************************************
 * update: chenwentao 2015-01-08
 * desc: 增加挂失补卡统计
 * *************************************************************/

public partial class ASP_Financial_FI_CardManagerReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            selType.Items.Add(new ListItem("售卡统计", "CardSaleStat"));
            //selType.Items.Add(new ListItem("充值统计", "ChargeCardStat"));
            selType.Items.Add(new ListItem("换卡统计", "ChangeCardStat"));
            selType.Items.Add(new ListItem("退卡统计", "ReturnCardStat"));
            selType.Items.Add(new ListItem("卡使用统计", "Cardusestate"));
            selType.Items.Add(new ListItem("集团客户统计", "groupcuststate"));
            selType.Items.Add(new ListItem("补卡统计","LossSupply"));

            DateTime tf = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1);
            DateTime tt = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month));

            txtFromDate.Text = tf.ToString("yyyyMMdd");
            txtToDate.Text = tt.ToString("yyyyMMdd");
        }
    }
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);
        UserCardHelper.resetData(gvChangeReport, null);
        UserCardHelper.resetData(gvSaleReport, null);
        validate();
        if (context.hasError()) return;

        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = selType.SelectedValue;
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = drpStaticDisplay.SelectedValue;//售卡统计 显示方式 换卡统计
        StoreProScene storePro = new StoreProScene();
        DataTable data = null;
        if (drpStaticDisplay.SelectedValue == "2" && selType.SelectedValue == "ChangeCardStat")
        {
           
        }
        else
        {
            data = storePro.Execute(context, pdo);
        }
        labTitle.Text = selType.SelectedItem.Text;
        //hidNo.Value = pdo.var9;

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
            btnPrint.Enabled = false;
        }
        else
        {
            btnPrint.Enabled = true;
        }
        if (selType.SelectedValue == "ChangeCardStat")//换卡统计
        {
            UserCardHelper.resetData(gvChangeReport, data);
        }
        else if (selType.SelectedValue == "CardSaleStat")//售卡统计
        {
            UserCardHelper.resetData(gvSaleReport, data);
        }
        else if (selType.SelectedValue == "LossSupply")//补卡统计
        {
            UserCardHelper.resetData(gvSaleReport, data);
        }
        else
        {
            UserCardHelper.resetData(gvResult, data);
        }
        
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else if (gvChangeReport.Rows.Count > 0)
        {
            ExportGridView(gvChangeReport);
        }
        else if (gvSaleReport.Rows.Count > 0)
        {
            ExportGridView(gvSaleReport);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    private Array intArray = null;
    private Array doubleArray = null;
    protected void lvwQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (selType.SelectedValue == "CardSaleStat")
        //{
        //    //ControlDeal.RowDataBound(e);
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {

        //        if (intArray == null)
        //        {
        //            intArray = Array.CreateInstance(typeof(int), e.Row.Cells.Count);
        //            doubleArray = Array.CreateInstance(typeof(double), e.Row.Cells.Count);

        //        }
        //        for (int i = 1; i < e.Row.Cells.Count - 1; )
        //        {
        //            intArray.SetValue(Convert.ToInt32(intArray.GetValue(i)) + Convert.ToInt32(GetTableCellValue(e.Row.Cells[i])), i);
        //            doubleArray.SetValue(Convert.ToDouble(doubleArray.GetValue(i + 1)) + Convert.ToDouble(GetTableCellValue(e.Row.Cells[i + 1])), i + 1);
        //            i = i + 2;
        //        }

        //    }
        //    else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        //    {
        //        //e.Row.Cells[0].ColumnSpan = 2;
        //        e.Row.Cells[0].Text = "总计";
        //        //e.Row.Cells[1].Visible = false;
        //        for (int i = 1; i < e.Row.Cells.Count - 1; )
        //        {
        //            e.Row.Cells[i].Text = intArray.GetValue(i).ToString();
        //            e.Row.Cells[i + 1].Text = doubleArray.GetValue(i + 1).ToString();
        //            i = i + 2;
        //        }
        //    }
        //}
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }



    protected void gvChangeReport_PreRender(object sender, EventArgs e)
    {
       // GridViewMergeHelper.MergeGridViewRows(gvChangeReport, 0, 1);
    }
    protected void gvChangeReport_DataBound(object sender, EventArgs e)
    {

    }
    protected void gvChangeReport_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (drpStaticDisplay.SelectedValue == "0")//公司业务
            {
                //普卡、学生卡、老人卡、异形卡、金福卡、市民卡、天翼龙城通卡
                //string header = "网点#龙城通 自然损坏(张),人为损坏(张)#金福卡 自然损坏(张),人为损坏(张)#学生卡 自然损坏(张),人为损坏(张)#老人卡 自然损坏(张),人为损坏(张)#天翼龙城通卡 自然损坏(张),人为损坏(张)";
                string header = "网点";
                header += "#" + "标准卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "学生卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "老人卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "高龄卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "异形卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "市民卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "金福卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "测试卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "换卡自然损总数";
                header += "#" + "换卡人为损总数";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (drpStaticDisplay.SelectedValue == "1")//合作商业务
            {
                string header = "网点";
                header += "#" + "天翼龙城通卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "移动龙城通卡" + " 自然损坏(张),人为损坏(张)";
                header += "#" + "换卡自然损总数";
                header += "#" + "换卡人为损总数";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            
        }
    }

    #region 售卡统计
    protected void gvSaleReport_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (drpStaticDisplay.SelectedValue == "0")//公司业务
            {
                string header = "网点";
                header += "#" + "标准卡" + " 总数,总金额";
                header += "#" + "学生卡" + " 总数,总金额";
                header += "#" + "老人卡" + " 总数,总金额";
                header += "#" + "高龄卡" + " 总数,总金额";
                header += "#" + "异形卡" + " 总数,总金额";
                header += "#" + "市民卡" + " 总数,总金额";
                header += "#" + "金福卡" + " 总数,总金额";
                header += "#" + "测试卡" + " 总数,总金额";
                header += "#" + "售卡总数";
                header += "#" + "售卡总金额";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (drpStaticDisplay.SelectedValue == "1")//合作商业务
            {
                string header = "网点";
                header += "#" + "天翼龙城通卡" + " 总数,总金额";
                header += "#" + "移动龙城通卡" + " 总数,总金额";
                header += "#" + "售卡总数";
                header += "#" + "售卡总金额";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (drpStaticDisplay.SelectedValue == "2")//龙城通普通卡
            {
                string header = "网点";
                header += "#" + "龙城通首发卡" + " 总数,总金额";
                header += "#" + "天宁寺卡" + " 总数,总金额";
                header += "#" + "新天宁寺卡" + " 总数,总金额";
                header += "#" + "腾龙卡" + " 总数,总金额";
                header += "#" + "售卡总数";
                header += "#" + "售卡总金额";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }

            if (selType.SelectedValue == "LossSupply")
            {
                if (drpStaticDisplay.SelectedValue == "0")//公司业务
                {
                    string header = "网点";
                    header += "#" + "标准卡" + " 总数,总金额";
                    header += "#" + "学生卡" + " 总数,总金额";
                    header += "#" + "老人卡" + " 总数,总金额";
                    header += "#" + "高龄卡" + " 总数,总金额";
                    header += "#" + "异形卡" + " 总数,总金额";
                    header += "#" + "市民卡" + " 总数,总金额";
                    header += "#" + "金福卡" + " 总数,总金额";
                    header += "#" + "测试卡" + " 总数,总金额";
                    header += "#" + "补卡总数";
                    header += "#" + "补卡总金额";
                    GridViewHeaderHelper.SplitTableHeader(e.Row, header);
                }
            }
        }
    }
    protected void selType_SelectedIndexChanged(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);
        UserCardHelper.resetData(gvChangeReport, null);
        UserCardHelper.resetData(gvSaleReport, null);
        if (selType.SelectedValue == "CardSaleStat" || selType.SelectedValue == "ChangeCardStat")
        {
            pnlSaleStatic.Visible = true;
            //选择换卡统计
            if (selType.SelectedValue == "ChangeCardStat")
            {
                drpStaticDisplay.Items[2].Enabled = false;
            }
            else
            {
                drpStaticDisplay.Items[2].Enabled = true;
            }
        }
        else
        {
            pnlSaleStatic.Visible = false;
        }
    }

    #endregion

   
}
