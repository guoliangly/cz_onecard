﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDO.Financial;
using Master;
using System.Data;
using Common;
using System.IO;
using TDO.BalanceChannel;
using TM;
using System.Text.RegularExpressions;

public partial class ASP_Financial_FI_SpeAdjustAccInput : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            gvResultBind();

            //初始化行业
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211", "TD_M_CALLINGNO", null);

            ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

            //清空临时表
            clearTempTable();
        }
    }

    private void gvResultBind()
    {
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
        gvResult.SelectedIndex = -1;
    }

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[8].Visible = false;
        }
    }

    protected void ClearContol()
    {
        //txtBalunitName.Text = "";
        //txtBalunitNo.Text = "";
        txtSdMoney.Text = "";
        txtReMoney.Text = "";
        txtRemark.Text = "";
        txtcommission.Text = "";
        
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        ClearContol();

        //if (selCorp.SelectedValue == "")
        //{
        //    context.AddError("A009478004:请选择单位名称", selCorp);
        //    return;
        //}


        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = "speinput";
        pdo.var1 = selCorp.SelectedValue;
        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        UserCardHelper.resetData(gvResult, data);
        GetMoney();
    }

    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {

        //选择行业名称后处理
        if (selCalling.SelectedValue == "")
        {
            selCorp.Items.Clear();
        }
        else
        {
            //初始化该行业下的单位名称
            TMTableModule tmTMTableModule = new TMTableModule();
            TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
            tdoTD_M_CORPIn.CALLINGNO = selCalling.SelectedValue;

            TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, "TD_M_CORPCALLUSAGE", null);
            ControlDeal.SelectBoxFill(selCorp.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
        }

        selBalUint.Items.Clear();
    }

    //选择查询的单位名称后初始化结算单元名称
    protected void selCorp_SelectedIndexChanged(object sender, EventArgs e)
    {

        //初始化结算单元(属于选择单位)名称下拉列表值

        TMTableModule tmTMTableModule = new TMTableModule();
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;

        tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
        tdoTF_TRADE_BALUNITIn.CORPNO = selCorp.SelectedValue;
        tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCORP", null);

        ControlDeal.SelectBoxFill(selBalUint.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", false);

        selCycleDate.Items.Clear();
        try
        {
            txtBalunitNo.Text = selBalUint.SelectedValue;
            txtBalunitName.Text = selBalUint.SelectedItem.Text.Split(':')[1].ToString();

            SP_FI_StatPDO pdo = new SP_FI_StatPDO();
            pdo.funcCode = "CycleQuery";
            pdo.var1 = txtBalunitNo.Text;
            StoreProScene storePro = new StoreProScene();

            DataTable dataTable = storePro.Execute(context, pdo);
            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                return;
            }

            Object[] itemArray;
            ListItem li;
            for (int i = 0; i < dataTable.Rows.Count; ++i)
            {
                itemArray = dataTable.Rows[i].ItemArray;

                li = new ListItem("" + itemArray[0].ToString() + "-" + itemArray[1].ToString(), itemArray[2].ToString());
                selCycleDate.Items.Add(li);
            }
        }
        catch
        {
            txtBalunitNo.Text = "";
            txtBalunitName.Text = "";
            txtSdMoney.Text = "0";
            selCycleDate.Items.Clear();
        }
    }

    protected void CheckAll(object sender, EventArgs e)
    {
        //全选调帐明细记录
        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            if (!gvr.Cells[0].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
        GetMoney();
    }

    protected void on_check(object sender, EventArgs e)
    {
        GetMoney();
    }

    private void GetMoney()
    {
        double count = 0;
        foreach (GridViewRow item in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)item.FindControl("ItemCheckBox");
            if (cb.Checked)
            {
                count = count + Convert.ToDouble(item.Cells[8].Text.ToString());
            }
        }
        labCount.Text = count.ToString();
        txtSdMoney.Text = count.ToString();
    }

    protected void txtcommission_TextChanged(object sender, EventArgs e)
    {
        if (txtSdMoney.Text == "0")
        {
            return;
        }

        double ReMoney = Convert.ToDouble(txtSdMoney.Text.Trim()) - (Convert.ToDouble(txtSdMoney.Text.Trim()) * Convert.ToDouble(txtcommission.Text.Trim())) / 100;

        txtReMoney.Text = ReMoney.ToString("0.00");
    }

    private void clearTempTable()
    {   //清空临时表数据

        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_ADJUSTACC_IMP where SessionId='"
            + Session.SessionID + "'");
        context.DBCommit();
    }

    private bool RecordIntoTmp()
    {
        //记录入临时表
        context.DBOpen("Insert");

        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (cb != null && cb.Checked)
            {
                ++count;

                context.ExecuteNonQuery("insert into TMP_ADJUSTACC_IMP values('" + gvr.Cells[9].Text + "','1234567890123456','1','" + Session.SessionID + "')");
            }
        }

        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A009111001");
            return false;
        }

        return true;
    }

    //提交
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        clearTempTable();

        if (!vailidate()) return;

        if (!RecordIntoTmp()) return;

        context.SPOpen();
        context.AddField("p_balunitno").Value = txtBalunitNo.Text;
        context.AddField("p_comscheme").Value = Convert.ToDouble(txtcommission.Text) / 100;
        context.AddField("p_readjustmoney").Value = Convert.ToDouble(txtReMoney.Text.Trim()) * 100;
        context.AddField("p_begindate").Value = selCycleDate.SelectedItem.Text.Split('-')[0].ToString().Replace("/","");
        context.AddField("p_enddate").Value = selCycleDate.SelectedItem.Text.Split('-')[1].ToString().Replace("/", "");
        context.AddField("p_remark").Value = txtRemark.Text;
        context.AddField("p_sessionID").Value = Session.SessionID;
        bool ok = context.ExecuteSP("SP_FI_SPEADJUSTINPUT");
        if (ok)
        {
            AddMessage("特殊调帐统计录入成功");

            btnQuery_Click(sender, e);
        }
    }

    private bool vailidate()
    {
        if (txtBalunitNo.Text.Trim() == "")
        {
            context.AddError("商户代码不能为空", txtBalunitNo);
        }
        if (txtcommission.Text == "0" || txtcommission.Text == "")
        {
            context.AddError("A009478005:佣金率不能为空或者为零", txtcommission);
        }
        else
        {
            Regex regex = new Regex("^([0-9.]+)$", RegexOptions.IgnoreCase);
            Match match = regex.Match(txtcommission.Text);
            if (!match.Success)
            {
                context.AddError("A009478006:佣金率格式输入不正确", txtcommission);
            }
        }

        if (txtRemark.Text.Length > 100)
        {
            context.AddError("A001001129:备注长度大于50位", txtRemark);
        }

        if (context.hasError())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
