﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDO.Financial;
using Master;
using System.Data;
using Common;

public partial class ASP_Financial_FI_CG_RenewReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期
            txtDate.Text = DateTime.Today.AddMonths(-1).ToString("yyyyMM");
            DDLBind();
        }
    }
    private int totalCharges = 0;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            totalCharges += Convert.ToInt32(GetTableCellValue(e.Row.Cells[3]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[3].Text = totalCharges.ToString();
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    private void DDLBind()
    {
        string strSql = string.Format("select departcode,departcode||':'||departname name from td_m_cgseldepart order by departcode");
        context.DBOpen("Select");
        DataTable dt = context.ExecuteReader(strSql);
        context.DBCommit();
        selCorp.DataTextField = "name";
        selCorp.DataValueField = "departcode";
        selCorp.DataSource = dt;
        selCorp.DataBind();

        selCorp.Items.Insert(0,new ListItem("---请选择---",""));
    }

    //protected void queryBalUnit(object sender, EventArgs e)
    //{
    //    string name = txtBalUnitName.Text.Trim();
    //    if (name == "")
    //    {
    //        selBalUnit.Items.Clear();
    //        txtBalUnit.Text = "";
    //        return;
    //    }

    //    TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
    //    TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;
    //    tdoTF_TRADE_BALUNITIn.BALUNIT = "%" + name + "%";
    //    tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tm.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_NAME", null);

    //    selBalUnit.Items.Clear();
    //    foreach (TF_TRADE_BALUNITTDO ddoBalUnit in tdoTF_TRADE_BALUNITOutArr)
    //    {
    //        selBalUnit.Items.Add(new ListItem(ddoBalUnit.BALUNITNO + ":" + ddoBalUnit.BALUNIT, ddoBalUnit.BALUNITNO));
    //    }

    //    if (tdoTF_TRADE_BALUNITOutArr.Length > 0)
    //    {
    //        selBalUnit.SelectedValue = tdoTF_TRADE_BALUNITOutArr[0].BALUNITNO;
    //        txtBalUnit.Text = tdoTF_TRADE_BALUNITOutArr[0].BALUNITNO;
    //    }
    //    else
    //    {
    //        txtBalUnit.Text = "";
    //    }
    //}

    //protected void selBalUnit_Change(object sender, EventArgs e)
    //{
    //    txtBalUnit.Text = selBalUnit.SelectedValue;
    //}

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "LIJIN_RENEW_REPORT";
        pdo.var1 = txtDate.Text.Trim();
        pdo.var2 = selCorp.SelectedValue;
        //pdo.var7 = txtBalUnit.Text;

        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);
        //hidNo.Value = pdo.var9;

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        totalCharges = 0;
        UserCardHelper.resetData(gvResult, data);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    // 查询输入校验处理
    private bool validate()
    {
        Validation valid = new Validation(context);

        if (Validation.isEmpty(txtDate))
        {
            context.AddError("查询时间不能为空");
        }
        else if (!Validation.isDate(txtDate.Text.Trim(), "yyyyMM"))
        {
            context.AddError("查询时间格式必须为yyyyMM");
        }
        if (context.hasError())
            return false;
        else
            return true;
    }
}
