﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Master;
using Common;
using TM;
using TDO.UserManager;
using PDO.Financial;

public partial class ASP_Financial_FI_CardMouldDailyEndReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");

            if (HasOperPower("201008") || context.s_DepartID == "0007")//全部网点主管或财务部
            {
                //初始化部门

                TMTableModule tmTMTableModule = new TMTableModule();
                TD_M_INSIDEDEPARTTDO tdoTD_M_INSIDEDEPARTIn = new TD_M_INSIDEDEPARTTDO();
                TD_M_INSIDEDEPARTTDO[] tdoTD_M_INSIDEDEPARTOutArr = (TD_M_INSIDEDEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDEDEPARTIn, typeof(TD_M_INSIDEDEPARTTDO), null, "");
                ControlDeal.SelectBoxFill(selDept.Items, tdoTD_M_INSIDEDEPARTOutArr, "DEPARTNAME", "DEPARTNO", true);
            }
        }
    }

    private bool HasOperPower(string powerCode)
    {
        //TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_ROLEPOWERTDO ddoTD_M_ROLEPOWERIn = new TD_M_ROLEPOWERTDO();
        string strSupply = " Select POWERCODE From TD_M_ROLEPOWER Where POWERCODE = '" + powerCode + "' And ROLENO IN ( SELECT ROLENO From TD_M_INSIDESTAFFROLE Where STAFFNO ='" + context.s_UserID + "')";
        DataTable dataSupply = tm.selByPKDataTable(context, ddoTD_M_ROLEPOWERIn, null, strSupply, 0);
        if (dataSupply.Rows.Count > 0)
            return true;
        else
            return false;
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        DataTable data = SPHelper.callCMQuery(context, "CM_DailyEndReport", selDept.SelectedValue, txtFromDate.Text.Trim(), txtToDate.Text.Trim());

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;

        if (!b1)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        if (!b2)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }


        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    private int _ckCount;   //出库
    private int _csCount;   //出售
    private int _shCount;   //损坏
    private int _zsCount;   //赠送
    private double _jkCount;   //解款
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (selDept.SelectedValue != "")
        {
            //用于统计的部门列
            e.Row.Cells[0].Visible = false;


            if (e.Row.RowType == DataControlRowType.DataRow)    // 数据行
            {
                _ckCount = _ckCount + Convert.ToInt32(e.Row.Cells[4].Text.Trim());
                _csCount = _csCount + Convert.ToInt32(e.Row.Cells[5].Text.Trim());
                _shCount = _shCount + Convert.ToInt32(e.Row.Cells[6].Text.Trim());
                _zsCount = _zsCount + Convert.ToInt32(e.Row.Cells[7].Text.Trim());
                _jkCount = _jkCount + Convert.ToDouble(e.Row.Cells[8].Text.Trim());
            }

            if (e.Row.RowType == DataControlRowType.Footer)    //页脚
            {
                e.Row.Cells[4].Text = _ckCount.ToString();
                e.Row.Cells[5].Text = _csCount.ToString();
                e.Row.Cells[6].Text = _shCount.ToString();
                e.Row.Cells[7].Text = _zsCount.ToString();
                e.Row.Cells[8].Text = _jkCount.ToString();
            }
        }
        else
        {
            e.Row.Cells[0].Visible = false; //隐藏用于排序的DEPARTNO列
            gvResult.ShowFooter = false;
        }
    }
}
