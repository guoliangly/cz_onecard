﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDO.Financial;
using Master;
using Common;
using TDO.BalanceParameter;

// 商户转账日报
public partial class ASP_FI_BUS_REPORTFiApproval : Master.ExportMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期

            txtFromDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

            UserCardHelper.resetData(gvResult, null);
            return;
        }

    }

    private double totalCharges = 0;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow ||
           e.Row.RowType == DataControlRowType.Header ||
           e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Visible = false;
        }
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[4].Text = totalCharges.ToString("n");
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    private bool checkEndDate()
    {
        TP_DEALTIMETDO tdoTP_DEALTIMEIn = new TP_DEALTIMETDO();
        TP_DEALTIMETDO[] tdoTP_DEALTIMEOutArr = (TP_DEALTIMETDO[])tm.selByPKArr(context, tdoTP_DEALTIMEIn, typeof(TP_DEALTIMETDO), null, "DEALTIME", null);
        if(tdoTP_DEALTIMEOutArr.Length == 0)
        {
            context.AddError("没有找到有效的结算处理时间");
            return false;
        }
        else
        {
            DateTime dealDate = tdoTP_DEALTIMEOutArr[0].DEALDATE.Date;
            DateTime endDate = DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null);
            if (endDate.CompareTo(dealDate) >= 0)
            {
                context.AddError("结束日期过大，未结算");
                return false;
            }
        }
        return true;
    } 

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b  = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        if (!checkEndDate()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TD_BUS_REPORTFiApproval";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = selApproval.SelectedValue;

        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);

        hidNo.Value = pdo.var9;

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    protected void btnExportPayFile_Click(object sender, EventArgs e)
    {
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        submitApproval("", "D004P03001: 审核成功");
    }

    // 调用信息更改审核存储过程

    private void submitApproval(string stateCode, string okMsgCode)
    {

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);

        //插入临时表
        context.DBOpen("Insert");
        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("chkCust");
            if (cb != null && cb.Checked)
            {
                ++count;

                String id = gvr.Cells[1].Text;
                String state = selApproval2.SelectedValue;
                context.ExecuteNonQuery("insert into TMP_COMMON_NEW (F0,F14,SESSIONID) "
               + " values('" +id+ "','"+state+"','" + Session.SessionID + "')");
            }
        }
        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A001002213");
            return;
        }

        // 调用信息更改审核存储过程
        context.SPOpen();
        context.AddField("P_SESSIONID").Value = Session.SessionID;

        bool ok = context.ExecuteSP("SP_FI_BusDailyReportFiApproval");

        if (ok) AddMessage(okMsgCode);

        //清空临时表
        CommonHelper.clearNewTempTable(context, Session.SessionID);
    }
    public String getDataKeys2(String keysname)
    {
        return gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();
    }

    public String getDataKeys2(String keysname, int rowIndex)
    {
        return gvResult.DataKeys[rowIndex][keysname].ToString();
    }
}
