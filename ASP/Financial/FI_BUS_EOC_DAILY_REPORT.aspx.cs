﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDO.Financial;
using Master;
using Common;
using TDO.BalanceParameter;

// 商户转账日报
public partial class ASP_FI_BUS_EOC_DAILY_REPORT : Master.ExportMaster
{
    // 页面装载
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期

            txtFromDate.Text = DateTime.Today.AddDays(-2).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-2).ToString("yyyyMMdd");

            UserCardHelper.resetData(gvResult, null);
            return;
        }

    }

    private double totalCharges = 0;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    //    if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
    //    }
    //    else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
    //    {
    //        e.Row.Cells[0].Text = "总计";
    //        e.Row.Cells[4].Text = totalCharges.ToString("n");
    //    }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    private bool checkEndDate()
    {
        TP_DEALTIMETDO tdoTP_DEALTIMEIn = new TP_DEALTIMETDO();
        TP_DEALTIMETDO[] tdoTP_DEALTIMEOutArr = (TP_DEALTIMETDO[])tm.selByPKArr(context, tdoTP_DEALTIMEIn, typeof(TP_DEALTIMETDO), null, "DEALTIME", null);
        if(tdoTP_DEALTIMEOutArr.Length == 0)
        {
            context.AddError("没有找到有效的结算处理时间");
            return false;
        }
        else
        {
            DateTime dealDate = tdoTP_DEALTIMEOutArr[0].DEALDATE.Date;
            DateTime endDate = DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null);
            if (endDate.CompareTo(dealDate) >= 0)
            {
                context.AddError("结束日期过大，未结算");
                return false;
            }
        }
        return true;
    } 

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b  = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        if (!checkEndDate()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "TD_BUS_EOC_DAILY_REPORT";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var3 = selTrans.SelectedValue;

        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);

        hidNo.Value = pdo.var9;

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    protected void btnExportPayFile_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            if (selTrans.SelectedValue == "0")
		    		{
		        		ExportToFile(gvResult, "农行转账支付文件_" + hidNo.Value + ".txt");
		        }
		        
		        if (selTrans.SelectedValue == "4")
		        {
		        		ExportToFile(gvResult, "交行转账支付文件_" + hidNo.Value + ".plz");
		        }
		        else
		        {
		            context.AddError("只能导出农行、交行转账文件");
		        }
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    protected void ExportToFile(GridView gv, string filename)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "GB2312";
        Response.ContentType = "application/vnd.text"; Response.ContentType = "text/plain";
        Response.AddHeader("Content-disposition", "attachment; filename=" + Server.UrlEncode(filename));
        Response.ContentEncoding=System.Text.Encoding.GetEncoding("GB2312");

    		if (selTrans.SelectedValue == "0")
    		{
		        foreach (GridViewRow gvr in gv.Rows)
		        {
		            Response.Write("2@"); // 业务种类
		            Response.Write(DateTime.Now.ToString("yyyy-MM-dd")); // 导出日期
		            Response.Write("@553301040004064@"); // 苏信农行帐号
		            string money = gvr.Cells[4].Text.Trim(); // 以元为单位
		            Response.Write(money.PadLeft(11));
		            string temp = gvr.Cells[0].Text.Trim();
		            Response.Write("@" + temp + "".PadRight(50-Validation.strLen(temp)));
		            Response.Write("@" + "".PadRight(24));
		            temp = gvr.Cells[3].Text.Trim().PadRight(34);
		            Response.Write("@" + temp);
		            temp = gvr.Cells[2].Text.Trim();
		            Response.Write("@" + temp + "".PadRight(50 - Validation.strLen(temp)) + "@");
		            temp = gvr.Cells[5].Text.Trim(); // 以元为单位
		            Response.Write(temp.PadLeft(18));
		            temp = gvr.Cells[1].Text.Trim();
		            Response.Write("@" + temp + "".PadRight(20 - Validation.strLen(temp)));
		            Response.Write("\r\n");
		        }
    		}
    		
    		if (selTrans.SelectedValue == "4")
				{
    				decimal totalmoney = 0;
		        foreach (GridViewRow gvr in gv.Rows)
		        {
		        	totalmoney = totalmoney + Convert.ToDecimal(gvr.Cells[4].Text) * 100;
		        }
    				Response.Write("<?xml version=\"1.0\" encoding=\"GBK\"?>");
		        Response.Write("\r\n");
    				Response.Write("<batchPayFile>");
		        Response.Write("\r\n");
    				Response.Write("<summary date=\"" + txtToDate.Text.Trim() + "\" currencyType=\"CNY\" sumMoney=\"" + Convert.ToInt32(totalmoney) + "\" sumRecords=\"" + gv.Rows.Count + "\"/>");
		        Response.Write("\r\n");
    				Response.Write("<payList>");
		        Response.Write("\r\n");
		        
		        int irows;
		        foreach (GridViewRow gvr in gv.Rows)
		        {
		        		irows = gvr.RowIndex + 1;
								if (gvr.Cells[1].Text.Trim().Substring(0,2) == "交")
		            {
		            		Response.Write("<record date=\"" + txtToDate.Text.Trim() + "\" currencyType=\"CNY\" orderNo=\"" + irows + "\" erpNo=\"\" payAccNo=\"325661000018010179037\" payAccNme=\"苏州市城市信息化建设有限公司（充值户）\" payOpenBank=\"交通银行苏州吴中支行\" isOtherBank=\"0\" isSameCity=\"1\" recAccNo=\"" + gvr.Cells[3].Text.Trim() + "\" recAccNme=\"" + gvr.Cells[2].Text.Trim() + "\" recOpenBank=\"" + gvr.Cells[0].Text.Trim() + "\" money=\"" + Convert.ToInt32(Convert.ToDecimal(gvr.Cells[4].Text) * 100) + "\" usage=\"" + gvr.Cells[1].Text.Trim() + ", " + gvr.Cells[5].Text.Trim() + "\" comment=\"\" preflg=\"0\" predate=\"\"/>");
		            }
		            else
		            {
		            		Response.Write("<record date=\"" + txtToDate.Text.Trim() + "\" currencyType=\"CNY\" orderNo=\"" + irows + "\" erpNo=\"\" payAccNo=\"325661000018010179037\" payAccNme=\"苏州市城市信息化建设有限公司（充值户）\" payOpenBank=\"交通银行苏州吴中支行\" isOtherBank=\"1\" isSameCity=\"1\" recAccNo=\"" + gvr.Cells[3].Text.Trim() + "\" recAccNme=\"" + gvr.Cells[2].Text.Trim() + "\" recOpenBank=\"" + gvr.Cells[0].Text.Trim() + "\" money=\"" + Convert.ToInt32(Convert.ToDecimal(gvr.Cells[4].Text) * 100) + "\" usage=\"" + gvr.Cells[1].Text.Trim() + ", " + gvr.Cells[5].Text.Trim() + "\" comment=\"\" preflg=\"0\" predate=\"\"/>");
		            }
		            Response.Write("\r\n");
		        }
    				Response.Write("</payList>");
		        Response.Write("\r\n");
    				Response.Write("</batchPayFile>");
		        Response.Write("\r\n");
				}

        Response.Flush();
        Response.End();
    }

    

}
