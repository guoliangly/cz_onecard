﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_SpeAdjustAccApproval.aspx.cs" Inherits="ASP_Financial_FI_SpeAdjustAccApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>特殊调帐审核</title>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
   
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">财务报表->特殊调帐审核</div>
     <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <ContentTemplate>
         
         <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
         
          
         <div class="con">
                <div class="card">查询</div>
               <div class="kuang5">
               <table border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                   <td width="10%"> <div align="right">审核状态:</div></td>
                   <td width="15%">
                        <asp:DropDownList ID="selCheckType" CssClass="input" runat="server">
                                        <asp:ListItem Text="0:待审核" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1:审核通过" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2:审核作废" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                   </td>
                    <td width="10%"> <div align="right"></div></td>
                   <td width="15%">
                       <asp:Button ID="btnQuery" OnClick="btnQuery_Click" runat="server" CssClass="button1"
                                        Text="查询"></asp:Button>
                   </td>
                     <td width="10%"> <div align="right"></div></td>
                   <td width="15%">
                   </td>
                   <td width="25%">
                   </td>
                </tr>
               </table>
               
             </div>
          <div class="jieguo">待审核调帐信息</div>
          <div class="kuang5">
            <div id="gdtb" style="height:400px">
             
                <asp:GridView ID="gvResult" runat="server"
                    CssClass="tab1"
                    Width ="95%"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="10"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="Left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="False" 
                    
                    OnPageIndexChanging="gvResult_Page"
                    OnRowDataBound="gvResult_RowDataBound"
                    >
                    <Columns>
                    
                          <asp:TemplateField>
                            <HeaderTemplate>
                              <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="true" OnCheckedChanged="CheckAll" />
                            </HeaderTemplate>
                            <ItemTemplate>
                              <asp:CheckBox ID="ItemCheckBox" runat="server"/>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:BoundField DataField="BALUNITNO"       HeaderText="商户代码"/>
                          <asp:BoundField DataField="BALUNIT"   HeaderText="商户名称"/> 
                          <asp:BoundField DataField="STATECODE" HeaderText="审核状态" />
                          <asp:BoundField DataField="BANK"   HeaderText="开户行"/>
                          <asp:BoundField DataField="BANKACCNO"    HeaderText="银行帐号"/>
                          <asp:BoundField DataField="COMSCHEME"  HeaderText="佣金率"/>
                          <asp:BoundField DataField="READJUSTMONEY"  HeaderText="实际调帐金额"/>                 
                          <asp:BoundField DataField="BEGINDATE"     HeaderText="调帐帐期"/>
                          <asp:BoundField DataField="ENDDATE"     HeaderText="调帐帐期"/>
                          <asp:BoundField DataField="TRADEID" HeaderText="业务流水号" />
                   </Columns>
                   
                   <EmptyDataTemplate>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                         <tr class="tabbt">
                              <td><input type="checkbox" /></td>
                              <td>商户代码</td>
                              <td>商户名称</td>
                              <td>审核状态</td>
                              <td>开户行</td>
                              <td>银行帐号</td>
                              <td>佣金率</td>
                              <td>实际调帐金额</td>
                              <td>调帐帐期</td>
                         </tr>
                      </table>
                   </EmptyDataTemplate>
                  </asp:GridView>
              
            </div>
          </div>
         
          
          </div>
        <div class="btns">
          <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
            <td><asp:Button ID="btnPass" runat="server" Text="通过" CssClass="button1" OnClick="btnPass_Click" /></td>
              <td><asp:Button ID="btnCancel" runat="server" Text="作废" CssClass="button1" OnClick="btnCancel_Click" /></td>
            </tr>
          </table>

        </div>
        
         
         </ContentTemplate>
       </asp:UpdatePanel>

    
 
    </form>
</body>
</html>
