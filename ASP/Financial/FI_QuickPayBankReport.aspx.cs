﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using PDO.Financial;
using Master;

/**************************************
 * chenwentao 20141013
 * 闪付卡银行到账日报
 * ************************************/

public partial class ASP_Financial_FI_QuickPayBankReport : Master.ExportMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtAccountFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtAccountToDate.Text = DateTime.Today.ToString("yyyyMMdd");
        }
    }

    /// <summary>
    /// 取数据并绑定到GridView
    /// </summary>
    private void BindData()
    {
        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "FI_QuickPayBankReport";
        pdo.var1 = ddlSelBank.SelectedValue;
        pdo.var2 = txtAccountFromDate.Text;
        pdo.var3 = txtAccountToDate.Text;
        StoreProScene StorePro = new StoreProScene();
        DataTable dt = StorePro.Execute(context, pdo);
        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddMessage("查询结果为空");
        }
        gvResult.DataSource = dt;
        gvResult.DataBind();
    }

    #endregion

    #region Event

    /// <summary>
    /// 查询按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError())
            return;
        BindData();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    #endregion

    #region Function

    /// <summary>
    /// 验证查询条件
    /// </summary>
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtAccountFromDate);
        bool b2 = Validation.isEmpty(txtAccountToDate);
        DateTime? AccountFromDate = null, AccountToDate = null;
        if (b1 || b2)
        {
            context.AddError("日期必须填写");
        }
        else
        {
            if (!b1)
            {
                AccountFromDate = valid.beDate(txtAccountFromDate, "结算开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                AccountToDate = valid.beDate(txtAccountToDate, "结算结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (AccountFromDate != null && AccountToDate != null)
        {
            valid.check(AccountFromDate.Value.CompareTo(AccountToDate.Value) <= 0, "结算开始日期不能大于结束日期");
        }
    }

    #endregion

}