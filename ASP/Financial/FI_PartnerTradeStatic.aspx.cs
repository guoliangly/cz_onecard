﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDO.Financial;
using Master;
using Common;
using TDO.BalanceChannel;
using TM;
using TDO.BalanceParameter;

public partial class ASP_Financial_FI_PartnerTradeStatic : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期

           
            txtFromDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");


            //从行业编码表(TD_M_CALLINGNO)中读取数据，放入查询输入行业名称下拉列表中
            TMTableModule tmTMTableModule = new TMTableModule();

            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211");

            SelectBoxFillWithCode1(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);
            //去除公交行业
            selCalling.Items.Remove(selCalling.Items[1]);
            selCalling.SelectedIndex = 0;
            selCorp.Items.Add(new ListItem("---请选择---", ""));
        }
    }

    public void SelectBoxFillWithCode1(ListItemCollection Items, DDOBase[] ddoDDOBaseArr, String textName, String valueName, Boolean emptFill)
    {
        Items.Clear();

        if (emptFill)
            Items.Add(new ListItem("---请选择---", ""));

        foreach (DDOBase ddoDDOBase in ddoDDOBaseArr)
        {
            Items.Add(new ListItem(ddoDDOBase.GetString(textName), ddoDDOBase.GetString(valueName)));
        }
    }

    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的行业名称后,初始化单位名称
        selCorp.Items.Clear();
        InitCorp(selCalling, selCorp, "TD_M_CORPCALLUSAGE");

    }
    protected void InitCorp(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        // 从单位编码表(TD_M_CORP)中读取数据，放入增加,修改区域中单位信息下拉列表中

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = origindwls.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        SelectBoxFillWithCode(extdwls.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }
    public void SelectBoxFillWithCode(ListItemCollection Items, DDOBase[] ddoDDOBaseArr, String textName, String valueName, Boolean emptFill)
    {
        Items.Clear();

        if (emptFill)
            Items.Add(new ListItem("---请选择---", ""));

        foreach (DDOBase ddoDDOBase in ddoDDOBaseArr)
        {
            Items.Add(new ListItem(ddoDDOBase.GetString(textName), ddoDDOBase.GetString(valueName)));
        }
    }

    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        //GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 2);
    }

    private bool checkEndDate()
    {
        TP_DEALTIMETDO tdoTP_DEALTIMEIn = new TP_DEALTIMETDO();
        TP_DEALTIMETDO[] tdoTP_DEALTIMEOutArr = (TP_DEALTIMETDO[])tm.selByPKArr(context, tdoTP_DEALTIMEIn, typeof(TP_DEALTIMETDO), null, "DEALTIME", null);
        if (tdoTP_DEALTIMEOutArr.Length == 0)
        {
            context.AddError("没有找到有效的结算处理时间");
            return false;
        }
        else
        {
            DateTime dealDate = tdoTP_DEALTIMEOutArr[0].DEALDATE.Date;
            DateTime endDate = DateTime.ParseExact(txtToDate.Text.Trim(), "yyyyMMdd", null);
            if (endDate.CompareTo(dealDate) >= 0)
            {
                context.AddError("结束日期过大，未结算");
                return false;
            }
        }
        return true;
    } 
 
    private double totalCharges = 0;


    string _tempvalue="";
    int _temprowspan = 1;
    TableCell _temptablecell=null;
    //合并相同行和列
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
         {
           //合并列
             if (e.Row.Cells[0].Text == _tempvalue)
             {
                 _temprowspan++;
                 e.Row.Cells.Remove(e.Row.Cells[0]);
             }
             else
             {
                 if (_temprowspan != 1)
                     _temptablecell.RowSpan = _temprowspan;
 
                 _tempvalue = e.Row.Cells[0].Text;
                 _temptablecell = e.Row.Cells[0];
                 _temprowspan = 1;
             }
           ////合并行
           //  if (e.Row.Cells[0].Text == _tempvalue)
           //  {
           //      _temprowspan++;
           //      e.Row.Cells.Remove(e.Row.Cells[0]);
           //  }
           //  else
           //  {
           //      if (_temprowspan != 1)
           //          _temptablecell.ColumnSpan = _temprowspan;

           //      _tempvalue = e.Row.Cells[1].Text;
           //      _temptablecell = e.Row.Cells[1];
           //      _temprowspan = 1;
           //  }
         }

    }
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }
    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);

        validate();
        if (context.hasError()) return;
        DataTable data = null;
        if (selStaticType.SelectedValue == "1")//行业消费按月
        {
            data = GetCallingByMonth(txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selCalling.Items[selCalling.SelectedIndex].Text);
        }
        else if (selStaticType.SelectedValue == "2")//行业消费按日
        {
            data = GetCallingByDay(txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selCalling.Items[selCalling.SelectedIndex].Text);
        }
        else if (selStaticType.SelectedValue == "3")//单位消费按月
        {

            data = GetCorpByMonth(txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selCalling.Items[selCalling.SelectedIndex].Text,selCorp.Items[selCorp.SelectedIndex].Text);
        }
        else if (selStaticType.SelectedValue == "4")//单位消费按日
        {

            data = GetCorpByDay(txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selCalling.Items[selCalling.SelectedIndex].Text, selCorp.Items[selCorp.SelectedIndex].Text);
        }
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
            btnPrint.Enabled = false;
        }
        else
        {
            btnPrint.Enabled = true;
        }

        totalCharges = 0;
        UserCardHelper.resetData(gvResult, data);

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }

    }

    protected void btnExportPayFile_Click(object sender, EventArgs e)
    {
        //ExportToFile(gvResult, "转账_" + hidNo.Value + ".txt");
    }
    protected void ExportToFile(GridView gv, string filename)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "GB2312";
        Response.ContentType = "application/vnd.text"; Response.ContentType = "text/plain";
        Response.AddHeader("Content-disposition", "attachment; filename=" + Server.UrlEncode(filename));
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");

        foreach (GridViewRow gvr in gv.Rows)
        {
            string temp = gvr.Cells[1].Text.Trim();
            Response.Write(temp.PadRight(20)); // 银行账号
            string money = "" + Convert.ToInt32(Convert.ToDecimal(gvr.Cells[3].Text) * 100);
            Response.Write(money.PadLeft(16)); // 金额
            temp = gvr.Cells[2].Text.Trim();
            Response.Write(temp + "".PadRight(30 - Validation.strLen(temp)));// 卡号
            Response.Write("\r\n");
        }

        Response.Flush();
        Response.End();
    }

    //行业消费按月
    public DataTable GetCallingByMonth(string begindate, string enddate,string calling)
    {
        if (calling == "---请选择---")
            calling = "";
        TMTableModule tmTMTableModule = new TMTableModule();
        //先把所有行业查找出来
        string strSql = "select calling from td_m_callingno where callingno<>'01' and isopen='1' and (''='{0}' or '{0}' is null or calling='{0}') order by callingno";
        strSql = string.Format(strSql, calling);
        DataTable dtDataCalling = tm.selByPKDataTable(context, strSql, 0);

        //消费统计表按行业统计,按月(只能统计最近6个月)
        strSql = "select  b.calling calling,substr(a.tradedate,1,6) tradedate,sum(trademoney)/100.0 totalmoney ";
        strSql = strSql + " from tq_trade_right a,td_m_callingno b	where a.callingno=b.callingno			";
        strSql = strSql + " and a.tradedate between '{0}'||'01' and '{1}'||'31' and  (''='{2}' or '{2}' is null or calling='{2}')  group by b.calling,substr(a.tradedate,1,6)";
        strSql = string.Format(strSql, begindate.Substring(0, 6), enddate.Substring(0, 6),calling);
        DataTable dtData = tm.selByPKDataTable(context, strSql, 0);

        DataTable dt = new DataTable();
        dt.Columns.Add("行业名称");
        DateTime bd = DateTime.ParseExact(begindate.Substring(0, 6), "yyyyMM", null);
        DateTime ed = DateTime.ParseExact(enddate.Substring(0, 6), "yyyyMM", null);
        DateTime bdtemp = bd;
        ArrayList al1 = new ArrayList();
        while (bdtemp <= ed)
        {
            dt.Columns.Add(bdtemp.ToString("yyyyMM"));
            bdtemp = bdtemp.AddMonths(1);
        }
        dt.Columns.Add("总计", System.Type.GetType("System.Decimal"));
        foreach (DataRow drCalling in dtDataCalling.Rows)
        {

            decimal callingtotalmoney = 0;
            ArrayList al = new ArrayList();
            al.Add(drCalling["calling"].ToString());//行业
            bdtemp = bd;
            while (bdtemp <= ed)
            {
                DataRow[] drs = dtData.Select("calling='" + drCalling["calling"].ToString() + "' and  tradedate='" + bdtemp.ToString("yyyyMM") + "'");
                if (drs.Length > 0)
                {
                    callingtotalmoney += Convert.ToDecimal(drs[0]["totalmoney"].ToString());
                    al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
                }
                else
                {
                    al.Add("0.00");
                }
                bdtemp = bdtemp.AddMonths(1);
            }
            al.Add(Convert.ToDecimal(callingtotalmoney.ToString("0.00")));
            dt.Rows.Add(al.ToArray());
        }

        //按总计金额从大到小排序
        DataView dv = dt.DefaultView;
        dv.Sort = "总计 desc";
        dt = dv.ToTable();

        //合计
        ArrayList al2 = new ArrayList();
        al2.Add("合计");
        for (int j = 1; j < dt.Columns.Count; j++)
        {
            decimal hejitotalmoney = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                hejitotalmoney += Convert.ToDecimal(dt.Rows[i][j].ToString());
            }
            al2.Add(hejitotalmoney.ToString("0.00"));
        }
        dt.Rows.Add(al2.ToArray());
        return dt;
    }

    //行业消费按日
    public DataTable GetCallingByDay(string begindate, string enddate, string calling)
    {
        if (calling == "---请选择---")
            calling = "";
        TMTableModule tmTMTableModule = new TMTableModule();
        //先把所有行业查找出来
        string strSql = "select calling from td_m_callingno where callingno<>'01' and isopen='1' and (''='{0}' or '{0}' is null or calling='{0}') order by callingno";
        strSql = string.Format(strSql, calling);
        DataTable dtDataCalling = tm.selByPKDataTable(context, strSql, 0);

        //消费统计表按行业统计,按月(先只能统计最近6个月)
        strSql = "select  b.calling calling,a.tradedate tradedate,sum(trademoney)/100.0 totalmoney ";
        strSql = strSql + " from tq_trade_right a,td_m_callingno b	where a.callingno=b.callingno			";
        strSql = strSql + " and a.tradedate between '{0}' and '{1}' and  (''='{2}' or '{2}' is null or calling='{2}')  group by b.calling,a.tradedate";
        strSql = string.Format(strSql, begindate, enddate, calling);
        DataTable dtData = tm.selByPKDataTable(context, strSql, 0);

        DataTable dt = new DataTable();
        dt.Columns.Add("行业名称");
        DateTime bd = DateTime.ParseExact(begindate, "yyyyMMdd", null);
        DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
        DateTime bdtemp = bd;
        ArrayList al1 = new ArrayList();
        while (bdtemp <= ed)
        {
            dt.Columns.Add(bdtemp.ToString("yyyyMMdd"));
            bdtemp = bdtemp.AddDays(1);
        }
        dt.Columns.Add("总计",System.Type.GetType("System.Decimal"));
        foreach (DataRow drCalling in dtDataCalling.Rows)
        {

            decimal callingtotalmoney = 0;
            ArrayList al = new ArrayList();
            al.Add(drCalling["calling"].ToString());//行业
            bdtemp = bd;
            while (bdtemp <= ed)
            {
                DataRow[] drs = dtData.Select("calling='" + drCalling["calling"].ToString() + "' and  tradedate='" + bdtemp.ToString("yyyyMMdd") + "'");
                if (drs.Length > 0)
                {
                    callingtotalmoney += Convert.ToDecimal(drs[0]["totalmoney"].ToString());
                    al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
                }
                else
                {
                    al.Add("0.00");
                }
                bdtemp = bdtemp.AddDays(1);
            }
            al.Add(Convert.ToDecimal(callingtotalmoney.ToString("0.00")));
            dt.Rows.Add(al.ToArray());
        }
        //按总计金额从大到小排序
        DataView dv = dt.DefaultView;
        dv.Sort = "总计 desc";
        dt = dv.ToTable();
        //合计
        ArrayList al2 = new ArrayList();
        al2.Add("合计");
        for (int j = 1; j < dt.Columns.Count; j++)
        {
            decimal hejitotalmoney = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                hejitotalmoney += Convert.ToDecimal(dt.Rows[i][j].ToString());
            }
            al2.Add(hejitotalmoney.ToString("0.00"));
        }
        dt.Rows.Add(al2.ToArray());
        return dt;
    }
    
    //单位消费按月
    public DataTable GetCorpByMonth(string begindate, string enddate, string calling, string corp)
    {
        if (calling == "---请选择---")
            calling = "";
        if (corp == "---请选择---")
            corp = "";
        TMTableModule tmTMTableModule = new TMTableModule();
        //先把所有行业和单位查找出来
        string strSql = " select b.calling,a.corp from td_m_corp a ,td_m_callingno b　where a.callingno=b.callingno and a.callingno<>'01' and  (''='{1}' or '{1}' is null or a.corp='{1}')  and (''='{0}' or '{0}' is null or b.calling='{0}')  order by a.callingno ";
        //string strSql = "select calling from td_m_callingno where callingno<>'01' and isopen='1' and (''='{0}' or '{0}' is null or calling='{0}') order by callingno";
        strSql = string.Format(strSql, calling, corp);
        DataTable dtDataCallingAndCorp = tm.selByPKDataTable(context, strSql, 0);


        strSql = "select  b.calling,c.corp,substr(a.tradedate,1,6) tradedate,sum(trademoney)/100.0 totalmoney 	";
        strSql = strSql + " from tq_trade_right_nonbus a,td_m_callingno b,td_m_corp c	where a.callingno=b.callingno 		";
        strSql = strSql + " and a.corpno=c.corpno and b.callingno=c.callingno  and  (''='{3}' or '{3}' is null or c.corp='{3}')  and (''='{2}' or '{2}' is null or b.calling='{2}')	and  a.tradedate between '{0}'||'01' and '{1}'||'31'	";
        strSql = strSql + " group by b.calling,c.corp,substr(a.tradedate,1,6) ";

        //消费统计表按行业统计,按月(先只能统计最近6个月)
        //strSql = "select  b.calling calling,substr(a.tradedate,1,6) tradedate,sum(trademoney)/100.0 totalmoney ";
        //strSql = strSql + " from tq_trade_right a,td_m_callingno b	where a.callingno=b.callingno			";
        //strSql = strSql + " and substr(a.tradedate,1,6) between '{0}' and '{1}' and  (''='{2}' or '{2}' is null or calling='{2}')  group by b.calling,substr(a.tradedate,1,6)";
        strSql = string.Format(strSql, begindate.Substring(0, 6), enddate.Substring(0, 6), calling, corp);
        DataTable dtData = tm.selByPKDataTable(context, strSql, 0);

        DataTable dt = new DataTable();
        dt.Columns.Add("行业名称");
        dt.Columns.Add("单位名称");
        DateTime bd = DateTime.ParseExact(begindate.Substring(0, 6), "yyyyMM", null);
        DateTime ed = DateTime.ParseExact(enddate.Substring(0, 6), "yyyyMM", null);
        DateTime bdtemp = bd;
        ArrayList al1 = new ArrayList();
        while (bdtemp <= ed)
        {
            dt.Columns.Add(bdtemp.ToString("yyyyMM"));
            bdtemp = bdtemp.AddMonths(1);
        }
        dt.Columns.Add("总计", System.Type.GetType("System.Decimal"));
        foreach (DataRow drCalling in dtDataCallingAndCorp.Rows)
        {
            decimal callingtotalmoney = 0;
            ArrayList al = new ArrayList();
            al.Add(drCalling["calling"].ToString());//行业
            al.Add(drCalling["corp"].ToString());//单位
            bdtemp = bd;
            while (bdtemp <= ed)
            {
                DataRow[] drs = dtData.Select("calling='" + drCalling["calling"].ToString() + "' and corp='" + drCalling["corp"].ToString() + "' and  tradedate='" + bdtemp.ToString("yyyyMM") + "'");
                if (drs.Length > 0)
                {
                    callingtotalmoney += Convert.ToDecimal(drs[0]["totalmoney"].ToString());
                    al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
                }
                else
                {
                    al.Add("0.00");
                }
                bdtemp = bdtemp.AddMonths(1);
            }
            al.Add(Convert.ToDecimal(callingtotalmoney.ToString("0.00")));
            
            dt.Rows.Add(al.ToArray());
        }
        //按总计金额从大到小排序
        DataView dv = dt.DefaultView;
        dv.Sort = "总计 desc";
        dt = dv.ToTable();
        //合计
        ArrayList al2 = new ArrayList();
        al2.Add("合计");
        al2.Add("");
        for (int j = 2; j < dt.Columns.Count; j++)
        {
            decimal hejitotalmoney = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                hejitotalmoney += Convert.ToDecimal(dt.Rows[i][j].ToString());
            }
            al2.Add(hejitotalmoney.ToString("0.00"));
        }
        dt.Rows.Add(al2.ToArray());

        return dt;
    }

    //单位消费按日
    public DataTable GetCorpByDay(string begindate, string enddate, string calling, string corp)
    {
        if (calling == "---请选择---")
            calling = "";
        if (corp == "---请选择---")
            corp = "";
        TMTableModule tmTMTableModule = new TMTableModule();
        //先把所有行业和单位查找出来
        string strSql = " select b.calling,a.corp from td_m_corp a ,td_m_callingno b　where a.callingno=b.callingno and a.callingno<>'01' and  (''='{1}' or '{1}' is null or a.corp='{1}')  and (''='{0}' or '{0}' is null or b.calling='{0}')  order by a.callingno ";
        //string strSql = "select calling from td_m_callingno where callingno<>'01' and isopen='1' and (''='{0}' or '{0}' is null or calling='{0}') order by callingno";
        strSql = string.Format(strSql, calling, corp);
        DataTable dtDataCallingAndCorp = tm.selByPKDataTable(context, strSql, 0);


        strSql = "select  b.calling,c.corp,a.tradedate tradedate,sum(trademoney)/100.0 totalmoney 	";
        strSql = strSql + " from tq_trade_right_nonbus a,td_m_callingno b,td_m_corp c	where a.callingno=b.callingno 		";
        strSql = strSql + " and a.corpno=c.corpno and b.callingno=c.callingno  and  (''='{3}' or '{3}' is null or c.corp='{3}')  and (''='{2}' or '{2}' is null or b.calling='{2}')	and a.tradedate between '{0}' and '{1}'	";
        strSql = strSql + " group by b.calling,c.corp,a.tradedate ";

        //消费统计表按行业统计,按月(先只能统计最近6个月)
        //strSql = "select  b.calling calling,substr(a.tradedate,1,6) tradedate,sum(trademoney)/100.0 totalmoney ";
        //strSql = strSql + " from tq_trade_right a,td_m_callingno b	where a.callingno=b.callingno			";
        //strSql = strSql + " and substr(a.tradedate,1,6) between '{0}' and '{1}' and  (''='{2}' or '{2}' is null or calling='{2}')  group by b.calling,substr(a.tradedate,1,6)";
        strSql = string.Format(strSql, begindate, enddate, calling, corp);
        DataTable dtData = tm.selByPKDataTable(context, strSql, 0);

        DataTable dt = new DataTable();
        dt.Columns.Add("行业名称");
        dt.Columns.Add("单位名称");
        DateTime bd = DateTime.ParseExact(begindate, "yyyyMMdd", null);
        DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
        DateTime bdtemp = bd;
        ArrayList al1 = new ArrayList();
        while (bdtemp <= ed)
        {
            dt.Columns.Add(bdtemp.ToString("yyyyMMdd"));
            bdtemp = bdtemp.AddDays(1);
        }
        dt.Columns.Add("总计", System.Type.GetType("System.Decimal"));
        foreach (DataRow drCalling in dtDataCallingAndCorp.Rows)
        {
            decimal callingtotalmoney = 0;
            ArrayList al = new ArrayList();
            al.Add(drCalling["calling"].ToString());//行业
            al.Add(drCalling["corp"].ToString());//单位
            bdtemp = bd;
            while (bdtemp <= ed)
            {
                DataRow[] drs = dtData.Select("calling='" + drCalling["calling"].ToString() + "' and corp='" + drCalling["corp"].ToString() + "' and  tradedate='" + bdtemp.ToString("yyyyMMdd") + "'");
                if (drs.Length > 0)
                {
                    callingtotalmoney += Convert.ToDecimal(drs[0]["totalmoney"].ToString());
                    al.Add(Convert.ToDecimal(drs[0]["totalmoney"].ToString()).ToString("0.00"));
                }
                else
                {
                    al.Add("0.00");
                }
                bdtemp = bdtemp.AddDays(1);
            }
            al.Add(Convert.ToDecimal(callingtotalmoney.ToString("0.00")));
            dt.Rows.Add(al.ToArray());
        }
        //按总计金额从大到小排序
        DataView dv = dt.DefaultView;
        dv.Sort = "总计 desc";
        dt = dv.ToTable();
        //合计
        ArrayList al2 = new ArrayList();
        al2.Add("合计");
        al2.Add("");
        for (int j = 2; j < dt.Columns.Count; j++)
        {
            decimal hejitotalmoney = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                hejitotalmoney += Convert.ToDecimal(dt.Rows[i][j].ToString());
            }
            al2.Add(hejitotalmoney.ToString("0.00"));
        }
        dt.Rows.Add(al2.ToArray());
        return dt;
    }


}
