﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using PDO.Financial;
using Master;

public partial class ASP_Financial_FI_SpecialCardTransferReport : Master.ExportMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtStartDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtEndDate.Text = DateTime.Today.ToString("yyyyMMdd");
        }
    }

    #endregion

    #region Event

    /// <summary>
    /// 选择统计方式
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDayMonth_Changed(object sender, EventArgs e)
    {
        if (ddlDayMonth.SelectedValue == "Day")
        {
            tdTitle.InnerText = "特种卡转账日报表";
            tdStart.InnerText = "开始日期：";
            tdEnd.InnerText = "结束日期：";
            divSDate.Visible = true;
            divEDate.Visible = true;
            divSMonth.Visible = false;
            divEMonth.Visible = false;
            divFDay.Visible = true;
            divTDay.Visible = true;
            divFMonth.Visible = false;
            divTMonth.Visible = false;
            txtStartDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtEndDate.Text = DateTime.Today.ToString("yyyyMMdd");
            UserCardHelper.resetData(gvResult, null);
        }
        else
        {
            tdTitle.InnerText = "特种卡转账月报表";
            tdStart.InnerText = "开始月份：";
            tdEnd.InnerText = "结束月份：";
            divSDate.Visible = false;
            divEDate.Visible = false;
            divSMonth.Visible = true;
            divEMonth.Visible = true;
            divFDay.Visible = false;
            divTDay.Visible = false;
            divFMonth.Visible = true;
            divTMonth.Visible = true;
            txtStartMonth.Text = DateTime.Today.ToString("yyyyMM");
            txtEndMonth.Text = DateTime.Today.ToString("yyyyMM");
            txtStartDate.Text = DateTime.Today.ToString("yyyyMM");
            txtEndDate.Text = DateTime.Today.ToString("yyyyMM");
            UserCardHelper.resetData(gvResult, null);
        }
    }

    /// <summary>
    /// 查询数据绑定到gridview
    /// </summary>
    private void BindData()
    {
        DataTable data = new DataTable();
        if (ddlDayMonth.SelectedValue == "Day")
        {
            data = SPHelper.callQuery("SP_FI_SpecialCard", context, "SpecialTransfer_Day", txtStartDate.Text.Trim(),
            txtEndDate.Text.Trim());
        }
        else
        {
            data = SPHelper.callQuery("SP_FI_SpecialCard", context, "SpecialTransfer_Month", txtStartMonth.Text.Trim(),
            txtEndMonth.Text.Trim());
        }

        if (null == data || data.Rows.Count == 0)
        {
            context.AddMessage("查询结果为空");
            gvResult.DataSource = data;
            gvResult.DataBind();
            return;
        }

        gvResult.DataSource = data;
        gvResult.DataBind();
    }

    /// <summary>
    /// 查询事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        if (ddlDayMonth.SelectedValue == "Day")
        {
            ValidateDate();
        }
        else
        {
            ValidateMonth();
        }
        if (context.hasError())
            return;

        BindData();
    }

    private double totalMoney = 0;//合计总金额
    private int totalCount = 0;//合计总笔数
    /// <summary>
    /// 行绑定事件，处理页脚和要显示的数据格式
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ddlDayMonth.SelectedValue == "Day")
            {
                e.Row.Cells[0].Text = e.Row.Cells[0].Text.Substring(0, 4) + "年" + e.Row.Cells[0].Text.Substring(4, 2) + "月" + e.Row.Cells[0].Text.Substring(6, 2) + "日";
            }
            else
            {
                e.Row.Cells[0].Text = e.Row.Cells[0].Text.Substring(0, 4) + "年" + e.Row.Cells[0].Text.Substring(4, 2) + "月";
            }

            totalCount += Convert.ToInt32(GetTableCellValue(e.Row.Cells[2]));
            totalMoney += Convert.ToDouble(GetTableCellValue(e.Row.Cells[3]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].ColumnSpan = 2;
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[1].Visible = false;
          
            e.Row.Cells[2].Text = "" + totalCount.ToString();
            e.Row.Cells[3].Text = "" + totalMoney.ToString();
        }
    }

    /// <summary>
    /// 合并相同内容的行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 0);
    }

    /// <summary>
    /// 导出
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    #endregion

    #region Function

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    /// <summary>
    /// 日期输入验证处理
    /// </summary>
    private void ValidateDate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtStartDate);
        bool b2 = Validation.isEmpty(txtEndDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtStartDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtEndDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
    }

    /// <summary>
    /// 月份输入验证处理
    /// </summary>
    private void ValidateMonth()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtStartMonth);
        bool b2 = Validation.isEmpty(txtEndMonth);
        DateTime? fromMonth = null, toMonth = null;
        if (b1 || b2)
        {
            context.AddError("开始月份和结束月份必须填写");
        }
        else
        {
            if (!b1)
            {
                fromMonth = beDate(txtStartMonth, "开始月份范围起始值格式必须为yyyyMM");
            }
            if (!b2)
            {
                toMonth = beDate(txtEndMonth, "结束月份范围终止值格式必须为yyyyMM");
            }
        }

        if (fromMonth != null && toMonth != null)
        {
            valid.check(fromMonth.Value.CompareTo(toMonth.Value) <= 0, "开始月份不能大于结束月份");
        }
    }

    /// <summary>
    /// 验证指定格式日期
    /// </summary>
    /// <param name="tb"></param>
    /// <param name="errCode"></param>
    /// <returns></returns>
    private DateTime? beDate(TextBox tb, String errCode)
    {
        tb.Text = tb.Text.Trim();
        if (!Validation.isDate(tb.Text, "yyyyMM"))
        {
            context.AddError(errCode, tb);
            return null;
        }

        return DateTime.ParseExact(tb.Text, "yyyyMM", null); ;
    }

    #endregion

}