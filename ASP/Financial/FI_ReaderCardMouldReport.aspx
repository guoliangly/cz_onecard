﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_ReaderCardMouldReport.aspx.cs" Inherits="ASP_Financial_FI_ReaderCardMouldReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>充付器统计报表</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="tb">
            业务报表->充付器统计报表
        </div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:BulletedList ID="bulMsgShow" runat="server" />
                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>
                <div class="con">
                    <div class="card">
                        查询
                    </div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td>
                                    <div align="right">
                                        部门:
                                    </div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selDept" CssClass="inputmid" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                        充付器状态:
                                    </div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selCardMType" CssClass="inputmid" runat="server">
                                        <asp:ListItem Text="---请选择---" Value="" />
                                        <asp:ListItem Text="0:入库" Value="0" />
                                        <asp:ListItem Text="1:出库" Value="1" />
                                        <asp:ListItem Text="2:出售" Value="2" />
                                        <asp:ListItem Text="3:更换回收" Value="3" />
                                        <asp:ListItem Text="4:退货回收" Value="4" />
                                        <asp:ListItem Text="5:生成" Value="5" />
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询"
                                        OnClick="btnQuery_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table border="0" width="95%">
                        <tr>
                            <td align="left">
                                <div class="jieguo">
                                    查询结果
                                </div>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel"
                                    OnClick="btnExport_Click" />
                            </td>
                        </tr>
                    </table>
                    <div id="printarea" class="kuang5">
                        <div id="gdtbfix" style="height: 380px">
                            <table id="printReport" width="95%">
                                <tr align="center">
                                    <td style="font-size: 16px; font-weight: bold">充付器统计
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="gvResult" runat="server" Width="95%" CssClass="tab2" HeaderStyle-CssClass="tabbt"
                                FooterStyle-CssClass="tabcon" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel"
                                PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="true" ShowFooter="true" OnRowDataBound="gvResult_RowDataBound">
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
