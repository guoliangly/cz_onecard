﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using Master;
using PDO.Financial;

/***************************************************
 * create: chenwentao 2014-10-09
 * content: 公交闪付交易日报
****************************************************/

public partial class ASP_Financial_FI_BusQuickPayTradeReport : Master.ExportMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtAccountFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtAccountToDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtFileFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtFileToDate.Text = DateTime.Today.ToString("yyyyMMdd");
        }
    }

    /// <summary>
    /// 查询数据并绑定到gridview
    /// </summary>
    private void BindGird()
    {
        /*string SelSql = "select t1.balunitno,t1.balunit,t2.FILEDATE,t2.TOTALTIMES,t2.TOTALMONEY/100.0 TOTALMONEY,t2.CB_TIMES,t2.CB_MONEY/100.0 CB_MONEY,t2.CB_ERRORTIMES,t2.CB_ERRORMONEY/100.0 CB_ERRORMONEY,t2.OB_TIMES,t2.OB_MONEY/100.0 OB_MONEY,t2.OB_ERRORTIMES,t2.OB_ERRORMONEY/100.0 OB_ERRORMONEY,t2.DEALTIME,DECODE(t2.STATE,'0','未处理','1','已处理','2','处理异常') state from TF_TRADE_BALUNIT t1,TF_QP_BANK_BALANCE t2 where t1.balunitno=t2.BALUNITNO ";
        SelSql += string.Format(" and t2.DEALTIME between  to_date('{0}','yyyy-mm-dd hh24:mi:ss') and  to_date('{1}','yyyy-mm-dd hh24:mi:ss') and t2.FILEDATE between  '{2}' and  '{3}'", txtAccountFromDate.Text, txtAccountToDate.Text, txtFileFromDate.Text, txtFileToDate.Text);
        //闪付公交结算跳转页面跳转到此页面时候，显示出所选择的商户相关数据
        string id = GetQueryValue("id");
        if (!string.IsNullOrEmpty(id))
        {
            SelSql += string.Format(" and where t2.BALUNITNO='{0}'",id);
        }
        context.DBOpen("Select");
        DataTable SelData = new DataTable();
        SelData = context.ExecuteReader(SelSql);
        if (SelData == null || SelData.Rows.Count == 0)
        {
            context.AddMessage("查询结果为空");
        }*/

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "FI_BusQuickPayTradeReport";
        pdo.var1 = txtAccountFromDate.Text.Trim();
        pdo.var2 = txtAccountToDate.Text.Trim();
        pdo.var3 = txtFileFromDate.Text.Trim();
        pdo.var4 = txtFileToDate.Text.Trim();

        StoreProScene StorePro = new StoreProScene();
        DataTable dt = StorePro.Execute(context, pdo);
        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddMessage("查询结果为空");
        }

        string id = GetQueryValue("id");
        if (!string.IsNullOrEmpty(id))
        {

        }
        gvResult.DataSource = dt;
        gvResult.DataBind();
    }

    #endregion

    #region Event

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError())
            return;
        BindGird();
    }

    /// <summary>
    /// 用于GridView列的合计
    /// </summary>
    private double SumTotalMoney = 0;  //总金额
    private int SumTotalTimes = 0;    //总笔数

    private double SumCB_Money = 0;   //本行正常金额
    private int SumCB_Times = 0;     //本行正常笔数
    private double SumCB_ErrorMoney = 0; //本行异常金额
    private int SumCB_ErrorTimes = 0;    //本行异常笔数

    private double SumOB_Money = 0; //他行正常金额
    private int SumOB_Times = 0;   //他行正常笔数
    private double SumOB_ErrorMoney = 0; //他行异常金额
    private int SumOB_ErrorTimes = 0;   //他行异常笔数

    /// <summary>
    /// 合计gridview列的值，并显示到页脚
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        {
            //合计
            SumTotalMoney += Convert.ToDouble(GetTableCellValue(e.Row.Cells[3]));
            SumTotalTimes += Convert.ToInt32(GetTableCellValue(e.Row.Cells[4]));

            SumCB_Money += Convert.ToDouble(GetTableCellValue(e.Row.Cells[5]));
            SumCB_Times += Convert.ToInt32(GetTableCellValue(e.Row.Cells[6]));
            SumCB_ErrorMoney += Convert.ToDouble(GetTableCellValue(e.Row.Cells[7]));
            SumCB_ErrorTimes += Convert.ToInt32(GetTableCellValue(e.Row.Cells[8]));

            SumOB_Money += Convert.ToDouble(GetTableCellValue(e.Row.Cells[9]));
            SumOB_Times += Convert.ToInt32(GetTableCellValue(e.Row.Cells[10]));
            SumOB_ErrorMoney += Convert.ToDouble(GetTableCellValue(e.Row.Cells[11]));
            SumOB_ErrorTimes += Convert.ToInt32(GetTableCellValue(e.Row.Cells[12]));
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[0].ColumnSpan = 3;
            for (int index = 1; index <= 2; index++)
            {
                e.Row.Cells[index].Visible = false;
            }
            e.Row.Cells[0].Text = "合计";
            e.Row.Cells[3].Text = SumTotalMoney.ToString("n");//两位小数
            e.Row.Cells[4].Text = SumTotalTimes.ToString();

            e.Row.Cells[5].Text = SumCB_Money.ToString("n");
            e.Row.Cells[6].Text = SumCB_Times.ToString();
            e.Row.Cells[7].Text = SumCB_ErrorMoney.ToString("n");
            e.Row.Cells[8].Text = SumCB_ErrorTimes.ToString();

            e.Row.Cells[9].Text = SumOB_Money.ToString("n");
            e.Row.Cells[10].Text = SumOB_Times.ToString();
            e.Row.Cells[11].Text = SumOB_ErrorMoney.ToString("n");
            e.Row.Cells[12].Text = SumOB_ErrorTimes.ToString();
        }
    }

    /// <summary>
    /// 导出页面上查询出的数据到excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    #endregion

    #region Function

    /// <summary>
    /// 获取GridView单元格的值
    /// </summary>
    /// <param name="cell"></param>
    /// <returns></returns>
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }

    /// <summary>
    /// 获取查询字符串中的参数值
    /// </summary>
    private string GetQueryValue(string queryKey)
    {
        return Request.QueryString[queryKey];
    }

    /// <summary>
    /// 验证查询条件
    /// </summary>
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtAccountFromDate);
        bool b2 = Validation.isEmpty(txtAccountToDate);
        bool b3 = Validation.isEmpty(txtFileFromDate);
        bool b4 = Validation.isEmpty(txtFileToDate);
        DateTime? AccountFromDate = null, AccountToDate = null, FileFromDate = null, FileToDate=null;
        if (b1 || b2 || b3 || b4)
        {
            context.AddError("日期必须填写");
        }
        else
        {
            if (!b1)
            {
                AccountFromDate = valid.beDate(txtAccountFromDate, "结算开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                AccountToDate = valid.beDate(txtAccountToDate, "结算结束日期范围终止值格式必须为yyyyMMdd");
            }
            if (!b3)
            {
                FileFromDate = valid.beDate(txtFileFromDate,"文件开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b4)
            {
                FileToDate = valid.beDate(txtFileToDate, "文件结束日期范围起始值格式必须为yyyyMMdd");
            }
        }

        if (AccountFromDate != null && AccountToDate != null)
        {
            valid.check(AccountFromDate.Value.CompareTo(AccountToDate.Value) <= 0, "结算开始日期不能大于结束日期");
        }

        if (FileFromDate != null && FileToDate != null)
        {
            valid.check(FileFromDate.Value.CompareTo(FileToDate.Value) <= 0, "文件开始日期不能大于结束日期");
        }
    }

    #endregion
}