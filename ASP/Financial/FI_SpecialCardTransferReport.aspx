﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_SpecialCardTransferReport.aspx.cs" Inherits="ASP_Financial_FI_SpecialCardTransferReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>特种卡转账报表</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        var cal1;
        var cal2;

        function pageLoad() {
            try {
                cal1 = $find("FMonthCalendar");
                cal2 = $find("TMonthCalendar");
                modifyCalDelegates(cal1);
                modifyCalDelegates(cal2);
            }
            catch (e)
            { }

        }

        function modifyCalDelegates(cal) {
            cal._cell$delegates = {
                mouseover: Function.createDelegate(cal, cal._cell_onmouseover),
                mouseout: Function.createDelegate(cal, cal._cell_onmouseout),

                click: Function.createDelegate(cal, function (e) {

                    e.stopPropagation();
                    e.preventDefault();

                    if (!cal._enabled) return;

                    var target = e.target;
                    var visibleDate = cal._getEffectiveVisibleDate();
                    Sys.UI.DomElement.removeCssClass(target.parentNode, "ajax__calendar_hover");
                    switch (target.mode) {
                        case "prev":
                        case "next":
                            cal._switchMonth(target.date);
                            break;
                        case "title":
                            switch (cal._mode) {
                                case "days": cal._switchMode("months"); break;
                                case "months": cal._switchMode("years"); break;
                            }
                            break;
                        case "month":
                            //if the mode is month, then stop switching to day mode. 
                            if (target.month == visibleDate.getMonth()) {
                                //this._switchMode("days"); 
                            } else {
                                cal._visibleDate = target.date;
                                //this._switchMode("days"); 
                            }
                            cal.set_selectedDate(target.date);
                            cal._switchMonth(target.date);
                            cal._blur.post(true);
                            cal.raiseDateSelectionChanged();
                            break;
                        case "year":
                            if (target.date.getFullYear() == visibleDate.getFullYear()) {
                                cal._switchMode("months");
                            } else {
                                cal._visibleDate = target.date;
                                cal._switchMode("months");
                            }
                            break;                   
                        case "today":
                            cal.set_selectedDate(target.date);
                            cal._switchMonth(target.date);
                            cal._blur.post(true);
                            cal.raiseDateSelectionChanged();
                            break;
                    }

                })
            }

        }

        function onCalendarShown(sender, args) {
            //set the default mode to month 
            sender._switchMode("months", true);
            changeCellHandlers(cal1);
        }


        function changeCellHandlers(cal) {

            if (cal._monthsBody) {

                //remove the old handler of each month body. 
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        $common.removeHandlers(row.cells[j].firstChild, cal._cell$delegates);
                    }
                }
                //add the new handler of each month body. 
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        $addHandlers(row.cells[j].firstChild, cal._cell$delegates);
                    }
                }

            }
        }

        function onCalendarHidden(sender, args) {

            if (sender.get_selectedDate()) {
                if (cal1.get_selectedDate() && cal2.get_selectedDate() && cal1.get_selectedDate() > cal2.get_selectedDate()) {
                    //alert('The "From" Date should smaller than the "To" Date, please reselect!');
                    //sender.show();
                    //return;
                }
                //get the final date 
                var finalDate = new Date(sender.get_selectedDate());
                var selectedMonth = finalDate.getMonth();
                finalDate.setDate(1);
                if (sender == cal2) {
                    // set the calender2's default date as the last day 
                    finalDate.setMonth(selectedMonth + 1);
                    finalDate = new Date(finalDate - 1);
                }
                //set the date to the TextBox 
                sender.get_element().value = finalDate.format(sender._format);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
     <div class="tb">
		    财务报表->特种卡转账报表
	 </div>
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
	        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
	        swpmIntance.add_initializeRequest(BeginRequestHandler);
	        swpmIntance.add_pageLoading(EndRequestHandler);
	        function BeginRequestHandler(sender, args) {
	            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
	        }
	        function EndRequestHandler(sender, args) {
	            try { MyExtHide(); } catch (ex) { }
	        }
          </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">  
            <ContentTemplate>
             <!-- #include file="../../ErrorMsg.inc" -->
             <div class="con">
              <div class="card">查询条件</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td>
                            <div align="right">按日/按月:</div>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDayMonth" CssClass="inputmid" AutoPostBack="true" OnSelectedIndexChanged="ddlDayMonth_Changed">
                                <asp:ListItem Selected="True" Text="按日统计" Value="Day"></asp:ListItem>
                                <asp:ListItem Selected="False" Text="按月统计" Value="Month"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <div align="right" runat="server" id="tdStart">开始日期:</div>
                        </td>
                       <td>
                           <div runat="server" id="divFDay">
                           <asp:TextBox runat="server" ID="txtStartDate" MaxLength="8" CssClass="input"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtStartDate" Format="yyyyMMdd"></ajaxToolkit:CalendarExtender>
                           </div>
                           <div runat="server" id="divFMonth" visible="false">
                               <asp:TextBox runat="server" ID="txtStartMonth" MaxLength="6" CssClass="input"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="FMonthCalendar" runat="server" TargetControlID="txtStartMonth" Format="yyyyMM"
                                OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown"></ajaxToolkit:CalendarExtender>
                           </div>
                       </td>
                       <td><div align="right" runat="server" id="tdEnd">结束日期:</div></td>
                       <td>
                           <div runat="server" id="divTDay">
                           <asp:TextBox runat="server" ID="txtEndDate" MaxLength="8" CssClass="input"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="TCalendar" runat="server" TargetControlID="txtEndDate" Format="yyyyMMdd"></ajaxToolkit:CalendarExtender>
                            </div>
                           <div runat="server" id="divTMonth" visible="false">
                               <asp:TextBox runat="server" ID="txtEndMonth" MaxLength="8" CssClass="input"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="TMonthCalendar" runat="server" TargetControlID="txtEndMonth" Format="yyyyMM"
                               OnClientHidden="onCalendarHidden" OnClientShown="onCalendarShown"></ajaxToolkit:CalendarExtender>
                           </div>
                       </td>
                    <td align="right">
                            <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
               </table>
             </div>
             <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                        <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click"/>
                        <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="return printGridView('printarea');"/>
                    </td>
                </tr>
            </table>
           <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:380px">
                    <table id="printReport" width ="95%">
                        <tr align="center">
                            <td style ="font-size:16px;font-weight:bold" runat="server" id="tdTitle">特种卡转账报表</td>
                        </tr>
                         <tr>
                            <td>
                                <table width="300px" align="right">
                                    <tr align="right">
                                        <td><div runat="server" id="divSDate">
                                            <span id="fText">开始日期：</span><%=txtStartDate.Text%></div>
                                            <div runat="server" id="divSMonth" visible="false">
                                                <span>开始月份：</span><%=txtStartMonth.Text%>
                                            </div>
                                        </td>
                                        <td><div runat="server" id="divEDate">
                                            <span id="tText">结束日期：</span><%=txtEndDate.Text%></div>
                                            <div runat="server" id="divEMonth" visible="false">
                                                <span>结束月份：</span><%=txtEndMonth.Text%></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <asp:GridView ID="gvResult" runat="server"
                    Width = "95%"
                    CssClass="tab2"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    PagerStyle-HorizontalAlign=left
                    PagerStyle-VerticalAlign=Top
                    AutoGenerateColumns="False"
                    OnRowDataBound="gvResult_RowDataBound"
                    OnPreRender="gvResult_PreRender"
                    ShowFooter="true"
                    >
                    <Columns>
                        <asp:BoundField HeaderText="时间" DataField="时间"/>
                        <asp:BoundField HeaderText="业务类型" DataField="业务类型"/>
                        <asp:BoundField HeaderText="张数" DataField="张数"/>
                        <asp:BoundField HeaderText="金额" DataField="金额"/>
                    </Columns>
                    </asp:GridView>
                </div>
              </div>
            </div>
            </ContentTemplate>
             <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
