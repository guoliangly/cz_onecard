﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_PartnerPosSupplyReport.aspx.cs" Inherits="ASP_Financial_FI_PartnerPosSupplyReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>POS代理充值商户日报</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
      <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
        <div class="tb">
		    财务报表->POS代理充值商户日报

	    </div>
	
	    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" AsyncPostBackTimeout="12000" EnableScriptLocalization="true" ID="ScriptManager1" />
	    <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel1"  UpdateMode="Conditional"  runat="server">  
            <ContentTemplate>  
                  <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
	        <div class="con">

	           <div class="card">查询</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                        <td><div align="right">开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar1" runat="server" TargetControlID="txtFromDate" Format="yyyyMMdd" />
                        </td>
                        <td><div align="right">结束日期:</div></td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate" Format="yyyyMMdd" />
                        </td>
                        
                   </tr>
                   <tr>
                    <td align="right">统计方式:</td>
                <td><asp:DropDownList ID="selQueryType" CssClass="input" runat="server">
                    <asp:ListItem Text="按结算日期统计" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="按消费日期统计" Value="1"></asp:ListItem>
                     </asp:DropDownList>    
		            </td>
                    <td></td>
                      <td></td>
                   </tr>
                   <tr>
                  <td align="right">结算单元:</td>
                  <td><asp:DropDownList ID="selBalunit" runat="server"></asp:DropDownList>  
                  </td>
                  <td align="right">&nbsp;</td>
                   <td align="right">
                            <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>

                </tr>
                
               </table>
               
             </div>
	
	        <asp:HiddenField ID="hidNo" runat="server" Value="" />
	
            <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                    <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click" />
                    <asp:Button ID="btnPrint" CssClass="button1" runat="server" Text="打印" OnClientClick="return printGridView('printarea');"/>
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:380px">
                    <table id="printReport" width ="80%">
                        <tr align="center">
                            <td  id="tdHeader" runat="server" style ="font-size:16px;font-weight:bold">代理充值日报表</td>
                        </tr>
                        <tr>
                            <td>
                            
                                <table width="400px" align="left">
                                    <tr align="right">
                                        <td>报告期：<%=DateTime.ParseExact(txtFromDate.Text, "yyyyMMdd", null).ToString("yyyy年MM月dd日")%>--<%=DateTime.ParseExact(txtToDate.Text, "yyyyMMdd", null).ToString("yyyy年MM月dd日")%></td>
                                        <td>单位：元</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                  
<div id="tbResult" runat="server">
<%-- <table cellpadding="0" cellspacing="0" style="width: 100%;"  align="center" class="tab2">
           
           
           <tr><td rowspan="2" colspan="2" style="background-color: #FFFF00;border:1px solid black;"  width="50px">财务划账金额</td>
             <%=Dispaly3() %></tr><tr>
                  <%=Dispaly2() %></tr><%=Dispaly1() %>
            </table>--%>
</div>
           


                    
                </div>
              </div>
            </div>
       
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
              </Triggers>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>