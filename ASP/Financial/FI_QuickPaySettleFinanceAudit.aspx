﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_QuickPaySettleFinanceAudit.aspx.cs" Inherits="ASP_Financial_FI_QuickPaySettleFinanceAudit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>闪付公交结算财审</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <script type="text/javascript" src="../../js/checkall.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cz {
            margin-left:800px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
		    财务管理->闪付公交结算财审
	 </div>
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
	    <script type="text/javascript" language="javascript">
	        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
	        swpmIntance.add_initializeRequest(BeginRequestHandler);
	        swpmIntance.add_pageLoading(EndRequestHandler);
	        function BeginRequestHandler(sender, args) {
	            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
	        }
	        function EndRequestHandler(sender, args) {
	            try { MyExtHide(); } catch (ex) { }
	        }
          </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <!-- #include file="../../ErrorMsg.inc" -->
             <div class="con">

	           <div class="card">查询条件</div>
               <div class="kuang5">
               <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                   <tr>
                   <td>
                      <div align="right">审核状态:</div></td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlSelState" CssClass="inputmid">
                               <%-- <asp:ListItem Text="---请选择---" Value="-1" Selected="True"></asp:ListItem>--%>
                                <asp:ListItem Text="1:审核通过" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="3:财务人工已转账" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td><div align="right">结算开始日期:</div></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAccountFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtAccountFromDate" Format="yyyyMMdd" />
                        </td>
                       <td><div align="right">结算结束日期:</div></td>
                       <td>
                           <asp:TextBox runat="server" ID="txtAccountToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                           <ajaxToolkit:CalendarExtender ID="TCalendar" runat="server" TargetControlID="txtAccountToDate" Format="yyyyMMdd"></ajaxToolkit:CalendarExtender>
                       </td>
                    <td align="right">
                            <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/>
                        </td>
                   </tr>
               </table>
             </div>
	
	        <table border="0" width="95%">
                <tr>
                    <td align="left"><div class="jieguo">查询结果</div></td>
                    <td align="right">
                        <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel" OnClick="btnExport_Click"/>
                    </td>
                </tr>
            </table>
            
              <div id="printarea" class="kuang5">
                <div id="gdtbfix" style="height:380px">
                    <table id="printReport" width ="95%">
                        <tr align="center">
                            <td style ="font-size:16px;font-weight:bold">闪付公交结算财审</td>
                        </tr>
                         <tr>
                            <td>
                                <table width="300px" align="right">
                                    <tr align="right">
                                        <td>开始日期：<%=txtAccountFromDate.Text%></td>
                                        <td>结束日期：<%=txtAccountToDate.Text%></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
           <asp:GridView ID="gvResult" runat="server"
                            Width = "95%"
                            CssClass="tab2"
                            HeaderStyle-CssClass="tabbt" 
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            EmptyDataText="查询结果为空"
                            EmptyDataRowStyle-Font-Bold="true"
                            EmptyDataRowStyle-BorderWidth="0"
                            ShowFooter="false" AutoGenerateColumns="false"
                            DataKeyNames="balunitno,bankcode,FILEDATE,DEALTIME">
                <Columns>
                <asp:TemplateField>
                <HeaderTemplate>
                <input type="checkbox" ID="chkAll" onclick="CheckAll(this, 'gvResult')" name="chkAll"/>
                </HeaderTemplate>
                <ItemTemplate>
                <input type="checkbox" name="Item" onclick="ItemCheck(this, 'gvResult', 'chkAll')"  id="Item" disabled='<%#Eval("state").ToString()=="1"?false:true%>' runat="server"/>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="商户代码" DataField="balunitno"/>
                <asp:BoundField HeaderText="商户名称" DataField="balunit"/>
                <asp:BoundField HeaderText="开户行" DataField="bank"/>
                <asp:BoundField HeaderText="银行帐号" DataField="bankaccno"/>
                <asp:BoundField HeaderText="结算金额" DataField="MONEY" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="转账金额" DataField="TRANSFERMONEY" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="应收佣金" DataField="COMMISSION" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="日期范围" DataField="SETTLECYCLE"/>
                </Columns>
               </asp:GridView>
                </div>
              </div>

            <div class="card">财务审核</div>
            <div class="kuang5">
            <table width="95%" border="0" class="text25" cellpadding="0" cellspacing="0">
            <tr>
                <td><asp:DropDownList runat="server" ID="DDLState" CssClass="cz">
                <asp:ListItem Text="3:财务人工已转账" Value="3"></asp:ListItem>
                    </asp:DropDownList></td>
                <td><asp:Button CssClass="button1" ID="btnConfirm" Text="提交" runat="server" 
                    OnClientClick="return confirm('确认提交？');" OnClick="btnConfirm_Click"/>
                </td>
            </tr>
            </table>
            </div>

            <div style="display:none">
            <asp:GridView ID="gridToExcel" runat="server"
                            Width = "95%"
                            CssClass="tab2"
                            HeaderStyle-CssClass="tabbt" 
                            FooterStyle-CssClass="tabcon"
                            AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel"
                            PagerSettings-Mode="NumericFirstLast"
                            PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top"
                            EmptyDataText="查询结果为空"
                            EmptyDataRowStyle-Font-Bold="true"
                            EmptyDataRowStyle-BorderWidth="0"
                            ShowFooter="false" AutoGenerateColumns="false">
                <Columns>
                <asp:BoundField HeaderText="商户代码" DataField="balunitno"/>
                <asp:BoundField HeaderText="商户名称" DataField="balunit"/>
                <asp:BoundField HeaderText="开户行" DataField="bank"/>
                <asp:BoundField HeaderText="银行帐号" DataField="bankaccno"/>
                <asp:BoundField HeaderText="结算金额" DataField="MONEY" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="转账金额" DataField="TRANSFERMONEY" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="应收佣金" DataField="COMMISSION" DataFormatString="{0:f2}"/>
                <asp:BoundField HeaderText="日期范围" DataField="SETTLECYCLE"/>
                </Columns>
               </asp:GridView>
            </div>
            </div>
         </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
