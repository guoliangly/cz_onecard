﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_BUS_REPORTFiApproval.aspx.cs" Inherits="ASP_FI_BUS_REPORTFiApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>公交转账日报财审</title>
      <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <script type="text/javascript" src="../../js/print.js"></script>
     <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />

       <script language="javascript">
           function SelectAll(tempControl) {
               //将除头模板中的其它所有的CheckBox取反 

               var theBox = tempControl;
               xState = theBox.checked;

               elem = theBox.form.elements;
               for (i = 0; i < elem.length; i++)
                   if (elem[i].type == "checkbox" && elem[i].id != theBox.id) {
                       if (elem[i].checked != xState)
                           elem[i].click();
                   }
           }  
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server"/>
        <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
								function BeginRequestHandler(sender, args){
    							try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
								}
								function EndRequestHandler(sender, args) {
    							try {MyExtHide(); } catch(ex){}
								}
          </script>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional"  runat="server">
            <ContentTemplate>
        <div class="tb">
        财务管理->公交转账日报财审

        </div>

        <asp:HiddenField ID="hidNo" runat="server" Value="" />
        
    <asp:BulletedList ID="bulMsgShow" runat="server"/>
    <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>

        <div class="con">
          <div class="base">查询条件</div>
          <div class="kuang5">
         <table border="0" cellpadding="0" cellspacing="0" class="text25">
          <tr>
          <td>审核状态:<asp:DropDownList ID="selApproval" CssClass="input" runat="server">
                     <asp:ListItem Text="2:审核通过" Value="2"></asp:ListItem>
                     <asp:ListItem Text="3:财务人工已转账" Value="3"></asp:ListItem>
                </asp:DropDownList></td>
            <td width="100"><div align="right">结算开始日期:</div></td>
            <td width="100">
<asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
      <ajaxToolkit:CalendarExtender ID="FCalendar1" runat="server" TargetControlID="txtFromDate"
             Format="yyyyMMdd" />
            </td>
            <td width="100"><div align="right">结算结束日期:</div></td>
            <td width="100"><asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
      <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate"
            Format="yyyyMMdd" />
            </td>
            <td width="150" align="center"><asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询" OnClick="btnQuery_Click"/></td>
          </tr>
        </table>

         </div>
         
         <table border="0" width="95%">
            <tr>
                <td align="left"><div class="jieguo">查询结果</div></td>
                <td align="right">
                    <asp:Button ID="btnExport" CssClass="button1fix" runat="server" Text="导出表格" OnClick="btnExport_Click" />
                  </td>
            </tr>
        </table>

             <div id="printarea" class="kuang5">
              <div class="gdtbfix" style="height:380px;">
                    <table id="printReport" width ="95%">
                        <tr align="center">
                            <td style ="font-size:16px;font-weight:bold">公交转账日报</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="300px" align="left">
                                    <tr align="left">
                                        <td></td>
                                    </tr>
                                </table>
                            
                                <table width="300px" align="right">
                                    <tr align="right">
                                        <td>开始日期：<%=txtFromDate.Text%></td>
                                        <td>结束日期：<%=txtToDate.Text%></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                     <asp:GridView ID="gvResult" runat="server"
                    Width = "98%"
                    CssClass="tab2"
                    HeaderStyle-CssClass="tabbt"
                    FooterStyle-CssClass="tabcon"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    PagerSettings-Mode=NumericFirstLast
                    PagerStyle-HorizontalAlign=left
                    PagerStyle-VerticalAlign=Top
                    AutoGenerateColumns="false"
                    OnRowDataBound="gvResult_RowDataBound" >
           <Columns>
             <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAllCust" runat="server" onclick="javascript:SelectAll(this);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkCust" runat="server"  />
                    </ItemTemplate>
                 </asp:TemplateField>
                <asp:BoundField HeaderText="ID" DataField="ID"/>
                <asp:BoundField HeaderText="商户代码" DataField="商户代码"/>
                <asp:BoundField HeaderText="商户名称" DataField="商户名称"    />
                <asp:BoundField HeaderText="开户行" DataField="开户行"    />
                <asp:BoundField HeaderText="银行账号" DataField="银行账号"    />
                <asp:BoundField HeaderText="结算金额" DataField="结算金额"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="转帐金额" DataField="转帐金额"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="应收佣金" DataField="应收佣金"  NullDisplayText="0"  />
                <asp:BoundField HeaderText="结算周期" DataField="结算周期" />
                </Columns>
            <PagerSettings Mode="NumericFirstLast" />
            <SelectedRowStyle CssClass="tabsel" />
            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle CssClass="tabbt" />
            <AlternatingRowStyle CssClass="tabjg" />          
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                  <tr class="tabbt">
                    <td>商户代码</td>
                    <td>商户名称</td>
                     <td>开户行</td>
                      <td>银行账号</td>
                      <td>结算金额</td>
                    <td>转帐金额</td>
                    <td>应收佣金</td>
                    <td>结算周期</td>
                  </tr>
                  </table>
            </EmptyDataTemplate>
        </asp:GridView>


             </div>
           </div>    
           
           
           <div class="kuang5">
<table width="95%" border="0"cellpadding="0" cellspacing="0">
  <tr>
    <td width="70%">&nbsp;</td>
    <td align="right"><asp:DropDownList ID="selApproval2" CssClass="input" runat="server">
                     <asp:ListItem Text="3:财务人工已转账" Value="3"></asp:ListItem>
                </asp:DropDownList></td>
    <td align="right"><asp:Button ID="btnSubmit" Enabled="true" CssClass="button1" runat="server" Text="提交" OnClick="btnSubmit_Click" OnClientClick="{if(confirm('确定要提交吗?')){return true;}return false;}"/></td>
  </tr>
</table>
</div>   
         </div>
         
   </ContentTemplate>
      <Triggers>
        <asp:PostBackTrigger ControlID="btnExport" />
      </Triggers>
  </asp:UpdatePanel>

    </form>
</body>
</html>