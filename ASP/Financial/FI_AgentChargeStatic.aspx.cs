﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using PDO.Financial;
using Master;
using TM;
using TDO.BalanceChannel;

public partial class ASP_Financial_FI_AgentChargeStatic : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TMTableModule tmTMTableModule = new TMTableModule();
            selType.Items.Add(new ListItem("--请选择--", ""));
            selType.Items.Add(new ListItem("金额统计", "AgentMoney"));
            selType.Items.Add(new ListItem("笔数统计", "AgentCount"));

            selChargeType.Items.Add(new ListItem("--请选择--", ""));
            selChargeType.Items.Add(new ListItem("商城币", "SCB"));
            selChargeType.Items.Add(new ListItem("现金", "CASH"));
            selChargeType.Items.Add(new ListItem("翼支付","YZF"));
            //selChargeType.Items.Add(new ListItem(" 银行卡","YHK"));
            //DateTime tf = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1);
            //DateTime tt = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month));

            txtFromDate.Text = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

            TF_SELSUP_BALUNITTDO tdoTF_SELSUP_BALUNITIn = new TF_SELSUP_BALUNITTDO();
            TF_SELSUP_BALUNITTDO[] tdoTF_SELSUP_BALUNITOutArr = (TF_SELSUP_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_SELSUP_BALUNITIn, typeof(TF_SELSUP_BALUNITTDO), null);

            ControlDeal.SelectBoxFill(selBalunit.Items, tdoTF_SELSUP_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);
            selBalunit.Items[0].Value = "00000000";

            //只保留0D010101移动 0D020101电信 0D002204可的 ---请选择---
            for (int i = 0; i < selBalunit.Items.Count; i++)
            {
                if (selBalunit.Items[i].Value != "0D010101" && selBalunit.Items[i].Value != "0D020101" && selBalunit.Items[i].Value != "0D002204" && selBalunit.Items[i].Value != "00000000")
                {
                    selBalunit.Items.Remove(selBalunit.Items[i]);
                    i--;
                }
            }
        }
    }
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }
        //充值点不能为空
        if (selBalunit.SelectedValue == "00000000")
        {
            context.AddError("充值点不能为空", selBalunit);
            return;
        }

    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvChangeReport, null);
        validate();
        if (context.hasError()) return;

        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "")
        {
            pdo.funcCode = "YDChargeCardTypeAndSCB";
        }
        else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "")
        {
            pdo.funcCode = "DXChargeCardType";
        }
        else if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "AgentMoney")
        {
            pdo.funcCode = "YDChargeTypeAndSCBByMoney";//移动代理充值卡类型、移动商城币充值统计 金额
        }
        else if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "AgentCount")
        {
            pdo.funcCode = "YDChargeTypeAndSCBByCount";//移动代理充值卡类型、移动商城币充值统计 金额
        }
        else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "AgentMoney")
        {
            pdo.funcCode = "DXChargeCardTypeByMoney";//电信代理充值卡类型、移动商城币充值统计 金额
        }
        else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "AgentCount")
        {
            pdo.funcCode = "DXChargeCardTypeByCount";//电信代理充值卡类型、移动商城币充值统计 金额
        }
        else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "")
        {
            pdo.funcCode = "kediChargeCardType";      //可的代理充值类型、移动商城币充值统计 金额 笔数
        }
        else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "AgentMoney")
        {
            pdo.funcCode = "kediChargeCardTypeByMoney"; //可的代理充值类型、移动商城币充值统计 金额
        }
        else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "AgentCount")
        {
            pdo.funcCode = "kediChargeCardTypeByCount"; //可的代理充值类型、移动商城币充值统计 笔数
        }
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var7 = selBalunit.SelectedValue;
        pdo.var8 = selChargeType.SelectedValue;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        labTitle.Text = selType.SelectedItem.Text;
        if (labTitle.Text == "--请选择--")
        {
            labTitle.Text = "金额、笔数统计";
        }

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
            btnPrint.Enabled = false;
        }
        else
        {
            btnPrint.Enabled = true;
        }
        UserCardHelper.resetData(gvChangeReport, data);

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvChangeReport.Rows.Count > 0)
        {
            ExportGridView(gvChangeReport);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    private Array intArray = null;
    private Array doubleArray = null;
    protected void lvwQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }



    protected void gvChangeReport_PreRender(object sender, EventArgs e)
    {
        //GridViewMergeHelper.MergeGridViewRows(gvChangeReport, 0, 1);
    }
    protected void gvChangeReport_DataBound(object sender, EventArgs e)
    {

    }
    protected void gvChangeReport_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

            if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡" + " 总金额,笔数";
                header += "#" + "龙城通" + " 总金额,笔数";
                header += "#" + "联名卡" + " 总金额,笔数";
                header += "#" + "异型卡" + " 总金额,笔数";
                header += "#" + "公交IC卡" + " 总金额,笔数";
                header += "#" + "总计" + " 总金额,笔数";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡" + " 总金额,笔数";
                header += "#" + "龙城通" + " 总金额,笔数";
                header += "#" + "联名卡" + " 总金额,笔数";
                header += "#" + "异型卡" + " 总金额,笔数";
                header += "#" + "公交IC卡" + " 总金额,笔数";
                header += "#" + "总计" + " 总金额,笔数";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "AgentMoney")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D010101" && selType.SelectedValue == "AgentCount")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "AgentMoney")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D020101" && selType.SelectedValue == "AgentCount")
            {
                string header = "充值日期#代理充值点#充值方式";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "")
            {
                string header = "充值日期#代理充值点";
                header += "#" + "市民卡" + " 总金额,笔数";
                header += "#" + "龙城通" + " 总金额,笔数";
                header += "#" + "联名卡" + " 总金额,笔数";
                header += "#" + "异型卡" + " 总金额,笔数";
                header += "#" + "公交IC卡" + " 总金额,笔数";
                header += "#" + "总计" + " 总金额,笔数";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "AgentMoney")
            {
                string header = "充值日期#代理充值点";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
            else if (selBalunit.SelectedValue == "0D002204" && selType.SelectedValue == "AgentCount")
            {
                string header = "充值日期#代理充值点";
                header += "#" + "市民卡";
                header += "#" + "龙城通";
                header += "#" + "联名卡";
                header += "#" + "异型卡";
                header += "#" + "公交IC卡";
                header += "#" + "总计";
                GridViewHeaderHelper.SplitTableHeader(e.Row, header);
            }
        }
    }


}
