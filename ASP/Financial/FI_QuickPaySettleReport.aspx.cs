﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Common;
using PDO.Financial;
using Master;

/**************************************
 * create:chenwentao 2014-10-10
 * content:闪付公交结算日报 
 * ************************************/

public partial class ASP_Financial_FI_QuickPaySettleReport : Master.ExportMaster
{

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtAccountFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtAccountToDate.Text = DateTime.Today.ToString("yyyyMMdd");
        }
    }

    private void BindData()
    {
        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "FI_QuickPaySettleReport";
        pdo.var1 = ddlSelState.SelectedValue;
        pdo.var2 = txtAccountFromDate.Text;
        pdo.var3 = txtAccountToDate.Text;
        StoreProScene StorePro = new StoreProScene();
        DataTable dt = StorePro.Execute(context,pdo);
        if (dt == null || dt.Rows.Count == 0)
        {
            context.AddMessage("查询结果为空");
        }
        gvResult.DataSource = dt;
        gvResult.DataBind();

        girdToExcel.DataSource = dt;
        girdToExcel.DataBind();
    }

    #endregion

    #region Event

    /// <summary>
    /// 查询按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError())
            return;
        BindData();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (girdToExcel.Rows.Count > 0)
        {
            ExportGridView(girdToExcel);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    /// <summary>
    /// gridview行单击事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvResult_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        string Balunitno = gvResult.DataKeys[index]["balunitno"].ToString();
        if (e.CommandName == "Detail")
        {
            Response.Redirect(string.Format("FI_BusQuickPayTradeReport.aspx?id={0}",Balunitno));
        }
    }

    /// <summary>
    /// 确认提交
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count <= 0)
        {
            context.AddError("未选择数据，不能提交！");
            return;
        }
        if (!IsChkRow())
        {
            context.AddError("未选择数据，不能提交！");
            return;
        }
        string SessionID = Session.SessionID;
        ClearTempTable(SessionID);
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            System.Web.UI.HtmlControls.HtmlInputCheckBox cb = gvResult.Rows[i].Cells[0].FindControl("Item") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (cb.Checked)
            {
                string[] parm = GetDataKeyValue(gvResult,i);
                string InsertSql = string.Format("insert into TMP_COMMON(f0,f1,f2,f3,f4) values('{0}','{1}','{2}','{3}','{4}')", SessionID,parm[0], parm[1], parm[2], parm[3]);
                context.DBOpen("Insert");
                context.ExecuteNonQuery(InsertSql);
            }
        }
        if (!context.hasError())
        {
            context.DBCommit();
        }
        else
        {
            context.RollBack();
        }

        //审核处理
        context.SPOpen();
        context.AddField("P_STATE").Value=DDLState.SelectedValue;
        context.AddField("P_SESSIONID").Value = SessionID;
        bool ok = context.ExecuteSP("SP_FI_QuickPayAudit");
        if (ok)
        {
            context.DBCommit();
            context.AddMessage("提交成功");
            btnQuery_Click(sender,e);
        }
    }

    #endregion

    #region Function

    /// <summary>
    /// 根据条件清空临时表
    /// </summary>
    /// <param name="key">条件</param>
    private void ClearTempTable(string key)
    {
        context.DBOpen("Delete");
        context.ExecuteNonQuery(string.Format("delete from TMP_COMMON where f0='{0}'", key));
        context.DBCommit();
    }

    /// <summary>
    /// 验证查询条件
    /// </summary>
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtAccountFromDate);
        bool b2 = Validation.isEmpty(txtAccountToDate);
        DateTime? AccountFromDate = null, AccountToDate = null;
        if (b1 || b2)
        {
            context.AddError("日期必须填写");
        }
        else
        {
            if (!b1)
            {
                AccountFromDate = valid.beDate(txtAccountFromDate, "结算开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                AccountToDate = valid.beDate(txtAccountToDate, "结算结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (AccountFromDate != null && AccountToDate != null)
        {
            valid.check(AccountFromDate.Value.CompareTo(AccountToDate.Value) <= 0, "结算开始日期不能大于结束日期");
        }
    }

    /// <summary>
    /// 返回指定行的datakeynames的值
    /// </summary>
    /// <param name="grid">gridview的id</param>
    /// <param name="index">行索引</param>
    /// <returns></returns>
    private string[] GetDataKeyValue(GridView grid,int index)
    {
        string[] valuelist = new string[4];
        valuelist[0] = grid.DataKeys[index]["balunitno"].ToString();//商户代码
        valuelist[1] = grid.DataKeys[index]["bankcode"].ToString();//银行代码
        valuelist[2] = grid.DataKeys[index]["FILEDATE"].ToString();//文件生成日期
        valuelist[3] = grid.DataKeys[index]["DEALTIME"].ToString();//结算日期
        return valuelist;
    }

    /// <summary>
    /// 判断是否有数据记录被选中
    /// </summary>
    /// <returns></returns>
    private bool IsChkRow()
    {
        for (int i = 0; i < gvResult.Rows.Count; i++)
        {
            System.Web.UI.HtmlControls.HtmlInputCheckBox cb = gvResult.Rows[i].Cells[0].FindControl("Item") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (cb.Checked)
            {
                return true;
            }
        }
        return false;
    }

    #endregion

}