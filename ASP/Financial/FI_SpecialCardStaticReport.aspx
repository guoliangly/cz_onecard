﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_SpecialCardStaticReport.aspx.cs" Inherits="ASP_Financial_FI_SpecialCardStaticReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>特种卡库存盘点表</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <script type="text/javascript" src="../../js/myext.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="tb">
            业务报表->特种卡库存盘点表
        </div>
        <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ToolkitScriptManager1" />
        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- #include file="../../ErrorMsg.inc" -->
                <div class="con">
                    <div class="card">
                        查询
                    </div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <td>
                                    <div align="right">
                                        开始日期:
                                    </div>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtFromDate" MaxLength="8" CssClass="input"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="FCalendar1" runat="server" TargetControlID="txtFromDate"
                                        Format="yyyyMMdd" />
                                </td>
                                <td>
                                    <div align="right">
                                        结束日期:
                                    </div>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtToDate" MaxLength="8" CssClass="input"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="FCalendar2" runat="server" TargetControlID="txtToDate"
                                        Format="yyyyMMdd" />
                                </td>
                                <td>
                                    <div align="right">
                                        部门:
                                    </div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selDept" CssClass="inputmid" runat="server">
                                        <asp:ListItem Value="">--请选择--</asp:ListItem>
                                        <asp:ListItem Value="0003">0003:江南网点</asp:ListItem>
                                        <asp:ListItem Value="0011">0011:延陵西路网点</asp:ListItem>
                                        <asp:ListItem Value="0013">0013:晋陵北路网点</asp:ListItem>
                                        <asp:ListItem Value="0014">0014:延陵东路网点</asp:ListItem>
                                        <asp:ListItem Value="0016">0016:大学城网点</asp:ListItem>
                                        <asp:ListItem Value="0020">0020:溧阳客运总站网点</asp:ListItem>
                                        <asp:ListItem Value="0022">0022:金坛网点</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div align="right">
                                        卡类型:
                                    </div>
                                </td>
                                <td>
                                    <asp:DropDownList ID="selSpecialCardType" CssClass="inputmid" runat="server">
                                        <asp:ListItem Value="">--请选择--</asp:ListItem>
                                        <asp:ListItem Value="9901">9901:高龄老人卡</asp:ListItem>
                                        <asp:ListItem Value="9902">9902:优惠卡</asp:ListItem>
                                        <asp:ListItem Value="9903">9903:残疾人优待卡</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnQuery" CssClass="button1" runat="server" Text="查询"
                                        OnClick="btnQuery_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table border="0" width="95%">
                        <tr>
                            <td align="left">
                                <div class="jieguo">
                                    查询结果
                                </div>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnExport" CssClass="button1" runat="server" Text="导出Excel"
                                    OnClick="btnExport_Click" />
                            </td>
                        </tr>
                    </table>
                    <div id="printarea" class="kuang5">
                        <div id="gdtbfix" style="height: 380px">
                            <table id="printReport" width="95%">
                                <tr align="center">
                                    <td style="font-size: 16px; font-weight: bold">特种卡库存盘点表
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="gvResult" runat="server" Width="95%" CssClass="tab2" HeaderStyle-CssClass="tabbt"
                                FooterStyle-CssClass="tabcon" AlternatingRowStyle-CssClass="tabjg" SelectedRowStyle-CssClass="tabsel"
                                PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left" PagerStyle-VerticalAlign="Top"
                                AutoGenerateColumns="true" OnRowDataBound="gvResult_RowDataBound" ShowFooter="false">
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
