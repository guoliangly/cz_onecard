﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.PersonalTrade;
using TM;
using TDO.BalanceChannel;
using TDO.UserManager;
using PDO.SpecialDeal;
using Master;
using PDO.Financial;

public partial class ASP_Financial_FI_SpeAdjustAccApproval : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化审核列表
            showResult();

            //清空临时表数据

            clearTempTable();
        }
    }

    private void createTempTable()
    {    //创建临时表

        context.DBOpen("Select");
        context.ExecuteNonQuery("exec SP_SD_SpeAdjustCheckTmp");
    }

    private void clearTempTable()
    {   //清空临时表数据

        context.DBOpen("Delete");
        context.ExecuteNonQuery("delete from TMP_ADJUSTACC_IMP where SessionId='"
            + Session.SessionID + "'");
        context.DBCommit();
    }

    private void QueryAdjustAcc()
    {
        //取得查询结果
        ICollection dataView = QueryResultColl();

        //显示查询结果信息
        gvResult.DataSource = dataView;
        gvResult.DataBind();
    }

    private void showResult()
    {
        //显示调帐信息列表信息
        gvResult.DataSource = new DataTable();
        gvResult.DataBind();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        QueryAdjustAcc();
        if (selCheckType.SelectedValue == "0")  //待审核
        {
            btnPass.Enabled = true;
            btnCancel.Enabled = true;
        }
        else if (selCheckType.SelectedValue == "1") //审核通过
        {
            btnPass.Enabled = false;
            btnCancel.Enabled = false;
        }
        else if (selCheckType.SelectedValue == "2") //审核作废
        {
            btnPass.Enabled = false;
            btnCancel.Enabled = false;
        }
    }


    protected void CheckAll(object sender, EventArgs e)
    {
        //全选审核信息记录

        CheckBox cbx = (CheckBox)sender;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            if (!gvr.Cells[0].Enabled) continue;
            CheckBox ch = (CheckBox)gvr.FindControl("ItemCheckBox");
            ch.Checked = cbx.Checked;
        }
    }


    public ICollection QueryResultColl()
    {
        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = "speApproval";
        pdo.var1 = selCheckType.SelectedValue;
        StoreProScene storePro = new StoreProScene();

        DataTable data = storePro.Execute(context, pdo);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        DataView dataView = new DataView(data);
        return dataView;

    }

    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        //分页处理 
        gvResult.PageIndex = e.NewPageIndex;

        QueryAdjustAcc();

    }



    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //佣金率
            e.Row.Cells[6].Text = (Convert.ToDouble(e.Row.Cells[6].Text)).ToString("P");
            
            //实际调帐金额 转换为元
            e.Row.Cells[7].Text = (Convert.ToDouble(e.Row.Cells[7].Text) / 100).ToString("0.00");

            DateTime beginDate = Convert.ToDateTime(e.Row.Cells[8].Text.ToString());
            DateTime endDate = Convert.ToDateTime(e.Row.Cells[9].Text.ToString());
            e.Row.Cells[8].Text = beginDate.ToString("yyyy/MM/dd") + "-" + endDate.ToString("yyyy/MM/dd");
        }

        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            //隐藏结束日期和业务流水号
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[10].Visible = false;
        }
    }


    private bool RecordIntoTmp()
    {
        //回收记录入临时表
        context.DBOpen("Insert");

        int count = 0;
        foreach (GridViewRow gvr in gvResult.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("ItemCheckBox");
            if (cb != null && cb.Checked)
            {
                ++count;
                context.ExecuteNonQuery("insert into TMP_ADJUSTACC_IMP values('"
                    + gvr.Cells[10].Text + "','1234567890123456','1','" + Session.SessionID + "')");
            }
        }

        context.DBCommit();

        // 没有选中任何行，则返回错误

        if (count <= 0)
        {
            context.AddError("A009111001");
            return false;
        }

        return true;
    }


    protected void btnPass_Click(object sender, EventArgs e)
    {
        //清空临时表数据

        clearTempTable();

        //信息插入临时表

        if (!RecordIntoTmp()) return;

        //调用通过的存储过程

        context.SPOpen();
        context.AddField("p_sessionID").Value = Session.SessionID;

        bool ok = context.ExecuteSP("SP_FI_SpeAdjustAccChkPass");
        if (ok)
        {
            AddMessage("M009111112");
        }

        //清空临时表数据

        clearTempTable();

        //指定审核状态为待审核
        selCheckType.SelectedIndex = -1;
        //查询待审核的调账信息
        QueryAdjustAcc();

    }




    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //清空临时表数据

        clearTempTable();

        //信息插入临时表

        if (!RecordIntoTmp()) return;

        //调用作废的存储过程

        context.SPOpen();
        context.AddField("p_sessionID").Value = Session.SessionID;

        bool ok = context.ExecuteSP("SP_FI_SpeAdjustAccChkCancel");
        if (ok)
        {
            AddMessage("M009111113");
        }

        //清空临时表数据

        clearTempTable();

        //指定审核状态为待审核
        selCheckType.SelectedIndex = -1;
        //查询待审核的调账信息
        QueryAdjustAcc();
    }

}
