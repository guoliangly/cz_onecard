﻿using Common;
using Master;
using PDO.Financial;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TDO.UserManager;
using TM;

public partial class ASP_Financial_FI_SpecialCardStaticReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

        }
    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;
        DataTable data = null;
        if (selDept.SelectedValue != "" && selSpecialCardType.SelectedValue == "")
        {
            data = BusCardHelper.callQuery(context, "QrySpecialCardStaticByDept", selDept.SelectedValue, txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selSpecialCardType.SelectedValue);
        }
        else if (selDept.SelectedValue == "" && selSpecialCardType.SelectedValue != "")
        {
            data = BusCardHelper.callQuery(context, "QrySpecialCardStaticByCardType", selDept.SelectedValue, txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selSpecialCardType.SelectedValue);
        }
        else if (selDept.SelectedValue != "" && selSpecialCardType.SelectedValue != "")
        {
            data = BusCardHelper.callQuery(context, "QrySpecialCardStaticByDeptCard", selDept.SelectedValue, txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selSpecialCardType.SelectedValue);
        }
        else
        {
            data = BusCardHelper.callQuery(context, "QrySpecialCardStaticReport", selDept.SelectedValue, txtFromDate.Text.Trim(), txtToDate.Text.Trim(), selSpecialCardType.SelectedValue);
        }
        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        UserCardHelper.resetData(gvResult, data);
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;

        if (!b1)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        if (!b2)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }


        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
            DateTime endDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd"), "yyyyMMdd", null);
            valid.check(toDate.Value.CompareTo(endDate) <= 0, "结束日期须小于当前日期");
        }

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

    private int _lyCount;   //领用
    private int _skCount;   //售卡
    private int _hkCount;   //换卡
    private int _kcCount;   //库存    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    //    if (selDept.SelectedValue != "")
    //    {
    //        ////用于统计的部门列
    //        //e.Row.Cells[0].Visible = false;


    //        if (e.Row.RowType == DataControlRowType.DataRow)    // 数据行
    //        {
    //            _lyCount = _ckCount + Convert.ToInt32(e.Row.Cells[4].Text.Trim());
    //            _csCount = _csCount + Convert.ToInt32(e.Row.Cells[5].Text.Trim());
    //            _shCount = _shCount + Convert.ToInt32(e.Row.Cells[6].Text.Trim());
    //            _zsCount = _zsCount + Convert.ToInt32(e.Row.Cells[7].Text.Trim());
    //            _jkCount = _jkCount + Convert.ToDouble(e.Row.Cells[8].Text.Trim());
    //        }

    //        if (e.Row.RowType == DataControlRowType.Footer)    //页脚
    //        {
    //            e.Row.Cells[4].Text = _ckCount.ToString();
    //            e.Row.Cells[5].Text = _csCount.ToString();
    //            e.Row.Cells[6].Text = _shCount.ToString();
    //            e.Row.Cells[7].Text = _zsCount.ToString();
    //            e.Row.Cells[8].Text = _jkCount.ToString();
    //        }
    //    }
    //    else
    //    {
    //        e.Row.Cells[0].Visible = false; //隐藏用于排序的DEPARTNO列
    //        gvResult.ShowFooter = false;
    //    }
    }
}
