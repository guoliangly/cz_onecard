﻿using System;
using System.Data;
using System.Web.UI.WebControls;

using PDO.Financial;
using Master;
using Common;

using TDO.Financial;

public partial class ASP_Financial_FI_TradeCollectReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化业务
            TD_TRADETYPE_SHOWTDO tdoTD_TRADETYPE_SHOWIn = new TD_TRADETYPE_SHOWTDO();
            TD_TRADETYPE_SHOWTDO[] tdoTD_TRADETYPE_SHOWOutArr = (TD_TRADETYPE_SHOWTDO[])tm.selByPKArr(context, tdoTD_TRADETYPE_SHOWIn, typeof(TD_TRADETYPE_SHOWTDO), null, "TRADETYPE_SHOW", null);
            selTradeType.Items.Add(new ListItem("0000:全部业务", "0000"));
            foreach (DDOBase ddoDDOBase in tdoTD_TRADETYPE_SHOWOutArr)
            {
                selTradeType.Items.Add(new ListItem(ddoDDOBase.GetString("SHOWNO") + ":" + ddoDDOBase.GetString("SHOWNAME"), ddoDDOBase.GetString("SHOWNO")));
            }
            selTradeType.Items.Add(new ListItem("0040:充付器", "0040"));

            //初始化日期
            txtFromDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");
            txtFromMonth.Text = DateTime.Today.ToString("yyyyMM");
            txtToMonth.Text = DateTime.Today.ToString("yyyyMM");

            tdTitle.InnerText = "业务统计日报表";
            tdFromDate.InnerText = "开始日期：" + txtFromDate.Text;
            tdToDate.InnerText = "结束日期：" + txtToDate.Text;
         
        }
    }

    protected void selDayAndMonth_Changed(object sender, EventArgs e)
    {
        if (selDayAndMonth.SelectedValue == "DAY")
        {
            trDay.Visible = true;
            trMonth.Visible = false;
            tdTitle.InnerText = "业务统计日报表";
            tdFromDate.InnerText = "开始日期：" + txtFromDate.Text;
            tdToDate.InnerText = "结束日期：" + txtToDate.Text;
            UserCardHelper.resetData(gvResult, null);
        }
        else
        {
            trDay.Visible = false;
            trMonth.Visible = true;
            tdTitle.InnerText = "业务统计月报表";
            tdFromDate.InnerText = "开始月份：" + txtFromMonth.Text;
            tdToDate.InnerText = "结束月份：" + txtToMonth.Text;
            UserCardHelper.resetData(gvResult, null);
        }
    }

    private double totalMoney = 0;
    private int totalCount = 0;
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try {
                if (selDayAndMonth.SelectedValue == "DAY")
                {
                    e.Row.Cells[0].Text = e.Row.Cells[0].Text.Substring(0, 4) + "年" + e.Row.Cells[0].Text.Substring(4, 2) + "月" + e.Row.Cells[0].Text.Substring(6, 2) + "日";
                }
                else
                {
                    e.Row.Cells[0].Text = e.Row.Cells[0].Text.Substring(0, 4) + "年" + e.Row.Cells[0].Text.Substring(4, 2) + "月";
              
                }
               
                totalCount += Convert.ToInt32(GetTableCellValue(e.Row.Cells[3]));
                totalMoney += Convert.ToDouble(GetTableCellValue(e.Row.Cells[4]));
            }
            catch (Exception ex)
            {
            }
           
        }
        else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        {
            e.Row.Cells[0].ColumnSpan = 3;
            e.Row.Cells[0].Text = "总计";
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Text = "" + totalCount.ToString();
            e.Row.Cells[4].Text = "" + totalMoney.ToString();
        }
    }

    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;      
    }

    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b1 = Validation.isEmpty(txtFromDate);
        bool b2 = Validation.isEmpty(txtToDate);
        DateTime? fromDate = null, toDate = null;
        if (b1 || b2)
        {
            context.AddError("开始日期和结束日期必须填写");
        }
        else
        {
            if (!b1)
            {
                fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
            }
            if (!b2)
            {
                toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
            }
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

        //if (selStaff.SelectedValue == "")
        //    context.AddError("请选择员工");

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        if (selDayAndMonth.SelectedValue == "DAY")
        {
            pdo.funcCode = "TRADE_COLLECT_REPORT_DAY";
            pdo.var6 = selTradeType.SelectedValue;
            pdo.var1 = txtFromDate.Text;
            pdo.var2 = txtToDate.Text;
            tdFromDate.InnerText = "开始日期：" + txtFromDate.Text;
            tdToDate.InnerText = "结束日期：" + txtToDate.Text;
        }

        else
        {
            pdo.funcCode = "TRADE_COLLECT_REPORT_MONTH";
            pdo.var6 = selTradeType.SelectedValue;
            pdo.var1 = txtFromMonth.Text;
            pdo.var2 = txtToMonth.Text;
            tdFromDate.InnerText = "开始月份：" + txtFromMonth.Text;
            tdToDate.InnerText = "结束月份：" + txtToMonth.Text;
        }
            
        

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }
        UserCardHelper.resetData(gvResult, data);
        
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    protected void gvResult_PreRender(object sender, EventArgs e)
    {
        GridViewMergeHelper.MergeGridViewRows(gvResult, 0, 2);
    }

    protected void gvResult_DataBound(object sender, EventArgs e)
    {
        //GridViewMergeHelper.DataBoundWithEmptyRows(gvResult);
    }
}