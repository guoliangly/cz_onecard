﻿using System;
using System.Data;
using System.Web.UI;
using PDO.Financial;
using Master;

public partial class ASP_Financial_FI_StaffDailyEndReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lbDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            lbDepartName.Text = context.s_DepartName;
            lbStaffNo.Text = context.s_UserID;
            lbStaffName.Text = context.s_UserName;

            gvResult.DataSource = createDateSource();
            gvResult.DataBind();
        }
    }

    private DataTable createDateSource()
    {
        SP_FI_StatPDO pdo = new SP_FI_StatPDO();
        pdo.funcCode = "DailyEndReport";
        pdo.var1 = context.s_UserID;
        pdo.var2 = DateTime.Now.ToString("yyyyMMdd");
        pdo.var3 = context.s_DepartID;
        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);
        if (data == null || data.Rows.Count == 0)
        {
            return new DataTable();
        }
        return data;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }

}