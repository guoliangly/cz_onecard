﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FI_SpeAdjustAccInput.aspx.cs"
    Inherits="ASP_Financial_FI_SpeAdjustAccInput" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>特殊调帐统计录入</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />

    <script type="text/javascript" src="../../js/print.js"></script>

    <script type="text/javascript" src="../../js/myext.js"></script>

    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="tb">
        财务管理->特殊调帐统计录入
    </div>
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        AsyncPostBackTimeout="600" EnableScriptLocalization="true" ID="ToolkitScriptManager1"/>

    <script type="text/javascript" language="javascript">
        var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
        swpmIntance.add_initializeRequest(BeginRequestHandler);
        swpmIntance.add_pageLoading(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
        }
        function EndRequestHandler(sender, args) {
            try { MyExtHide(); } catch (ex) { }
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <!-- #include file="../../ErrorMsg.inc" -->
            <div class="con">
                <div class="card">
                    查询</div>
                <div class="kuang5">
                    <table class="text25" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 10%; height: 25px" align="right">
                                    行业名称:
                                </td>
                                <td style="width: 30%; height: 25px" valign="middle">
                                    <asp:DropDownList ID="selCalling" CssClass="inputmidder" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="selCalling_SelectedIndexChanged" />
                                </td>
                                <td style="width: 10%; height: 25px" align="right">
                                    单位名称:
                                </td>
                                <td style="width: 30%; height: 25px" valign="middle">
                                    <asp:DropDownList ID="selCorp" CssClass="inputmidder" runat="server" AutoPostBack="true" 
                                        OnSelectedIndexChanged="selCorp_SelectedIndexChanged"/>
                                </td>
                                <td style="width: 5%; height: 25px" align="right">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%; height: 25px" align="right">
                                    结算单元:
                                </td>
                                <td style="width: 30%; height: 25px" valign="middle">
                                    <asp:DropDownList ID="selBalUint" CssClass="inputmidder" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 5%; height: 25px" align="right">
                                    &nbsp;
                                </td>
                                <td style="height: 25px">
                                    <asp:Button ID="btnQuery" OnClick="btnQuery_Click" runat="server" CssClass="button1"
                                        Text="查询"></asp:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="kuang5">
                    <div class="gdtb" style="height: 300px">
                        <asp:GridView ID="gvResult" runat="server" AutoGenerateSelectButton="False" Width="95%"
                            CssClass="tab1" HeaderStyle-CssClass="tabbt" AlternatingRowStyle-CssClass="tabjg"
                            SelectedRowStyle-CssClass="tabsel" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="left"
                            PagerStyle-VerticalAlign="Top" AllowPaging="True" PageSize="1000" AutoGenerateColumns="false"
                            OnRowDataBound="gvResult_RowDataBound">
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            <SelectedRowStyle CssClass="tabsel" />
                            <HeaderStyle CssClass="tabbt" />
                            <AlternatingRowStyle CssClass="tabjg" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ItemCheckBox" runat="server"  AutoPostBack="true" OnCheckedChanged="on_check"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:BoundField DataField="CORP" HeaderText="单位" />
                                <asp:BoundField DataField="DEPART" HeaderText="部门" />
                                <asp:BoundField DataField="CARDNO" HeaderText="卡号" />
                                <asp:BoundField DataField="TRADEDATE" HeaderText="交易日期" />
                                <asp:BoundField DataField="TRADEMONEY" HeaderText="交易金额" />
                                <asp:BoundField DataField="STAFFNAME" HeaderText="录入员工" />
                                <asp:BoundField DataField="OPERATETIME" HeaderText="录入时间" />
                                <asp:BoundField DataField="REFUNDMENT" HeaderText="调帐金额" />
                                <asp:BoundField DataField="TRADEID" HeaderText="业务流水号" />
                            </Columns>
                            <EmptyDataTemplate>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                                    <tr class="tabbt">
                                        <td>
                                            <input type="checkbox" />
                                        </td>
                                        <td>
                                            部门
                                        </td>
                                        <td>
                                            卡号
                                        </td>
                                        <td>
                                            交易日期
                                        </td>
                                        <td>
                                            交易金额
                                        </td>
                                        <td>
                                            录入员工
                                        </td>
                                        <td>
                                            录入时间
                                        </td>
                                        <td>
                                            调帐金额
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div style="padding:5px 0 0 0">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="tab1">
	                        <tr>
                             <td width="8%"><div align="right"></div></td>
                                <td width="7%">
                                    
                                </td>
                                <td width="8%"><div align="right"></div></td>
                                <td width="7%">
                                </td>
                                <td width="8%"><div align="right"></div></td>
                                <td width="7%">
                                </td>
                                <td width="8%"><div align="right"></div></td>
                                <td width="7%">
                                </td>
                                <td width="8%"><div align="right"></div></td>
                                <td width="7%">
                                </td>
                                <td width="8%"><div align="right">总计：</div></td>
                                <td width="7%">
                                    <asp:Label ID="labCount" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="kuang5">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                        <tr>
                            <td>
                                <div align="right">
                                    商户代码:</div>
                            </td>
                            <td colspan="1">
                                <asp:TextBox ID="txtBalunitNo" CssClass="inputmid" runat="server" ReadOnly="true"></asp:TextBox>
                                <td>
                                    <div align="right">
                                        商户名称:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBalunitName" CssClass="inputmid" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                    <div align="right">
                                        应调金额:</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSdMoney" CssClass="input" runat="server" ReadOnly="true" Text="0"></asp:TextBox>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    调帐周期:</div>
                            </td>
                            <td>
                                <asp:DropDownList ID="selCycleDate" CssClass="inputmid" runat="server" />
                                <span class="red">*</span>
                            </td>
                            <td>
                                <div align="right">
                                    佣金率:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtcommission" runat="server" CssClass="inputshort" AutoPostBack="true" OnTextChanged="txtcommission_TextChanged"></asp:TextBox>
                                %
                            </td>
                            <td>
                                <div align="right">
                                    实际调帐金额:</div>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReMoney" runat="server" CssClass="input" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="right">
                                    备注:</div>
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtRemark" runat="server" CssClass="inputmidder"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="btns">
                <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnSubmit" CssClass="button1" runat="server" Text="提交"  OnClick="btnSubmit_Click"/>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
