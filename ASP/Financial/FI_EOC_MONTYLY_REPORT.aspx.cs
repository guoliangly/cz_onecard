﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common;
using PDO.Financial;
using Master;

public partial class ASP_Financial_FI_EOC_MONTYLY_REPORT : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Today.AddDays(-2).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.ToString("yyyyMMdd");

        }
    }
    
    private bool validate()
    {
        Validation valid = new Validation(context);

        if (Validation.isEmpty(txtFromDate))
        {
            context.AddError("查询时间不能为空");
        }
        else if (!Validation.isDate(txtFromDate.Text.Trim(), "yyyyMMdd"))
        {
            context.AddError("查询时间格式必须为yyyyMMdd");
        }

        if (Validation.isEmpty(txtToDate))
        {
            context.AddError("查询时间不能为空");
        }
        else if (!Validation.isDate(txtToDate.Text.Trim(), "yyyyMMdd"))
        {
            context.AddError("查询时间格式必须为yyyyMMdd");
        }

        if (txtFromDate != null && txtToDate != null)
        {
            valid.check(txtFromDate.Text.CompareTo(txtToDate.Text) <= 0, "开始日期不能大于结束日期");
        }

        if (context.hasError())
            return false;
        else
            return true;
    }
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);
        UserCardHelper.resetData(gvCWResult, null);

        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "DEPOSIT_FUND_COLLECT";
        pdo.var1 = txtFromDate.Text.Substring(0,8);
        pdo.var2 = txtToDate.Text.Substring(0, 8);

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);

        //hidNo.Value = pdo.var9;


        //根据财务目录统计
        SP_FI_QueryPDO pdo1 = new SP_FI_QueryPDO();
        pdo1.funcCode = "CW_DEPOSIT_FUND_COLLECT";
        pdo1.var1 = txtFromDate.Text.Substring(0, 8);
        pdo1.var2 = txtToDate.Text.Substring(0, 8);
        StoreProScene storePro1 = new StoreProScene();
        DataTable data1 = storePro1.Execute(context, pdo1);

        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
            btnPrint.Enabled = false;
        }
        else
        {
            btnPrint.Enabled = true;
        }

        UserCardHelper.resetData(gvResult, data);
        UserCardHelper.resetData(gvCWResult, data1);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    protected void lvwQuery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ControlDeal.RowDataBound(e);
    }
}
