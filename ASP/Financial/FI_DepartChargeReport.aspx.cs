﻿using System;
using System.Data;
using PDO.Financial;
using Master;
using Common;
using TM;
using System.Web.UI.WebControls;

public partial class ASP_Financial_FI_DepartChargeReport : Master.ExportMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //初始化日期
            TMTableModule tmTMTableModule = new TMTableModule();
            txtFromDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            txtToDate.Text = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

            selBalunit.Items.Add(new ListItem("---请选择---", ""));

            SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
            pdo.funcCode = "TD_M_INSIDEDEPARTITEM";
            StoreProScene storePro = new StoreProScene();
            DataTable data = storePro.Execute(context, pdo);
            if (data == null || data.Rows.Count == 0)
                return;

            for (int i = 0; i < data.Rows.Count; i++)
            {
                selBalunit.Items.Add(new ListItem(data.Rows[i]["DEPARTNAME"].ToString(), data.Rows[i]["DEPARTNO"].ToString()));
            }
        }
    }

    private double totalCharges = 0;

    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (gvResult.ShowFooter && e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    totalCharges += Convert.ToDouble(GetTableCellValue(e.Row.Cells[3]));
        //}
        //else if (e.Row.RowType == DataControlRowType.Footer)  //页脚 
        //{
        //    e.Row.Cells[0].Text = "总计";
        //    e.Row.Cells[3].Text = totalCharges.ToString("n");
        //}
    }
    private string GetTableCellValue(TableCell cell)
    {
        string s = cell.Text.Trim();
        if (s == "&nbsp;" || s == "")
            return "0";
        return s;
    }
    // 查询输入校验处理
    private void validate()
    {
        Validation valid = new Validation(context);

        bool b = Validation.isEmpty(txtFromDate);
        DateTime? fromDate = null, toDate = null;
        if (!b)
        {
            fromDate = valid.beDate(txtFromDate, "开始日期范围起始值格式必须为yyyyMMdd");
        }
        b = Validation.isEmpty(txtToDate);
        if (!b)
        {
            toDate = valid.beDate(txtToDate, "结束日期范围终止值格式必须为yyyyMMdd");
        }

        if (fromDate != null && toDate != null)
        {
            valid.check(fromDate.Value.CompareTo(toDate.Value) <= 0, "开始日期不能大于结束日期");
        }

    }

    // 查询处理
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        UserCardHelper.resetData(gvResult, null);

        validate();
        if (context.hasError()) return;

        SP_FI_QueryPDO pdo = new SP_FI_QueryPDO();
        pdo.funcCode = "DEPART_CHARGE_REPORT";
        pdo.var1 = txtFromDate.Text;
        pdo.var2 = txtToDate.Text;
        pdo.var7 = selBalunit.SelectedValue;

        StoreProScene storePro = new StoreProScene();
        DataTable data = storePro.Execute(context, pdo);



        if (data == null || data.Rows.Count == 0)
        {
            AddMessage("N005030001: 查询结果为空");
        }

        for (int i = 0; i < data.Rows.Count; i++)
        {
            decimal CZXJ = 0;
            decimal CZTZ = 0;
            decimal HJ = 0;

            CZXJ = Convert.ToDecimal(data.Rows[i]["现金"].ToString()) + Convert.ToDecimal(data.Rows[i]["账户宝"].ToString()) +
                    Convert.ToDecimal(data.Rows[i]["金福卡"].ToString()) + Convert.ToDecimal(data.Rows[i]["充值卡"].ToString())
                    + Convert.ToDecimal(data.Rows[i]["退卡"].ToString()) + Convert.ToDecimal(data.Rows[i]["银联"].ToString())
                    + Convert.ToDecimal(data.Rows[i]["翼支付"].ToString());
            data.Rows[i]["充值小计"] = CZXJ.ToString();

            CZTZ = Convert.ToDecimal(data.Rows[i]["特种售卡"].ToString()) + Convert.ToDecimal(data.Rows[i]["特种退款"].ToString());
            data.Rows[i]["特种小计"] = CZTZ.ToString();

            HJ = Convert.ToDecimal(data.Rows[i]["充值小计"].ToString())+Convert.ToDecimal(data.Rows[i]["售卡小计"].ToString())
                + Convert.ToDecimal(data.Rows[i]["充付器"].ToString()) + Convert.ToDecimal(data.Rows[i]["卡套"].ToString())
                + Convert.ToDecimal(data.Rows[i]["旅卡"].ToString())
                + Convert.ToDecimal(data.Rows[i]["停车收"].ToString()) + Convert.ToDecimal(data.Rows[i]["龙人自"].ToString()) + Convert.ToDecimal(data.Rows[i]["特种小计"].ToString());
            data.Rows[i]["合计"] = HJ.ToString();
        }

        UserCardHelper.resetData(gvResult, data);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvResult.Rows.Count > 0)
        {
            ExportGridView(gvResult);
        }
        else
        {
            context.AddMessage("查询结果为空，不能导出");
        }
    }
    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            string header = "交易类型";
            header += "#" + "网点" ;
            header += "#" + "充值" + " 充值小计,现金,账户宝,金福卡,充值卡,退卡,银联,翼支付";
            header += "#" + "售卡" + " 售卡小计,售卡,高龄卡";
            header += "#" + "其他" + " 充付器,卡套,旅卡";
            header += "#" + "代理业务" + " 停车收,龙人自";
            header += "#" + "特种卡" + " 特种小计,特种售卡,特种退款";
            header += "#" + "合计";
            GridViewHeaderHelper.SplitTableHeader(e.Row, header);
        }
    }
}
