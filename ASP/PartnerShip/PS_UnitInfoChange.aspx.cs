﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Master;
using Common;
using TM;
using TDO.BalanceChannel;
using PDO.PartnerShip;

public partial class ASP_PartnerShip_PS_UnitInfoChange : Master.Master
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TMTableModule tmTMTableModule = new TMTableModule();
            //从行业编码表(TD_M_CALLINGNO)中读取数据

            TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
            TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211", "TD_M_CALLING_ANDNO", null);

            //放入查询输入行业名称下拉列表中
            ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

            //设置单位信息中行业名称下拉列表值
            ControlDeal.SelectBoxFillWithCode(selCallingExt.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

            //设置有效标志下拉列表值
            TSHelper.initUseTag(selUseTag);

            //设置单位信息列表表头字段名
            lvwCorpQuery.DataSource = new DataTable();
            lvwCorpQuery.DataBind();
            lvwCorpQuery.SelectedIndex= -1;

            //指定GridView DataKeyNames
            lvwCorpQuery.DataKeyNames = new string[] {"CORPNO", "CORP","CALLINGNO", "LINKMAN", "CORPPHONE","CORPADD", "CORPMARK", "REMARK", "USETAG" };

            selCorp.Items.Add(new ListItem("---请选择---", ""));
        }
    }


    public void lvwCorpQuery_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwCorpQuery.PageIndex = e.NewPageIndex;
        lvwCorpQuery.DataSource = CreateCorpQueryDataSource();
        lvwCorpQuery.DataBind();

        lvwCorpQuery.SelectedIndex = -1;
        ClearCorp();

    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
         lvwCorpQuery.DataSource = CreateCorpQueryDataSource();
         lvwCorpQuery.DataBind();
         lvwCorpQuery.SelectedIndex = -1;
        
         ClearCorp();
    }

    public ICollection CreateCorpQueryDataSource()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从单位信息资料表(TD_M_CORP)中读取数据

        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        string strSql = "Select tcorp.USETAG ,tcorp.CORPNO,tcorp.CORP," +
                        "tcalling.CALLING,tcorp.CORPADD,tcorp.CORPMARK," +
                        "tcorp.LINKMAN,tcorp.CORPPHONE,tcorp.REMARK,tcorp.CALLINGNO";
        strSql += " From TD_M_CORP tcorp,TD_M_CALLINGNO tcalling ";

        ArrayList list = new ArrayList();
        list.Add("tcorp.CALLINGNO = tcalling.CALLINGNO(+) ");
        //list.Add("tcalling.ISOPEN = '1' ");
												
        if (selCalling.SelectedValue != "")
            list.Add("tcorp.CALLINGNO = '" + selCalling.SelectedValue + "'");

        if (selCorp.SelectedValue != "")
            list.Add("tcorp.CORPNO  = '" + selCorp.SelectedValue + "'");

        strSql += DealString.ListToWhereStr(list);

        strSql += " order by tcorp.CORPNO ";
        DataTable data = tmTMTableModule.selByPKDataTable(context, tdoTD_M_CORPIn, null, strSql, 0);
        DataView dataView = new DataView(data);
        return dataView;
    }


    private void CorpInputValidation()
    {
        //对单位编码进行非空,长度,数字的校验
        string strCorpNo = txtCorpNo.Text.Trim();
        if(strCorpNo == "")
            context.AddError("A008100102", txtCorpNo);
        else if(Validation.strLen(strCorpNo) != 4)
            context.AddError("A008100103", txtCorpNo);
        else if(!Validation.isCharNum(strCorpNo))
            context.AddError("A008100104", txtCorpNo);

        //对单位名称进行非空,长度校验
        string strCorp = txtCorp.Text.Trim();
        if (strCorp == "")
            context.AddError("A008100002", txtCorp);
        else if (Validation.strLen(strCorp) > 20)
            context.AddError("A008100003", txtCorp);

        //对行业进行非空的检验
        string strCalling = selCallingExt.SelectedValue;
        if (strCalling == "")
            context.AddError("A008100013", selCallingExt);

        //对联系人进行非空,长度校验
        String strLinkMan = txtLinkMan.Text.Trim();
        if (strLinkMan == "")
            context.AddError("A008100004", txtLinkMan);
        else if (Validation.strLen(strLinkMan) > 10)
            context.AddError("A008100005", txtLinkMan);

        //对联系电话进行非空,长度校验
        string strPhone = txtPhone.Text.Trim();
        if (strPhone == "")
            context.AddError("A008100006", txtPhone);
        else if (Validation.strLen(strPhone) > 40)
            context.AddError("A008100007", txtPhone);

        //对单位地址进行非空,长度校验
        string strAddr = txtAddr.Text.Trim();
        if (strAddr == "")
            context.AddError("A008100008", txtAddr);
        else if (Validation.strLen(strAddr) > 50)
            context.AddError("A008100009", txtAddr);

        //对单位说明进行长度校验
        string strCpRemrk = txtCpRemrk.Text.Trim();
        if (strCpRemrk != "")
        {
            if (Validation.strLen(strCpRemrk) > 50)
                context.AddError("A008100010", txtCpRemrk);
        }

        //对备注进行长度校验
        string strRemrk = txtRemark.Text.Trim();
        if (strRemrk != "")
        {
            if (Validation.strLen(strRemrk) > 100)
                context.AddError("A008100011", txtRemark);
        }

        //对有效标志进行非空检验
        String strUseTag = selUseTag.SelectedValue;
        if (strUseTag == "")
            context.AddError("A008100014", selUseTag);
    }

    private Boolean CorpAddValidation()
    {
        //对输入的单位信息检验
        CorpInputValidation();

        //对有效是否为有效的判断
        string strUseTag = selUseTag.SelectedValue;
        if ( strUseTag !="" && strUseTag != "1")
            context.AddError("A008100016", selUseTag);

        return CheckContext();
    }

    private Boolean CorpModifyValidation()
    {
        //检查是否选定单位的信息
        if (lvwCorpQuery.SelectedIndex == -1)
        {
            context.AddError("A008100001");
            return false;
        }

        //对输入的单位信息检验
        CorpInputValidation();

        //检查是否修改了选定的单位编码
        if(txtCorpNo.Text.Trim() != getDataKeys("CORPNO"))
        {
            context.AddError("A008100105", txtCorpNo);
            return false;
        }

        //检查是否修改了选定单位的行业名称
        if (selCallingExt.SelectedValue != getDataKeys("CALLINGNO"))
        {
            context.AddError("A008100015", selCallingExt);
            return false;
        }

        //当修改了选定单位的单位名称,检验是否库中已存在该单位
        else if (txtCorp.Text.Trim() != getDataKeys("CORP"))
        {
            //验证是否已存在该单位名称
           isNotExistCorpName();
        }
        return CheckContext();
    }


    private Boolean isNotExistCorpName()
    {
        
        TMTableModule tmTMTableModule = new TMTableModule();
        //从单位编码表中读取数据
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CORP = txtCorp.Text.Trim();

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, "TD_M_CORPNAME", null);
        if (tdoTD_M_CORPOutArr.Length != 0)
        {
            context.AddError("A008100017", txtCorp);
            return false;
        }
        return true;

    }


    private Boolean CheckContext()
    {
        //对context的error检测 
        if (context.hasError())
            return false;
        else
            return true;
    }

    private bool isExistCorpNo()
    {
        //是否已存在该单位编码
        TMTableModule tmTMTableModule = new TMTableModule();
        //从单位编码表中读取数据
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CORPNO = txtCorpNo.Text.Trim();

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, "TD_M_CORP_BYNO", null);
        
        if (tdoTD_M_CORPOutArr.Length != 0)
        {
            context.AddError("A008100106", txtCorpNo);
        }
        
        //是否存在该单位名称
        isNotExistCorpName();
 
        return context.hasError();
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //对输入的单位信息检验
        if (!CorpAddValidation()) return;

        //判断是否存在该单位编码 
        if (isExistCorpNo()) return;

        //执行单位信息增加的存储过程
        SP_PS_UnitInfoChangeAddPDO ddoSP_PS_UnitInfoChangeAddPDOIn = new SP_PS_UnitInfoChangeAddPDO();

        ddoSP_PS_UnitInfoChangeAddPDOIn.corpNo     = txtCorpNo.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.corp       = txtCorp.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.callingNo  = selCallingExt.SelectedValue;

        ddoSP_PS_UnitInfoChangeAddPDOIn.corpAdd    = txtAddr.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.corpMark   = txtCpRemrk.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.linkMan    = txtLinkMan.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.corpPhone  = txtPhone.Text.Trim();
        ddoSP_PS_UnitInfoChangeAddPDOIn.remark     = txtRemark.Text.Trim();
        

        bool ok = TMStorePModule.Excute(context, ddoSP_PS_UnitInfoChangeAddPDOIn);

        if (ok)
        {
            AddMessage("M008100113");


            //清除输入的单位信息
            //ClearCorp();

            //更新列表
            btnQuery_Click(sender, e);

        }
        
    }


    private void ClearCorp()
    {
        txtCorpNo.Text = "";
        txtCorp.Text = "";
        selCallingExt.SelectedValue = "";
        txtAddr.Text = "";
        txtCpRemrk.Text = "";
        txtLinkMan.Text = "";
        txtPhone.Text = "";
        selUseTag.SelectedValue = "";
        txtRemark.Text = "";

    }

    protected void btnModify_Click(object sender, EventArgs e)
    {
        //对输入的单位信息检验
        if (!CorpModifyValidation()) return;
        //执行单位信息修改的存储过程

        SP_PS_UnitInfoChangeModifyPDO ddoSP_PS_UnitInfoChangeModifyPDOIn = new SP_PS_UnitInfoChangeModifyPDO();

        ddoSP_PS_UnitInfoChangeModifyPDOIn.corpNo     = getDataKeys("CORPNO");
        ddoSP_PS_UnitInfoChangeModifyPDOIn.corp       = txtCorp.Text.Trim();
        ddoSP_PS_UnitInfoChangeModifyPDOIn.callingNo = selCallingExt.SelectedValue;
        ddoSP_PS_UnitInfoChangeModifyPDOIn.corpAdd    = txtAddr.Text.Trim();
        ddoSP_PS_UnitInfoChangeModifyPDOIn.corpMark   = txtCpRemrk.Text.Trim();
        ddoSP_PS_UnitInfoChangeModifyPDOIn.linkMan    = txtLinkMan.Text.Trim();
        ddoSP_PS_UnitInfoChangeModifyPDOIn.corpPhone  = txtPhone.Text.Trim();
        ddoSP_PS_UnitInfoChangeModifyPDOIn.useTag     = selUseTag.SelectedValue;
        ddoSP_PS_UnitInfoChangeModifyPDOIn.remark     = txtRemark.Text.Trim();
       

        bool ok = TMStorePModule.Excute(context, ddoSP_PS_UnitInfoChangeModifyPDOIn);
        if (ok)
        {
            AddMessage("M008100111");


            //清除输入的单位信息
            // ClearCorp();

            btnQuery_Click(sender, e); 
        }
    }

    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择行业后,设置单位名称下拉列表值
        if (selCalling.SelectedValue =="")
        {
            selCorp.Items.Clear();
        }
        else
        {
            TMTableModule tmTMTableModule = new TMTableModule();
            //从单位编码表(TD_M_CORP)中读取数据，放入下拉列表中

            TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
            tdoTD_M_CORPIn.CALLINGNO = selCalling.SelectedValue;

            TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, "TD_M_CORP", null);
            ControlDeal.SelectBoxFillWithCode(selCorp.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
        }

    }

    protected void lvwCorpQuery_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwCorpQuery','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void lvwCorpQuery_SelectedIndexChanged(object sender, EventArgs e)
    {

       //选择列表框一行记录后,设置单位信息增加,修改区域中数据
       txtCorp.Text   = getDataKeys("CORP");
       txtCorpNo.Text = getDataKeys("CORPNO");
       txtLinkMan.Text = getDataKeys("LINKMAN");
       txtPhone.Text   = getDataKeys("CORPPHONE");
       txtAddr.Text    = getDataKeys("CORPADD");
       txtCpRemrk.Text = getDataKeys("CORPMARK");
       txtRemark.Text  = getDataKeys("REMARK");
       selUseTag.SelectedValue = getDataKeys("USETAG");
       selCallingExt.SelectedValue = getDataKeys("CALLINGNO");

    }

    public string getDataKeys(string keysname)
    {
        return lvwCorpQuery.DataKeys[lvwCorpQuery.SelectedIndex][keysname].ToString();
    }
}
