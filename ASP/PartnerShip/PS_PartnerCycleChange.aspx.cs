﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using TDO.BalanceChannel;
using Common;
using TM;
using TDO.UserManager;
using TDO.PartnerShip;
using PDO.PartnerShip;
using TDO.BalanceParameter;
using System.Text.RegularExpressions;


public partial class ASP_PartnerShip_PS_PartnerCycleChange : Master.Master
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(gvResult, null);

        TMTableModule tmTMTableModule = new TMTableModule();
        //初始化查询输入的行业名称下拉列表框

        //从行业编码表(TD_M_CALLINGNO)中读取数据，放入查询输入行业名称下拉列表中


        TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
        TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211");

        ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

        gvResult.DataKeyNames = new string[] 
           {
               "BALUNITNO", "BALUNIT", "CALLINGNO",  "CALLINGNAME",
               "CORPNO", "CORPNAME", "DEPARTNO", "DEPARTNAME", "BEGINTIME",        
               "ENDTIME", "BALLEVEL", "BALINTERVAL", "FININTERVAL", "BALCYCLETYPECODE",           
               "FINCYCLETYPECODE", "BALCYCLETYPE", "FINCYCLETYPE"
           };

    }



    public void gvResult_Page(Object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);

    }


    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的行业名称后,初始化单位名称,初始化结算单元名称


        selCorp.Items.Clear();
        selDepart.Items.Clear();
        selBalUint.Items.Clear();

        InitCorp(selCalling, selCorp, "TD_M_CORPCALLUSAGE");

        //初始化结算单元(属于选择行业)名称下拉列表值

        InitBalUnit("00", selCalling);

    }
    protected void selCorp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的单位名称后,初始化部门名称,初始化结算单元名称


        //选定单位后,设置部门下拉列表数据
        if (selCorp.SelectedValue == "")
        {
            selDepart.Items.Clear();
            InitBalUnit("00", selCalling);
            return;
        }

        //初始化单位下的部门信息

        InitDepart(selCorp, selDepart, "TD_M_DEPARTUSAGE");

        //初始化结算单元(属于选择单位)名称下拉列表值

        InitBalUnit("01", selCorp);


    }
    protected void selDepart_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的部门名称后,初始化结算单元名称


        //选定单位后,设置部门下拉列表数据
        if (selDepart.SelectedValue == "")
        {
            InitBalUnit("01", selCorp);
            return;
        }

        //初始化结算单元(属于选择部门)名称下拉列表值

        InitBalUnit("02", selDepart);

    }


    protected void InitCorp(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        // 从单位编码表(TD_M_CORP)中读取数据，放入增加,修改区域中单位信息下拉列表中

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = origindwls.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }

    private void InitBalUnit(string balType, DropDownList dwls)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;

        //查询选定行业下的结算单元
        if (balType == "00")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCALLING", null);
        }

        //查询选定单位下的结算单元
        else if (balType == "01")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCORP", null);
        }

        //查询选定部门下的结算单元
        else if (balType == "02")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = selCorp.SelectedValue;
            tdoTF_TRADE_BALUNITIn.DEPARTNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYDEPART", null);
        }

        ControlDeal.SelectBoxFill(selBalUint.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);
    }

    //查询
    protected void btnQuery_Click(object sender, EventArgs e)
    {
        //将hidCorpNo置为空
        hidCorpNo.Value = "";

        clearContorls();

        BindResult();

        gvResult.SelectedIndex = -1;
    }

    private void BindResult()
    {
        //查询结算单元信息
        DataTable data = SPHelper.callPSQuery(context, "CycleChangeQuery", selCalling.SelectedValue,
            selCorp.SelectedValue, selDepart.SelectedValue, selBalUint.SelectedValue);

        UserCardHelper.resetData(gvResult, data);
    }

    //清空控件
    private void clearContorls()
    {
        txtBalUnitNo.Text = "";
        txtBalUnit.Text = "";
        selFinCyclType.SelectedValue = "";
        txtCycle.Text = "";
        txtBeginDate.Text = "";
        txtEndDate.Text = "";
        txtAdBeginDate.Text = "";
        txtAdEndDate.Text = "";
        txtRemark.Text = "";
    }

    protected void gvResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件

            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('gvResult','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void gvResult_SelectedIndexChanged(object sender, EventArgs e)
    {

        txtBalUnitNo.Text = getDataKeys("BALUNITNO");
        txtBalUnit.Text = getDataKeys("BALUNIT").Trim();
        selFinCyclType.SelectedValue = getDataKeys("FINCYCLETYPECODE").Trim();
        txtCycle.Text = getDataKeys("FININTERVAL").Trim();
        txtBeginDate.Text = getDataKeys("BEGINTIME").Trim();
        txtEndDate.Text = getDataKeys("ENDTIME").Trim();

        txtAdBeginDate.Text = txtBeginDate.Text;

        hidCorpNo.Value = getDataKeys("CORPNO").Trim();

        txtAdEndDate.Text = "";

        txtBalunitCount.Text = "1";

        //是否含有部门
        DataTable dt = SPHelper.callPSQuery(context, "BalunitCount", hidCorpNo.Value);

        try
        {
            int balunitcount = Convert.ToInt32(dt.Rows[0][0].ToString());
            if (balunitcount > 1)
            {
                txtBalunitCount.Text = "0";        //含有部门
            }
            else
            {
                txtBalunitCount.Text = "1";        //不含部门
            }
        }
        catch
        {
            txtBalunitCount.Text = "1";
        }

        if (txtBalunitCount.Text == "0")
        {
            labEx.Visible = true;
        }
        else
        {
            labEx.Visible = false;
        }
    }

    public String getDataKeys(string keysname)
    {
        string value = gvResult.DataKeys[gvResult.SelectedIndex][keysname].ToString();

        return value == "" ? "" : value;
    }

    //读取部门资料
    private void InitDepart(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从部门编码表(TD_M_CDEPART)中读取数据，放入下拉列表中


        TD_M_DEPARTTDO tdoTD_M_DEPARTIn = new TD_M_DEPARTTDO();
        tdoTD_M_DEPARTIn.CORPNO = origindwls.SelectedValue;

        TD_M_DEPARTTDO[] tdoTD_M_DEPARTOutArr = (TD_M_DEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_DEPARTIn, typeof(TD_M_DEPARTTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_DEPARTOutArr, "DEPART", "DEPARTNO", true);
    }

    protected void btnConfirm(object sender, EventArgs e)
    {
        validateFor();
        if (context.hasError()) return;

        context.SPOpen();
        context.AddField("p_cropNo").Value = hidCorpNo.Value;
        context.AddField("p_finCycleTypeCode").Value = selFinCyclType.SelectedValue;
        context.AddField("p_finInterval").Value = txtCycle.Text;
        context.AddField("p_AdEndDate").Value = txtAdEndDate.Text;
        context.AddField("p_Remark").Value = txtRemark.Text;

        bool ok = context.ExecuteSP("SP_PS_PARTNERCYCLECHANGE");
        if (ok)
        {
            AddMessage("M009478001");
            btnQuery_Click(sender, e);
        }
    }


    //提交
    protected void btnOk_Click(object sender, EventArgs e)
    {
        validateFor();
        if (context.hasError()) return;

        context.SPOpen();
        context.AddField("p_cropNo").Value = hidCorpNo.Value;
        context.AddField("p_finCycleTypeCode").Value = selFinCyclType.SelectedValue;
        context.AddField("p_finInterval").Value = txtCycle.Text;
        context.AddField("p_AdEndDate").Value = txtAdEndDate.Text;//wdx 账期结束时间默认往后延1天。DateTime.ParseExact(txtAdEndDate.Text, "yyyyMMdd", null).AddDays(1).ToString("yyyyMMdd");
        context.AddField("p_Remark").Value = txtRemark.Text;

        bool ok = context.ExecuteSP("SP_PS_PARTNERCYCLECHANGE");
        if (ok)
        {
            AddMessage("M009478001");

            BindResult();

            gvResult_SelectedIndexChanged(sender, e);
        }
    }

    private void validateFor()
    {
        if (txtRemark.Text.Length > 50)
        {
            context.AddError("A001001129:备注长度大于50位", txtRemark);
        }
        if (selFinCyclType.SelectedValue == "")
        {
            context.AddError("A009478013:请选择划账周期", selFinCyclType);
        }
        if (string.IsNullOrEmpty(txtCycle.Text.Trim()))
        {
            context.AddError("A009478014:划账周期跨度不能为空", txtCycle);
        }
        else
        {
            //校验划账周期跨度
            Regex regex = new Regex("^[0-9]*$");
            Match match = regex.Match(txtCycle.Text.Trim());
            if (!match.Success)
            {
                context.AddError("A009478010:划账周期跨度格式不正确", txtCycle);
            }
        }

        if (string.IsNullOrEmpty(txtAdEndDate.Text.Trim()))
        {
            context.AddError("A009478015:划账周期结束日期不能为空", txtAdEndDate);
        }
        else
        {
            if (!Validation.isDate(txtAdEndDate.Text.Trim(), "yyyyMMdd"))
            {
                context.AddError("A009478012:结束日期输入格式不正确", txtAdEndDate);
            }
        }
    }

    // 判断是否需要审批（如果只修改了联系人，联系电话，联系地址，电子邮件时，不需要审批）
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[9].Visible = false;
        }
    }

    private bool isTrivialInfoChanged()
    {
        return false;
    }

    private Boolean isInteger(string strInput)
    {
        System.Text.RegularExpressions.Regex reg1
                          = new System.Text.RegularExpressions.Regex(@"^[1-9][0-9]*$");

        return reg1.IsMatch(strInput);
    }

    //划账周期改变时
    protected void selFinCyclType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //校验帐期起始日期
        if (string.IsNullOrEmpty(txtBeginDate.Text.Trim()))
        {
            return;
        }

        //校验划账周期跨度
        Regex regex = new Regex("^[0-9]*$");
        Match match = regex.Match(txtCycle.Text.Trim());
        if (!match.Success)
        {
            context.AddError("A009478010:划账周期跨度格式不正确", txtCycle);
            return;
        }

        if (selFinCyclType.SelectedValue == "")
        {
            return;
        }
        else if (selFinCyclType.SelectedValue == "01")      //天
        {
            txtAdEndDate.Text = DateTime.ParseExact(txtBeginDate.Text.Trim(), "yyyyMMdd", null).AddDays(Convert.ToDouble(txtCycle.Text.Trim())).ToString("yyyyMMdd");
        }
        else if (selFinCyclType.SelectedValue == "02")      //周
        {
            txtAdEndDate.Text = DateTime.ParseExact(txtBeginDate.Text.Trim(), "yyyyMMdd", null).AddDays(Convert.ToDouble(txtCycle.Text.Trim()) * 7).ToString("yyyyMMdd");
        }
        else if (selFinCyclType.SelectedValue == "03")      //固定月
        {
            string endDate = DateTime.ParseExact(txtBeginDate.Text.Trim(), "yyyyMMdd", null).AddMonths(Convert.ToInt32(txtCycle.Text.Trim())).ToString("yyyyMMdd");
            if (Convert.ToInt32(endDate.Substring(6, 2).ToString()) > 28)
            {
                context.AddError("A009478011:固定月日期不允许大于28日", selFinCyclType);
                return;
            }

            txtAdEndDate.Text = DateTime.ParseExact(txtBeginDate.Text.Trim(), "yyyyMMdd", null).AddMonths(Convert.ToInt32(txtCycle.Text.Trim())).ToString("yyyyMMdd");
        }
        else if (selFinCyclType.SelectedValue == "04")      //自然月
        {
            txtAdEndDate.Text = DateTime.ParseExact(txtBeginDate.Text.Substring(0, 6) + "01", "yyyyMMdd", null).AddMonths(Convert.ToInt32(txtCycle.Text.Trim())).ToString("yyyyMMdd");
        }

        txtAdBeginDate.Text = txtBeginDate.Text;
    }

    protected void txtCycle_OnTextChanged(object sender, EventArgs e)
    {
        selFinCyclType_SelectedIndexChanged(sender, e);
    }

    //调整结束日期校验
    protected void txtAdEndDate_OnTextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtAdEndDate.Text))
        {
            if (!Validation.isDate(txtAdEndDate.Text.Trim(), "yyyyMMdd"))
            {
                context.AddError("A009478012:结束日期输入格式不正确", txtAdEndDate);
                return;
            }
        }

        if (string.IsNullOrEmpty(txtAdBeginDate.Text))
        {
            return;
        }

        DateTime beginTime = DateTime.ParseExact(txtAdBeginDate.Text.Trim(), "yyyyMMdd", null);
        DateTime endTime = DateTime.ParseExact(txtAdEndDate.Text.Trim(), "yyyyMMdd", null);

        if (DateTime.Compare(beginTime, endTime) > 0)
        {
            context.AddError("调整的结束日期不能小于" + txtAdBeginDate.Text, txtAdEndDate);
            return;
        }
    }
}
