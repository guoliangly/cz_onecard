﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PS_UnitInfoChange.aspx.cs" Inherits="ASP_PartnerShip_PS_UnitInfoChange" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">    
    <title>单位信息维护</title>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
   
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
       <div class="tb">合作伙伴->单位信息维护</div>
      
       <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
           
           <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
            
            <div class="con">
                <div class="card">查询单位信息</div>
                <div class="kuang5">
                <table width="95%" border="0" cellpadding="0" cellspacing="0" class="12text">
                 <tr>
                   <td width="12%"><div align="right">行业名称:</div></td>
                   <td width="15%">
                     <asp:DropDownList ID="selCalling" CssClass="inputmid" runat="server" OnSelectedIndexChanged="selCalling_SelectedIndexChanged" AutoPostBack="true">
                     </asp:DropDownList>
                   </td>
                   <td width="10%"><div align="right">单位名称:</div></td>
                   <td width="30%">
                     <asp:DropDownList ID="selCorp" CssClass="inputmidder" runat="server">
                     </asp:DropDownList>
                   </td>
                   <td width="26%"><asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" OnClick="btnQuery_Click" /></td>
                 </tr>
                </table>
                </div>
                <div class="jieguo">单位信息列表</div>
                <div class="kuang5">
                <div id="gdtb" style="height:290px">
                <asp:GridView ID="lvwCorpQuery" runat="server"
                    Width = "1000"
                    CssClass="tab1"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="10"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="False"
                    OnPageIndexChanging="lvwCorpQuery_Page"
                    OnSelectedIndexChanged="lvwCorpQuery_SelectedIndexChanged"
                    OnRowCreated="lvwCorpQuery_RowCreated">
                    <Columns>
                      <asp:TemplateField HeaderText="有效标志">
                       <ItemTemplate>
                         <asp:Label ID="labUseTag" Text='<%# Eval("USETAG").ToString() == "1" ? "有效" : "无效" %>' runat="server"></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateField>
                      
                    
                      <asp:BoundField DataField="CORPNO"    HeaderText="单位编码"/>
                      <asp:BoundField DataField="CORP"      HeaderText="单位名称"/>
                      <asp:BoundField DataField="CALLING"   HeaderText="行业名称"/>
                      <asp:BoundField DataField="LINKMAN"   HeaderText="联系人"  />
                      <asp:BoundField DataField="CORPPHONE" HeaderText="联系电话"/>
                      <asp:BoundField DataField="CORPADD"   HeaderText="单位地址"/>
                      <asp:BoundField DataField="CORPMARK"  HeaderText="单位说明"/>
                     
                    </Columns>     
                  
                   <EmptyDataTemplate>
                   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                     <tr class="tabbt">
                      <td>有效标志</td>
                      <td>单位编码</td>
                      <td>单位名称</td>
                      <td>行业名称</td>
                      <td>联系人</td>
                      <td>联系电话</td>
                      <td>单位地址</td>
                      <td>单位说明</td>
                    </tr>
                   </table>
                  </EmptyDataTemplate>
                </asp:GridView>
                </div>
                <%--<div id="gdtb" style="height:100px"></div>--%>
               
                </div>
                <div class="card">单位信息</div>
                <div class="kuang5">
                <table width="95%" border="0" cellpadding="0" cellspacing="0" class="12text">
                
                 <tr>
                   <td width="9%"><div align="right" >单位编码:</div></td>
                   <td>
                    <asp:TextBox runat="server" CssClass="input" MaxLength="4" ID="txtCorpNo"> </asp:TextBox>
                     <span class="red">*</span>
                     
                   </td>
                  
                   <td width="9%"><div align="right">单位名称:</div></td>
                   <td >
                    <asp:TextBox ID="txtCorp" runat="server" CssClass="inputmid" MaxLength="40" ></asp:TextBox>
                   
                    <span class="red">*</span>
                   <td width="8%"><div align="right">行业名称:</div></td>
                   <td width="34%">
                      <asp:DropDownList ID="selCallingExt" CssClass="inputmid" runat="server">
                      </asp:DropDownList>
                    <span class="red">*</span></td>
                 </tr>
                 
                 <tr>
                   <td><div align="right">联系人:</div></td>
                   <td><asp:TextBox ID="txtLinkMan" runat="server" CssClass="input" MaxLength="10" ></asp:TextBox>
                     <span class="red"> *</span></td>
                   <td><div align="right">联系电话:</div></td>
                   <td><asp:TextBox ID="txtPhone" runat="server" CssClass="inputmid" MaxLength="40" ></asp:TextBox>
                     <span class="red"> *</span></td>
                   <td><div align="right">单位说明:</div></td>
                   <td><asp:TextBox ID="txtCpRemrk" runat="server" CssClass="inputlong" MaxLength="50" ></asp:TextBox></td>
                 </tr>

                 <tr>
                   <td><div align="right">单位地址:</div></td>
                   <td colspan="3"><asp:TextBox ID="txtAddr" runat="server" CssClass="inputlong" MaxLength="50" ></asp:TextBox>
                    <span class="red">*</span></td>
                   <td><div align="right">备注:</div></td>
                   <td><asp:TextBox ID="txtRemark" runat="server" CssClass="inputlong" MaxLength="100" ></asp:TextBox></td>
                 </tr>

                 <tr>
                   <td><div align="right">有效标志:</div></td>
                   <td>
                      <asp:DropDownList ID="selUseTag" CssClass="input" runat="server">
                      </asp:DropDownList>
                     <span class="red"> *</span>
                   </td>
                   <td>&nbsp;</td>
                   <td colspan="3">
                     <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
                       <tr>
                         <td height="24"><asp:Button ID="btnAdd" runat="server" Text="增加" CssClass="button1" OnClick="btnAdd_Click" /></td>
                         <td><asp:Button ID="btnModify" runat="server" Text="修改" CssClass="button1" OnClick="btnModify_Click" /></td>
                       </tr>
                     </table>
                   </td>
                  </tr>
                </table>
                </div>
                </div>
          </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
