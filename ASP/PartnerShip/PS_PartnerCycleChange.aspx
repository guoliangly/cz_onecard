﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PS_PartnerCycleChange.aspx.cs" Inherits="ASP_PartnerShip_PS_PartnerCycleChange" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>商户帐期查询配置维护</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <link href="../../css/card.css" rel="stylesheet" type="text/css" />
     
     <script type="text/javascript">
         function submitCycle() {
             alert('1');
             if ($get(txtBalunitCount).value = "0") {
                 alert('1');
                 MyExtConfirm("提示", "此结算单元含有部门级账单或者为部门级账单，是否一起调帐？", submitConfirm);
             }
             else {
                 $get('btnConfirm').click();
             }
         }

         function submitConfirm(btn) {
             if (btn == 'yes') {
                 $get('btnConfirm').click();
             }
         }
     </script>

</head>
<body>
    <form id="form1" runat="server">
     <div class="tb">合作伙伴->商户帐期查询配置维护</div>
     <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>              
       <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
        <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
       </ContentTemplate></asp:UpdatePanel>
            <div class="con">
              <div class="jieguo">查询帐期信息</div>
              <div class="kuang5">
              <table width="98%" border="0" cellpadding="0" cellspacing="0" class="text25">
                <tr>
                  <td align="right">行业名称:</td>
                  <td><asp:DropDownList ID="selCalling" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCalling_SelectedIndexChanged">
                     </asp:DropDownList>  
                  </td>
                  <td align="right">单位名称:</td>
                    <td><asp:DropDownList ID="selCorp" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCorp_SelectedIndexChanged">
                     </asp:DropDownList>
		            </td>

                </tr>
                <tr>
                    <td align="right">部门名称:</td>
                    <td><asp:DropDownList ID="selDepart" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selDepart_SelectedIndexChanged">
                      </asp:DropDownList>
                     </td>
                  <td align="right">结算单元:</td>
                  <td><asp:DropDownList ID="selBalUint" CssClass="inputmidder" runat="server">
                      </asp:DropDownList>
                      
                  </td>
                  <td><asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" 
                        OnClick="btnQuery_Click" /></td>
                </tr>
              </table>
              
              <div class="gdtb" style="height:260px">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"  RenderMode="Inline">
                <ContentTemplate>

                 <asp:GridView ID="gvResult" runat="server"
                    CssClass="tab1"
                    Width ="98%"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="1000"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="Left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="False"
                    OnPageIndexChanging="gvResult_Page"
                    OnSelectedIndexChanged="gvResult_SelectedIndexChanged"
                    OnRowCreated="gvResult_RowCreated"
                    OnRowDataBound="gvResult_RowDataBound"
                    EmptyDataText="没有数据记录!"
                    >
                    <Columns>
                        <asp:BoundField DataField="BALUNITNO"      HeaderText="结算单元编码"/> 
                        <asp:BoundField DataField="BALUNIT"        HeaderText="结算单元名称"/>
                        <asp:BoundField DataField="BALLEVEL"    HeaderText="结算级别"/>
                        <asp:BoundField DataField="BALCYCLETYPE"    HeaderText="结算周期类型"/>
                        <asp:BoundField DataField="BALINTERVAL"    HeaderText="结算周期跨度"/>
                        <asp:BoundField DataField="FINCYCLETYPE"     HeaderText="划账周期类型"/>
                        <asp:BoundField DataField="FININTERVAL"     HeaderText="划账周期跨度"/>
                        <asp:BoundField DataField="BEGINTIME"     HeaderText="当前帐期开始时间" DataFormatString="{0:yyyy-MM-dd}"/>
                        <asp:BoundField DataField="ENDTIME"     HeaderText="当前帐期结束时间" DataFormatString="{0:yyyy-MM-dd}"/>
                        <asp:BoundField DataField="CORPNO"      HeaderText="单位编码" />
                   </Columns>
                   
                   <EmptyDataTemplate>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                         <tr class="tabbt">
                          <td>结算单元编码</td>
                          <td>结算单元名称</td>
                          <td>结算级别</td>
                          <td>结算周期类型</td>
                          <td>结算周期跨度</td>
                          <td>划账周期类型</td>
                          <td>划账周期跨度</td>
                          <td>当前帐期开始时间</td>
                          <td>当前帐期结束时间</td>
                         </tr>
                      </table>
                   </EmptyDataTemplate>
                  </asp:GridView>
             
                    </ContentTemplate></asp:UpdatePanel>
                </div>

              </div>
            </div>
            
            <asp:UpdatePanel ID="UpdatePanel3" runat="server"  RenderMode="Inline">
            <ContentTemplate>
            <div class="con">
                <asp:HiddenField ID="hidCorpNo" runat="server" />
                <asp:TextBox ID="txtBalunitCount" runat="server" Visible="false"></asp:TextBox>
             <%--<div class="base">结算单元信息</div>--%>
             <div class="kuang5">
               <table width="880" border="0" cellpadding="0" cellspacing="0" class="text25">
                
                 <tr>
                   
                   <td><div align="right">结算单元编码:</div></td>
                   <td >
                     <asp:TextBox ID="txtBalUnitNo" CssClass="input" runat="server" MaxLength="8" ReadOnly="true"></asp:TextBox>
                       <asp:Label ID="labEx" runat="server" Text="此结算单元含有部门级账单或者为部门级账单" ForeColor="Red" Visible="false"></asp:Label>
                   </td>
                 <td><div align="right">&nbsp;</div></td>
                   <td>
                      &nbsp;
                   </td>
                   
                   <td><div align="right">划账周期:</div></td>
                   <td>
                      <asp:DropDownList ID="selFinCyclType" CssClass="input" runat="server"
                       AutoPostBack="true" OnSelectedIndexChanged="selFinCyclType_SelectedIndexChanged">
                        <asp:ListItem Text="---请选择---" Value="" ></asp:ListItem>
                        <asp:ListItem Text="01:天" Value="01" ></asp:ListItem>
                        <asp:ListItem Text="02:周" Value="02" ></asp:ListItem>
                        <asp:ListItem Text="03:固定月" Value="03" ></asp:ListItem>
                        <asp:ListItem Text="04:自然月" Value="04" ></asp:ListItem>
                      </asp:DropDownList>
                   </td>
                   </tr>
                   
                 <tr>
                   <td><div align="right">结算单元:</div></td>
                   <td colspan="3">
                      <asp:TextBox ID="txtBalUnit" runat="server" CssClass="inputmid" ReadOnly="true"></asp:TextBox>
                   </td>
                   <td><div align="right">划账周期跨度:</div></td>
                   <td>
                       <asp:TextBox ID="txtCycle" runat="server" CssClass="input" AutoPostBack="true" OnTextChanged="txtCycle_OnTextChanged"></asp:TextBox>
                   </td>
                 </tr>
                 <tr>
                   <td><div align="right">当前帐期时间:</div></td>
                   <td colspan="3">
                       <asp:TextBox ID="txtBeginDate" runat="server" CssClass="input" ReadOnly="true"></asp:TextBox>
                        -
                       <asp:TextBox ID="txtEndDate" runat="server" CssClass="input" ReadOnly="true"></asp:TextBox>
                   </td>
                   <td><div align="right">调整后帐期时间:</div></td>
                   <td>
                       <asp:TextBox ID="txtAdBeginDate" runat="server" CssClass="input" Enabled="false"></asp:TextBox>
                        -
                       <asp:TextBox ID="txtAdEndDate" runat="server" CssClass="input" AutoPostBack="true" OnTextChanged="txtAdEndDate_OnTextChanged"></asp:TextBox>
                      <ajaxToolkit:CalendarExtender ID="CalendarExtender1" 
                          runat="server" TargetControlID="txtAdEndDate" Format="yyyyMMdd"  PopupPosition="TopLeft" />
                   </td>
                 </tr>
                 <tr>
                   <td><div align="right">备注:</div></td>
                   <td colspan="3">
                       <asp:TextBox ID="txtRemark" runat="server" CssClass="inputlong"></asp:TextBox>
                   </td>
                   <td><div align="right"></div></td>
                   <td>
                   </td>
                 </tr>
               </table>

             <div class="footall" style="height:1px;"></div>
             </div>

            </div>
          

             <div class="btns">
               <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
                 <tr>
                   <td><asp:Button ID="btnOk" runat="server" Text="确认" CssClass="button1" OnClick="btnOk_Click"/></td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
             </div>
            
            </ContentTemplate>

        </asp:UpdatePanel>
    </form>
</body>
</html>
