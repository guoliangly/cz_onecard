﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using TDO.BalanceChannel;
using Common;
using TM;
using TDO.UserManager;
using TDO.PartnerShip;
using PDO.PartnerShip;
using TDO.BalanceParameter;


public partial class ASP_PartnerShip_PS_TransferDetailChange : Master.Master
{
    private void getNextBalUnitNo()
    {
        DataTable dt = ChargeCardHelper.callQuery(context, "NEXTBALUNITNO");
        txtBalUnitNo.Text = (selCallingExt.SelectedValue == "" 
            ? "0B00" : (selCallingExt.SelectedValue + "00")) + dt.Rows[0].ItemArray[0];
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        UserCardHelper.resetData(lvwBalUnits, null);

        TMTableModule tmTMTableModule = new TMTableModule();
        //初始化查询输入的行业名称下拉列表框

        //从行业编码表(TD_M_CALLINGNO)中读取数据，放入查询输入行业名称下拉列表中


        TD_M_CALLINGNOTDO tdoTD_M_CALLINGNOIn = new TD_M_CALLINGNOTDO();
        TD_M_CALLINGNOTDO[] tdoTD_M_CALLINGNOOutArr = (TD_M_CALLINGNOTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CALLINGNOIn, typeof(TD_M_CALLINGNOTDO), "S008100211");

        ControlDeal.SelectBoxFillWithCode(selCalling.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);

        //初始化结算单元信息增加修改区域的字段初始值


        //初始化行业名称下拉列表值

        ControlDeal.SelectBoxFillWithCode(selCallingExt.Items, tdoTD_M_CALLINGNOOutArr, "CALLING", "CALLINGNO", true);


        //从内部员工编码表(TD_M_INSIDESTAFF)中读取数据,初始化商户经理下拉列表值

        TD_M_INSIDESTAFFTDO tdoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        TD_M_INSIDESTAFFTDO[] tdoTD_M_INSIDESTAFFOutArr = (TD_M_INSIDESTAFFTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), "S008111111", "TD_M_INSIDESTAFF_SVR", null);

        //ControlDeal.SelectBoxFill(selSerMgr.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);
        ControlDeal.SelectBoxFill(selMsgQry.Items, tdoTD_M_INSIDESTAFFOutArr, "STAFFNAME", "STAFFNO", true);


        //初始化转出银行,开户银行下拉列表值

        TD_M_BANKTDO ddoTD_M_BANKIn = new TD_M_BANKTDO();
        TD_M_BANKTDO[] ddoTD_M_BANKOutArr = (TD_M_BANKTDO[])tmTMTableModule.selByPKArr(context, ddoTD_M_BANKIn, typeof(TD_M_BANKTDO), "S008107211", "", null);

        //初始化合帐结算单元列表
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), "S008100212", "TF_BALUNITNO_DETAIL", null);
        ControlDeal.SelectBoxFillWithCode(selIntegrateBalunit.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);


        getNextBalUnitNo();



        //指定GridView  lvwBalUnits DataKeyNames
        lvwBalUnits.DataKeyNames = new string[] 
           {
               "TRADEID", "COMSCHEMENO", "BEGINTIME",  "ENDTIME",
               "BALUNITNO", "BALUNIT", "BALUNITTYPECODE", "SOURCETYPECODE", "SERMANAGERCODE",        
               "CALLINGNO", "CORPNO", "DEPARTNO", "FINBANKCODE", "BANKCODE",           
               "BANKACCNO", "BALLEVEL", "BALCYCLETYPECODE", "BALINTERVAL",             
               "FINCYCLETYPECODE", "FININTERVAL", "FINTYPECODE",  "UNITEMAIL",                     
               "COMFEETAKECODE", "USETAG", "LINKMAN", "UNITADD", "UNITPHONE", "REMARK"
           };


        //初始化佣金规则名称下拉列表

        TF_TRADE_COMSCHEMETDO ddoTF_TRADE_COMSCHEMEIn = new TF_TRADE_COMSCHEMETDO();
        TF_TRADE_COMSCHEMETDO[] ddoTF_TRADE_COMSCHEMEOutArr = (TF_TRADE_COMSCHEMETDO[])tmTMTableModule.selByPKArr(context, ddoTF_TRADE_COMSCHEMEIn, typeof(TF_TRADE_COMSCHEMETDO), "S008107212", "TF_TRADE_COMSCHEME_ALL_USEAGE", null);

    }



    public void lvwBalUnits_Page(Object sender, GridViewPageEventArgs e)
    {
        lvwBalUnits.PageIndex = e.NewPageIndex;
        btnQuery_Click(sender, e);
        ClearBalUnit();

    }


    protected void selCalling_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的行业名称后,初始化单位名称,初始化结算单元名称

        selCorp.Items.Clear();
        selDepart.Items.Clear();
        selBalUint.Items.Clear();

        InitCorp(selCalling, selCorp, "TD_M_CORPCALLUSAGE");

        //初始化结算单元(属于选择行业)名称下拉列表值
        InitBalUnit("00", selCalling);

    }
    protected void selCorp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的单位名称后,初始化部门名称,初始化结算单元名称

        //选定单位后,设置部门下拉列表数据
        if (selCorp.SelectedValue == "")
        {
            selDepart.Items.Clear();
            InitBalUnit("00", selCalling);
            return;
        }

        //初始化单位下的部门信息
        InitDepart(selCorp, selDepart, "TD_M_DEPARTUSAGE");

        //初始化结算单元(属于选择单位)名称下拉列表值
        InitBalUnit("01", selCorp);


    }
    protected void selDepart_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择查询的部门名称后,初始化结算单元名称

        //选定单位后,设置部门下拉列表数据
        if (selDepart.SelectedValue == "")
        {
            InitBalUnit("01", selCorp);
            return;
        }

        //初始化结算单元(属于选择部门)名称下拉列表值
        InitBalUnit("02", selDepart);

    }


    protected void InitCorp(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        // 从单位编码表(TD_M_CORP)中读取数据，放入增加,修改区域中单位信息下拉列表中

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_CORPTDO tdoTD_M_CORPIn = new TD_M_CORPTDO();
        tdoTD_M_CORPIn.CALLINGNO = origindwls.SelectedValue;

        TD_M_CORPTDO[] tdoTD_M_CORPOutArr = (TD_M_CORPTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_CORPOutArr, "CORP", "CORPNO", true);
    }

    private void InitBalUnit(string balType, DropDownList dwls)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        TF_TRADE_BALUNITTDO tdoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        TF_TRADE_BALUNITTDO[] tdoTF_TRADE_BALUNITOutArr = null;

        //查询选定行业下的结算单元
        if (balType == "00")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCALLING", null);
        }

        //查询选定单位下的结算单元
        else if (balType == "01")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYCORP", null);
        }

        //查询选定部门下的结算单元
        else if (balType == "02")
        {
            tdoTF_TRADE_BALUNITIn.CALLINGNO = selCalling.SelectedValue;
            tdoTF_TRADE_BALUNITIn.CORPNO = selCorp.SelectedValue;
            tdoTF_TRADE_BALUNITIn.DEPARTNO = dwls.SelectedValue;
            tdoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, tdoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNITALL_BYDEPART", null);
        }

        ControlDeal.SelectBoxFill(selBalUint.Items, tdoTF_TRADE_BALUNITOutArr, "BALUNIT", "BALUNITNO", true);
    }


    protected void btnQuery_Click(object sender, EventArgs e)
    {
        hidAprvState.Value = selAprvState.SelectedValue;

        //查询结算单元信息
        DataTable data = SPHelper.callPSQuery(context, "QueryBalUnit", selCalling.SelectedValue,
            selCorp.SelectedValue, selDepart.SelectedValue, selBalUint.SelectedValue, 
            selAprvState.SelectedValue, selMsgQry.SelectedValue);

        UserCardHelper.resetData(lvwBalUnits, data);

        ClearBalUnit();

       // InitCorpExt();
       // InitDepart();
    }

    protected void lvwBalUnits_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //注册行单击事件
            e.Row.Attributes.Add("onclick", "javascirpt:__doPostBack('lvwBalUnits','Select$" + e.Row.RowIndex + "')");
        }
    }

    public void lvwBalUnits_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        txtBalUnitNo.Text           = getDataKeys("BALUNITNO");
        txtBalUnit.Text             = getDataKeys("BALUNIT").Trim();

        
        try
        {
            //显示行业的名称
            selCallingExt.SelectedValue = getDataKeys("CALLINGNO").Trim();

            //初始行业下的单位名称
            InitCorp(selCallingExt, selCorpExt, "TD_M_CORPCALLUSAGE");
            selCorpExt.SelectedValue = GetCorpCode(getDataKeys("CORPNO").Trim());
            
            //初始单位下的部门名称
            InitDepart(selCorpExt, selDepartExt, "TD_M_DEPARTUSAGE");
            selDepartExt.SelectedValue  = GetDepartCode(getDataKeys("DEPARTNO").Trim());

        }
        catch (Exception)
        {
           selCallingExt.SelectedValue = "";
           selCorpExt.SelectedValue = "";
           selDepartExt.SelectedValue = "";
        }
       
        hidSeqNo.Value = getDataKeys("TRADEID");

       
    }

    private string GetCorpCode(string strCorpCode)
    {
        TD_M_CORPTDO ddoTD_M_CORPIn = new TD_M_CORPTDO();
        ddoTD_M_CORPIn.CORPNO = strCorpCode;

        TMTableModule tmTMTableModule = new TMTableModule();

        TD_M_CORPTDO ddoTD_M_CORPOut = (TD_M_CORPTDO)tmTMTableModule.selByPK(context, ddoTD_M_CORPIn, typeof(TD_M_CORPTDO), null, "TD_M_CORP_BYNO", null);

        if (ddoTD_M_CORPOut == null || ddoTD_M_CORPOut.USETAG == "0")
            return "";
        else
            return strCorpCode;

    }

    private string GetDepartCode(string strDepartCode)
    {
        TD_M_DEPARTTDO ddoTD_M_DEPARTIn = new TD_M_DEPARTTDO();
        ddoTD_M_DEPARTIn.DEPARTNO = strDepartCode;
 
        TMTableModule tmTMTableModule = new TMTableModule();

        TD_M_DEPARTTDO ddoTD_M_DEPARTOut = (TD_M_DEPARTTDO)tmTMTableModule.selByPK(context, ddoTD_M_DEPARTIn, typeof(TD_M_DEPARTTDO), null, "TD_M_DEPART_BYNO", null);

        if (ddoTD_M_DEPARTOut == null || ddoTD_M_DEPARTOut.USETAG == "0")
            return "";
        else
            return strDepartCode;

    }


    private string GetSerMgrCode(string strSerMgrCode)
    {
        TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFIn = new TD_M_INSIDESTAFFTDO();
        ddoTD_M_INSIDESTAFFIn.STAFFNO = strSerMgrCode;

        TMTableModule tmTMTableModule = new TMTableModule();
        TD_M_INSIDESTAFFTDO ddoTD_M_INSIDESTAFFOut = (TD_M_INSIDESTAFFTDO)tmTMTableModule.selByPK(context, ddoTD_M_INSIDESTAFFIn, typeof(TD_M_INSIDESTAFFTDO), null, "TD_M_INSIDESTAFF_BY_STAFFNO", null);

        if (ddoTD_M_INSIDESTAFFOut == null || ddoTD_M_INSIDESTAFFOut.DIMISSIONTAG == "0")
            return "";
        else
            return strSerMgrCode;
    }


    public String getDataKeys(string keysname)
    {
        string value = lvwBalUnits.DataKeys[lvwBalUnits.SelectedIndex][keysname].ToString();

        return value == "" ? "" : value;
    }

    

    protected void selCallingExt_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择增加,查询区域的行业名称后,查询对应的单位信息
        selDepartExt.Items.Clear();
        selCorpExt.Items.Clear();
      
        if (selCallingExt.SelectedValue != "")
            InitCorp(selCallingExt, selCorpExt, "TD_M_CORPCALLUSAGE");

        getNextBalUnitNo();
    }

    protected void selCorpExt_SelectedIndexChanged(object sender, EventArgs e)
    {
        //选择增加,查询区域的单位名称后,查询对应的部门信息

        if (selCorpExt.SelectedValue == "")
        {
            selDepartExt.Items.Clear();
            return;
        }
        InitDepart(selCorpExt, selDepartExt, "TD_M_DEPART");
    }

    private void InitDepart(DropDownList origindwls, DropDownList extdwls, String sqlCondition)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从部门编码表(TD_M_CDEPART)中读取数据，放入下拉列表中

        TD_M_DEPARTTDO tdoTD_M_DEPARTIn = new TD_M_DEPARTTDO();
        tdoTD_M_DEPARTIn.CORPNO = origindwls.SelectedValue;

        TD_M_DEPARTTDO[] tdoTD_M_DEPARTOutArr = (TD_M_DEPARTTDO[])tmTMTableModule.selByPKArr(context, tdoTD_M_DEPARTIn, typeof(TD_M_DEPARTTDO), null, sqlCondition, null);
        ControlDeal.SelectBoxFillWithCode(extdwls.Items, tdoTD_M_DEPARTOutArr, "DEPART", "DEPARTNO", true);
    }

    private void ClearBalUnit()
    {
        lvwBalUnits.SelectedIndex = -1;
        getNextBalUnitNo();
        txtBalUnit.Text = "";

        selCallingExt.SelectedValue = "";
        selCorpExt.SelectedValue = "";
        selDepartExt.SelectedValue = "";
      
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //调用增加的判断处理        if (!BalUnitAddValidation()) return;

        if (BalUnitNoChk()) return;
        
        TMTableModule tmTMTableModule = new TMTableModule();
        //从合帐结算单元中选出详细信息
        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        ddoTF_TRADE_BALUNITIn.BALUNITNO = selIntegrateBalunit.SelectedValue;

        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITOut = (TF_TRADE_BALUNITTDO)tmTMTableModule.selByPK(context, ddoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null);

        if (ddoTF_TRADE_BALUNITOut == null)
        {
            context.AddError(selIntegrateBalunit.SelectedValue+"结算单元信息为空");
            return;
        }
        //调用增加的存储过程
        SP_PS_TransferChangeAddPDO pdo = new SP_PS_TransferChangeAddPDO();
        pdo.balUnitNo = txtBalUnitNo.Text.Trim();
        pdo.balUnit = txtBalUnit.Text.Trim();
        pdo.balUnitTypeCode = "02";//部门
        pdo.sourceTypeCode = ddoTF_TRADE_BALUNITOut.SOURCETYPECODE;
        pdo.callingNo = selCallingExt.SelectedValue.Trim();
        pdo.corpNo = selCorpExt.SelectedValue.Trim();
        pdo.departNo = selDepartExt.SelectedValue.Trim();
        pdo.bankCode = ddoTF_TRADE_BALUNITOut.BANKCODE;
        pdo.bankAccno        = ddoTF_TRADE_BALUNITOut.BANKACCNO;
        pdo.serManagerCode = ddoTF_TRADE_BALUNITOut.SERMANAGERCODE;
        pdo.balLevel = ddoTF_TRADE_BALUNITOut.BALLEVEL;
        pdo.balCycleTypeCode = ddoTF_TRADE_BALUNITOut.BALCYCLETYPECODE;
        pdo.balInterval      = ddoTF_TRADE_BALUNITOut.BALINTERVAL;
        pdo.finCycleTypeCode = ddoTF_TRADE_BALUNITOut.FINCYCLETYPECODE;
        pdo.finInterval = ddoTF_TRADE_BALUNITOut.FININTERVAL;
        pdo.finTypeCode = "1";//财务不转账
        pdo.comFeeTakeCode = "0";//不在转帐金额扣减;
        pdo.finBankCode = ddoTF_TRADE_BALUNITOut.FINBANKCODE;
        pdo.linkMan   = ddoTF_TRADE_BALUNITOut.LINKMAN;
        pdo.unitPhone = ddoTF_TRADE_BALUNITOut.UNITPHONE;
        pdo.unitAdd   = ddoTF_TRADE_BALUNITOut.UNITADD;
        pdo.remark    = txtRemark.Text.Trim();

        pdo.uintEmail = ddoTF_TRADE_BALUNITOut.UNITEMAIL;
        pdo.comSchemeNo = "00000000";//不收佣金
        //部门账期要保持和单位账期的一致，
        //在关联部门和单位时，查下部门对应的单位的结算单元，
        //然后到tp_finance查找单位结算单元的begintime，
        //把这个日期作为部门佣金方案的起始日期
        pdo.beginTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        pdo.endTime = "2050-12-31 23:59:59";

        bool ok = TMStorePModule.Excute(context, pdo);
        if (ok)
        {
            AddMessage("M008107113");
            //ClearBalUnit();
            btnQuery_Click(sender, e);
        }
    }

    protected void btnModify_Click(object sender, EventArgs e)
    {
        //调用修改的判断处理        Boolean keyInfoChanged;
        if (!BalUnitModifyValidation(out keyInfoChanged)) return;



        TMTableModule tmTMTableModule = new TMTableModule();
        //从合帐结算单元中选出详细信息
        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        ddoTF_TRADE_BALUNITIn.BALUNITNO = selIntegrateBalunit.SelectedValue;

        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITOut = (TF_TRADE_BALUNITTDO)tmTMTableModule.selByPK(context, ddoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null);

        //调用修改的存储过程        SP_PS_TransferChangeModifyPDO pdo = new SP_PS_TransferChangeModifyPDO();

        pdo.balUnitNo        = getDataKeys("BALUNITNO").Trim();
        pdo.balUnit          = txtBalUnit.Text.Trim();
        pdo.balUnitTypeCode  = "02";//部门
        pdo.sourceTypeCode   = ddoTF_TRADE_BALUNITOut.SOURCETYPECODE;
        pdo.callingNo        = selCallingExt.SelectedValue.Trim();
        pdo.corpNo           = selCorpExt.SelectedValue.Trim();
        pdo.departNo         = selDepartExt.SelectedValue.Trim();
        pdo.bankCode = ddoTF_TRADE_BALUNITOut.BANKCODE;
        pdo.bankAccno = ddoTF_TRADE_BALUNITOut.BANKACCNO;
        pdo.serManagerCode = ddoTF_TRADE_BALUNITOut.SERMANAGERCODE;
        pdo.balLevel = ddoTF_TRADE_BALUNITOut.BALLEVEL;
        pdo.balCycleTypeCode = ddoTF_TRADE_BALUNITOut.BALCYCLETYPECODE;
        pdo.balInterval = ddoTF_TRADE_BALUNITOut.BALINTERVAL;
        pdo.finCycleTypeCode = ddoTF_TRADE_BALUNITOut.FINCYCLETYPECODE;
        pdo.finInterval = ddoTF_TRADE_BALUNITOut.FININTERVAL;
        pdo.finTypeCode = "1";//财务不转账
        pdo.comFeeTakeCode = "0";//不在转帐金额扣减
        pdo.finBankCode = ddoTF_TRADE_BALUNITOut.FINBANKCODE;
        pdo.linkMan = ddoTF_TRADE_BALUNITOut.LINKMAN;
        pdo.unitPhone = ddoTF_TRADE_BALUNITOut.UNITPHONE;
        pdo.unitAdd = ddoTF_TRADE_BALUNITOut.UNITADD;
        pdo.remark = txtRemark.Text.Trim();

        pdo.unitEmail = ddoTF_TRADE_BALUNITOut.UNITEMAIL;
        pdo.comSchemeNo = "00000000";//不收佣金
        pdo.beginTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
       
        pdo.endTime = "2050-12-31 23:59:59";
		pdo.keyInfoChanged = keyInfoChanged ? "Y" : "N";
		
        bool ok = TMStorePModule.Excute(context, pdo);

        if (ok)
        {
            AddMessage("M008107111");
            //ClearBalUnit();
            btnQuery_Click(sender, e);
        }
    }

    private Boolean BalUnitAddValidation()
    {
        //调用结算单元输入判断
        BalUnitInputValidation();

        return  !(context.hasError());
    }

    private Boolean BalUnitModifyValidation(out Boolean keyInfoChanged)
    {
        keyInfoChanged = true;
        //判断是否选择了需要修改的结算单元
        if (lvwBalUnits.SelectedIndex == -1)
        {
            context.AddError("A008107052");
            return false;
        }
        //当选择结算单元所有信息都没有修改时,不能执行修改
        keyInfoChanged = isKeyInfoChanged();
        if (!keyInfoChanged && !isTrivialInfoChanged())
        {
            context.AddError("A008107066");
            return false;
        }

        //调用结算单元输入判断
        if (!BalUnitInputValidation())
        {
            return false;
        }

        //检验结算单元编码是否修改        if (txtBalUnitNo.Text.Trim() != getDataKeys("BALUNITNO"))
        {
            context.AddError("A008107108", txtBalUnitNo);
            return false;
        }
        return true;
    }

    private Boolean BalUnitNameChk()
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //从结算单元编码表(TD_TRADE_BALUNIT)中读取数据

        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        ddoTF_TRADE_BALUNITIn.BALUNIT = txtBalUnit.Text.Trim();
        TF_TRADE_BALUNITTDO[] ddoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, ddoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNIT_BY_NAME", null);

        //从合作伙伴审批台帐中读取数据
        TF_B_TRADE_BALUNITCHANGETDO ddoTF_B_TRADE_BALUNITCHANGEIn = new TF_B_TRADE_BALUNITCHANGETDO();
        ddoTF_B_TRADE_BALUNITCHANGEIn.BALUNIT = txtBalUnit.Text.Trim();
        TF_B_TRADE_BALUNITCHANGETDO[] ddoTF_B_TRADE_BALUNITCHANGEOutArr = (TF_B_TRADE_BALUNITCHANGETDO[])tmTMTableModule.selByPKArr(context, ddoTF_B_TRADE_BALUNITCHANGEIn, typeof(TF_B_TRADE_BALUNITCHANGETDO), null, "TF_B_TRADE_BALUNITCHANGE_BY_NAME", null);


        //从合作伙伴审核台帐中读取数据
        TF_B_TRADE_BALUNITCHANGETDO ddoTF_B_TRADE_BALUNITCHANGEInExt = new TF_B_TRADE_BALUNITCHANGETDO();
        ddoTF_B_TRADE_BALUNITCHANGEInExt.BALUNIT = txtBalUnit.Text.Trim();
        TF_B_TRADE_BALUNITCHANGETDO[] ddoTF_B_TRADE_BALUNITCHANGEOutArrExt = (TF_B_TRADE_BALUNITCHANGETDO[])tmTMTableModule.selByPKArr(context, ddoTF_B_TRADE_BALUNITCHANGEInExt, typeof(TF_B_TRADE_BALUNITCHANGETDO), null, "TF_B_TRADE_BALUNIT_EXAM", null);

        if (ddoTF_TRADE_BALUNITOutArr.Length != 0 ||
            ddoTF_B_TRADE_BALUNITCHANGEOutArr.Length != 0 ||
            ddoTF_B_TRADE_BALUNITCHANGEOutArrExt.Length != 0)
        {
            context.AddError("A008107063", txtBalUnit);
            return false;
        }

        return true;
    }

    private Boolean BalUnitNoChk()
    {
        //是否该结算单元编码已存在
        TMTableModule tmTMTableModule = new TMTableModule();

        //从结算单元编码表(TD_TRADE_BALUNIT)中读取数据

        TF_TRADE_BALUNITTDO ddoTF_TRADE_BALUNITIn = new TF_TRADE_BALUNITTDO();
        ddoTF_TRADE_BALUNITIn.BALUNITNO = txtBalUnitNo.Text.Trim();
        TF_TRADE_BALUNITTDO[] ddoTF_TRADE_BALUNITOutArr = (TF_TRADE_BALUNITTDO[])tmTMTableModule.selByPKArr(context, ddoTF_TRADE_BALUNITIn, typeof(TF_TRADE_BALUNITTDO), null, "TF_TRADE_BALUNIT_BY_BALNO", null);

        //从合作伙伴审批台帐中待审批读取数据
        TF_B_TRADE_BALUNITCHANGETDO ddoTF_B_TRADE_BALUNITCHANGEIn = new TF_B_TRADE_BALUNITCHANGETDO();
        ddoTF_B_TRADE_BALUNITCHANGEIn.BALUNITNO = txtBalUnitNo.Text.Trim();
        TF_B_TRADE_BALUNITCHANGETDO[] ddoTF_B_TRADE_BALUNITCHANGEOutArr = (TF_B_TRADE_BALUNITCHANGETDO[])tmTMTableModule.selByPKArr(context, ddoTF_B_TRADE_BALUNITCHANGEIn, typeof(TF_B_TRADE_BALUNITCHANGETDO), null, "TF_B_TRADE_BALUNITCHANGE_BY_BALNO", null);


        //从合作伙伴审核台帐中待审核读取数据
        TF_B_TRADE_BALUNITCHANGETDO ddoTF_B_TRADE_BALUNITCHANGEInExt = new TF_B_TRADE_BALUNITCHANGETDO();
        ddoTF_B_TRADE_BALUNITCHANGEInExt.BALUNITNO = txtBalUnitNo.Text.Trim();
        TF_B_TRADE_BALUNITCHANGETDO[] ddoTF_B_TRADE_BALUNITCHANGEOutArrExt = (TF_B_TRADE_BALUNITCHANGETDO[])tmTMTableModule.selByPKArr(context, ddoTF_B_TRADE_BALUNITCHANGEInExt, typeof(TF_B_TRADE_BALUNITCHANGETDO), null, "TF_B_TRADE_BALUNIT_EXAM_BY_BALNO", null);

        if (ddoTF_TRADE_BALUNITOutArr.Length != 0 ||
            ddoTF_B_TRADE_BALUNITCHANGEOutArr.Length != 0 ||
            ddoTF_B_TRADE_BALUNITCHANGEOutArrExt.Length != 0)
        {
            context.AddError("A008107109", txtBalUnitNo);
        }

        //检查结算单元名称是否已存在
        //BalUnitNameChk();

        return context.hasError();

    }
    
    private Boolean BalUnitInputValidation()
    {
        //对结算单元输入信息的判断处理

        //合帐结算单元不能为空
        string strIntergretBalunit = selIntegrateBalunit.SelectedValue;
        if (strIntergretBalunit == "")
        {
            context.AddError("A008107110", selIntegrateBalunit);
        }

        //结算单元非空,长度,数字的判断
        string strBalUnitNo = txtBalUnitNo.Text.Trim();
        if(strBalUnitNo=="")
            context.AddError("A008107105", txtBalUnitNo);
        else if (Validation.strLen(strBalUnitNo) != 8 )
            context.AddError("A008107106", txtBalUnitNo);
        else if (!Validation.isCharNum(strBalUnitNo))
            context.AddError("A008107107", txtBalUnitNo);

        //对结算单元名称进行非空,长度校验
        string strBalUnit = txtBalUnit.Text.Trim();
        if (strBalUnit == "")
        {
            context.AddError("A008107005", txtBalUnit);
        }

        else if (Validation.strLen(strBalUnit) > 50)
        {
            context.AddError("A008107006", txtBalUnit);
        }

        //对单元类型的判断
        string strCalling = selCallingExt.SelectedValue;
        string strCorp = selCorpExt.SelectedValue;
        string strDepart = selDepartExt.SelectedValue;
        //对结算单元编码的校验
        if ((strBalUnitNo.Length == 8) && strCalling != "")
        {
            if (strBalUnitNo.Substring(0, 2) != strCalling)
                context.AddError("A008107077", txtBalUnitNo);
        }

        //判断行业名称是否为空 //判断单位名称是否为空 //判断部门名称是否为空
        if (strCalling == "")
            context.AddError("A008107031", selCallingExt);
        if (strCorp == "")
            context.AddError("A008107032", selCorpExt);
        if (strDepart == "")
            context.AddError("A008107033", selDepartExt);


        //对context的error检测 
        if (context.hasError())
            return false;
        else
            return true;
    }
	
	// 判断是否需要审批（如果只修改了联系人，联系电话，联系地址，电子邮件时，不需要审批）
	private bool isKeyInfoChanged()
	{
        return
            txtBalUnitNo.Text.Trim() != getDataKeys("BALUNITNO").Trim() ||
            txtBalUnit.Text.Trim() != getDataKeys("BALUNIT").Trim() ||
            selCallingExt.SelectedValue != getDataKeys("CALLINGNO").Trim() ||
            selCorpExt.SelectedValue != getDataKeys("CORPNO").Trim() ||
            selDepartExt.SelectedValue != getDataKeys("DEPARTNO").Trim();
	       
	}

	private bool isTrivialInfoChanged()
	{
        return
            txtRemark.Text.Trim() != getDataKeys("REMARK").Trim();
    }





    private Boolean isInteger(string strInput)
    {
        System.Text.RegularExpressions.Regex reg1
                          = new System.Text.RegularExpressions.Regex(@"^[1-9][0-9]*$");

        return reg1.IsMatch(strInput);
    }

}
