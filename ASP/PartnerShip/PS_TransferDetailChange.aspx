﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PS_TransferDetailChange.aspx.cs" Inherits="ASP_PartnerShip_PS_TransferDetailChange" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>明细结算单元维护</title>
    <script type="text/javascript" src="../../js/myext.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/frame.css" />
     <link href="../../css/card.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
     <div class="tb">合作伙伴->明细结算单元增加</div>
     <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" ID="ScriptManager2" />
            <script type="text/javascript" language="javascript">
                var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
                swpmIntance.add_initializeRequest(BeginRequestHandler);
                swpmIntance.add_pageLoading(EndRequestHandler);
				function BeginRequestHandler(sender, args){
				    try {MyExtShow('请等待', '正在提交后台处理中...'); } catch(ex){}
				}
				function EndRequestHandler(sender, args) {
				    try {MyExtHide(); } catch(ex){}
				}
          </script>              
       <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
        <asp:BulletedList ID="bulMsgShow" runat="server"/>
         <script runat="server" >public override void ErrorMsgShow(){ErrorMsgHelper(bulMsgShow);}</script>
       </ContentTemplate></asp:UpdatePanel>
            <div class="con">
              <div class="jieguo">查询结算单元信息</div>
              <div class="kuang5">
              <table width="98%" border="0" cellpadding="0" cellspacing="0" class="text25">
                <tr>
                  <td align="right">行业名称:</td>
                  <td><asp:DropDownList ID="selCalling" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCalling_SelectedIndexChanged">
                     </asp:DropDownList>  
                  </td>
                  <td align="right">单位名称:</td>
                    <td><asp:DropDownList ID="selCorp" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCorp_SelectedIndexChanged">
                     </asp:DropDownList>    
		            </td>

                </tr>
                <tr>
                    <td align="right">部门名称:</td>
                    <td><asp:DropDownList ID="selDepart" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selDepart_SelectedIndexChanged">
                      </asp:DropDownList>
                     </td>
                  <td align="right">结算单元:</td>
                  <td><asp:DropDownList ID="selBalUint" CssClass="inputmidder" runat="server">
                      </asp:DropDownList>
                  </td>
                </tr>
                <tr>
		          <td align="right">审批状态:</td>
		          <td><asp:DropDownList ID="selAprvState" CssClass="input" runat="server">
		                <asp:ListItem Text="0: 财审通过" Value="0"></asp:ListItem>
		                <asp:ListItem Text="1: 财审作废" Value="1"></asp:ListItem>
		                <asp:ListItem Text="2: 审批通过" Value="2"></asp:ListItem>
		                <asp:ListItem Text="3: 审批作废" Value="3"></asp:ListItem>
		                <asp:ListItem Text="4: 等待审批" Value="4"></asp:ListItem>
		              </asp:DropDownList>
		          </td>                
                  <td><div align="right">商户经理:</div></td>
                  <td>
                     <asp:DropDownList ID="selMsgQry" CssClass="input" runat="server"/>                     

                         <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline">
                     <ContentTemplate>
                     <asp:HiddenField runat="server" ID="hidAprvState" Value="0" />
		              <asp:HiddenField runat="server" ID="hidSeqNo" />

                    <asp:Button ID="btnQuery" runat="server" Text="查询" CssClass="button1" 
                        OnClick="btnQuery_Click" />
                   </ContentTemplate></asp:UpdatePanel>
                  </td>
		          
		          </tr>
              </table>
              
              <div class="gdtb" style="height:160px">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"  RenderMode="Inline">
                <ContentTemplate>

                 <asp:GridView ID="lvwBalUnits" runat="server"
                    CssClass="tab1"
                    Width ="280%"
                    HeaderStyle-CssClass="tabbt"
                    AlternatingRowStyle-CssClass="tabjg"
                    SelectedRowStyle-CssClass="tabsel"
                    AllowPaging="True"
                    PageSize="1000"
                    PagerSettings-Mode="NumericFirstLast"
                    PagerStyle-HorizontalAlign="Left"
                    PagerStyle-VerticalAlign="Top"
                    AutoGenerateColumns="False"
                    OnPageIndexChanging="lvwBalUnits_Page"
                    OnSelectedIndexChanged="lvwBalUnits_SelectedIndexChanged"
                    OnRowCreated="lvwBalUnits_RowCreated"
                    EmptyDataText="没有数据记录!"
                    >
                    <Columns>
                         <asp:BoundField DataField="ROWNUM"      HeaderText="#"/> 
                        <asp:TemplateField HeaderText="有效标志">
                         <ItemTemplate>
                         <asp:Label ID="labUseTag" Text='<%# Eval("USETAG").ToString() == "1" ? "有效" : Eval("USETAG").ToString() == "0" ? "无效" : Eval("USETAG").ToString()%>' runat="server"></asp:Label>
                         </ItemTemplate>
                        </asp:TemplateField>
                  
                        <asp:BoundField DataField="BALUNITNO"      HeaderText="结算单元编码"/> 
                        <asp:BoundField DataField="BALUNIT"        HeaderText="结算单元名称"/>             
                        <asp:BoundField DataField="BALUNITTYPE"    HeaderText="单元类型"/>   
                        <asp:BoundField DataField="CHANNELNAME"    HeaderText="结算通道"/>                
                        <asp:BoundField DataField="SOURCETYPE"     HeaderText="来源识别类型"/>             
                        <asp:BoundField DataField="CREATETIME"     HeaderText="合作时间" DataFormatString="{0:yyyy-MM-dd}" HtmlEncode="false"/>                 
                        <asp:BoundField DataField="CALLINGNAME"    HeaderText="行业"/>                     
                        <asp:BoundField DataField="CORPNAME"       HeaderText="单位"/>                    
                        <asp:BoundField DataField="DEPARTNAME"     HeaderText="部门"/>                      
                        <asp:BoundField DataField="BANKNAME"       HeaderText="开户银行"/>                  
                        <asp:BoundField DataField="BANKACCNO"      HeaderText="银行帐号"/>                  
                                        
                        <asp:BoundField DataField="SERMANAGER"      HeaderText="商户经理"/>      
                        <asp:BoundField DataField="BALLEVEL"        HeaderText="结算级别"/>                 
                        <asp:BoundField DataField="BALCYCLETYPE"    HeaderText="结算周期类型"/>            
                        <asp:BoundField DataField="BALINTERVAL"     HeaderText="结算周期跨度"/>             
                        <asp:BoundField DataField="FINCYCLETYPE"    HeaderText="划账周期类型"/>             
                        <asp:BoundField DataField="FININTERVAL"     HeaderText="划账周期跨度"/>             
                        <asp:BoundField DataField="FINTYPE"         HeaderText="转账类型"/>                 
                        <asp:BoundField DataField="COMFEETAKE"      HeaderText="佣金扣减方式"/> 
                        <asp:BoundField DataField="FINBANK"         HeaderText="转出银行"/>              
                        <asp:BoundField DataField="LINKMAN"         HeaderText="联系人"/>                                     
                        <asp:BoundField DataField="UNITPHONE"       HeaderText="联系电话"/>                
                        <asp:BoundField DataField="UNITADD"         HeaderText="联系地址"/>  
                        <asp:BoundField DataField="UNITEMAIL"       HeaderText="电子邮件"/>         
                   </Columns>   
                   
                   <EmptyDataTemplate>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1">
                         <tr class="tabbt">
                          <td>#</td>
                          <td>有效标志</td>
                          <td>结算单元编码</td>
                          <td>结算单元名称</td>
                          <td>单元类型</td>
                          <td>结算通道</td>
                          <td>来源识别类型</td>
                          <td>合作时间</td>
                          <td>行业</td>
                          <td>单位</td>
                          <td>部门</td>
                          <td>开户银行</td>
                          <td>银行帐号</td>
                          <td>商户经理</td>
                          <td>结算级别</td>
                          <td>结算周期类型</td>
                          <td>结算周期跨度</td>
                          <td>划账周期类型</td>
                          <td>划账周期跨度</td>
                          <td>转账类型</td>
                          <td>佣金扣减方式</td>
                          <td>转出银行</td>
                          <td>联系人</td>
                          <td>联系电话</td>
                          <td>联系地址</td>
                         </tr>
                      </table>
                   </EmptyDataTemplate>
                  </asp:GridView>
             
                    </ContentTemplate></asp:UpdatePanel>
                </div>

              </div>
            </div>
            
            <asp:UpdatePanel ID="UpdatePanel3" runat="server"  RenderMode="Inline">
            <ContentTemplate>
            <div class="con">
             <%--<div class="base">结算单元信息</div>--%>
             <div class="kuang5">
               <table width="880" border="0" cellpadding="0" cellspacing="0" class="12text">
                
                 <tr>
                   
                   <td><div align="right">单元编码:</div></td>
                   <td >
                     <asp:TextBox ID="txtBalUnitNo" CssClass="input" runat="server" MaxLength="8"></asp:TextBox>
                     
                   </td>
                 <td><div align="right">&nbsp;</div></td>
                   <td>
                      &nbsp;
                      
                   </td>
                   <td><div align="right">结算单元:</div></td>
                   <td>
                      <asp:TextBox ID="txtBalUnit" runat="server" CssClass="inputmidder" MaxLength="40" ></asp:TextBox>
                   </td>
                   </tr>
                   
                   <tr>
                   
                   <td><div align="right">行业名称:</div></td>
                   <td colspan="5">
                   
                  <asp:DropDownList ID="selCallingExt" CssClass="inputmidder" runat="server" 
                        AutoPostBack="true" OnSelectedIndexChanged="selCallingExt_SelectedIndexChanged">
                      </asp:DropDownList>
                     
                   </td>
                  
                 
                 </tr>
                 <tr>
                   
                   <td><div align="right">单位名称:</div></td>
                   <td colspan="3">
                     <asp:DropDownList ID="selCorpExt" CssClass="inputmidder" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selCorpExt_SelectedIndexChanged">
                     </asp:DropDownList>
              
                   </td>
                   <td><div align="right">部门名称:</div></td>
                   <td>
                   
                     <asp:DropDownList ID="selDepartExt" CssClass="inputmidder" runat="server">
                     </asp:DropDownList>
                     
                   </td>
                   </tr>
                   <tr>
                   <td><div align="right">合帐结算单元</div></td>
                   <td colspan="3"><asp:DropDownList ID="selIntegrateBalunit" CssClass="inputmidder" runat="server">
                      </asp:DropDownList></td>
                       <td><div align="right">
                       备注:</div></td>
                   <td><asp:TextBox ID="txtRemark" runat="server" CssClass="inputmidder" MaxLength="100"></asp:TextBox></td>
                 
                 </tr>

               </table>

             <div class="footall" style="height:1px;"></div>
             </div>

            </div>
          

             <div class="btns">
               <table width="200" border="0" align="right" cellpadding="0" cellspacing="0">
                 <tr>
                   <td><asp:Button ID="btnAdd" runat="server" Text="增加" CssClass="button1" OnClick="btnAdd_Click" /></td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
             </div>
            
            </ContentTemplate>

        </asp:UpdatePanel>
    </form>
</body>
</html>
