﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using PDO.BusService;
using TM;
using System.Data;
using TDO.UserManager;

public partial class ASP_BusService_BS_SaleSpecialCardNew : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        // 设置焦点以及按键事件
        txtRealRecv.Attributes["onfocus"] = "this.select();";
        txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this);";

        // 初始化证件类型
        ASHelper.initPaperTypeList(context, selPaperType);

        // 初始化性别
        ASHelper.initSexList(selCustSex);

        // 初始化费用列表
        DepositFee.Text = "0.00";
        CardcostFee.Text = "0.00";
        ProcedureFee.Text = "0.00";
        OtherFee.Text = "0.00";
        Total.Text = "0.00";
    }

    private void initValue()
    {
        //业务类型编码表：K1高龄老人卡开卡,K2其它计次优惠卡,K3残疾人优待卡开卡
        string typehidden = selSpecialCardType.SelectedValue;
        hidTradeTypeCode.Value
                = typehidden == "9901" ? "K1"
                : typehidden == "9902" ? "K2"
                : typehidden == "9903" ? "K3"
                : "";
        //TD_M_APPAREA,TD_M_FUNCTION：S1高龄老人卡,S2其它计次优惠卡,S3残疾人优待卡（库内记录）
        hidAppTypeCode.Value
                = typehidden == "9901" ? "S1"
                : typehidden == "9902" ? "S2"
                : typehidden == "9903" ? "S3"
                : "";  
    }

    // 特种卡类型选择
    protected void selSpecialCardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selSpecialCardType.SelectedValue == "9901")
        {
            isOverseas.Visible = true;
        }
        else
        {
            isOverseas.Visible = false;
        }

        if (selSpecialCardType.SelectedValue != "")
        {
            btnSubmit.Enabled = true;
        }
        else
        {
            btnSubmit.Enabled = false;
        }
        if (txtPaperNo.Text.Trim() != "" && selSpecialCardType.SelectedValue != "")
        {
            CheckPaperNo();
        }
        initValue();
    }

    protected void Paperno_Changed(object sender, EventArgs e)
    {
        if (selSpecialCardType.SelectedValue != "")
        {
            CheckPaperNo();
        }
    }

    // 确认对话框确认处理
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")    // 是否继续
        {
            btnSubmit.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess") // 写卡成功
        {
            saleSpecialCard();
            hidWarning.Value = "";     
        }
        else if (hidWarning.Value == "writeFail") // 写卡失败
        {
            context.AddError("前台写卡失败，特种卡售卡失败");
        }

        if (chkShouju.Checked && btnPrintSJ.Enabled)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "printShouJu();", true);
        }

        hidWarning.Value = "";// 清除警告信息
    }

    // 特种卡售卡提交
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //用户信息判断
        if (!SaleInfoValidation())
            return;

        if (selSpecialCardType.SelectedValue == "")
        {
            context.AddError("特种卡类型不能为空", selSpecialCardType);
        }
        submitValidate();

        if (context.hasError()) return;

        //高龄老人优待乘车卡判断年龄是否达到70周岁，归国华侨需达到60周岁
        if (selSpecialCardType.SelectedValue == "9901")
        {
            int birthYear = Convert.ToInt16(txtCustBirth.Text.Trim().Trim().Substring(0, 4));
            int birthMonth = Convert.ToInt16(txtCustBirth.Text.Trim().Trim().Substring(4, 2));
            int birthDay = Convert.ToInt16(txtCustBirth.Text.Trim().Trim().Substring(6, 2));
            int age = DateTime.Now.Year - birthYear;
            if (DateTime.Now.Month < birthMonth)
            //if (DateTime.Now.Month < birthMonth || (DateTime.Now.Month == birthMonth && DateTime.Now.Day < birthDay))
                age--;

            if (age < 70)
            {
                if (!(age >= 60 && isOverseas.Checked))
                {
                    context.AddError("年龄不足70周岁或者不满足60周岁华侨条件，无法开通高龄老人优待卡");
                    return;
                }
            }
        }

        //优惠卡，须验证系统内存在一张且已经失效的卡片方可办理。
        if (selSpecialCardType.SelectedValue == "9902")
        {
            string paperno = txtPaperNo.Text.Trim();
            string oldPaperno = paperno.Remove(6, 2);
            oldPaperno = oldPaperno.Remove(oldPaperno.Length - 1, 1);
            DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardExist", txtPaperNo.Text.Trim(), oldPaperno);
            if (dt == null || dt.Rows.Count == 0)
            {
                context.AddError("系统不支持该类型特种卡新售卡，只支持补卡");
                return;
            }
        }

        //写卡
        if (selSpecialCardType.SelectedValue == "9902")
        {
            //计算卡内年审时间
            string year = "20001231";
            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;
            if (currentMonth == 12)
            {
                year = DateTime.Now.AddYears(1).Year.ToString() + "1231";
            }
            else
            {
                year = DateTime.Now.Year.ToString() + "1231";
            }
            this.txtVerifyDate.Value = year;//给年审函数参数控件赋值

            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "startSpecialInfo2();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript", "startSpecialInfo1();", true);
        }
    }

    private void saleSpecialCard()
    {
        if (hidCardNo.Value == "" || hidCardNo.Value == null)
        {
            context.AddError("写卡成功后未获取到卡号，写库失败");
            return;
        }

        if (hidTradeNo.Value == "" || hidAsn.Value == "" || hidSpecialCardType.Value == "" || hidSpecialCardName.Value == "")
        {
            context.AddError("写卡成功后未获取到卡信息，写库失败");
            return;
        }

        //校验卡内类型和库内类型是否一致
        string typehid = hidSpecialCardType.Value;
        hidSpecialCardType.Value
            = typehid == "J" ? "9901"
            : typehid == "I" ? "9902"
            : typehid == "P" ? "9903"
            : "";
       
        if (selSpecialCardType.SelectedValue != hidSpecialCardType.Value)
        {
            context.AddError("卡类型和当前售卡类型不一致，写库失败，卡片已锁");
            return;
        }

        // 校验卡片是否存在库中
        DataTable data = ASHelper.callQuery(context, "ReadCardTypeByCardNo", hidCardNo.Value);
        if (data == null || data.Rows.Count == 0)
        {
            context.AddError("未在用户卡库存表中查询出卡片信息或者卡片类型没有配置");
            return;
        }

        // 调用特种卡售卡存储过程
        SP_BS_SpecialSaleCardPDO pdo = new SP_BS_SpecialSaleCardPDO();
        pdo.ID = DealString.GetRecordID(hidTradeNo.Value, hidAsn.Value);
        pdo.cardNo = hidCardNo.Value;
        pdo.deposit = 0;
        pdo.cardCost = (int)(Double.Parse(hidAccRecv.Value) * 100);
        pdo.otherFee = 0;
        pdo.cardTradeNo = hidTradeNo.Value;
        pdo.asn = hidAsn.Value;
        pdo.cardMoney = 0;
        pdo.sellChannelCode = "01";
        pdo.serTakeTag = "0";
        pdo.tradeTypeCode = hidTradeTypeCode.Value;
        pdo.terminalNo = "112233445566";   // 目前固定写成112233445566

        pdo.custName = txtCustName.Text;
        pdo.custBirth = txtCustBirth.Text;
        pdo.paperType = selPaperType.SelectedValue;
        pdo.paperNo = txtPaperNo.Text;
        pdo.custSex = selCustSex.SelectedValue;
        pdo.custPhone = txtCustPhone.Text;
        pdo.custPost = txtCustPost.Text;
        pdo.custAddr = txtCustAddr.Text;
        pdo.custEmail = txtEmail.Text;
        pdo.remark = txtRemark.Text;

        pdo.custRecTypeCode = "1";
        pdo.appType = hidAppTypeCode.Value;
        pdo.assignedArea = hidAppTypeCode.Value;
        pdo.endTime = txtVerifyDate.Value;

        pdo.currCardNo = context.s_CardID;

        // 执行存储过程
        bool ok = TMStorePModule.Excute(context, pdo);
        btnSubmit.Enabled = false;

        // 存储过程执行成功，显示成功消息
        if (ok)
        {
            AddMessage("特种卡售卡成功");
            
            // 准备收据打印数据
            ASHelper.prepareShouJu(ptnShouJu, hidCardNo.Value, txtCustName.Text, "特种卡售卡", hidAccRecv.Value
            , "", "", "", "", "", "", context.s_UserName, context.s_DepartName, "", "0.00", "");

            clearCustInfo(txtCustName, txtCustBirth, selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark, selSpecialCardType);

            btnPrintSJ.Enabled = true;
            isOverseas.Visible = false;
        }
    }

    //身份证号码检查
    private void CheckPaperNo()
    {
        //读卡时判断此身份证号是否可以优惠，防止输完身份证号后再读卡，则优惠方案会变化。
        if (txtPaperNo.Text.Trim() != "")
        {
            //所有特种卡每人仅限办理一张,根据新旧身份证号判断
           string paperno = txtPaperNo.Text.Trim();
           string oldPaperno = paperno.Remove(6, 2);
           oldPaperno = oldPaperno.Remove(oldPaperno.Length - 1, 1);

           DataTable dt = BusCardHelper.callQuery(context, "QrySpecialCardByPaperNo", txtPaperNo.Text.Trim(), oldPaperno, selSpecialCardType.SelectedValue);
            if (dt == null || dt.Rows.Count == 0)
            {
                CardcostFee.Text = 10.ToString("0.00");
                Total.Text = 10.ToString("0.00");
                txtRealRecv.Text = 10.ToString("0.00");
                hidAccRecv.Value = 10.ToString("n");

                if (selSpecialCardType.SelectedValue == "9901" || selSpecialCardType.SelectedValue == "")
                {
                    //高龄老人卡首次开通显示收费为0元，第二次开通显示收费为10元,其它卡种收费10元
                    DataTable data = BusCardHelper.callQuery(context, "QrySpecialDiscount", txtPaperNo.Text.Trim(), oldPaperno);
                    if (data == null || data.Rows.Count == 0)
                    {
                        CardcostFee.Text = 0.ToString("0.00");
                        Total.Text = 0.ToString("0.00");
                        txtRealRecv.Text = 0.ToString("0.00");
                        hidAccRecv.Value = 0.ToString("n");
                    }
                }       
            }
            else
            {
                context.AddError("该身份证已开通同类特种卡，不可重复办理。");
            }
        }
        btnSubmit.Enabled = !context.hasError();

        hidUserName.Value = txtCustName.Text;
        hidUserSex.Value = selCustSex.SelectedValue == "0" ? "01" //写卡时转换性别编码
                         : selCustSex.SelectedValue == "1" ? "02"
                         : "09";
        hidUserPaperno.Value = txtPaperNo.Text;
    }

    //对售卡用户信息进行检验
    private Boolean SaleInfoValidation()
    {
        //对用户姓名进行非空、长度检验
        if (txtCustName.Text.Trim() == "")
            context.AddError("A001001111", txtCustName);

        //对用户性别进行非空检验
        if (selCustSex.SelectedValue == "")
            context.AddError("A001001116", selCustSex);

        //对证件类型进行非空检验
        if (selPaperType.SelectedValue == "")
            context.AddError("A001001117", selPaperType);

        //对出生日期进行非空、日期格式检验
        String cDate = txtCustBirth.Text.Trim();
        if (cDate == "")
            context.AddError("A001001114", txtCustBirth);

        //对联系电话进行非空、长度、数字检验
        if (txtCustPhone.Text.Trim() == "")
            context.AddError("A001001124", txtCustPhone);

        //对证件号码进行非空、长度、英数字检验
        if (txtPaperNo.Text.Trim() == "")
            context.AddError("A001001121", txtPaperNo);
        else if (!Validation.isCharNum(txtPaperNo.Text.Trim()))
            context.AddError("A001001122", txtPaperNo);
        else if (Validation.strLen(txtPaperNo.Text.Trim()) > 20)
            context.AddError("A001001123", txtPaperNo);
        else if (selPaperType.SelectedValue == "00")//身份证必须为15或18位。
        {
            if (Validation.strLen(txtPaperNo.Text.Trim()) != 18 && Validation.strLen(txtPaperNo.Text.Trim()) != 15)
                context.AddError("A001001130", txtPaperNo);
            else
            {
                //身份证号规则校验
                if (!CommonHelper.CheckIDCard(txtPaperNo.Text.Trim()))
                {
                    context.AddError("A001001131", txtPaperNo);
                }
            }
        }

        //对联系地址进行非空、长度检验
        if (txtCustAddr.Text.Trim() == "")
            context.AddError("A001001127", txtCustAddr);

        return !(context.hasError());
    }

    // 提交判断
    private void submitValidate()
    {
        // 校验客户信息
        custInfoValidate(txtCustName, txtCustBirth, selPaperType, txtPaperNo, selCustSex, txtCustPhone, txtCustPost, txtCustAddr, txtEmail, txtRemark);
    }

    private void clearControls(Control control)
    {
        foreach (Control c in control.Controls)
        {
            if (c is Label)
            {
                ((Label)c).Text = "";
            }
            else if (c is TextBox)
            {
                ((TextBox)c).Text = "";
            }
            else if (c is HiddenField)
            {
                ((HiddenField)c).Value = "";
            }
            else if (c is CheckBox)
            {
                ((CheckBox)c).Checked = false;
            }
            if (c.Controls.Count > 0)
            {
                clearControls(c);
            }
        }
    }
}
