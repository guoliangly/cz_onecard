﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BS_SaleSpecialCardNew.aspx.cs" Inherits="ASP_BusService_BS_SaleSpecialCardNew" %>

<%@ Register Src="../../CardReader.ascx" TagName="CardReader" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>特种卡售卡</title>
    <link rel="stylesheet" type="text/css" href="../../css/frame.css" />
    <script type="text/javascript" src="../../js/print.js"></script>
    <link href="../../css/card.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <cr:CardReader ID="cardReader" runat="server" />

    <form id="form1" runat="server">
        <asp:Label runat="server" ID="labTitle" Visible="false" />
        <div class="tb">
            公交卡->特种卡售卡
        </div>
        <ajaxToolkit:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" runat="server" />
        <script type="text/javascript" language="javascript">
            var swpmIntance = Sys.WebForms.PageRequestManager.getInstance();
            swpmIntance.add_initializeRequest(BeginRequestHandler);
            swpmIntance.add_pageLoading(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                try { MyExtShow('请等待', '正在提交后台处理中...'); } catch (ex) { }
            }
            function EndRequestHandler(sender, args) {
                try { MyExtHide(); } catch (ex) { }
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <aspControls:PrintShouJu ID="ptnShouJu" runat="server" PrintArea="ptnShouJu1" />

                <asp:BulletedList ID="bulMsgShow" runat="server">
                </asp:BulletedList>
                <script runat="server">public override void ErrorMsgShow() { ErrorMsgHelper(bulMsgShow); }</script>

                <div class="con">
                    <div class="pip">[公交卡]用户信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text25">
                            <tr>
                                <tr>
                                    <td width="10%">
                                        <div align="right">用户姓名:</div>
                                    </td>
                                    <td width="13%">
                                        <asp:TextBox ID="txtCustName" CssClass="input" runat="server" MaxLength="50" /></td>
                                    <td width="9%">
                                        <div align="right">出生日期:</div>
                                    </td>
                                    <td width="13%">
                                        <asp:TextBox ID="txtCustBirth" CssClass="input" runat="server" MaxLength="8" /></td>
                                    <ajaxToolkit:CalendarExtender ID="FCalendar" runat="server" TargetControlID="txtCustBirth"
                                        Format="yyyyMMdd" />
                                    <asp:HiddenField ID="txtVerifyDate" runat="server" />
                                    <td width="9%">
                                        <div align="right">证件类型:</div>
                                    </td>
                                    <td width="13%">
                                        <asp:DropDownList ID="selPaperType" CssClass="input" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="9%">
                                        <div align="right">证件号码:</div>
                                    </td>
                                    <td width="24%">
                                        <asp:TextBox ID="txtPaperNo" CssClass="inputmid" runat="server" MaxLength="20" AutoPostBack="true" OnTextChanged="Paperno_Changed" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right">用户性别:</div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selCustSex" CssClass="input" runat="server">
                                        </asp:DropDownList></td>
                                    <td>
                                        <div align="right">联系电话:</div>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCustPhone" CssClass="input" runat="server" MaxLength="20" />
                                    </td>
                                    <td>
                                        <div align="right">邮政编码:</div>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCustPost" CssClass="input" runat="server" MaxLength="6" />
                                    </td>
                                    <td>
                                        <div align="right">联系地址:</div>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCustAddr" CssClass="inputmid" runat="server" MaxLength="50" />
                                    </td>
                                </tr>
                                <asp:HiddenField ID="hidUserName" runat="server" />
                                <asp:HiddenField ID="hidUserSex" runat="server" />
                                <asp:HiddenField ID="hidUserPaperno" runat="server" />
                                <asp:HiddenField ID="hidUserPhone" runat="server" />
                                <tr>
                                    <td>
                                        <div align="right">电子邮件:</div>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" CssClass="input" runat="server" MaxLength="30" />
                                    </td>
                                    <td>
                                        <div align="right">备注:</div>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtRemark" CssClass="inputlong" runat="server" MaxLength="100" />
                                    </td>
                                    <td>
                                        <asp:Button ID="txtReadPaper" Text="读二代证" CssClass="button1" runat="server"
                                            OnClientClick="readIDCard('txtCustName', 'selCustSex', 'txtCustBirth', 'selPaperType', 'txtPaperNo', 'txtCustAddr')" />

                                        <asp:HiddenField runat="server" ID="hidWarning" />
                                        <asp:HiddenField runat="server" ID="hidCardNo" />
                                        <asp:HiddenField runat="server" ID="hidTradeNo" />
                                        <asp:HiddenField runat="server" ID="hidAsn" />
                                        <asp:HiddenField runat="server" ID="hidSpecialCardType" />
                                        <asp:HiddenField runat="server" ID="hidSpecialCardName" />
                                        <asp:HiddenField runat="server" ID="hidAccRecv" />
                                        <asp:HiddenField runat="server" ID="hidTradeTypeCode" />
                                        <asp:HiddenField runat="server" ID="hidAppTypeCode" />
                                        <asp:LinkButton runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" />
                                    </td>
                                </tr>
                        </table>
                    </div>

                    <div class="card">[公交卡]卡片信息</div>
                    <div class="kuang5">
                        <table width="95%" border="0" cellpadding="0" cellspacing="0" class="text20">
                            <tr>
                                <td width="10%">
                                    <div align="right">特种卡类型:</div>
                                </td>
                                <td width="15%">
                                    <asp:DropDownList AutoPostBack="true" ID="selSpecialCardType" CssClass="inputmid" OnSelectedIndexChanged="selSpecialCardType_SelectedIndexChanged" runat="server">
                                        <asp:ListItem Value="">--请选择--</asp:ListItem>
                                        <asp:ListItem Value="9901">9901:高龄老人卡</asp:ListItem>
                                        <asp:ListItem Value="9902">9902:优惠卡</asp:ListItem>
                                        <asp:ListItem Value="9903">9903:残疾人优待卡</asp:ListItem>
                                    </asp:DropDownList>
                                    <span class="red">*</span>
                                </td>
                                <td>
                                    <asp:CheckBox ID="isOverseas" runat="server" Text="是否华侨" Checked="false" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="basicinfo">
                    <div class="money">费用信息</div>
                    <div class="kuang5">
                        <table width="180" border="0" cellpadding="0" cellspacing="0" class="tab1">
                            <tr class="tabbt">
                                <td width="66">费用项目
                                </td>
                                <td width="94">费用金额(元)
                                </td>
                            </tr>
                            <tr>
                                <td>押金
                                </td>
                                <td>
                                    <asp:Label ID="DepositFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td>卡费
                                </td>
                                <td>
                                    <asp:Label ID="CardcostFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>手续费
                                </td>
                                <td>
                                    <asp:Label ID="ProcedureFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td>其他费用
                                </td>
                                <td>
                                    <asp:Label ID="OtherFee" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr class="tabjg">
                                <td>合计应收
                                </td>
                                <td>
                                    <asp:Label ID="Total" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="pipinfo">
                    <div class="info">收款信息</div>
                    <div class="kuang5">
                        <div class="bigkuang">
                            <div class="left">
                                <img src="../../Images/show-sale.JPG" />
                            </div>
                            <div class="big">
                                <table width="150" border="0" cellpadding="0" cellspacing="0" class="text25">
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td width="50%" align="right">
                                            <label>本次实收:&nbsp;</label></td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtRealRecv" CssClass="inputshort" runat="server" MaxLength="9" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">本次应找:&nbsp;</td>
                                        <td>
                                            <div id="txtChanges">0.00</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="red">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="red">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footall"></div>
                <div class="btns">
                    <table width="200" align="right" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Button ID="btnPrintSJ" runat="server" Text="打印收据"
                                    CssClass="button1" Enabled="false" OnClientClick="printdiv('ptnShouJu1')" /></td>
                            <td>
                                <asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                    OnClick="btnSubmit_Click" />
                                <%--<asp:Button ID="btnSubmit" Enabled="false" CssClass="button1" runat="server" Text="提交"
                                    OnClientClick="return saleSpecialCardBeforeRead()" />--%>
                            </td>

                        </tr>
                    </table>
                    <asp:CheckBox ID="chkShouju" runat="server" Text="自动打印收据" Checked="true" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
