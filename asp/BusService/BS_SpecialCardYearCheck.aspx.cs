﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TDO.BusinessCode;
using TM;
using TDO.CardManager;
using PDO.PersonalBusiness;
using TDO.UserManager;
using TDO.ResourceManager;

public partial class ASP_BusService_BS_SpecialCardYearCheck : Master.FrontMaster
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LabAsn.Attributes["readonly"] = "true";
            LabCardtype.Attributes["readonly"] = "true";
            sDate.Attributes["readonly"] = "true";
            accMoney.Attributes["readonly"] = "true";
            txtRealRecv.Attributes["onfocus"] = "this.select();";
            txtRealRecv.Attributes["onkeyup"] = "realRecvChanging(this, 'test', 'hidAccRecv');";

            if (!context.s_Debugging) txtCardNo.Attributes["readonly"] = "true";

            hidAccRecv.Value = Total.Text;
            txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();
        }
    }

    /// <summary>
    /// 读卡
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReadCard_Click(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();

        //卡账户有效性检验
        context.SPOpen();
        context.AddField("p_CARDNO").Value = txtCardNo.Text;

        bool ok = context.ExecuteSP("SP_Credit_Check");
        if (ok)
        {
            //从卡资料表(TF_F_CARDREC)中读取数据
            TF_F_CARDRECTDO ddoTF_F_CARDRECIn = new TF_F_CARDRECTDO();
            ddoTF_F_CARDRECIn.CARDNO = txtCardNo.Text;

            TF_F_CARDRECTDO ddoTF_F_CARDRECOut = (TF_F_CARDRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDRECIn, typeof(TF_F_CARDRECTDO), null);

            //从IC卡电子钱包帐户表(TF_F_CARDEWALLETACC)中读取数据
            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCIn = new TF_F_CARDEWALLETACCTDO();
            ddoTF_F_CARDEWALLETACCIn.CARDNO = txtCardNo.Text;

            TF_F_CARDEWALLETACCTDO ddoTF_F_CARDEWALLETACCOut = (TF_F_CARDEWALLETACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDEWALLETACCIn, typeof(TF_F_CARDEWALLETACCTDO), null);

            //从持卡人资料表(TF_F_CUSTOMERREC)中读取数据
            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECIn = new TF_F_CUSTOMERRECTDO();
            ddoTF_F_CUSTOMERRECIn.CARDNO = txtCardNo.Text;

            TF_F_CUSTOMERRECTDO ddoTF_F_CUSTOMERRECOut = (TF_F_CUSTOMERRECTDO)tmTMTableModule.selByPK(context, ddoTF_F_CUSTOMERRECIn, typeof(TF_F_CUSTOMERRECTDO), null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                context.AddError("A001010106");
                return;
            }

            //从月票表中取公交卡类型
            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
            ddoTF_F_CARDCOUNTACCIn.CARDNO = ddoTF_F_CARDRECOut.CARDNO;
            TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC", null);
            if (ddoTF_F_CARDCOUNTACCOut == null)
            {
                context.AddError("A001002113");
                return;
            }
            //公交类型编码
            TD_M_APPAREATDO ddoTD_M_APPAREATDOIn = new TD_M_APPAREATDO();
            ddoTD_M_APPAREATDOIn.AREACODE = ddoTF_F_CARDCOUNTACCOut.ASSIGNEDAREA;
            TD_M_APPAREATDO ddoTD_M_APPAREATDOOut = (TD_M_APPAREATDO)tmTMTableModule.selByPK(context, ddoTD_M_APPAREATDOIn, typeof(TD_M_APPAREATDO), null, "TD_M_APPAREA", null);

            //从证件类型编码表(TD_M_PAPERTYPE)中读取数据
            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEIn = new TD_M_PAPERTYPETDO();
            ddoTD_M_PAPERTYPEIn.PAPERTYPECODE = ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE;

            TD_M_PAPERTYPETDO ddoTD_M_PAPERTYPEOut = (TD_M_PAPERTYPETDO)tmTMTableModule.selByPK(context, ddoTD_M_PAPERTYPEIn, typeof(TD_M_PAPERTYPETDO), null, "TD_M_PAPERTYPE_DESTROY", null);

            if (ddoTF_F_CUSTOMERRECOut == null)
            {
                Papertype.Text = "";
            }

            //从用户卡库存表(TL_R_ICUSER)中读取数据
            TL_R_ICUSERTDO ddoTL_R_ICUSERIn = new TL_R_ICUSERTDO();
            ddoTL_R_ICUSERIn.CARDNO = txtCardNo.Text;

            TL_R_ICUSERTDO ddoTL_R_ICUSEROut = (TL_R_ICUSERTDO)tmTMTableModule.selByPK(context, ddoTL_R_ICUSERIn, typeof(TL_R_ICUSERTDO), null, "TL_R_ICUSER", null);

            if (ddoTL_R_ICUSEROut == null)
            {
                context.AddError("A001001101");
                return;
            }

            //从资源状态编码表中读取数据
            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEIn = new TD_M_RESOURCESTATETDO();
            ddoTD_M_RESOURCESTATEIn.RESSTATECODE = ddoTL_R_ICUSEROut.RESSTATECODE;

            TD_M_RESOURCESTATETDO ddoTD_M_RESOURCESTATEOut = (TD_M_RESOURCESTATETDO)tmTMTableModule.selByPK(context, ddoTD_M_RESOURCESTATEIn, typeof(TD_M_RESOURCESTATETDO), null, "TD_M_RESOURCESTATE", null);

            if (ddoTD_M_RESOURCESTATEOut == null)
                RESSTATE.Text = ddoTL_R_ICUSEROut.RESSTATECODE;
            else
                RESSTATE.Text = ddoTD_M_RESOURCESTATEOut.RESSTATE;

            //页面显示项赋值
            hiddenCardtypecode.Value = ddoTF_F_CARDRECOut.CARDTYPECODE;
            LabAsn.Text = ddoTF_F_CARDRECOut.ASN;
            LabCardtype.Text = ddoTD_M_APPAREATDOOut.AREANAME;
            sDate.Text = ddoTF_F_CARDCOUNTACCOut.ENDTIME;
            Decimal aMoney = Convert.ToDecimal(ddoTF_F_CARDEWALLETACCOut.CARDACCMONEY);
            accMoney.Text = (aMoney / (Convert.ToDecimal(100))).ToString("0.00");

            CustName.Text = ddoTF_F_CUSTOMERRECOut.CUSTNAME;

            //卡里的年审日期统一成“yyyyMMdd”格式
            if (this.txtPreVerifyDate.Value.Trim().Length != 0)
            {
                this.txtCardEndtime.Text = this.txtPreVerifyDate.Value;
            }

            //性别赋值
            if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "0")
                Custsex.Text = "男";
            else if (ddoTF_F_CUSTOMERRECOut.CUSTSEX == "1")
                Custsex.Text = "女";
            else Custsex.Text = "";
            
            //出生日期赋值
            if (ddoTF_F_CUSTOMERRECOut.CUSTBIRTH != "")
            {
                String Bdate = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
                if (Bdate.Length == 8)
                {
                    CustBirthday.Text = Bdate.Substring(0, 4) + "-" + Bdate.Substring(4, 2) + "-" + Bdate.Substring(6, 2);
                }
                else CustBirthday.Text = Bdate;
            }
            else CustBirthday.Text = ddoTF_F_CUSTOMERRECOut.CUSTBIRTH;
            //证件类型赋值
            if (ddoTF_F_CUSTOMERRECOut.PAPERTYPECODE != "")
            {
                Papertype.Text = ddoTD_M_PAPERTYPEOut.PAPERTYPENAME;
            }
            else Papertype.Text = "";

            Paperno.Text = ddoTF_F_CUSTOMERRECOut.PAPERNO;
            Custaddr.Text = ddoTF_F_CUSTOMERRECOut.CUSTADDR;
            Custpost.Text = ddoTF_F_CUSTOMERRECOut.CUSTPOST;
            Custphone.Text = ddoTF_F_CUSTOMERRECOut.CUSTPHONE;
            txtEmail.Text = ddoTF_F_CUSTOMERRECOut.CUSTEMAIL;
            Remark.Text = ddoTF_F_CUSTOMERRECOut.REMARK;

            //查询卡片开通功能并显示
            PBHelper.openFunc(context, openFunc, txtCardNo.Text);

            hidAccRecv.Value = Total.Text;
            txtRealRecv.Text = Convert.ToInt32(Convert.ToDecimal(Total.Text)).ToString();
            btnYearCheck.Enabled = true;
            btnPrintPZ.Enabled = false;
        }
    }

    /// <summary>
    /// 写卡后执行
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (hidWarning.Value == "yes")
        {
            btnYearCheck.Enabled = true;
        }
        else if (hidWarning.Value == "writeSuccess")
        {
            AddMessage("前台写卡成功，年审至" + this.txtVerifyDate.Value);
            this.txtVerifyDate.Value = "";
            clearCustInfo(this.txtCardNo);
        }
        else if (hidWarning.Value == "writeFail")
        {
            context.AddError("前台写卡失败");
        }
        else if (hidWarning.Value == "submit")
        {
            btnYearCheck_ServerClick(sender, e);
        }
        if (chkPingzheng.Checked && btnPrintPZ.Enabled)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "writeCardScript","printInvoice();", true);
        }
        hidWarning.Value = "";
    }

    /// <summary>
    /// 年审按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnYearCheck_ServerClick(object sender, EventArgs e)
    {
        TMTableModule tmTMTableModule = new TMTableModule();
        //从月票表中取公交卡类型
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCIn = new TF_F_CARDCOUNTACCTDO();
        ddoTF_F_CARDCOUNTACCIn.CARDNO = txtCardNo.Text;
        TF_F_CARDCOUNTACCTDO ddoTF_F_CARDCOUNTACCOut = (TF_F_CARDCOUNTACCTDO)tmTMTableModule.selByPK(context, ddoTF_F_CARDCOUNTACCIn, typeof(TF_F_CARDCOUNTACCTDO), null, "TF_F_CARDCOUNTACC", null);
        if (ddoTF_F_CARDCOUNTACCOut == null)
        {
            context.AddError("A001002113");
            return;
        }
        //公交类型编码
        TD_M_APPAREATDO ddoTD_M_APPAREATDOIn = new TD_M_APPAREATDO();
        ddoTD_M_APPAREATDOIn.AREACODE = ddoTF_F_CARDCOUNTACCOut.ASSIGNEDAREA;
        TD_M_APPAREATDO ddoTD_M_APPAREATDOOut = (TD_M_APPAREATDO)tmTMTableModule.selByPK(context, ddoTD_M_APPAREATDOIn, typeof(TD_M_APPAREATDO), null, "TD_M_APPAREA", null);

        if (ddoTD_M_APPAREATDOOut.AREACODE == "S1" || ddoTD_M_APPAREATDOOut.AREACODE == "S3")
        {
            context.AddError("A002002912:当前类型特种卡不需要年审");
            return;
        }

        if (ddoTD_M_APPAREATDOOut.AREACODE != "S2")
        {
            context.AddError("A002002913:请检查卡片是否是特种优惠卡");
            return;
        }
        else
        {
            if (txtCardNo.Text.Trim().Substring(0, 2) != "80")
            {
                context.AddError("A002002914:非80特种优惠卡由公交公司年审");
                return;
            }
        }
          
        //判断该特种卡当前时间是否可年审
        string year = "20161231";
        int preVerifyYear = 0;
        if (txtCardEndtime.Text.Trim().Length >= 8)
        {
            preVerifyYear = Convert.ToInt16(txtCardEndtime.Text.Trim().Substring(0, 4));
        }
        int currentYear= DateTime.Now.Year;
        int currentMonth = DateTime.Now.Month;
        if (currentMonth == 12)
        {
            if (preVerifyYear <= currentYear)
            {
                year = DateTime.Now.AddYears(Convert.ToInt32(ddlYears.SelectedValue)).Year.ToString() + "1231";
            }
            else
            {
                context.AddError("A002003800:此卡当前时间不可年审");
                return;
            }
        }
        else
        {
            if (preVerifyYear <= currentYear)
            {
                year = DateTime.Now.Year.ToString() + "1231";
            }
            else
            {
                context.AddError("A002003801:此卡当前月份不能年审");
                return;
            }
        }
      
        context.SPOpen();
        context.AddField("p_CARDNO").Value = txtCardNo.Text;
        context.AddField("p_ASN").Value = LabAsn.Text;
        context.AddField("p_CARDTYPECODE").Value = hiddenCardtypecode.Value;
        context.AddField("p_ENDTIME").Value = year;

        bool ok = context.ExecuteSP("SP_BS_SpecialCard_YearCheck");

        if (ok)
        {
            this.txtVerifyDate.Value = year;//给年审函数参数控件赋值
            hidCardReaderToken.Value = cardReader.createToken(context);
            ScriptManager.RegisterStartupScript(
                this, this.GetType(), "writeCardScript", " VerifySpecialCard();", true);


            btnPrintPZ.Enabled = true;

            ASHelper.preparePingZheng(ptnPingZheng, txtCardNo.Text, CustName.Text, "年审", "0.00", "", "", Paperno.Text, "", "",
                Total.Text, context.s_UserName, context.s_DepartName, Papertype.Text, ProcdFee.Text, "");

            btnYearCheck.Enabled = false;

            AddMessage("年审成功");
        }
    }
}
